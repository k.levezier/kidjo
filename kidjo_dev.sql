-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le :  ven. 22 nov. 2019 à 16:06
-- Version du serveur :  5.6.24
-- Version de PHP :  7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `kidjo_dev`
--

-- --------------------------------------------------------

--
-- Structure de la table `blocs`
--

CREATE TABLE `blocs` (
  `id_bloc` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `blocs_elements`
--

CREATE TABLE `blocs_elements` (
  `id` int(11) NOT NULL,
  `id_bloc` int(11) DEFAULT NULL,
  `id_element` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `value` text NOT NULL,
  `complement` text NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `contact_form`
--

CREATE TABLE `contact_form` (
  `id_contact` int(11) NOT NULL,
  `unikey` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cookies_visites`
--

CREATE TABLE `cookies_visites` (
  `id_cookie` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `cookie` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `countries`
--

CREATE TABLE `countries` (
  `id_pays` int(11) NOT NULL,
  `code` varchar(2) NOT NULL,
  `fr` varchar(255) NOT NULL,
  `en` varchar(255) NOT NULL,
  `de` varchar(255) NOT NULL,
  `es` varchar(255) NOT NULL,
  `nl` varchar(255) NOT NULL,
  `it` varchar(255) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `capital` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `ordre` int(11) NOT NULL,
  `id_zone` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `countries`
--

INSERT INTO `countries` (`id_pays`, `code`, `fr`, `en`, `de`, `es`, `nl`, `it`, `latitude`, `longitude`, `currency`, `capital`, `region`, `ordre`, `id_zone`, `status`, `added`, `updated`) VALUES
(1, 'aw', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba', 'Aruba', '12.5', '-69.96666666', 'AWG', 'Oranjestad', 'Americas', 0, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(2, 'af', 'Afghanistan', 'Afghanistan', 'Afghanistan', 'Afganistán', 'Afghanistan', 'Afghanistan', '33', '65', 'AFN', 'Kabul', 'Asia', 1, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(3, 'ao', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola', 'Angola', '-12.5', '18.5', 'AOA', 'Luanda', 'Africa', 2, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(4, 'ai', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', 'Anguilla', '18.25', '-63.16666666', 'XCD', 'The Valley', 'Americas', 3, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(5, 'ax', 'Åland', 'Åland Islands', 'Åland', 'Alandia', 'Ålandeilanden', 'Isole Aland', '60.116667', '19.9', 'EUR', 'Mariehamn', 'Europe', 4, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(6, 'al', 'Albanie', 'Albania', 'Albanien', 'Albania', 'Albanië', 'Albania', '41', '20', 'ALL', 'Tirana', 'Europe', 5, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(7, 'ad', 'Andorre', 'Andorra', 'Andorra', 'Andorra', 'Andorra', 'Andorra', '42.5', '1.5', 'EUR', 'Andorra la Vella', 'Europe', 6, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(8, 'ae', 'Émirats arabes unis', 'United Arab Emirates', 'Vereinigte Arabische Emirate', 'Emiratos Árabes Unidos', 'Verenigde Arabische Emiraten', 'Emirati Arabi Uniti', '24', '54', 'AED', 'Abu Dhabi', 'Asia', 7, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(9, 'ar', 'Argentine', 'Argentina', 'Argentinien', 'Argentina', 'Argentinië', 'Argentina', '-34', '-64', 'ARS', 'Buenos Aires', 'Americas', 8, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(10, 'am', 'Arménie', 'Armenia', 'Armenien', 'Armenia', 'Armenië', 'Armenia', '40', '45', 'AMD', 'Yerevan', 'Asia', 9, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(11, 'as', 'Samoa américaines', 'American Samoa', 'Amerikanisch-Samoa', 'Samoa Americana', 'Amerikaans Samoa', 'Samoa Americane', '-14.33333333', '-170', 'USD', 'Pago Pago', 'Oceania', 10, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(12, 'aq', 'Antarctique', 'Antarctica', 'Antarktis', 'Antártida', 'Antarctica', 'Antartide', '-90', '0', '', '', '', 11, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(13, 'tf', 'Terres australes et antarctiques françaises', 'French Southern and Antarctic Lands', 'Französische Süd- und Antarktisgebiete', 'Tierras Australes y Antárticas Francesas', 'Franse Gebieden in de zuidelijke Indische Oceaan', 'Territori Francesi del Sud', '-49.25', '69.167', 'EUR', 'Port-aux-Français', '', 12, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(14, 'ag', 'Antigua-et-Barbuda', 'Antigua and Barbuda', 'Antigua und Barbuda', 'Antigua y Barbuda', 'Antigua en Barbuda', 'Antigua e Barbuda', '17.05', '-61.8', 'XCD', 'Saint John\'s', 'Americas', 13, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(15, 'au', 'Australie', 'Australia', 'Australien', 'Australia', 'Australië', 'Australia', '-27', '133', 'AUD', 'Canberra', 'Oceania', 14, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(16, 'at', 'Autriche', 'Austria', 'Österreich', 'Austria', 'Oostenrijk', 'Austria', '47.33333333', '13.33333333', 'EUR', 'Vienna', 'Europe', 15, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(17, 'az', 'Azerbaïdjan', 'Azerbaijan', 'Aserbaidschan', 'Azerbaiyán', 'Azerbeidzjan', 'Azerbaijan', '40.5', '47.5', 'AZN', 'Baku', 'Asia', 16, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(18, 'bi', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi', 'Burundi', '-3.5', '30', 'BIF', 'Bujumbura', 'Africa', 17, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(19, 'be', 'Belgique', 'Belgium', 'Belgien', 'Bélgica', 'België', 'Belgio', '50.83333333', '4', 'EUR', 'Brussels', 'Europe', 18, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(20, 'bj', 'Bénin', 'Benin', 'Benin', 'Benín', 'Benin', 'Benin', '9.5', '2.25', 'XOF', 'Porto-Novo', 'Africa', 19, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(21, 'bq', '', 'Bonaire', '', '', '', '', '12.15', '-68.266667', 'USD', 'Kralendijk', 'Americas', 20, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(22, 'bf', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso', '13', '-2', 'XOF', 'Ouagadougou', 'Africa', 21, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(23, 'bd', 'Bangladesh', 'Bangladesh', 'Bangladesch', 'Bangladesh', 'Bangladesh', 'Bangladesh', '24', '90', 'BDT', 'Dhaka', 'Asia', 22, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(24, 'bg', 'Bulgarie', 'Bulgaria', 'Bulgarien', 'Bulgaria', 'Bulgarije', 'Bulgaria', '43', '25', 'BGN', 'Sofia', 'Europe', 23, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(25, 'bh', 'Bahreïn', 'Bahrain', 'Bahrain', 'Bahrein', 'Bahrein', 'Bahrein', '26', '50.55', 'BHD', 'Manama', 'Asia', 24, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(26, 'bs', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahamas', 'Bahama’s', 'Bahamas', '24.25', '-76', 'BSD', 'Nassau', 'Americas', 25, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(27, 'ba', 'Bosnie-Herzégovine', 'Bosnia and Herzegovina', 'Bosnien und Herzegowina', 'Bosnia y Herzegovina', 'Bosnië en Herzegovina', 'Bosnia ed Erzegovina', '44', '18', 'BAM', 'Sarajevo', 'Europe', 26, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(28, 'bl', 'Saint-Barthélemy', 'Saint Barthélemy', 'Saint-Barthélemy', 'San Bartolomé', 'Saint Barthélemy', 'Antille Francesi', '18.5', '-63.41666666', 'EUR', 'Gustavia', 'Americas', 27, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(29, 'by', 'Biélorussie', 'Belarus', 'Weißrussland', 'Bielorrusia', 'Wit-Rusland', 'Bielorussia', '53', '28', 'BYR', 'Minsk', 'Europe', 28, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(30, 'bz', 'Belize', 'Belize', 'Belize', 'Belice', 'Belize', 'Belize', '17.25', '-88.75', 'BZD', 'Belmopan', 'Americas', 29, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(31, 'bm', 'Bermudes', 'Bermuda', 'Bermuda', 'Bermudas', 'Bermuda', 'Bermuda', '32.33333333', '-64.75', 'BMD', 'Hamilton', 'Americas', 30, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(32, 'bo', 'Bolivie', 'Bolivia', 'Bolivien', 'Bolivia', 'Bolivia', 'Bolivia', '-17', '-65', 'BOB', 'Sucre', 'Americas', 31, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(33, 'br', 'Brésil', 'Brazil', 'Brasilien', 'Brasil', 'Brazilië', 'Brasile', '-10', '-55', 'BRL', 'Brasília', 'Americas', 32, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(34, 'bb', 'Barbade', 'Barbados', 'Barbados', 'Barbados', 'Barbados', 'Barbados', '13.16666666', '-59.53333333', 'BBD', 'Bridgetown', 'Americas', 33, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(35, 'bn', 'Brunei', 'Brunei', 'Brunei', 'Brunei', 'Brunei', 'Brunei', '4.5', '114.66666666', 'BND', 'Bandar Seri Begawan', 'Asia', 34, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(36, 'bt', 'Bhoutan', 'Bhutan', 'Bhutan', 'Bután', 'Bhutan', 'Bhutan', '27.5', '90.5', 'BTN', 'Thimphu', 'Asia', 35, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(37, 'bv', 'Île Bouvet', 'Bouvet Island', 'Bouvetinsel', 'Isla Bouvet', 'Bouveteiland', 'Isola Bouvet', '-54.43333333', '3.4', 'NOK', '', '', 36, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(38, 'bw', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana', 'Botswana', '-22', '24', 'BWP', 'Gaborone', 'Africa', 37, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(39, 'cf', 'République centrafricaine', 'Central African Republic', 'Zentralafrikanische Republik', 'República Centroafricana', 'Centraal-Afrikaanse Republiek', 'Repubblica Centrafricana', '7', '21', 'XAF', 'Bangui', 'Africa', 38, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(40, 'ca', 'Canada', 'Canada', 'Kanada', 'Canadá', 'Canada', 'Canada', '60', '-95', 'CAD', 'Ottawa', 'Americas', 39, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(41, 'cc', 'Îles Cocos', 'Cocos (Keeling) Islands', 'Kokosinseln', 'Islas Cocos o Islas Keeling', 'Cocoseilanden', 'Isole Cocos e Keeling', '-12.5', '96.83333333', 'AUD', 'West Island', 'Oceania', 40, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(42, 'ch', 'Suisse', 'Switzerland', 'Schweiz', 'Suiza', 'Zwitserland', 'Svizzera', '47', '8', 'CHE', 'Bern', 'Europe', 41, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(43, 'cl', 'Chili', 'Chile', 'Chile', 'Chile', 'Chili', 'Cile', '-30', '-71', 'CLF', 'Santiago', 'Americas', 42, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(44, 'cn', 'Chine', 'China', 'China', 'China', 'China', 'Cina', '35', '105', 'CNY', 'Beijing', 'Asia', 43, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(45, 'ci', 'Côte d\'Ivoire', 'Ivory Coast', 'Elfenbeinküste', 'Costa de Marfil', 'Ivoorkust', 'Costa D\'Avorio', '8', '-5', 'XOF', 'Yamoussoukro', 'Africa', 44, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(46, 'cm', 'Cameroun', 'Cameroon', 'Kamerun', 'Camerún', 'Kameroen', 'Camerun', '6', '12', 'XAF', 'Yaoundé', 'Africa', 45, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(47, 'cd', 'Congo (Rép. dém.)', 'DR Congo', 'Kongo (Dem. Rep.)', 'Congo (Rep. Dem.)', 'Congo (DRC)', 'Congo (Rep. Dem.)', '0', '25', 'CDF', 'Kinshasa', 'Africa', 46, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(48, 'cg', 'Congo', 'Republic of the Congo', 'Kongo', 'Congo', 'Congo', 'Congo', '-1', '15', 'XAF', 'Brazzaville', 'Africa', 47, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(49, 'ck', 'Îles Cook', 'Cook Islands', 'Cookinseln', 'Islas Cook', 'Cookeilanden', 'Isole Cook', '-21.23333333', '-159.76666666', 'NZD', 'Avarua', 'Oceania', 48, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(50, 'co', 'Colombie', 'Colombia', 'Kolumbien', 'Colombia', 'Colombia', 'Colombia', '4', '-72', 'COP', 'Bogotá', 'Americas', 49, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(51, 'km', 'Comores', 'Comoros', 'Union der Komoren', 'Comoras', 'Comoren', 'Comore', '-12.16666666', '44.25', 'KMF', 'Moroni', 'Africa', 50, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(52, 'cv', 'Cap Vert', 'Cape Verde', 'Kap Verde', 'Cabo Verde', 'Kaapverdië', 'Capo Verde', '16', '-24', 'CVE', 'Praia', 'Africa', 51, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(53, 'cr', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica', 'Costa Rica', '10', '-84', 'CRC', 'San José', 'Americas', 52, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(54, 'cu', 'Cuba', 'Cuba', 'Kuba', 'Cuba', 'Cuba', 'Cuba', '21.5', '-80', 'CUC', 'Havana', 'Americas', 53, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(55, 'cw', '', 'Curaçao', '', '', 'Curaçao', '', '12.116667', '-68.933333', 'ANG', 'Willemstad', 'Americas', 54, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(56, 'cx', 'Île Christmas', 'Christmas Island', 'Weihnachtsinsel', 'Isla de Navidad', 'Christmaseiland', 'Isola di Natale', '-10.5', '105.66666666', 'AUD', 'Flying Fish Cove', 'Oceania', 55, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(57, 'ky', 'Îles Caïmans', 'Cayman Islands', 'Kaimaninseln', 'Islas Caimán', 'Caymaneilanden', 'Isole Cayman', '19.5', '-80.5', 'KYD', 'George Town', 'Americas', 56, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(58, 'cy', 'Chypre', 'Cyprus', 'Zypern', 'Chipre', 'Cyprus', 'Cipro', '35', '33', 'EUR', 'Nicosia', 'Europe', 57, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(59, 'cz', 'République tchèque', 'Czech Republic', 'Tschechische Republik', 'República Checa', 'Tsjechië', 'Repubblica Ceca', '49.75', '15.5', 'CZK', 'Prague', 'Europe', 58, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(60, 'de', 'Allemagne', 'Germany', 'Deutschland', 'Alemania', 'Duitsland', 'Germania', '51', '9', 'EUR', 'Berlin', 'Europe', 59, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(61, 'dj', 'Djibouti', 'Djibouti', 'Dschibuti', 'Yibuti', 'Djibouti', 'Gibuti', '11.5', '43', 'DJF', 'Djibouti', 'Africa', 60, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(62, 'dm', 'Dominique', 'Dominica', 'Dominica', 'Dominica', 'Dominica', 'Dominica', '15.41666666', '-61.33333333', 'XCD', 'Roseau', 'Americas', 61, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(63, 'dk', 'Danemark', 'Denmark', 'Dänemark', 'Dinamarca', 'Denemarken', 'Danimarca', '56', '10', 'DKK', 'Copenhagen', 'Europe', 62, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(64, 'do', 'République dominicaine', 'Dominican Republic', 'Dominikanische Republik', 'República Dominicana', 'Dominicaanse Republiek', 'Repubblica Dominicana', '19', '-70.66666666', 'DOP', 'Santo Domingo', 'Americas', 63, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(65, 'dz', 'Algérie', 'Algeria', 'Algerien', 'Argelia', 'Algerije', 'Algeria', '28', '3', 'DZD', 'Algiers', 'Africa', 64, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(66, 'ec', 'Équateur', 'Ecuador', 'Ecuador', 'Ecuador', 'Ecuador', 'Ecuador', '-2', '-77.5', 'USD', 'Quito', 'Americas', 65, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(67, 'eg', 'Égypte', 'Egypt', 'Ägypten', 'Egipto', 'Egypte', 'Egitto', '27', '30', 'EGP', 'Cairo', 'Africa', 66, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(68, 'er', 'Érythrée', 'Eritrea', 'Eritrea', 'Eritrea', 'Eritrea', 'Eritrea', '15', '39', 'ERN', 'Asmara', 'Africa', 67, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(69, 'eh', 'Sahara Occidental', 'Western Sahara', 'Westsahara', 'Sahara Occidental', 'Westelijke Sahara', 'Sahara Occidentale', '24.5', '-13', 'MAD', 'El Aaiún', 'Africa', 68, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(70, 'es', 'Espagne', 'Spain', 'Spanien', 'España', 'Spanje', 'Spagna', '40', '-4', 'EUR', 'Madrid', 'Europe', 69, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(71, 'ee', 'Estonie', 'Estonia', 'Estland', 'Estonia', 'Estland', 'Estonia', '59', '26', 'EUR', 'Tallinn', 'Europe', 70, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(72, 'et', 'Éthiopie', 'Ethiopia', 'Äthiopien', 'Etiopía', 'Ethiopië', 'Etiopia', '8', '38', 'ETB', 'Addis Ababa', 'Africa', 71, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(73, 'fi', 'Finlande', 'Finland', 'Finnland', 'Finlandia', 'Finland', 'Finlandia', '64', '26', 'EUR', 'Helsinki', 'Europe', 72, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(74, 'fj', 'Fidji', 'Fiji', 'Fidschi', 'Fiyi', 'Fiji', 'Figi', '-18', '175', 'FJD', 'Suva', 'Oceania', 73, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(75, 'fk', 'Îles Malouines', 'Falkland Islands', 'Falklandinseln', 'Islas Malvinas', 'Falklandeilanden', 'Isole Falkland o Isole Malvine', '-51.75', '-59', 'FKP', 'Stanley', 'Americas', 74, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(76, 'fr', 'France', 'France', 'Frankreich', 'Francia', 'Frankrijk', 'Francia', '46', '2', 'EUR', 'Paris', 'Europe', 75, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(77, 'fo', 'Îles Féroé', 'Faroe Islands', 'Färöer-Inseln', 'Islas Faroe', 'Faeröer', 'Isole Far Oer', '62', '-7', 'DKK', 'Tórshavn', 'Europe', 76, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(78, 'fm', 'Micronésie', 'Micronesia', 'Mikronesien', 'Micronesia', 'Micronesië', 'Micronesia', '6.91666666', '158.25', 'USD', 'Palikir', 'Oceania', 77, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(79, 'ga', 'Gabon', 'Gabon', 'Gabun', 'Gabón', 'Gabon', 'Gabon', '-1', '11.75', 'XAF', 'Libreville', 'Africa', 78, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(80, 'gb', 'Royaume-Uni', 'United Kingdom', 'Vereinigtes Königreich', 'Reino Unido', 'Verenigd Koninkrijk', 'Regno Unito', '54', '-2', 'GBP', 'London', 'Europe', 79, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(81, 'ge', 'Géorgie', 'Georgia', 'Georgien', 'Georgia', 'Georgië', 'Georgia', '42', '43.5', 'GEL', 'Tbilisi', 'Asia', 80, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(82, 'gg', 'Guernesey', 'Guernsey', 'Guernsey', 'Guernsey', 'Guernsey', 'Guernsey', '49.46666666', '-2.58333333', 'GBP', 'St. Peter Port', 'Europe', 81, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(83, 'gh', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana', 'Ghana', '8', '-2', 'GHS', 'Accra', 'Africa', 82, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(84, 'gi', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibraltar', 'Gibilterra', '36.13333333', '-5.35', 'GIP', 'Gibraltar', 'Europe', 83, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(85, 'gn', 'Guinée', 'Guinea', 'Guinea', 'Guinea', 'Guinee', 'Guinea', '11', '-10', 'GNF', 'Conakry', 'Africa', 84, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(86, 'gp', 'Guadeloupe', 'Guadeloupe', 'Guadeloupe', 'Guadalupe', 'Guadeloupe', 'Guadeloupa', '16.25', '-61.583333', 'EUR', 'Basse-Terre', 'Americas', 85, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(87, 'gm', 'Gambie', 'Gambia', 'Gambia', 'Gambia', 'Gambia', 'Gambia', '13.46666666', '-16.56666666', 'GMD', 'Banjul', 'Africa', 86, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(88, 'gw', 'Guinée-Bissau', 'Guinea-Bissau', 'Guinea-Bissau', 'Guinea-Bisáu', 'Guinee-Bissau', 'Guinea-Bissau', '12', '-15', 'XOF', 'Bissau', 'Africa', 87, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(89, 'gq', 'Guinée-Équatoriale', 'Equatorial Guinea', 'Äquatorial-Guinea', 'Guinea Ecuatorial', 'Equatoriaal-Guinea', 'Guinea Equatoriale', '2', '10', 'XAF', 'Malabo', 'Africa', 88, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(90, 'gr', 'Grèce', 'Greece', 'Griechenland', 'Grecia', 'Griekenland', 'Grecia', '39', '22', 'EUR', 'Athens', 'Europe', 89, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(91, 'gd', 'Grenade', 'Grenada', 'Grenada', 'Grenada', 'Grenada', 'Grenada', '12.11666666', '-61.66666666', 'XCD', 'St. George\'s', 'Americas', 90, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(92, 'gl', 'Groenland', 'Greenland', 'Grönland', 'Groenlandia', 'Groenland', 'Groenlandia', '72', '-40', 'DKK', 'Nuuk', 'Americas', 91, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(93, 'gt', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', 'Guatemala', '15.5', '-90.25', 'GTQ', 'Guatemala City', 'Americas', 92, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(94, 'gf', 'Guayane', 'French Guiana', 'Französisch Guyana', 'Guayana Francesa', 'Frans-Guyana', 'Guyana francese', '4', '-53', 'EUR', 'Cayenne', 'Americas', 93, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(95, 'gu', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam', 'Guam', '13.46666666', '144.78333333', 'USD', 'Hagåtña', 'Oceania', 94, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(96, 'gy', 'Guyane', 'Guyana', 'Guyana', 'Guyana', 'Guyana', 'Guyana', '5', '-59', 'GYD', 'Georgetown', 'Americas', 95, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(97, 'hk', 'Hong Kong', 'Hong Kong', 'Hongkong', 'Hong Kong', 'Hongkong', 'Hong Kong', '22.267', '114.188', 'HKD', 'City of Victoria', 'Asia', 96, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(98, 'hm', 'Îles Heard-et-MacDonald', 'Heard Island and McDonald Islands', 'Heard und die McDonaldinseln', 'Islas Heard y McDonald', 'Heard- en McDonaldeilanden', 'Isole Heard e McDonald', '-53.1', '72.51666666', 'AUD', '', '', 97, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(99, 'hn', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras', 'Honduras', '15', '-86.5', 'HNL', 'Tegucigalpa', 'Americas', 98, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(100, 'hr', 'Croatie', 'Croatia', 'Kroatien', 'Croacia', 'Kroatië', 'Croazia', '45.16666666', '15.5', 'HRK', 'Zagreb', 'Europe', 99, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(101, 'ht', 'Haïti', 'Haiti', 'Haiti', 'Haiti', 'Haïti', 'Haiti', '19', '-72.41666666', 'HTG', 'Port-au-Prince', 'Americas', 100, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(102, 'hu', 'Hongrie', 'Hungary', 'Ungarn', 'Hungría', 'Hongarije', 'Ungheria', '47', '20', 'HUF', 'Budapest', 'Europe', 101, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(103, 'id', 'Indonésie', 'Indonesia', 'Indonesien', 'Indonesia', 'Indonesië', 'Indonesia', '-5', '120', 'IDR', 'Jakarta', 'Asia', 102, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(104, 'im', 'Île de Man', 'Isle of Man', 'Insel Man', 'Isla de Man', 'Isle of Man', 'Isola di Man', '54.25', '-4.5', 'GBP', 'Douglas', 'Europe', 103, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(105, 'in', 'Inde', 'India', 'Indien', 'India', 'India', 'India', '20', '77', 'INR', 'New Delhi', 'Asia', 104, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(106, 'io', 'Territoire britannique de l\'océan Indien', 'British Indian Ocean Territory', 'Britisches Territorium im Indischen Ozean', 'Territorio Británico del Océano Índico', 'Britse Gebieden in de Indische Oceaan', 'Territorio britannico dell\'oceano indiano', '-6', '71.5', 'USD', 'Diego Garcia', 'Africa', 105, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(107, 'ie', 'Irlande', 'Ireland', 'Irland', 'Irlanda', 'Ierland', 'Irlanda', '53', '-8', 'EUR', 'Dublin', 'Europe', 106, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(108, 'ir', 'Iran', 'Iran', 'Iran', 'Iran', 'Iran', '', '32', '53', 'IRR', 'Tehran', 'Asia', 107, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(109, 'iq', 'Irak', 'Iraq', 'Irak', 'Irak', 'Irak', 'Iraq', '33', '44', 'IQD', 'Baghdad', 'Asia', 108, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(110, 'is', 'Islande', 'Iceland', 'Island', 'Islandia', 'IJsland', 'Islanda', '65', '-18', 'ISK', 'Reykjavik', 'Europe', 109, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(111, 'il', 'Israël', 'Israel', 'Israel', 'Israel', 'Israël', 'Israele', '31.47', '35.13', 'ILS', 'Jerusalem', 'Asia', 110, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(112, 'it', 'Italie', 'Italy', 'Italien', 'Italia', 'Italië', 'Italia', '42.83333333', '12.83333333', 'EUR', 'Rome', 'Europe', 111, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(113, 'jm', 'Jamaïque', 'Jamaica', 'Jamaika', 'Jamaica', 'Jamaica', 'Giamaica', '18.25', '-77.5', 'JMD', 'Kingston', 'Americas', 112, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(114, 'je', 'Jersey', 'Jersey', 'Jersey', 'Jersey', 'Jersey', 'Isola di Jersey', '49.25', '-2.16666666', 'GBP', 'Saint Helier', 'Europe', 113, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(115, 'jo', 'Jordanie', 'Jordan', 'Jordanien', 'Jordania', 'Jordanië', 'Giordania', '31', '36', 'JOD', 'Amman', 'Asia', 114, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(116, 'jp', 'Japon', 'Japan', 'Japan', 'Japón', 'Japan', 'Giappone', '36', '138', 'JPY', 'Tokyo', 'Asia', 115, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(117, 'kz', 'Kazakhstan', 'Kazakhstan', 'Kasachstan', 'Kazajistán', 'Kazachstan', 'Kazakistan', '48', '68', 'KZT', 'Astana', 'Asia', 116, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(118, 'ke', 'Kenya', 'Kenya', 'Kenia', 'Kenia', 'Kenia', 'Kenya', '1', '38', 'KES', 'Nairobi', 'Africa', 117, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(119, 'kg', 'Kirghizistan', 'Kyrgyzstan', 'Kirgisistan', 'Kirguizistán', 'Kirgizië', 'Kirghizistan', '41', '75', 'KGS', 'Bishkek', 'Asia', 118, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(120, 'kh', 'Cambodge', 'Cambodia', 'Kambodscha', 'Camboya', 'Cambodja', 'Cambogia', '13', '105', 'KHR', 'Phnom Penh', 'Asia', 119, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(121, 'ki', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', 'Kiribati', '1.41666666', '173', 'AUD', 'South Tarawa', 'Oceania', 120, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(122, 'kn', 'Saint-Christophe-et-Niévès', 'Saint Kitts and Nevis', 'Saint Christopher und Nevis', 'San Cristóbal y Nieves', 'Saint Kitts en Nevis', 'Saint Kitts e Nevis', '17.33333333', '-62.75', 'XCD', 'Basseterre', 'Americas', 121, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(123, 'kr', 'Corée du Sud', 'South Korea', 'Südkorea', 'Corea del Sur', 'Zuid-Korea', 'Corea del Sud', '37', '127.5', 'KRW', 'Seoul', 'Asia', 122, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(124, 'xk', '', 'Kosovo', '', 'Kosovo', '', '', '42.666667', '21.166667', 'EUR', 'Pristina', 'Europe', 123, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(125, 'kw', 'Koweït', 'Kuwait', 'Kuwait', 'Kuwait', 'Koeweit', 'Kuwait', '29.5', '45.75', 'KWD', 'Kuwait City', 'Asia', 124, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(126, 'la', 'Laos', 'Laos', 'Laos', 'Laos', 'Laos', 'Laos', '18', '105', 'LAK', 'Vientiane', 'Asia', 125, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(127, 'lb', 'Liban', 'Lebanon', 'Libanon', 'Líbano', 'Libanon', 'Libano', '33.83333333', '35.83333333', 'LBP', 'Beirut', 'Asia', 126, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(128, 'lr', 'Liberia', 'Liberia', 'Liberia', 'Liberia', 'Liberia', 'Liberia', '6.5', '-9.5', 'LRD', 'Monrovia', 'Africa', 127, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(129, 'ly', 'Libye', 'Libya', 'Libyen', 'Libia', 'Libië', 'Libia', '25', '17', 'LYD', 'Tripoli', 'Africa', 128, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(130, 'lc', 'Saint-Lucie', 'Saint Lucia', 'Saint Lucia', 'Santa Lucía', 'Saint Lucia', 'Santa Lucia', '13.88333333', '-60.96666666', 'XCD', 'Castries', 'Americas', 129, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(131, 'li', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', 'Liechtenstein', '47.26666666', '9.53333333', 'CHF', 'Vaduz', 'Europe', 130, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(132, 'lk', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka', '7', '81', 'LKR', 'Colombo', 'Asia', 131, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(133, 'ls', 'Lesotho', 'Lesotho', 'Lesotho', 'Lesotho', 'Lesotho', 'Lesotho', '-29.5', '28.5', 'LSL', 'Maseru', 'Africa', 132, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(134, 'lt', 'Lituanie', 'Lithuania', 'Litauen', 'Lituania', 'Litouwen', 'Lituania', '56', '24', 'LTL', 'Vilnius', 'Europe', 133, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(135, 'lu', 'Luxembourg', 'Luxembourg', 'Luxemburg', 'Luxemburgo', 'Luxemburg', 'Lussemburgo', '49.75', '6.16666666', 'EUR', 'Luxembourg', 'Europe', 134, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(136, 'lv', 'Lettonie', 'Latvia', 'Lettland', 'Letonia', 'Letland', 'Lettonia', '57', '25', 'EUR', 'Riga', 'Europe', 135, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(137, 'mo', 'Macao', 'Macau', 'Macao', 'Macao', 'Macao', 'Macao', '22.16666666', '113.55', 'MOP', '', 'Asia', 136, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(138, 'mf', 'Saint-Martin', 'Saint Martin', 'Saint Martin', 'Saint Martin', 'Saint-Martin', 'Saint Martin', '18.08333333', '-63.95', 'EUR', 'Marigot', 'Americas', 137, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(139, 'ma', 'Maroc', 'Morocco', 'Marokko', 'Marruecos', 'Marokko', 'Marocco', '32', '-5', 'MAD', 'Rabat', 'Africa', 138, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(140, 'mc', 'Monaco', 'Monaco', 'Monaco', 'Mónaco', 'Monaco', 'Principato di Monaco', '43.73333333', '7.4', 'EUR', 'Monaco', 'Europe', 139, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(141, 'md', 'Moldavie', 'Moldova', 'Moldawie', 'Moldavia', 'Moldavië', 'Moldavia', '47', '29', 'MDL', 'Chișinău', 'Europe', 140, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(142, 'mg', 'Madagascar', 'Madagascar', 'Madagaskar', 'Madagascar', 'Madagaskar', 'Madagascar', '-20', '47', 'MGA', 'Antananarivo', 'Africa', 141, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(143, 'mv', 'Maldives', 'Maldives', 'Malediven', 'Maldivas', 'Maldiven', 'Maldive', '3.25', '73', 'MVR', 'Malé', 'Asia', 142, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(144, 'mx', 'Mexique', 'Mexico', 'Mexiko', 'México', 'Mexico', 'Messico', '23', '-102', 'MXN', 'Mexico City', 'Americas', 143, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(145, 'mh', 'Îles Marshall', 'Marshall Islands', 'Marshallinseln', 'Islas Marshall', 'Marshalleilanden', 'Isole Marshall', '9', '168', 'USD', 'Majuro', 'Oceania', 144, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(146, 'mk', 'Macédoine', 'Macedonia', 'Mazedonien', 'Macedonia', 'Macedonië', 'Macedonia', '41.83333333', '22', 'MKD', 'Skopje', 'Europe', 145, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(147, 'ml', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali', 'Mali', '17', '-4', 'XOF', 'Bamako', 'Africa', 146, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(148, 'mt', 'Malte', 'Malta', 'Malta', 'Malta', 'Malta', 'Malta', '35.83333333', '14.58333333', 'EUR', 'Valletta', 'Europe', 147, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(149, 'mm', 'Myanmar', 'Myanmar', 'Myanmar', 'Myanmar', 'Myanmar', 'Birmania', '22', '98', 'MMK', 'Naypyidaw', 'Asia', 148, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(150, 'me', 'Monténégro', 'Montenegro', 'Montenegro', 'Montenegro', 'Montenegro', 'Montenegro', '42.5', '19.3', 'EUR', 'Podgorica', 'Europe', 149, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(151, 'mn', 'Mongolie', 'Mongolia', 'Mongolei', 'Mongolia', 'Mongolië', 'Mongolia', '46', '105', 'MNT', 'Ulan Bator', 'Asia', 150, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(152, 'mp', 'Îles Mariannes du Nord', 'Northern Mariana Islands', 'Nördliche Marianen', 'Islas Marianas del Norte', 'Noordelijke Marianeneilanden', 'Isole Marianne Settentrionali', '15.2', '145.75', 'USD', 'Saipan', 'Oceania', 151, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(153, 'mz', 'Mozambique', 'Mozambique', 'Mosambik', 'Mozambique', 'Mozambique', 'Mozambico', '-18.25', '35', 'MZN', 'Maputo', 'Africa', 152, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(154, 'mr', 'Mauritanie', 'Mauritania', 'Mauretanien', 'Mauritania', 'Mauritanië', 'Mauritania', '20', '-12', 'MRO', 'Nouakchott', 'Africa', 153, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(155, 'ms', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', 'Montserrat', '16.75', '-62.2', 'XCD', 'Plymouth', 'Americas', 154, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(156, 'mq', 'Martinique', 'Martinique', 'Martinique', 'Martinica', 'Martinique', 'Martinica', '14.666667', '-61', 'EUR', 'Fort-de-France', 'Americas', 155, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(157, 'mu', 'Île Maurice', 'Mauritius', 'Mauritius', 'Mauricio', 'Mauritius', 'Mauritius', '-20.28333333', '57.55', 'MUR', 'Port Louis', 'Africa', 156, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(158, 'mw', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi', 'Malawi', '-13.5', '34', 'MWK', 'Lilongwe', 'Africa', 157, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(159, 'my', 'Malaisie', 'Malaysia', 'Malaysia', 'Malasia', 'Maleisië', 'Malesia', '2.5', '112.5', 'MYR', 'Kuala Lumpur', 'Asia', 158, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(160, 'yt', 'Mayotte', 'Mayotte', 'Mayotte', 'Mayotte', 'Mayotte', 'Mayotte', '-12.83333333', '45.16666666', 'EUR', 'Mamoudzou', 'Africa', 159, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(161, 'na', 'Namibie', 'Namibia', 'Namibia', 'Namibia', 'Namibië', 'Namibia', '-22', '17', 'NAD', 'Windhoek', 'Africa', 160, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(162, 'nc', 'Nouvelle-Calédonie', 'New Caledonia', 'Neukaledonien', 'Nueva Caledonia', 'Nieuw-Caledonië', 'Nuova Caledonia', '-21.5', '165.5', 'XPF', 'Nouméa', 'Oceania', 161, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(163, 'ne', 'Niger', 'Niger', 'Niger', 'Níger', 'Niger', 'Niger', '16', '8', 'XOF', 'Niamey', 'Africa', 162, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(164, 'nf', 'Île de Norfolk', 'Norfolk Island', 'Norfolkinsel', 'Isla de Norfolk', 'Norfolkeiland', 'Isola Norfolk', '-29.03333333', '167.95', 'AUD', 'Kingston', 'Oceania', 163, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(165, 'ng', 'Nigéria', 'Nigeria', 'Nigeria', 'Nigeria', 'Nigeria', 'Nigeria', '10', '8', 'NGN', 'Abuja', 'Africa', 164, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(166, 'ni', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', 'Nicaragua', '13', '-85', 'NIO', 'Managua', 'Americas', 165, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(167, 'nu', 'Niue', 'Niue', 'Niue', 'Niue', 'Niue', 'Niue', '-19.03333333', '-169.86666666', 'NZD', 'Alofi', 'Oceania', 166, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(168, 'nl', 'Pays-Bas', 'Netherlands', 'Niederlande', 'Países Bajos', 'Nederland', 'Paesi Bassi', '52.5', '5.75', 'EUR', 'Amsterdam', 'Europe', 167, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(169, 'no', 'Norvège', 'Norway', 'Norwegen', 'Noruega', 'Noorwegen', 'Norvegia', '62', '10', 'NOK', 'Oslo', 'Europe', 168, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(170, 'np', 'Népal', 'Nepal', 'Népal', 'Nepal', 'Nepal', 'Nepal', '28', '84', 'NPR', 'Kathmandu', 'Asia', 169, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(171, 'nr', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru', 'Nauru', '-0.53333333', '166.91666666', 'AUD', 'Yaren', 'Oceania', 170, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(172, 'nz', 'Nouvelle-Zélande', 'New Zealand', 'Neuseeland', 'Nueva Zelanda', 'Nieuw-Zeeland', 'Nuova Zelanda', '-41', '174', 'NZD', 'Wellington', 'Oceania', 171, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(173, 'om', 'Oman', 'Oman', 'Oman', 'Omán', 'Oman', 'oman', '21', '57', 'OMR', 'Muscat', 'Asia', 172, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(174, 'pk', 'Pakistan', 'Pakistan', 'Pakistan', 'Pakistán', 'Pakistan', 'Pakistan', '30', '70', 'PKR', 'Islamabad', 'Asia', 173, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(175, 'pa', 'Panama', 'Panama', 'Panama', 'Panamá', 'Panama', 'Panama', '9', '-80', 'PAB', 'Panama City', 'Americas', 174, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(176, 'pn', 'Îles Pitcairn', 'Pitcairn Islands', 'Pitcairn', 'Islas Pitcairn', 'Pitcairneilanden', 'Isole Pitcairn', '-25.06666666', '-130.1', 'NZD', 'Adamstown', 'Oceania', 175, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(177, 'pe', 'Pérou', 'Peru', 'Peru', 'Perú', 'Peru', 'Perù', '-10', '-76', 'PEN', 'Lima', 'Americas', 176, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(178, 'ph', 'Philippines', 'Philippines', 'Philippinen', 'Filipinas', 'Filipijnen', 'Filippine', '13', '122', 'PHP', 'Manila', 'Asia', 177, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(179, 'pw', 'Palaos', 'Palau', 'Palau', 'Palau', 'Palau', 'Palau', '7.5', '134.5', 'USD', 'Ngerulmud', 'Oceania', 178, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(180, 'pg', 'Papouasie-Nouvelle-Guinée', 'Papua New Guinea', 'Papua-Neuguinea', 'Papúa Nueva Guinea', 'Papoea-Nieuw-Guinea', 'Papua Nuova Guinea', '-6', '147', 'PGK', 'Port Moresby', 'Oceania', 179, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(181, 'pl', 'Pologne', 'Poland', 'Polen', 'Polonia', 'Polen', 'Polonia', '52', '20', 'PLN', 'Warsaw', 'Europe', 180, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(182, 'pr', 'Porto Rico', 'Puerto Rico', 'Puerto Rico', 'Puerto Rico', 'Puerto Rico', 'Porto Rico', '18.25', '-66.5', 'USD', 'San Juan', 'Americas', 181, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(183, 'kp', 'Corée du Nord', 'North Korea', 'Nordkorea', 'Corea del Norte', 'Noord-Korea', 'Corea del Nord', '40', '127', 'KPW', 'Pyongyang', 'Asia', 182, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(184, 'pt', 'Portugal', 'Portugal', 'Portugal', 'Portugal', 'Portugal', 'Portogallo', '39.5', '-8', 'EUR', 'Lisbon', 'Europe', 183, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(185, 'py', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', 'Paraguay', '-23', '-58', 'PYG', 'Asunción', 'Americas', 184, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(186, 'ps', 'Palestine', 'Palestine', 'Palästina', 'Palestina', 'Palestijnse gebieden', 'Palestina', '31.9', '35.2', 'ILS', 'Ramallah', 'Asia', 185, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(187, 'pf', 'Polynésie française', 'French Polynesia', 'Französisch-Polynesien', 'Polinesia Francesa', 'Frans-Polynesië', 'Polinesia Francese', '-15', '-140', 'XPF', 'Papeetē', 'Oceania', 186, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(188, 'qa', 'Qatar', 'Qatar', 'Katar', 'Catar', 'Qatar', 'Qatar', '25.5', '51.25', 'QAR', 'Doha', 'Asia', 187, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(189, 're', 'Réunion', 'Réunion', 'Réunion', 'Reunión', 'Réunion', 'Riunione', '-21.15', '55.5', 'EUR', 'Saint-Denis', 'Africa', 188, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(190, 'ro', 'Roumanie', 'Romania', 'Rumänien', 'Rumania', 'Roemenië', 'Romania', '46', '25', 'RON', 'Bucharest', 'Europe', 189, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(191, 'ru', 'Russie', 'Russia', 'Russland', 'Rusia', 'Rusland', 'Russia', '60', '100', 'RUB', 'Moscow', 'Europe', 190, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(192, 'rw', 'Rwanda', 'Rwanda', 'Ruanda', 'Ruanda', 'Rwanda', 'Ruanda', '-2', '30', 'RWF', 'Kigali', 'Africa', 191, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(193, 'sa', 'Arabie Saoudite', 'Saudi Arabia', 'Saudi-Arabien', 'Arabia Saudí', 'Saoedi-Arabië', 'Arabia Saudita', '25', '45', 'SAR', 'Riyadh', 'Asia', 192, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(194, 'sd', 'Soudan', 'Sudan', 'Sudan', 'Sudán', 'Soedan', 'Sudan', '15', '30', 'SDG', 'Khartoum', 'Africa', 193, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(195, 'sn', 'Sénégal', 'Senegal', 'Senegal', 'Senegal', 'Senegal', 'Senegal', '14', '-14', 'XOF', 'Dakar', 'Africa', 194, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(196, 'sg', 'Singapour', 'Singapore', 'Singapur', 'Singapur', 'Singapore', 'Singapore', '1.36666666', '103.8', 'SGD', 'Singapore', 'Asia', 195, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(197, 'gs', 'Géorgie du Sud-et-les Îles Sandwich du Sud', 'South Georgia', 'Südgeorgien und die Südlichen Sandwichinseln', 'Islas Georgias del Sur y Sandwich del Sur', 'Zuid-Georgia en Zuidelijke Sandwicheilanden', 'Georgia del Sud e Isole Sandwich Meridionali', '-54.5', '-37', 'GBP', 'King Edward Point', 'Americas', 196, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(198, 'sh', 'Sainte-Hélène', 'Saint Helena, Ascension and Tristan da Cunha', 'Sankt Helena', 'Santa Helena', 'Sint-Helena', 'Sant\'Elena', '-15.95', '-5.7', 'SHP', 'Jamestown', 'Africa', 197, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(199, 'sj', 'Svalbard et Jan Mayen', 'Svalbard and Jan Mayen', 'Svalbard und Jan Mayen', 'Islas Svalbard y Jan Mayen', 'Svalbard en Jan Mayen', 'Svalbard e Jan Mayen', '78', '20', 'NOK', 'Longyearbyen', 'Europe', 198, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(200, 'sb', 'Îles Salomon', 'Solomon Islands', 'Salomonen', 'Islas Salomón', 'Salomonseilanden', 'Isole Salomone', '-8', '159', 'SDB', 'Honiara', 'Oceania', 199, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(201, 'sl', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone', '8.5', '-11.5', 'SLL', 'Freetown', 'Africa', 200, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(202, 'sv', 'Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador', 'El Salvador', '13.83333333', '-88.91666666', 'SVC', 'San Salvador', 'Americas', 201, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(203, 'sm', 'Saint-Marin', 'San Marino', 'San Marino', 'San Marino', 'San Marino', 'San Marino', '43.76666666', '12.41666666', 'EUR', 'City of San Marino', 'Europe', 202, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(204, 'so', 'Somalie', 'Somalia', 'Somalia', 'Somalia', 'Somalië', 'Somalia', '10', '49', 'SOS', 'Mogadishu', 'Africa', 203, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(205, 'pm', 'Saint-Pierre-et-Miquelon', 'Saint Pierre and Miquelon', 'Saint-Pierre und Miquelon', 'San Pedro y Miquelón', 'Saint Pierre en Miquelon', 'Saint-Pierre e Miquelon', '46.83333333', '-56.33333333', 'EUR', 'Saint-Pierre', 'Americas', 204, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(206, 'rs', 'Serbie', 'Serbia', 'Serbien', 'Serbia', 'Servië', 'Serbia', '44', '21', 'RSD', 'Belgrade', 'Europe', 205, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(207, 'ss', 'Soudan du Sud', 'South Sudan', 'Südsudan', 'Sudán del Sur', 'Zuid-Soedan', 'Sudan del sud', '7', '30', 'SSP', 'Juba', 'Africa', 206, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(208, 'st', 'Sao Tomé-et-Principe', 'São Tomé and Príncipe', 'São Tomé und Príncipe', 'Santo Tomé y Príncipe', 'Sao Tomé en Principe', 'São Tomé e Príncipe', '1', '7', 'STD', 'São Tomé', 'Africa', 207, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(209, 'sr', 'Surinam', 'Suriname', 'Suriname', 'Surinam', 'Suriname', 'Suriname', '4', '-56', 'SRD', 'Paramaribo', 'Americas', 208, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(210, 'sk', 'Slovaquie', 'Slovakia', 'Slowakei', 'República Eslovaca', 'Slowakije', 'Slovacchia', '48.66666666', '19.5', 'EUR', 'Bratislava', 'Europe', 209, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(211, 'si', 'Slovénie', 'Slovenia', 'Slowenien', 'Eslovenia', 'Slovenië', 'Slovenia', '46.11666666', '14.81666666', 'EUR', 'Ljubljana', 'Europe', 210, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(212, 'se', 'Suède', 'Sweden', 'Schweden', 'Suecia', 'Zweden', 'Svezia', '62', '15', 'SEK', 'Stockholm', 'Europe', 211, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(213, 'sz', 'Swaziland', 'Swaziland', 'Swasiland', 'Suazilandia', 'Swaziland', 'Swaziland', '-26.5', '31.5', 'SZL', 'Lobamba', 'Africa', 212, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(214, 'sx', 'Saint-Martin', 'Sint Maarten', 'Sint Maarten', 'Sint Maarten', 'Sint Maarten', 'Sint Maarten', '18.033333', '-63.05', 'ANG', 'Philipsburg', 'Americas', 213, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(215, 'sc', 'Seychelles', 'Seychelles', 'Seychellen', 'Seychelles', 'Seychellen', 'Seychelles', '-4.58333333', '55.66666666', 'SCR', 'Victoria', 'Africa', 214, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(216, 'sy', 'Syrie', 'Syria', 'Syrien', 'Siria', 'Syrië', 'Siria', '35', '38', 'SYP', 'Damascus', 'Asia', 215, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(217, 'tc', 'Îles Turques-et-Caïques', 'Turks and Caicos Islands', 'Turks- und Caicosinseln', 'Islas Turks y Caicos', 'Turks- en Caicoseilanden', 'Isole Turks e Caicos', '21.75', '-71.58333333', 'USD', 'Cockburn Town', 'Americas', 216, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(218, 'td', 'Tchad', 'Chad', 'Tschad', 'Chad', 'Tsjaad', 'Ciad', '15', '19', 'XAF', 'N\'Djamena', 'Africa', 217, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(219, 'tg', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo', 'Togo', '8', '1.16666666', 'XOF', 'Lomé', 'Africa', 218, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(220, 'th', 'Thaïlande', 'Thailand', 'Thailand', 'Tailandia', 'Thailand', 'Tailandia', '15', '100', 'THB', 'Bangkok', 'Asia', 219, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(221, 'tj', 'Tadjikistan', 'Tajikistan', 'Tadschikistan', 'Tayikistán', 'Tadzjikistan', 'Tagikistan', '39', '71', 'TJS', 'Dushanbe', 'Asia', 220, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(222, 'tk', 'Tokelau', 'Tokelau', 'Tokelau', 'Islas Tokelau', 'Tokelau', 'Isole Tokelau', '-9', '-172', 'NZD', 'Fakaofo', 'Oceania', 221, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(223, 'tm', 'Turkménistan', 'Turkmenistan', 'Turkmenistan', 'Turkmenistán', 'Turkmenistan', 'Turkmenistan', '40', '60', 'TMT', 'Ashgabat', 'Asia', 222, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(224, 'tl', 'Timor oriental', 'Timor-Leste', 'Timor-Leste', 'Timor Oriental', 'Oost-Timor', 'Timor Est', '-8.83333333', '125.91666666', 'USD', 'Dili', 'Asia', 223, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(225, 'to', 'Tonga', 'Tonga', 'Tonga', 'Tonga', 'Tonga', 'Tonga', '-20', '-175', 'TOP', 'Nuku\'alofa', 'Oceania', 224, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(226, 'tt', 'Trinité et Tobago', 'Trinidad and Tobago', 'Trinidad und Tobago', 'Trinidad y Tobago', 'Trinidad en Tobago', 'Trinidad e Tobago', '11', '-61', 'TTD', 'Port of Spain', 'Americas', 225, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(227, 'tn', 'Tunisie', 'Tunisia', 'Tunesien', 'Túnez', 'Tunesië', 'Tunisia', '34', '9', 'TND', 'Tunis', 'Africa', 226, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(228, 'tr', 'Turquie', 'Turkey', 'Türkei', 'Turquía', 'Turkije', 'Turchia', '39', '35', 'TRY', 'Ankara', 'Asia', 227, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(229, 'tv', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', 'Tuvalu', '-8', '178', 'AUD', 'Funafuti', 'Oceania', 228, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(230, 'tw', 'Taïwan', 'Taiwan', 'Taiwan', 'Taiwán', 'Taiwan', 'Taiwan', '23.5', '121', 'TWD', 'Taipei', 'Asia', 229, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(231, 'tz', 'Tanzanie', 'Tanzania', 'Tansania', 'Tanzania', 'Tanzania', 'Tanzania', '-6', '35', 'TZS', 'Dodoma', 'Africa', 230, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(232, 'ug', 'Uganda', 'Uganda', 'Uganda', 'Uganda', 'Oeganda', 'Uganda', '1', '32', 'UGX', 'Kampala', 'Africa', 231, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(233, 'ua', 'Ukraine', 'Ukraine', 'Ukraine', 'Ucrania', 'Oekraïne', 'Ucraina', '49', '32', 'UAH', 'Kiev', 'Europe', 232, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(234, 'um', 'Îles mineures éloignées des États-Unis', 'United States Minor Outlying Islands', 'Kleinere Inselbesitzungen der Vereinigten Staaten', 'Islas Ultramarinas Menores de Estados Unidos', 'Kleine afgelegen eilanden van de Verenigde Staten', 'Isole minori esterne degli Stati Uniti d\'America', '', '', 'USD', '', 'Americas', 233, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(235, 'uy', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', 'Uruguay', '-33', '-56', 'UYI', 'Montevideo', 'Americas', 234, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(236, 'us', 'États-Unis', 'United States', 'Vereinigte Staaten von Amerika', 'Estados Unidos', 'Verenigde Staten', 'Stati Uniti D\'America', '38', '-97', 'USD', 'Washington D.C.', 'Americas', 235, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(237, 'uz', 'Ouzbékistan', 'Uzbekistan', 'Usbekistan', 'Uzbekistán', 'Oezbekistan', 'Uzbekistan', '41', '64', 'UZS', 'Tashkent', 'Asia', 236, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(238, 'va', 'Cité du Vatican', 'Vatican City', 'Vatikanstadt', 'Ciudad del Vaticano', 'Vaticaanstad', 'Città del Vaticano', '41.9', '12.45', 'EUR', 'Vatican City', 'Europe', 237, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(239, 'vc', 'Saint-Vincent-et-les-Grenadines', 'Saint Vincent and the Grenadines', 'Saint Vincent und die Grenadinen', 'San Vicente y Granadinas', 'Saint Vincent en de Grenadines', 'Saint Vincent e Grenadine', '13.25', '-61.2', 'XCD', 'Kingstown', 'Americas', 238, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(240, 've', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', 'Venezuela', '8', '-66', 'VEF', 'Caracas', 'Americas', 239, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(241, 'vg', 'Îles Vierges britanniques', 'British Virgin Islands', 'Britische Jungferninseln', 'Islas Vírgenes del Reino Unido', 'Britse Maagdeneilanden', 'Isole Vergini Britanniche', '18.431383', '-64.62305', 'USD', 'Road Town', 'Americas', 240, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(242, 'vi', 'Îles Vierges des États-Unis', 'United States Virgin Islands', 'Amerikanische Jungferninseln', 'Islas Vírgenes de los Estados Unidos', 'Amerikaanse Maagdeneilanden', 'Isole Vergini americane', '18.35', '-64.933333', 'USD', 'Charlotte Amalie', 'Americas', 241, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(243, 'vn', 'Viêt Nam', 'Vietnam', 'Vietnam', 'Vietnam', 'Vietnam', 'Vietnam', '16.16666666', '107.83333333', 'VND', 'Hanoi', 'Asia', 242, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(244, 'vu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', 'Vanuatu', '-16', '167', 'VUV', 'Port Vila', 'Oceania', 243, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(245, 'wf', 'Wallis-et-Futuna', 'Wallis and Futuna', 'Wallis und Futuna', 'Wallis y Futuna', 'Wallis en Futuna', 'Wallis e Futuna', '-13.3', '-176.2', 'XPF', 'Mata-Utu', 'Oceania', 244, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(246, 'ws', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa', 'Samoa', '-13.58333333', '-172.33333333', 'WST', 'Apia', 'Oceania', 245, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(247, 'ye', 'Yémen', 'Yemen', 'Jemen', 'Yemen', 'Jemen', 'Yemen', '15', '48', 'YER', 'Sana\'a', 'Asia', 246, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(248, 'za', 'Afrique du Sud', 'South Africa', 'Republik Südafrika', 'República de Sudáfrica', 'Zuid-Afrika', 'Sud Africa', '-29', '24', 'ZAR', 'Pretoria', 'Africa', 247, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(249, 'zm', 'Zambie', 'Zambia', 'Sambia', 'Zambia', 'Zambia', 'Zambia', '-15', '30', 'ZMW', 'Lusaka', 'Africa', 248, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00'),
(250, 'zw', 'Zimbabwe', 'Zimbabwe', 'Simbabwe', 'Zimbabue', 'Zimbabwe', 'Zimbabwe', '-20', '30', 'ZWL', 'Harare', 'Africa', 249, 0, 1, '2015-09-01 00:00:00', '2015-09-01 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `elements`
--

CREATE TABLE `elements` (
  `id_element` int(11) NOT NULL,
  `id_template` int(11) DEFAULT NULL,
  `id_bloc` int(11) DEFAULT NULL,
  `id_groupe` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `complement` text NOT NULL COMMENT 'Le complément est utilisé pour stocker les informations supplémentaires nécessaire au bon fonctionnement des élément de type RB, ou Select par exemple',
  `ordre` int(11) NOT NULL,
  `type_element` varchar(50) NOT NULL,
  `is_product` tinyint(1) NOT NULL,
  `is_combinaison` tinyint(1) NOT NULL,
  `is_index` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `elements_groupes`
--

CREATE TABLE `elements_groupes` (
  `id_groupe` int(11) NOT NULL,
  `id_template` int(11) DEFAULT NULL,
  `id_bloc` int(11) DEFAULT NULL,
  `nom` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ordre` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `globals`
--

CREATE TABLE `globals` (
  `id_global` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `lock` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `logs`
--

CREATE TABLE `logs` (
  `id_log` int(11) NOT NULL,
  `id_site` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `action` varchar(255) NOT NULL,
  `details` longtext NOT NULL,
  `complement` longtext NOT NULL,
  `ip` varchar(50) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `logs_404`
--

CREATE TABLE `logs_404` (
  `id_log` int(11) NOT NULL,
  `request` varchar(255) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `mails_filer`
--

CREATE TABLE `mails_filer` (
  `id_filermails` int(11) NOT NULL,
  `id_textemail` int(11) NOT NULL,
  `email_destinataire` varchar(255) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `headers` text NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `mails_text`
--

CREATE TABLE `mails_text` (
  `id_textemail` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `exp_name` varchar(255) NOT NULL,
  `exp_email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `id_nmp` varchar(255) NOT NULL,
  `nmp_unique` varchar(255) NOT NULL,
  `nmp_secure` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `mails_text`
--

INSERT INTO `mails_text` (`id_textemail`, `type`, `id_langue`, `name`, `exp_name`, `exp_email`, `subject`, `content`, `id_nmp`, `nmp_unique`, `nmp_secure`, `added`, `updated`) VALUES
(1, 'admin-identifiants-de-connexion', 'fr', 'Aspartam - Identifiants de connexion', 'Aspartam', 'contact@aspartam.io', '[EMV DYN]site[EMV /DYN] : Vos identifiants de connexion', '<html>\n	<head>\n    	<title>[EMV DYN]site[EMV /DYN] : Vos identifiants de connexion</title>\n  	</head>\n    <body>\n		<h1>Vos identifiants de connexion</h1>\n		<p>\n			Bonjour,<br><br>\n			Voici les codes d\'acc&egrave;s &agrave; votre compte :<br><br>\n			\n			<strong>Identifiant</strong> : [EMV DYN]email[EMV /DYN]<br>\n            <strong>Mot de Passe</strong> : [EMV DYN]password[EMV /DYN]<br><br>\n			\n			Rendez-vous sur la page d\'identification de votre compte :\n			<a href=\'[EMV DYN]url[EMV /DYN]\' target=\'_blank\'>[EMV DYN]url[EMV /DYN]</a>\n		</p>\n  	</body>\n</html>', '', '', '', '2018-06-22 07:22:35', '2018-06-22 07:22:35');

-- --------------------------------------------------------

--
-- Structure de la table `menus`
--

CREATE TABLE `menus` (
  `id_menu` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `menus_content`
--

CREATE TABLE `menus_content` (
  `id` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `complement` varchar(255) NOT NULL,
  `target` varchar(50) NOT NULL,
  `ordre` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `newsletters`
--

CREATE TABLE `newsletters` (
  `id_newsletter` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `id_client` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `origine` varchar(50) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déclencheurs `newsletters`
--
DELIMITER $$
CREATE TRIGGER `lower_allNl` BEFORE INSERT ON `newsletters` FOR EACH ROW SET NEW.email = LOWER(NEW.email)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `nmp`
--

CREATE TABLE `nmp` (
  `id_nmp` int(11) NOT NULL,
  `serialize_content` text NOT NULL,
  `date` date NOT NULL,
  `mailto` varchar(255) NOT NULL,
  `reponse` text NOT NULL,
  `erreur` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `senddate` datetime NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `nmp_desabo`
--

CREATE TABLE `nmp_desabo` (
  `id_desabo` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `id_textemail` int(11) NOT NULL,
  `raison` varchar(255) NOT NULL,
  `commentaire` text NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `redirections`
--

CREATE TABLE `redirections` (
  `id_redirection` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `from_slug` varchar(255) NOT NULL,
  `to_slug` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sets`
--

CREATE TABLE `sets` (
  `id_set` int(11) NOT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `nom` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `lock` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sets`
--

INSERT INTO `sets` (`id_set`, `id_categorie`, `nom`, `slug`, `description`, `type`, `value`, `lock`, `added`, `updated`) VALUES
(1, 2, 'Format Datepicker', 'format-datepicker', 'Format de la date sur les éléments datepicker du site', 'Texte', 'mpyV0KZk3quprQ==', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(2, 3, 'Google Site Verification', 'google-site-verification', 'Code pour valider la propriété du site', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(3, 3, 'Google Analytics', 'google-analytics', 'Code de suivi Universal Analytics', 'Texte', '', 1, '2018-06-22 07:22:35', '2019-10-01 13:50:09'),
(4, 3, 'Alerte Cookie', 'alerte-cookie', 'Directive européenne 2009/136/CE sur les cookies', 'Booléen', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(5, 4, 'NMP Server API', 'nmp-server-api', 'Adresse du serveur API NMP', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(6, 4, 'NMP From Mail', 'nmp-from-mail', 'Adresse email du sender NMP', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(7, 4, 'NMP Key', 'nmp-key', 'Clef de sécurité API NMP', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(8, 4, 'NMP Login', 'nmp-login', 'Identifiant API NMP', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(9, 4, 'NMP Password', 'nmp-password', 'Mot de passe API NMP', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(10, 4, 'NMP Mail', 'nmp-mail', 'Adresse email du compte NMP sur SmartFocus', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(11, 4, 'NMP ID Clonage', 'nmp-id-clonage', 'ID du Template NMP pour le clonage', 'Texte', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(12, 2, 'Format des dates', 'format-des-dates', 'Format de la date sur le site', 'Texte', 'mmfTkpI=', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(13, 4, 'NMP Alias mails', 'nmp-alias-mails', 'Adresse de réception des emails NMP en dev et demo', 'Texte', 'qmbYxLKi1KCUdJmn28/R08RgltTQ', 1, '2018-06-22 07:22:35', '2019-11-22 15:52:55'),
(14, 5, 'Mails Serveur Alias', 'mails-serveur-alias', 'Adresse de réception des emails serveur', 'Texte', 'qmbYxLKi1KCUdJmn28/R08RgltTQ', 1, '2018-06-22 07:22:35', '2019-11-22 15:53:20'),
(17, 1, 'Favicon', 'favicon', 'Favicon utilisé sur le FO et le BO. <br />Doit être un PNG de 64px par 64px !', 'Favicon', '', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35');

-- --------------------------------------------------------

--
-- Structure de la table `sets_categories`
--

CREATE TABLE `sets_categories` (
  `id_categorie` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `lock` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sets_categories`
--

INSERT INTO `sets_categories` (`id_categorie`, `nom`, `lock`, `added`, `updated`) VALUES
(1, 'Générale', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(2, 'Formats', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(3, 'SEO &amp; Mentions', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(4, 'Mails NMP', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(5, 'Mails Serveur', 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35');

-- --------------------------------------------------------

--
-- Structure de la table `se_logs`
--

CREATE TABLE `se_logs` (
  `id_log` int(11) NOT NULL,
  `recherche` varchar(255) NOT NULL,
  `nb_results` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `se_matches`
--

CREATE TABLE `se_matches` (
  `id_match` int(11) NOT NULL,
  `id_word` int(11) NOT NULL,
  `id_tree` int(11) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `se_words`
--

CREATE TABLE `se_words` (
  `id_word` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `word` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sites`
--

CREATE TABLE `sites` (
  `id_site` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `app` varchar(50) DEFAULT NULL,
  `base` varchar(50) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `sites`
--

INSERT INTO `sites` (`id_site`, `nom`, `app`, `base`, `url`, `added`, `updated`) VALUES
(1, 'Kidjo', 'admin', 'kidjo_dev', 'http://kidjo-bo.local.equinoa.net', '2019-10-01 07:22:35', '2019-10-01 07:22:35');

-- --------------------------------------------------------

--
-- Structure de la table `templates`
--

CREATE TABLE `templates` (
  `id_template` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `affichage` tinyint(1) NOT NULL,
  `is_gamme` tinyint(1) NOT NULL,
  `is_mobile` tinyint(1) NOT NULL,
  `is_index` tinyint(1) NOT NULL,
  `class_template` varchar(255) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `templates`
--

INSERT INTO `templates` (`id_template`, `name`, `description`, `slug`, `type`, `status`, `affichage`, `is_gamme`, `is_mobile`, `is_index`, `class_template`, `added`, `updated`) VALUES
(1, 'Home', 'Template de la page d\'accueil du site', 'home', 'Page', 1, 1, 0, 0, 0, '', '2018-06-22 07:22:35', '2018-06-22 07:22:35');

-- --------------------------------------------------------

--
-- Structure de la table `traductions`
--

CREATE TABLE `traductions` (
  `id_traduction` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `section` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `texte` text NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `traductions`
--

INSERT INTO `traductions` (`id_traduction`, `id_langue`, `section`, `nom`, `texte`, `added`, `updated`) VALUES
(1, 'fr', 'admin-generic', 'admin-site', 'Administration du site', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(2, 'fr', 'debug', 'infos', 'Informations', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(3, 'fr', 'debug', 'reqMySQL', 'Requêtes MySQL', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(4, 'fr', 'debug', 'listeSessions', '$_SESSION', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(5, 'fr', 'debug', 'listeCookies', '$_COOKIE', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(6, 'fr', 'debug', 'listeParams', 'Params', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(7, 'fr', 'debug', 'kill-session', 'Détruire la session', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(8, 'fr', 'debug', 'temps-exec', 'Temps d\'exécution', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(9, 'fr', 'debug', 'lab-controller', 'Controller', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(10, 'fr', 'debug', 'lab-vue', 'Vue', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(11, 'fr', 'debug', 'lab-template', 'Template', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(12, 'fr', 'debug', 'lab-ip', 'Adresse IP', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(13, 'fr', 'debug', 'lab-bdd', 'Base de données utilisée', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(14, 'fr', 'debug', 'lab-bdd-commun', 'Base de données globale', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(15, 'fr', 'admin-login', 'titre', 'Administration du site', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(16, 'fr', 'admin-generic', 'backsite', 'Retour au site', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(17, 'fr', 'admin-generic', 'logout', 'Se déconnecter', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(18, 'fr', 'admin-navigation', 'edition', 'Edition', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(19, 'fr', 'admin-navigation', 'site principal', 'Site principal', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(20, 'fr', 'admin-navigation', 'blocs', 'Blocs', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(21, 'fr', 'admin-navigation', 'menus', 'Menus', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(22, 'fr', 'admin-navigation', 'templates', 'Templates', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(23, 'fr', 'admin-navigation', 'redirections', 'Redirections', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(24, 'fr', 'admin-navigation', 'indexer le site', 'Indexer le site', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(25, 'fr', 'admin-navigation', 'utilisateurs', 'Utilisateurs', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(26, 'fr', 'admin-navigation', 'gestion des clients', 'Gestion des clients', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(27, 'fr', 'admin-navigation', 'historique des mails', 'Historique des mails', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(28, 'fr', 'admin-navigation', 'templates des mails', 'Templates des mails', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(29, 'fr', 'admin-navigation', 'paramètres', 'Paramètres', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(30, 'fr', 'admin-navigation', 'paramètres catégories', 'Paramètres catégories', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(31, 'fr', 'admin-navigation', 'paramètres globaux', 'Paramètres globaux', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(32, 'fr', 'admin-navigation', 'paramètres application', 'Paramètres application', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(33, 'fr', 'admin-navigation', 'localisation', 'Localisation', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(34, 'fr', 'admin-navigation', 'traductions', 'Traductions', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(35, 'fr', 'admin-navigation', 'administration', 'Administration', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(36, 'fr', 'admin-navigation', 'sites', 'Sites', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(37, 'fr', 'admin-navigation', 'zones', 'Zones', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(38, 'fr', 'admin-navigation', 'administrateurs', 'Administrateurs', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(39, 'fr', 'admin-navigation', 'logs du bo', 'Logs du BO', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(40, 'fr', 'admin-navigation', 'dashboard', 'Dashboard', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(41, 'fr', 'admin-navigation', 'documents', 'Documents', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(42, 'fr', 'admin-navigation', 'trafic', 'Trafic', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(43, 'fr', 'admin-navigation', 'statistiques', 'Statistiques', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(44, 'fr', 'admin-navigation', 'catégories de requête', 'Catégories de requête', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(45, 'fr', 'admin-navigation', 'liste des requêtes', 'Liste des requêtes', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(46, 'fr', 'admin-navigation', 'equinoa', 'Equinoa', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(47, 'fr', 'admin-navigation', 'db view', 'DB View', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(48, 'fr', 'admin-generic', 'barredebug', 'Afficher le débug', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(49, 'fr', 'admin-edition', 'edition-site', 'Edition du site', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(50, 'fr', 'admin-edition', 'edition', 'Edition', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(51, 'fr', 'admin-edition', 'site-principal', 'Site principal', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(52, 'fr', 'admin-edition', 'add-page', 'Ajouter une page', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(53, 'fr', 'admin-edition', 'edition-site-principal', 'Edition du site principal', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(54, 'fr', 'admin-generic', 'btn-collapseall', 'Replier les dossiers', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(55, 'fr', 'admin-generic', 'btn-expandall', 'Déplier les dossiers', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(56, 'fr', 'admin-edition', 'edition-infos-actions-page', 'Informations et actions sur la page', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(57, 'fr', 'admin-generic', 'btn-edit', 'Modifier', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(58, 'fr', 'admin-generic', 'btn-add-child', 'Ajouter un enfant', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(59, 'fr', 'admin-generic', 'btn-duplicate', 'Dupliquer', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(60, 'fr', 'admin-generic', 'btn-delete', 'Supprimer', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(61, 'fr', 'admin-edition', 'form-quickpage-lab-title', 'Titre de la page', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(62, 'fr', 'admin-edition', 'form-quickpage-lab-menutitle', 'Titre dans le menu', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(63, 'fr', 'admin-edition', 'form-quickpage-lab-slug', 'Lien permanent', '2019-11-22 15:48:12', '2019-11-22 15:48:12'),
(64, 'fr', 'admin-edition', 'form-quickpage-lab-status', 'Statut de la page', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(65, 'fr', 'admin-edition', 'form-quickpage-lab-online', 'En ligne', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(66, 'fr', 'admin-edition', 'form-quickpage-lab-offline', 'Hors ligne', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(67, 'fr', 'admin-generic', 'save-modifs', 'Enregistrer les modifications', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(68, 'fr', 'admin-generic', 'alert-confirm-required', 'Confirmation requise', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(69, 'fr', 'admin-edition', 'delete-page-alert', 'Confirmez vous la suppression de la page et de tous les enfants éventuels ?', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(70, 'fr', 'admin-generic', 'alert-oui', 'Oui', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(71, 'fr', 'admin-generic', 'alert-non', 'Non', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(72, 'fr', 'admin-generic', 'licence', 'Licence', '2019-11-22 15:48:13', '2019-11-22 15:48:13'),
(73, 'fr', 'admin-parametres', 'parametres-categories', 'Paramètres catégories', '2019-11-22 15:52:18', '2019-11-22 15:52:18'),
(74, 'fr', 'admin-parametres', 'parametres', 'Paramètres', '2019-11-22 15:52:18', '2019-11-22 15:52:18'),
(75, 'fr', 'admin-parametres', 'add-categ', 'Ajouter une catégorie', '2019-11-22 15:52:18', '2019-11-22 15:52:18'),
(76, 'fr', 'admin-parametres', 'liste-parametres-categ', 'Liste des catégories de paramètre', '2019-11-22 15:52:18', '2019-11-22 15:52:18'),
(77, 'fr', 'admin-parametres', 'tab-nom-categ', 'Nom de la catégorie', '2019-11-22 15:52:18', '2019-11-22 15:52:18'),
(78, 'fr', 'admin-generic', 'edit', 'Modifier', '2019-11-22 15:52:18', '2019-11-22 15:52:18'),
(79, 'fr', 'admin-parametres', 'parametres-globaux', 'Paramètres globaux', '2019-11-22 15:52:20', '2019-11-22 15:52:20'),
(80, 'fr', 'admin-parametres', 'add-param', 'Ajouter un paramètre', '2019-11-22 15:52:20', '2019-11-22 15:52:20'),
(81, 'fr', 'admin-parametres', 'liste-parametres-type', 'Liste des paramètres de type', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(82, 'fr', 'admin-parametres', 'type-texte', 'Texte', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(83, 'fr', 'admin-parametres', 'tab-nom', 'Nom', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(84, 'fr', 'admin-parametres', 'tab-description', 'Description', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(85, 'fr', 'admin-parametres', 'tab-valeur', 'Valeur', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(86, 'fr', 'admin-generic', 'utilisation', 'Utilisation :', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(87, 'fr', 'admin-parametres', 'type-fichier', 'Fichier', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(88, 'fr', 'admin-generic', 'no-results', 'Il n\'y a pas de données pour le moment', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(89, 'fr', 'admin-parametres', 'type-date', 'Date', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(90, 'fr', 'admin-parametres', 'type-booleen', 'Booléen', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(91, 'fr', 'admin-generic', 'fnc-copied', 'Fonction copiée', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(92, 'fr', 'admin-generic', 'clipboard', 'Presse-papier', '2019-11-22 15:52:21', '2019-11-22 15:52:21'),
(93, 'fr', 'admin-parametres', 'parametres-appli', 'Paramètres application', '2019-11-22 15:52:31', '2019-11-22 15:52:31'),
(94, 'fr', 'admin-parametres', 'liste-param-categ', 'Liste des paramètres de la catégorie', '2019-11-22 15:52:31', '2019-11-22 15:52:31'),
(95, 'fr', 'admin-parametres', 'tab-type', 'Type', '2019-11-22 15:52:31', '2019-11-22 15:52:31'),
(96, 'fr', 'debug', 'listePost', '$_POST', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(97, 'fr', 'admin-parametres', 'edit-param', 'Modifier un paramètre', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(98, 'fr', 'admin-parametres', 'back-parametres', 'Retour aux paramètres', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(99, 'fr', 'admin-parametres', 'form-edit-param', 'Formulaire de modification d\'un paramètre', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(100, 'fr', 'admin-parametres', 'form-param-lab-nom', 'Nom', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(101, 'fr', 'admin-parametres', 'form-param-lab-description', 'Description', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(102, 'fr', 'admin-parametres', 'form-zone-lab-type', 'Type', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(103, 'fr', 'admin-parametres', 'form-choix-type', 'Choisir un type', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(104, 'fr', 'admin-parametres', 'param-lab-value', 'Valeur', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(105, 'fr', 'admin-parametres', 'form-param-lab-categ', 'Catégorie', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(106, 'fr', 'admin-administration', 'form-param-choix-categ', 'Choisir une catégorie', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(107, 'fr', 'admin-parametres', 'form-param-lab-lock', 'Bloquer le paramètre', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(108, 'fr', 'admin-generic', 'cancel', 'Annuler', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(109, 'fr', 'admin-generic', 'save-and-stay', 'Enregistrer et rester', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(110, 'fr', 'admin-generic', 'save', 'Enregistrer', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(111, 'fr', 'admin-parametres', 'delete-param-alert', 'Confirmez vous la suppression du paramètre ?', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(112, 'fr', 'admin-generic', 'no-file', 'Aucun fichier séléctionné', '2019-11-22 15:52:49', '2019-11-22 15:52:49'),
(113, 'fr', 'admin-logs', 'action-parameditapp', 'Modification d\'un paramètre application', '2019-11-22 15:52:55', '2019-11-22 15:52:55'),
(114, 'fr', 'admin-banner', 'param-app', 'Paramètres application', '2019-11-22 15:52:55', '2019-11-22 15:52:55'),
(115, 'fr', 'admin-banner', 'param-edit', 'Le paramètre a bien été modifié', '2019-11-22 15:52:55', '2019-11-22 15:52:55'),
(116, 'fr', 'admin-edition', 'gestion-blocs', 'Gestion des blocs', '2019-11-22 15:54:04', '2019-11-22 15:54:04'),
(117, 'fr', 'admin-edition', 'add-bloc', 'Ajouter un bloc', '2019-11-22 15:54:04', '2019-11-22 15:54:04'),
(118, 'fr', 'admin-edition', 'liste-blocs', 'Liste des blocs', '2019-11-22 15:54:04', '2019-11-22 15:54:04'),
(119, 'fr', 'admin-login', 'logo-uriage', 'Uriage', '2019-11-22 15:55:03', '2019-11-22 15:55:03'),
(120, 'fr', 'admin-login', 'email', 'Adresse email', '2019-11-22 15:55:03', '2019-11-22 15:55:03'),
(121, 'fr', 'admin-login', 'password', 'Mot de passe', '2019-11-22 15:55:03', '2019-11-22 15:55:03'),
(122, 'fr', 'admin-login', 'btn-login', 'Connexion', '2019-11-22 15:55:03', '2019-11-22 15:55:03'),
(123, 'fr', 'admin-login', 'lost-password', 'Mot de passe oublié ?', '2019-11-22 15:55:03', '2019-11-22 15:55:03'),
(124, 'fr', 'admin-edition', 'gestion-menus', 'Gestion des menus', '2019-11-22 15:55:21', '2019-11-22 15:55:21'),
(125, 'fr', 'admin-edition', 'add-menu', 'Ajouter un menu', '2019-11-22 15:55:21', '2019-11-22 15:55:21'),
(126, 'fr', 'admin-edition', 'liste-menus', 'Liste des menus', '2019-11-22 15:55:21', '2019-11-22 15:55:21'),
(127, 'fr', 'admin-edition', 'gestion-templates', 'Gestion des templates', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(128, 'fr', 'admin-edition', 'add-tpl', 'Ajouter un template', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(129, 'fr', 'admin-edition', 'liste-tpl-type', 'Liste des templates de type', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(130, 'fr', 'admin-edition', 'type-tpl-page', 'Page', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(131, 'fr', 'admin-edition', 'tab-nom-tpl', 'Nom', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(132, 'fr', 'admin-edition', 'tab-affichage-tpl', 'Type', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(133, 'fr', 'admin-generic', 'fichier', 'Fichier :', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(134, 'fr', 'admin-edition', 'type-affichage-tpl', 'Affichage', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(135, 'fr', 'admin-generic', 'btn-code', 'Code source', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(136, 'fr', 'admin-generic', 'btn-elements', 'Eléments', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(137, 'fr', 'admin-edition', 'type-tpl-produit', 'Produit', '2019-11-22 15:55:23', '2019-11-22 15:55:23'),
(138, 'fr', 'admin-edition', 'type-tpl-formulaire', 'Formulaire', '2019-11-22 15:55:24', '2019-11-22 15:55:24'),
(139, 'fr', 'admin-edition', 'redirections', 'Redirections', '2019-11-22 15:55:31', '2019-11-22 15:55:31'),
(140, 'fr', 'admin-edition', 'add-redir', 'Ajouter une redirection', '2019-11-22 15:55:31', '2019-11-22 15:55:31'),
(141, 'fr', 'admin-edition', 'liste-redir', 'Liste des redirections', '2019-11-22 15:55:31', '2019-11-22 15:55:31'),
(142, 'fr', 'admin-generic', 'aucune-donnees', 'Il n\'y a pas de données pour le moment', '2019-11-22 15:55:31', '2019-11-22 15:55:31'),
(143, 'fr', 'admin-administration', 'administration-users', 'Gestion des administrateurs', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(144, 'fr', 'admin-administration', 'administration', 'Administration', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(145, 'fr', 'admin-administration', 'users', 'Administrateurs', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(146, 'fr', 'admin-administration', 'add-user', 'Ajouter un administrateur', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(147, 'fr', 'admin-administration', 'liste-users', 'Liste des administrateurs', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(148, 'fr', 'admin-administration', 'filtre-par-site', 'Filtre par site', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(149, 'fr', 'admin-generic', 'recherche', 'Recherche...', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(150, 'fr', 'admin-generic', 'ok', 'OK', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(151, 'fr', 'admin-administration', 'tab-nom', 'Nom', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(152, 'fr', 'admin-administration', 'tab-prenom', 'Prénom', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(153, 'fr', 'admin-administration', 'tab-email', 'Adresse email', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(154, 'fr', 'admin-administration', 'tab-lastlogin', 'Dernière connexion', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(155, 'fr', 'admin-generic', 'agrave', 'à', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(156, 'fr', 'admin-generic', 'desactiver', 'Désactiver', '2019-11-22 16:02:09', '2019-11-22 16:02:09'),
(157, 'fr', 'admin-administration', 'administration-zones', 'Administration des zones', '2019-11-22 16:04:03', '2019-11-22 16:04:03'),
(158, 'fr', 'admin-administration', 'zones', 'Zones', '2019-11-22 16:04:03', '2019-11-22 16:04:03'),
(159, 'fr', 'admin-administration', 'add-zone', 'Ajouter une zone', '2019-11-22 16:04:03', '2019-11-22 16:04:03'),
(160, 'fr', 'admin-administration', 'liste-zones', 'Liste des zones du BO', '2019-11-22 16:04:03', '2019-11-22 16:04:03'),
(161, 'fr', 'admin-generic', 'details', 'Détails', '2019-11-22 16:04:03', '2019-11-22 16:04:03'),
(162, 'fr', 'admin-administration', 'ordre-zones', 'Ordonnancement des zones', '2019-11-22 16:04:04', '2019-11-22 16:04:04'),
(163, 'fr', 'admin-administration', 'expli-ordre-zones', 'Pour trier les zones vous pouvez les déplacer avec votre souris.', '2019-11-22 16:04:04', '2019-11-22 16:04:04'),
(164, 'fr', 'admin-alerte', 'alert-fonctionnalite', 'Votre navigateur ne supporte pas cette fonctionnalité !', '2019-11-22 16:04:04', '2019-11-22 16:04:04'),
(165, 'fr', 'admin-administration', 'edit-zone', 'Modifier une zone', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(166, 'fr', 'admin-administration', 'back-zones', 'Retour aux zones', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(167, 'fr', 'admin-administration', 'delete-zones', 'Supprimer la zone', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(168, 'fr', 'admin-administration', 'form-edit-zone', 'Formulaire de modification de la zone', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(169, 'fr', 'admin-administration', 'form-zone-lab-parent', 'Zone parente', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(170, 'fr', 'admin-administration', 'form-zone-choix-parent', 'Choisir une zone parente', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(171, 'fr', 'admin-administration', 'form-zone-lab-nom', 'Nom de la zone', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(172, 'fr', 'admin-administration', 'form-zone-lab-slug', 'Destination', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(173, 'fr', 'admin-administration', 'form-zone-lab-css', 'Classe CSS picto', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(174, 'fr', 'admin-administration', 'form-zone-lab-recherche', 'Activer la recherche', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(175, 'fr', 'admin-administration', 'delete-zones-alert', 'Confirmez vous la suppression de la zone et de toutes les zones dont elle est le parent ?', '2019-11-22 16:04:07', '2019-11-22 16:04:07'),
(176, 'fr', 'admin-logs', 'backlog-zone', 'Zone', '2019-11-22 16:04:10', '2019-11-22 16:04:10'),
(177, 'fr', 'admin-logs', 'action-deletezones', 'Supression d\'une  zone', '2019-11-22 16:04:10', '2019-11-22 16:04:10'),
(178, 'fr', 'admin-banner', 'gestion-zone', 'Gestion des zones', '2019-11-22 16:04:10', '2019-11-22 16:04:10'),
(179, 'fr', 'admin-banner', 'del-zone', 'La zone a bien été supprimée', '2019-11-22 16:04:10', '2019-11-22 16:04:10'),
(180, 'fr', 'admin-dash', 'etat-cmd-en-cours', 'En cours', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(181, 'fr', 'admin-dash', 'etat-cmd-validee', 'Validée', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(182, 'fr', 'admin-dash', 'etat-cmd-expediee', 'Expédiée', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(183, 'fr', 'admin-dash', 'etat-cmd-annulee', 'Annulée', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(184, 'fr', 'admin-dash', 'etat-logistic-en-cours', 'En cours', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(185, 'fr', 'admin-dash', 'etat-logistic-envoyee', 'Envoyée', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(186, 'fr', 'admin-dash', 'etat-logistic-en-erreur', 'En erreur', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(187, 'fr', 'admin-dash', 'etat-paiement-non-valide', 'Non valide', '2019-11-22 16:04:47', '2019-11-22 16:04:47'),
(188, 'fr', 'admin-dash', 'etat-paiement-valide', 'Valide', '2019-11-22 16:04:47', '2019-11-22 16:04:47');

-- --------------------------------------------------------

--
-- Structure de la table `tree`
--

CREATE TABLE `tree` (
  `id_tree` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `id_canonical` int(11) DEFAULT NULL,
  `id_target` int(11) DEFAULT NULL,
  `id_template` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `filtre` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `menu_title` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `og_title` varchar(255) NOT NULL,
  `og_image` varchar(255) NOT NULL,
  `og_description` text NOT NULL,
  `ordre` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `status_menu` tinyint(1) NOT NULL,
  `prive` tinyint(1) NOT NULL,
  `indexation` tinyint(1) NOT NULL DEFAULT '1',
  `planification` tinyint(1) NOT NULL,
  `planification_status` tinyint(1) NOT NULL,
  `planification_date` datetime NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `tree`
--

INSERT INTO `tree` (`id_tree`, `id_langue`, `id_parent`, `id_canonical`, `id_target`, `id_template`, `id_user`, `filtre`, `title`, `slug`, `menu_title`, `meta_title`, `meta_description`, `meta_keywords`, `og_title`, `og_image`, `og_description`, `ordre`, `status`, `status_menu`, `prive`, `indexation`, `planification`, `planification_status`, `planification_date`, `added`, `updated`) VALUES
(1, 'fr', NULL, 0, 0, NULL, 1, 0, 'Kidjo', '', 'Kidjo', 'Kidjo', 'Kidjo', '', 'Kidjo', 'Kidjo', 'Kidjo', 1, 1, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', '2018-06-22 07:22:35', '2019-10-31 13:51:40');

-- --------------------------------------------------------

--
-- Structure de la table `tree_elements`
--

CREATE TABLE `tree_elements` (
  `id` int(11) NOT NULL,
  `id_tree` int(11) NOT NULL,
  `id_element` int(11) NOT NULL,
  `id_langue` varchar(2) NOT NULL,
  `value` text NOT NULL,
  `complement` text NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_tree` int(11) DEFAULT NULL,
  `debug` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `lastlogin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_user`, `hash`, `firstname`, `name`, `email`, `password`, `id_tree`, `debug`, `status`, `added`, `updated`, `lastlogin`) VALUES
(1, 'eaca73d1621e9cd8de518dd502fc4161', 'Admin', 'Aspartam', 'admin@aspartam.io', '$2y$10$JYjoan8uklDum.m4nPtJRuSt.D8a/kPPdYKEgKMeAydJTAB53Acey', NULL, 1, 1, '2018-06-22 07:22:35', '2019-11-22 15:55:14', '2019-11-22 16:55:14');

-- --------------------------------------------------------

--
-- Structure de la table `users_sites`
--

CREATE TABLE `users_sites` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_site` int(11) DEFAULT NULL,
  `ordre` int(11) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users_sites`
--

INSERT INTO `users_sites` (`id`, `id_user`, `id_site`, `ordre`, `added`, `updated`) VALUES
(2, 1, 1, 1, '2019-10-01 09:24:29', '2019-10-01 09:24:29');

-- --------------------------------------------------------

--
-- Structure de la table `users_zones`
--

CREATE TABLE `users_zones` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_zone` int(11) DEFAULT NULL,
  `ordre` int(11) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users_zones`
--

INSERT INTO `users_zones` (`id`, `id_user`, `id_zone`, `ordre`, `added`, `updated`) VALUES
(1, 1, 1, 1, '2018-06-22 07:22:35', '2019-10-01 09:25:57'),
(2, 1, 2, 4, '2018-06-22 07:22:35', '2019-10-01 09:25:57'),
(4, 1, 4, 5, '2018-06-22 07:22:35', '2019-10-01 09:25:57'),
(5, 1, 5, 6, '2018-06-22 07:22:35', '2019-10-01 09:25:57'),
(8, 1, 8, 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(9, 1, 9, 2, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(10, 1, 10, 3, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(11, 1, 11, 4, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(12, 1, 12, 5, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(13, 1, 13, 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(14, 1, 14, 2, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(15, 1, 15, 3, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(17, 1, 17, 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(19, 1, 19, 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(20, 1, 20, 2, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(21, 1, 21, 3, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(22, 1, 22, 5, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(28, 1, 28, 9, '2018-06-22 07:22:35', '2019-10-01 09:25:57'),
(29, 1, 29, 1, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(38, 1, 38, 17, '2019-10-01 08:40:16', '2019-10-01 08:40:16'),
(39, 1, 39, 18, '2019-10-01 08:40:29', '2019-10-01 08:40:29'),
(40, 1, 40, 19, '2019-10-01 08:40:42', '2019-10-01 08:40:42'),
(41, 1, 41, 7, '2019-10-01 08:42:07', '2019-10-01 09:25:57'),
(44, 1, 44, 8, '2019-10-01 08:43:05', '2019-10-01 09:25:57'),
(45, 1, 45, 24, '2019-10-01 08:43:17', '2019-10-01 08:43:17'),
(46, 1, 46, 25, '2019-10-01 08:43:28', '2019-10-01 08:43:28'),
(47, 1, 37, 3, '2019-10-01 09:24:23', '2019-10-01 09:25:57');

-- --------------------------------------------------------

--
-- Structure de la table `versions`
--

CREATE TABLE `versions` (
  `id_version` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `numero` varchar(50) NOT NULL,
  `changelog` longtext NOT NULL,
  `active` tinyint(4) NOT NULL,
  `copy_nom` varchar(255) NOT NULL,
  `copy_lien` varchar(255) NOT NULL,
  `copy_annee` year(4) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `versions`
--

INSERT INTO `versions` (`id_version`, `nom`, `numero`, `changelog`, `active`, `copy_nom`, `copy_lien`, `copy_annee`, `added`, `updated`) VALUES
(1, 'Aspartam', 'v2.0', '', 1, 'Equinoa', 'https://www.equinoa.com', 2018, '2018-06-22 07:22:35', '2018-06-22 07:22:35');

-- --------------------------------------------------------

--
-- Structure de la table `zones`
--

CREATE TABLE `zones` (
  `id_zone` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `checksecure` varchar(255) NOT NULL,
  `slug_destination` varchar(255) NOT NULL,
  `css` varchar(50) NOT NULL,
  `recherche` tinyint(1) NOT NULL,
  `added` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `zones`
--

INSERT INTO `zones` (`id_zone`, `id_parent`, `name`, `checksecure`, `slug_destination`, `css`, `recherche`, `added`, `updated`) VALUES
(1, NULL, 'Edition', 'edition', 'edition', 'fa-edit', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(2, NULL, 'Paramètres', 'parametres', 'parametres', 'fa-cogs', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(4, NULL, 'Localisation', 'localisation', 'localisation', 'fa-globe', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(5, NULL, 'Administration', 'administration', 'administration', 'fa-star', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(8, 1, 'Site principal', 'editsite', 'edition/site', 'fa-edit', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(9, 1, 'Blocs', 'editblocs', 'edition/blocs', 'fa-edit', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(10, 1, 'Menus', 'editmenus', 'edition/menus', 'fa-edit', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(11, 1, 'Templates', 'edittemplates', 'edition/templates', 'fa-edit', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(12, 1, 'Redirections', 'editredirections', 'edition/redirections', 'fa-edit', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(13, 2, 'Paramètres catégories', 'parametrescategories', 'parametres/categories', 'fa-cogs', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(14, 2, 'Paramètres globaux', 'paramglobaux', 'parametres/globaux', 'fa-cogs', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(15, 2, 'Paramètres application', 'paramapplication', 'parametres/application', 'fa-cogs', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(17, 4, 'Traductions', 'loctraductions', 'localisation/traductions', 'fa-globe', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(19, 5, 'Sites', 'adminsites', 'administration/sites', 'fa-star', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(20, 5, 'Zones', 'adminzones', 'administration/zones', 'fa-star', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(21, 5, 'Administrateurs', 'adminusers', 'administration/users', 'fa-star', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(22, 5, 'Logs du BO', 'admilogs', 'administration/logs', 'fa-star', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(28, NULL, 'Equinoa', 'equinoa', 'equinoa', 'fa-lock', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(29, 28, 'DB View', 'admidb', 'equinoa/db_view', 'fa-database', 0, '2018-06-22 07:22:35', '2018-06-22 07:22:35'),
(37, NULL, 'Utilisateurs', 'utilisateurs', 'clients', 'fa-group', 0, '2019-10-01 08:36:50', '2019-10-01 08:36:50'),
(38, 37, 'Gestion des clients', 'utilgestion-des-clients', 'clients/gestion', 'fa-group', 0, '2019-10-01 08:40:16', '2019-10-01 08:40:16'),
(39, 37, 'Historique des mails', 'mailshistory', 'mails/history', 'fa-group', 0, '2019-10-01 08:40:29', '2019-10-01 08:40:29'),
(40, 37, 'Templates des mails', 'mailstemplates', 'mails/templates', 'fa-group', 0, '2019-10-01 08:40:42', '2019-10-01 08:40:42'),
(41, NULL, 'Dashboard', 'dashboard', 'dashboard', 'fa-th-large', 0, '2019-10-01 08:42:07', '2019-10-01 08:42:07'),
(44, NULL, 'Statistiques', 'statistiques', 'statistiques', 'fa-bar-chart-o', 0, '2019-10-01 08:43:05', '2019-10-01 08:43:05'),
(45, 44, 'Catégories de requête', 'statcategories-de-requete', 'statistiques/catrequete', 'fa-bar-chart-o', 0, '2019-10-01 08:43:17', '2019-10-01 08:43:17'),
(46, 44, 'Liste des requêtes', 'statliste-des-requetes', 'statistiques/requetes', 'fa-bar-chart-o', 0, '2019-10-01 08:43:28', '2019-10-01 08:43:28');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `blocs`
--
ALTER TABLE `blocs`
  ADD PRIMARY KEY (`id_bloc`),
  ADD KEY `slug` (`slug`);

--
-- Index pour la table `blocs_elements`
--
ALTER TABLE `blocs_elements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_bloc` (`id_bloc`,`id_element`,`id_langue`),
  ADD KEY `id_element` (`id_element`),
  ADD KEY `idBloc` (`id_bloc`);

--
-- Index pour la table `contact_form`
--
ALTER TABLE `contact_form`
  ADD PRIMARY KEY (`id_contact`),
  ADD UNIQUE KEY `unikey` (`unikey`);

--
-- Index pour la table `cookies_visites`
--
ALTER TABLE `cookies_visites`
  ADD PRIMARY KEY (`id_cookie`),
  ADD UNIQUE KEY `id_client` (`id_client`,`cookie`),
  ADD KEY `idClient` (`id_client`);

--
-- Index pour la table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id_pays`);

--
-- Index pour la table `elements`
--
ALTER TABLE `elements`
  ADD PRIMARY KEY (`id_element`),
  ADD KEY `id_template` (`id_template`),
  ADD KEY `id_bloc` (`id_bloc`),
  ADD KEY `slug` (`slug`),
  ADD KEY `is_product` (`is_product`),
  ADD KEY `is_combinaison` (`is_combinaison`),
  ADD KEY `is_index` (`is_index`),
  ADD KEY `ordre` (`ordre`),
  ADD KEY `id_groupe` (`id_groupe`);

--
-- Index pour la table `elements_groupes`
--
ALTER TABLE `elements_groupes`
  ADD PRIMARY KEY (`id_groupe`),
  ADD KEY `id_template` (`id_template`),
  ADD KEY `id_bloc` (`id_bloc`);

--
-- Index pour la table `globals`
--
ALTER TABLE `globals`
  ADD PRIMARY KEY (`id_global`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `id_site` (`id_site`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `logs_404`
--
ALTER TABLE `logs_404`
  ADD PRIMARY KEY (`id_log`);

--
-- Index pour la table `mails_filer`
--
ALTER TABLE `mails_filer`
  ADD PRIMARY KEY (`id_filermails`),
  ADD KEY `id_textemail` (`id_textemail`),
  ADD KEY `email_destinataire` (`email_destinataire`),
  ADD KEY `to` (`to`);

--
-- Index pour la table `mails_text`
--
ALTER TABLE `mails_text`
  ADD PRIMARY KEY (`id_textemail`),
  ADD UNIQUE KEY `type` (`type`,`id_langue`);

--
-- Index pour la table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id_menu`),
  ADD KEY `slug` (`slug`);

--
-- Index pour la table `menus_content`
--
ALTER TABLE `menus_content`
  ADD PRIMARY KEY (`id`,`id_langue`),
  ADD UNIQUE KEY `id_langue` (`id_langue`,`id_menu`,`nom`,`value`,`complement`),
  ADD KEY `ordre` (`ordre`),
  ADD KEY `menu-menus_contents` (`id_menu`);

--
-- Index pour la table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id_newsletter`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id_client` (`id_client`),
  ADD KEY `origine` (`origine`),
  ADD KEY `status` (`status`);

--
-- Index pour la table `nmp`
--
ALTER TABLE `nmp`
  ADD PRIMARY KEY (`id_nmp`),
  ADD KEY `status` (`status`);

--
-- Index pour la table `nmp_desabo`
--
ALTER TABLE `nmp_desabo`
  ADD PRIMARY KEY (`id_desabo`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `redirections`
--
ALTER TABLE `redirections`
  ADD PRIMARY KEY (`id_redirection`),
  ADD KEY `from_slug` (`from_slug`),
  ADD KEY `to_slug` (`to_slug`),
  ADD KEY `status` (`status`);

--
-- Index pour la table `sets`
--
ALTER TABLE `sets`
  ADD PRIMARY KEY (`id_set`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `id_categorie` (`id_categorie`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `sets_categories`
--
ALTER TABLE `sets_categories`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `se_logs`
--
ALTER TABLE `se_logs`
  ADD PRIMARY KEY (`id_log`),
  ADD KEY `ip` (`ip`);

--
-- Index pour la table `se_matches`
--
ALTER TABLE `se_matches`
  ADD PRIMARY KEY (`id_match`),
  ADD UNIQUE KEY `id_word` (`id_word`,`id_tree`);

--
-- Index pour la table `se_words`
--
ALTER TABLE `se_words`
  ADD PRIMARY KEY (`id_word`),
  ADD UNIQUE KEY `id_langue` (`id_langue`,`word`);

--
-- Index pour la table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id_site`),
  ADD UNIQUE KEY `base` (`base`),
  ADD UNIQUE KEY `app` (`app`);

--
-- Index pour la table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id_template`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `type` (`type`),
  ADD KEY `is_gamme` (`is_gamme`),
  ADD KEY `is_mobile` (`is_mobile`),
  ADD KEY `is_index` (`is_index`);

--
-- Index pour la table `traductions`
--
ALTER TABLE `traductions`
  ADD PRIMARY KEY (`id_traduction`),
  ADD UNIQUE KEY `id_langue` (`id_langue`,`section`,`nom`),
  ADD KEY `section` (`section`),
  ADD KEY `nom` (`nom`);

--
-- Index pour la table `tree`
--
ALTER TABLE `tree`
  ADD PRIMARY KEY (`id_tree`,`id_langue`),
  ADD KEY `id_parent` (`id_parent`),
  ADD KEY `id_template` (`id_template`),
  ADD KEY `id_tree` (`id_tree`),
  ADD KEY `slug` (`slug`),
  ADD KEY `id_langue` (`id_langue`),
  ADD KEY `idLangue` (`id_langue`,`slug`),
  ADD KEY `parent-childs` (`id_parent`,`id_langue`);

--
-- Index pour la table `tree_elements`
--
ALTER TABLE `tree_elements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_tree` (`id_tree`,`id_element`,`id_langue`),
  ADD KEY `id_element` (`id_element`),
  ADD KEY `idTree` (`id_tree`),
  ADD KEY `tree-tree_elements` (`id_tree`,`id_langue`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `hash` (`hash`),
  ADD KEY `id_tree` (`id_tree`);

--
-- Index pour la table `users_sites`
--
ALTER TABLE `users_sites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_user` (`id_user`,`id_site`),
  ADD KEY `id_site` (`id_site`);

--
-- Index pour la table `users_zones`
--
ALTER TABLE `users_zones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_user` (`id_user`,`id_zone`),
  ADD KEY `id_zone` (`id_zone`);

--
-- Index pour la table `versions`
--
ALTER TABLE `versions`
  ADD PRIMARY KEY (`id_version`);

--
-- Index pour la table `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id_zone`),
  ADD UNIQUE KEY `checksecure` (`checksecure`),
  ADD KEY `recherche` (`recherche`),
  ADD KEY `id_parent` (`id_parent`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `blocs`
--
ALTER TABLE `blocs`
  MODIFY `id_bloc` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `blocs_elements`
--
ALTER TABLE `blocs_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `contact_form`
--
ALTER TABLE `contact_form`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `cookies_visites`
--
ALTER TABLE `cookies_visites`
  MODIFY `id_cookie` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `countries`
--
ALTER TABLE `countries`
  MODIFY `id_pays` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT pour la table `elements`
--
ALTER TABLE `elements`
  MODIFY `id_element` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `elements_groupes`
--
ALTER TABLE `elements_groupes`
  MODIFY `id_groupe` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `globals`
--
ALTER TABLE `globals`
  MODIFY `id_global` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `logs`
--
ALTER TABLE `logs`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `logs_404`
--
ALTER TABLE `logs_404`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `mails_filer`
--
ALTER TABLE `mails_filer`
  MODIFY `id_filermails` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `mails_text`
--
ALTER TABLE `mails_text`
  MODIFY `id_textemail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `menus`
--
ALTER TABLE `menus`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id_newsletter` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `nmp`
--
ALTER TABLE `nmp`
  MODIFY `id_nmp` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `nmp_desabo`
--
ALTER TABLE `nmp_desabo`
  MODIFY `id_desabo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `redirections`
--
ALTER TABLE `redirections`
  MODIFY `id_redirection` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sets`
--
ALTER TABLE `sets`
  MODIFY `id_set` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `sets_categories`
--
ALTER TABLE `sets_categories`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `se_logs`
--
ALTER TABLE `se_logs`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `se_matches`
--
ALTER TABLE `se_matches`
  MODIFY `id_match` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `se_words`
--
ALTER TABLE `se_words`
  MODIFY `id_word` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `sites`
--
ALTER TABLE `sites`
  MODIFY `id_site` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `templates`
--
ALTER TABLE `templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `traductions`
--
ALTER TABLE `traductions`
  MODIFY `id_traduction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT pour la table `tree_elements`
--
ALTER TABLE `tree_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `users_sites`
--
ALTER TABLE `users_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `users_zones`
--
ALTER TABLE `users_zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=455;

--
-- AUTO_INCREMENT pour la table `versions`
--
ALTER TABLE `versions`
  MODIFY `id_version` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `zones`
--
ALTER TABLE `zones`
  MODIFY `id_zone` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `blocs_elements`
--
ALTER TABLE `blocs_elements`
  ADD CONSTRAINT `bloc-blocs_elements` FOREIGN KEY (`id_bloc`) REFERENCES `blocs` (`id_bloc`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `element-blocs_elements` FOREIGN KEY (`id_element`) REFERENCES `elements` (`id_element`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `bloc-elements` FOREIGN KEY (`id_bloc`) REFERENCES `blocs` (`id_bloc`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `elements_groupe-elements` FOREIGN KEY (`id_groupe`) REFERENCES `elements_groupes` (`id_groupe`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `template-elements` FOREIGN KEY (`id_template`) REFERENCES `templates` (`id_template`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `elements_groupes`
--
ALTER TABLE `elements_groupes`
  ADD CONSTRAINT `bloc-elements_groupes` FOREIGN KEY (`id_bloc`) REFERENCES `blocs` (`id_bloc`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `template-elements_groupes` FOREIGN KEY (`id_template`) REFERENCES `templates` (`id_template`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `sites-logs` FOREIGN KEY (`id_site`) REFERENCES `sites` (`id_site`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `users-logs` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `menus_content`
--
ALTER TABLE `menus_content`
  ADD CONSTRAINT `menu-menus_contents` FOREIGN KEY (`id_menu`) REFERENCES `menus` (`id_menu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sets`
--
ALTER TABLE `sets`
  ADD CONSTRAINT `sets_categories-sets` FOREIGN KEY (`id_categorie`) REFERENCES `sets_categories` (`id_categorie`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `tree`
--
ALTER TABLE `tree`
  ADD CONSTRAINT `parent-childs` FOREIGN KEY (`id_parent`,`id_langue`) REFERENCES `tree` (`id_tree`, `id_langue`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `template-trees` FOREIGN KEY (`id_template`) REFERENCES `templates` (`id_template`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `tree_elements`
--
ALTER TABLE `tree_elements`
  ADD CONSTRAINT `element-tree_elements` FOREIGN KEY (`id_element`) REFERENCES `elements` (`id_element`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tree-tree_elements` FOREIGN KEY (`id_tree`,`id_langue`) REFERENCES `tree` (`id_tree`, `id_langue`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `users_sites`
--
ALTER TABLE `users_sites`
  ADD CONSTRAINT `sites-users_sites` FOREIGN KEY (`id_site`) REFERENCES `sites` (`id_site`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users-users_sites` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Contraintes pour la table `users_zones`
--
ALTER TABLE `users_zones`
  ADD CONSTRAINT `users-users_zones` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `zones-users_zones` FOREIGN KEY (`id_zone`) REFERENCES `zones` (`id_zone`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `zones`
--
ALTER TABLE `zones`
  ADD CONSTRAINT `parent-enfants` FOREIGN KEY (`id_parent`) REFERENCES `zones` (`id_zone`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
