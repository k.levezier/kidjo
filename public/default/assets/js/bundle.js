;(function(window, document, $) {
	var $win = $(window);
	var $doc = $(document);

	$doc.ready(function(){
		// Dropdown Logic
		const $content = $('.content');
		const $menu_content = $('.menu .menu__content');
		const $nav_dropdown_items = $('.nav .has-dropdown');

		$('.nav > ul > .has-dropdown > a').on('click', function(event) {
			event.preventDefault();

			const $this = $(this);

			$this.addClass('active');
			$this.parents('.has-dropdown').addClass('active');

			$this.parents('.has-dropdown').siblings()
				.removeClass('active')
				.find('> a').removeClass('active');

			$menu_content.addClass('menu__content--active');
			$content.addClass('content--small');
		});

		// Inner Dropdown Menu
		$('.menu .menu__dropdown .has-dropdown > a').on('click', function(event) {
			event.preventDefault();

			const $parent = $(this).parent();


			if ( $parent.hasClass('active') ) {

				$parent.removeClass('active');
				$parent.find('ul').stop().slideUp(300);

			} else {
				$parent
					.addClass('active')
					.siblings().removeClass('active');

				$parent.siblings().find('ul').stop().slideUp(300);
				$parent.find('ul').stop().slideDown(300);
			}
		});

		// Close Navigation
		$('.nav-close').on('click', function(event){
			event.preventDefault();

			$nav_dropdown_items.removeClass('active');
			$menu_content.removeClass('menu__content--active');
			$content.removeClass('content--small');

			$('.menu .menu__dropdown .has-dropdown')
				.removeClass('active')
				.find('ul').slideUp(0);

			$nav_dropdown_items.find('> a').removeClass('active');
		});

		// Magnific Popup
		$('.open-popup-link').magnificPopup({
			type: 'inline',
			midClick: true
		});

		// Pretty Dropdowns
		$('.pretty').prettyDropdown({
			width: 113,
			height: 44,
			classic: true,
			selectedMarker: ''
		});

		// Add Tag From Dropdown List
		const $tag_list = $('.list-tags');

		$('.prettydropdown').on('click', 'li:not(.disabled)', function() {
			const $this = $(this);

			$this.removeClass('selected');
			$this.closest('ul').find('li').first().addClass('selected');

			/**** Gestion de la récupération des tags dans les input ****/
			$("#erreur-profiles").slideUp();
			// On récupère le value
			var type = $(this).data('value').split('_')[0];
			var value = $(this).data('value').split('_')[1];

			// On regarde si c'est une zone ou un profil
			var inputVal = "";
			if(type == "profiles")			inputVal = $("#liste-profiles").val();
			else if(type == "geozones")		inputVal = $("#liste-geozones").val();


			if(inputVal!="")	var liste = $.parseJSON(inputVal);
			else				var liste = [];

			if(liste.indexOf(value) === -1){
				liste.push(value);

				if(type == "profiles")			$("#liste-profiles").val(JSON.stringify(liste));
				else if(type == "geozones")		$("#liste-geozones").val(JSON.stringify(liste));

				/**** FIN Gestion de la récupération des tags dans les input ****/

				const tag_value = $this.text();

				let new_tag = $('<li class="js-tag"></li>');
				let new_tag_content = $('<p></p>');

				new_tag_content.text(tag_value);
				new_tag_content.append('<a href="#" data-value="'+$(this).data('value')+'" class="js-tag-remove"><i class="ico-close"></i></a>');

				new_tag.append(new_tag_content);

				$tag_list.append(new_tag);
			}

		});

		// Tag Remove
		$tag_list.on('click', '.js-tag-remove', function(event) {
			event.preventDefault();

			const $tag = $(this).closest('.js-tag');

			/**** Gestion de la récupération des tags dans les input ****/
			// On récupère le value
			var type = $(this).data('value').split('_')[0];
			var value = $(this).data('value').split('_')[1];

			// On regarde si c'est une zone ou un profil
			var inputVal = "";
			if(type == "profiles")			inputVal = $("#liste-profiles").val();
			else if(type == "geozones")	inputVal = $("#liste-geozones").val();

			if(inputVal!="")	var liste = $.parseJSON(inputVal);
			else				var liste = [];

			var index = liste.indexOf(value);
			if(index > -1) {
				var removed = liste.splice(index,1);
				if(type == "profiles")		$("#liste-profiles").val(JSON.stringify(liste));
				else if(type == "geozones")	$("#liste-geozones").val(JSON.stringify(liste));
			}

			/**** FIN Gestion de la récupération des tags dans les input ****/

			$tag.fadeOut(300, function() {
				$tag.remove();
			});
		});

		$("#form-to-share").submit(function(){
			if( $("#liste-profiles").val() == "" || $("#liste-profiles").val() == "[]"){
				$("#erreur-profiles").slideDown();
				return false;
			}
		})

		// Nav Trigger
		$('.nav-trigger').on('click', function(){
			$(this).toggleClass('nav-trigger--open');
			$('.menu').toggleClass('menu--is-open');
			$('body').toggleClass('body--no-scroll');
		});



		var waiting = "";
		$('.open-popup-link').on('click', function(){
			// On charge le contenu de la popup
			if($(this).data('url')!=undefined) {
				elem = $(this);
				$(elem.attr('href')).html(waiting);
				$.ajax({
					url: $(this).data('url'),
					method: "POST",
					success: function (data) {
						$(elem.attr('href')).html(data);
						action_cart();
					},
					error: function (resultat, statut, erreur) {
						console.log("erreur " + erreur);
					}
				});
			}
		});

		if($("#show-popup-document").length > 0){ // Si on veut afficher une popup au chargement de la page
			$("#show-popup-document").click();
		}

		action_cart();

		function action_cart(){
			console.log("Load Action Cart");
			$('.add-cart').on('click', function(){
				btn = $(this);
				$.ajax({
					url: $(this).data('action'),
					method: "POST",
					dataType: "json", data: { id_document: $(this).data('document') },
					success: function( data ) { console.log(data); $(".add-cart-doc-"+btn.data('document')).addClass('active'); $('#nbr_documents').html(data.nbr_doc);},
					error : function(resultat, statut, erreur){ console.log("erreur "+erreur); }
				});
			});

			$('.remove-cart').on('click', function(){
				btn = $(this);
				$.ajax({
					url: $(this).data('action'),
					method: "POST",
					dataType: "json", data: { id_document: $(this).data('document') },
					success: function( data ) { console.log(data); $('#document_'+btn.data('document')).slideUp(); $('#nbr_documents').html(data.nbr_doc); },
					error : function(resultat, statut, erreur){ console.log("erreur "+erreur); }
				});
			});
		}
	});
})(window, document, window.jQuery);
