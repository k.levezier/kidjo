/*
 * Version : 1.0
 * Author : Equinoa Digital Agency
 */
function barreDebug() {
    if ($("#debugSummary").css('display') === 'none') {
        $("#debugSummary").show();
        $("#debugWindow").hide();
        $("#debugWindow section").each(function () {
            $(this).hide();
        });
    } else {
        $("#debugSummary").hide();
        $("#debugWindow").hide();
        $("#debugWindow section").each(function () {
            $(this).hide();
        });
    }
}

function barreDebugDetails(elmt) {
    if ($("#debugWindow").css('display') === 'none') {
        $("#debugWindow").show();
        $("#debugWindow section." + elmt).show();
    } else if ($("#debugWindow section." + elmt).css('display') === 'none') {
        $("#debugWindow section").each(function () {
            $(this).hide();
        });

        $("#debugWindow section." + elmt).show();
    } else {
        $("#debugWindow").hide();
        $("#debugWindow section").each(function () {
            $(this).hide();
        });
    }
}

var isCtrl = false;

$(document).keyup(function (e) {
    if (e.which === 18)
        isCtrl = false;
}).keydown(function (e) {
    if (e.which === 18)
        isCtrl = true;
    if (e.which === 68 && isCtrl === true) {
        barreDebug();

        return false;
    }
});