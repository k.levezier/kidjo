//***************************//
// *** FICHIER AJAX V2 ***//
//***************************//

function no_cache() {
    date_object = new Date();
    var param = date_object.getTime();

    return param;
}

function AjaxObject() {
    if (window.XMLHttpRequest) {
        xhr_object = new XMLHttpRequest();
        return xhr_object;
    }
    else if (window.ActiveXObject) {
        xhr_object = new ActiveXObject('Microsoft.XMLHTTP');
        return xhr_object;
    }
    else {
        alert('Votre navigateur ne supporte pas les objets XMLHTTPRequest...');
        return;
    }
}

/* Fonction AJAX chargement des langues d'un folder */
function loadLanguageFolder(id_folder)
{
    if(id_folder != "")
    {
        xhr_object = AjaxObject();
        var param = no_cache();

        xhr_object.onreadystatechange = function()
        {
            if (xhr_object.readyState != 4)
            {
                //document.getElementById('ajaxContent').innerHTML = '<img src="' + add_surl + '/images/admin/ajax-loader.gif">';
            }
            if(xhr_object.readyState == 4 && xhr_object.status == 200)
            {
                var reponse = xhr_object.responseText;
                document.getElementById('bloc_language').innerHTML = reponse;
            }
        }
        xhr_object.open('GET',addLURL + '/ajax/loadLanguageFolder/' + id_folder ,true);
        xhr_object.send(null);
    }
    else
    {
        document.getElementById('bloc_language').innerHTML = '';
    }
}