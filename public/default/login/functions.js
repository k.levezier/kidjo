;(function($, window, document, undefined) {
	var $win = $(window);
	var $doc = $(document);

	$doc.ready(function() {
		$win.on('load', function() {
			// Init Slider Home
			if( $('.slider').length ) {
				$('.slider-home .owl-carousel').owlCarousel({
					items: 1,
					loop: true,
					autoplay: true,
					smartSpeed: 500,
                                        dots: true,
				});
			}

			$('.nav-primary a').each(function() {
				if( $(this).siblings('.nav-dropdown').length ) {
					$(this).parent().addClass('has-dropdown');
				}
			});
		});

		$win.on('load resize', function() {
			$('.sidebar .accordion').css({
				'max-height': function() {
					return $win.height() - $('.header').outerHeight() - $('.sidebar').outerHeight();
				}
			});

			$('.has-dropdown > a').on('click', function(event) {
				event.preventDefault();

				$(this).parent().toggleClass('active').siblings().removeClass('active');
			});
		});

		// Init Custom Select
		if( $('.select').length ) {
			$('.select').dropdown();

			$('.fs-dropdown-selected').on('click', function() {
				$(this).parent().removeClass('fs-dropdown-bottom');
			});
		}
                
                if( $('.multiselect').length ) {
			$('.multiselect').chosen({
				disable_search: true,
				width: '32.5%'
			});
		}
                
		// Change logo in sidebar
		$('.select-site').on('change', function() {
			var newSrc = $(this).val();

			document.location = '/'+newSrc;
		});

		// Accordion Functionality
		$('.accordion-head').on('click', function() {
			$(this).parent().toggleClass('active').siblings().removeClass('active').find('.accordion-body').slideUp(400);
			$(this).next('.accordion-body').slideToggle(400);
		});

		// Toggle Nav Lang
		$('.btn-lang').on('click', function(event) {
			event.stopPropagation();

			$(this).next('ul').toggleClass('active');
		});

		$('.nav-lang ul').on('click', function(event) {
			event.stopPropagation();
		});


		// Close Elements
		$doc.on('click', function() {
			$('.nav-lang ul').removeClass('active');
		});

		// Init Liste Details Popup
		if( $('.link-view').length ) {
			$('.link-view').magnificPopup({
				type: 'ajax',
                                modal:true,
				removalDelay: 400,
				mainClass: 'mfp-fade'
			});
		}

		// Toggle Nav
		$('.btn-nav').on('click', function() {
			$(this).toggleClass('active');
			$('.sidebar .accordion').toggleClass('active');
		});
	});
})(jQuery, window, document);
