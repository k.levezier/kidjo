/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.editorConfig = function( config ) {
    config.toolbar_Full = [
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'Undo', 'Redo', 'RemoveFormat' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        '/',
        { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        { name: 'document',    groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] }
    ];
    
    config.toolbar = "Full";
    config.format_tags = 'p;h1;h2;h3;div';
    config.filebrowserBrowseUrl = addURL + '/editeur/filemanager/dialog.php?type=2&editor=ckeditor&fldr=&lang=' + lngSite + '_' + lngSite.toUpperCase();
    config.filebrowserUploadUrl = addURL + '/editeur/filemanager/dialog.php?type=2&editor=ckeditor&fldr=&lang=' + lngSite + '_' + lngSite.toUpperCase();
    config.filebrowserImageBrowseUrl = addURL + '/editeur/filemanager/dialog.php?type=1&editor=ckeditor&fldr=&lang=' + lngSite + '_' + lngSite.toUpperCase();
    config.language = lngSite;
};