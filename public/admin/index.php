<?php

ini_set('default_charset', 'utf-8');
session_start();

error_reporting(999);
ini_set('display_errors', TRUE);

// Déclaration de l'application
$app = 'admin';

// Chargement du fichier prepend
include('../../core/before.php');

// Chargement du fichier de config
require_once('../../config.php');

// Chargement du core
require_once('../../core/includes.php');

if($config['env']=='demo' && $_SERVER['REMOTE_ADDR']!= "91.151.70.8")
    header('location:'.$config['url']['prod']['admin']);

// Chargement de l'objet de gestion des erreurs
if ($config['error_handler'][$config['env']]['activate']) {
    require_once('../../core/errorhandler.class.php');
    $handler = new ErrorHandler($config);
}

// Chargement de l'objet dispatcher
$dispatcher = new Dispatcher($config, $app);

// Chargement du fichier append
include('../../core/after.php');
