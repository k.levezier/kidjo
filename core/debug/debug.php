<?php
// Nettoyage tableau de session
$tabSession = $_SESSION;
unset($tabSession['mysql_error']);
unset($tabSession['mysql_debug']);
unset($tabSession['mysql_panic']);
unset($tabSession['setDebug']);

// Récupération du nombre d'erreurs
$this->nbErreurs = 0;
if(!empty($_SESSION['mysql_error']) && is_array($_SESSION['mysql_error']))
    $this->nbErreurs += count($_SESSION['mysql_error']);
if(!empty($_SESSION['mysql_panic']) && is_array($_SESSION['mysql_panic']))
    $this->nbErreurs += count($_SESSION['mysql_error']);
?>
<link media="all" href="<?= $this->surl ?>/styles/debug/debug.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="<?= $this->surl ?>/scripts/debug/debug.js"></script>
<div id="debugSummary">
    <button class="btn btn-primary floatBTNleft" onclick="barreDebugDetails('infosGlobale');"><?= $this->ln->txt('debug', 'infos', $this->language, 'Informations') ?></button>
    <?php if (isset($_SESSION['mysql_debug']) && count($_SESSION['mysql_debug']) > 0) { ?>
        <button class="btn btn-success floatBTNleft" onclick="barreDebugDetails('reqMySQL');"><?= count($_SESSION['mysql_debug']) ?> <?= $this->ln->txt('debug', 'reqMySQL', $this->language, 'Requêtes MySQL') ?></button>
    <?php } ?>
    <?php if (isset($tabSession) && count($tabSession) > 0) { ?>
        <button class="btn btn-warning floatBTNleft" onclick="barreDebugDetails('listeSessions');"><?= count($tabSession) ?> <?= $this->ln->txt('debug', 'listeSessions', $this->language, '$_SESSION') ?></button>
    <?php } ?>
    <?php if (isset($_COOKIE) && count($_COOKIE) > 0) { ?>
        <button class="btn btn-warning floatBTNleft" onclick="barreDebugDetails('listeCookies');"><?= count($_COOKIE) ?> <?= $this->ln->txt('debug', 'listeCookies', $this->language, '$_COOKIE') ?></button>
    <?php } ?>
    <?php if (isset($this->params) && count($this->params) > 0) { ?>
        <button class="btn btn-success floatBTNleft" onclick="barreDebugDetails('listeParams');"><?= count($this->params) ?> <?= $this->ln->txt('debug', 'listeParams', $this->language, 'Params') ?></button>
    <?php } ?>
    <?php if (isset($_SESSION['setDebug']) && count($_SESSION['setDebug']) > 0) { ?>
        <button class="btn btn-warning floatBTNleft" onclick="barreDebugDetails('listeDebugs');"><?= count($_SESSION['setDebug']) ?> <?= $this->ln->txt('debug', 'listeDebugs', $this->language, 'setDebug') ?></button>
    <?php } ?>
    <?php if (isset($_SESSION['mysql_error']) && count($_SESSION['mysql_error']) > 0) { ?>
        <button class="btn btn-danger floatBTNleft" onclick="barreDebugDetails('listeErrors');"><?= count($_SESSION['mysql_error']) ?> <?= $this->ln->txt('debug', 'listeErrors', $this->language, 'MySQL Erreurs') ?></button>
    <?php } ?>
    <?php if (isset($_SESSION['mysql_panic']) && count($_SESSION['mysql_panic']) > 0) { ?>
        <button class="btn btn-danger floatBTNleft" onclick="barreDebugDetails('listePanics');"><?= count($_SESSION['mysql_panic']) ?> <?= $this->ln->txt('debug', 'listePanics', $this->language, 'MySQL Panics') ?></button>
    <?php } ?>
    <?php if (isset($_POST) && count($_POST) > 0) { ?>
        <button class="btn btn-warning floatBTNleft" onclick="barreDebugDetails('listePost');"><?= count($_POST) ?> <?= $this->ln->txt('debug', 'listePost', $this->language, '$_POST') ?></button>
    <?php } ?>
    <?php if (isset($_GET) && count($_GET) > 0) { ?>
        <button class="btn btn-warning floatBTNleft" onclick="barreDebugDetails('listeGet');"><?= count($_GET) ?> <?= $this->ln->txt('debug', 'listeGet', $this->language, '$_GET') ?></button>
    <?php } ?>
    <form name="formKillSessions" method="POST">
        <input type="hidden" name="killSessions" value="destructSessions">
        <button class="btn btn-danger floatBTNright" type="submit"><?= $this->ln->txt('debug', 'kill-session', $this->language, 'Détruire la session') ?></button>
    </form>
</div>
<div id="debugWindow">
    <section class="reqMySQL">
        <div class="col-lg-12">
            <?php
            if (isset($_SESSION['mysql_debug']) && count($_SESSION['mysql_debug']) > 0) {
                foreach ($_SESSION['mysql_debug'] as $req) {
                    echo '
                    <div class="alert alert-info">
                        ' . $this->ln->txt('debug', 'temps-exec', $this->language, 'Temps d\'exécution') . ' : <strong>' . $req['time'] . '</strong> sec.
                        </br>
                        ' . $req['requete'] . '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listePanics">
        <div class="col-lg-12">
            <?php
            if (isset($_SESSION['mysql_panic']) && count($_SESSION['mysql_panic']) > 0) {
                foreach ($_SESSION['mysql_panic'] as $req) {
                    echo '
                    <div class="alert alert-danger">
                        ' . $this->ln->txt('debug', 'temps-exec', $this->language, 'Temps d\'exécution') . ' : <strong>' . $req['time'] . '</strong> sec.
                        </br>
                        ' . $req['requete'] . '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listeErrors">
        <div class="col-lg-12">
            <?php
            if (isset($_SESSION['mysql_error']) && count($_SESSION['mysql_error']) > 0) {
                foreach ($_SESSION['mysql_error'] as $elem) {
                    echo '
                    <div class="alert alert-danger">
                        ' . $elem . '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listeSessions">
        <div class="col-lg-12">
            <?php
            if (isset($tabSession) && count($tabSession) > 0) {
                foreach ($tabSession as $key => $elem) {
                    echo '
                    <div class="alert alert-warning">
                        <strong>' . $key . '</strong>
                        </br>';

                    if (is_array($elem)) {
                        foreach ($elem as $ind => $val) {
                            echo '[' . $ind . '] => ' . $val . '</br>';
                        }
                    } else {
                        echo $elem;
                    }

                    echo '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listeCookies">
        <div class="col-lg-12">
            <?php
            if (isset($_COOKIE) && count($_COOKIE) > 0) {
                foreach ($_COOKIE as $key => $elem) {
                    echo '
                    <div class="alert alert-warning">
                        <strong>' . $key . '</strong>
                        </br>';

                    if (is_array($elem)) {
                        foreach ($elem as $ind => $val) {
                            echo '[' . $ind . '] => ' . $val . '</br>';
                        }
                    } else {
                        echo $elem;
                    }

                    echo '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listeParams">
        <div class="col-lg-12">
            <?php
            if (isset($this->params) && count($this->params) > 0) {
                foreach ($this->params as $key => $elem) {
                    echo '
                    <div class="alert alert-info">
                        $this->params[\'' . $key . '\'] = ' . $elem . '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listePost">
        <div class="col-lg-12">
            <?php
            if (isset($_POST) && count($_POST) > 0) {
                foreach ($_POST as $key => $elem) {
                    echo '
                    <div class="alert alert-warning">';

                    if (is_array($elem)) {
                        echo '
                                <strong>$_POST[\'' . $key . '\']</strong>
                                </br>';

                        foreach ($elem as $ind => $val) {
                            echo '[' . $ind . '] => ' . $val . '</br>';
                        }
                    } else {
                        echo '<strong>$_POST[\'' . $key . '\']</strong> = ' . $elem;
                    }

                    echo '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listeGet">
        <div class="col-lg-12">
            <?php
            if (isset($_GET) && count($_GET) > 0) {
                foreach ($_GET as $key => $elem) {
                    echo '
                    <div class="alert alert-warning">';

                    if (is_array($elem)) {
                        echo '
                        <strong>$_GET[\'' . $key . '\']</strong>
                        </br>';

                        foreach ($elem as $ind => $val) {
                            echo '[' . $ind . '] => ' . $val . '</br>';
                        }
                    } else {
                        echo '<strong>$_GET[\'' . $key . '\']</strong> = ' . $elem;
                    }

                    echo '
                    </div>';
                }
            }
            ?>
        </div>
    </section>
    <section class="listeDebugs">
        <div class="col-lg-12">
            <?php
            if (isset($_SESSION['setDebug']) && count($_SESSION['setDebug']) > 0) {
                foreach ($_SESSION['setDebug'] as $title => $elem) {
                    echo '
                    <pre class="alert-warning">';

                    echo($title != '' ? '<strong>' . $title . '</strong></br>' : '');
                    print_r($elem);

                    echo '
                    </pre>';
                }
            }
            ?>
        </div>
    </section>
    <section class="infosGlobale">
        <div class="col-lg-12">
            <div class="alert alert-success">
                <?= $this->ln->txt('debug', 'lab-controller', $this->language, 'Controller') ?> :
                <strong><?= $this->current_controller ?></strong>
            </div>
            <div class="alert alert-success">
                <?= $this->ln->txt('debug', 'lab-vue', $this->language, 'Vue') ?> :
                <strong><?= $this->current_function ?></strong>
            </div>
            <div class="alert alert-success">
                <?= $this->ln->txt('debug', 'lab-template', $this->language, 'Template') ?> :
                <strong><?= $this->current_template ?></strong>
            </div>
            <div class="alert alert-success">
                <?= $this->ln->txt('debug', 'lab-ip', $this->language, 'Adresse IP') ?> :
                <strong><?= (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']) ?></strong>
            </div>
            <div class="alert alert-success">
                <?= $this->ln->txt('debug', 'lab-bdd', $this->language, 'Base de données utilisée') ?> :
                <strong><?= $this->Config['bdd_config'][$this->Config['env']]['BDD'] ?></strong></br>
                <?= $this->ln->txt('debug', 'lab-bdd-commun', $this->language, 'Base de données globale') ?> :
                <strong><?= $this->Config['bdd_config'][$this->Config['env']]['BDD_COMMUN'] ?></strong>
            </div>
        </div>
    </section>
</div>
