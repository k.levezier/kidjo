<?php

/**
 * Charchement autoload Composer
 */
require __DIR__ . '/../vendor/autoload.php';

/**
 * Informations Serveur
 *
 * @param boolean $infos Statut des infos (true : active, false : inactive)
 */
$infos = false;

if ($infos) {
    phpinfo();
    die;
}

/**
 * Maintenance du site
 *
 * @param boolean $maintenance Statut de la maintenance (true : active, false : inactive).
 * @param array $allowedIP Tableau contenant les adresses IP qui ne passent pas par la maintenance.
 * @param string $adressIP Récupération de l'adresse IP avec prise en charge des services type CloudFlare.
 */
$maintenance = false;
$allowedIP = array('78.225.42.28', '93.26.42.99', '91.151.70.8', '::1');
$adressIP = (!empty($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);

if ($maintenance && !in_array($adressIP, $allowedIP)) {
    header('HTTP/1.1 503 Service Unavailable');
    header('location:/maintenance.php');
    die;
}

/**
 * Pseudo Frame en Index
 *
 * @param boolean $activationPseudoFrame Statut de l'affichage de la page.
 * @param string $urlPseudoFrame Adresse de la page.
 */
$activationPseudoFrame = false;
$urlPseudoFrame = 'http://www.equinoa.com';

if ($activationPseudoFrame) {
    $affichagePage = file_get_contents($urlPseudoFrame);
    echo $affichagePage;
    die;
}

/**
 * Detection Mobile
 *
 * @return string Peut prendre 3 valeurs : phone, tablet et computer.
 */
$mobileDetect = new Mobile_Detect();
$deviceType = ($mobileDetect->isMobile() ? ($mobileDetect->isTablet() ? 'tablet' : 'phone') : 'computer');
