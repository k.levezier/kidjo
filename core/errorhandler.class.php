<?php

/**
 * La classe ErrorHandler permet la gestion des erreurs.
 *
 * @class       ErrorHandler
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class ErrorHandler
{
    /**
     * @var string $file Fichier de log
     * @var string $allow_display Affichage des erreurs à l'écran
     * @var string $allow_log Enregistrement des erreurs
     * @var string $report Types d'erreurs à afficher
     * @var array $error_handler Tableau de config des erreurs
     */
    private $file;
    private $allow_display;
    private $allow_log;
    private $report;
    private $error_handler;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param array $config Tableau de config des erreurs
     */
    public function __construct($config)
    {
        $this->error_handler = $config;
        $this->file = $this->error_handler['error_handler'][$this->error_handler['env']]['file'];
        $this->allow_display = $this->error_handler['error_handler'][$this->error_handler['env']]['allow_display'];
        $this->allow_log = $this->error_handler['error_handler'][$this->error_handler['env']]['allow_log'];
        $this->report = $this->error_handler['error_handler'][$this->error_handler['env']]['report'];

        // Si le fichier de logs n'existe pas on le créé
        if (!file_exists($this->file)) {
            touch($this->file);
            chmod($this->file, 0766);
        }

        // On applique les paramètres de gestion des erreurs du fichier de config
        ini_set('display_errors', $this->allow_display);
        ini_set('log_errors', $this->allow_log);
        ini_set('error_log', $this->file);
        error_reporting($this->report);
    }
}


// indiquer l'app au debut des erreurs
