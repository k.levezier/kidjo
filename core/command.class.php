<?php

/**
 * La classe Command est historique donc on y touche pas.
 *
 * @class       Command
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class Command
{
    /**
     * @var string $Name Nom du controller
     * @var string $Function Nom de la fonction
     * @var array $Parameters Tableau des paramètres d'URL
     * @var string $Language Code de la langue
     */
    private $Name = '';
    private $Function = '';
    private $Parameters = array();
    private $Language = '';

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param string $controllerName Nom du controller
     * @param string $functionName Nom de la fonction
     * @param array $paramArray Tableau des paramètres d'URL
     * @param string $langue Code de la langue
     */
    public function __construct($controllerName, $functionName, $paramArray, $langue = '')
    {
        $this->Parameters = $this->castParamNull($paramArray);
        $this->Name = $controllerName;
        $this->Function = $functionName;
        $this->Language = $langue;
    }

    /**
     * Méthode pour caster le premier élément du tableau
     * de paramètres s'il est égal à NULL
     *
     * @function castParamNull
     * @param array $paramArray Tableau de paramètres
     * @return array
     */
    private function castParamNull($paramArray)
    {
        if ($paramArray[0] === null) {
            $paramArray[0] = '';
        }
        return $paramArray;
    }

    /**
     * Méthode magique GET
     *
     * @function __get
     * @param string $name Nom de l'attribut
     * @return string
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Méthode magique SET
     *
     * @function __set
     * @param string $name Nom de l'attribut
     * @param string $value Valeur de l'attribut
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }
}
