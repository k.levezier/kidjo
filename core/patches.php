<?php

/**
 * Listes de méthodes PHP si non disponible sur la version courante
 *
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */

/**
 * Définie la fonction array_column si elle n'est pas disponible
 *
 * @function array_column
 * @param array $array Un tableau multi-dimensionnel depuis lequel la colonne de valeurs sera prélevée
 * @param string $column_key La colonne de valeurs à retourner
 * @param string $index_key La colonne à utiliser comme index/clé pour le tableau retourné
 * @return array
 */
if (!function_exists("array_column")) {
    function array_column(array $array, $column_key, $index_key = null)
    {
        $return = array();

        if ($index_key != null) {
            foreach ($array as $key => $value) {
                $return[$value[$index_key]] = $value[$column_key];
            }
        } else {
            foreach ($array as $key => $value) {
                $return[] = $value[$column_key];
            }
        }

        return $return;
    }
}

/**
 * Définie la fonction notFound si elle n'est pas disponible
 *
 * @function notFound
 * @param string $haystack chaine dans laquelle on recherche
 * @param array $needles tableau avec les chaines que l'on cherche à trouver
 * @return bool
 */
if (!function_exists("notFound")) {
    function notFound($haystack, $needles = array())
    {
        foreach ($needles as $needle) {
            if (strpos($haystack, $needle) !== false) {
                return false;
            }
        }
        return true;
    }
}
