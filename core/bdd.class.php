<?php

/**
 * La classe bdd permet la gestion des accès et requêtes BDD.
 *
 * @class       bdd
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class bdd
{
    /**
     * @var string $config Serialize de la config
     * @var string $option Serialize des options
     * @var string $requete Requête demandée
     * @var object $ressource Objet de réponse Mysqli
     * @var integer $num_rows Nombre de lignes retournées
     * @var integer $affected_rows Nombre de lignes impactées
     * @var bdd $instance Instance unique de bdd
     */
    private $config;
    private $option;
    private $requete;
    private $ressource;
    private $num_rows;
    private $affected_rows;
    private static $instance;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     */
    private function __construct()
    {
        $this->config = unserialize(BDD_CONFIG);
        $this->option = unserialize(BDD_OPTION);
        $this->connect();
    }

    /**
     * Destructeur de la classe
     *
     * @function __destruct
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * Méthode pour enregistrer les erreurs
     *
     * @function error
     * @param string $msg Texte de l'erreur
     */
    private function error($msg = null)
    {
        $this->log_error[] = (mysqli_errno($this->connect_id) != 0 ? '[' . mysqli_errno($this->connect_id) . '] ' : '') . mysqli_error($this->connect_id) . ($msg != null && mysqli_error($this->connect_id) != '' ? ' - ' : '') . ($msg != null ? $msg : '');

        if ($this->option['DISPLAY_ERREUR'] == true) {
            $cle = key($this->log_error);
            $_SESSION['mysql_error'][] = $this->log_error[$cle];
            next($this->log_error);
        }
    }

    /**
     * Méthode pour enregistrer les debugs
     *
     * @function debug
     * @param string $function Requête executée
     * @param float $time Temps d'execution de la requête
     */
    private function debug($function, $time = '')
    {
        $this->log_debug[] = $function . ' ' . $time;

        if ($this->option['DISPLAY_ERREUR'] == true) {
            $_SESSION['mysql_debug'][] = array('requete' => $function, 'time' => $time);
        }
    }

    /**
     * Méthode pour se connecter à la BDD
     *
     * @function connect
     * @param string $host Serveur host de la BDD
     * @param string $user Nom d'utilisateur
     * @param string $password Mot de passe
     * @param string $bdd Base de données
     * @return boolean
     */
    private function connect($host = null, $user = null, $password = null, $bdd = null)
    {
        if ($host == null) {
            $host = $this->config['HOST'];
        }

        if ($user == null) {
            $user = $this->config['USER'];
        }

        if ($password == null) {
            $password = $this->config['PASSWORD'];
        }

        if ($bdd == null) {
            $bdd = $this->config['BDD'];
        }

        $this->connect_id = mysqli_connect($host, $user, $password, $bdd);

        if (!mysqli_set_charset($this->connect_id, 'UTF8')) {
            $this->error('Error loading the character set UTF8');

            return false;
        }

        if (!$this->connect_id) {
            $this->log_error[] = '[Unable to connect to database] ' . mysqli_connect_error();

            if ($this->option['DISPLAY_ERREUR'] == true) {
                $cle = key($this->log_error);
                $_SESSION['mysql_error'][] = $this->log_error[$cle];
                next($this->log_error);
            }

            return false;
        } else {
            return $this->connect_id;
        }
    }

    /**
     * Méthode pour fermer la connexion à la BDD
     *
     * @function close
     * @return boolean
     */
    public function close()
    {
        if (!mysqli_close($this->connect_id)) {
            $this->error('Error closing the connection');

            return false;
        }
    }

    /**
     * Méthode pour vider le résultat d'une requête
     *
     * @function free_result
     * @param object $ressource Objet contenant le résultat de la requête
     * @return boolean
     */
    public function free_result($ressource = null)
    {
        if ($ressource == null) {
            $ressource = $this->ressource;
        }

        mysqli_free_result($ressource);

        return true;
    }

    /**
     * Méthode pour traiter une requête
     *
     * @function query
     * @param string $requete La requête à traiter
     * @return boolean
     */
    public function query($requete)
    {
        $time = '';
        $this->requete = $requete;

        if ($this->option['BDD_PANIC_SEUIL'] > 0) {
            $start = microtime(true);
        }

        $this->ressource = mysqli_query($this->connect_id, $requete);

        if ($this->option['BDD_PANIC_SEUIL'] > 0) {
            $stop = microtime(true);
            $time = ($stop - $start);

            if (($stop - $start) > $this->option['BDD_PANIC_SEUIL']) {
                $_SESSION['mysql_panic'][] = array('requete' => $requete, 'time' => $time);
            }
        }

        if (!$this->ressource) {
            $this->error('Error executing the query : ' . $this->requete);
            return false;
        } else {
            $this->debug($this->requete, $time);
            return $this->ressource;
        }
    }

    /**
     * Méthode pour traiter une requête ou plusieurs requêtes séparée par des ';'
     *
     * @function multi_query
     * @param string $requete Les requêtes à traiter
     * @return array Tableau contenant la ressource de chaque query contenu dans $requete ou false en cas d'erreur
     */
    public function multi_query($requete)
    {
        $time = '';
        $this->requete = $requete;

        if ($this->option['BDD_PANIC_SEUIL'] > 0) {
            $start = microtime(true);
        }

        $this->ressource = mysqli_multi_query($this->connect_id, $requete);
        $ressourceReturn = array();
        do {
            // Store first result set
            if ($result = mysqli_store_result($this->connect_id)) {
                // Fetch one and one row
                $ressourceReturn[] = $result;
            }
        } while (mysqli_next_result($this->connect_id));

        if ($this->option['BDD_PANIC_SEUIL'] > 0) {
            $stop = microtime(true);
            $time = ($stop - $start);

            if (($stop - $start) > $this->option['BDD_PANIC_SEUIL']) {
                $_SESSION['mysql_panic'][] = array('requete' => $requete, 'time' => $time);
            }
        }

        if (count($ressourceReturn) == 0) {
            $this->error('Error executing the query : ' . $this->requete);
            return false;
        } else {
            $this->debug($this->requete, $time);
            return $ressourceReturn;
        }
    }

    /**
     * Méthode qui retourne le résultat dans un tableau associatif et indexé
     *
     * @function fetch_array
     * @param object $ressource Objet contenant le résultat de la requête
     * @return array
     */
    public function fetch_array($ressource = null)
    {
        if ($ressource == null) {
            $ressource = $this->ressource;
        }

        $array = mysqli_fetch_array($ressource);

        if (!is_object($ressource)) {
            $this->error('The argument passed to the mysqli_fetch_array() function is not a resource');
        } else {
            return $array;
        }
    }

    /**
     * Méthode qui retourne le résultat dans un tableau associatif
     *
     * @function fetch_assoc
     * @param object $ressource Objet contenant le résultat de la requête
     * @return array
     */
    public function fetch_assoc($ressource = null)
    {
        if ($ressource == null) {
            $ressource = $this->ressource;
        }

        $array = mysqli_fetch_assoc($ressource);

        if (!is_object($ressource)) {
            $this->error('The argument passed to the mysqli_fetch_assoc() function is not a resource');
        } else {
            return $array;
        }
    }

    /**
     * Méthode qui retourne le nombre de lignes impactées par la requête
     *
     * @function affected_rows
     * @return integer
     */
    public function affected_rows()
    {
        $this->affected_rows = mysqli_affected_rows($this->connect_id);

        if (!is_object($this->connect_id)) {
            $this->error('The argument passed to the mysqli_affected_rows() function is not a resource');
        } elseif ($this->affected_rows == -1) {
            $this->error('Error when executing the mysqli_affected_rows() function');
        } else {
            return $this->affected_rows;
        }
    }

    /**
     * Méthode qui retourne le nombre de lignes dans un résultat
     *
     * @function num_rows
     * @param object $ressource Objet contenant le résultat de la requête
     * @return integer
     */
    public function num_rows($ressource = null)
    {
        if ($ressource == null) {
            $ressource = $this->ressource;
        }

        $this->num_rows = mysqli_num_rows($ressource);

        if (!is_object($ressource)) {
            $this->error('The argument passed to the mysqli_num_rows() function is not a resource');
        } else {
            return $this->num_rows;
        }
    }

    /**
     * Méthode qui protège les données avant traitement
     *
     * @function escape_string
     * @param string $arg Donnée à protéger
     * @return string
     */
    public function escape_string($arg)
    {
        $magic_quotes_config = get_magic_quotes_gpc();

        if ($magic_quotes_config == 1) {
            $arg = stripslashes($arg);
        }

        return mysqli_real_escape_string($this->connect_id, $arg);
    }

    /**
     * Méthode qui retourne l'identifiant automatiquement généré par la dernière requête
     *
     * @function insert_id
     * @return integer
     */
    public function insert_id()
    {
        $last_insert_id = mysqli_insert_id($this->connect_id);

        if (!$last_insert_id) {
            $this->error('Error when executing the mysql_insert_id() function');
        } elseif ($last_insert_id == 0) {
            $this->error('No ID generated in the last query');
        } else {
            return $last_insert_id;
        }
    }

    /**
     * Méthode pour les requêtes SELECT
     *
     * @function select
     * @param string $table Nom de la table
     * @param string $where Conditions pour le WHERE
     * @param string $order Conditions pour le ORDER
     * @param integer $start Ligne de début du résultat
     * @param integer $nb Nombre de lignes à retourner
     * @return array
     */
    public function select($table, $where = '', $order = '', $start = '', $nb = '')
    {
        if ($where != '') {
            $where = ' WHERE ' . $where;
        }

        if ($order != '') {
            $order = ' ORDER BY ' . $order;
        }

        $sql = 'SELECT * FROM `' . $table . '`' . $where . $order . ($nb != '' && $start != '' ? ' LIMIT ' . $start . ',' . $nb : ($nb != '' ? ' LIMIT ' . $nb : ''));
        $res = $this->query($sql);
        $liste = array();

        while ($rec = $this->fetch_assoc($res)) {
            $liste[] = $rec;
        }

        return $liste;
    }

    /**
     * Méthode pour les requêtes COUNTER
     *
     * @function counter
     * @param string $table Nom de la table
     * @param string $where Conditions pour le WHERE
     * @return integer
     */
    public function counter($table, $where = '')
    {
        if ($where != '') {
            $where = ' WHERE ' . $where;
        }

        $sql = 'SELECT * FROM `' . $table . '`' . $where;
        $res = $this->query($sql);

        return (int)$this->num_rows($res);
    }

    /**
     * Méthode pour les requêtes EXIST
     *
     * @function exist
     * @param string $table Nom de la table
     * @param string $id Valeur du champ
     * @param string $field Nom du champ
     * @return array
     */
    public function exist($table, $id, $field)
    {
        $sql = 'SELECT * FROM `' . $table . '` WHERE `' . $field . '` = "' . $this->escape_string($id) . '"';
        $res = $this->query($sql);
        return ($this->fetch_array($res, 0, 0) > 0);
    }

    /**
     * Méthode pour le controle des slugs
     *
     * @function controlSlug
     * @param string $table Nom de la table
     * @param string $slug Slug à controler
     * @param string $id_name Nom du champ
     * @param string $id_value Valeur du champ
     */
    public function controlSlug($table, $slug, $id_name, $id_value)
    {
        $sql = 'SELECT `slug` FROM `' . $table . '` WHERE `slug` = "' . $this->escape_string($slug) . '" AND `' . $id_name . '` != "' . $this->escape_string($id_value) . '"';
        $res = $this->query($sql);

        if ($this->num_rows($res) == 1 || $slug == '') {
            if ($table == 'tree' && $id_value == 1 && $slug == '') {
                $slug = '';
            } else {
                $slug = $slug . '-' . $id_value;
            }
        }

        $sql = 'UPDATE `' . $table . '` SET `slug` = "' . $this->escape_string($slug) . '" WHERE `' . $id_name . '` = "' . $this->escape_string($id_value) . '"';
        $this->query($sql);
    }

    /**
     * Méthode pour le controle des slugs sur du multilangue / multiclefs
     *
     * @function controlSlugMultiLn
     * @param string $table Nom de la table
     * @param string $slug Slug à controler
     * @param string $id_value Valeur du champ
     * @param array $list_field_value Tableau des champs
     * @param string $id_langue Langue
     */
    public function controlSlugMultiLn($table, $slug, $id_value, $list_field_value, $id_langue)
    {
        $sql = 'SELECT * FROM `' . $table . '` WHERE `slug` = "' . $this->escape_string($slug) . '" AND `id_langue` = "' . $this->escape_string($id_langue) . '"';
        $res = $this->query($sql);

        if ($this->num_rows($res) > 1) {
            $new_slug = $slug;

            if ($id_langue != '') {
                $new_slug .= '-' . $id_langue;
            }

            $new_slug .= '-' . $id_value;

            $list = '';
            foreach ($list_field_value as $champ => $valeur) {
                $list .= ' AND `' . $champ . '` = "' . $this->escape_string($valeur) . '"';
            }

            $sql = 'UPDATE `' . $table . '` SET `slug` = "' . $this->escape_string($new_slug) . '" WHERE 1 = 1' . $list;
            $this->query($sql);
            $this->controlSlugMultiLn($table, $new_slug, $id_value, $list_field_value, $id_langue);
        }
    }

    /**
     * Méthode pour la génération des slugs
     *
     * @function generateSlug
     * @param string $str Chaîne à slugiser
     * @param array $options Tableua d'options
     * @return string
     */
    public function generateSlug($str, $options = array())
    {
        // Make sure string is in UTF-8 and strip invalid UTF-8 characters
        $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());

        // Default options
        $defaults = array(
            'delimiter'     => '-',
            'limit'         => null,
            'lowercase'     => true,
            'replacements'  => array(),
            'transliterate' => true,
        );

        // Merge options
        $options = array_merge($defaults, $options);
        $char_map = array(
            // Latin
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'AE',
            'Ç' => 'C',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ð' => 'D',
            'Ñ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ö' => 'O',
            'Ő' => 'O',
            'Ø' => 'O',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ü' => 'U',
            'Ű' => 'U',
            'Ý' => 'Y',
            'Þ' => 'TH',
            'ß' => 'ss',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ä' => 'a',
            'å' => 'a',
            'æ' => 'ae',
            'ç' => 'c',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ð' => 'd',
            'ñ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ö' => 'o',
            'ő' => 'o',
            'ø' => 'o',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ü' => 'u',
            'ű' => 'u',
            'ý' => 'y',
            'þ' => 'th',
            'ÿ' => 'y',
            // Latin symbols
            '©' => '(c)',
            // Greek
            'Α' => 'A',
            'Β' => 'B',
            'Γ' => 'G',
            'Δ' => 'D',
            'Ε' => 'E',
            'Ζ' => 'Z',
            'Η' => 'H',
            'Θ' => '8',
            'Ι' => 'I',
            'Κ' => 'K',
            'Λ' => 'L',
            'Μ' => 'M',
            'Ν' => 'N',
            'Ξ' => '3',
            'Ο' => 'O',
            'Π' => 'P',
            'Ρ' => 'R',
            'Σ' => 'S',
            'Τ' => 'T',
            'Υ' => 'Y',
            'Φ' => 'F',
            'Χ' => 'X',
            'Ψ' => 'PS',
            'Ω' => 'W',
            'Ά' => 'A',
            'Έ' => 'E',
            'Ί' => 'I',
            'Ό' => 'O',
            'Ύ' => 'Y',
            'Ή' => 'H',
            'Ώ' => 'W',
            'Ϊ' => 'I',
            'Ϋ' => 'Y',
            'α' => 'a',
            'β' => 'b',
            'γ' => 'g',
            'δ' => 'd',
            'ε' => 'e',
            'ζ' => 'z',
            'η' => 'h',
            'θ' => '8',
            'ι' => 'i',
            'κ' => 'k',
            'λ' => 'l',
            'μ' => 'm',
            'ν' => 'n',
            'ξ' => '3',
            'ο' => 'o',
            'π' => 'p',
            'ρ' => 'r',
            'σ' => 's',
            'τ' => 't',
            'υ' => 'y',
            'φ' => 'f',
            'χ' => 'x',
            'ψ' => 'ps',
            'ω' => 'w',
            'ά' => 'a',
            'έ' => 'e',
            'ί' => 'i',
            'ό' => 'o',
            'ύ' => 'y',
            'ή' => 'h',
            'ώ' => 'w',
            'ς' => 's',
            'ϊ' => 'i',
            'ΰ' => 'y',
            'ϋ' => 'y',
            'ΐ' => 'i',
            // Turkish
            'Ş' => 'S',
            'İ' => 'I',
            'Ç' => 'C',
            'Ü' => 'U',
            'Ö' => 'O',
            'Ğ' => 'G',
            'ş' => 's',
            'ı' => 'i',
            'ç' => 'c',
            'ü' => 'u',
            'ö' => 'o',
            'ğ' => 'g',
            // Russian
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'Yo',
            'Ж' => 'Zh',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'J',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'H',
            'Ц' => 'C',
            'Ч' => 'Ch',
            'Ш' => 'Sh',
            'Щ' => 'Sh',
            'Ъ' => '',
            'Ы' => 'Y',
            'Ь' => '',
            'Э' => 'E',
            'Ю' => 'Yu',
            'Я' => 'Ya',
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г' => 'g',
            'д' => 'd',
            'е' => 'e',
            'ё' => 'yo',
            'ж' => 'zh',
            'з' => 'z',
            'и' => 'i',
            'й' => 'j',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о' => 'o',
            'п' => 'p',
            'р' => 'r',
            'с' => 's',
            'т' => 't',
            'у' => 'u',
            'ф' => 'f',
            'х' => 'h',
            'ц' => 'c',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'sh',
            'ъ' => '',
            'ы' => 'y',
            'ь' => '',
            'э' => 'e',
            'ю' => 'yu',
            'я' => 'ya',
            // Ukrainian
            'Є' => 'Ye',
            'І' => 'I',
            'Ї' => 'Yi',
            'Ґ' => 'G',
            'є' => 'ye',
            'і' => 'i',
            'ї' => 'yi',
            'ґ' => 'g',
            // Czech
            'Č' => 'C',
            'Ď' => 'D',
            'Ě' => 'E',
            'Ň' => 'N',
            'Ř' => 'R',
            'Š' => 'S',
            'Ť' => 'T',
            'Ů' => 'U',
            'Ž' => 'Z',
            'č' => 'c',
            'ď' => 'd',
            'ě' => 'e',
            'ň' => 'n',
            'ř' => 'r',
            'š' => 's',
            'ť' => 't',
            'ů' => 'u',
            'ž' => 'z',
            // Polish
            'Ą' => 'A',
            'Ć' => 'C',
            'Ę' => 'e',
            'Ł' => 'L',
            'Ń' => 'N',
            'Ó' => 'o',
            'Ś' => 'S',
            'Ź' => 'Z',
            'Ż' => 'Z',
            'ą' => 'a',
            'ć' => 'c',
            'ę' => 'e',
            'ł' => 'l',
            'ń' => 'n',
            'ó' => 'o',
            'ś' => 's',
            'ź' => 'z',
            'ż' => 'z',
            // Latvian
            'Ā' => 'A',
            'Č' => 'C',
            'Ē' => 'E',
            'Ģ' => 'G',
            'Ī' => 'i',
            'Ķ' => 'k',
            'Ļ' => 'L',
            'Ņ' => 'N',
            'Š' => 'S',
            'Ū' => 'u',
            'Ž' => 'Z',
            'ā' => 'a',
            'č' => 'c',
            'ē' => 'e',
            'ģ' => 'g',
            'ī' => 'i',
            'ķ' => 'k',
            'ļ' => 'l',
            'ņ' => 'n',
            'š' => 's',
            'ū' => 'u',
            'ž' => 'z',
        );

        // Make custom replacements
        $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);

        // Transliterate characters to ASCII
        if ($options['transliterate']) {
            $str = str_replace(array_keys($char_map), $char_map, $str);
        }

        // Replace non-alphanumeric characters with our delimiter
        $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);

        // Remove duplicate delimiters
        $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);

        // Truncate slug to max. characters
        $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');

        // Remove delimiter from ends

        $str = trim($str, $options['delimiter']);

        return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
    }

    /**
     *  Méthode pour l'execution d'une requête
     *
     * @function run
     * @param string $req La requête à executer
     * @return array
     */
    public function run($req)
    {
        $resultat = $this->query($req);
        $result = array();

        while ($record = $this->fetch_assoc($resultat)) {
            $result[] = $record;
        }

        return $result;
    }

    /**
     *  Méthode qui retourne une instance unique de bdd
     *
     * @function get_instance
     * @return bdd
     */
    public static function get_instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new bdd();
        }

        return self::$instance;
    }

    /**
     *  Méthode qui retourne la liste des tables de la base
     *
     * @function getTables
     * @param string $bdd Nom de la BDD
     * @return array
     */
    public function getTables($bdd = null)
    {
        $bdd = ($bdd ? $bdd : $this->config['BDD']);
        return array_map('current', $this->run('SHOW TABLES FROM ' . $bdd));
    }

    /**
     * Méthode qui retourne la liste des tables InnoDB de la base
     *
     * @function getInnoDBTables
     * @param string $bdd Nom de la BDD
     * @return array
     */
    public function getInnoDBTables($bdd = null)
    {
        $bdd = ($bdd ? $bdd : $this->config['BDD']);
        return array_column($this->run('SHOW TABLE STATUS FROM ' . $bdd . ' WHERE engine = "InnoDB"'), 'Name');
    }

    /**
     * Méthode qui retourne la liste des contraintes de la base
     *
     * @function getDatabaseConstraints
     * @param string $bdd Nom de la BDD
     * @return array
     */
    public function getDatabaseConstraints($bdd = null)
    {
        $bdd = ($bdd ? $bdd : $this->config['BDD']);
        return $this->run('SELECT * FROM information_schema.KEY_COLUMN_USAGE WHERE TABLE_SCHEMA = "' . $bdd . '" AND REFERENCED_TABLE_SCHEMA ="' . $bdd . '"');
    }

    /**
     * Méthode qui retourne la liste des contraintes d'une table
     *
     * @function getTableConstraints
     * @param string $bdd Nom de la BDD
     * @param string $table Nom de la table
     * @return array
     */
    public function getTableConstraints($bdd = null, $table = null)
    {
        if ($table == null) {
            return array();
        }

        $bdd = ($bdd ? $bdd : $this->config['BDD']);
        $result = $this->fetch_assoc($this->query("SHOW CREATE TABLE `" . $bdd . "`.`" . $table . "`"));

        // On crée l'Array qui va contenir les différentes contraintes de la table courante
        $constrains = array();
        preg_match_all("/CONSTRAINT `([a-zA-Z0-9_\-]+)` FOREIGN KEY \(([a-zA-Z0-9, `_]+)\) REFERENCES (`[a-zA-Z0-9_]+`\.)?`([a-zA-Z0-9_]+)` \(([a-zA-Z0-9, `_]+)\) ([a-zA-Z0-9, `_]+)/", $result['Create Table'], $constrains, PREG_SET_ORDER);

        if (empty($constrains)) {
            preg_match_all("/CONSTRAINT `([a-zA-Z0-9_\-]+)` FOREIGN KEY \(([a-zA-Z0-9, `_]+)\) REFERENCES (`[a-zA-Z0-9_]+`\.)?`([a-zA-Z0-9_]+)` \(([a-zA-Z0-9, `_]+)\)/", $result['Create Table'], $constrains, PREG_SET_ORDER);
        }

        return $constrains;
    }

    /**
     * Méthode qui retourne le moteur de la table
     *
     * @function getTableEngine
     * @param string $bdd Nom de la BDD
     * @param string $table Nom de la table
     * @return string
     */
    public function getTableEngine($bdd = null, $table = null)
    {
        if ($table == null) {
            return array();
        }

        $bdd = ($bdd ? $bdd : $this->config['BDD']);
        $result = $this->fetch_assoc($this->query("SHOW CREATE TABLE `" . $bdd . "`.`" . $table . "`"));

        // On crée l'Array qui va contenir les différentes infos de la table courante
        $engine = array();
        preg_match("/ ENGINE=([a-zA-Z0-9_\-]+) /", $result['Create Table'], $engine);

        return $engine[1];
    }

    /**
     * Méthode qui retourne le charset de la table
     *
     * @function getTableCharset
     * @param string $bdd Nom de la BDD
     * @param string $table Nom de la table
     * @return string
     */
    public function getTableCharset($bdd = null, $table = null)
    {
        if ($table == null) {
            return array();
        }

        $bdd = ($bdd ? $bdd : $this->config['BDD']);
        $result = $this->fetch_assoc($this->query("SHOW CREATE TABLE `" . $bdd . "`.`" . $table . "`"));

        // On crée l'Array qui va contenir les différentes infos de la table courante
        $charset = array();
        preg_match("/ CHARSET=([a-zA-Z0-9_\-]+)/", $result['Create Table'], $charset);

        return $charset[1];
    }

    /**
     * Méthode qui retourne la BDD commune
     *
     * @function getBddCummun
     * @return string
     */
    public function getBddCummun()
    {
        return $this->config['BDD_COMMUN'];
    }

    /**
     * Méthode qui bloque et débloque le check des clefs étrangères
     *
     * @function activateCheckForeignKeys
     * @param boolean $active Paramètre pour dire si on active ou désactive
     */
    public function activateCheckForeignKeys($active)
    {
        if ($active) {
            $this->query('SET FOREIGN_KEY_CHECKS = 1;');
        } else {
            $this->query('SET FOREIGN_KEY_CHECKS = 0;');
        }
    }

    /**
     *  Méthode qui retourne la liste des moteurs de bdd
     *
     * @function showEngines
     * @return array
     */
    public static function showEngines()
    {
        $bdd = bdd::get_instance();
        return $bdd->run('SHOW ENGINES');
    }

    /**
     *  Méthode qui retourne la liste des charset
     *
     * @function showCharset
     * @return array
     */
    public static function showCharset()
    {
        $bdd = bdd::get_instance();
        return $bdd->run('SHOW CHARACTER SET');
    }

}
