<?php

namespace o;

use \bdd;

/**
 * La classe dat est la mère des classes data et instance et regroupe les mécanismes communs.
 * Require : bdd.class.php et librairie functions.class.php
 *
 * @class       dat
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
abstract class dat
{
    /**
     * @var bdd $bdd Instance de BDD
     */
    protected $bdd;
    /**
     * @var string $table Table de la BDD
     */
    protected $table;
    /**
     * @var string $desc Description de la table
     */
    public static $desc;
    /**
     * @var array $primary Tableau des clefs primaires
     */
    public static $primary;
    /**
     * @var resource $ressource Resource de la requête
     */
    protected $ressource;
    /**
     * @var array $setted Tableau de valeurs attribuées
     */
    protected $setted;
    /**
     * @var array $where Tableau de conditions
     */
    protected $where;
    /**
     * @var array $pregshortcuts Stockage des expressions regulières liées aux raccourcis
     */
    protected $pregshortcuts;
    /**
     * @var bool $read_only Mécanisme automatique de modification de la BDD
     */
    protected $read_only = true;
    /**
     * @var array $cles Tableau des clefs étrangeres
     */
    public static $cles;
    /**
     * @var array $labels Tableau des labels
     */
    public static $labels;
    /**
     * @var array $defaultConditions Tableau des conditions par defaut
     */
    protected static $defaultConditions;
    /**
     * @var bool $enabledDefaultConditions Activation des conditions par defaut
     */
    protected $enabledDefaultConditions = true;
    /**
     * @var array $bases_tables Associe au nom d'une table son accesseur MySQL base.table
     */
    protected static $bases_tables = array();

    /**
     * @const key_separator Séparateur de clefs
     */
    const key_separator = "_";
    /**
     * @const join_separator Séparateur de jointure
     */
    const join_separator = '->';

    /**
     * Constructeur de la classe
     *
     * @function __construct
     */
    public function __construct()
    {
        $this->bdd = bdd::get_instance();
        //Initialisation des racourcis
        $class = __NAMESPACE__ . '\\' . $this->table;
        $shortcuts = (array)$class::$shortcuts;
        $this->pregshortcuts = array_combine(array_map(function ($value) {
            return '/^' . $value . '/';
        }, array_keys($shortcuts)), array_values($shortcuts)
        );
    }

    /**
     * Destructeur de la classe
     *
     * @function __destruct
     */
    public function __destruct()
    {
        if (!$this->read_only && $this->setted !== null) {
            $this->save();
        }
    }

    /**
     * Méthode magique SET
     *
     * @function __set
     * @param string $name Nom de l'attribut
     * @param string $value Valeur de l'attribut
     */
    public function __set($name, $value)
    {
        $name = $this->applyShortcuts($name);

        //S'il s'agit d'une suite
        $names = explode(self::join_separator, $name);
        if (count($names) > 1) {
            $cur = $this;
            $name = array_pop($names);
            foreach ($names as $accs) {
                if (substr($accs, -2, 2) == '()') {
                    $func = substr($accs, 0, -2);
                    $cur = $cur->$func();
                } else {
                    $cur = $cur->$accs;
                    if ($cur === null) {
                        return;
                    }
                }
            }
            $cur->$name = $value;
        } //S'il s'agit d'une clé
        elseif (isset(self::$cles[$this->table][$name])) {
            $cle = self::$cles[$this->table][$name];
            $this->setted[$name] = $value;
            // SI Many -> One
            if ($cle['create_type'] == 'one') {
                // On copie la valeur des clés étrangère
                foreach ($cle['keys'] as $one => $many) {
                    $this->setted[$many] = $value->$one;
                }
            } // SI One -> Many
            else {
                // On copie la valeur des clés étrangère
                foreach ($cle['keys'] as $one => $many) {
                    $value->$many = $this->$one;
                }
            }
        } //Sinon
        else {
            $this->setted[$name] = $value;
        }
    }

    /**
     * Méthode magique ISSET
     *
     * @function __isset
     * @param string $name Nom de l'attribut
     */
    public function __isset($name)
    {
        $name = $this->applyShortcuts($name);
        // S'il s'agit d'une suite (parent->template->name)
        $names = explode(self::join_separator, $name);
        if (count($names) > 1) {
            $cur = $this;
            foreach ($names as $name) {
                if (!isset($cur[$name])) {
                    return false;
                }
                $cur = $cur[$name];
            }
            return true;
        }

        // S'il sagit d'un int
        if (ctype_digit($name)) {
            if ($field = $this->field(intval($name))) {
                return true;
            } else {
                return false;
            }
        }

        // Si c'est une variable setted
        if (isset($this->setted[$name])) {
            return true;
        }

        // S'il s'agit d'une colonne de la table
        if (in_array($name, array_column($this->desc(), 'Field'))) {
            return true;
        }

        // S'il s'agit d'une clé étrangère
        if (isset(self::$cles[$this->table][$name])) {
            return true;
        }

        // S'il s'agit d'une fonction
        if (substr($name, -2, 2) == '()') {
            return true;
        }
    }

    /**
     * Méthode Reset
     *
     * @function reset
     * @param bool $all
     */
    public function reset($all = false)
    {
        $this->setted = null;
        if ($all) {
            $this->data = null;
            $this->ressource = null;
        }
    }

    /**
     * Méthode qui défini le paramètre ReadOnly (si true, aucun mécanisme
     * automatique de modification de la BDD ne s'exécutera)
     *
     * @function setReadOnly
     * @param bool $read_only
     */
    public function setReadOnly(bool $read_only)
    {
        $this->read_only = $read_only;
    }

    /**
     * Méthode qui retourne la ressource (execute la requête si nécéssaire)
     *
     * @function ressource
     * @return ressource Ressource mysqli_result
     */
    protected function ressource()
    {
        if ($this->ressource === null) {
            $ressource = $this->bdd->query($this->query());
            if ($ressource === false) {
                $backtrace = debug_backtrace();
                while ($step = next($backtrace)) {
                    if (strpos($step['file'], 'octeract') === false) {
                        trigger_error("SQL Error in Octeract's object (" . $this->table . ") in " . $step['file'] . " on line " . $step['line'] . " (Query : " . $this->query() . "). Triggered", E_USER_ERROR);
                    }
                }
            }
            $this->ressource = new ressource($ressource);
        }
        return $this->ressource->ressource;
    }

    /**
     * Méthode qui retourne la requête SQL permettant la récupération des
     * données de l'objet
     *
     * @function query
     * @return string
     */
    public function query()
    {
        return "SELECT * FROM " . self::getTableToQuery($this->table) . $this->condition();
    }

    /**
     * Méthode qui retourne le résultat d'une requête delete
     *
     * @function delete
     * @return bool
     */
    public function delete()
    {
        if ($this->bdd->query($this->deleteQuery()) === false) {
            $backtrace = debug_backtrace();
            while ($step = next($backtrace)) {
                if (strpos($step['file'], 'octeract') === false) {
                    trigger_error("SQL Error in Octeract's object (" . $this->table . ") in " . $step['file'] . " on line " . $step['line'] . " (Query : " . $this->deleteQuery() . "). Triggered", E_USER_ERROR);
                }
            }
        }
        return true;
    }

    /**
     * Méthode qui retourne la requête delete
     *
     * @function deleteQuery
     * @return string
     */
    public function deleteQuery()
    {
        return "DELETE FROM " . self::getTableToQuery($this->table) . $this->condition();
    }

    /**
     * Méthode qui retourne la requête d'update
     *
     * @function updateQuery
     * @return string
     */
    public function updateQuery()
    {
        $table = self::getTableToQuery($this->table);
        $data = $this->prepareValues($this->getSettedArray());
        $this->defaultAddValue($data);
        $join = '';
        if ($this instanceof data) {
            $this->prepareCondition();
            $join = $this->joinStatement();
        }
        $where = $this->condition();
        $fields = implode(', ', array_map(function ($field, $value) use ($table) {
            return $table . '.' . self::addAccent($field) . " = " . $value;
        }, array_keys($data), array_values($data)));
        return "UPDATE $table $join SET $fields $where";
    }

    /**
     * Méthode qui éxécute et retourne le résultat de la requête d'update
     *
     * @function update
     * @return bool
     */
    public function update()
    {
        if ($this->bdd->query($this->updateQuery()) === false) {
            $backtrace = debug_backtrace();
            while ($step = next($backtrace)) {
                if (strpos($step['file'], 'octeract') === false) {
                    trigger_error("SQL Error in Octeract's object (" . $this->table . ") in " . $step['file'] . " on line " . $step['line'] . " (Query : " . $this->updateQuery() . "). Triggered", E_USER_ERROR);
                }
            }
        }
        return true;
    }

    /**
     * Méthode qui retourne le tableau des champs modifiés
     *
     * @function getSettedArray
     * @return array
     */
    public function getSettedArray()
    {
        return array_intersect_key((array)$this->setted, array_flip($this->fields()));
    }

    /**
     * Méthode qui retourne les conditions appliquées (sauf les addWheres et les conditions par défaut)
     *
     * @function getWhere
     * @param string $field Nom du champ (facultatif, si pas renseigné tous les champs seront retournées dans un array)
     * @return mixed
     */
    public function getWhere($field = null)
    {
        if ($field !== null) {
            if (isset($this->where[$field])) {
                return $this->where[$field];
            }
            return false;
        }
        return $this->where;
    }

    /**
     * Méthode qui retourne les conditions de la clause where
     *
     * @function condition
     * @return string
     */
    abstract protected function condition();

    /**
     * Méthode d'ajout de condition par defaut
     *
     * @function addDefaultCondition
     * @param array $condition Tableau des conditions
     * @return array
     */
    public static function addDefaultCondition($condition)
    {
        self::$defaultConditions = array_merge((array)self::$defaultConditions, $condition);
    }

    /**
     * Méthode de reset des conditions par defaut
     *
     * @function resetDefaultCondition
     * @return array
     */
    public static function resetDefaultCondition()
    {
        self::$defaultConditions = array();
    }

    /**
     * Méthode qui retourne la description de la table
     *
     * @function desc
     * @return array
     */
    public function desc()
    {
        return $this->descOf($this->table);
    }

    /**
     * Méthode qui retourne la description de la table
     *
     * @function descOf
     * @param string $table Table à analyser
     * @return array
     */
    public function descOf($table)
    {
        if (!isset(self::$desc[$table])) {
            self::$desc[$table] = $this->bdd->run("DESC " . self::getTableToQuery($table));
        }
        return self::$desc[$table];
    }

    /**
     * Méthode qui retourne les indexes de la table
     *
     * @function indexes
     * @return array
     */
    public function indexes()
    {
        return $this->indexesOf($this->table);
    }

    /**
     * Méthode qui retourne les indexes de la table
     *
     * @function indexesOf
     * @param string $table Table à analyser
     * @return array
     */
    public function indexesOf($table)
    {
        $indexes = $this->bdd->run("SHOW INDEXES FROM " . self::getTableToQuery($table));
        $indexes = array_combine(array_column($indexes, 'Column_name'), $indexes);
        return $indexes;
    }

    /**
     * Méthode qui retourne la liste de champs
     *
     * @function fields
     * @return array
     */
    public function fields()
    {
        return $this->fieldsOf($this->table);
    }

    /**
     * Méthode qui retourne la liste de champs
     *
     * @function fieldsOf
     * @param string $table Table à analyser
     * @return array
     */
    public function fieldsOf($table)
    {
        return array_column($this->descOf($table), 'Field');
    }

    /**
     * Méthode qui retourne le nom du champ à la position $offset
     *
     * @function field
     * @param int $offset Position
     * @return array
     */
    public function field($offset)
    {
        $desc = $this->desc();
        if (isset($desc[$offset])) {
            return $desc[$offset]['Field'];
        }
        return false;
    }

    /**
     * Méthode qui retourne la liste des clés primaires
     *
     * @function primary
     * @return array
     */
    public function primary()
    {
        if (!isset(self::$primary[$this->table])) {
            self::$primary[$this->table] = array();
            foreach ($this->desc() as $field) {
                if ($field['Key'] == 'PRI') {
                    self::$primary[$this->table][] = $field['Field'];
                }
            }
        }
        return self::$primary[$this->table];
    }

    /**
     * Méthode qui indique si la table dispose d'un auto-increment
     *
     * @function hasAutoIncrement
     * @return bool
     */
    public function hasAutoIncrement()
    {
        return array_search('auto_increment', array_column($this->desc(), 'Extra')) !== false;
    }

    /**
     * Méthode qui check si une table existe dans la/les bases.
     *
     * @function tableExist
     * @param array $table_to_check Table à checker
     * @return bool
     */
    public static function tableExist($table_to_check)
    {
        $config = unserialize(BDD_CONFIG);
        $lBases = array();
        $lTables = array();
        $bdd = bdd::get_instance();

        if ($config['BDD'] == $config['BDD_COMMUN']) {
            $lBases = array($config['BDD']);
        } else {
            $lBases = array($config['BDD'], $config['BDD_COMMUN']);
        }

        foreach ((array)$lBases as $bdd_name) {
            foreach ($bdd->getInnoDBTables($bdd_name) as $table) {
                $lTables[] = $table;
            }
        }

        if (in_array($table_to_check, $lTables)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Méthode charge les clés étrangères depuis la BDD.
     * Charge aussi la liste des tables pour la construction
     * des requêtes et la liste des champs qui servent de label.
     *
     * @function getForeignKeys
     * @param array $bdd_names Tableau des BDD
     * @param array $ip_admin Tableau des IP admin
     */
    public static function getForeignKeys($bdd_names = null, $ip_admin = array())
    {
        // Emplacement du cache
        $filename = '../../tmp/octeract/octeract.cache';

        if (file_exists($filename) && !in_array(user_ip(), $ip_admin)) {
            include($filename);
        } else {
            $bdd = bdd::get_instance();

            // On parcourt les tables InnoDB de la base de données
            foreach ((array)$bdd_names as $bdd_name) {
                foreach ($bdd->getInnoDBTables($bdd_name) as $table) {
                    // Si on connait pas ce nom de table
                    if (!isset(self::$bases_tables[$table])) {
                        // On stocke l'accesseur à la table comprenant le nom de la base
                        self::$bases_tables[$table] = ($bdd_name ? '`' . $bdd_name . '`.`' : '`') . $table . '`';
                    } else {
                        // On saute cette table
                        trigger_error('Two tables share the same name (' . $table . ') in databases. To use Octeract, each name must be unique.', E_USER_ERROR);
                        continue;
                    }

                    // On obtient l'instruction SQL de création de table
                    $result = $bdd->fetch_assoc($bdd->query("SHOW CREATE TABLE " . self::getTableToQuery($table)));

                    // On crée l'Array qui va contenir les différentes contraintes de la table courante
                    $constrains = array();
                    preg_match_all("/CONSTRAINT `([a-zA-Z0-9_\-]+)` FOREIGN KEY \(([a-zA-Z0-9, `_]+)\) REFERENCES (`[a-zA-Z0-9_]+`\.)?`([a-zA-Z0-9_]+)` \(([a-zA-Z0-9, `_]+)\)/", $result['Create Table'], $constrains, PREG_SET_ORDER);

                    // Pour chaque contrainte
                    foreach ($constrains as $constrain) {
                        // On coupe le nom de la clé étrangère pour obtenir les accesseurs
                        $accesseurs = explode('-', $constrain[1]);
                        $keys = array_combine(
                            self::explode_keys($constrain[5]), // On parse les clés d'origine
                            self::explode_keys($constrain[2])   // et les clés d'arrivée
                        );

                        // On enregistre la clé étrangère dans le sens many=>one
                        if (isset(self::$cles[$table][$accesseurs[0]])) {
                            trigger_error('Two accesors share the same name (' . $accesseurs[0] . '). To use Octeract, each accessor\'s name must be unique.', E_USER_ERROR);
                        }
                        self::$cles[$table][$accesseurs[0]] = array(
                            'create_type' => 'one', // Type à créer
                            'table'       => $constrain[4], // La table concernée
                            'keys'        => $keys,
                        );           // Les clés à utiliser
                        // On enregistre la clé étrangère dans le sens one=>many
                        if (!isset($accesseurs[1])) {
                            $accesseurs[1] = $accesseurs[0];
                        }
                        if (isset(self::$cles[$constrain[4]][$accesseurs[1]])) {
                            trigger_error('Two accesors share the same name (' . $accesseurs[1] . ') in databases. To use Octeract, each accessor\'s name must be unique.', E_USER_ERROR);
                        }
                        self::$cles[$constrain[4]][$accesseurs[1]] = array(
                            'create_type' => 'many', // Type à créer
                            'table'       => $table, // La table concerné
                            'keys'        => $keys,
                        );           // Les clés à utiliser
                    }
                    $labels = array();
                    // Recherche l'annotation @label dans les commentaires
                    preg_match_all("/`(.+)` .+ COMMENT '.*@label.*'/", $result['Create Table'], $labels, PREG_SET_ORDER);
                    // et enregistre la liste des champs concernés
                    foreach ($labels as $label) {
                        self::$labels[$table][] = $label[1];
                    }
                }
            }

            $file = "<?php \n";
            foreach (array('cles', 'bases_tables', 'labels') as $var) {
                $file .= 'self::$' . $var . ' = ' . var_export(self::${$var}, true) . ";\n\n";
            }
            file_put_contents($filename, $file);
        }
    }

    /**
     * Méthode qui parse la liste des clés
     *
     * @function explode_keys
     * @param string $keys Liste des clefs
     * @return array
     */
    public static function explode_keys($keys)
    {
        $keys = explode(',', $keys);
        foreach ($keys as $pos => $key) {
            $keys[$pos] = trim(str_replace('`', '', $key));
        }
        return $keys;
    }

    /**
     * Méthode qui récupère les clés liès à la table courante
     *
     * @function getCles
     * @return array
     */
    public function getCles()
    {
        return (array)self::$cles[$this->table];
    }

    /**
     * Méthode qui récupère le nom de la table courante
     *
     * @function getTable
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Retourne la table correspond à l'acceseur appliqué sur l'objet actuel
     *
     * @param string $accesseur
     * @return string
     */
    public function getJoinedTableName($accesseur)
    {
        $accessors = array_filter(explode(self::join_separator, $accesseur), 'strlen');
        $current_table = $this->table;
        foreach ($accessors as $accessor) {
            $current_table = self::$cles[$current_table][$accessor]['table'];
            if ($current_table === null) {
                break;
            }
        }
        return $current_table;
    }

    /**
     * Méthode qui retourne le nom de la table préparée pour les requêtes
     *
     * @function getTableToQuery
     * @param string $table Nom de la table
     * @return string
     */
    public static function getTableToQuery($table)
    {
        return isset(self::$bases_tables[$table]) ? self::$bases_tables[$table] : '`' . $table . '`';
    }

    /**
     * Méthode qui prépare l'array de valeur pour construire une requête SQL
     *
     * @function prepareValues
     * @param array $values Tableau de valeurs
     * @return array
     */
    public function prepareValues(array $values)
    {
        foreach ($values as $key => $value) {
            if ($value === null) {
                $values[$key] = 'NULL';
            } elseif (is_string($value)) {
                $values[$key] = '"' . $this->bdd->escape_string($value) . '"';
            }
        }
        return $values;
    }

    /**
     * Méthode qui prépare l'array de valeur pour construire les conditions d'une requête SQL
     *
     * @function prepareValuesToCondition
     * @param array $values Tableau de valeurs
     * @return array
     */
    public function prepareValuesToCondition(array $values)
    {
        foreach ($values as &$value) {
            $value = $this->prepareValueToCondition($value);
        }
        return $values;
    }

    /**
     * Méthode qui prépare une valeur pour construire une conditions d'une requête SQL
     *
     * @function prepareValueToCondition
     * @param mixed $value Valeur
     * @return array
     */
    public function prepareValueToCondition($value)
    {
        if ($value === null) {
            return ' IS NULL';
        } elseif (is_numeric($value)) {
            return ' = ' . $this->bdd->escape_string($value) . '';
        } elseif (is_string($value)) {
            return ' LIKE "' . $this->bdd->escape_string($value) . '"';
        } elseif (is_array($value)) {
            if (count($value) > 0) {
                return ' IN (' . implode(', ', $this->prepareValues($value)) . ')';
            } else {
                return ' IS NULL AND 1=0';
            }
        } else {
            return ' = ' . $this->bdd->escape_string($value);
        }
    }

    /**
     * Méthode qui ajoute une valeur au tableau s'il s'agit bien d'un champ de la table.
     *
     * @function addValue
     * @param array $values Liste des valeurs
     * @param string $name Nom de la valeur
     * @param string $value Valeur tel qu'elle sera exprimée dans la requête
     * @param bool $replace Indique si la valeur existante doit être remplacé ou pas
     */
    public function addValue(array &$values, $name, $value, $replace = false)
    {
        if ((!isset($values[$name]) || $replace) && in_array($name, $this->fields())) {
            $values[$name] = $value;
        }
    }

    /**
     * Méthode qui ajoute les valeurs habituelles
     *
     * @function defaultAddValue
     * @param array $values Liste des valeurs
     */
    public function defaultAddValue(array &$values)
    {
        $this->addValue($values, 'updated', 'NOW()');
        $this->addValue($values, 'ip', '"' . user_ip() . '"');
    }

    /**
     * Méthode qui ajoute un accent grave de chaque côté de la valeur.
     * Destiné au nom des champs dans une requête SQL.
     *
     * @function addAccent
     * @param string $value Nom du champ
     * @return string
     */
    public static function addAccent($value)
    {
        return "`$value`";
    }

    /**
     * Applique les raccourcis définis dans la class instance de cette table à l'accesseur passé en paramètre
     *
     * @function applyShortcuts
     * @param string $link
     * @return string
     */
    protected function applyShortcuts($link)
    {
        return preg_replace(array_keys($this->pregshortcuts), array_values($this->pregshortcuts), $link);
    }

    /**
     * Applique les raccourcis définis dans la class instance de cette table aux clés de l'array passées en paramètre
     *
     * @function applyShortcutsToKeys
     * @param array $array_link
     * @return array
     */
    protected function applyShortcutsToKeys($array_link)
    {
        if (!count($this->pregshortcuts)) {
            return $array_link;
        }
        foreach ($array_link as $k => $v) {
            unset($array_link[$k]);
            $array_link[$this->applyShortcuts($k)] = $v;
        }
        return $array_link;
    }

    /**
     * Retourne l'array de conditions par défaut à appliquer, si celles-ci sont activées
     *
     * @function defaultConditions
     * @return array
     */
    public function defaultConditions()
    {
        if ($this->enabledDefaultConditions) {
            return $this->applyShortcutsToKeys((array)self::$defaultConditions);
        } else {
            return array();
        }
    }

    /**
     * Active/Désactive les conditions par défaut pour cet objet
     *
     * @function enableDefaultConditions
     * @param bool $enable
     */
    public function enableDefaultConditions($enable = true)
    {
        $this->enabledDefaultConditions = (bool)$enable;
        return $this;
    }

    /**
     * Active/Désactive la vérification des clés étrangères
     *
     * @function setCheckKeys
     * @param bool $check
     */
    public static function setCheckKeys($check = true)
    {
        $bdd = \bdd::get_instance();
        $bdd->query('SET FOREIGN_KEY_CHECKS = ' . ($check ? '1' : '0') . ';');
    }

    //////////////////////////////////////////////////////////////////////////////////////
    ////////////////////// INTERFACE: IteratorAggregate (undeclared) /////////////////////

    /**
     * Méthode qui sert à obtenir l'iterateur correspondant à l'objet,
     * c'est à dire lui-même
     *
     * @function getIterator
     * @return $this
     */
    public function getIterator()
    {
        return $this;
    }

}

/**
 * La classe ressource permet la gestion des ressources SQL
 *
 * @class       ressource
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class ressource
{
    /**
     * @var object $ressource La ressource
     */
    public $ressource;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     */
    public function __construct($ressource)
    {
        $this->ressource = $ressource;
    }

    /**
     * Destructeur de la classe
     *
     * @function __destruct
     */
    public function __destruct()
    {
        $this->ressource->free();
    }

}

// Lance la récupération des tables et des clés étrangères
$lTables = array();
if ($config['bdd_config'][$config['env']]['BDD'] == $config['bdd_config'][$config['env']]['BDD_COMMUN']) {
    $lTables = array(null);
} else {
    $lTables = array(null, $config['bdd_config'][$config['env']]['BDD_COMMUN']);
}
dat::getForeignKeys($lTables, $config['ip_admin'][$config['env']]);
