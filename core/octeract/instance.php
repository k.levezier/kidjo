<?php

namespace o;

spl_autoload_register(__NAMESPACE__ . '\load_instance');

/**
 * Méthode de chargement de l'instance
 *
 * @function load_instance
 * @param string $classname Nom de la classe à charger
 */
function load_instance($classname)
{
    if (strpos($classname, __NAMESPACE__ . '\\') === 0) {
        $classname = substr($classname, strlen(__NAMESPACE__) + 1);

        if (!file_exists('../../datas/' . $classname . '.o.php')) {
            eval("namespace " . __NAMESPACE__ . "; class " . $classname . "_core extends instance {}");
        } else {
            require_once('../../datas/' . $classname . '.o.php');
        }

        if (!file_exists('../../override/datas/' . $classname . '.o.php')) {
            eval("namespace " . __NAMESPACE__ . "; class " . $classname . " extends " . $classname . "_core {}");
        } else {
            require_once('../../override/datas/' . $classname . '.o.php');
        }
    }
}

/**
 * La classe instance représente, sous forme d'objet, une entrée bien précise de la base donnée.
 * Une classe homonyme héritant d'instance est créée par table.
 *
 * @class       instance
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class instance extends dat implements \Countable, \ArrayAccess, \Iterator
{
    /**
     * @var array $data Tableau de résultats
     */
    protected $data;
    /**
     * @var array $shortcuts Définition des reccourcis
     */
    public static $shortcuts;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     */
    public function __construct()
    {
        // On récupére le nom de la classe pour définir la table sur laquelle on travaille
        $this->table = substr(get_class($this), strlen(__NAMESPACE__) + 1);
        parent::__construct();
        // On récupère les paramètres du constructeur
        $this->setData(func_get_args());
    }

    /**
     * Gère la gestion des paramètres pour la sélection d'une instance
     *
     * @function setData
     * @param array $params Liste des paramètres du constructeur qui eux-mêmes peuvent être:
     *      - Un array non-associatif avec la liste des clés non définies par les conditions globales
     *      - Un array associatif
     *      - La liste des clés non définies par les conditions globales
     *      - La concaténation des clés avec le séparateurs de clés
     */
    private function setData($params)
    {
        // On cast les $params
        $params = (array)$params;

        // Si le tableau est vide on met un zéro
        if (count($params) == 0) {
            $params = array(0);
        }

        // Si on a un seul paramètre, qui est un array non-associatif
        if (count($params) == 1 && is_array($params[0]) && array_values($params[0]) === $params[0]) {
            // On considère que ce premier paramètre est la liste des paramètres
            $params = $params[0];
        }

        // Si le premier paramètre est un tableau associatif
        if (isset($params[0]) && is_array($params[0]) && array_values($params[0]) !== $params[0]) {
            // alors on considère qu'il correspond aux données
            $this->data = array_intersect_key($this->applyShortcutsToKeys($params[0]), array_flip($this->fields()));
            //On s'occupe enseuite de spécifier la clause where
            $toWhere = array_merge($this->defaultConditions(), $this->data);
            $primary = $this->primary();
            $primary_keys = array_intersect_key($toWhere, array_flip($primary));
            if (count($primary) == count($primary_keys)) {
                $this->where = $primary_keys;
            } else {
                $this->where = $toWhere;
            }
        } // Sinon
        elseif (count($params) != 0) {
            // Si on a un seul paramètre
            if (count($params) == 1) {
                // et s'il s'agit d'un array
                if (is_array($params[0])) {
                    // on considère que c'est la liste des clés
                    $params = $params[0];
                } // s'il ne s'agit pas d'un array
                else {
                    // on explode via le key_separator pour obtenir la liste des clés
                    $params = explode(self::key_separator, $params[0]);
                }
            }

            // On obtient la liste des clés manquantes en tenant compte des conditions par défault
            $missing_keys = array_diff($this->primary(), array_keys($this->defaultConditions()));
            // On compte le nombre de paramètres qu'on va utiliser
            $count = min(count($missing_keys), count($params));
            // On obtient les clés définis dans les conditions par défaut
            $this->data = array_intersect_key($this->defaultConditions(), array_flip($this->primary()));
            // puis on les merge aux paramètres qu'on attributs aux clés manquantes
            $this->data = array_merge(
                $this->data, array_combine(
                    array_slice($missing_keys, 0, $count), array_slice($params, 0, $count)
                )
            );
            $this->where = $this->data;
        }
    }

    /**
     * Méthode magique GET
     *
     * @function __get
     * @param string $name Nom de l'attribut
     * @return \data|\class
     */
    public function __get($name)
    {
        $name = $this->applyShortcuts($name);
        // S'il s'agit d'une suite (parent->template->name)
        $names = explode(self::join_separator, $name);
        if (count($names) > 1) {
            $cur = $this;
            foreach ($names as $name) {
                if (substr($name, -2, 2) == '()') {
                    $func = substr($name, 0, -2);
                    $cur = $cur->$func();
                } else {
                    $cur = $cur->$name;
                    if ($cur === null) {
                        break;
                    }
                }
            }
            return $cur;
        }

        // S'il sagit d'un int
        if (ctype_digit($name)) {
            if ($field = $this->field(intval($name))) {
                return $this->$field;
            } else {
                return null;
            }
        }

        // Si c'est une variable setted
        if (isset($this->setted[$name])) {
            return $this->setted[$name];
        }

        // S'il s'agit d'une colonne de la table
        if (in_array($name, array_column($this->desc(), 'Field'))) {
            if (!isset($this->data[$name])) {
                $this->load();
            }
            return $this->data[$name];
        }

        // S'il s'agit d'une clé étrangère
        if (isset(self::$cles[$this->table][$name])) {
            $cle = self::$cles[$this->table][$name];
            $data = array();
            // SI Many -> One
            if ($cle['create_type'] == 'one') {
                $class = __NAMESPACE__ . '\\' . $cle['table'];
                // On copie la valeur des clés étrangère
                foreach ($cle['keys'] as $one => $many) {
                    if ($this->$many === null) {
                        return new noResults();
                    }
                    $data[$one] = $this->$many;
                }
                $this->setted[$name] = new $class($data);
                return $this->setted[$name];
            } // SI One -> Many
            else {
                // On copie la valeur des clés étrangère
                foreach ($cle['keys'] as $one => $many) {
                    if ($this->$one === null) {
                        return new noResults();
                    }
                    $data[$many] = $this->$one;
                }
                $this->setted[$name] = new data($cle['table'], $data);
                return $this->setted[$name];
            }
        }

        // S'il s'agit d'une fonction
        if (substr($name, -2, 2) == '()') {
            $func = substr($name, 0, -2);
            return $this->$func();
        }
        // Sinon NULL
        $backtrace = current(debug_backtrace());
        trigger_error("Undefined property: " . __NAMESPACE__ . "\\" . $this->table . "::" . $name . " in " . $backtrace['file'] . " on line " . $backtrace['line'] . ". Triggered ", E_USER_NOTICE);
    }

    /**
     * Méthode qui charge les données depuis SQL
     *
     * @function load
     * @return bool
     */
    private function load()
    {
        if (!empty($this->data) && $this->count() > count($this->data)) {
            if ($this->exist()) {
                if ($this->data = $this->ressource()->fetch_assoc()) {
                    return true;
                }
            }
            // ERROR: Donnée non accessible
            $this->data = null;
        }
    }

    /**
     * Méthode qui indique si l'instance existe ou non
     *
     * @function exist
     * @return bool
     */
    public function exist()
    {
        return $this->ressource()->num_rows == 1 && $this->condition() !== null;
    }

    /**
     * Méthode qui retourne la clé primaire ou la concaténation des clés primaires
     *
     * @function getPrimaryKey
     * @return string
     */
    public function getPrimaryKey()
    {
        return implode(self::key_separator, array_intersect_key((array)$this->getArray(), array_flip($this->primary())));
    }

    /**
     * Méthode qui retourne sous forme d'array les données de l'instance
     *
     * @function getArray
     * @return array
     */
    public function getArray()
    {
        $this->load();
        return array_intersect_key(array_merge((array)$this->data, (array)$this->setted), array_flip($this->fields()));
    }

    /**
     * Méthode qui retourne les labels
     *
     * @function getLabel
     * @return string
     */
    public function getLabel()
    {
        $labels = array();
        foreach ((array)self::$labels[$this->table] as $label) {
            $labels[] = $this->$label;
        }
        return implode(' ', $labels);
    }

    /**
     * Méthode qui exécute la requête de suppression
     *
     * @function delete
     * @return bool
     */
    public function delete()
    {
        if ($this->exist()) {
            return parent::delete();
        }
        return false;
    }

    /**
     * Méthode qui retourne la requête d'update
     *
     * @function updateQuery
     * @param bool $all Si a true, tous les champs seront inclus dans la requête et non seulement les champs modifiés
     * @return string
     */
    public function updateQuery($all = false)
    {
        if ($all) {
            $this->setted = array_merge($this->getArray(), (array)$this->setted);
        }
        return parent::updateQuery();
    }

    /**
     * Méthode qui exécute la requête d'update
     *
     * @function update
     * @param bool $all Si a true, tous les champs seront inclus dans la requête et non seulement les champs modifiés
     * @return bool
     */
    public function update($all = false)
    {
        if ($this->exist()) {
            if ($all) {
                $this->setted = array_merge($this->getArray(), (array)$this->setted);
            }
            $retour = parent::update();
            $primarykey = $this->getPrimaryKey();
            $this->reset(true);
            $this->setData($primarykey);
            return $retour;
        }
        return false;
    }

    /**
     * Méthode qui enregistre les modifications, soit par une insertion,
     * soit par un update
     *
     * @function save
     * @param bool $all Si a true, tous les champs seront inclus dans la requête et non seulement les champs modifiés
     * @return bool
     */
    public function save($all = false)
    {
        if ($this->exist()) {
            $this->update($all);
        } else {
            $this->insert($all);
        }
    }

    /**
     * Méthode qui retourne la requête d'insertion
     *
     * @function insertQuery
     * @param bool $all Si a true, tous les champs seront inclus dans la requête et non seulement les champs modifiés
     * @return string
     */
    public function insertQuery($all = false)
    {
        if ($all) {
            $data_array = $this->getArray();
        } else {
            $data_array = $this->getSettedArray();
        }
        $data = $this->prepareValues($data_array);
        $this->addValue($data, 'added', 'NOW()');
        $this->addValue($data, 'hash', 'md5(UUID())');
        $this->addValue($data, 'ip_insert', '"' . user_ip() . '"');
        $this->defaultAddValue($data);
        $fields = implode(', ', array_map('self::addAccent', array_keys($data)));
        $values = implode(', ', array_values($data));
        return "INSERT INTO " . self::getTableToQuery($this->table) . " ($fields) VALUES ($values)";
    }

    /**
     * Méthode qui exécute l'insertion
     *
     * @function insert
     * @param bool $all Si a true, tous les champs seront inclus dans la requête et non seulement les champs modifiés
     * @return bool
     */
    public function insert($all = false)
    {
        $retour = $this->bdd->query($this->insertQuery($all));
        if ($this->hasAutoIncrement() && $id = $this->bdd->insert_id()) {
            $this->reset(true);
            $this->setData($id);
        } else {
            $primarykey = $this->getPrimaryKey();
            $this->reset(true);
            $this->setData($primarykey);
        }
        return $retour;
    }

    /**
     * Méthode alias de insert (deprecated: use insert())
     *
     * @function create
     * @return bool
     */
    public function create()
    {
        return $this->insert();
    }

    /**
     * Méthode qui retourne la clause WHERE
     *
     * @function condition
     * @return string
     */
    public function condition()
    {
        $conditions = array();
        $champs = $this->prepareValuesToCondition((array)$this->where);

        foreach ($champs as $champ => $valeur) {
            $conditions[] = $champ . $valeur;
        }

        if (count($conditions)) {
            return ' WHERE ' . implode(' AND ', $conditions);
        }
    }

    /**
     * Retourne l'array de conditions par défaut à appliquer, si celles-ci sont activées
     *
     * @function defaultConditions
     * @return array
     */
    public function defaultConditions()
    {
        return array_intersect_key(parent::defaultConditions(), array_flip($this->fields()));
    }

    ////////////////////////////////////////////////////////////////////////
    ///////////////////////// INTERFACE: Countable /////////////////////////

    /**
     * Méthode qui retourne le nombre de résultats à la requête
     *
     * @function count
     * @return int
     */
    function count()
    {
        return count($this->desc());
    }

    ////////////////////////////////////////////////////////////////////////
    //////////////////////// INTERFACE: ArrayAccess ////////////////////////

    /**
     * Méthode qui indique si cette position existe dans la liste de résultats
     *
     * @function offsetExists
     * @param int $offset Position
     * @return bool
     */
    public function offsetExists($offset)
    {
        return (bool)(in_array($offset, $this->fields()) || isset(self::$cles[$this->table][$offset]) || (is_int($offset) && $this->field($offset) !== false));
    }

    /**
     * Méthode qui récupère la position
     *
     * @function offsetGet
     * @param int $offset Position
     * @return object
     */
    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    /**
     * Méthode qui modifie la position avec value
     *
     * @function offsetSet
     * @param int $offset Position
     * @param mixed $value Valeur
     */
    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    /**
     * Méthode qui supprime la position avec value
     * TODO : pour le moment cela ne fait rien
     *
     * @function offsetUnset
     * @param int $offset Position
     */
    public function offsetUnset($offset)
    {
        $this->ressource()->data_seek($offset);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////// INTERFACE: Iterator /////////////////////////

    /**
     * @var int $position Position de l'offset
     */
    private $position = 0;

    /**
     * Méthode qui réinitialise la position
     *
     * @function rewind
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Méthode qui retourne la position actuelle
     *
     * @function current
     * @return array
     */
    public function current()
    {
        return $this->{$this->field($this->position)};
    }

    /**
     * Méthode qui retourne la position
     *
     * @function key
     * @return int
     */
    public function key()
    {
        return $this->field($this->position);
    }

    /**
     * Méthode qui avance d'une position
     *
     * @function next
     */
    public function next()
    {
        $this->position++;
    }

    /**
     * Méthode qui indique si la position courrante existe
     *
     * @function valid
     * @return bool
     */
    public function valid()
    {
        return $this->offsetExists($this->position);
    }

    ////////////////////////////////////////////////////////////////////////
    //////////////////////////// AUTRES INTERFACE //////////////////////////

    /**
     * Méthode magique SLEEP
     * Appelée avant toute linéarisation
     *
     * @function __sleep
     * @return array
     */
    public function __sleep()
    {
        return array('data');
    }

    /**
     * Méthode qui retourne sous forme d'array les données de l'instance
     *
     * @function __debugInfo
     * @return array
     */
    public function __debugInfo()
    {
        return $this->getArray();
    }

    /**
     * Méthode magique SET_STATE
     * Utilisée pour le var_export
     *
     * @function __set_state
     * @param array $array Tableau de données
     */
    public static function __set_state($array)
    {
        self::setData($array);
    }

    /**
     * Méthode appelée lors d'un cast string de l'objet:
     * La valeur retournée sera la même que getLabel()
     *
     * @function __toString
     * @return string
     */
    public function __toString()
    {
        return $this->getLabel();
    }

}

/**
 * La classe noResults permet d'avoir des réponses même si on a pas de résultats.
 *
 * @class       noResults
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class noResults implements \Countable, \ArrayAccess, \Iterator, \SeekableIterator
{
    /**
     * Méthode appelée lors d'un cast string de l'objet:
     * La valeur retournée sera vide
     *
     * @function __toString
     * @return string
     */
    public function __toString()
    {
        return '';
    }

    /**
     * Méthode magique GET
     *
     * @function __get
     * @param string $name Nom de l'attribut
     * @return \data|\class
     */
    public function __get($name)
    {
        return $this;
    }

    /**
     * Méthode magique SET
     *
     * @function __set
     * @param string $name Nom de l'attribut
     * @param string $value Valeur de l'attribut
     */
    public function __set($name, $value)
    {
    }

    /**
     * Méthode magique CALL
     *
     * @function __call
     * @param string $name Nom de la fonction
     * @param array $arguments Tableau des arguments de la fonctions
     * @return \data|\class
     */
    public function __call($name, $arguments)
    {
        return $this;
    }

    /**
     * Méthode magique SLEEP
     *
     * @function __sleep
     */
    public function __sleep()
    {
    }

    /**
     * Méthode magique ISSET
     *
     * @function __isset
     * @param string $name Nom de l'attribut
     * @return bool
     */
    public function __isset($name)
    {
        return false;
    }

    /**
     * Méthode qui retourne le nombre de résultats à la requête
     *
     * @function count
     * @return int
     */
    public function count()
    {
        return 0;
    }

    /**
     * Méthode qui retourne la position actuelle
     *
     * @function current
     * @return \data|\class
     */
    public function current()
    {
        return $this;
    }

    /**
     * Méthode qui avance d'une position
     *
     * @function next
     */
    public function next()
    {
    }

    /**
     * Méthode qui retourne la position
     *
     * @function key
     * @return int
     */
    public function key()
    {
        return 0;
    }

    /**
     * Méthode qui indique si la position courrante existe
     *
     * @function valid
     * @return bool
     */
    public function valid()
    {
        return false;
    }

    /**
     * Méthode qui réinitialise la position
     *
     * @function rewind
     */
    public function rewind()
    {
    }

    /**
     * Méthode qui recherche une position
     *
     * @function seek
     * @param int $position Position à atteindre
     */
    public function seek($position)
    {
    }

    /**
     * Méthode qui indique si cette position existe dans la liste de résultats
     *
     * @function offsetExists
     * @param int $offset Position
     * @return bool
     */
    public function offsetExists($offset)
    {
        return true;
    }

    /**
     * Méthode qui récupère la position
     *
     * @function offsetGet
     * @param int $offset Position
     * @return object
     */
    public function offsetGet($offset)
    {
        return $this;
    }

    /**
     * Méthode qui modifie la position avec value
     *
     * @function offsetSet
     * @param int $offset Position
     * @param mixed $value Valeur
     */
    public function offsetSet($offset, $value)
    {
    }

    /**
     * Méthode qui supprime la position avec value
     *
     * @function offsetUnset
     * @param int $offset Position
     */
    public function offsetUnset($offset)
    {
    }

}
