<?php

namespace o;

/**
 * La classe data permet de représenter des ensembles de données, c'est-à-dire, le contenu d'une table,
 * d'une portion de table ou d'une jointure.
 *
 * @class       data
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class data extends dat implements \Countable, \ArrayAccess, \Iterator, \SeekableIterator
{
    /**
     * @var string $column Colonne sélectionnée
     */
    protected $column;
    /**
     * @var string $champs Champs à récupérer
     */
    protected $champs = "*";
    /**
     * @var array $order Tableau d'ordonnancement
     */
    protected $order;
    /**
     * @var array $limit Tableau des limit
     */
    protected $limit = array('limit' => null, 'offset' => 0);
    /**
     * @var array $join Tableau des jointures
     */
    protected $join;
    /**
     * @var array $join_names Tableau des noms de jointures
     */
    protected $join_names;
    /**
     * @var array $join_setted_alias Tableau des alias de jointures
     */
    protected $join_setted_alias = array();
    /**
     * @var array $join_conditions Tableau des contiditions à appliquer sur les jointures
     */
    protected $join_conditions = array();
    /**
     * @var array $join_tables Tableau des tables de jointures
     */
    protected $join_tables;
    /**
     * @var array $group_by Tableau des groupements
     */
    protected $group_by;
    /**
     * @var string $having Tableau des having
     */
    protected $having;
    /**
     * @var array $fetch_fields Tableau des id des champs dans le résultat de la requête par table.
     * Lors d'une jointure la fonction load_fields() est appelée depuis ressource() pour remplir cette variable
     */
    protected $fetch_fields;
    /**
     * @var string $addWhere Condition addition au format sql à définir avec addWhere()
     */
    public $addWhere;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param string $table
     * @param array|null $where
     */
    public function __construct($table, $where = null)
    {
        $this->table = $table;
        parent::__construct();
        $this->where($where);
    }

    /**
     * Méthode qui récupère une instance de la table courante
     *
     * @function get
     * @param array $args via func_get_args()
     * @return object  \o\class
     */
    public function get()
    {
        $class = __NAMESPACE__ . '\\' . $this->table;
        return new $class(func_get_args());
    }

    /**
     * Méthode magique GET
     *
     * @function __get
     * @param string $name Nom de l'attribut
     * @return \data|\class
     */
    public function __get($name)
    {
        $name = $this->applyShortcuts($name);
        // S'il s'agit d'une suite (parent->template->name)
        $names = explode(self::join_separator, $name);
        if (count($names) > 1) {
            $cur = $this;
            foreach ($names as $name) {
                if (substr($name, -2, 2) == '()') {
                    $func = substr($name, 0, -2);
                    $cur = $cur->$func();
                } else {
                    $cur = $cur->$name;
                    if ($cur === null) {
                        break;
                    }
                }
            }
            return $cur;
        }

        // Si c'est une variable setted
        if (isset($this->setted[$name])) {
            return $this->setted[$name];
        }

        // S'il s'agit d'une colonne de la table
        if (in_array($name, array_column($this->desc(), 'Field'))) {
            $column = clone $this;
            return $column->setColumn($name);
        }

        // S'il s'agit d'une clé étrangère
        if (isset(self::$cles[$this->table][$name])) {
            $cle = self::$cles[$this->table][$name];
            $data = array();
            // SI Many -> One
            if ($cle['create_type'] == 'one') {
                // On copie la valeur des clés étrangère
                foreach ($cle['keys'] as $one => $many) {
                    $data[$one] = array_unique($this->$many->getArray());
                }
                $this->setted[$name] = new data($cle['table'], $data);
                return $this->setted[$name];
            } // SI One -> Many
            else {
                // On copie la valeur des clés étrangère
                foreach ($cle['keys'] as $one => $many) {
                    $data[$many] = array_unique($this->$one->getArray());
                }
                $this->setted[$name] = new data($cle['table'], $data);
                return $this->setted[$name];
            }
        }

        // S'il s'agit d'une fonction
        if (substr($name, -2, 2) == '()') {
            $func = substr($name, 0, -2);
            return $this->$func();
        }
        // Sinon NULL
        $backtrace = current(debug_backtrace());
        trigger_error("Undefined property: " . __NAMESPACE__ . "\\data(" . $this->table . ")::" . $name . " in " . $backtrace['file'] . " on line " . $backtrace['line'] . ". Triggered ", E_USER_NOTICE);
    }

    /**
     * METHODE MAGIQUE: Permet d'appeler une fonction statique d'une classe fille de instance depuis une instance de o\data.
     *
     * //////////////////////// EXEMPLE ////////////////////////
     * // Dans data/ma_table.o.php:
     * namespace o;
     * class ma_table extends instance{
     *  public static function ma_methode($that,$params){
     *      $that -> contient le data sur lequel on a appliqué la fonction
     *      $params -> contient un array avec les parametres passés à la fonction
     *  }
     * }
     * // N'importe où:
     * $mon_data = new o\data('ma_table');
     * $resultat = $mon_data->ma_methode('param1','param2');
     * /////////////////////////////////////////////////////////
     *
     * @function __call
     * @param string $name
     * @param array $arguments
     */
    public function __call($name, $arguments)
    {
        $class = __NAMESPACE__ . '\\' . $this->table;
        return $class::$name($this, $arguments);
    }

    /**
     * Défini la colonne que l'on veut récupérer
     *
     * @function setColumn
     * @param string $column
     * @return \o\data
     */
    private function setColumn($column)
    {
        $this->column = $column;
        return $this;
    }

    /**
     * Permet de récupérer une colonne de façon DISTINCT
     *
     * @function distinct
     * @return \o\data
     */
    public function distinct()
    {
        if (isset($this->column)) {
            $new = clone $this;
            $new->champs = "DISTINCT " . $this->column;
            $new->reset(true);
            return $new;
        }
        return $this;
    }

    /**
     * Méthode qui réalise les jointures nécéssaires au vue des conditions (appelé par query())
     *
     * @function prepareCondition
     */
    protected function prepareCondition()
    {
        foreach ((array)array_merge($this->defaultConditions(), (array)$this->where) as $f => $v) {
            if (strpos($f, self::join_separator) !== false) {
                $joins = explode(self::join_separator, $f);
                $accesseur = array_pop($joins);
                $parent_join = implode(self::join_separator, $joins);
                if (isset(self::$cles[$this->table][reset($joins)])) {
                    if (!isset($this->join_names[$parent_join])) {
                        $this->join($parent_join);
                    }
                }
            }
        }
    }

    /**
     * Méthode qui retourne la clause WHERE
     *
     * @function condition
     * @return string|null
     */
    public function condition()
    {
        $champs = array();
        //On récupère les conditions
        $conditions = array_merge($this->defaultConditions(), (array)$this->where);
        foreach ($conditions as $champ => $valeur) {
            //On obtient la table sur laquelle s'applique la condition
            $chemin = explode(self::join_separator, $champ);
            if (count($chemin) > 1) {
                $champ = array_pop($chemin);
                $table = implode(self::join_separator, $chemin);
            } else {
                $table = '';
            }
            //On élimine les champs appartenant à des tables dont la jointure n'est pas faites
            if (!isset($this->join_names[$table])) {
                continue;
            }
            //On récupère le nom de la table physique
            $table_name = $this->join_tables[$table];
            //S'il s'agit d'un champ
            if (in_array($champ, $this->fieldsOf($table_name))) {
                $champs[] = array(
                    'table'  => $table,
                    'champ'  => $champ,
                    'valeur' => $this->prepareValueToCondition($valeur),
                );
            } //S'il s'agit d'un accesseur
            elseif (in_array($champ, array_keys((array)self::$cles[$table_name])) && self::$cles[$table_name][$champ]['create_type'] == 'one') {
                $cle = self::$cles[$table_name][$champ];
                $valeur = is_string($valeur) ? explode(self::key_separator, $valeur) : $valeur;
                $valeur = is_object($valeur) ? $valeur : (array)$valeur;
                $indx = 0;
                foreach ($cle['keys'] as $value_field => $condition_field) {
                    $indx++;
                    if (isset($valeur[$value_field]) || isset($valeur[$indx])) {
                        $champs[] = array(
                            'table'  => $table,
                            'champ'  => $condition_field,
                            'valeur' => $this->prepareValueToCondition(isset($valeur[$value_field]) ? $valeur[$value_field] : $valeur[$indx]),
                        );
                    }
                }
            }
        }

        $conditions = array();
        //On déclare les conditions
        foreach ($champs as $champ) {
            $conditions[] = $this->join_names[$champ['table']] . '.' . $champ['champ'] . $champ['valeur'];
        }

        //On ajoute les conditions spécifiques si nécéssaire
        if (isset($this->addWhere)) {
            $conditions[] = $this->addWhere;
        }

        //Enfin, si on a des conditions on construit la clause WHERE et on la retourne
        if (count($conditions)) {
            return ' WHERE ' . implode(' AND ', $conditions);
        }

        return null;
    }

    /**
     * Méthode qui retourne la ressource (execute la requête si nécéssaire)
     *
     * @function ressource
     * @return ressource Ressource mysqli_result
     */
    protected function ressource()
    {
        $ressource = parent::ressource();
        if ($ressource !== null && $this->join !== null) {
            $this->load_fields();
        }

        return $ressource;
    }

    /**
     * Méthode qui charge dans la variable $this->fetch_fields les id des champs
     * dans le résultat de la requête par table
     *
     * @function load_fields
     */
    private function load_fields()
    {
        $fetch_fields = array_map('get_object_vars', $this->ressource->ressource->fetch_fields());
        $orgnames = array_column($fetch_fields, 'orgname');
        $tables = array_column($fetch_fields, 'table');
        foreach ($this->join_names as $cle => $jointure) {
            $this->fetch_fields[$cle] = array_intersect_key($orgnames, array_flip(array_keys($tables, $jointure)));
        }
    }

    /**
     * Méthode qui retourne la requête SQL permettant la récupération des
     * données de l'objet
     *
     * @function query
     * @return string
     */
    public function query()
    {
        $champs = $this->champs;
        $this->prepareCondition();
        $join = $this->joinStatement();
        $where = $this->condition();
        $order = $this->orderStatement();
        $group_by = $this->groupByStatement();

        return "SELECT $champs FROM " . self::getTableToQuery($this->table) . $join . $where . $group_by . $order;
    }

    /**
     * Méthode qui retourne la requête delete
     *
     * @function deleteQuery
     * @return string
     */
    public function deleteQuery()
    {
        $table = self::getTableToQuery($this->table);
        $this->prepareCondition();
        $join = $this->joinStatement();
        $where = $this->condition();

        return "DELETE $table FROM $table $join $where";
    }

    /**
     * Méthode qui ajoute des conditions, écrase les ancienne si nécéssaire
     *
     * @function where
     * @param string/array $field Soit un array contenant la liste des champs=>valeurs, soit le nom du champ
     * @param mixed $value Valeur à appliquer à la condition (ne sert que si field est une string)
     * @return $this
     */
    public function where($field, $value = null)
    {
        if (!is_array($field)) {
            $field = array($field => $value);
        }
        $field = $this->applyShortcutsToKeys($field);
        $this->where = array_merge((array)$this->where, $field);

        $this->reset(true);

        return $this;
    }

    /**
     * Permet d'ajouter une condition au format SQL à la condition déjà définie
     *
     * @function addWhere
     * @param string $sql
     * @param bool $multiple Si true indique que la condition doit s'ajouter au précédentes (optionnel, false par défaut)
     * @return \o\data
     */
    public function addWhere($sql, $multiple = false)
    {
        return $this->concatSQLCondition($this->addWhere, $sql, $multiple);
    }

    /**
     * Construit ou complète la variable $var. Utilisé dans addWhere() et having()
     *
     * @function concatSQLCondition
     * @param string $var Variable que l'on veut modifier
     * @param string $sql Condition SQL à ajouter
     * @param bool $multiple Si true indique que la condition doit s'ajouter au précédentes (optionnel, false par défaut)
     * @return \o\data
     */
    private function concatSQLCondition(&$var, $sql, $multiple = false)
    {
        if ($sql === null) {
            return $this;
        }
        if ($var !== null && $multiple) {
            $var = $var . " AND (" . $sql . ")";
        } else {
            $var = "(" . $sql . ")";
        }
        $this->reset(true);

        return $this;
    }

    /**
     * Méthode qui ajoute une jointure si elle existe
     *
     * @function join
     * @param string $name Nom de la clé étrangère à utiliser pour la jointure, possibiliter de réaliser
     * des jointures multiple et intercalant le join_separator entre les noms
     * @param string $alias Permet de spécifier un alias manuellement. NULL sinon.
     * @param array $conditions Tableau des conditions à appliquer sur les jointures
     * @return $this
     */
    public function join($name, $alias = null, $conditions = array())
    {
        $name = $this->applyShortcuts($name);
        $table = $this->table;
        $jointure_name = '';
        foreach (explode(self::join_separator, $name) as $jointure) {
            if (isset(self::$cles[$table][$jointure])) {
                $jointure_name .= $jointure;
                $table = self::$cles[$table][$jointure]['table'];
                if (!in_array($jointure_name, (array)$this->join)) {
                    $this->join[] = $jointure_name;
                }
                $this->join_conditions[$jointure_name] = array_merge(
                    (array)$this->join_conditions[$jointure_name],
                    $this->filterJoinConditions($conditions, $jointure_name, $table)
                );
                $jointure_name .= self::join_separator;
                $this->reset(true);
            } else {
                break;
            }
        }
        if ($alias !== null && !in_array($alias, $this->join_setted_alias)) {
            $this->join_setted_alias[$name] = $alias;
        }
        $this->reset(true);

        return $this;
    }

    /**
     * Méthode qui filtre les conditions en fonction de la base et la table
     *
     * @function filterJoinConditions
     * @param array $conditions Tableau des conditions
     * @param string $base Base qui serai necéssaire et retiré au nom des clées du tableau des conditions
     * @param string $table Table sur laquelles sera vérifié l'existance du champ
     * @return Generator
     */
    protected function filterJoinConditions(&$conditions, $base, $table)
    {
        $filtered_conditions = array();
        foreach ($conditions as $field => $value) {
            if ($base == '' || strpos($field, $base) === 0) {
                $field = str_replace(self::join_separator, '', substr($field, strlen($base)));
                if (in_array($field, $this->fieldsOf($table))) {
                    $filtered_conditions[$field] = $value;
                }
            }
        }
        return $filtered_conditions;
    }

    /**
     * Méthode qui retourne le statement correspondant aux jointures
     *
     * @function joinStatement
     * @return string
     */
    protected function joinStatement()
    {
        $join = '';
        $this->join_names = array('' => $this->table);
        $this->join_tables = array('' => $this->table);

        // Gestion des Jointures
        foreach ((array)$this->join as $jointure_name) {
            // Initializasition de variable
            $joins = explode(self::join_separator, $jointure_name);
            $accesseur = array_pop($joins);
            $parent_join = implode(self::join_separator, $joins);
            $jointure = self::$cles[$this->join_tables[$parent_join]][$accesseur];
            $this->join_tables[$jointure_name] = $jointure['table'];
            $link = array();
            $alias = '';

            // Gestion de l'alias
            //Si on a spécifié un alias lors de la déclaration de la jointure
            if (isset($this->join_setted_alias[$jointure_name])) {
                // On définie comme alias le nom de la jointure
                $jointure['table_alias'] = $this->join_setted_alias[$jointure_name];
                // et on le déclare
                $alias = ' as ' . $jointure['table_alias'];
            } // Si le nom de la table a déjà été utilisé mais pas celui de la jointure
            elseif (in_array($jointure['table'], $this->join_names) && !in_array($accesseur, $this->join_names)) {
                // On définie comme alias le nom de la jointure
                $jointure['table_alias'] = $accesseur;
                // et on le déclare
                $alias = ' as ' . $jointure['table_alias'];
            } // Si le nom de la table et celui de la jointure ont déjà été utilisés
            elseif (in_array($jointure['table'], $this->join_names)) {
                // On définie comme alias le nom de la jointure + un id unique
                $jointure['table_alias'] = $accesseur . uniqid();
                // et on le déclare
                $alias = ' as ' . $jointure['table_alias'];
            } // Sinon, si le nom de la table n'a pas encore été utilisé
            else {
                // On définie comme alias son propre nom et pas besoin de le déclarer
                $jointure['table_alias'] = $jointure['table'];
            }

            // On ajoute cet alias à la liste des alias utilisés
            $this->join_names[$jointure_name] = $jointure['table_alias'];

            // Gestion des clés
            // Ordonne les clés si nécéssaire
            if ($jointure['create_type'] == 'many') {
                $jointure['keys'] = array_flip($jointure['keys']);
            }

            // Pour chaque clé
            foreach ($jointure['keys'] as $cle_dest => $cle_orig) {
                // On crée la condition qui sera incluse dans le ON de la jointure
                $link[] = $this->join_names[$parent_join] . '.' . $cle_orig . ' = ' . $jointure['table_alias'] . '.' . $cle_dest;
            }
            foreach ($this->join_conditions[$jointure_name] as $champ => $valeur) {
                $link[] = $jointure['table_alias'] . '.' . $champ . $this->prepareValueToCondition($valeur);
            }

            // Déclaration de la jointure
            $join .= ' LEFT JOIN ' . self::getTableToQuery($jointure['table']) . $alias . ' ON ' . implode(' AND ', $link);
        }
        return $join;
    }

    /**
     * Méthode qui permet de récupérer un générateur de la liste mélangée
     *
     * @function shuffle
     * @return Generator
     */
    public function shuffle()
    {
        $order = range(0, $this->countAll() - 1);
        shuffle($order);
        foreach (array_slice($order, 0, $this->count()) as $position) {
            yield $position => $this[$position];
        }
    }

    /**
     * Méthode qui créé le tableau d'ordonnancement
     * Les paramètres de cette fonction sont analysés par groupes de 3
     *
     * @function order
     * @param string Nom du champ sur lequel on veut baser l'ordonnance
     * @param string/array 'ASC' ou 'DESC' ou un Array pour faire un ORDER BY FIELD() (optionnel)
     * @return $this
     */
    public function order()
    {
        $args = func_get_args();
        $this->order = array();
        //Si les paramètres ont un format compatible avec le nouveaux paramètrages
        if (count($args) < 3) {
            $orders = $args[0];
            if (!is_array($orders)) {
                $orders = array($args[0] => $args[1]);
            }
            $orders = $this->applyShortcutsToKeys($orders);
            foreach ($orders as $field => $value) {
                $chemin = explode(self::join_separator, $field);
                if (count($chemin) > 1) {
                    $field = array_pop($chemin);
                    $table = implode(self::join_separator, $chemin);
                    $this->join($table);
                } else {
                    $table = '';
                }
                //On récupère le nom de la table physique
                $table_name = $this->getJoinedTableName($table);
                //S'il s'agit d'un champ
                if (in_array($field, $this->fieldsOf($table_name))) {
                    $this->order[] = [$field, $value, $table];
                } //S'il s'agit d'un accesseur
                elseif (in_array($field, array_keys((array)self::$cles[$table_name])) && self::$cles[$table_name][$field]['create_type'] == 'one') {
                    foreach (array_values(self::$cles[$table_name][$field]['keys']) as $fld) {
                        $this->order[] = [$fld, $value, $table];
                    }
                } else {
                    $this->order[] = [$field, $value, $table];
                }
            }
        } //Sinon on fait comme avant /!\ Déprécié
        else {
            trigger_error('Cette façon de spécifier l\'ordonnancement est dépréciée et fortement déconseillée.', E_USER_NOTICE);
            foreach (array_chunk($args, 3) as $order) {
                if (isset($order[0])) {
                    if (isset($order[2]) && !isset($this->join_names[$order[2]])) {
                        $this->join($order[2]);
                    }
                    $this->order[] = $order;
                }
            }
        }
        $this->reset(true);
        return $this;
    }

    /**
     * Méthode qui créé la chaîne d'ordonnancement
     *
     * @function orderStatement
     * @return string
     */
    protected function orderStatement()
    {
        $orders = array();
        foreach ((array)$this->order as $order) {
            $cle = ((isset($order[2]) && isset($this->join_names[$order[2]])) ? $order[2] : '');
            if (in_array($order[0], $this->fieldsOf($this->join_tables[$cle]))) {
                if (is_array($order[1])) {
                    $orders[] = 'FIELD(' . $this->join_names[$cle] . '.' . $order[0] . ', ' . implode(', ', $order[1]) . ')';
                } else {
                    $orders[] = $this->join_names[$cle] . '.' . $order[0] . ' ' . (isset($order[1]) ? $order[1] : 'ASC');
                }
            } else {
                $orders[] = $order[0] . ' ' . (isset($order[1]) ? $order[1] : 'ASC');
            }
        }
        if (count($orders)) {
            return " ORDER BY " . implode(', ', $orders);
        }
    }

    /**
     * Méthode qui créé le tableau de groupement
     *
     * @function groupBy
     * @param string (n) Champ à grouper
     * @return $this
     */
    public function groupBy()
    {
        $this->group_by = array();
        foreach ($this->applyShortcuts(func_get_args()) as $arg) {
            $joins = explode(self::join_separator, $arg);
            $field = array_pop($joins);
            $table = implode(self::join_separator, $joins);
            $this->join($parent_join);
            //On récupère le nom de la table physique
            $table_name = $this->getJoinedTableName($table);
            //S'il s'agit d'un champ
            if (in_array($field, $this->fieldsOf($table_name))) {
                $this->group_by[] = [$field, $table];
            } //S'il s'agit d'un accesseur
            elseif (in_array($field, array_keys((array)self::$cles[$table_name])) && self::$cles[$table_name][$field]['create_type'] == 'one') {
                foreach (array_values(self::$cles[$table_name][$field]['keys']) as $fld) {
                    $this->group_by[] = [$fld, $table];
                }
            }
        }
        $this->reset(true);

        return $this;
    }

    /**
     * Permet d'ajouter un HAVING au format SQL
     *
     * @function having
     * @param string $sql
     * @param bool $multiple Si true indique que la condition doit s'ajouter au précédentes (optionnel, false par défaut)
     * @return \o\data
     */
    public function having($sql, $multiple = false)
    {
        return $this->concatSQLCondition($this->having, $sql, $multiple);
    }

    /**
     * Supprime le contenu du HAVING
     *
     * @function resetHaving
     * @return $this
     */
    public function resetHaving()
    {
        $this->having = null;

        return $this;
    }

    /**
     * Méthode qui créé la chaîne de groupement
     *
     * @function groupByStatement
     * @function orderStatement
     * @return string
     */
    public function groupByStatement()
    {
        $having = "";
        if ($this->having !== null) {
            $having = " HAVING " . $this->having;
        }
        if ($this->group_by !== null && count($this->group_by) > 0) {
            $fields = array();
            foreach ($this->group_by as $arg) {
                $fields[] = $this->join_names[$arg[1]] . '.' . $arg[0];
            }
            return " GROUP BY " . implode(', ', $fields) . $having;
        }
        return '';
    }

    /**
     * Méthode qui active l'agrégation SUM sur le champ passé en paramètre
     *
     * @function sum
     * @param string $champ
     * @return $this
     */
    public function sum($champ)
    {
        $this->champs = "*, sum($champ) as $champ";
        $this->reset(true);

        return $this;
    }

    /**
     * Méthode qui active l'agrégation AVG sur le champ passé en paramètre
     *
     * @function avg
     * @param string $champ
     * @return $this
     */
    public function avg($champ)
    {
        $this->champs = "*, avg($champ) as $champ";
        $this->reset(true);

        return $this;
    }

    /**
     * Méthode qui active l'agrégation MAX sur le champ passé en paramètre
     *
     * @function max
     * @param string $champ
     * @return $this
     */
    public function max($champ)
    {
        $this->champs = "*, max($champ) as $champ";
        $this->reset(true);

        return $this;
    }

    /**
     * Méthode qui active l'agrégation MIN sur le champ passé en paramètre
     *
     * @function min
     * @param string $champ
     * @return $this
     */
    public function min($champ)
    {
        $this->champs = "*, min($champ) as $champ";
        $this->reset(true);

        return $this;
    }

    /**
     * Méthode qui active l'agrégation COUNT sur le champ passé en paramètre
     *
     * @function sqlCount
     * @param string $champ
     * @return $this
     */
    public function sqlCount($champ)
    {
        $this->champs = "*, count($champ) as $champ";
        $this->reset(true);

        return $this;
    }

    /**
     * Méthode qui défini/initialise les limites des data.
     * Valeurs négatives acceptés, se base sur la logique de substr (ordre des paramètres inversé).
     *
     * @function limit
     * @param int $limit Nombre limit de résultats
     * @param int $offset Position de l'offset
     * @return $this
     */
    public function limit($limit = null, $offset = 0)
    {
        $countAll = $this->countAll();
        $offset = intval($offset);

        if ($offset < 0) {
            $offset = max($offset + $countAll, 0);
        }

        if ($limit !== null) {
            $limit = intval($limit);
            if ($limit < 0) {
                $limit = max($countAll - $offset + $limit, 0);
            }
        }

        $this->limit = compact('limit', 'offset');

        return $this;
    }

    /**
     * Méthode qui retourne l'instance courante de la ressource
     *
     * @function getInstance
     * @return object
     */
    protected function getInstance()
    {
        $class = __NAMESPACE__ . '\\' . $this->table;

        if ($this->column !== null) {
            return $this->ressource()->fetch_assoc()[$this->column];
        } elseif ($this->join === null) {
            return new $class($this->ressource()->fetch_assoc());
        } else {
            $data = $this->ressource()->fetch_array();
            $instance = new $class(array_combine($this->fetch_fields[''], array_intersect_key($data, $this->fetch_fields[''])));
            foreach ($this->join as $jointure) {
                $to_set = $instance;
                $chaine = explode(self::join_separator, $jointure);
                $attr_to_set = array_pop($chaine);
                foreach ($chaine as $maillon) {
                    $to_set = $to_set->$maillon;
                }
                $joinClass = __NAMESPACE__ . '\\' . $this->join_tables[$jointure];
                $to_set->$attr_to_set = new $joinClass(array_combine($this->fetch_fields[$jointure], array_intersect_key($data, $this->fetch_fields[$jointure])));
            }

            return $instance;
        }
    }

    /**
     * Méthode qui retourne les labels
     *
     * @function getLabel
     * @return string
     */
    public function getLabel()
    {
        $labels = array();
        foreach ($this as $item) {
            $labels[] = $item->getLabel();
        }
        return implode(', ', $labels);
    }

    /**
     * Méthode qui retourne la liste d'instance sour forme d'array
     *
     * @return array*o\instance
     */
    public function getArray()
    {
        $array = array();
        foreach ($this as $item) {
            $array[] = $item;
        }
        return $array;
    }

    /**
     * Méthode qui retourne la liste sous forme d'array bi-dimmensionnel
     *
     * @return array^2
     */
    public function getFullArray()
    {
        $array = array();
        foreach ($this as $item) {
            $array[] = $item->getArray();
        }
        return $array;
    }

    ////////////////////////////////////////////////////////////////////////
    ///////////////////////// INTERFACE: Countable /////////////////////////

    /**
     * Méthode qui retourne le nombre de résultats en tenant compte des limites
     *
     * @function count
     * @return int
     */
    public function count()
    {
        if ($this->limit['limit'] !== null) {
            return min($this->countAll() - $this->limit['offset'], $this->limit['limit']);
        }
        return $this->countAll() - $this->limit['offset'];
    }

    /**
     * Méthode qui retourne le nombre de résultats à la requête
     *
     * @function countAll
     * @return int
     */
    public function countAll()
    {
        return $this->ressource()->num_rows;
    }

    ////////////////////////////////////////////////////////////////////////
    //////////////////////// INTERFACE: ArrayAccess ////////////////////////

    /**
     * Méthode qui indique si cette position existe dans la liste de résultats
     *
     * @function offsetExists
     * @param int $offset Position
     * @return bool
     */
    public function offsetExists($offset)
    {
        return $offset > -1 && $offset < $this->count();
    }

    /**
     * Méthode qui récupère la position
     *
     * @function offsetGet
     * @param int $offset Position
     * @return object
     */
    public function offsetGet($offset)
    {
        try {
            $this->seek($offset);
        } catch (\Exception $e) {
            return null;
        }
        return $this->getInstance();
    }

    /**
     * Méthode qui modifie la position avec value
     * TODO : pour le moment cela ne fait rien
     *
     * @function offsetSet
     * @param int $offset Position
     * @param mixed $value Valeur
     */
    public function offsetSet($offset, $value)
    {
        $this->seek($offset);
        $this->value = $value;
    }

    /**
     * Méthode qui supprime la position avec value
     * TODO : pour le moment cela ne fait rien
     *
     * @function offsetUnset
     * @param int $offset Position
     */
    public function offsetUnset($offset)
    {
        $this->seek($offset);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////////// INTERFACE: Iterator /////////////////////////

    /**
     * @var int $position Position de l'offset
     */
    private $position = 0;

    /**
     * Méthode qui réinitialise la position
     *
     * @function rewind
     */
    public function rewind()
    {
        $this->privateSeek(0);
    }

    /**
     * Méthode qui retourne la position actuelle
     *
     * @function current
     * @return object
     */
    public function current()
    {
        $data = $this->getInstance();
        $this->privateSeek($this->position);
        return $data;
    }

    /**
     * Méthode qui retourne la position
     *
     * @function key
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Méthode qui avance d'une position
     *
     * @function next
     */
    public function next()
    {
        $this->position++;
        $this->privateSeek($this->position);
    }

    /**
     * Méthode qui indique si la position courrante existe
     *
     * @function valid
     * @return bool
     */
    public function valid()
    {
        return $this->offsetExists($this->position);
    }

    ////////////////////////////////////////////////////////////////////////
    ////////////////////// INTERFACE: SeekableIterator /////////////////////

    /**
     * Méthode qui recherche une position
     *
     * @function seek
     * @param int $position Position à atteindre
     */
    public function seek($position)
    {
        if ($position > -1 && $position < $this->count()) {
            return $this->privateSeek($position);
        } else {
            throw new \OutOfBoundsException();
        }
    }

    /**
     * Méthode qui recherche une la position de l'élément
     *
     * @function privateSeek
     * @param int $position Position à atteindre
     */
    private function privateSeek($position)
    {
        $this->position = $position;
        return $this->ressource()->data_seek($position + $this->limit['offset']);
    }

}
