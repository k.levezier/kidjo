<?php

// Chargement des classes du core
require_once(__DIR__ . '/dispatcher.class.php');
require_once(__DIR__ . '/controller.class.php');
require_once(__DIR__ . '/command.class.php');
require_once(__DIR__ . '/bdd.class.php');

// Classes
require_once(__DIR__ . '/../apps/common.class.php');
require_once(__DIR__ . '/../override/apps/common.class.php');

// Fonctions
require_once('../../librairies/functions.class.php');

// Patchs
require_once(__DIR__ . '/patches.php');

// Octeract
require_once(__DIR__ . '/octeract/dat.php');
require_once(__DIR__ . '/octeract/data.php');
require_once(__DIR__ . '/octeract/instance.php');
