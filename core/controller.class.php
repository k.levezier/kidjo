<?php

/**
 * La classe Controller est la base de tout.
 *
 * @class       Controller
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class Controller
{
    /**
     * @var bdd $bdd Objet de la BDD
     * @var Command $Command Objet Command
     * @var array $Config Tableau de configuration
     * @var string $App Identifiant de l'application
     * @var boolean $activateCSSmin Minification des CSS
     * @var boolean $activateJSmin Minification des JS
     * @var integer $currentVersion Chemin de l'environnement
     * @var array $included_js Tableau des JS
     * @var array $included_css Tableau des CSS
     * @var boolean $autoFireHead Affichage du Head
     * @var boolean $autoFireHeader Affichage du Header
     * @var boolean $autoFireView Affichage de la vue
     * @var boolean $autoFireFooter Affichage du footer
     * @var boolean $autoFireDebug Affichage du debug
     * @var boolean $autoFireNothing Affiche de rien du tout
     * @var string $head Nom du Head
     * @var string $header Nom du Header
     * @var string $footer Nom du Footer
     * @var string $view Nom de la Vue
     */
    private $bdd;
    private $Command;
    private $Config;
    private $App;
    private $activateCSSmin;
    private $activateJSmin;
    private $currentVersion;
    private $included_js;
    private $included_css;
    private $autoFireHead = true;
    private $autoFireHeader = true;
    private $autoFireView = true;
    private $autoFireFooter = true;
    private $autoFireDebug = true;
    private $autoFireNothing = false;
    private $head;
    private $header;
    private $footer;
    private $view;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param Command $command Objet Command
     * @param array $config Tableau de la configuration
     * @param string $app Identifiant de l'application
     */
    public function __construct($command, $config, $app)
    {
        $this->killSessions((isset($_POST['killSessions']) ? $_POST['killSessions'] : ''));
        $this->bdd = bdd::get_instance();
        $this->Command = $command;
        $this->Config = $config;
        $this->App = $app;
        $this->head = 'head';
        $this->header = 'header';
        $this->footer = 'footer';
        $this->view = 'default';
        $this->activateCSSmin = $this->Config['params'][$this->App]['cssmin'];
        $this->activateJSmin = $this->Config['params'][$this->App]['jsmin'];
        $this->currentVersion = $this->Config['params'][$this->App]['currentVersion'];
        $this->language = $this->Command->Language;
        $this->lLangues = $this->Config['multilanguage']['allowed_languages'];
        $this->lLocales = $this->Config['multilanguage']['allowed_locale'];
        $this->current_controller = $this->Command->Name;
        $this->current_function = $this->Command->Function;
        $this->path = $this->Config['path'][$this->Config['env']];
        $this->spath = $this->Config['user_path'][$this->Config['env']];
        $_SESSION['spath'] = $this->spath;
        $this->static_path = $this->Config['static_path'][$this->Config['env']];
        $this->surl = $this->Config['static_url'][$this->Config['env']];
        $this->static_url = $this->Config['url'][$this->Config['env']]['static'];
        $this->url = $this->Config['url'][$this->Config['env']][$this->App];
        $this->lurl = $this->Config['url'][$this->Config['env']][$this->App] . ($this->Config['multilanguage']['enabled'] ? '/' . $this->language : '');
        $this->aurl = $this->Config['url'][$this->Config['env']]['admin'] . ($this->Config['multilanguage']['enabled'] ? '/' . $this->language : '');
        $this->lTypesRedir = array('301' => '301 Moved Permanently', '302' => '302 Moved Temporarily');
        $this->included_js = array();
        $this->included_css = array();
        $this->redirectToCall();

        spl_autoload_register(array($this, 'loadLib'));
    }

    /**
     * Méthode magique GET
     *
     * @function __get
     * @param string $name Nom de l'attribut
     * @return string
     */
    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Méthode magique SET
     *
     * @function __set
     * @param string $name Nom de l'attribut
     * @param string $value Valeur de l'attribut
     */
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    /**
     * Méthode qui enregistre la page appelée pour redirection après login
     *
     * @function redirectToCall
     */
    private function redirectToCall()
    {
        $excludes = array('login', 'lostpassword', 'choiceSite', 'logout', 'keepAlive');

        if (!in_array($this->current_function, $excludes)) {
            $_SESSION['requestUrl'] = (!empty($_SERVER['CONTEXT_PREFIX']) ? str_replace($_SERVER['CONTEXT_PREFIX'], '', $_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI']);
        }
    }

    /**
     * Méthode qui permet de détruire les sessions
     *
     * @function killSessions
     * @param string $kill Pour choisir si on détruit tout ou pas
     */
    private function killSessions($kill = '')
    {
        if ($kill == 'destructSessions') {
            unset($_SESSION);
            $_SESSION = '';
            session_destroy();
        } else {
            unset($_SESSION['mysql_error']);
            unset($_SESSION['mysql_debug']);
            unset($_SESSION['mysql_panic']);
            unset($_SESSION['setDebug']);
        }
    }

    /**
     * Méthode de déconnexion
     *
     * @function logout
     */
    public function logout()
    {
        unset($_SESSION);
        $_SESSION = '';
        session_destroy();
    }

    /**
     * Méthode qui permet de charger les librairies et classes
     *
     * @function loadLib
     * @param string $libName Nom de la librairie
     */
    public function loadLib($libName)
    {
        if (file_exists($this->path . 'override/librairies/' . $libName . '.class.php')) {
            include_once($this->path . 'override/librairies/' . $libName . '.class.php');
        } elseif (file_exists($this->path . 'librairies/' . $libName . '.class.php')) {
            include_once($this->path . 'librairies/' . $libName . '.class.php');
        } elseif (file_exists($this->path . 'override/classes/' . $libName . '.class.php')) {
            include_once($this->path . 'override/classes/' . $libName . '.class.php');
        } elseif (file_exists($this->path . 'classes/' . $libName . '.class.php')) {
            include_once($this->path . 'classes/' . $libName . '.class.php');
        }
    }

    /**
     * Méthode de chargement des fichiers JS
     *
     * @function loadJs
     * @param string $js Fichier JS
     * @param string $url Url du domaine vers le JS (optionnel si rien : surl)
     * @param integer $ieonly Limitation à la version IE
     */
    public function loadJs($js, $url = '', $ieonly = 0)
    {
        if (!array_key_exists($js, $this->included_js)) {
            $ligneJS = '';
            $ligneJS .= ($ieonly != 0 ? '<!--[if IE ' . $ieonly . ']>' : '');
            $ligneJS .= '<script type="text/javascript" src="' . (!empty($url) ? $url : $this->surl . '/scripts/' . $this->App) . '/' . $js . '.js' . ($this->Config['env'] == 'dev' || $this->Config['env'] == 'demo' ? '?' . time() : ($this->currentVersion != '' ? '?' . $this->currentVersion : '')) . '"></script>';
            $ligneJS .= ($ieonly != 0 ? '<![endif]-->' : '');
            $this->included_js[$js] = $ligneJS;
        }
    }

    /**
     * Méthode de chargement des fichiers CSS
     *
     * @function loadCss
     * @param string $css Fichier CSS
     * @param integer $ieonly Limitation à la version IE
     * @param string $media Type de média pour l'affichage
     */
    public function loadCss($css, $ieonly = 0, $media = 'all')
    {
        if (!array_key_exists($css, $this->included_css)) {
            $ligneCSS = '';
            $ligneCSS .= ($ieonly != 0 ? '<!--[if IE ' . $ieonly . ']>' : '');
            $ligneCSS .= '<link media ="' . $media . '" href="' . $this->surl . '/styles/' . $this->App . '/' . $css . '.css' . ($this->Config['env'] == 'dev' || $this->Config['env'] == 'demo' ? '?' . time() : ($this->currentVersion != '' ? '?' . $this->currentVersion : '')) . '" type="text/css" rel="stylesheet" />';
            $ligneCSS .= ($ieonly != 0 ? '<![endif]-->' : '');
            $this->included_css[$css] = $ligneCSS;
        }
    }

    /**
     * Méthode de déchargement des fichiers JS
     *
     * @function unLoadJs
     * @param string $js Fichier JS
     */
    public function unLoadJs($js)
    {
        if (array_key_exists($js, $this->included_js)) {
            unset($this->included_js[$js]);
        }
    }

    /**
     * Méthode de déchargement des fichiers CSS
     *
     * @function unLoadCss
     * @param string $css Fichier CSS
     */
    public function unLoadCss($css)
    {
        if (array_key_exists($css, $this->included_css)) {
            unset($this->included_css[$css]);
        }
    }

    /**
     * Méthode d'appel des JS
     *
     * @function callJs
     */
    public function callJs()
    {
        if ($this->activateJSmin) {
            $miniJS = $this->static_path . 'scripts/' . $this->App . '/' . md5($this->currentVersion . implode('', $this->included_js)) . '.mini.js';

            if (file_exists($miniJS)) {
                $js = '<script type="text/javascript" src="' . $this->surl . '/scripts/' . $this->App . '/' . md5($this->currentVersion . implode('', $this->included_js)) . '.mini.js"></script>';
                echo $js . "\n";

                return;
            } else {
                $initJS = '';
                foreach ($this->included_js as $key => $js) {
                    $initJS .= file_get_contents($this->static_path . 'scripts/' . $this->App . '/' . $key . '.js') . "\n";
                }

                $finalJS = JSMin::minify($initJS);

                touch($miniJS);
                $file = fopen($miniJS, 'a');
                fputs($file, $finalJS);
                fclose($file);
                $js = '<script type="text/javascript" src="' . $this->surl . '/scripts/' . $this->App . '/' . md5($this->currentVersion . implode('', $this->included_js)) . '.mini.js"></script>';
                echo $js . "\n";

                return;
            }
        } else {
            foreach ($this->included_js as $js) {
                echo $js . "\r\n";
            }

            return;
        }
    }

    /**
     * Méthode d'appel des CSS
     *
     * @function callCss
     */
    public function callCss()
    {
        if ($this->activateCSSmin) {
            $miniCSS = $this->static_path . 'styles/' . $this->App . '/' . md5($this->currentVersion . implode('', $this->included_css)) . '.mini.css';

            if (file_exists($miniCSS)) {
                $css = '<link media ="all" href="' . $this->surl . '/styles/' . $this->App . '/' . md5($this->currentVersion . implode('', $this->included_css)) . '.mini.css" type="text/css" rel="stylesheet" />';
                echo $css . "\n";

                return;
            } else {
                $initCSS = '';
                foreach ($this->included_css as $key => $css) {
                    $initCSS .= file_get_contents($this->static_path . 'styles/' . $this->App . '/' . $key . '.css') . "\n";
                }

                $CSSmin = new CSSmin;
                $finalCSS = $CSSmin->run($initCSS);

                touch($miniCSS);
                $file = fopen($miniCSS, 'a');
                fputs($file, $finalCSS);
                fclose($file);
                $css = '<link media ="all" href="' . $this->surl . '/styles/' . $this->App . '/' . md5($this->currentVersion . implode('', $this->included_css)) . '.mini.css" type="text/css" rel="stylesheet" />';
                echo $css . "\n";

                return;
            }
        } else {
            foreach ($this->included_css as $css) {
                echo $css . "\n";
            }

            return;
        }
    }


    /**
     * Méthode de chargement des vues et controllers
     *
     * @function execute
     */
    public function execute()
    {
        $FunctionToCall = $this->current_function;

        if (!is_callable(array($this, '_' . $FunctionToCall))) {
            $arrfunc = array(0 => $FunctionToCall);
            $arr = array_merge($arrfunc, $this->Command->Parameters);
            $this->Command->Parameters = $arr;
            $FunctionToCall = 'default';
        }

        $this->setView($FunctionToCall);
        $this->params = $this->Command->Parameters;
        call_user_func(array($this, '_' . $FunctionToCall));

        if ($this->is_view_template && !$this->autoFireNothing) {
            $this->fireControllerTemplate();
        }

        if ($this->autoFireHead && !$this->autoFireNothing) {
            $this->fireHead();
        }

        if (isset($_SESSION['user']) && $_SESSION['user']['debug'] == 1 && $this->autoFireDebug && !$this->autoFireNothing && $this->App != 'admin') {
            $this->fireAdmin();
        }

        if (isset($_SESSION['user']) && $_SESSION['user']['debug'] == 1 && $this->autoFireDebug && !$this->autoFireNothing) {
            $this->fireDebug();
        }

        if ($this->autoFireHeader && !$this->autoFireNothing) {
            $this->fireHeader();
        }

        if ($this->autoFireView && !$this->autoFireNothing) {
            $this->fireView();
        }

        if ($this->autoFireFooter && !$this->autoFireNothing) {
            $this->fireFooter();
        }
    }

    /**
     * Méthode d'attribution de la vue
     *
     * @function setView
     * @param string $view Nom de la vue
     * @param boolean $is_template La vue est-elle un template
     */
    public function setView($view, $is_template = false)
    {
        $this->view = $view;
        $this->is_view_template = $is_template;
    }

    /**
     * Méthode d'attribution de la session setDebug
     *
     * @function setDebug
     * @param string $var Valeur du debug
     * @param string $title Titre
     */
    public function setDebug($var, $title = '')
    {
        if ($title == '') {
            $title = count($_SESSION['setDebug']);
        }

        $_SESSION['setDebug'][$title] = $var;
    }

    /**
     * Méthode qui charge le controller de template
     *
     * @function fireControllerTemplate
     */
    private function fireControllerTemplate()
    {
        if (file_exists($this->path . 'override/apps/' . $this->App . '/controllers/templates/' . $this->view . '.php')) {
            include($this->path . 'override/apps/' . $this->App . '/controllers/templates/' . $this->view . '.php');
        } elseif (file_exists($this->path . 'apps/' . $this->App . '/controllers/templates/' . $this->view . '.php')) {
            include($this->path . 'apps/' . $this->App . '/controllers/templates/' . $this->view . '.php');
        } elseif (file_exists($this->path . 'apps/default/controllers/templates/' . $this->view . '.php')) {
            include($this->path . 'apps/default/controllers/templates/' . $this->view . '.php');
        }
    }

    /**
     * Méthode qui charge le HEAD
     *
     * @function fireHead
     */
    private function fireHead()
    {
        if ($this->App == 'admin') {
            if (file_exists($this->path . 'override/apps/admin/views/' . $this->head . '.php')) {
                include($this->path . 'override/apps/admin/views/' . $this->head . '.php');
            } elseif (file_exists($this->path . 'apps/admin/views/' . $this->head . '.php')) {
                include($this->path . 'apps/admin/views/' . $this->head . '.php');
            } else {
                trigger_error('ASPARTAM - head not found : ' . $this->path . 'apps/admin/views/' . $this->head . '.php', E_USER_ERROR);
            }
        } else {
            if (file_exists($this->path . 'override/apps/' . $this->App . '/views/' . $this->head . '.php')) {
                include($this->path . 'override/apps/' . $this->App . '/views/' . $this->head . '.php');
            } elseif (file_exists($this->path . 'apps/' . $this->App . '/views/' . $this->head . '.php')) {
                include($this->path . 'apps/' . $this->App . '/views/' . $this->head . '.php');
            } elseif (file_exists($this->path . 'apps/default/views/' . $this->head . '.php')) {
                include($this->path . 'apps/default/views/' . $this->head . '.php');
            } else {
                trigger_error('ASPARTAM - head not found : ' . $this->path . 'apps/default/views/' . $this->head . '.php', E_USER_ERROR);
            }
        }
    }

    /**
     * Méthode qui charge le Debug
     *
     * @function fireDebug
     */
    private function fireDebug()
    {
        if (file_exists($this->path . 'core/debug/debug.php')) {
            include($this->path . 'core/debug/debug.php');
        } else {
            trigger_error('ASPARTAM - debug not found : ' . $this->path . 'core/debug/debug.php', E_USER_ERROR);
        }
    }

    /**
     * Méthode qui charge la barre d'admin
     *
     * @function fireAdmin
     */
    private function fireAdmin()
    {
        if (file_exists($this->path . 'core/adminbar/adminbar.php')) {
            include($this->path . 'core/adminbar/adminbar.php');
        } else {
            trigger_error('ASPARTAM - adminbar not found : ' . $this->path . 'core/adminbar/adminbar.php', E_USER_ERROR);
        }
    }

    /**
     * Méthode qui charge le HEADER
     *
     * @function fireHeader
     */
    private function fireHeader()
    {
        if ($this->App == 'admin') {
            if (file_exists($this->path . 'override/apps/admin/views/' . $this->header . '.php')) {
                include($this->path . 'override/apps/admin/views/' . $this->header . '.php');
            } elseif (file_exists($this->path . 'apps/admin/views/' . $this->header . '.php')) {
                include($this->path . 'apps/admin/views/' . $this->header . '.php');
            } else {
                trigger_error('ASPARTAM - header not found : ' . $this->path . 'apps/admin/views/' . $this->header . '.php', E_USER_ERROR);
            }
        } else {
            if (file_exists($this->path . 'override/apps/' . $this->App . '/views/' . $this->header . '.php')) {
                include($this->path . 'override/apps/' . $this->App . '/views/' . $this->header . '.php');
            } elseif (file_exists($this->path . 'apps/' . $this->App . '/views/' . $this->header . '.php')) {
                include($this->path . 'apps/' . $this->App . '/views/' . $this->header . '.php');
            } elseif (file_exists($this->path . 'apps/default/views/' . $this->header . '.php')) {
                include($this->path . 'apps/default/views/' . $this->header . '.php');
            } else {
                trigger_error('ASPARTAM - header not found : ' . $this->path . 'apps/default/views/' . $this->header . '.php', E_USER_ERROR);
            }
        }
    }

    /**
     * Méthode qui charge la VUE
     *
     * @function fireView
     */
    private function fireView()
    {
        if ($this->App == 'admin') {
            if (file_exists($this->path . 'override/apps/admin/views/' . $this->current_controller . '/' . $this->view . '.php')) {
                include($this->path . 'override/apps/admin/views/' . $this->current_controller . '/' . $this->view . '.php');
            } elseif (file_exists($this->path . 'apps/admin/views/' . $this->current_controller . '/' . $this->view . '.php')) {
                include($this->path . 'apps/admin/views/' . $this->current_controller . '/' . $this->view . '.php');
            } else {
                trigger_error('ASPARTAM - view not found : ' . $this->path . 'apps/admin/views/' . $this->current_controller . '/' . $this->view . '.php', E_USER_ERROR);
            }
        } else {
            if ($this->is_view_template) {
                if (file_exists($this->path . 'override/apps/' . $this->App . '/views/templates/' . $this->view . '.php')) {
                    include($this->path . 'override/apps/' . $this->App . '/views/templates/' . $this->view . '.php');
                } elseif (file_exists($this->path . 'apps/' . $this->App . '/views/templates/' . $this->view . '.php')) {
                    include($this->path . 'apps/' . $this->App . '/views/templates/' . $this->view . '.php');
                } elseif (file_exists($this->path . 'apps/default/views/templates/' . $this->view . '.php')) {
                    include($this->path . 'apps/default/views/templates/' . $this->view . '.php');
                }
            } else {
                if (file_exists($this->path . 'override/apps/' . $this->App . '/views/' . $this->current_controller . '/' . $this->view . '.php')) {
                    include($this->path . 'override/apps/' . $this->App . '/views/' . $this->current_controller . '/' . $this->view . '.php');
                } elseif (file_exists($this->path . 'apps/' . $this->App . '/views/' . $this->current_controller . '/' . $this->view . '.php')) {
                    include($this->path . 'apps/' . $this->App . '/views/' . $this->current_controller . '/' . $this->view . '.php');
                } elseif (file_exists($this->path . 'apps/default/views/' . $this->current_controller . '/' . $this->view . '.php')) {
                    include($this->path . 'apps/default/views/' . $this->current_controller . '/' . $this->view . '.php');
                } else {
                    trigger_error('ASPARTAM - view not found : ' . $this->path . 'apps/default/views/' . $this->current_controller . '/' . $this->view . '.php', E_USER_ERROR);
                }
            }
        }
    }

    /**
     * Méthode qui charge le FOOTER
     *
     * @function fireFooter
     */
    private function fireFooter()
    {
        if ($this->App == 'admin') {
            if (file_exists($this->path . 'override/apps/admin/views/' . $this->footer . '.php')) {
                include($this->path . 'override/apps/admin/views/' . $this->footer . '.php');
            } elseif (file_exists($this->path . 'apps/admin/views/' . $this->footer . '.php')) {
                include($this->path . 'apps/admin/views/' . $this->footer . '.php');
            } else {
                trigger_error('ASPARTAM - footer not found : ' . $this->path . 'apps/admin/views/' . $this->footer . '.php', E_USER_ERROR);
            }
        } else {
            if (file_exists($this->path . 'override/apps/' . $this->App . '/views/' . $this->footer . '.php')) {
                include($this->path . 'override/apps/' . $this->App . '/views/' . $this->footer . '.php');
            } elseif (file_exists($this->path . 'apps/' . $this->App . '/views/' . $this->footer . '.php')) {
                include($this->path . 'apps/' . $this->App . '/views/' . $this->footer . '.php');
            } elseif (file_exists($this->path . 'apps/default/views/' . $this->footer . '.php')) {
                include($this->path . 'apps/default/views/' . $this->footer . '.php');
            } else {
                trigger_error('ASPARTAM - footer not found : ' . $this->path . 'apps/default/views/' . $this->footer . '.php', E_USER_ERROR);
            }
        }
    }

    /**
     * Méthode qui charge le BLOC
     *
     * @function fireBloc
     * @param string $bloc Slug du bloc à charger
     */
    public function fireBloc($bloc)
    {
        if (file_exists($this->path . 'override/apps/' . $this->App . '/views/blocs/' . $bloc . '.php')) {
            include($this->path . 'override/apps/' . $this->App . '/views/blocs/' . $bloc . '.php');
        } elseif (file_exists($this->path . 'apps/default/views/blocs/' . $bloc . '.php')) {
            include($this->path . 'apps/default/views/blocs/' . $bloc . '.php');
        } else {
            trigger_error('ASPARTAM - bloc not found : ' . $this->path . 'apps/default/views/blocs/' . $bloc . '.php', E_USER_ERROR);
        }
    }

    /**
     * Méthode qui charge une vue Ajax
     *
     * @function fireAjax
     * @param string $view Slug du la vue ajax à charger
     */
    public function fireAjax($view)
    {
        if (file_exists($this->path . 'override/apps/' . $this->App . '/views/ajax/' . $view . '.php')) {
            include($this->path . 'override/apps/' . $this->App . '/views/ajax/' . $view . '.php');
        } elseif (file_exists($this->path . 'apps/default/views/ajax/' . $view . '.php')) {
            include($this->path . 'apps/default/views/ajax/' . $view . '.php');
        } else {
            trigger_error('ASPARTAM - Ajax not found : ' . $this->path . 'apps/default/views/ajax/' . $view . '.php', E_USER_ERROR);
        }
    }

    /**
     * Méthode qui gère les URL pour le changment de langue
     *
     * @function changeLanguage
     * @param string $lang Langue à charger
     * @param string $current_lang Langue en cours
     * @return string URL de la page avec la nouvelle langue
     */
    public function changeLanguage($lang, $current_lang)
    {
        $requestURI = explode('/', (!empty($_SERVER['CONTEXT_PREFIX']) ? str_replace($_SERVER['CONTEXT_PREFIX'], '', $_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI']));
        $requestURI = array_slice($requestURI, 2);
        $slug = $requestURI[0];
        $tree = new o\tree(array('slug' => $slug, 'id_langue' => $current_lang));

        if ($tree->exist()) {
            $treeNew = new o\tree(array('id_tree' => $tree->id_tree, 'id_langue' => $lang));

            if ($treeNew->exist()) {
                $requestURI[0] = $treeNew->slug;
                $requestURI = implode('/', $requestURI);
                return $this->url . '/' . $lang . '/' . $requestURI;
            } else {
                return $this->url . '/' . $lang . '/';
            }
        } else {
            $requestURI = implode('/', $requestURI);
            return $this->url . '/' . $lang . '/' . $requestURI;
        }
    }

    /**
     * Méthode qui gère les redirections
     *
     * @function redirect
     * @param string $url URL de destination
     * @param string $type Type de la redirection
     */
    public function redirect($url, $type = '')
    {
        if ($type != '') {
            header('HTTP/1.1 ' . $this->lTypesRedir[$type]);
        }

        header('location:' . $url);
        die;
    }
}
