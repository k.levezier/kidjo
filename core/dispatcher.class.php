<?php

/**
 * La classe Dispatcher permet l'analyse de l'url pour l'injection
 * dans le modèle MVC.
 *
 * @class       Dispatcher
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class Dispatcher
{
    /**
     * @var Command $Command Objet Command instancié dans la fonction handleUrl
     * @var array $Config Tableau de la configuration
     * @var string $App Identifiant de l'application
     * @var string $path Chemin de l'environnement
     */
    private $Command;
    private $Config;
    private $App;
    private $path;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param array $config Tableau de la configuration
     * @param string $app Identifiant de l'application
     */
    public function __construct($config, $app)
    {
        $this->Config = $config;
        $this->App = $app;
        $this->path = $this->Config['path'][$this->Config['env']];
        $this->handleUrl();
        $this->dispatch();
    }

    /**
     * Méthode pour déterminer si c'est un controller
     *
     * @function isController
     * @param string $controllerName Nom du controller
     * @return boolean
     */
    private function isController($controllerName)
    {
        if ($this->App == 'admin') {
            if (file_exists($this->path . 'override/apps/admin/controllers/' . $controllerName . '.php')) {
                return true;
            } else {
                if (file_exists($this->path . 'apps/admin/controllers/' . $controllerName . '.php')) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if (file_exists($this->path . 'override/apps/' . $this->App . '/controllers/' . $controllerName . '.php')) {
                return true;
            } else {
                if (file_exists($this->path . 'apps/' . $this->App . '/controllers/' . $controllerName . '.php')) {
                    return true;
                } elseif (file_exists($this->path . 'apps/default/controllers/' . $controllerName . '.php')) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    /**
     * Méthode pour déterminer si c'est une action dans un controller
     *
     * @function isActionInController
     * @param string $controllerName Nom du controller
     * @param string $action Nom de l'action
     * @return boolean
     */
    private function isActionInController($controllerName, $action)
    {
        if ($this->App == 'admin') {
            if (file_exists($this->path . 'override/apps/admin/controllers/' . $controllerName . '.php')) {
                $controller_content = file_get_contents($this->path . 'override/apps/admin/controllers/' . $controllerName . '.php');
            } else {
                $controller_content = file_get_contents($this->path . 'apps/admin/controllers/' . $controllerName . '.php');
            }
        } else {
            if (file_exists($this->path . 'override/apps/' . $this->App . '/controllers/' . $controllerName . '.php')) {
                $controller_content = file_get_contents($this->path . 'override/apps/' . $this->App . '/controllers/' . $controllerName . '.php');
            } elseif (file_exists($this->path . 'apps/' . $this->App . '/controllers/' . $controllerName . '.php')) {
                $controller_content = file_get_contents($this->path . 'apps/' . $this->App . '/controllers/' . $controllerName . '.php');
            } else {
                $controller_content = file_get_contents($this->path . 'apps/default/controllers/' . $controllerName . '.php');
            }
        }

        if (strpos($controller_content, 'function _' . $action . '(') === false && strpos($controller_content, 'function _' . $action . ' (') === false) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Méthode pour la création de l'URL avant le dispatch
     *
     * @function handleUrl
     */
    private function handleUrl()
    {
        $requestServeurURI = (!empty($_SERVER['CONTEXT_PREFIX']) ? str_replace($_SERVER['CONTEXT_PREFIX'], '', $_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI']);
        $scriptNameServeur = (!empty($_SERVER['CONTEXT_PREFIX']) ? str_replace($_SERVER['CONTEXT_PREFIX'], '', $_SERVER['SCRIPT_NAME']) : $_SERVER['SCRIPT_NAME']);
        $requestURI = explode('/', $requestServeurURI);
        $scriptName = explode('/', $scriptNameServeur);
        $commandArrayDiff = array_diff_assoc($requestURI, $scriptName);
        $commandArray = array_values($commandArrayDiff);

        if ($commandArray[count($commandArray) - 1] == '') {
            unset($commandArray[count($commandArray) - 1]);
        }

        if ($this->Config['multilanguage']['enabled']) {
            if (!array_key_exists($commandArray[0], $this->Config['multilanguage']['allowed_languages'])) {
                if (array_key_exists($_SERVER['HTTP_HOST'], $this->Config['multilanguage']['domain_default_languages'])) {
                    $url = $this->Config['url'][$this->Config['env']][$this->App];
                    $langue = $this->Config['multilanguage']['domain_default_languages'][$_SERVER['HTTP_HOST']];

                    header('HTTP/1.1 301 Moved Permanently');
                    header('location:' . $url . '/' . $langue . $requestServeurURI);
                    die;
                } else {
                    $url = $this->Config['url'][$this->Config['env']][$this->App];
                    $array = array_keys($this->Config['multilanguage']['allowed_languages']);
                    $langue = $array[0];

                    header('HTTP/1.1 301 Moved Permanently');
                    header('location:' . $url . '/' . $langue . $requestServeurURI);
                    die;
                }
            } else {
                $langue = $commandArray[0];
                $commandArray = array_slice($commandArray, 1);
            }
        } else {
            if (array_key_exists($_SERVER['HTTP_HOST'], $this->Config['multilanguage']['domain_default_languages'])) {
                $langue = $this->Config['multilanguage']['domain_default_languages'][$_SERVER['HTTP_HOST']];
            } else {
                $array = array_keys($this->Config['multilanguage']['allowed_languages']);
                $langue = $array[0];
            }
        }

        if ($commandArray[0] == '') {
            $controllerName = 'root';
            $controllerFunction = 'default';
            $parameters = array();
        } elseif ($commandArray[0] != '' && $commandArray[1] == '') {
            if ($this->isController($commandArray[0])) {
                $controllerName = $commandArray[0];
                $controllerFunction = 'default';
                $parameters = array();
            } elseif ($this->isActionInController('root', $commandArray[0]) === true) {
                $controllerName = 'root';
                $controllerFunction = $commandArray[0];
                $parameters = array();
            } else {
                $controllerName = 'root';
                $controllerFunction = 'default';
                $parameters = $commandArray;
            }
        } else {
            if ($this->isController($commandArray[0])) {
                $controllerName = $commandArray[0];

                if ($this->isActionInController($controllerName, $commandArray[1])) {
                    $controllerFunction = $commandArray[1];
                    $parameters = array_slice($commandArray, 2);
                } else {
                    $controllerFunction = 'default';
                    $parameters = array_slice($commandArray, 1);
                }
            } elseif ($this->isActionInController('root', $commandArray[0])) {
                $controllerName = 'root';
                $controllerFunction = $commandArray[0];
                $parameters = array_slice($commandArray, 1);
            } else {
                $controllerName = 'root';
                $controllerFunction = 'default';
                $parameters = $commandArray;
            }
        }

        $i = 0;

        foreach ($parameters as $p) {
            $var = explode($this->Config['params']['separator'], $p);

            if ($var[1] != '') {
                $tmp[$var[0]] = $var[1];
            } else {
                $tmp[$i] = $p;
            }

            $i++;
        }

        if ($i > 0) {
            $parameters = $tmp;
        }

        $this->Command = new Command($controllerName, $controllerFunction, $parameters, $langue);
    }

    /**
     * Méthode qui charge et execute l'environnement MVC
     *
     * @function dispatch
     */
    private function dispatch()
    {
        $controllerName = $this->Command->Name;

        if ($this->App == 'admin') {
            if (file_exists($this->path . 'apps/admin/bootstrap.php')) {
                include($this->path . 'apps/admin/bootstrap.php');

                if (file_exists($this->path . 'override/apps/admin/bootstrap.php')) {
                    include($this->path . 'override/apps/admin/bootstrap.php');
                } else {
                    trigger_error('ASPARTAM - bootstrap override not found : ' . $this->path . 'override/apps/admin/bootstrap.php', E_USER_ERROR);
                }
            } else {
                trigger_error('ASPARTAM - bootstrap core not found : ' . $this->path . 'apps/admin/bootstrap.php', E_USER_ERROR);
            }
        } else {
            if (file_exists($this->path . 'apps/' . $this->App . '/bootstrap.php')) {
                include($this->path . 'apps/' . $this->App . '/bootstrap.php');

                if (file_exists($this->path . 'override/apps/' . $this->App . '/bootstrap.php')) {
                    include($this->path . 'override/apps/' . $this->App . '/bootstrap.php');
                } else {
                    trigger_error('ASPARTAM - bootstrap override not found : ' . $this->path . 'override/apps/' . $this->App . '/bootstrap.php', E_USER_ERROR);
                }
            } else {
                if (file_exists($this->path . 'apps/default/bootstrap.php')) {
                    include($this->path . 'apps/default/bootstrap.php');

                    if (file_exists($this->path . 'override/apps/' . $this->App . '/bootstrap.php')) {
                        include($this->path . 'override/apps/' . $this->App . '/bootstrap.php');
                    } else {
                        trigger_error('ASPARTAM - bootstrap override not found : ' . $this->path . 'override/apps/' . $this->App . '/bootstrap.php', E_USER_ERROR);
                    }
                } else {
                    trigger_error('ASPARTAM - bootstrap core not found : ' . $this->path . 'apps/default/bootstrap.php', E_USER_ERROR);
                }
            }
        }

        if ($this->App == 'admin') {
            if (file_exists($this->path . 'apps/admin/controllers/' . $controllerName . '.php')) {
                if (file_exists($this->path . 'override/apps/admin/controllers/' . $controllerName . '.php')) {
                    include($this->path . 'apps/admin/controllers/' . $controllerName . '.php');
                    include($this->path . 'override/apps/admin/controllers/' . $controllerName . '.php');
                    $controllerClass = $controllerName . "Override";
                } else {
                    include($this->path . 'apps/admin/controllers/' . $controllerName . '.php');
                    $controllerClass = $controllerName . "Controller";
                }
            } else {
                if (file_exists($this->path . 'override/apps/admin/controllers/' . $controllerName . '.php')) {
                    include($this->path . 'override/apps/admin/controllers/' . $controllerName . '.php');
                    $controllerClass = $controllerName . "Controller";
                } else {
                    trigger_error('ASPARTAM - controller not found : ' . $this->path . 'override/apps/admin/controllers/' . $controllerName . '.php', E_USER_ERROR);
                }
            }
        } else {
            if (file_exists($this->path . 'apps/' . $this->App . '/controllers/' . $controllerName . '.php')) {
                if (file_exists($this->path . 'override/apps/' . $this->App . '/controllers/' . $controllerName . '.php')) {
                    include($this->path . 'apps/' . $this->App . '/controllers/' . $controllerName . '.php');
                    include($this->path . 'override/apps/' . $this->App . '/controllers/' . $controllerName . '.php');
                    $controllerClass = $controllerName . "Override";
                } else {
                    include($this->path . 'apps/' . $this->App . '/controllers/' . $controllerName . '.php');
                    $controllerClass = $controllerName . "Controller";
                }
            } elseif (file_exists($this->path . 'override/apps/' . $this->App . '/controllers/' . $controllerName . '.php')) {
                include($this->path . 'override/apps/' . $this->App . '/controllers/' . $controllerName . '.php');
                $controllerClass = $controllerName . "Controller";
            } else {
                if (file_exists($this->path . 'apps/default/controllers/' . $controllerName . '.php')) {
                    if (file_exists($this->path . 'override/apps/default/controllers/' . $controllerName . '.php')) {
                        include($this->path . 'apps/default/controllers/' . $controllerName . '.php');
                        include($this->path . 'override/apps/default/controllers/' . $controllerName . '.php');
                        $controllerClass = $controllerName . "Override";
                    } else {
                        include($this->path . 'apps/default/controllers/' . $controllerName . '.php');
                        $controllerClass = $controllerName . "Controller";
                    }
                } else {
                    if (file_exists($this->path . 'override/apps/default/controllers/' . $controllerName . '.php')) {
                        include($this->path . 'override/apps/default/controllers/' . $controllerName . '.php');
                        $controllerClass = $controllerName . "Controller";
                    } else {
                       trigger_error('ASPARTAM - controller not found : ' . $this->path . 'override/apps/default/controllers/' . $controllerName . '.php', E_USER_ERROR);
                    }
                }
            }
        }

        $controller = new $controllerClass($this->Command, $this->Config, $this->App);
        $controller->execute();
    }
}
