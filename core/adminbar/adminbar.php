<link media="all" href="<?= $this->surl ?>/styles/admin/font-awesome.css" type="text/css" rel="stylesheet"/>
<link media="all" href="<?= $this->surl ?>/styles/adminbar/adminbar.css" type="text/css" rel="stylesheet"/>
<div class="adminbar">
    <div class="quicklinks">
        <ul>
            <li>
                <a href="<?= $this->aurl ?>" title="<?= $this->ln->txt('adminbar', 'admin-site', $this->language, 'Administration du site') ?>">
                    <i class="fa fa-cog"></i>&nbsp;&nbsp;
                    <?= $this->ln->txt('adminbar', 'admin-site', $this->language, 'Administration du site') ?>
                </a>
            </li>
            <?php if (!empty($this->tree)) { ?>
                <li>
                    <a href="<?= $this->aurl ?>/edition/formPage/page___<?= $this->tree->id_tree ?>/part___3" title="">
                        <i class="fa fa-pencil"></i>&nbsp;&nbsp;
                        <?= $this->ln->txt('adminbar', 'modif', $this->language, 'Modifier') ?>
                    </a>
                </li>
            <?php } ?>
            <li class="rightblock">
                <i class="fa fa-user"></i>&nbsp;&nbsp;
                <?= $_SESSION['user']['firstname'] ?> <?= $_SESSION['user']['name'] ?>
            </li>
        </ul>
    </div>
</div>
