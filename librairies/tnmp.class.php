<?php

/**
 * La classe tnmp regroupe un ensemble de methodes utilisées pour
 * l'envoi des mails transactionnels via NMP
 *
 * @class       tnmp
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class tnmp
{
    /**
     * Constructeur de la classe
     *
     * @function __construct
     */
    public function __construct()
    {
        $this->nmp = new o\nmp(array('id_nmp' => 0));
        $this->nmp_desabo = new o\data('nmp_desabo');
    }

    /**
     * Méthode qui permet la construction des variables pour les mails serveur
     *
     * @function constructionVariablesServeur
     * @param array $tab tableau des variables
     * @return array
     */
    function constructionVariablesServeur($tab)
    {
        $result = array();

        foreach ($tab as $key => $value) {
            $result['[EMV DYN]' . $key . '[EMV /DYN]'] = $value;
        }

        return $result;
    }

    /**
     * Méthode qui permet l'enregistrement du mail dans la queue NMP
     *
     * @function sendMailNMP
     * @param array $tabFiler tableau des variables de retour du filer mail
     * @param array $varMail tableau des variables
     * @param string $nmp_secure code secure du template sur SF
     * @param string $id_nmp id du template sur SF
     * @param string $nmp_unique code unique du template sur SF
     */
    function sendMailNMP($tabFiler, $varMail, $nmp_secure, $id_nmp, $nmp_unique)
    {
        // Recuperation des variables
        foreach ($tabFiler as $key => $value) {
            ${$key} = $value;
        }

        // Initialisation du contenu
        $contentPush = array();

        // Initialisation des variables dynamiques
        $varDyn = array();

        foreach ($varMail as $key => $value) {
            $varDyn['entry'][] = array('key' => $key, 'value' => $value);
        }

        $varDyn['entry'][] = array(
            'key'   => 'miroir',
            'value' => $this->lurl . '/miroir/' . $id_filermails . '/' . md5($id_textemail),
        );
        $varDyn['entry'][] = array(
            'key'   => 'desabo',
            'value' => $this->lurl . '/removeNMP/' . $desabo . '/' . $id_filermails . '/' . $email_nmp,
        );

        // Initialisation des parametres
        $arg0['arg0'] = array(
            'content'        => $contentPush,
            'dyn'            => $varDyn,
            'email'          => $email_nmp,
            'encrypt'        => $nmp_secure,
            'notificationId' => $id_nmp,
            'random'         => $nmp_unique,
            'senddate'       => date('Y-m-d'),
            'synchrotype'    => 'NOTHING',
            'uidkey'         => 'EMAIL',
        );

        // Preparation du queue
        $this->nmp->serialize_content = serialize($arg0);
        $this->nmp->date = date('Y-m-d');
        $this->nmp->mailto = $email_nmp;
        $this->nmp->status = 0;
        $this->nmp->save();
    }

    /**
     * Méthode qui permet le traitement de la queue NMP
     *
     * @function processQueue
     * @param int $limit nombre de mails à traiter par appel
     */
    function processQueue($limit = 500)
    {
        // Connection au serveur
        $location = 'http://api.notificationmessaging.com/NMSOAP/NotificationService?wsdl';
        $client = new SoapClient($location);

        // Recuperation de la file d'attente
        $lQueue = (new o\data('nmp', array('status' => 0)))->limit($limit);

        // Traitement
        foreach ($lQueue->order('added', 'ASC') as $q) {
            // Recuperation des donnees
            $this->nmp = $q;

            // Envoi du message
            try {
                $respo = $client->sendObject(unserialize($this->nmp->serialize_content));
                $this->nmp->reponse = serialize($respo);
                $this->nmp->status = 1;
            } catch (Exception $e) {
                $this->nmp->erreur = serialize($e);
                $this->nmp->status = 2;
            }

            // MAJ
            $this->nmp->update();
        }
    }
}
