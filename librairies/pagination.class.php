<?php

/**
 * Cette classe permet de gérer la pagination de n'importe quelle Iterateur ou array
 *
 * @author Jon
 */
class pagination
{
    protected $list;
    protected $page;
    protected $nb_par_page;
    protected $pagination;

    /**
     * Constructeur
     * @param SeekableIterator|array $list La liste que l'on veut paginer (doit implémenter l'interface Countable)
     * @param int $page
     * @param int $nb_par_page
     */
    public function __construct($list, $page = 1, $nb_par_page = 10)
    {
        if (is_array($list)) {
            $list = new ArrayIterator($list);
        }
        $this->list = $list;
        $this->nb_par_page = max(1, (int)$nb_par_page);
        $this->page = min($this->nbPage(), max(1, (int)$page));
    }

    /**
     * Retourne un générateur à utiliser dans un foreach() qui liste les éléments de la page active.
     * @return generator
     */
    public function getList()
    {
        for ($i = ($this->page - 1) * $this->nb_par_page; $i < ($this->page) * $this->nb_par_page; $i++) {
            try {
                $this->list->seek($i);
            } catch (Exception $e) {
                return;
            }
            yield $this->list->key() => $this->list->current();
        }
        return;
    }

    /**
     * Retourne la construction HTML des liens des pages sous la forme 1 ... 5 6 7 8 9 10 11 ... 24
     * @param string $url L'url pour chque lien. $i sera remplacé par le numéro de la page
     * @param int $nb_buttons nombre de boutons de chaque côté de la page active.
     * @return string|null HTML
     */
    public function getHTML($url = '?page=$i', $nb_buttons = 4)
    {
        $paging = $this->getPaging($nb_buttons);
        foreach ($paging as &$page) {
            if (is_int($page)) {
                $active = $page == $this->page;
                $page = '<a href="' . str_replace('$i', $page, $url) . '" style="text-decoration:none;" >' . $page . '</a>';
                if ($active) {
                    $page = "<b>$page</b>";
                }
            }
        }

        $links = implode(' ', $paging);
        if (count($paging)) {
            return "<div class='paging' style='text-align:center;margin-bottom:15px;' >$links</div>";
        }

        return null;
    }

    /**
     * Retourne la liste des liens à afficher
     * @param int $nb_buttons nombre de boutons de chaque côté de la page active.
     * @return array
     */
    public function getPaging($nb_buttons = 4)
    {
        $liens = array();
        $nb_page = $this->nbPage();
        if ($nb_page != 1) {
            $debut_page = max($this->page - $nb_buttons, 1);
            $fin_page = min($debut_page + $nb_buttons + $nb_buttons, $nb_page);
            $debut_page = min($debut_page, max($fin_page - $nb_buttons - $nb_buttons, 1));
            for ($i = $debut_page; $i < $fin_page + 1; $i++) {
                $liens[] = $i;
            }
            if ($debut_page != 1) {
                $liens[0] = '...';
                array_unshift($liens, 1);
            }
            if ($fin_page != $nb_page) {
                array_pop($liens);
                array_push($liens, '...', $nb_page);
            }
        }
        return $liens;
    }

    /**
     * Retourne le nombre de page
     *
     * @return int
     */
    public function nbPage()
    {
        return (int)max(1, ceil(count($this->list) / $this->nb_par_page));
    }

    /**
     * Retourne le numéro de page actuelle
     *
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }
}
