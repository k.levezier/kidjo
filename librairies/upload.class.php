<?php

/**
 * Classe d'upload de fichiers
 * Cette classe permet d'uploader des fichiers
 *
 * @class       upload
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class upload
{

    /**
     * $var string $msg_erreur Variable contenant le message d'erreur de l'upload
     * $var string $uploadedFileName Donnée du fichier que l'on upload
     * $var string $uploadedFile Donnée du fichier que l'on upload
     * $var string $uploadedFileSize Donnée du fichier que l'on upload
     * $var string $uploadedFileType Donnée du fichier que l'on upload
     * $var string $uploadedFileExtension Donnée du fichier que l'on upload
     * $var string $upload_dir Repertoire de destination pour les fichiers
     * $var int $taille_max Taille max des fichiers pouvant êtres uploadés en octets
     * $var array $ext_valides Tableau des extensions que l'on autorise a etre uploadé
     */
    private $msg_erreur;
    private $uploadedFileName;
    private $uploadedFile;
    private $uploadedFileSize;
    private $uploadedFileType;
    private $uploadedFileExtension;
    private $upload_dir = '/';
    private $taille_max = 819200000;
    private $ext_valides = array(
        'jpg',
        'jpeg',
        'png',
        'gif',
        'JPG',
        'JPEG',
        'PNG',
        'GIF',
        'pdf',
        'doc',
        'xls',
        'ppt',
        'docx',
        'xlsx',
        'pptx',
        'PDF',
        'DOC',
        'XLS',
        'PPT',
        'DOCX',
        'XLSX',
        'PPTX',
        'mp4',
        'MP4'
    );

    /**
     * Constructeur de la classe
     *
     * @function __construct
     */
    public function __construct()
    {

    }

    /**
     * Méthode pour definir un dossier de destination
     *
     * @function setUploadDir
     * @param string $path repertoire du site
     * @param string $upload_dir chemin des dossiers dans lequel sera le fichier
     */
    public function setUploadDir($path, $upload_dir)
    {
        $this->upload_dir = $path . $upload_dir;
        $this->path_dir = $path;
        $this->file_dir = $upload_dir;
    }

    /**
     * Méthode pour definir un Poids Max au fichier
     *
     * @function setPoidsMax
     * @param int $taille_max Taille max des fichiers pouvant êtres uploadés en octets
     */
    public function setPoidsMax($taille_max)
    {
        $this->taille_max = $taille_max;
    }

    /**
     * Méthode pour definir les extensions de fichier
     *
     * @function setExtValide
     * @param array $ext_valides Tableau des extensions que l'on autorise a etre uploadé
     */
    public function setExtValide($ext_valides)
    {
        $this->ext_valides = $ext_valides;
    }

    /**
     * Méthode pour definir le nom fichier uploadé
     *
     * @function setUploadedFileName
     * @param string $uploadedFileName nom du fichier que l'on upload
     */
    public function setUploadedFileName($uploadedFileName)
    {
        $this->uploadedFileName = $uploadedFileName;
    }

    /**
     * Méthode pour nettoyer le nom du fichier si on ne le renomme pas
     *
     * @function clean_name
     * @param string $name_file nom du fichier
     * @return string nom du fichier
     */
    private function clean_name($name_file)
    {
        $name_file = strtr($name_file, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        return strtolower(preg_replace('/([^.a-z0-9]+)/i', '-', $name_file));
    }

    /**
     * Méthode qui retourne les messages d'erreur
     *
     * @function getErrorType
     * @return string message d'erreur
     */
    public function getErrorType()
    {
        return $this->msg_erreur;
    }

    /**
     * Méthode qui récupére le nom du fichier uploadé
     *
     * @function getName
     * @return string nom du fichier uploadé
     */
    public function getName()
    {
        return $this->uploadedFileName;
    }

    /**
     * Méthode qui récupére le type du fichier uploadé
     *
     * @function getTypeMine
     * @return string type du fichier uploadé
     */
    public function getTypeMine()
    {
        return $this->uploadedFileType;
    }

    /**
     * Méthode qui récupére l'extension du fichier uploadé
     *
     * @function getExtension
     * @return string extension du fichier uploadé
     */
    public function getExtension()
    {
        return $this->uploadedFileExtension;
    }

    /**
     * Méthode qui récupére le poids du fichier uploadé
     *
     * @function getFileSize
     * @return string poids du fichier uploadé
     */
    public function getFileSize()
    {
        return $this->uploadedFileSize;
    }

    /**
     * Méthode pour l'upload des fichiers
     *
     * @function doUpload
     * @param string $file_form_name Nom du champ file du formulaire (obligatoire)
     * @param string $new_name Nouveau nom du fichier sans extension (facultatif)
     * @param bool $erase Permet de remplacer le fichier sur le serveur s'il existe deja (facultatif)
     * @return bool
     */
    public function doUpload($file_form_name, $new_name = '', $erase = false)
    {
        // Recuperation de l'extension du fichier
        $extension = pathinfo($_FILES[$file_form_name]['name']);
        $extension = $extension['extension'];

        // Recuperation du nom sans l'extension
        $nom_tmp = explode('.', $_FILES[$file_form_name]['name']);
        $nom_tmp = $nom_tmp[0];

        // Si l'on a pas donné un nouveau nom au fichier, il garde le nom d'origine
        if ($new_name == '') {
            // On clean le nom d'origine
            $this->uploadedFileName = $this->clean_name($nom_tmp) . '.' . $extension;
        } else {
            // On applique sur le nouveau nom
            $this->uploadedFileName = $new_name . '.' . $extension;
        }

        // Récupération du nom temporaire sur le serveur, de la taille du fichier, son type et son extension
        $this->uploadedFile = $_FILES[$file_form_name]['tmp_name'];
        $this->uploadedFileSize = $_FILES[$file_form_name]['size'];
        $this->uploadedFileType = $_FILES[$file_form_name]['type'];
        $this->uploadedFileExtension = $extension;

        // On commence par verifier que le dossier d'upload existe sinon on le créé
        $this->file_dir = explode('/', $this->file_dir);

        for ($i = 0; $i < count($this->file_dir); $i++) {
            if ($this->file_dir[$i] != '') {
                $this->path_dir .= '/' . $this->file_dir[$i];

                if (!is_dir($this->path_dir)) {
                    if (!mkdir($this->path_dir, 0777)) {
                        $this->msg_erreur = 'Impossible de creer le repertoire : ' . $this->path_dir;
                        return false;
                    }
                }
            }
        }

        // On verifie que le fichier soit bien uploader pour des questions de securite
        if (is_uploaded_file($this->uploadedFile) && !empty($this->uploadedFile)) {
            // On verifie que le fichier n'est pas trop lourd
            if ($this->uploadedFileSize < $this->taille_max) {
                // On verifie que le fichier est bien d'une extension valide
                if (in_array($this->uploadedFileExtension, $this->ext_valides)) {
                    // On vérifie que le nom de fichier n'est pas déja utilisé sinon on lui rajoute le timestamp a la fin seulement si le erase est a false
                    if (file_exists($this->upload_dir . $this->uploadedFileName) && $erase == false) {
                        sleep(1); // Temporisation du script pour eviter d'avoir une image avec le même nom (time() s'increment toutes les secondes)
                        $this->uploadedFileName = time() . '-' . $this->uploadedFileName;
                    }

                    // Si le fichier est ok, on l'upload sur le serveur
                    if (move_uploaded_file($this->uploadedFile, $this->upload_dir . $this->uploadedFileName)) {
                        // On donne un acces total sur le fichier
                        chmod($this->upload_dir . $this->uploadedFileName, 0777);
                        return true;
                    } else {
                        $this->msg_erreur = 'Un probleme est survenu pendant le chargement du fichier';
                        return false;
                    }
                } else {
                    $this->msg_erreur = 'Le fichier a une mauvaise extension (ext : ' . $this->uploadedFileExtension . ')';
                    return false;
                }
            } else {
                $this->msg_erreur = 'Le fichier est trop lourd (size : ' . $this->uploadedFileSize . ')';
                return false;
            }
        } else {
            $this->msg_erreur = 'Aucun fichier a uploader';
            return false;
        }
    }

    /**
     * Méthode pour la recuperation de fichiers distants
     *
     * @function doRetrieval
     * @param string $url Url complete du fichier distant (obligatoire)
     * @param string $new_name Nouveau nom du fichier sans extension (facultatif)
     * @param bool $erase Permet de remplacer le fichier sur le serveur s'il existe deja (facultatif)
     * @return bool
     */
    public function doRetrieval($url, $new_name = '', $erase = false)
    {
        // On explode l'url et on compte le nombre d'elements retournes pour pouvoir recuperer le nom du fichier (=> le dernier element de l'url)
        $tab = explode('/', $url);
        $taille = count($tab);
        $nom_tmp = $tab[$taille - 1];

        // Recuperation de l'extension du fichier
        $ext_tmp = explode('.', $nom_tmp);
        $this->uploadedFileExtension = $ext_tmp[1];
        $this->uploadedFileName = $ext_tmp[0];

        // Si l'on a pas donné un nouveau nom au fichier, il garde le nom d'origine
        if ($new_name == '') {
            // On clean le nom d'origine
            $this->uploadedFileName = $this->clean_name($this->uploadedFileName);
        } else {
            // On applique sur le nouveau nom
            $this->uploadedFileName = $new_name . '.' . $this->uploadedFileExtension;
        }

        // On commence par verifier que le dossier d'upload existe sinon on le créé
        $this->file_dir = explode('/', $this->file_dir);

        for ($i = 0; $i < count($this->file_dir); $i++) {
            if ($this->file_dir[$i] != '') {
                $this->path_dir .= '/' . $this->file_dir[$i];

                if (!is_dir($this->path_dir)) {
                    if (!mkdir($this->path_dir, 0777)) {
                        $this->msg_erreur = 'Impossible de creer le repertoire : ' . $this->path_dir;
                        return false;
                    }
                }
            }
        }

        // On verifie que le fichier existe bien
        if (file_get_contents($url)) {
            // On verifie que le fichier est bien d'une extension valide
            if (in_array($this->uploadedFileExtension, $this->ext_valides)) {
                // On vérifie que le nom de fichier n'est pas déja utilisé sinon on lui rajoute le timestamp a la fin seulement si le erase est a false
                if (file_exists($this->upload_dir . $this->uploadedFileName) && $erase == false) {
                    sleep(1); // Temporisation du script pour eviter d'avoir une image avec le même nom (time() s'increment toutes les secondes)
                    $this->uploadedFileName = $this->uploadedFileName . '-' . time();
                }

                // Si le fichier est ok, on l'upload sur le serveur
                if (copy($url, $this->upload_dir . $this->uploadedFileName)) {
                    // On donne un acces total sur le fichier
                    chmod($this->upload_dir . $this->uploadedFileName, 0777);
                    return true;
                } else {
                    $this->msg_erreur = 'Un probleme est survenu pendant le chargement du fichier';
                    return false;
                }
            } else {
                $this->msg_erreur = 'Le fichier a une mauvaise extension (ext : ' . $this->uploadedFileExtension . ')';
                return false;
            }
        } else {
            $this->msg_erreur = 'Aucun fichier a uploader';
            return false;
        }
    }
}
