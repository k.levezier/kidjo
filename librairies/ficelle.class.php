<?php

/**
 * La classe Ficelle permet de traiter les strings
 *
 * @class       ficelle
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class ficelle
{
    /**
     * @var array $params Tableau de paramètres
     */
    private $params;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param array $params Tableau de paramètres
     */
    public function __construct($params = array())
    {
        $this->params = $params;
    }

    /**
     * Méthode qui permet de déterminer si la chaïne en entrée est bien une
     * adresse email
     *
     * @function isEmail
     * @param string $email Chaïne à vérifier
     * @return boolean
     */
    public function isEmail($email)
    {
        if (preg_match('#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,99}$#', $email)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Méthode qui permet générer un mot de passe
     *
     * @function generatePassword
     * @param integer $length Nombre de caractères pour le mot de passe
     * @return string
     */
    public function generatePassword($length = 8)
    {
        $min_length = 8;
        $nb = max($min_length, $length);
        return substr(base64_encode(openssl_random_pseudo_bytes((int)$nb + 1)), 0, (int)$nb);
    }

    /**
     * Méthode qui permet de couper une chaïne
     *
     * @function resumeBrut
     * @param string $texte Texte à couper
     * @param integer $nbreCar Longueur à garder en nbre de caractères
     * @return string
     */
    public function resumeBrut($texte, $nbreCar)
    {
        return (strlen($texte) > $nbreCar ? substr($texte, 0, $nbreCar) . ' [...]' : $texte);
    }

    /**
     * Méthode qui permet de couper une chaïne en gardant le formatage HTML
     *
     * @function resume
     * @param string $texte Texte à couper
     * @param integer $nbreCar Longueur à garder en nbre de caractères
     * @return string
     */
    public function resume($texte, $nbreCar)
    {
        $LongueurTexteBrutSansHtml = strlen(strip_tags($texte));

        if ($LongueurTexteBrutSansHtml < $nbreCar) {
            return $texte;
        }

        $MasqueHtmlSplit = '#</?([a-zA-Z1-6]+)(?: +[a-zA-Z]+="[^"]*")*( ?/)?>#';
        $MasqueHtmlMatch = '#<(?:/([a-zA-Z1-6]+)|([a-zA-Z1-6]+)(?: +[a-zA-Z]+="[^"]*")*( ?/)?)>#';
        $texte .= ' ';
        $BoutsTexte = preg_split($MasqueHtmlSplit, $texte, -1, PREG_SPLIT_OFFSET_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $NombreBouts = count($BoutsTexte);

        if ($NombreBouts == 1) {
            $longueur = strlen($texte);
            return substr($texte, 0, strpos($texte, ' ', ($longueur > $nbreCar ? $nbreCar : $longueur)));
        }

        $longueur = 0;
        $indexDernierBout = $NombreBouts - 1;
        $position = $BoutsTexte[$indexDernierBout][1] + strlen($BoutsTexte[$indexDernierBout][0]) - 1;
        $indexBout = $indexDernierBout;
        $rechercheEspace = true;

        foreach ($BoutsTexte as $index => $bout) {
            $longueur += strlen($bout[0]);

            if ($longueur >= $nbreCar) {
                $position_fin_bout = $bout[1] + strlen($bout[0]) - 1;
                $position = $position_fin_bout - ($longueur - $nbreCar);

                if (($positionEspace = strpos($bout[0], ' ', $position - $bout[1])) !== false) {
                    $position = $bout[1] + $positionEspace;
                    $rechercheEspace = false;
                }

                if ($index != $indexDernierBout) {
                    $indexBout = $index + 1;
                }
                break;
            }
        }

        if ($rechercheEspace === true) {
            for ($i = $indexBout; $i <= $indexDernierBout; $i++) {
                $position = $BoutsTexte[$i][1];

                if (($positionEspace = strpos($BoutsTexte[$i][0], ' ')) !== false) {
                    $position += $positionEspace;
                    break;
                }
            }
        }

        $texte = substr($texte, 0, $position);
        $retour = array();
        preg_match_all($MasqueHtmlMatch, $texte, $retour, PREG_OFFSET_CAPTURE);
        $BoutsTag = array();

        foreach ($retour[0] as $index => $tag) {
            if (isset($retour[3][$index][0])) {
                continue;
            }

            if ($retour[0][$index][0][1] != '/') {
                array_unshift($BoutsTag, $retour[2][$index][0]);
            } else {
                array_shift($BoutsTag);
            }
        }

        if (!empty($BoutsTag)) {
            foreach ($BoutsTag as $tag) {
                $texte .= '</' . $tag . '>';
            }
        }

        if ($LongueurTexteBrutSansHtml > $nbreCar) {
            $texte .= ' [...]';
            $texte = str_replace('</p> [......]', '[...] </p>', $texte);
            $texte = str_replace('</ul> [......]', '[...] </ul>', $texte);
            $texte = str_replace('</div> [......]', '[...] </div>', $texte);
        }

        return $texte;
    }
}
