<?php

/**
 * Classe de cryptage réversible de données
 * Cette classe permet de coder ou décoder une chaîne de caractères
 *
 * @class       daVinciCode
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class daVinciCode
{
    /**
     * $var string $key Clé utilisée pour générer le cryptage
     * $var string $data Donnée à crypter
     */
    public $key;
    public $data;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param string $key Clé utilisée pour générer l'encodage
     */
    public function __construct($key)
    {
        $this->key = sha1($key);
    }

    /**
     * Méthode qui permet d'encoder la donnée
     *
     * @function encode
     * @param string $string Chaîne à encoder
     * @return string Chaîne encodée
     */
    public function encode($string)
    {
        $this->data = '';

        for ($i = 0; $i < strlen($string); $i++) {
            $kc = substr($this->key, ($i % strlen($this->key)) - 1, 1);
            $this->data .= chr(ord($string{$i}) + ord($kc));
        }

        $this->data = base64_encode($this->data);

        return $this->data;
    }

    /**
     * Méthode qui permet de décoder la donnée
     *
     * @function decode
     * @param string $string Chaîne à décoder
     * @return string Chaîne décodée
     */
    public function decode($string)
    {
        $this->data = '';
        $string_tmp = base64_decode($string);

        for ($i = 0; $i < strlen($string_tmp); $i++) {
            $kc = substr($this->key, ($i % strlen($this->key)) - 1, 1);
            $this->data .= chr(ord($string_tmp{$i}) - ord($kc));
        }

        return $this->data;
    }

}
