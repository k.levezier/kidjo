<?php

/**
 * Environnement
 * L'environnement est récupéré par la variable serveur définie dans le .htaccess
 * @env : dev, demo ou prod
 */
$config['env'] = (!empty($_SERVER['REDIRECT_APPENV']) ? $_SERVER['REDIRECT_APPENV'] : $_SERVER['APPENV']);

/**
 * Configuration de la BDD
 * Remplissage des tableaux de config pour les 3 environnements
 * @HOST : Host du serveur MySQL
 * @USER : Utilisateur de la BDD
 * @PASSWORD : Mot de passe de l'utilisateur
 * @BDD : Nom de la base de donnée
 * @BDD_COMMUN : Nom de la base de donnée commune
 * @NOM : Nom de la BDD dans les éléments de debug
 */
$config['bdd_config']['dev']['HOST'] = 'mysql';
$config['bdd_config']['dev']['USER'] = 'root';
$config['bdd_config']['dev']['PASSWORD'] = 'root';
$config['bdd_config']['dev']['BDD'] = 'kidjo_dev';
$config['bdd_config']['dev']['BDD_COMMUN'] = 'kidjo_dev';
$config['bdd_config']['dev']['NOM'] = 'kidjo_dev';

$config['bdd_config']['demo']['HOST'] = 'db-mutu.cylncloud.local';
$config['bdd_config']['demo']['USER'] = 'kidjo';
$config['bdd_config']['demo']['PASSWORD'] = 'GJ3vw9ux62YX';
$config['bdd_config']['demo']['BDD'] = 'kidjo_demo';
$config['bdd_config']['demo']['BDD_COMMUN'] = 'kidjo_demo';
$config['bdd_config']['demo']['NOM'] = 'kidjo_demo';

$config['bdd_config']['prod']['HOST'] = 'db-mutu.cylncloud.local';
$config['bdd_config']['prod']['USER'] = 'kidjo';
$config['bdd_config']['prod']['PASSWORD'] = 'GJ3vw9ux62YX';
$config['bdd_config']['prod']['BDD'] = 'kidjo';
$config['bdd_config']['prod']['BDD_COMMUN'] = 'kidjo';
$config['bdd_config']['prod']['NOM'] = 'kidjo';

/**
 * Configuration de la BDD
 * Remplissage des tableaux d'options pour les 3 environnements
 * @DISPLAY_ERREUR : Activer les erreurs MySQL et le debug MySQL
 * @BDD_PANIC_SEUIL : Seuil en secondes pour indiquer dans le debug qu'une requête est longue
 */
$config['bdd_option']['dev']['DISPLAY_ERREUR'] = true;
$config['bdd_option']['dev']['BDD_PANIC_SEUIL'] = 10;

$config['bdd_option']['demo']['DISPLAY_ERREUR'] = false;
$config['bdd_option']['demo']['BDD_PANIC_SEUIL'] = 10;

$config['bdd_option']['prod']['DISPLAY_ERREUR'] = false;
$config['bdd_option']['prod']['BDD_PANIC_SEUIL'] = 10;

/**
 * Variables d'environnement
 * @url : URL d'accès aux applications (default, admin, ...)
 * @static_url : URL d'accès aux éléments statiques des applications (CSS, JS, images, ...)
 * @path : Chemin d'accès sur le serveur
 * @user_path : Chemin d'accès aux fichiers utilisateurs sur le serveur
 * @static_path : Chemin d'accès au dossier static sur le serveur
 */
$config['url']['dev']['default'] = 'http://kidjo-static.local.equinoa.net';
$config['url']['demo']['default'] = 'https://kidjo-static.equinoa.net';
$config['url']['prod']['default'] = '';

$config['url']['dev']['admin'] = 'http://kidjo-bo.local.equinoa.net';
$config['url']['demo']['admin'] = 'https://kidjo-bo.equinoa.net';
$config['url']['prod']['admin'] = '';

$config['static_url']['dev'] = 'http://kidjo-static.local.equinoa.net';
$config['static_url']['demo'] = 'https://kidjo-static.equinoa.net';
$config['static_url']['prod'] = '';

$config['path']['dev'] = dirname(dirname(getcwd())) . '/';
$config['path']['demo'] = dirname(dirname(getcwd())) . '/';
$config['path']['prod'] = dirname(dirname(getcwd())) . '/';

$config['user_path']['dev'] = dirname(dirname(getcwd())) . '/public/default/var/';
$config['user_path']['demo'] = dirname(dirname(getcwd())) . '/public/default/var/';
$config['user_path']['prod'] = dirname(dirname(getcwd())) . '/public/default/var/';

$config['static_path']['dev'] = dirname(dirname(getcwd())) . '/public/default/';
$config['static_path']['demo'] = dirname(dirname(getcwd())) . '/public/default/';
$config['static_path']['prod'] = dirname(dirname(getcwd())) . '/public/default/';

/**
 * Gestion des langues
 * @enabled : Activation du multilanguage
 * @domain_default_languages : Tableau des langues par défaut d'un domaine (ex: ['fr.aspartam.io' => 'fr'])
 * @allowed_languages : Liste des langues disponibles, la première est la langue par défaut
 */
$config['multilanguage']['enabled'] = false;
$config['multilanguage']['domain_default_languages'] = [];
$config['multilanguage']['allowed_languages'] = ['fr' => 'Français'];
$config['multilanguage']['allowed_locale'] = ['fr' => 'fr_FR'];

/**
 * Gestion de l'URL
 * @separator : Séparateur entre le nom et la valeur du paramètre de l'URL
 */
$config['params']['separator'] = '___';

/**
 * Gestion des minifications
 * Le choix peut se faire par application
 * @jsmin : Activation de la minification des JS
 * @cssmin : Activation de la minification des CSS
 * @currentVersion : Version des JS et CSS
 */
$config['params']['default']['jsmin'] = false;
$config['params']['static']['jsmin'] = false;
$config['params']['admin']['jsmin'] = false;

$config['params']['default']['cssmin'] = false;
$config['params']['static']['cssmin'] = false;
$config['params']['admin']['cssmin'] = false;

$config['params']['default']['currentVersion'] = 1;
$config['params']['static']['currentVersion'] = 1;
$config['params']['admin']['currentVersion'] = 1;

/**
 * Adresses IP des administrateurs
 * @ip_admin : Tableau avec les adresses IP
 */
$config['ip_admin']['dev'] = ['91.151.70.8', '172.18.0.1'];
$config['ip_admin']['demo'] = [];
$config['ip_admin']['prod'] = ['91.151.70.8', '172.18.0.1'];

/**
 * Gestion des erreurs
 * @activate : Activation de la gestion des erreurs
 * @file : Chemin vers le fichier de log des erreurs
 * @allow_display : Afficher les erreurs sur la page (0 ou 1)
 * @allow_log : Enregistrer les erreurs dans le fichier de log (0 ou 1)
 * @report : Liste des erreurs que l'on enregistre
 */
$config['error_handler']['dev']['activate'] = true;
$config['error_handler']['dev']['file'] = $config['path']['dev'] . 'logs/error-dev.log';
$config['error_handler']['dev']['allow_display'] = '1';
$config['error_handler']['dev']['allow_log'] = '1';
$config['error_handler']['dev']['report'] = E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR;

$config['error_handler']['demo']['activate'] = true;
$config['error_handler']['demo']['file'] = $config['path']['demo'] . 'logs/error-demo.log';
$config['error_handler']['demo']['allow_display'] = '0';
$config['error_handler']['demo']['allow_log'] = '1';
$config['error_handler']['demo']['report'] = E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR;

$config['error_handler']['prod']['activate'] = true;
$config['error_handler']['prod']['file'] = $config['path']['prod'] . 'logs/error-prod.log';
$config['error_handler']['prod']['allow_display'] = '0';
$config['error_handler']['prod']['allow_log'] = '1';
$config['error_handler']['prod']['report'] = E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR;

/**
 * Constantes de la BDD
 * Remplissage des constantes de config et option de la BDD
 * @BDD_CONFIG : serialize du tableau $config['bdd_config']
 * @BDD_OPTION : serialize du tableau $config['bdd_option']
 * @BDD_COMMUN : constante du nom de la table commune
 */
define('BDD_CONFIG', serialize($config['bdd_config'][$config['env']]));
define('BDD_OPTION', serialize($config['bdd_option'][$config['env']]));
define('BDD_COMMUN', $config['bdd_config'][$config['env']]['BDD_COMMUN']);

/**
 * Clef de chiffrage
 * Permet de chiffrer le contenu des settings en BDD
 * @CRYPT_KEY : valeur de la clef de chiffrage du site
 */
define('CRYPT_KEY', '~Rx!*^_+45-=:9U:+3c=!%9C6=%77D5__T:%^E5~S-Y~_-Q9.R');

/**
 * Activation des logs du BO
 * @ACTIVATE_LOGSBO : true ou false
 */
define('ACTIVATE_LOGSBO', true);

/**
 * Activation des logs des 404 FO
 * @ACTIVATE_LOGS404 : true ou false
 */
define('ACTIVATE_LOGS404', true);

/**
 * Mail SMTP Configuration
 */
define('MAIL_SERVER', 'smtp.mailgun.org');
define('MAIL_PORT', 25);
define('MAIL_LOGIN', 'postmaster@sandbox916913f34de745bbb59d86fe74480e09.mailgun.org');
define('MAIL_PASSWORD', 'A6d35eec670b7771b3fe98f8f7e87a9cd');
