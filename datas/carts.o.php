<?php

namespace o;

class carts_core extends instance {

    /**
     * Méthode pour la récupération de l'ordre max d'un élément
     * dans le couple template / groupe
     *
     * @function getMaxOrdre
     * @param int $post $_POST du template/bloc
     * @param int $id_groupe ID du groupe
     * @param string $type type associé ('template','bloc')
     * @return int
     */
    public function test() {
        echo "ok";
        return 1;
    }


    public function getFourProduct() {

        echo "ok";
        return 1;
    }

    public function getSharedToMe() {

        // Si on est admin tous les paniers partagés apparaissent
        if($_SESSION['client']['status']==2){


            $lCarts = new data('carts');
            $lCarts->addWhere("shared = 1 AND send > '".date('Y-m-d 00:00:00',strtotime('-15 days'))."'");
            $lCarts->order('added','DESC');

            $returnCarts = [];
            foreach($lCarts as $c) {
                $returnCarts[$c['id_cart']] = $c;
            }
        }else{
            // On va chercher les zones et profiles du client
            $zonesOk = [];
            $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
            foreach($zones as $z){
                $zonesOk[] = $z->id_geozone;
            }

            $profilesOk = [];
            $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
            foreach($profiles as $p){
                $profilesOk[] = $p->id_profile;
            }

            $lCarts = new data('carts');
            $lCarts->where('shared',1)
                ->where('send','< "'.date('Y-m-d 00:00:00').'"')
                ->order('send','DESC');


            $returnCarts = [];
            foreach($lCarts as $c){
                // Par défaut on authorise
                $authProfil = true;
                $authGeozone = true;

                $carts_profils = new data('carts_profiles');
                foreach($carts_profils->where("id_cart",$c['id_cart']) as $p){
                    $authProfil = false; // si on a au moins un profil alors on limite
                    if(in_array($p['id_profile'],$profilesOk)){$authProfil = true; break;}
                    $this->cartProfiles[] = $p['id_profile'];
                }
                $carts_geozones = new data('carts_geozones');
                foreach($carts_geozones->where("id_cart",$c['id_cart']) as $z){
                    $authGeozone = false; // si on a au moins un profil alors on limite
                    $this->cartGeozone[] = $z['id_geozone'];
                    if(in_array($z->id_geozone,$zonesOk)){$authGeozone = true; break;}
                }

                if($authProfil && $authGeozone){
                    $returnCarts[$c['id_cart']] = $c;
                }
            }
        }

        return $returnCarts;
    }

    public function getClientsAuthorized() {

        $lClients = [];
        // On ajoute tous les admin
        $admins = new data('clients');
        $admins->where('status',2);
        foreach($admins as $a){
            $lClients[$a->id_client] = $a;
        }

        // Pour les clients
        $lClientsZones = $lClientsProfiles = [];

        // On va chercher les clients dans les zones séléctionnées
        $carts_geozones = new data('carts_geozones');
        foreach($carts_geozones->where("id_cart",$this->id_cart) as $z){
            $clients_geozones = new data('clients_geozones');
            foreach ($clients_geozones->where('id_geozone',$z->id_geozone) as $c){
                $lClientsZones[] = $c->id_client;
            }
        }
        // On check si ils ont le bon profil
        $carts_profils = new data('carts_profiles');
        foreach($carts_profils->where("id_cart",$this->id_cart) as $p){
            $clients_profiles = new data('clients_profiles');
            foreach ($clients_profiles->where('id_profile',$p->id_profile) as $c){
                $lClientsProfiles[] = $c->id_client;
            }
        }

        $lClientsTemp = array_intersect($lClientsZones,$lClientsProfiles);
        $clients = new data('clients');
        foreach($clients as $c){
            if(in_array($c->id_client,$lClientsTemp) && $c->status > 0){
                $lClients[$c->id_client] = $c;
            }
        }

        return $lClients;
    }

    public function authorized(){

        // Si l'utilisateur est le propriétaire
        if($_SESSION['client']['id_client'] == $this->id_client)
            return true;

        // On va chercher les zones et profiles du client
        $zonesOk = [];
        $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($zones as $z){
            $zonesOk[] = $z->id_geozone;
        }

        $profilesOk = [];
        $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($profiles as $p){
            $profilesOk[] = $p->id_profile;
        }

        $authProfil = true;
        $authGeozone = true;

        $carts_profils = new data('carts_profiles');
        foreach($carts_profils->where("id_cart",$this->id_cart) as $p){
            $authProfil = false; // si on a au moins un profil alors on limite
            if(in_array($p['id_profile'],$profilesOk)){$authProfil = true; break;}
            $this->cartProfiles[] = $p['id_profile'];
        }
        $carts_geozones = new data('carts_geozones');
        foreach($carts_geozones->where("id_cart",$this->id_cart) as $z){
            $authGeozone = false; // si on a au moins un profil alors on limite
            $this->cartGeozone[] = $z['id_geozone'];
            if(in_array($z->id_geozone,$zonesOk)){$authGeozone = true; break;}
        }

        if($authProfil && $authGeozone)
            return true;
        else
            return false;
    }

    public function getThumbForShared($id_cart=NULL){
        if(is_null($id_cart))
            $id_cart = $this->id_cart;
        $docs = new data('carts_documents',['id_cart'=>$id_cart]);
        $lThumbs = [];
        foreach($docs as $d) {

            $doc = new documents(['id_document' => $d['id_document']]);
            $thumbnail = $extension = "";
            $activeLang = [];
            foreach (['en', 'fr', 'es'] as $ln) {
                $aTester = 'file_' . $ln;
                if (empty($activeLang) && !empty($doc->$aTester)) {
                    $activeLang = $ln;
                    $file = 'file_' . $ln;
                    $thumbnail = 'thumbnail_' . $ln;
                    $extension = 'extension_' . $ln;
                    $size = 'size_' . $ln;
                }
            }

            $lThumbs[] = ['thumbnail'=>$doc->$thumbnail,'extension'=>$doc->$extension];
            if (count($docs) < 4 || count($lThumbs) >= 4) { // Si il y a moins de 4 produits ou si il y a 4 thumb on break and dance!
                break;
            }
        }

        return $lThumbs;


        // Sinon on fait un mix des 4 1er document
    }
}
