<?php

namespace o;

class traductions_core extends instance
{
    /**
     * Permet de créer les champs pour les nouvelles
     * langues au chargement dans le BO Traductions
     *
     * @function checkNewLanguage
     */
    public function checkNewLanguage()
    {
        $tabLangues = array_keys($this->tabLangues);
        $sqlSections = new data('traductions');
        foreach ($sqlSections->section->distinct() as $section) {
            $sqlNoms = new data('traductions', array('section' => $section));
            foreach ($sqlNoms->nom->distinct() as $nom) {
                $trad = new data('traductions', array('section' => $section, 'nom' => $nom));
                foreach (array_diff($tabLangues, $trad->id_langue->getArray()) as $code) {
                    $newtrad = new traductions();
                    $newtrad->id_langue = $code;
                    $newtrad->section = $section;
                    $newtrad->nom = $nom;
                    $newtrad->texte = '[' . $code . ']';
                    $newtrad->insert();
                }
            }
        }
    }

    /**
     * Récupération de la traduction
     *
     * @function txt
     * @param string $section Section de la traduction
     * @param string $nom Nom de la traduction
     * @param string $id_langue Langue en cours
     * @param string $texte Texte par défaut si rien n'est trouvé en base
     *   les valeurs de type {cle} seront remplacées par les 'value' correspondant à 'cle' de $remplacements si définies
     * @param array $remplacements
     *   eg. 'cle' => 'value',
     * @return string
     */
    public function txt($section, $nom, $id_langue, $texte = '', $remplacements = array())
    {
        $traduction = '';
        $trad = new traductions(array('section' => $section, 'nom' => $nom, 'id_langue' => $id_langue));
        if ($trad->exist()) {
            $traduction = $trad->texte;
            foreach ($this->tabLangues as $code => $langue) {
                if ($code != $id_langue) {
                    $newtrad = new traductions(array('section' => $section, 'nom' => $nom, 'id_langue' => $code));
                    if (!$newtrad->exist()) {
                        $newtrad->id_langue = $code;
                        $newtrad->section = $section;
                        $newtrad->nom = $nom;
                        $newtrad->texte = '[' . $langue . '] ' . $texte;
                        $newtrad->insert();
                    }
                }
            }
        } else {
            foreach ($this->tabLangues as $code => $langue) {
                if ($code == $id_langue) {
                    $trad->id_langue = $code;
                    $trad->section = $section;
                    $trad->nom = $nom;
                    $trad->texte = $texte;
                    $trad->insert();
                } else {
                    $newtrad = new traductions(array('section' => $section, 'nom' => $nom, 'id_langue' => $code));
                    if (!$newtrad->exist()) {
                        $newtrad->id_langue = $code;
                        $newtrad->section = $section;
                        $newtrad->nom = $nom;
                        $newtrad->texte = '[' . $langue . '] ' . $texte;
                        $newtrad->insert();
                    }
                }
            }
            $trad = new traductions(array('section' => $section, 'nom' => $nom, 'id_langue' => $id_langue));
            if ($trad->exist()) {
                $traduction = $trad->texte;
            } else {
                $traduction = '';
            }
        }

        if (!empty($remplacements) && is_array($remplacements)) {
            foreach ($remplacements as $cle => $value) {
                $traduction = str_replace(
                    '{' . mb_ereg_replace('\{(.*)\}', '\1', $cle) . '}',
                    $value,
                    $traduction
                );
            }
        }
        return (string)$traduction;
    }

    /**
     * Récupération de la liste des sections de traduction
     *
     * @function listeSections
     * @param string $id_langue Langue en cours
     * @return array
     */
    public function listeSections($id_langue)
    {
        $liste = array();
        $sqlSections = new data('traductions', array('id_langue' => $id_langue));
        foreach ($sqlSections->section->distinct()->order('section', 'ASC') as $section) {
            $liste[] = $section;
        }
        return (array)$liste;
    }

    /**
     * Récupération de la liste des traductions d'une section
     * avec filtrage sur le nom et le contenu
     *
     * @function listeTraductionsSection
     * @param string $section Nom de la section
     * @param string $filtre Filtre de recherche
     * @return array
     */
    public function listeTraductionsSection($section, $filtre)
    {
        $liste = array();
        $traductions = new data('traductions', array('section' => $section));
        if (!empty($filtre)) {
            $traductions->addWhere('`nom` LIKE "%' . $filtre . '%" OR `texte` LIKE "%' . $filtre . '%"');
        }
        foreach ($traductions->nom->distinct() as $nom) {
            $sectrads = new data('traductions', array('section' => $section, 'nom' => $nom));
            foreach ($sectrads->order('nom', 'ASC') as $traduc) {
                $liste[$traduc->id_langue][] = $traduc->getArray();
            }
        }
        return (array)$liste;
    }

    /**
     * Récupération de la liste des traductions
     * pour une recherche
     *
     * @function listeTraductionsFiltre
     * @param string $filtre Filtre de recherche
     * @return array
     */
    public function listeTraductionsFiltre($filtre)
    {
        $liste = array();
        $lsections = new data('traductions');
        $lsections->addWhere('`nom` LIKE "%' . $filtre . '%" OR `texte` LIKE "%' . $filtre . '%"');
        foreach ($lsections->section->distinct()->order('section', 'ASC') as $section) {
            $lnoms = new data('traductions', array('section' => $section));
            $lnoms->addWhere('`nom` LIKE "%' . $filtre . '%" OR `texte` LIKE "%' . $filtre . '%"');
            foreach ($lnoms->nom->distinct()->order('nom', 'ASC') as $nom) {
                $traductions = new data('traductions', array('section' => $section, 'nom' => $nom));
                foreach ($traductions->order('nom', 'ASC') as $traduc) {
                    $liste[$section][$traduc->id_langue][] = $traduc->getArray();
                }
            }
        }
        return (array)$liste;
    }
}
