<?php

namespace o;

class menus_core extends instance
{
    /**
     * Récupération du contenu d'un menu
     *
     * @function getMenu
     * @param string $slug Slug du menu
     * @param string $lurl Url du site avec la langue
     * @param string $id_langue Langue en cours (optionnel pour utilisation des conditions par défault)
     * @return array
     */
    public function getMenu($slug, $lurl, $id_langue = null)
    {
        $clFonctions = new \clFonctions($lurl, (new traductions()));
        $leMenu = array();
        $menus = new menus(array('slug' => $slug, 'status' => 1));
        if ($menus->exist()) {
            $leMenu['nom'] = $menus->nom;
            $menus_content = $menus->menus_contents->where(array('status' => 1));
            if ($id_langue !== null) {
                $menus_content->where(array('id_langue' => $id_langue));
            }

            $lesLiens = array();
            if ($menus_content->count() > 0) {
                foreach ($menus_content->order('ordre') as $link) {
                    $lesLiens[] = array(
                        'id_langue' => $link->id_langue,
                        'id_tree'   => ($link->complement == 'L' ? $link->value : ''),
                        'nom'       => $link->nom,
                        'lien'      => $clFonctions->getLink($link),
                    );
                }
            }
            $leMenu['liens'] = $lesLiens;
        }
        return $leMenu;
    }
}
