<?php

namespace o;

class tree_core extends instance {

    /**
     * Récupération de l'arbo du site
     *
     * @function getArboSite
     * @param string $id_langue Langue en cours
     * @param integer $id_parent ID du parent
     * @param integer $filtre Version filtre ou non
     * @param array $lPages Tableau de l'arbo
     * @return array
     */
    public function getArboSite($id_langue, $id_parent, $filtre = 0, $lPages = array()) {
        $tree = new data('tree', array('id_parent' => $id_parent, 'id_langue' => $id_langue, 'filtre' => ($filtre == 0 ? 0 : 1)));
        $lPages = array();

        foreach($tree->order('ordre', 'ASC') as $page) {
            $lPages[] = array(
                'id_tree' => $page->id_tree,
                'slug' => $page->slug,
                'id_langue' => $page->id_langue,
                'title' => $page->title,
                'menu_title' => $page->menu_title,
                'prive' => $page->prive,
                'status' => $page->status,
                'up' => ($page->ordre == $this->getFirstPosition($id_parent) ? false : true),
                'down' => ($page->ordre == $this->getLastPosition($id_parent) ? false : true),
                'children' => $this->getArboSite($id_langue, $page->id_tree, $filtre, $lPages));
        }

        return (array) $lPages;
    }

    /**
     * Récupération de l'arbo du site
     *
     * @function getArboSite
     * @param string $id_langue Langue en cours
     * @param integer $id_parent ID du parent
     * @param integer $filtre Version filtre ou non
     * @param array $lPages Tableau de l'arbo
     * @return array
     */
    public function getArboSiteZoneProfil($id_langue, $id_parent, $filtre = 0, $lPages = array()) {
        $tree = new data('tree', array('id_parent' => $id_parent, 'id_langue' => $id_langue, 'status' => 1, 'status_menu' => 1,'filtre' => ($filtre == 0 ? 0 : 1)));
        $lPages = array();

        $zonesOk = [];
        $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($zones as $z){
            $zonesOk[] = $z->id_geozone;
        }

        $profilesOk = [];
        $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($profiles as $p){
            $profilesOk[] = $p->id_profile;
        }

        foreach($tree->order('ordre', 'ASC') as $page) {
            $zoneDone = false;
            $zones = new data('tree_geozones',['id_tree'=>$page->id_tree]);

            if(count($zones)==0){
                $zoneDone = true;
            }else{
                foreach($zones as $key=>$z){
                    if(in_array($z->id_geozone,$zonesOk)){$zoneDone = true; break;}
                }
            }

            $profilDone = false;
            $profiles = new data('tree_profiles',['id_tree'=>$page->id_tree]);
            foreach($profiles as $p){
                if(in_array($p->id_profile,$profilesOk)){$profilDone = true; break;}
            }

            if($zoneDone && $profilDone){
                $lPages[] = array(
                    'id_tree' => $page->id_tree,
                    'slug' => $page->slug,
                    'id_langue' => $page->id_langue,
                    'title' => $page->title,
                    'menu_title' => $page->menu_title,
                    'prive' => $page->prive,
                    'status' => $page->status,
                    'up' => ($page->ordre == $this->getFirstPosition($id_parent) ? false : true),
                    'down' => ($page->ordre == $this->getLastPosition($id_parent) ? false : true),
                    'children' => $this->getArboSite($id_langue, $page->id_tree, $filtre, $lPages));
            }
        }

        return (array) $lPages;
    }

    public function authorized($id_tree = NULL){
        if(is_null($id_tree))
            $id_tree = $this->id_tree;
        $zonesOk = [];
        $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($zones as $z){
            $zonesOk[] = $z->id_geozone;
        }

        $profilesOk = [];
        $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($profiles as $p){
            $profilesOk[] = $p->id_profile;
        }


        $zoneDone = false;
        $zones = new data('tree_geozones',['id_tree'=>$id_tree]);

        if(count($zones)==0){
            $zoneDone = true;
        }else{
            foreach($zones as $key=>$z){
                if(in_array($z->id_geozone,$zonesOk)){$zoneDone = true; break;}
            }
        }

        $profilDone = false;
        $profiles = new data('tree_profiles',['id_tree'=>$id_tree]);
        foreach($profiles as $p){
            if(in_array($p->id_profile,$profilesOk)){$profilDone = true; break;}
        }

        if($zoneDone && $profilDone)
            return true;
        else
            return false;

    }

    public function getArboSiteWithStatus($id_langue, $id_parent, $filtre = 0, $lPages = array()) {
        $cachefile='arbo-'.$_SESSION['client']['id_client'].'-'.$id_parent;
        if(file_exists(dirname(dirname(getcwd())).'/cache/'.$cachefile))
            return json_decode(file_get_contents(dirname(dirname(getcwd())).'/cache/'.$cachefile),true);
        $tree = new data('tree', array('id_parent' => $id_parent, 'id_langue' => $id_langue,'status'=>1, 'filtre' => ($filtre == 0 ? 0 : 1)));

        $lPages = array();

        foreach($tree->order('ordre', 'ASC') as $page) {
            $where = 'status=1 AND id_document in (select id_document FROM documents_tree WHERE id_tree = ' . $page['id_tree'] . ') AND id_document in (SELECT id_document FROM documents_geozones where id_geozone in (' . $_SESSION['user_geozones'] . ')) AND id_document IN (SELECT id_document FROM documents_profiles where id_profile in (' . $_SESSION['user_profiles'] . '))';
            $docs = (new data('documents'))->addWhere($where);
            if($_SERVER['REMOTE_ADDR']=='109.7.252.175')
            {
                $_SESSION['count']++;
                echo 'timing req:'.mktime();
                //echo $where;
            }
            $nbre_docs = count($docs);

            $children = $this->getArboSiteWithStatus($id_langue, $page->id_tree, $filtre, $lPages);

            if($children == ""){
                $nbr_of_children = 0;
            } else {
                $nbr_of_children = count($children);
            }

            if($nbre_docs != 0 || $nbr_of_children != 0){
                $lPages[] = array(
                    'id_tree' => $page->id_tree,
                    'slug' => $page->slug,
                    'id_langue' => $page->id_langue,
                    'title' => $page->title,
                    'menu_title' => $page->menu_title,
                    'status' => $page->status,
                    'up' => ($page->ordre == $this->getFirstPosition($id_parent) ? false : true),
                    'down' => ($page->ordre == $this->getLastPosition($id_parent) ? false : true),
                    'children' => $children,
                    'nbr_of_children' => $nbr_of_children,
                    'nbr_docs' => $nbre_docs
                );
            }
        }

        if(!empty($lPages)){
            file_put_contents(dirname(dirname(getcwd())).'/cache/'.$cachefile,json_encode($lPages));
            return (array) $lPages;
        } else {
            return '';
        }
    }


    /**
     * Récupération du contenu d'un élément de la page
     *
     * @function getContent
     * @param string $slug Slug de l'élément
     * @return string
     */
    public function getContentOld($slug) {
        var_dump($slug);
        $obj =  $this->tree_elements->where(array('element->slug' => $slug, 'element->status' => 1));
        var_dump(empty($obj->value));
        return NULL;
        return $obj->getInstance()->value;
    }

    /**
     * Récupération du contenu d'un élément de la page
     *
     * @function getContent
     * @param string $slug Slug de l'élément
     * @return string
     */
    public function getContent($slug,$id_tree=NULL) {
        if(is_null($id_tree))
            $id_tree = $this->id_tree;
        $element = new elements(array('slug' => $slug,'status'=>1));
        $element = new tree_elements(array('id_tree' => $id_tree,'id_element' => $element->id_element));
        return $element->value;
    }

    /**
     * Récupération du contenu complémentaire d'un élément de la page
     *
     * @function getContentCompl
     * @param string $slug Slug de l'élément
     * @return string
     */
    public function getContentCompl($slug) {
        return $this->tree_elements->where(array('element->slug' => $slug, 'element->status' => 1))->getInstance()->complement;
    }

    /**
     * Récupération du contenu d'un élément d'une page enfant
     *
     * @function getChildContent
     * @param string $slug Slug de l'élément
     * @return string
     */
    public function getChildContent($slug) {
        return $this->childs->where(array('status' => 1))->tree_elements->where(array('element->slug' => $slug))->getInstance()->value;
    }

    /**
     * Récupération du contenu complémentaire d'un élément de de la page
     *
     * @function getChildContentCompl
     * @param string $slug Slug de l'élément
     * @return string
     */
    public function getChildContentCompl($slug) {
        return $this->childs->where(array('status' => 1))->tree_elements->where(array('element->slug' => $slug))->getInstance()->complement;
    }

    /**
     * Récupération de la première position des enfants d'un parent
     *
     * @function getFirstPosition
     * @param integer $id_parent ID du parent
     * @return integer
     */
    public function getFirstPosition($id_parent) {
        $ordrepage = new data('tree', array('id_parent' => $id_parent));
        $ordermin = min($ordrepage->ordre->getArray());
        return $ordermin;
    }

    /**
     * Récupération de la dernière position des enfants d'un parent
     *
     * @function getLastPosition
     * @param integer $id_parent ID du parent
     * @return integer
     */
    public function getLastPosition($id_parent) {
        $ordrepage = new data('tree', array('id_parent' => $id_parent));
        $ordermax = max($ordrepage->ordre->getArray());
        return $ordermax;
    }

    /**
     * Récupération du slug de la page
     *
     * @function getSlug
     * @param integer $id_tree ID de la page
     * @param string $id_langue Langue en cours
     * @return string
     */
    function getSlug($id_tree, $id_langue='fr')
    {
        // Protect
        $id_tree = $this->bdd->escape_string($id_tree);

        $tree = new tree(array('id_tree' => $id_tree, 'id_langue' => $id_langue));
        return $tree->slug;
    }

    /**
     * Déplacement d'une page dans son parent
     *
     * @function movePage
     * @param string $direction direction du déplacement
     * @return boolean
     */
    public function movePage($direction) {
        $return = false;

        if($direction === 'up') {
            $this->movePageUp($this->id_parent, $this->id_tree, $this->ordre);
            $this->reordre($this->id_parent);
            $return = true;
        } elseif($direction === 'down') {
            $this->movePageDown($this->id_parent, $this->id_tree, $this->ordre);
            $this->reordre($this->id_parent);
            $return = true;
        } else {
            $return = false;
        }
        return $return;
    }

    /**
     * Déplacement d'une page vers le haut
     *
     * @function movePageUp
     * @param int $id_parent ID du parent
     * @param int $id_tree ID de la page
     * @param int $ordre Ordre actuel de la page
     */
    public function movePageUp($id_parent, $id_tree, $ordre) {
        $this->bdd->query('UPDATE `tree` SET `ordre` = `ordre` + 1 WHERE `id_parent` = '.$id_parent.' AND `ordre` < '.$ordre.' ORDER BY `ordre` DESC LIMIT 1');
        $this->bdd->query('UPDATE `tree` SET `ordre` = `ordre` - 1 WHERE `id_tree` = '.$id_tree);
    }

    /**
     * Déplacement d'une page vers le bas
     *
     * @function movePageDown
     * @param int $id_parent ID du parent
     * @param int $id_tree ID de la page
     * @param int $ordre Ordre actuel de la page
     */
    public function movePageDown($id_parent, $id_tree, $ordre) {
        $this->bdd->query('UPDATE `tree` SET `ordre` = `ordre` - 1 WHERE `id_parent` = '.$id_parent.' AND `ordre` > '.$ordre.' ORDER BY `ordre` ASC LIMIT 1');
        $this->bdd->query('UPDATE `tree` SET `ordre` = `ordre` + 1 WHERE `id_tree` = '.$id_tree);
    }

    /**
     * Réordonner les pages d'un parent
     *
     * @function reordre
     * @param int $id_parent ID du parent
     */
    public function reordre($id_parent) {
        $tree = new data('tree', array('id_parent' => $id_parent));
        $tree->order('ordre', 'ASC');
        $ordre = 1;
        foreach ($tree->id_tree->distinct() as $id_tree) {
            $pages = new data('tree', array('id_tree' => $id_tree));
            $pages->ordre = $ordre;
            $pages->update();
            $ordre++;
        }
    }

    /**
     * Récupérer le parent avec template unlock
     *
     * @function getFirstUnlock
     * @param int $id_tree ID de la page
     * @param string $id_langue Langue en cours
     * @return integer
     *
     */
    public function getFirstUnlock($id_tree, $id_langue='fr')
    {
        // Protect
        $id_tree = $this->bdd->escape_string($id_tree);

        $tree = new tree(array('id_tree' => $id_tree, 'id_langue' => $id_langue));

        if($tree->template->affichage == 1)
            return $tree->id_tree;
        else
            return $this->getFirstUnlock($tree->id_parent, $id_langue);
    }

    /**
     * Construire le breadcrumb
     *
     * @function constructBreadCrumb
     * @param int $id_tree ID de la page
     * @param string $id_langue Langue en cours
     * @return array
     *
     */
    function constructBreadCrumb($id_tree, $id_langue='fr', $breadcrumb=array(), $first=true)
    {
        // Protect
        $id_tree = $this->bdd->escape_string($id_tree);

        $tree = new tree(array('id_tree' => $id_tree, 'id_langue' => $id_langue));

        $breadcrumb[] = $tree;

        if($tree->id_parent != 0)
        {
            $breadcrumb = $this->constructBreadCrumb($tree->id_parent, $id_langue, $breadcrumb, false);
        }

        return $breadcrumb;
    }

    /**
     * Récupérer le breadcrumb
     *
     * @function getBreadCrumb
     * @param int $id_tree ID de la page
     * @param string $id_langue Langue en cours
     * @return array
     *
     */
    public function getBreadCrumb($id_tree, $id_langue='fr')
    {
        // Protect
        $id_tree = $this->bdd->escape_string($id_tree);

        return array_reverse($this->constructBreadCrumb($id_tree, $id_langue));
    }

}
