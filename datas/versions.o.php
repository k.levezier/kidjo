<?php

namespace o;

class versions_core extends instance
{
    /**
     * Méthode pour la récupération de la version active
     * de la plateforme
     *
     * @function getVersionActive
     * @return array
     */
    public function getVersionActive()
    {
        $versions = new data('versions', array('active' => 1));
        $return = array();
        foreach ($versions->order('added', 'DESC')->limit(1) as $version) {
            $return = $version->getArray();
            break;
        }
        return $return;
    }
}
