<?php

namespace o;

class clients_core extends instance {

    /**
     * Méthode pour la récupération du total des commandes d'un client
     * 
     * @function getTotalCommandes
     * 
     * @return float 
     */
    public function getTotalCommandes() {
        $this->transactions->where('status', 1);
        return array_sum($this->transactions->montant->getArray())/100;
    }
    
    
    /**
     * Méthode pour vérifier le droit d'accès du client
     * 
     * @function checkAccess
     * 
     * @return boolean 
     */
    public function checkAccess()
    {
        if($_SESSION['auth'] != true || trim($_SESSION['token']) == '')
        {
            return false;
        }
        
        $access = new clients(array('id_client' => $_SESSION['client']['id_client'], 'password' => $_SESSION['client']['password']));
        
        return $access->exist();
    }
    
}
