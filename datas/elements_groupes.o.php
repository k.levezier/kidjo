<?php

namespace o;

class elements_groupes_core extends instance {

    /**
     * Méthode pour la récupération de l'ordre max d'un groupe
     * dans le template
     *
     * @function getMaxOrdre
     * @param array $params paramètres de sélection
     * @return int
     */
    public function getMaxOrdre($params) {
        $ordregpe = new data('elements_groupes', $params);
        $ordermax = max(array_merge(array(0),$ordregpe->ordre->getArray())) + 1;
        return $ordermax;
    }

}
