<?php

namespace o;

class sets_core extends instance
{
    /**
     * Méthode pour la récupération de la valeur d'un param
     *
     * @function getParamValue
     * @param string $slug Slug du param
     * @return string
     */
    public function getParamValue($slug)
    {
        $sets = new sets(array('slug' => $slug));
        return $sets->value;
    }
}
