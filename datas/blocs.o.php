<?php

namespace o;

class blocs_core extends instance
{
    /**
     * Récupération du contenu d'un élément du bloc
     *
     * @function getContent
     * @param string $slug Slug de l'élément
     * @param string $id_langue Langue en cours (optionnel pour utilisation des conditions par défault)
     * @return string
     */
    public function getContent($slug, $id_langue = null)
    {
        $blocs_elements = clone $this->blocs_elements;
        $content = $blocs_elements->where(array('element->slug' => $slug, 'element->status' => 1));
        if ($id_langue !== null) {
            $content->where(array('id_langue' => $id_langue));
        }
        return ($content->count() > 0 ? $content->current()->value : '');
    }

    /**
     * Récupération du complément d'un élément du bloc
     *
     * @function getComplement
     * @param string $slug Slug de l'élément
     * @param string $id_langue Langue en cours (optionnel pour utilisation des conditions par défault)
     * @return string
     */
    public function getComplement($slug, $id_langue = null)
    {
        $blocs_elements = clone $this->blocs_elements;
        $content = $blocs_elements->where(array('element->slug' => $slug, 'element->status' => 1));
        if ($id_langue !== null) {
            $content->where(array('id_langue' => $id_langue));
        }
        return ($content->count() > 0 ? $content->current()->complement : '');
    }

    /**
     * Récupération des contenus des élément du bloc
     *
     * @function content
     * @param string $id_langue Langue en cours (optionnel pour utilisation des conditions par défault)
     * @return array
     */
    public function content($id_langue = null)
    {
        $content = array();
        $blocs_elements = clone $this->blocs_elements;
        $elements = $blocs_elements->where(array('element->status' => 1));
        if ($id_langue !== null) {
            $elements->where(array('id_langue' => $id_langue));
        }
        foreach ($elements as $elt) {
            $content[$elt->element->slug] = $elt;
        }
        return $content;
    }

    /**
     * Récupération du lien d'un élément du bloc
     *
     * @function getLink
     * @param string $slug Slug de l'élément de type lien
     * @param string $lurl Url du site avec la langue
     * @param string $id_langue Langue en cours (optionnel pour utilisation des conditions par défault)
     * @return string
     */
    public function getLink($slug, $lurl, $id_langue = null)
    {
        $clFonctions = new \clFonctions($lurl, (new traductions()));
        $blocs_elements = clone $this->blocs_elements;
        $content = $blocs_elements->where(array('element->slug' => $slug, 'element->status' => 1));
        if ($id_langue !== null) {
            $content->where(array('id_langue' => $id_langue));
        }
        return ($content->count() > 0 ? $clFonctions->getLink($content->current()) : '');
    }

    /**
     * Méthode pour savoir si un groupe d'éléments est vide
     *
     * @function isEmptyGroup
     * @param string $slug Slug du groupe d'éléments
     * @return boolean
     */
    public function isEmptyGroup($slug)
    {
        $return = true;
        $elements_groupes = new data('elements_groupes', array('id_bloc' => $this->id_bloc, 'slug' => $slug));
        if ($elements_groupes->count() == 1) {
            $elements = $elements_groupes->elements;
            if ($elements->count() > 0) {
                foreach ($elements as $elt) {
                    foreach ($elt->blocs_elements->where(array('id_langue' => $this->id_langue)) as $be) {
                        if ($be->value != '') {
                            $return = false;
                            break;
                        }
                    }
                }
            }
        }

        return $return;
    }

}
