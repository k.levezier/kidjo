<?php

namespace o;

/**
 * La classe templates.o regroupe les méthodes liées aux templates.
 *
 * @class       templates_core
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class templates_core extends instance
{
    /**
     * Récupération de la liste des templates
     *
     * @function getListeTemplates
     * @param integer $status Statut des templates à lister
     * @param array $type tableau des types voulus
     * @return array
     */
    public static function getListeTemplates($status = 1, $type = array())
    {
        $status = (int)$status;
        $templates = new data('templates', array('status' => $status));
        if (count($type) > 0) {
            $templates->addWhere('type IN ("' . implode('","', $type) . '")');
        }
        $liste = array();
        foreach ($templates->order('type', 'ASC', '', 'name', 'ASC') as $tpl) {
            $liste[$tpl->type][] = $tpl->getArray();
        }
        return (array)$liste;
    }

    /**
     * Récupération du template, de ses groupes et de ses elements JSONifiés
     *
     * @function getJSON
     * @return array
     */
    public function getJSON()
    {
        $tplGroupes = array();
        foreach ($this->elements_groupes as $groupe) {
            $tplGroupes[] = $groupe->getArray();
        }

        $tplElements = array();
        foreach ($this->elements as $elt) {
            $tplElements[] = $elt->getArray();
        }

        return json_encode(array(
            'instance' => $this->getArray(),
            'groupes'  => $tplGroupes,
            'elements' => $tplElements,
        ));
    }

    /**
     * Import d'un template qui a été exporté
     *
     * @function importTpl
     * @param $arrayTpl Template à importer
     * @return array
     */
    public function importTpl($arrayTpl)
    {
        $newTpl = new templates();
        foreach ($arrayTpl['instance'] as $field => $value) {
            if (in_array($field, $newTpl->primary())) {
                continue;
            }
            $newTpl->{$field} = $value;
        }
        $newTpl->save();

        $id_groupe_match = array();
        foreach ($arrayTpl['groupes'] as $groupe) {
            $newGroupeTpl = new elements_groupes();
            foreach ($groupe as $gField => $gValue) {
                if (in_array($gField, $newGroupeTpl->primary())) {
                    continue;
                }
                $newGroupeTpl->{$gField} = $gValue;
            }
            $newGroupeTpl->id_template = $newTpl->id_template;
            $newGroupeTpl->save();
            $id_groupe_match[$groupe['id_groupe']] = $newGroupeTpl->id_groupe;
        }

        $id_element_match = array();
        foreach ($arrayTpl['elements'] as $elements) {
            $newElementTpl = new elements();
            foreach ($elements as $eField => $eValue) {
                if (in_array($eField, $newElementTpl->primary())) {
                    continue;
                }
                $newElementTpl->{$eField} = $eValue;
            }
            $newElementTpl->id_groupe = $id_groupe_match[$elements['id_groupe']];
            $newElementTpl->id_template = $newTpl->id_template;
            $newElementTpl->save();
            $id_element_match[$elements['id_element']] = $newElementTpl->id_element;
        }

        return array(
            'id_template'      => $newTpl->id_template,
            'id_element_match' => $id_element_match,
        );
    }
}
