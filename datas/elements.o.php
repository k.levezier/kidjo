<?php

namespace o;

class elements_core extends instance {

    /**
     * Méthode pour la récupération de l'ordre max d'un élément
     * dans le couple template / groupe
     *
     * @function getMaxOrdre
     * @param int $post $_POST du template/bloc
     * @param int $id_groupe ID du groupe
     * @param string $type type associé ('template','bloc')
     * @return int
     */
    public function getMaxOrdre($post, $id_groupe, $type) {
        $ordrelmt = new data('elements', array('id_'.$type => $post['id_'.$type], 'id_groupe' => $id_groupe));
        $ordermax = max(array_merge(array(0),$ordrelmt->ordre->getArray())) + 1;
        return $ordermax;
    }

}
