<?php

namespace o;

class globals_core extends instance
{
    /**
     * Méthode pour la récupération de la valeur d'un param
     *
     * @function getParamValue
     * @param string $slug Slug du param
     * @return string
     */
    public function getParamValue($slug)
    {
        $globals = new globals(array('slug' => $slug));
        return $globals->value;
    }
}
