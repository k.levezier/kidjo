<?php

namespace o;

class zones_core extends instance
{
    /**
     * Méthode pour la génération des checkSecure de zone
     *
     * @function generateCheckSecure
     * @param string $name Nom de la zone
     * @param int $id_parent ID de la zone parente
     * @return string
     */
    public function generateCheckSecure($name, $id_parent)
    {
        if ($id_parent !== null) {
            $parent = new zones($id_parent);
            $checksecure = mb_substr($parent->checksecure, 0, 4, 'UTF-8') . generateSlug($name);
        } else {
            $checksecure = generateSlug($name);
        }
        return $checksecure;
    }
}
