<?php

/**
 * La classe DATA de la table clients
 * 
 * @class       clients_core
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2008-2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://leeloo.equinoa.net/licence.txt
 */
class clients_core extends clients_crud {

    public function __construct($bdd, $params = array()) {
        parent:: __construct($bdd, $params);
    }

    public function select($where = '', $order = '', $start = '', $nb = '') {
        return $this->db->select('clients', $where, $order, $start, $nb);
    }

    public function counter($where = '') {
        return $this->db->counter('clients', $where);
    }

    public function exist($id, $field) {
        return $this->db->exist('clients', $id, $field);
    }

}
