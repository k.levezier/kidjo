<?php

namespace o;

class produits_core extends instance {

    /**
     * Méthode pour la récuperation du nom d'un produit
     * 
     * @function getName
     * 
     * @return string 
     */
    public function getName($id_langue) {
        if(strlen(trim($id_langue))===0)
            return '';
        else
            return  $this->elements->where(array('id_element'=>5, 'id_langue'=>$id_langue))->current()->value;
    }
    
    /**
     * Méthode pour la récuperation d'une image produit
     * 
     * @function getImgSrc
     * 
     * @return string 
     */
    public function getImgFile() {
        if($this->image_site->exist()){
            return $this->image_site->fichier;
        } else {
            if($this->images->count() > 0){
                return $this->images->current()->fichier;
            } else {
                return '';
            }
        }
    }
    
    /**
     * Méthode pour la récuperation du nom d'un produit
     * 
     * @function getName
     * 
     * @return string 
     */
    public function getImageDefaultSite() {
        if($this->id_img_site){
            
        }
    }
    
    /**
     * Récupération du contenu d'un élément du produit
     * 
     * @function getContent
     * @param string $slug Slug de l'élément
     * @return string
     */
    public function getContent($slug) {        
        return $this->elements->where(array('element->slug' => $slug))->getInstance()->value;
    }
    
    /**
     * Récupération du contenu complémentaire d'un élément du produit
     * 
     * @function getContentCompl
     * @param string $slug Slug de l'élément
     * @return string
     */
    public function getContentCompl($slug) {        
        return $this->elements->where(array('element->slug' => $slug))->getInstance()->complement;
    }
}