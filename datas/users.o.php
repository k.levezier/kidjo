<?php

namespace o;

class users_core extends instance
{
    /**
     * Méthode pour la récuperation de la liste des zones autorisées
     * pour affichage du menu de l'utilisateur
     *
     * @function selectZonesHeader
     * @param int $id_user ID du user connecté
     * @return array
     */
    public function selectZonesHeader($id_user)
    {
        $uz = new data('users_zones', array('id_user' => $id_user, 'zones->id_parent' => null));
        $return = array();
        foreach ($uz->order('ordre', 'ASC') as $zone) {
            $content = array_merge($zone->getArray(), $zone->zones->getArray());
            $uzc = new data('users_zones', array('id_user' => $id_user, 'zones->id_parent' => $zone->zones->id_zone));
            foreach ($uzc->order('ordre', 'ASC') as $zonec) {
                $content['childs'][] = array_merge($zonec->getArray(), $zonec->zones->getArray());
            }
            $return[] = $content;
        }
        return $return;
    }

    /**
     * Méthode pour la récuperation de la liste des sites autorisés
     * pour l'utilisateur
     *
     * @function selectSitesUser
     * @param int $id_user ID du user connecté
     * @return array
     */
    public function selectSitesUser($id_user)
    {
        $us = new data('users_sites', array('id_user' => $id_user));
        $us->join('sites', 's');
        $us->where('s.app != NULL AND s.base != NULL');
        $return = array();
        foreach ($us->order('ordre', 'ASC') as $site) {
            $return[] = array_merge($site->getArray(), $site->sites->getArray());
        }
        return $return;
    }

    /**
     * Méthode pour la récuperation de la première zone autorisée
     * pour l'utilisateur
     *
     * @function getFirstZoneUser
     * @param int $id_user ID du user connecté
     * @return array
     */
    public function getFirstZoneUser($id_user)
    {
        $uz = new data('users_zones', array('id_user' => $id_user, 'zones->id_parent' => null));
        $return = array();
        foreach ($uz->order('ordre', 'ASC')->limit(1) as $zone) {
            $return = $zone->zones->getArray();
        }
        return $return;
    }

    /**
     * Méthode pour checker si l'utilisateur connecté peut
     * accéder à la zone
     *
     * @function checkAccess
     * @param string $zone Zone à vérifier
     * @return boolean
     */
    public function checkAccess($zone = '')
    {
        $users = new data('users', array(
            'id_user'  => $_SESSION['user']['id_user'],
            'password' => $_SESSION['user']['password'],
            'hash'     => $_SESSION['user']['hash'],
        ));
        if ($users->count() == 1) {
            if (!empty($zone)) {
                $zones = new zones(array('checksecure' => $zone));
                if ($this->checkUserZone($_SESSION['user']['id_user'], $zones->id_zone)) {
                    return true;
                } else {
                    unset($_SESSION);
                    $_SESSION = '';
                    session_destroy();
                    header('location:' . $this->url . '/login/acces___interdit');
                    die;
                }
            } else {
                return true;
            }
        } else {
            unset($_SESSION);
            $_SESSION = '';
            session_destroy();
            header('location:' . $this->url . '/login');
            die;
        }
    }

    /**
     * Méthode pour checker si l'utilisateur connecté peut
     * interagir avec la zone
     *
     * @function checkUserZone
     * @param int $id_user ID de l'utilisateur
     * @param int $id_zone ID de la zone
     * @return boolean
     */
    public function checkUserZone($id_user, $id_zone)
    {
        $uz = new data('users_zones', array('id_user' => $id_user, 'id_zone' => $id_zone));
        if ($uz->count() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Méthode pour checker si l'utilisateur connecté peut
     * interagir avec le site
     *
     * @function checkUserSite
     * @param int $id_user ID de l'utilisateur
     * @param int $id_site ID du site
     * @return boolean
     */
    public function checkUserSite($id_user, $id_site)
    {
        $us = new data('users_sites', array('id_user' => $id_user, 'id_site' => $id_site));
        if ($us->count() == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Méthode pour récupérer la liste des ID user d'un site
     *
     * @function getListeUserForSite
     * @param int $id_site ID du site
     * @return array
     */
    public function getListeUserForSite($id_site)
    {
        $lIDusers = array();
        $us = new data('users_sites', array('id_site' => $id_site));
        foreach ($us as $u) {
            $lIDusers[] = $u->id_user;
        }
        return $lIDusers;
    }

    /**
     * Méthode pour la récuperation de la liste des ID sites autorisés
     * pour l'utilisateur
     *
     * @function selectIDSitesUser
     * @param int $id_user ID du user connecté
     * @return array
     */
    public function selectIDSitesUser($id_user)
    {
        $us = new data('users_sites', array('id_user' => $id_user));
        $return = array();
        foreach ($us as $site) {
            $return[] = $site->id_site;
        }
        return $return;
    }

    /**
     * Méthode pour la récuperation de la liste des ID zones autorisées
     * pour l'utilisateur
     *
     * @function selectIDZonesUser
     * @param int $id_user ID du user connecté
     * @return array
     */
    public function selectIDZonesUser($id_user)
    {
        $uz = new data('users_zones', array('id_user' => $id_user));
        $return = array();
        foreach ($uz as $zone) {
            $return[] = $zone->id_zone;
        }
        return $return;
    }

    /**
     * Méthode pour l'ajout d'une zone à l'utilisateur
     *
     * @function addZoneToUser
     * @param int $id_zone ID de la zone
     * @param int $id_user ID du user
     * @return boolean
     */
    public function addZoneToUser($id_zone, $id_user)
    {
        $ordreuz = new data('users_zones', array('id_user' => $id_user));
        $ordermax = max($ordreuz->ordre->getArray()) + 1;
        $users_zones = new users_zones();
        $users_zones->id_user = $id_user;
        $users_zones->id_zone = $id_zone;
        $users_zones->ordre = $ordermax;
        $users_zones->insert();
        return true;
    }

    /**
     * Méthode pour l'ajout d'un site à l'utilisateur
     *
     * @function addSiteToUser
     * @param int $id_site ID du site
     * @param int $id_user ID du user
     * @return boolean
     */
    public function addSiteToUser($id_site, $id_user)
    {
        $ordreus = new data('users_sites', array('id_user' => $id_user));
        $ordermax = max($ordreus->ordre->getArray()) + 1;
        $users_sites = new users_sites();
        $users_sites->id_user = $id_user;
        $users_sites->id_site = $id_site;
        $users_sites->ordre = $ordermax;
        $users_sites->insert();
        return true;
    }
}
