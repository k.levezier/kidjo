<?php

/**
 * La classe Common est une classe mère à tous les bootstrap
 * permettant de centraliser les mécanismes communs.
 *
 * @class       Common
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */

class commonCore extends Controller
{

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->params = $this->Command->Parameters;

        // Datas
        $this->ln = new o\traductions();
        $this->ln->tabLangues = $this->lLangues;

        // Classes
        $this->clSettings = new clSettings((new o\sets()), (new o\globals()), $this->ln, $this->surl);
        $GLOBALS['clSettings'] = $this->clSettings;
        $this->clFonctions = new clFonctions($this->lurl, $this->ln);
        $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);

        // Librairies
        $this->photos = new photos(array($this->spath, $this->surl));
        $this->ficelle = new ficelle();
    }

}
