<?php

use o\data;

class bootstrapCore extends Controller {

    public function __construct($command, $config, $app) {
        header('Access-Control-Allow-Origin: *');

        parent::__construct($command, $config, $app);
        $this->params = $this->Command->Parameters;
        
        // Datas
        $this->ln = new o\traductions();
        $this->ln->tabLangues = $this->lLangues;
        
        // Classes
        $this->clSettings = new clSettings((new o\sets()), (new o\globals()), $this->ln, $this->surl);
        $this->clFonctions = new clFonctions($this->lurl, $this->surl, $this->ln, $this->language);
        
        // Navigation principale
        $this->navSite = (new o\tree())->getArboSite($this->language, null);
        
        if(isset($_POST['login']))
        {
            $access = new o\clients(array('email' => $_POST['email'], 'password' => md5($_POST['password'])));
            if(!$access->exist()){ // On vérifie avec l'ancien mot de passe
                $access2 = new o\clients(array('email' => $_POST['email']));
                if(!empty($access2->password_old) && password_verify($_POST['password'],$access2->password_old)){
                    $access = $access2;
                }
            }

            if($access->exist())
            {
                $_SESSION['client'] = $access->getArray();
                $_SESSION['auth']='ok';
                $_SESSION['token']='boleke';
                $logins= new o\logins();
                $logins->ip = $_SERVER['REMOTE_ADDR'];
                $logins->id_client = $_SESSION['client']['id_client'];
                $logins->email = $_SESSION['client']['email'];
                $logins->insert();
                $access->lastlogin = date('Y-m-d H:i:s');
                $access->save();

                $zonesOk = [];
                $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
                foreach($zones as $z){
                    $zonesOk[] = $z->id_geozone;
                }
                $_SESSION['client']['zones'] = $zonesOk;

                $profilesOk = [];
                $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
                foreach($profiles as $p){
                    $profilesOk[] = $p->id_profile;
                }
                $_SESSION['client']['profiles'] = $profilesOk;

                if($_SESSION['postlogin'] != '' && $_SESSION['postlogin']!='/en')
                {
                    header('location:'.$_SESSION['postlogin']);die;
                }
                else
                {
                    header('location:'.$this->url.'/en');die;
                }

            }
            else
            {
                $this->message = '<h4 style="color:red; font-size:14px;">Your email or password is invalid, please try again or request a new password<br/></h4>';
            }
        }

        if(isset($_POST['loginbo']))
        {
            $access = new o\clients(array('email' => $_POST['email'], 'password' => ($_POST['password'])));

            if($access->exist())
            {
                unset($_SESSION['user_geozones']);
                unset($_SESSION['user_profiles']);
                $_SESSION['client'] = $access->getArray();
                $_SESSION['auth']='ok';
                $_SESSION['token']='boleke';
                $_SESSION['frombo']='ok';
                header('location:'.$this->url.'/en');die;


            }
            else
            {
                $this->message = '<h4 style="color:red; font-size:14px;">Your email or password is invalid, please try again or request a new password<br/></h4>';
            }
        }
        
        // CSS
        $this->loadCss('debug');
        
        // JS
        $this->loadJs('jquery-2.1.1');
    }

    /**
     * Fonction qui envoi les mails NMP et Server
     * 
     * @function envoyerMail
     * 
     * @param string $type_mail slug de l'email en bdd
     * @param string $id_langue langue de l'email
     * @param string $emailDest adresse mail du destinataire du mail
     * @param array $varMailcomp tableau de variables de mail complémentaire
     * 
     */
    public function envoyerMail($type_mail, $id_langue, $emailDest, $varMailcomp = array()) {
        $this->nmp = new o\nmp(array('id_nmp'=>0));
        $this->nmp_desabo = new o\data('nmp_desabo');
        $this->tnmp = new tnmp(array($this->nmp, $this->nmp_desabo));


            $emailDestNMP = $emailDest;
        

        $emailDestServer = $this->clSettings->getParam('mails-server-alias', 'globals');

        // Recuperation du modele de mail
        $this->mails_text = new o\mails_text(array('type'=>$type_mail, 'id_langue'=>$id_langue));
        if(!$this->mails_text->exist()){
            return false;
        }

        // Variables du mailing
        $varMail = array(
            'url' => $this->lurl,
            'surl' => $this->surl,
            'iurl' => $this->surl,
            'sender' => $this->mails_text->exp_email,
        );
        
        $varMail = array_merge($varMail, $varMailcomp);

        // Construction du tableau avec les balises EMV
        $tabVars = $this->tnmp->constructionVariablesServeur($varMail);


        // Attribution des données aux variables
        $sujetMail = strtr(utf8_decode($this->mails_text->subject), $tabVars);
        $texteMail = strtr(utf8_decode($this->mails_text->content), $tabVars);
        $exp_name = strtr(utf8_decode($this->mails_text->exp_name), $tabVars);

        // Envoi du mail
        $this->email = new email(array());
        $this->email->setFrom($this->mails_text->exp_email, $exp_name);
        $this->email->addRecipient($emailDestNMP);
        $this->email->setSubject(stripslashes(utf8_decode($sujetMail)));
        $this->email->setHTMLBody(stripslashes(utf8_decode($texteMail)));

        $this->mails_filer = new o\mails_filer(array('id_filermails'=>0));
        Mailer::sendNMP($this->email, $this->mails_filer, $this->mails_text->id_textemail, $emailDestNMP, $tabFiler);
                    
        // Injection du mail NMP dans la queue
        if ($this->Config['env'] != 'dev'){
            $this->tnmp->sendMailNMP($tabFiler, $varMail, $this->mails_text->nmp_secure, $this->mails_text->id_nmp, $this->mails_text->nmp_unique);
        }
        
        return true;
    }
}
