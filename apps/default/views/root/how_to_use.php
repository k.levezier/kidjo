<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Uriage Marketing Zone</title>

	<link rel="shortcut icon" type="image/x-icon" href="/<?=$this->surl?>/assets/images/favicon.ico" />

	<!-- Vendor Styles -->
	<link rel="stylesheet" href="/login/dropdown/dropdown.css" />
	<link rel="stylesheet" href="/login/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="/login/owl-carousel/owl.carousel.min.css" />

	<!-- App Styles -->
	<link rel="stylesheet" href="/<?=$this->surl?>/assets/css/bundle.css" />

	<!-- Vendor JS -->
	<script src="/login/jquery-1.11.3.min.js"></script>
	<script src="/login/dropdown/core.js"></script>
	<script src="/login/dropdown/touch.js"></script>
	<script src="/login/dropdown/dropdown.js"></script>
	<script src="/login/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="/login/owl-carousel/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="/login/functions.js"></script>
</head>

<body class="body-white">
    <script>
        function sendMe(mail)
        {
            if(mail!=null)
            {
                $.post( "/newpass", { eml: mail}).done( function( data ) {
                 alert(data);
            });}
        }
        </script>
<div class="wrapper" id="video">
	<div class="main">
		<section class="section-login">
			<div class="section-body">
                <div class="form form-login" >
                    <form method="post">
                        <header class="form-head">
                            <img src="/assets/images/logo.png" alt="uShare - Uriage">
                        </header><!-- /.form-head -->

                        <div class="form-body">
                            <video controls="" width="100%" autoplay>
                                <source src="<?=$this->surl?>/assets/videos/uriage_ushare_english.mp4" type="video/mp4">
                            </video>
                        </div><!-- /.form-body -->
                    </form>
                </div><!-- /.form form-login -->
			</div><!-- /.section-body -->
		</section><!-- /.section-login -->

    </div><!-- /.main -->

    <footer class="footer" style="background-color:#0387c2">
        <div class="footer-inner">
            <nav class="nav-footer">
                <ul>
                    <li>
                        <a href="mailto:contact@uriage.com">Contact</a>
                    </li>
                    <li>
                        <p class="copyright">&copy; Laboratoires Dermatologiques d'Uriage <?=date('Y')?></p>
                    </li>
                </ul>
            </nav><!-- /.nav -->
        </div><!-- /.footer-inner -->
    </footer><!-- /.footer -->
    </div><!-- /.wrapper -->
</body>
</html>


