<div>
    <?php
    $nopreview = array('zip','rar','7z','');
    $images = array('jpg','png','tif','tiff','psd');
    $googles = array('xls','xlsx');
    $videos = array('mov','mp4');
    $viewer = array('pdf','ppt','pptx','eps','doc','docx');
    $preview = array('ppt','pptx');
    if(in_array($this->documents->extension,$googles)) { ?>
        <script>
            $(document).ready(function() {
                var url = 'http://docs.google.com/viewer?url=<?=$this->lurl?>/view/<?=$this->documents->id_document?>/<?=sha1($this->documents->id_document.'kldjskdf'.$_SESSION['client']['id_client'])?>/<?=$_SESSION['client']['id_client']?>/doc.<?=$this->documents->extension?>'; // changer l'url par celle de votre site
                url += '&amp;embedded=true';
                var iframe = '<iframe src="'+url+'" width="100%" height="550" style="border: none;"></iframe>';
                $('.popup-preview').html(iframe);
            });
        </script>
    <?php } ?>

    <script>
        function like() {
            $.get( "<?=$this->lurl?>/like/<?=$this->params[0]?>", function( data ) { $( "#like" ).replaceWith( data );});
        }
    </script>
    <div class="popup popup-list-details">
        <button title="Close (Esc)" type="button" class="mfp-close" style="right:0;top:0;color:#333333;">×</button>
        <?php if(!in_array($this->documents->extension,$nopreview) || ($this->documents->extension=='zip' && $this->documents->preview!='') || ($this->documents->extension=='rar' && $this->documents->preview!='')) { ?>
	        <div class="popup-preview" <?=(in_array($this->documents->extension,$images)?' style="height:500px;overflow-y:scroll"':'')?>>
                <?php if(in_array($this->documents->extension,$images)) { ?>
                    <img src="/thumb/<?=str_replace('-thumb','-bigthumb',$this->documents->thumbnail)?>" style="width:100%">
                <?php } ?>
                <?php if(in_array($this->documents->extension,$videos)) { ?>
                    <video controls="" width="100%" autoplay="true">
                        <source src="/view/<?=$this->documents->id_document?>/<?=sha1($this->documents->id_document.'kldjskdf'.$_SESSION['client']['id_client'])?>/<?=$_SESSION['client']['id_client']?>/video.mp4" type="video/mp4">
                    </video>
                <?php } ?>
                <?php if(in_array($this->documents->extension,$viewer)  || ($this->documents->extension=='zip' && $this->documents->preview!='')) { ?>
                    <iframe src="/vendor/ViewerJS/#/view/<?=$this->documents->id_document?>/<?=sha1($this->documents->id_document.'kldjskdf'.$_SESSION['client']['id_client'])?>/<?=$_SESSION['client']['id_client']?>/doc.<?=$this->documents->extension?>/<?=mktime()?>.pdf" width='100%' height='500' allowfullscreen webkitallowfullscreen id="dociframe"></iframe>
                <?php } ?>
	        </div><!-- /.popup-preview -->
        <?php } ?>

        <div class="popup-content" <?=(in_array($this->documents->extension,$nopreview) && $this->documents->preview==''?' style="width:100%"':'')?>>
            <div class="popup-head">
                <?php include('like.php');?>
                <h2><?=$this->documents->label?></h2>

                <h6>
                    <?=(new DateTime($this->documents->added))->format($this->clSettings->getParam('format-de-date', 'sets'))?>

                    <span>Format : .<?=$this->documents->extension?> -  Size : <?=$this->clDocuments->showSize($this->documents->size)?></span>
                </h6>

                <h5><?=$this->documents->doctype->name?></h5>
            </div><!-- /.popup-head -->

            <div class="popup-body">
                <p><?=$this->documents->description?></p>
                <?php if($this->isAdmin) { ?>
                    <div class="form-actions" style="margin-top:25px">
                        <button type="button" class="btn btn-black" name="edit" onclick="window.open('http://adminpartners.nuxetest.com/documents/add/<?=$this->documents->id_document?>')" href="" style="width:100%"><span class="fa fa-edit"></span> EDIT DOCUMENT</button>
                    </div>
                <?php } ?>
            </div><!-- /.popup-body -->

            <div class="popup-actions">
                <p><?=$this->documents->droits?></p>
                <a href="/download/<?=$this->documents->id_document?>" onclick="ga('send','event','Documents','download','<?= stripslashes($this->documents->name) ?>');addDownload('<?=$this->documents->id_document?>','<?=$this->documents->file?>');" class="link-download" target="_blank">
                    <i class="ico-download-large"></i>
                    <span>Download (HD)</span>
                </a>
                <?php if($this->documents->noshare==0) {
                    $expiry = strtotime('+10 days'); ?>
                <a href="<?= $this->lurl ?>/share/<?=$this->documents->id_document?>/<?=md5('npartners'.$this->documents->id_document.$expiry)?>/<?=$expiry?>/fl.<?=$this->documents->extension?>" class="link-download" id="copy" type="button">
                    <span>
                        <i class="ico-copy"></i>
                        Copier le lien de partage
                    </span>
                </a>
                <?php } ?>
            </div><!-- /.popup-actions -->
        </div><!-- /.popup-content -->
    </div><!-- /.popup popup-list-details -->
</div>
<script>
    $( document ).ready(function() {
    var btnCopy = document.getElementById( 'copy' );
    btnCopy.addEventListener( 'click', function(e){
        e.preventDefault();
        ga('send','event','Documents','share','<?= stripslashes($this->documents->name) ?>');
        $( '.popup-actions' ).append( '<textarea id="to-copy"></textarea>' );
        $('#to-copy').val($(this).attr('href'));
        $('#to-copy').select();
        //
        if ( document.execCommand( 'copy' ) ) {
            $('#to-copy').remove();
            $('#copy span').html('Le lien a bien été copié');
        } else {
            console.info( 'Copy went wrong…' )
        }
        return false;
    } );
    });

    function addDownload(idDoc,file){
        $.post(
            '<?= $this->lurl ?>/addDownload',
            {
                id_document : idDoc,
                file : file
            },
        );
    }
</script>