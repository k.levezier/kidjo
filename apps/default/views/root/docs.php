<?php if(count($this->documents)==0) { ?>
    <p style="text-align:center; font-size:16px; margin-top:20px;">No document available</p>
<?php } else { ?>
    <script>
    $( document ).ready(function() {
        // Init Liste Details Popup
		if( $('.link-view').length ) {
			$('.link-view').magnificPopup({
				type: 'ajax',
				removalDelay: 400,
				mainClass: 'mfp-fade'
			});
		}        
    });
    </script>

    <?php foreach($this->documents as $doc) {
        $clt = new o\clients(array('id_client'=>$doc['id_client']));

        if($doc['new'] != $previous && $doc['id_client'] == 0) { ?>
            <?= ($previous != '' ? '</ul>' : '') ?>
            <h3>
                <span><?=($doc['new'] == 1? 'NEW' : 'ALL DOCUMENTS') ?></span>
            </h3>
            <ul class="list-items">
        <?php }
        $previous = $doc['new'];

        if($doc['id_client'] != $previousclt && $doc['id_client'] > 0) { ?>
            <?= ($previous != '' ? '</ul>' : '') ?>
                <h3>
                    <span><?=($clt->mobile)?></span>
                </h3>
                <ul class="list-items">
        <?php }
        $previousclt = $doc['id_client'];

        if ($this->docCache('doc-' . $doc->id_client . '-' . $doc->id_document)) { ?>
            <li class="list-item">
                <div class="list-item-inner">
                    <div class="list-item-image">
                        <a href="<?=$this->lurl?>/details/<?=$doc->id_document?>" id="linkfor<?=$doc->id_document?>" class="link-view" style="width:100%; text-align:center; background-color: #f8f8f8; height:150px; font-size:80px; display:block;<?=($doc->thumbnail!=''?'background-image:url(\'/thumb/'.addslashes($doc->thumbnail).'\');background-repeat: no-repeat;background-size: 50%;background-position:center':'')?>">
                            <?php
                            if($doc->thumbnail=='') {
                                switch($doc->extension) {
                                    case 'xlsx' :
                                    case 'xls' : ?>
                                        <i class="fa fa-bar-chart-o"></i>
                                        <?php break;
                                    case 'zip' :
                                    case 'rar' :
                                    case '7z' : ?>
                                        <i class="fa fa-file-archive-o"></i>
                                        <?php
                                        break;
                                    case 'eps' :
                                    case 'jpg' :
                                    case 'png' :
                                    case 'psd' : ?>
                                        <i class="fa fa-file-image-o"></i>
                                        <?php
                                        break;
                                    case 'pptx' :
                                    case 'ppt' : ?>
                                        <i class="fa fa-file-powerpoint-o"></i>
                                        <?php
                                        break;
                                    case 'mp4' :
                                    case 'mov' : ?>
                                        <i class="fa fa-file-video-o"></i>
                                        <?php
                                        break;
                                    case 'docx' :
                                    case 'doc' : ?>
                                        <i class="fa fa-file-text-o"></i>
                                        <?php
                                        break;
                                    case 'pdf' : ?>
                                        <i class="fa fa-file-pdf-o"></i>
                                        <?php
                                        break;
                                    default: ?>
                                        <i class="fa fa-file"></i>
                                        <?php
                                        break;
                                }
                            } ?>
                        </a>
                    </div>
                    <div class="list-item-content">
                        <h4 title="<?=$doc->label?>">
                            <?=$doc->label?><?=(($clt->exist()?'<br/><span style="font-weight:normal">'.$clt->prenom.' '.$clt->nom.'</span>':''))?>
                        </h4>
                        <p><?=(new DateTime($doc->added))->format($this->clSettings->getParam('format-de-date', 'sets'))?> (.<?=$doc->extension?> - <?=$this->clDocuments->showSize($doc->size)?>)</p>
                        <h6><?=$doc->doctype->name?></h6>
                        <div class="list-item-links">
                            <a href="<?=$this->lurl?>/details/<?=$doc->id_document?>" onclick="ga('send','event','Documents','preview','<?= stripslashes($doc->name) ?>');" class="link-view">
                                <i class="ico-view"></i>
                                <span>Preview</span>
                            </a>
                            <a href="/download/<?=$doc->id_document?>" onclick="ga('send','event','Documents','download','<?= stripslashes($doc->name) ?>');addDownload('<?=$doc->id_document?>','<?=$doc->file?>');" class="link-download" target="_blank">
                                <i class="ico-download"></i>
                                <span>Download (HD)</span>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
            <?php $this->docCacheEnd('doc-' . $doc->id_client . '-' . $doc->id_document);
        } ?>
    <?php } ?>
    </ul>
<?php } ?>
<script>
    function addDownload(idDoc,file){
        $.post(
            '<?= $this->lurl ?>/addDownload',
            {
                id_document : idDoc,
                file : file
            },
        );
    }
</script>

