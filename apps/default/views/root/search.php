<div class="main">
    <section class="section">
        <header class="section__head">
            <h3>Search results</h3>
        </header><!-- /.section__head -->

        <div class="section__body">
            <div class="components">
                <?php foreach($this->lDocuments as $this->doc){ ?>
                    <!-- <?= $this->doc->id_document ?> -->
                    <?php $this->fireBloc('document'); ?>
                <?php } ?>
            </div><!-- /.components -->
        </div><!-- /.section__body -->
    </section><!-- /.section -->
</div><!-- /.main -->