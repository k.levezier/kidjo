<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Uriage Marketing Zone</title>

	<link rel="shortcut icon" type="image/x-icon" href="/<?=$this->surl?>/assets/images/favicon.ico" />

	<!-- Vendor Styles -->
	<link rel="stylesheet" href="/login/dropdown/dropdown.css" />
	<link rel="stylesheet" href="/login/magnific-popup/magnific-popup.css" />
	<link rel="stylesheet" href="/login/owl-carousel/owl.carousel.min.css" />

	<!-- App Styles -->
	<link rel="stylesheet" href="/<?=$this->surl?>/assets/css/bundle.css" />

	<!-- Vendor JS -->
	<script src="/login/jquery-1.11.3.min.js"></script>
	<script src="/login/dropdown/core.js"></script>
	<script src="/login/dropdown/touch.js"></script>
	<script src="/login/dropdown/dropdown.js"></script>
	<script src="/login/magnific-popup/jquery.magnific-popup.min.js"></script>
	<script src="/login/owl-carousel/owl.carousel.min.js"></script>

	<!-- App JS -->
	<script src="/login/functions.js"></script>
</head>

<body class="body-white">
    <script>
        function sendMe(mail)
        {
            if(mail!=null)
            {
                $.post( "/newpass", { eml: mail}).done( function( data ) {
                 alert(data);
            });}
        }
        </script>
<div class="wrapper" id="login">
	<div class="main">

            <?php if($_SESSION['confirm']=='confirm') { ?>
            <div style="text-align:center;color:green;font-size:16px; margin-top:100px;font-weight:bold">
                        Your account has been confirmed and will remain active for 3 months. Thank you.
            </div>
            <?php unset($_SESSION['confirm']); } ?>
		<section class="section-login">
			<div class="section-head">

			</div><!-- /.section-head -->

			<div class="section-body">
                <?php if($this->params[0]=="newpassword"){ ?>
                    <div class="form form-login" >
                        <form method="post">
                            <header class="form-head">
                                <img src="/assets/images/logo.png" height="100" width="348" alt="uShare - Uriage">
                                <h4>New Password<br/><?=$this->client->email?></h4>

                            </header><!-- /.form-head -->

                            <div class="form-body">
                                <div class="form-row">
                                    <label for="field-email" class="form-label">New Password</label>

                                    <div class="form-controls">
                                        <input type="password" class="field" name="password" id="password" value="" placeholder="New Password" autocomplete="OFF">
                                    </div><!-- /.form-controls -->
                                </div><!-- /.form-row -->

                                <div class="form-row">
                                    <label for="field-password" class="form-label">Confirm Password</label>

                                    <div class="form-controls">
                                        <input type="password" class="field" name="password2" id="password2" value="" placeholder="Confirm Password" autocomplete="OFF">
                                    </div><!-- /.form-controls -->
                                </div><!-- /.form-row -->
                            </div><!-- /.form-body -->

                            <div class="form-actions">
                                <button type="submit" class="btn" name="new_password" value="confirm">Confirm</button>
                                <?php /* <h4 style="font-size:14px; cursor:pointer" onclick="sendMe(prompt('Please enter your e-mail address'))">Forgotten password</h4> */ ?>
                            </div><!-- /.form-actions -->
                            <?php if(!empty($this->error)){ ?><div class="error"><?=$this->error?></div><?php } ?>
                        </form>
                    </div><!-- /.form form-login -->
                <?php }else{ ?>
                    <div class="form form-login" >
                        <form method="post">
                            <header class="form-head">
                                <img src="/assets/images/logo.png" height="100" width="348" alt="uShare - Uriage">
                                <?php if(!empty($this->params[0]) && $this->params[0]=="success_pw"){ ?>
                                    <div class="success">Password changed</div>
                                <?php } ?>
                            </header><!-- /.form-head -->

                            <div class="form-body">
                                <div class="form-row">
                                    <label for="field-email" class="form-label">email</label>

                                    <div class="form-controls">
                                        <input type="text" class="field" name="email" id="email" value="" placeholder="Email"  autocomplete="OFF">
                                    </div><!-- /.form-controls -->
                                </div><!-- /.form-row -->

                                <div class="form-row">
                                    <label for="field-password" class="form-label">password</label>

                                    <div class="form-controls">
                                        <input type="password" class="field" name="password" id="password" value="" placeholder="Password"  autocomplete="OFF">
                                    </div><!-- /.form-controls -->
                                </div><!-- /.form-row -->
                            </div><!-- /.form-body -->

                            <div class="form-actions">
                                <button type="submit" class="btn" name="login">log in</button>
                                <h4 style="font-size:14px; cursor:pointer" onclick="sendMe(prompt('Please enter your e-mail address'))">Forgotten password</h4>
                            </div><!-- /.form-actions --><?=$this->message?>
                        </form>
                    </div><!-- /.form form-login -->
                <?php } ?>

			</div><!-- /.section-body -->
		</section><!-- /.section-login -->

    </div><!-- /.main -->

    <footer class="footer" style="background-color:#0387c2">
        <div class="footer-inner">
            <nav class="nav-footer">
                <ul>
                    <li>
                        <a href="mailto:contact@uriage.com">Contact</a>
                    </li>
                    <li>
                        <p class="copyright">&copy; Laboratoires Dermatologiques d'Uriage <?=date('Y')?></p>
                    </li>
                </ul>
            </nav><!-- /.nav -->
        </div><!-- /.footer-inner -->
    </footer><!-- /.footer -->
    </div><!-- /.wrapper -->
</body>
</html>


