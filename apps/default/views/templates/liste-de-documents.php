<div class="main">
    <section class="section">
        <header class="section__head">
            <h3><?=$this->tree->title?></h3>
        </header><!-- /.section__head -->

        <div class="section__body">
            <div class="components">
                <?php foreach($this->lDocuments as $this->doc){ ?>
                    <?php $this->fireBloc('document'); ?>
                <?php } ?>
            </div><!-- /.components -->
        </div><!-- /.section__body -->
    </section><!-- /.section -->
<?php
    if(!empty($this->docShow->id_document)){ ?>
        <a id="show-popup-document" href="#popup-document" data-url="<?=$this->lurl?>/documents/popup/<?=$this->docShow->id_document?>" class="open-popup-link"></a>
<?php } ?>
    <?php foreach($this->enfants as $e){
        if(!empty($this->lEnfantsDocuments[$e['id_tree']])){?>
        <section class="section">
            <header class="section__head">
                <h3><?=$e['title']?></h3>
            </header><!-- /.section__head -->

            <div class="section__body">
                <div class="components">
                    <?php foreach($this->lEnfantsDocuments[$e['id_tree']] as $this->doc){ ?>
                        <?php $this->fireBloc('document'); ?>
                    <?php } ?>
                </div><!-- /.components -->
            </div><!-- /.section__body -->
        </section><!-- /.section -->
    <?php   }
        } ?>
</div><!-- /.main -->