<div class="main">
    <section class="section-cta">
        <header class="section__head">
            <h3>What’s new ?</h3>
        </header><!-- /.section__head -->

        <div class="section__body" style="background-image: url('<?=$this->surl?>/assets/images/temp/intro-bg.png');">
            <figure class="section__product">
                <img src="<?=$this->surl?>/assets/images/temp/intro-product-1.png" alt="">
            </figure><!-- /.section__product -->

            <h2>
                <span>Bariésun</span>
            </h2>

            <p>
                <span>Lancement de notre dernier produit</span>
            </p>

            <div class="section__actions">
                <a href="#" class="btn">Découvrir</a>
            </div><!-- /.section__actions -->
        </div><!-- /.section__body -->
    </section><!-- /.section-cta -->

    <section class="section-cards">
        <div class="cards">
            <div class="card">
                <div class="card__inner">
                    <div class="card__head" style="background-image: url('<?=$this->surl?>/assets/images/temp/card-1.png');"></div><!-- /.card__head -->

                    <div class="card__body">
                        <h3>Notre catalogue</h3>

                        <div class="card__actions">
                            <a href="#" class="link">
                                Découvrir

                                <i class="ico-chevron-right-black"></i>
                            </a>
                        </div><!-- /.card__actions -->
                    </div><!-- /.card__body -->
                </div><!-- /.card__inner -->
            </div><!-- /.card -->

            <div class="card">
                <div class="card__inner">
                    <div class="card__head" style="background-image: url('<?=$this->surl?>/assets/images/temp/card-2.png');"></div><!-- /.card__head -->

                    <div class="card__body">
                        <h3>Les dernières UFO</h3>

                        <div class="card__actions">
                            <a href="#" class="link">
                                Découvrir

                                <i class="ico-chevron-right-black"></i>
                            </a>
                        </div><!-- /.card__actions -->
                    </div><!-- /.card__body -->
                </div><!-- /.card__inner -->
            </div><!-- /.card -->
        </div><!-- /.cards -->
    </section><!-- /.section-cards -->
</div><!-- /.main -->
