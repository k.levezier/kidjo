<div class="main">
    <section class="section-cta section-cta--small">
        <div class="section__body" style="background-image: url('<?= $this->surl.'/var/images/'. $this->tree->getContent('visuel') ?>');">
            <div class="shell">
<?php           if(!empty($this->tree->getContent('titre'))){ ?>
                <h2>
                    <span><?=$this->tree->getContent('titre')?></span>
                </h2>
<?php           } ?>

<?php           if(!empty($this->tree->getContent('sous-titre'))){ ?>
                <p>
                    <span><?=$this->tree->getContent('sous-titre')?></span>
                </p>
<?php           } ?>
            </div><!-- /.shell -->
        </div><!-- /.section__body -->
    </section><!-- /.section-cta -->

    <article class="article">
        <div class="shell">
            <h3><?=$this->tree->getContent('titre-1')?></h3>

            <p><?=nl2br($this->tree->getContent('contenu-1'));?></p>

            <h3><?=$this->tree->getContent('titre-2')?></h3>

            <p><?=nl2br($this->tree->getContent('contenu-2'));?></p>

            <?php if(!is_null($this->doc)){ ?>
            <div class="products">
                <?php $this->fireBloc('document-article'); ?>
            </div><!-- /.products -->
            <?php } ?>
        </div><!-- /.shell -->
    </article><!-- /.article -->
</div><!-- /.main -->