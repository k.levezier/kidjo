<div class="main">
    <section class="section-cta">
        <header class="section__head">
            <h3><?=$this->tree->title?></h3>
        </header><!-- /.section__head -->

        <?php if(!empty($this->article1->id_tree)){ ?>
            <div class="section__body" style="background-image: url('<?= $this->surl.'/var/images/'. $this->article1->getContent('visuel') ?>');">
                <h2>
                    <span><?=$this->article1->getContent('titre')?></span>
                </h2>

                <p>
                    <span><?=$this->article1->getContent('sous-titre')?></span>
                </p>

                <div class="section__actions">
                    <a href="<?=$this->surl?>/<?=$this->article1->slug?>" class="btn">Discover</a>
                </div><!-- /.section__actions -->
            </div><!-- /.section__body -->
        <?php } ?>

    </section><!-- /.section-cta -->

    <?php if($this->tree->authorized()){ ?>
    <section class="section-cards">
        <div class="cards">
            <?php if(!empty($this->article2->id_tree)){ ?>
            <div class="card">
                <div class="card__inner">
                    <div class="card__head" style="background-image: url('<?= $this->surl.'/var/images/'. $this->article2->getContent('visuel') ?>');"></div><!-- /.card__head -->

                    <div class="card__body">
                        <h3><?=$this->article2->getContent('titre')?></h3>
                        <div class="card__actions">
                            <a href="<?=$this->surl?>/<?=$this->article2->slug?>" class="link">
                                Discover
                                <i class="ico-chevron-right-black"></i>
                            </a>
                        </div><!-- /.card__actions -->
                    </div><!-- /.card__body -->
                </div><!-- /.card__inner -->
            </div><!-- /.card -->
            <?php } ?>

            <?php if(!empty($this->article3->id_tree)){ ?>
                <div class="card">
                    <div class="card__inner">
                        <div class="card__head" style="background-image: url('<?= $this->surl.'/var/images/'. $this->article3->getContent('visuel') ?>');"></div><!-- /.card__head -->

                        <div class="card__body">
                            <h3><?=$this->article3->getContent('titre')?></h3>
                            <div class="card__actions">
                                <a href="<?=$this->surl?>/<?=$this->article3->slug?>" class="link">
                                    Discover
                                    <i class="ico-chevron-right-black"></i>
                                </a>
                            </div><!-- /.card__actions -->
                        </div><!-- /.card__body -->
                    </div><!-- /.card__inner -->
                </div><!-- /.card -->
            <?php } ?>
        </div><!-- /.cards -->
    </section><!-- /.section-cards -->
    <?php } ?>
</div><!-- /.main -->
