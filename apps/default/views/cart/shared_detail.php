<div class="main">
    <section class="section section--cart-details">
        <header class="section__head">
            <div class="section__message">
                <p>
                    This a shared cart. You are unable<br>
                    to delete items
                </p>
            </div><!-- /.section__message -->

            <div class="section__head2">
                <h3>Shared cart (<?=count($this->lDocuments)?>)</h3>

                <?php if($_SESSION['client']['status'] == 2 && !empty($this->lDocuments)){ ?>
                    <p style="float: left">
                        <a href="#share-popup" class="btn btn--alt open-popup-link">
                            <i class="ico-share"></i>
                            Share
                        </a>
                    </p>
                <?php } ?>
            </div>
        </header><!-- /.section__head -->

        <div class="section__body">
            <div class="products">
                <?php if(!empty($this->lDocuments)){
                    foreach($this->lDocuments as $this->doc){ ?>
                        <?php $this->fireBloc('document-cart'); ?>
                    <?php   }
                }else{ ?>
                    <p>
                        Your cart is empty!
                    </p>
                <?php } ?>
            </div><!-- /.products -->

            <div class="section__actions">
                <a href="<?=$this->lurl?>/download_cart/<?=$this->cart->hash?>" target="_blank" class="btn btn--outline">
                    <i class="ico-cloud"></i>

                    Download all
                </a>
            </div><!-- /.section__actions -->
        </div><!-- /.section__body -->
    </section><!-- /.section -->


<?php if($_SESSION['client']['status'] == 2){ ?>
    <?php $this->fireBloc('cart-form-share'); ?>
<?php } ?>
</div><!-- /.main -->
