<div class="main">
    <section class="section section--alt">
        <header class="section__head">
            <h3>My cart (<?=count($this->lDocuments)?>)</h3>

            <?php if($_SESSION['client']['status'] == 2 && !empty($this->lDocuments)){ ?>
            <p>
                <a href="#share-popup" class="btn btn--alt open-popup-link">
                    <i class="ico-share"></i>
                    Share
                </a>
            </p>
            <?php } ?>
        </header><!-- /.section__head -->

        <div class="section__body">
            <div class="products">

                <?php if(!empty($this->lDocuments)){
                        foreach($this->lDocuments as $this->doc){ ?>
                            <?php $this->fireBloc('document-cart'); ?>
                <?php   }
                    }else{ ?>
                    <p>
                        Your cart is empty!
                    </p>
                <?php } ?>
            </div><!-- /.products -->

            <div class="section__actions">
                <?php if(!empty($this->lDocuments)){ ?>
                <a href="<?=$this->lurl?>/download_cart/<?=$_SESSION['cart']['hash']?>" class="btn btn--outline">
                    <i class="ico-cloud"></i>
                    Download all
                </a>
                <?php } ?>
            </div><!-- /.section__actions -->
        </div><!-- /.section__body -->
    </section><!-- /.section -->
<?php if($_SESSION['client']['status'] == 2){ ?>
    <?php $this->fireBloc('cart-form-share'); ?>
<?php } ?>
</div><!-- /.main -->
