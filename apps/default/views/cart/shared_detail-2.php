<div class="main">
    <section class="section section--cart-details">
        <header class="section__head">
            <div class="section__message">
                <p>
                    This a shared cart. You are unable<br>
                    to delete items
                </p>
            </div><!-- /.section__message -->

            <h3>Shared cart (4)</h3>
        </header><!-- /.section__head -->

        <div class="section__body">
            <div class="products">
                <div class="product">
                    <div class="product__image">
                        <figure>
                            <img src="<?=$this->surl?>/assets/images/temp/asset-1.png" alt="" width="65">
                        </figure>
                    </div><!-- /.product__image -->

                    <div class="product__details">
                        <h3>Eau_thermale.jpg</h3>

                        <p>.jpg file</p>
                    </div><!-- /.product__details -->

                    <div class="product__actions">
                        <ul>
                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-vision"></i>

                                    See
                                </a>
                            </li>

                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-cloud"></i>

                                    Download
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.product__actions -->
                </div><!-- /.product -->

                <div class="product">
                    <div class="product__image">
                        <figure>
                            <img src="<?=$this->surl?>/assets/images/temp/asset-2.png" alt="" width="71">
                        </figure>
                    </div><!-- /.product__image -->

                    <div class="product__details">
                        <h3>Bariésun</h3>
                    </div><!-- /.product__details -->

                    <div class="product__actions">
                        <ul>
                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-vision"></i>

                                    See
                                </a>
                            </li>

                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-cloud"></i>

                                    Download
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.product__actions -->
                </div><!-- /.product -->


                <div class="product">
                    <div class="product__image">
                        <figure>
                            <img src="<?=$this->surl?>/assets/images/temp/asset-1.png" alt="" width="65">
                        </figure>
                    </div><!-- /.product__image -->

                    <div class="product__details">
                        <h3>Eau_thermale.jpg</h3>

                        <p>.jpg file</p>
                    </div><!-- /.product__details -->

                    <div class="product__actions">
                        <ul>
                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-vision"></i>

                                    See
                                </a>
                            </li>

                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-cloud"></i>

                                    Download
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.product__actions -->
                </div><!-- /.product -->

                <div class="product">
                    <div class="product__image">
                        <figure>
                            <img src="<?=$this->surl?>/assets/images/temp/asset-2.png" alt="" width="71">
                        </figure>
                    </div><!-- /.product__image -->

                    <div class="product__details">
                        <h3>Bariésun</h3>
                    </div><!-- /.product__details -->

                    <div class="product__actions">
                        <ul>
                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-vision"></i>

                                    See
                                </a>
                            </li>

                            <li>
                                <a href="#" class="btn btn--outline">
                                    <i class="ico-cloud"></i>

                                    Download
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.product__actions -->
                </div><!-- /.product -->
            </div><!-- /.products -->

            <div class="section__actions">
                <a href="#" class="btn btn--outline">
                    <i class="ico-cloud"></i>

                    Download all
                </a>
            </div><!-- /.section__actions -->
        </div><!-- /.section__body -->
    </section><!-- /.section -->
</div><!-- /.main -->