<div class="main">
    <section class="section section--cart section--cart-shared">
        <header class="section__head">
            <div class="section__message">
                <p>
                    These are shared carts. You are unable to delete items
                </p>
            </div><!-- /.section__message -->

            <h3>Shared carts (<?=count($this->lCarts)?>)</h3>
        </header><!-- /.section__head -->

        <div class="section__body">
            <div class="products">
                <?php foreach($this->lCarts as $c){
                    $thumbs = $this->carts->getThumbForShared($c['id_cart']);
                    $author = new o\clients(['id_client'=>$c['id_client']]); ?>
                <div class="product product--small">
                    <a href="<?=$this->lurl?>/cart/shared_detail/<?=$c['hash']?>">
                    <div class="product__cart">
                        <?php foreach($thumbs as $key=>$t){ ?>
                            <?=($key==2?'<div class="clearfix" style="clear:both; width:100%;"></div><br/>':'')?>
                            <figure>
                                <?php if(!empty($t['thumbnail'])){
                                    if(file_exists($this->path."protected/documents/thumbs/".$t['thumbnail'])){?>
                                        <img src="<?=$this->static_url?>/thumb/<?=$_SESSION['client']['hash']?>/<?=$t['thumbnail']?>" alt="<?=$t['thumbnail']?>" <?=(count($thumbs)>1?'style="max-width:27px; max-height:27px"':'')?>>
                                    <?php }else{ ?>
                                        <i class="fa fa-file-image-o fa-<?=(count($thumbs)>1?'1':'4')?>x"></i>
                                    <?php } ?>
                                <?php }else{
                                    switch($t['extension']) {
                                        case 'xlsx' : case 'xls' :
                                            echo '<i class="fa fa-bar-chart-o fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                        case 'zip' : case 'rar' : case '7z' :
                                            echo '<i class="fa fa-file-archive-o fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                        case 'eps' : case 'jpg' : case 'png' : case 'psd' :
                                            echo '<i class="fa fa-file-image-o fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                        case 'pptx' : case 'ppt' :
                                            echo '<i class="fa fa-file-powerpoint-o fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                        case 'mp4' : case 'mov' :
                                            echo '<i class="fa fa-file-video-o fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                        case 'docx' : case 'doc' :
                                            echo '<i class="fa fa-file-text-o fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                        case 'pdf' :
                                            echo '<i class="fa fa-file-pdf-o fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                        default:
                                            echo '<i class="fa fa-file fa-'.(count($thumbs)>1?'1':'4').'x"></i>';
                                            break;
                                    }
                                } ?>
                            </figure>
                        <?php } ?>
                    </div><!-- /.product__cart -->
                    </a>

                    <div class="product__details product__details--alt">
                        <a href="<?=$this->lurl?>/cart/shared_detail/<?=$c['hash']?>"><h3>Shared cart #<?=$c['id_cart']?></h3></a>

                        <p><?=date('d/m/Y',strtotime($c['added']))?></p>

                        <p>
                            Shared by <strong><?=$author->prenom?> <?=$author->nom?></strong>
                        </p>
                    </div><!-- /.product__details -->

                    <div class="product__actions">
                        <ul>
                            <li>
                                <a href="<?=$this->lurl?>/cart/shared_detail/<?=$c['hash']?>" class="btn btn--outline">
                                    <i class="ico-shop-alt"></i>

                                    Details
                                </a>
                            </li>

                            <li>
                                <a href="<?=$this->lurl?>/download_cart/<?=$c['hash']?>" target="_blank" class="btn btn--outline">
                                    <i class="ico-cloud"></i>

                                    Download
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.product__actions -->
                </div><!-- /.product -->
                <?php } ?>
            </div><!-- /.products -->
        </div><!-- /.section__body -->
    </section><!-- /.section -->
</div><!-- /.main -->