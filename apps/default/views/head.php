<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Uriage Marketing-zone</title>

    <link rel="shortcut icon" type="image/x-icon" href="<?=$this->surl?>/assets/images/favicon.ico">
    <link rel="stylesheet" href="<?=$this->surl?>/assets/vendor/magnific-popup/magnific-popup.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?=$this->surl?>/assets/vendor/pretty-dropdowns/css/prettydropdowns.css" type="text/css" media="all">
    <link rel="stylesheet" type="text/css" href="<?=$this->surl?>/assets/css/bundle.css">
    <link rel="stylesheet" href="<?=$this->surl?>/vendor/font-awesome-4.5.0/css/font-awesome.min.css">
</head>

<body>