<div id="share-popup" class="share-popup mfp-hide">
    <div class="share-popup__inner">
        <header class="share-popup__head">
            <h3>Share</h3>
        </header><!-- /.share-popup__head -->

        <form action="" method="post" id="form-to-share">
            <div class="share-popup__body">
                <div class="form">
                    <div class="form__inner">
                        <div class="custom-select">
                            <select id="profiles" class="pretty">
                                <option selected="" disabled="">Profiles</option>
                                <?php foreach($this->profiles as $p){ ?>
                                    <option value="profiles_<?=$p->id_profile?>"><?=$p->name?></option>
                                <?php } ?>
                            </select>
                        </div><!-- /.custom-select -->

                        <div class="custom-select">
                            <select name="geozones" id="geozones" class="pretty">
                                <option selected="" disabled="">Zone</option>
                                <?php foreach($this->geozones as $z){ ?>
                                    <option value="geozones_<?=$z->id_geozone?>"><?=$z->name?></option>
                                <?php } ?>
                            </select>
                        </div><!-- /.custom-select -->
                    </div><!-- /.form__inner -->
                </div><!-- /.form -->

                <div class="share-popup__tags">
                    <ul class="list-tags">
                        <?php if(!empty($this->lProfiles)){ foreach($this->lProfiles as $p){ ?>
                            <li class="js-tag"><p><?=(isset($this->lProfilesLabel[$p])?$this->lProfilesLabel[$p]:'')?> <a href="#" data-value="profiles_<?=$p?>" class="js-tag-remove"><i class="ico-close"></i></a></p></li>
                        <?php } } ?>
                        <?php if(!empty($this->lGeozones)){ foreach($this->lGeozones as $z){ ?>
                            <li class="js-tag"><p><?=(isset($this->lGeozonesLabel[$z])?$this->lGeozonesLabel[$z]:'')?> <a href="#" data-value="geozones_<?=$z?>" class="js-tag-remove"><i class="ico-close"></i></a></p></li>
                        <?php } } ?>

                    </ul><!-- /.list-tags -->
                </div><!-- /.share-popup__tags -->
            </div><!-- /.share-popup__body -->

            <div class="share-popup__actions">
                <div class="row checkbox-send-cart">
                    <input type="checkbox" name="send" id="send" value="true"/>
                    <label for="send">Send a email and update the expiration date</label>
                </div>

                <div id="erreur-profiles" class="message-error" style="display: none">You need to select a profile</div>
                <input type="hidden" name="profiles" id="liste-profiles" value='<?=json_encode($this->lProfiles)?>' />
                <input type="hidden" name="geozones" id="liste-geozones" value='<?=json_encode($this->lGeozones)?>' />
                <input type="hidden" name="client" value="<?=$_SESSION['client']['hash']?>" />
                <input type="hidden" name="cart" value="<?=$this->cart->hash?>" />
                <button type="submit" class="btn btn--alt">
                    <i class="ico-share"></i>
                    Share
                </button>
                <div class="row expiration-date">
                    <?=(!empty($this->cart->send) && $this->cart->send!="0000-00-00 00:00:00"?'Expiration date : '.date('d/m/Y',strtotime("+15 days",strtotime($this->cart->send))):'')?>
                </div>
            </div><!-- /.share-popup__actions -->
        </form>
        <button class="mfp-close">
            <i class="ico-close"></i>
        </button>
    </div><!-- /.share-popup__inner -->
</div><!-- /.share-popup -->