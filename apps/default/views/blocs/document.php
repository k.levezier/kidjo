<?php
    $activeLang = [];
    foreach(['en','fr','es'] as $ln){ $aTester = 'file_'.$ln;
        if(empty($activeLang) && !empty($this->doc->$aTester)){
            $activeLang = $ln;
            $file = 'file_'.$ln;
            $thumbnail = 'thumbnail_'.$ln;
            $extension = 'extension_'.$ln;
            $size = 'size_'.$ln;
        }
    }

?>
<div class="component<?=($this->doc->new==1?' component--special':'')?>">  <!-- component--special -->
    <div class="component__inner">
        <div class="component__image">
            <figure>
                <?php if(!empty($this->doc->$thumbnail)){
                    if(file_exists($this->path."protected/documents/thumbs/".$this->doc->$thumbnail)){?>
                    <img src="<?=$this->static_url?>/thumb/<?=$_SESSION['client']['hash']?>/<?=$this->doc->$thumbnail?>" alt="<?=$this->doc->$thumbnail?>">
                <?php }else{ ?>
                        <i class="fa fa-file-image-o fa-4x"></i>
                <?php } ?>
                <?php }else{
                        switch($this->doc->$extension) {
                            case 'xlsx' :
                            case 'xls' :
                            echo '<i class="fa fa-bar-chart-o fa-4x"></i>';
                                break;
                            case 'zip' :
                            case 'rar' :
                            case '7z' :
                            echo '<i class="fa fa-file-archive-o fa-4x"></i>';
                                break;
                            case 'eps' :
                            case 'jpg' :
                            case 'png' :
                            case 'psd' :
                            echo '<i class="fa fa-file-image-o fa-4x"></i>';
                                break;
                            case 'pptx' :
                            case 'ppt' :
                            echo '<i class="fa fa-file-powerpoint-o fa-4x"></i>';
                                break;
                            case 'mp4' :
                            case 'mov' :
                            echo '<i class="fa fa-file-video-o fa-4x"></i>';
                                break;
                            case 'docx' :
                            case 'doc' :
                            echo '<i class="fa fa-file-text-o fa-4x"></i>';
                                break;
                            case 'pdf' :
                            echo '<i class="fa fa-file-pdf-o fa-4x"></i>';
                                break;
                            default:
                            echo '<i class="fa fa-file fa-4x"></i>';
                                break;
                        }
                    } ?>
            </figure>
        </div><!-- /.component__image -->

        <div class="component__body">
            <div class="component__content">
                <h3><?=$this->doc->label?></h3>
                <p><span class="bg-lisibilite"><?=date('d/m/Y',strtotime($this->doc->added))?></span></p>
                <p><span class="bg-lisibilite"><?=$this->clDocuments->showSize($this->doc->$size)?></span></p>
            </div><!-- /.component__content -->

            <div class="component__actions">
                <ul>
                    <li>
                        <a href="#popup-document" data-url="<?=$this->lurl?>/documents/popup/<?=$this->doc->id_document?>" class="btn btn--outline open-popup-link">
                            <i class="ico-vision"></i> See
                        </a>
                    </li>

                    <li>
                        <a href="<?=$this->surl?>/download/<?=$this->doc->id_document?>/all" target="_blank" class="btn btn--outline">
                            <i class="ico-cloud"></i> Download
                        </a>
                    </li>

                    <li>
                        <button data-action="<?=$this->lurl?>/cart/add_document" data-document="<?=$this->doc->id_document?>" class="btn btn--outline add-cart add-cart-doc-<?=$this->doc->id_document?> <?=(!empty($_SESSION['cart']['documents']) && in_array($this->doc->id_document,$_SESSION['cart']['documents'])?' active':'')?>">
                            <i class="ico-shop-alt"></i>  Add to cart
                        </button>
                    </li>
                </ul>
            </div><!-- /.component__actions -->
        </div><!-- /.component__body -->
    </div><!-- /.component__inner -->
</div><!-- /.component -->