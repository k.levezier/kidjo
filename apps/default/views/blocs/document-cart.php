<?php
    $activeLang = [];
    foreach(['en','fr','es'] as $ln){ $aTester = 'file_'.$ln;
        if(empty($activeLang) && !empty($this->doc->$aTester)){
            $activeLang = $ln;
            $file = 'file_'.$ln;
            $thumbnail = 'thumbnail_'.$ln;
            $extension = 'extension_'.$ln;
            $size = 'size_'.$ln;
        }
    }
?>

<div class="product" id="document_<?=$this->doc->id_document?>">
    <div class="product__image">
        <figure>
            <?php if(!empty($this->doc->$thumbnail)){
                if(file_exists($this->path."protected/documents/thumbs/".$this->doc->$thumbnail)){?>
                    <img src="<?=$this->static_url?>/thumb/<?=$_SESSION['client']['hash']?>/<?=$this->doc->$thumbnail?>" alt="<?=$this->doc->$thumbnail?>">
                <?php }else{ ?>
                    <i class="fa fa-file-image-o fa-3x"></i>
                <?php } ?>
            <?php }else{
                switch($this->doc->$extension) {
                    case 'xlsx' :
                    case 'xls' :
                        echo '<i class="fa fa-bar-chart-o fa-3x"></i>';
                        break;
                    case 'zip' :
                    case 'rar' :
                    case '7z' :
                        echo '<i class="fa fa-file-archive-o fa-3x"></i>';
                        break;
                    case 'eps' :
                    case 'jpg' :
                    case 'png' :
                    case 'psd' :
                        echo '<i class="fa fa-file-image-o fa-3x"></i>';
                        break;
                    case 'pptx' :
                    case 'ppt' :
                        echo '<i class="fa fa-file-powerpoint-o fa-3x"></i>';
                        break;
                    case 'mp4' :
                    case 'mov' :
                        echo '<i class="fa fa-file-video-o fa-3x"></i>';
                        break;
                    case 'docx' :
                    case 'doc' :
                        echo '<i class="fa fa-file-text-o fa-3x"></i>';
                        break;
                    case 'pdf' :
                        echo '<i class="fa fa-file-pdf-o fa-3x"></i>';
                        break;
                    default:
                        echo '<i class="fa fa-file fa-3x"></i>';
                        break;
                }
            } ?>
        </figure>
    </div><!-- /.product__image -->

    <div class="product__details">
        <h3><?=$this->doc->label?></h3>

        <p>.<?=$this->doc->$extension?> file</p>

        <?php if(isset($this->authorizationRemove) && $this->authorizationRemove===true){ ?>
        <p>
            <a data-action="<?=$this->lurl?>/cart/remove_document" data-document="<?=$this->doc->id_document?>" class="link-text remove-cart">Remove from cart</a>
        </p>
        <?php } ?>
    </div><!-- /.product__details -->

    <div class="product__actions">
        <ul>
            <li>
                <a href="#popup-document" data-url="<?=$this->lurl?>/documents/popup/<?=$this->doc->id_document?>" class="btn btn--outline open-popup-link">
                    <i class="ico-vision"></i>
                    See
                </a>
            </li>

            <li>
                <a href="<?=$this->surl?>/download/<?=$this->doc->id_document?>/all" class="btn btn--outline">
                    <i class="ico-cloud"></i>
                    Download
                </a>
            </li>
        </ul>
    </div><!-- /.product__actions -->
</div><!-- /.product -->