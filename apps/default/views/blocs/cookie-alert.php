<link media ="all" href="<?= $this->surl ?>/styles/cookies/cookies.css" type="text/css" rel="stylesheet" />
<div class="cc_cookieAlert">
    <div class="cc_container">
        <a onClick="$.post('<?= $this->lurl ?>/alertCookie', {cookieContent: 'cookieAlertCookie-<?= date('Ymd') ?>'}, function () { $('.cc_cookieAlert').hide(); }); return false;" class="cc_btn">
            <?= $this->ln->txt('alerte-cookie', 'texte-bouton', $this->language, 'OK') ?>
        </a>
        <p class="cc_message">
            <?= $this->ln->txt('alerte-cookie', 'texte-legal', $this->language, 'En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies.') ?>             
            <a href="<?= $this->ln->txt('alerte-cookie', 'lien-en-savoir-plus', $this->language, '#') ?>" title="<?= $this->ln->txt('alerte-cookie', 'en-savoir-plus', $this->language, 'En savoir plus') ?>">
                <?= $this->ln->txt('alerte-cookie', 'en-savoir-plus', $this->language, 'En savoir plus') ?>
            </a>
        </p>
    </div>
</div>