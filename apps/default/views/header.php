<?php
    $idParent = [];
    if(!empty($this->tree->id_tree)){
        foreach($this->tree->getBreadCrumb($this->tree->id_tree,$this->language) as $p){
            $idParent[] = $p['id_tree'];
        }
    }

    $this->menuIcones = json_decode($this->clSettings->getParam('menu-icones', 'globals'),true);
    if(!isset($_SESSION['navigation']) || !isset($_SESSION['navigation-time']) || (isset($_SESSION['navigation-time']) && $_SESSION['navigation-time'] <= (time()-600))){
        //echo '<!-- MAJ NAV -->';
        $this->navigation = (new o\tree())->getArboSiteZoneProfil($this->language, 1);
        $_SESSION['navigation'] = $this->navigation;
        $_SESSION['navigation-time'] = time();
    }else{
        $this->navigation = $_SESSION['navigation'];
    }

?>
<div class="wrapper">
    <div class="menu">
        <div class="menu__content<?=(!empty($this->tree->id_tree) && $this->tree->id_tree != "1"?' menu__content--active':'')?>">
            <a href="#" class="nav-close">
                <i class="ico-close"></i>
            </a>

            <div class="logo-uriage">
                <img src="<?= $this->surl?>/assets/images/logo-uriage.png" />
            </div>
            <nav class="nav">
                <ul>
<?php               foreach ($this->navigation as $key => $nav) {?>
                    <li class="has-dropdown<?=(in_array($nav['id_tree'],$idParent)?' active':'')?>">
                        <a href="<?= $this->lurl.'/'.$nav['slug'] ?>" class="<?=(in_array($nav['id_tree'],$idParent)?'active':'')?>">
                            <figure>
                                <?=(!empty($this->menuIcones[$nav['id_tree']])?'<i class="'.$this->menuIcones[$nav['id_tree']].'"></i>':'')?>
                            </figure>

                            <span><?= $nav['menu_title'] ?></span>
                        </a>
<?php                   $this->ssNavigation = (new o\tree())->getArboSiteZoneProfil($this->language, $nav['id_tree']);
                        if(count($this->ssNavigation)>0) { ?>
                            <div class="menu__dropdown<?=(in_array($nav['id_tree'],$idParent)?' active':'')?>">
                                <h5>
                                    <a href="<?= $this->lurl.'/'.$nav['slug'] ?>">
                                        <?=(!empty($this->menuIcones[$nav['id_tree']])?'<i class="'.$this->menuIcones[$nav['id_tree']].'"></i>':'')?>
                                        <?= $nav['menu_title'] ?>
                                    </a>
                                </h5>

                                <ul>
<?php                       foreach ($this->ssNavigation as $key => $ssNav) {
                                    $this->ssSsNavigation = (new o\tree())->getArboSiteZoneProfil($this->language, $ssNav['id_tree']);?>
                                    <li class="<?=(count($this->ssSsNavigation)>0?'has-dropdown':'')?><?=(in_array($ssNav['id_tree'],$idParent)?' active':'')?>">
                                        <a href="<?= $this->lurl.'/'.$ssNav['slug'] ?>"><?= $ssNav['menu_title'] ?></a>
<?php                                   if(count($this->ssSsNavigation)>0) { ?>
                                        <ul <?=(in_array($ssNav['id_tree'],$idParent)?'style="display:block"':'')?>>
<?php                                           foreach ($this->ssSsNavigation as $key => $ssSsNav) {
                                                $this->SSssSsNavigation = (new o\tree())->getArboSiteZoneProfil($this->language, $ssSsNav['id_tree']);?>
                                            <li class="<?=(in_array($ssSsNav['id_tree'],$idParent)?'active':'')?>">
                                                <a href="<?= $this->lurl.'/'.$ssSsNav['slug'] ?>"><?= $ssSsNav['menu_title'] ?></a>
<?php                                           if(count($this->SSssSsNavigation)>0) { ?>
                                                <ul class="lv-4" <?=(in_array($ssNav['id_tree'],$idParent)?'style="display:block"':'')?>>
<?php                                               foreach ($this->SSssSsNavigation as $key => $SSssSsNav) { ?>
                                                    <li class="<?=($SSssSsNav['id_tree'] == $this->tree->id_tree?'active':'')?>"><a href="<?= $this->lurl.'/'.$SSssSsNav['slug'] ?>"><?= $SSssSsNav['menu_title'] ?></a></li>
<?php                                               } ?>
                                                </ul>
<?php                                           } ?>
                                            </li>
<?php                                       } ?>
                                        </ul>
                                        <?php } ?>
                                    </li>
<?php                       } ?>
                                </ul>
                            </div>
<?php                   }?>
                    </li>
<?php               } ?>
                    <li  class="<?=($this->menu_actif=="cart"?'active':'')?>">
                        <a href="<?=$this->lurl?>/cart/shared">
                            <figure>
                                <i class="ico-shop"></i>
                            </figure>

                            <span>Shared carts</span>
                        </a>
                    </li>
                </ul>
            </nav><!-- /.nav -->

            <div class="menu__actions">
                <a href="<?=$this->lurl?>/logout" class="link link--alt">
                    <i class="ico-logout"></i>
                    <span>Log out</span>
                </a>
            </div><!-- /.menu__actions -->
        </div><!-- /.menu__content -->
    </div><!-- /.menu -->


    <div class="content<?=(!empty($this->tree->id_tree) && $this->tree->id_tree != "1"?' content--small':'')?>">
        <header class="header">
            <div class="header__aside">
                <a href="#" class="nav-trigger">
                    <span></span>

                    <span></span>

                    <span></span>
                </a>

                <form action="<?=$this->lurl?>/search" method="post">
                    <div class="search">
                        <input type="search" id="aa-search-input" class="aa-input-search search__field" placeholder="Find a document" name="search" autocomplete="off"/>

                        <button type="submit" class="search__btn">
                            <i class="ico-search"></i>
                        </button>
                    </div><!-- /.search -->
                </form>
            </div><!-- /.header__aside -->

            <a href="<?=$this->lurl?>" class="logo">URIAGE EAU THERMALE</a>

            <div class="header__actions">
                <a href="#" class="btn-action">
                    <i class="ico-account"></i>

                    <span class="btn__text">Hello <?=$_SESSION['client']['prenom']?> <?=$_SESSION['client']['nom']?></span>
                </a>

                <a href="<?=$this->lurl?>/cart" class="btn-action btn-action--circle">
                    <i class="ico-cart"></i>

                    <span class="btn__number" id="nbr_documents"><?=(!empty($_SESSION['cart']['documents'])?count($_SESSION['cart']['documents']):'')?></span>
                </a>
            </div><!-- /.header__actions -->
        </header><!-- /.header -->