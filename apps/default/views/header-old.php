<?php if($_SESSION['auth'] == 'ok') { ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Nuxe Partners</title>

        <link rel="shortcut icon" type="image/x-icon" href="<?=$this->surl?>/styles/default/images/favicon.ico" />

        <!-- Vendor Styles -->
        <link rel="stylesheet" href="/vendor/dropdown/dropdown.css" />
        <link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.css" />
        <link rel="stylesheet" href="/vendor/owl-carousel/owl.carousel.min.css" />
        <link rel="stylesheet" href="/vendor/chosen_v1.5.1/chosen.min.css" />
        <link rel="stylesheet" href="/vendor/font-awesome-4.5.0/css/font-awesome.min.css">

        <!-- App Styles -->
        <link rel="stylesheet" href="<?=$this->surl?>/styles/default/style.css" />

        <!-- Vendor JS -->
        <script src="/vendor/jquery-1.11.3.min.js"></script>
        <script src="/vendor/dropdown/core.js"></script>
        <script src="/vendor/dropdown/touch.js"></script>
        <script src="/vendor/chosen_v1.5.1/chosen.jquery.min.js"></script>
        <script src="/vendor/dropdown/dropdown.js"></script>
        <script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
        <script src="/vendor/owl-carousel/owl.carousel.min.js"></script>

        <!-- App JS -->
        <script src="/scripts/default/functions.js"></script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-75054522-1', 'auto');
            ga('send', 'pageview');
        </script>




    </head>
    <?php

    if($_SERVER['REMOTE_ADDR']=='109.7.252.175')
    {
        echo 'timing 1:'.mktime();
    }
    $bc = $this->tree->getBreadCrumb($this->tree->id_tree,$this->language);
    $currentBrand = $bc[1]->id_tree;
    if(count($bc)==1)
        $currentBrand = $this->tree->id_tree;
    $this->currentBrand = $currentBrand;
    $currentNav = $bc[2]->id_tree;
    if($_SERVER['REMOTE_ADDR']=='109.7.252.175')
    {
        echo 'timing 2:'.mktime();
    }
    ?>
<body>
    <?php
    $nav = (new o\tree())->getArboSiteWithStatus($this->language, $currentBrand);

    $nav_select = (new o\tree())->getArboSiteWithStatus($this->language, "1");
    if($_SERVER['REMOTE_ADDR']=='109.7.252.175')
    {
        error_reporting(9999999);ini_set('display_errors','on');
        echo 'timing 3:'.mktime();
    }
    ?>
<div class="wrapper">
    <header class="header">
        <div class="header-inner">
            <select class="select select-site">
                <?php foreach ($nav_select as $level1) {
                    ?>
                    <option value="<?=$this->language?>/<?=$level1['slug']?>"
                        <?=($currentBrand==$level1['id_tree']?' selected':'')?>>
                        <?=$level1['menu_title']?>
                    </option>
                <?php } ?>
            </select>
            <style>
                .header-inner .fs-dropdown { float:left; }
                .sidebar { padding: 116px 20px 50px; }
                .wrapper { padding-top: 96px; }

                .aa-input-container {
                    display: inline-block;
                    position: relative;
                    width: 270px;
                    margin-left: 5px;
                    float: left; }
                .aa-input-search {
                    width: 100%;
                    border: 1px solid rgba(228, 228, 228, 0.6);
                    padding: 12px 28px 12px 12px;
                    font-family: Arial;
                    box-sizing: border-box;
                    -webkit-appearance: none;
                    -moz-appearance: none;
                    appearance: none; }
                .aa-input-search::-webkit-search-decoration, .aa-input-search::-webkit-search-cancel-button, .aa-input-search::-webkit-search-results-button, .aa-input-search::-webkit-search-results-decoration {
                    display: none; }
                .aa-input-icon {
                    height: 25px;
                    width: 25px;
                    position: absolute;
                    top: 50%;
                    right: 3px;
                    -webkit-transform: translateY(-50%);
                    transform: translateY(-50%);
                    fill: #e4e4e4; background-color: #034638 }
                .aa-dropdown-menu {
                    background-color: #fff;
                    border: 1px solid rgba(228, 228, 228, 0.6);
                    min-width: 270px;
                    width:100%;
                    box-sizing: border-box; }
                .aa-suggestion {
                    padding: 12px;
                    cursor: pointer;
                }
                .aa-suggestion + .aa-suggestion {
                    border-top: 1px solid rgba(228, 228, 228, 0.6);
                }

                .aa-suggestion .label {font-weight: 700;
                    font-size: 9px;
                    color: #fff;
                    text-transform: uppercase;
                    background-color: #034638;
                    line-height: 1;
                    border-radius: 3px;
                    padding: 2px 2px 1px;

                    opacity: 1;
                    transition: opacity .4s ease;}
                .aa-suggestion:hover, .aa-suggestion.aa-cursor {
                    background-color: rgba(241, 241, 241, 0.35); }
                .algolia-autocomplete { width:100%; }
            </style>
            <!-- HTML Markup -->
            <div class="aa-input-container" id="aa-input-container">
                <input type="search" id="aa-search-input" onclick="ga('send','event','Navigation','search','Utilisation du moteur de recherche')" class="aa-input-search" placeholder="Find a document on NUXE Partners" name="search" autocomplete="off" style="height:30px; font-size:14px"/>
                <svg class="aa-input-icon" viewBox="400 -600 2100 2100">
                    <path d="M1806,332c0-123.3-43.8-228.8-131.5-316.5C1586.8-72.2,1481.3-116,1358-116s-228.8,43.8-316.5,131.5  C953.8,103.2,910,208.7,910,332s43.8,228.8,131.5,316.5C1129.2,736.2,1234.7,780,1358,780s228.8-43.8,316.5-131.5  C1762.2,560.8,1806,455.3,1806,332z M2318,1164c0,34.7-12.7,64.7-38,90s-55.3,38-90,38c-36,0-66-12.7-90-38l-343-342  c-119.3,82.7-252.3,124-399,124c-95.3,0-186.5-18.5-273.5-55.5s-162-87-225-150s-113-138-150-225S654,427.3,654,332  s18.5-186.5,55.5-273.5s87-162,150-225s138-113,225-150S1262.7-372,1358-372s186.5,18.5,273.5,55.5s162,87,225,150s113,138,150,225  S2062,236.7,2062,332c0,146.7-41.3,279.7-124,399l343,343C2305.7,1098.7,2318,1128.7,2318,1164z" />
                </svg>
            </div>
            <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
            <script src="https://cdn.jsdelivr.net/algoliasearch/3/algoliasearch.min.js"></script>
            <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
            <!-- Initialize autocomplete menu -->
            <script>
                var client = algoliasearch('1KGT2I30J4', 'ab514ca5cbeae2e95a42fc57b805bbe6');
                var index = client.initIndex('nuxepartners');
                //initialize autocomplete on search input (ID selector must match)
                autocomplete('#aa-search-input',
                    { hint: false }, {
                        source: autocomplete.sources.hits(index, {hitsPerPage: 10,filters: 'status:1'}),
                        //value to be displayed in input control after user's suggestion selection
                        displayKey: 'label',
                        //hash of templates used when rendering dataset
                        templates: {
                            //'suggestion' templating function used to render a single suggestion
                            suggestion: function(suggestion) {
                                var isnew = '';
                                if(suggestion.new==1)
                                    isnew = '<span class="label">NEW</span>';
                                if(suggestion._highlightResult.thumbnail.value=='')
                                    suggestion._highlightResult.thumbnail.value= 'default.png';
                                return '<span style="font-size:12px; line-height:18px">' +
                                    '<div style="width:30px; display:inline-block; margin-right:3px; text-align:center"><img src="'.$this->lurl.'/thumb/'+suggestion.thumbnail+'" style="height:18px"/></div>' +
                                    suggestion._highlightResult.label.value + '</span> - <span>' +
                                    suggestion._highlightResult.type.value + ' </span> ';
                            }
                        }
                    }).on('autocomplete:selected', function (event, suggestion, dataset) {
                    console.log(suggestion, dataset);
                    document.location = suggestion.url;
                });
            </script>
            <a href="/<?=$this->language?>" class="logo">
                <img src="<?=$this->surl?>/styles/default/images/logo-simple@2x.png" alt="" width="155" height="42">
            </a>
            <nav class="nav-utilities">
                <ul>
                    <li>
                        <a href="#">
                            <i class="ico-user"></i>
                            <span>Hello <?=$_SESSION['client']['prenom']?> <?=$_SESSION['client']['nom']?></span>
                        </a>
                        <a href="/logout" class="disconnect"> / Log out</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <div class="main">
    <aside class="sidebar">
        <div class="sidebar-head">
            <a href="/<?=$this->tree->getslug($currentBrand,$this->language)?>" class="logo-site">
                <img src="<?=$this->surl?>/styles/default/images/logo-<?=($currentBrand==1?2:$currentBrand) ?>.png" height="54" width="127" alt="">
            </a>
        </div>
        <div class="sidebar-body">
            <button class="btn-nav">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </button>
            <div class="accordion">
                <?php foreach($nav as $page) { 
                $enfants[$page['id_tree']][] = $page['id_tree']; ?>
                    <div class="accordion-section">
                        <div class="accordion-head">
                            <h3 <?=($page['nbr_of_children']==0||$this->tree->id_tree==1?'onclick="document.location=\''.$this->lurl.'/'.$page['slug'].'\'"':'')?>><?=$page['menu_title']?></h3>
                        </div>
                        <?php if($this->tree->id_tree > 1) { ?>
                            <div class="accordion-body" <?=($page['id_tree']==$currentNav?' style="display:block"':'')?>>
                                <nav class="nav-primary">
                                    <ul>
                                        <?php
                                        if(!empty($page['children'])){
                                            foreach($page['children'] as $child) {
                                                if($_SESSION['new'.$child['id_tree']]=='') {
                                                    $where = 'new=1 AND status=1 AND id_document in (select id_document FROM documents_tree WHERE id_tree in ('.$child['id_tree'].')) AND id_document in (SELECT id_document FROM documents_geozones where id_geozone in ('.$_SESSION['user_geozones'].')) AND id_document IN (SELECT id_document FROM documents_profiles where id_profile in ('.$_SESSION['user_profiles'].'))';
                                                    $docs = (new o\data('documents'))->addWhere($where)->order('new','DESC','','label','ASC','');
                                                    $_SESSION['new'.$child['id_tree']] = count($docs);
                                                }

                                                $enfants[$page['id_tree']][] = $child['id_tree'];
                                                $enfants[$child['id_tree']][] = $child['id_tree']; ?>
                                                <li <?=($child['id_tree']==$this->tree->id_parent || $child['id_tree']==$this->tree->parent->id_parent?'class="active"':'')?>>
                                                    <a href="<?=$this->lurl?>/<?=$child['slug']?>" <?=($child['id_tree']==$this->tree->id_tree || $child['id_tree'] ==$this->tree->id_parent?' class="current"':'')?>>
                                                        <?=$child['menu_title']?><?=($_SESSION['new'.$child['id_tree']]>0?'<span class="label">NEW</span>':'')?>
                                                    </a>
                                                    <?php if($child['nbr_of_children'] != 0) { ?>
                                                        <div class="nav-dropdown">
                                                            <ul>
                                                                <?php foreach($child['children'] as $ss) {
                                                                    $enfants[$page['id_tree']][] = $ss['id_tree'];
                                                                    $enfants[$child['id_tree']][] = $ss['id_tree'];
                                                                    $enfants[$ss['id_tree']][] = $ss['id_tree'];
                                                                     ?>
                                                                    <li <?=($ss['id_tree']==$this->tree->id_parent?'class="active"':'')?>>
                                                                        <a href="<?=$this->lurl?>/<?=$ss['slug']?>">
                                                                            <?=$ss['menu_title']?>
                                                                        </a>
                                                                        <?php if($ss['nbr_of_children'] != 0) { ?>
                                                                            <div class="nav-dropdown">
                                                                                <ul style="background-color: #e4e2e2">
                                                                                    <?php foreach($ss['children'] as $sss) {
                                                                                        $enfants[$page['id_tree']][] = $sss['id_tree'];
                                                                                        $enfants[$child['id_tree']][] = $sss['id_tree'];
                                                                                        $enfants[$ss['id_tree']][] = $sss['id_tree'];
                                                                                        $enfants[$sss['id_tree']][] = $sss['id_tree'];?>
                                                                                        <li <?=($sss['id_tree']==$this->tree->id_tree?'class="thirdactive"':'')?>>
                                                                                            <a href="<?=$this->lurl?>/<?=$sss['slug']?>">
                                                                                                <?=$sss['menu_title']?>
                                                                                            </a>
                                                                                        </li>
                                                                                    <?php } ?>
                                                                                </ul>
                                                                            </div>
                                                                        <?php }?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    <?php }?>
                                                </li>
                                            <?php }
                                        } ?>
                                    </ul>
                                </nav>
                            </div>
                        <?php } ?>
                    </div>
                <?php  }
                $_SESSION['nav'] = serialize($enfants);  ?>
            </div>
        </div>
    </aside>
<?php }

if($_SERVER['REMOTE_ADDR']=='109.7.252.175')
{
    echo 'timing end head:'.mktime();
}