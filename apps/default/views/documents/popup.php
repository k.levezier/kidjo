<?php

    $activeLang = [];
    foreach(['en','fr','es'] as $ln){ $aTester = 'file_'.$ln;
        if(empty($activeLang) && !empty($this->doc->$aTester)){
            $activeLang = $ln;
            $file = 'file_'.$ln;
            $thumbnail = 'thumbnail_'.$ln;
            $size = 'size_'.$ln;
        }
    }

    echo $this->fireBloc('document-popup');
