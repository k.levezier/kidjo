            <div id="popup-document" class="component-popup mfp-hide"></div>
        </div><!-- /.content -->
    </div><!-- /.wrapper -->

    <script src="<?=$this->surl?>/assets/vendor/jquery-3.1.1.min.js"></script>
    <script src="<?=$this->surl?>/assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?=$this->surl?>/assets/vendor/pretty-dropdowns/js/jquery.prettydropdowns.js"></script>
    <script src="<?=$this->surl?>/assets/js/bundle.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


        <!-- Include AlgoliaSearch JS Client and autocomplete.js library -->
        <script src="https://cdn.jsdelivr.net/npm/algoliasearch@3.35.0/dist/algoliasearchLite.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@3.4.0/dist/instantsearch.production.min.js"></script>
        <script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.min.js"></script>
        <!-- Initialize autocomplete menu -->
        <script>

            $(function() {
                var client = algoliasearch('CBXZ2YNNVG', '77e2192836ee057f67fcf0880553ded0');
                var index = client.initIndex('Tree');

                autocomplete('#aa-search-input',
                    { hint: false }, {
                        source: autocomplete.sources.hits(index, {hitsPerPage: 10}),
                        //value to be displayed in input control after user's suggestion selection
                        displayKey: 'label',
                        //hash of templates used when rendering dataset
                        templates: {
                            //'suggestion' templating function used to render a single suggestion
                            <?php /* suggestion: function(suggestion) {
                                var isnew = '';
                                if(suggestion.new==1)
                                    isnew = '<span class="label">NEW</span>';
                                if(suggestion.thumbnail=='')
                                    suggestion.thumbnail= 'default.png';
                                return '<span class="label">' +
                                    '<div class="image">' +
                                        '<img src="<?=$this->surl?>/thumb/'+suggestion.thumbnail+'" style="height:18px"/>' +
                                    '</div>' +suggestion._highlightResult.label.value + '</span> - ' +
                                    '<span class="label-extension">'+suggestion._highlightResult.type.value + ' </span> ';
                            }*/ ?>
                           suggestion: function(suggestion) {
                                return '<div class="parents">' +suggestion._highlightResult.parents.value + '</div><div class="label">' +suggestion._highlightResult.menu_title.value + '</div>';
                            }
                        }
                    }).on('autocomplete:selected', function (event, suggestion, dataset) {
                        console.log(suggestion, dataset);
                        if(suggestion.url != "//404")
                            document.location = suggestion.url;
                    });
            });
        </script>
</body>
</html>