<?php

use o\data;
use PHPZip\Zip\File\Zip as ZipArchiveFile;

class rootController extends bootstrap {

    public function __construct($command, $config, $app) {
        parent::__construct($command, $config, $app);
        if($_SESSION['auth']=='ok')
        {
            if(!isset($_SESSION['user_geozones']) || !isset($_SESSION['user_profiles']))
            {

                $this->cgc = (new o\data('clients_geozones'))->addWhere('id_client='.$_SESSION['client']['id_client']);
                $this->cpc = (new o\data('clients_profiles'))->addWhere('id_client='.$_SESSION['client']['id_client']);

                $z = [];
                foreach($this->cgc as $g)
                    $z[] = $g->id_geozone;

                $pp = [];
                foreach($this->cpc as $p)
                    $pp[] = $p->id_profile;

                $_SESSION['user_geozones'] = implode(',',$z);
                $_SESSION['user_profiles'] = implode(',',$pp);
            }
        }
    }

    public function _how_to_use(){
        $this->autoFireHeader = false;
        $this->autoFireFooter = false;
    }


    public function _search()
    {
        $this->clDocuments = new clDocuments();

        $this->lDocuments = [];

        if(!empty($_POST['search'])){
            require $this->path.'vendor/algolia/algoliasearch-client-php/autoload.php';
            $appId = "CBXZ2YNNVG";
            $apiKey = "40ba07b382d37d829b1fa37f787dff0d";
            $client = \Algolia\AlgoliaSearch\SearchClient::create($appId, $apiKey);

            $index = $client->initIndex('Documents');

            //$res = $index->search('Thermal');
            $res = $index->search($_POST['search'], [
                'attributesToRetrieve' => [
                    'objectID',
                ],
                'hitsPerPage' => 50
            ]);

            foreach($res['hits'] as $r){
                $obj = new o\documents(['id_document'=>$r['objectID']]);

                if(!empty($obj->id_document) && $obj->status == 1)
                    $this->lDocuments[] = $obj;
            }
        }

    }

    /**
     * Permet de placer le cookie pour masquer l'affichage
     * du bandeau au clic sur le bouton.
     *
     * @function _alertCookie
     */

    function _newpass()
    {
        //ini_set('display_errors',1);
        //error_reporting(9999);
        //if(empty($_POST['eml'])) $_POST['eml'] = $this->params[0];

        $this->clients = new o\clients(array('email'=>$_POST['eml']));
        if($this->clients->exist())
        {
            $this->clients->password="";
            $this->clients->password_old="";
            $this->clients->save();

            $vars = [
                'client_prenom_nom' => $this->clients->prenom." ".$this->clients->nom,
                'client_email' => $this->clients->email,
                'surl' => $this->surl,
                'lien' => $this->lurl.'/newpassword/'.$this->clients->hash,
                'meta_tile' => "Forgotten password",
                'x-splio-ref' => $this->Config['env'] . '_mdp-oublie_' . date('Y-m-d')
            ];

            $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
            $return = $this->clEmail->sendMail("mdp-oublie", 'en', $this->clients->email,  $vars);

            echo 'You will receive a email in a few minutes';
        }
        else
        {
            echo 'We found no account with this email address';
        }

        die;
    }

    public function _alertCookie() {
        $this->autoFireNothing = true;
        setcookie('cookieAlertCookie', $_POST['cookieContent'], strtotime('+365 DAYS'), '/');
    }

    public function _testEmail() {
//        $data['login'] = "m.louis@equinoa.com";
//        $data['password'] = "FY294zOfA5";
//        $this->envoyerMail('test-mail', 'en', 'm.louis@equinoa.com', $data);
        $this->autoFireNothing = true;
        die;

    }

    function _update()
    {
        $this->clDocuments = new clDocuments();
        $this->documents = new o\data('documents');
        error_reporting(9999);
        ini_set('display_errors','on');
        ini_set('max_execution_time',3000);
        $this->documents = (new o\data('documents'))->addWhere('file like "%.mov"');
        foreach($this->documents as $doc)
        {
            try {
                //echo $this->path.'documents/'.$doc->file.'<br/>';
                if(file_exists($this->path.'protected/documents/'.$doc->file))
                {
                    $ext = explode('.',$doc->file);
                    $ledoc = new o\documents(array('id_document'=>$doc->id_document));
                    $ledoc->size = filesize($this->path.'protected/documents/'.$doc->file);
                    $ext[1] = strtolower($ext[1]);
                    $ledoc->extension = $ext[1];
                    echo $doc->file.'<br/>';
                    if($ext[1]=='pdf')
                    {
                        $im = new imagick($this->path.'protected/documents/'.$doc->file.'[0]');
                        $im->setImageFormat('jpg');
                        $im->scaleImage(200,200,true);
                        $im = $im->flattenImages();
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                        $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        //print_r($im);die;

                    }
                    if($ext[1]=='pptx' || $ext[1]=='ppt')
                    {
                        if(!file_exists($this->path.'protected/documents/previews/'.$doc->file.'.pdf'))
                        {
                            exec('curl -L "https://api.cloudconvert.com/convert" -F inputformat="'.$ext[1].'" -F outputformat="pdf" -F file=@"'.$this->path.'protected/documents/'.$doc->file.'" -F "apikey=5nRFFinyCYM87MJdum5nx_QvMmA_MW5G-VAng2ECuDd5MHb3r6ydyuEUdWSnoZyi0DhtefDyVCoDK09xGYA3GQ" -F "input=upload" -F "download=inline" > "'.$this->path.'protected/documents/previews/'.$doc->file.'.pdf'.'"');
                            $ledoc->preview = $doc->file.'.pdf';
                        }
                        else
                        {
                            $im = new imagick($this->path.'protected/documents/previews/'.$doc->file.'.pdf[0]');
                            $im->setImageFormat('jpg');
                            $im->scaleImage(200,200,true);
                            $im = $im->flattenImages();
                            //header('Content-Type: image/jpeg');
                            $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                            $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        }

                    }
                    if($ext[1]=='docx' || $ext[1]=='doc')
                    {
                        if(!file_exists($this->path.'protected/documents/previews/'.$doc->file.'.pdf'))
                        {
                            exec('curl -L "https://api.cloudconvert.com/convert" -F inputformat="'.$ext[1].'" -F outputformat="pdf" -F file=@"'.$this->path.'protected/documents/'.$doc->file.'" -F "apikey=5nRFFinyCYM87MJdum5nx_QvMmA_MW5G-VAng2ECuDd5MHb3r6ydyuEUdWSnoZyi0DhtefDyVCoDK09xGYA3GQ" -F "input=upload" -F "download=inline" > "'.$this->path.'protected/documents/previews/'.$doc->file.'.pdf'.'"');
                            $ledoc->preview = $doc->file.'.pdf';
                        }
                        else
                        {
                            $im = new imagick($this->path.'protected/documents/previews/'.$doc->file.'.pdf[0]');
                            $im->setImageFormat('jpg');
                            $im->scaleImage(200,200,true);
                            $im = $im->flattenImages();
                            //header('Content-Type: image/jpeg');
                            $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                            $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        }
                    }
                    if($ext[1]=='mov')
                    {
                        exec('ffmpeg -i "/home/nuxepartners/www/protected/documents/'.$doc->file.'" -vcodec h264 -acodec aac -strict -2 -y -movflags +faststart "/home/nuxepartners/www/protected/documents/previews/'.$doc->file.'.mp4"');
                        echo 'ffmpeg -i "/home/nuxepartners/www/protected/documents/'.$doc->file.'" -vcodec h264 -acodec aac "/home/nuxepartners/www/protected/documents/previews/'.$doc->file.'.mp4"';
                        if(file_exists($this->path.'protected/documents/previews/'.$doc->file.'.mp4'))
                            $ledoc->preview = $doc->file.'.mp4';
                        exec('ffmpeg -i "/home/nuxepartners/www/protected/documents/'.$doc->file.'" -ss 00:00:15.000 -vframes 1 "/home/nuxepartners/www/protected/documents/thumbs/'.$doc->file.'-thumb.jpg"');
                        if(file_exists($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg'))
                            $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        //print_r($im);die;

                    }
                    if($ext[1]=='mp4')
                    {
                        exec('ffmpeg -i "/home/nuxepartners/www/protected/documents/'.$doc->file.'" -ss 00:00:15.000 -vframes 1 "/home/nuxepartners/www/protected/documents/thumbs/'.$doc->file.'-thumb.jpg"');
                        if(file_exists($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg'))
                            $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        //print_r($im);die;

                    }
                    if($ext[1]=='jpg' || $ext[1]=='jpeg')
                    {
                        $im = new imagick($this->path.'protected/documents/'.$doc->file.'[0]');
                        $im->thumbnailImage(600,600,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-bigthumb.jpg');;
                        $im->thumbnailImage(200,200,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                        $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        //print_r($im);die;

                    }
                    if($ext[1]=='png')
                    {
                        $im = new imagick($this->path.'protected/documents/'.$doc->file.'[0]');
                        $im->thumbnailImage(600,600,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-bigthumb.png');;
                        $im->thumbnailImage(200,200,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.png');;
                        $ledoc->thumbnail = $doc->file.'-thumb.png';
                        //print_r($im);die;

                    }
                    if($ext[1]=='psd')
                    {
                        $im = new imagick($this->path.'protected/documents/'.$doc->file.'[0]');
                        $im->setImageFormat('jpg');
                        $im->scaleImage(500,500,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-bigthumb.jpg');;
                        $im->scaleImage(200,200,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                        $ledoc->thumbnail = $doc->file.'-thumb.jpg';


                    }
                    if($ext[1]=='tiff')
                    {
                        $im = new imagick($this->path.'protected/documents/'.$doc->file.'');
                        $im->setImageFormat('jpg');
                        $im->scaleImage(500,500,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-bigthumb.jpg');;
                        $im->scaleImage(200,200,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                        $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        //print_r($im);die;

                    }
                    if($ext[1]=='eps')
                    {
                        if(!file_exists($this->path.'protected/documents/previews/'.$doc->file.'.pdf'))
                        {
                            exec('curl -L "https://api.cloudconvert.com/convert" -F inputformat="'.$ext[1].'" -F outputformat="pdf" -F file=@"'.$this->path.'protected/documents/'.$doc->file.'" -F "apikey=5nRFFinyCYM87MJdum5nx_QvMmA_MW5G-VAng2ECuDd5MHb3r6ydyuEUdWSnoZyi0DhtefDyVCoDK09xGYA3GQ" -F "input=upload" -F "download=inline" > "'.$this->path.'protected/documents/previews/'.$doc->file.'.pdf'.'"');
                            $ledoc->preview = $doc->file.'.pdf';
                        }
                        else
                        {
                            $im = new imagick($this->path.'protected/documents/previews/'.$doc->file.'.pdf[0]');
                            $im->setImageFormat('jpg');
                            $im->scaleImage(200,200,true);
                            $im = $im->flattenImages();
                            //header('Content-Type: image/jpeg');
                            $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                            $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                        }


                    }
                    if($ext[1]==''&& is_dir($this->path.'protected/documents/'.$doc->file) && file_exists($this->path.'protected/documents/'.$doc->file.'/PDF/'.$doc->file.'.pdf'))
                    {
                        $im = new imagick($this->path.'protected/documents/'.$doc->file.'/PDF/'.$doc->file.'.pdf[0]');
                        $im->setImageFormat('jpg');
                        $im->scaleImage(200,200,true);
                        //header('Content-Type: image/jpeg');
                        $im->writeImage($this->path.'protected/documents/thumbs/'.$doc->file.'-thumb.jpg');;
                        $ledoc->thumbnail = $doc->file.'-thumb.jpg';
                    }

                    $ledoc->save();


                }
            }
            catch(Exception $e)
            {
                echo 'ERREUR'.$e->getMessage().'<br/>';
            }
        }
        die;
    }

    function _keepalive()
    {
        if($this->params[1]==md5('abc'.$this->params[0]))
        {
            $clt = new o\clients($this->params[0]);
            $logins= new o\logins();
            $logins->ip = $_SERVER['REMOTE_ADDR'];
            $logins->id_client = $clt->id_client;
            $logins->email = $clt->email;
            $logins->reset=1;
            $logins->insert();
            $_SESSION['confirm'] = 'confirm';
            header('location:/en');die;
        }
    }

    /*function _hygiene()
    {
        $clts = new o\data('clients');
        $clts->addWhere('id_client not in (select id_client from logins WHERE added > date_add(now(),interval -3 month))');
        foreach($clts as $c)
        {
            $data = array();
            $data['email'] = $c->id_client.'/'.md5('abc'.$c->id_client);
            $this->envoyerMail('mail-trimestriel', 'en', $c->email, $data);
            $i++;
        }
        echo $i.' mails sent';die;
    }*/

    function _killold()
    {
        if(date('Y-m-d')>'2017-01-05')
        {
            $clts = new o\data('clients');
            $clts->addWhere('id_client not in (select id_client from logins WHERE added > date_add(now(),interval -3 month))');
            foreach($clts as $c)
            {
                $c->email = 'DISABLED-'.$c->email;
                $c->save();
            }
        }
        die;
    }

    public function _default()
    {
        if($_SERVER['REMOTE_ADDR']=='109.7.252.175')
        {
            echo 'timing:'.mktime();
        }

        // Traitement du slug
        $paramSlug = $this->clFonctions->cleanSlug(str_replace('?','',$this->params[0]));
        $slugProduct = $this->params[1];

        $this->clDocuments = new clDocuments();

        // On regarde si on a une redirection sur ce slug
        $this->redirections = new o\redirections(array('from_slug' => $paramSlug, 'status' => 1, 'id_langue' => $this->language));

        if($this->redirections->exist())
        {
            header('location:'.$this->lurl.'/'.$this->redirections->to_slug, true, $this->redirections->type);
            die;
        }

        // Récupération des infos de la page
        $this->tree = new o\tree(array('slug' => $paramSlug, 'id_langue' => $this->language));
        if(!$this->tree->exist()){
            $this->tree = new o\tree(array('id_tree' => 1, 'id_langue' => $this->language));
        }
        if($this->tree->exist())
        {
            // Vérification des droits de l'utilisateur à consulter cette page
            // On parcourt tout le le breadcrumb
            $breadCrumb = $this->tree->getBreadCrumb($this->tree->id_tree,$this->language);
            foreach($breadCrumb as $p){
                if(!$this->tree->authorized($p['id_tree']) && $p['id_tree']!= 1){
                    header('location:'.$this->lurl);
                    die;
                    //echo "<!-- vérification des droits PAS OK DU TOUT ".$p['id_tree']." -->";
                }else{
                    //echo "<!-- vérification des droits OK ".$p['id_tree']." -->";
                }
            }

            // Déclaration des metas pour l'arbo
            $this->meta_title = strip_tags($this->tree->meta_title);
            $this->meta_description = strip_tags($this->tree->meta_description);
            $this->meta_keywords = $this->tree->meta_keywords;

            // Récuperation du template de la page
            $this->template = $this->tree->template;
            $this->current_template = $this->template->name.' | '.$this->template->slug;

            // Chargement CSS
            $this->loadCss('magnific-popup');
            $this->loadCss('owl.carousel.min');
            $this->loadCss('scrollbar');
            $this->loadCss('dropdown');
            $this->loadCss('style');

            // Chargement JS
            $this->loadJs('jquery-1.11.3.min');
            $this->loadJs('jquery.magnific-popup.min');
            $this->loadJs('owl.carousel.min');
            $this->loadJs('core');
            $this->loadJs('dropdown');
            $this->loadJs('functions');

            if($this->template->affichage != 1)
            {
                // On renvoie vers le premier parent qui a un affichage à 1
                header('Location:'.$this->lurl.'/'.$this->tree->getSlug($this->tree->getFirstUnlock($this->tree->id_tree, $this->language), $this->language), true, 301);
                die;
            }

            // Dans le cas où on a pas de template c'est-à-dire pas de page derrière le lien on prend le premier enfant
            if(is_null($this->tree->id_template))
            {
                $this->subpages = (new o\data('tree', array('id_parent' => $this->tree->id_tree, 'id_langue' => $this->language)))->order('ordre', 'ASC');

                if($this->subpages->count() > 0)
                {
                    header('Location:'.$this->lurl.'/'.$this->subpages[0]['slug'], true, 301);
                    die;
                }
            }

            // Pour récupérer un contenu de la page : $this->tree->getContent($slug) // $this->tree->getContentCompl($slug)

            // Pour récupérer un contenu d'une page enfant : $this->tree->getChildContent($slug) // $this->tree->getChildContentCompl($slug)

            // Pour récupérer un bloc : $this->bloc = new o\blocs(array('slug' => $slug_bloc));
            // Pour récupérer le contenu d'un élément de ce bloc : $this->bloc->getContent($slug_element) // $this->bloc->getContentCompl($slug_element)

            // Création du breadcrumb
            $this->breadCrumb = $this->tree->getBreadCrumb($this->tree->id_tree, $this->language);

            // Récupération de la navigation niveau 1
            $this->firstLevelNav = (new o\data('tree', array('id_parent' => NULL, 'status_menu' => 1, 'status' => 1, 'id_langue' => $this->language)))->order('ordre', 'ASC');

            // Vérification d'accès à une page privée
            if($this->tree->prive == 1 && !(new o\clients())->checkAccess())
            {
                $this->autoFireHeader = false;
                $this->autoFireFooter = false;

                if($this->params[0]=="newpassword"){
                    $this->client = new o\clients(['hash'=>$this->params[1]]);
                    if(empty($this->client->id_client) || !empty($this->client->password) || !empty($this->client->password_old)){
                        header('location:'.$this->lurl);
                        die;
                    }

                    if(isset($_POST['new_password'])){
                        if($_POST['password'] == $_POST['password2']){
                            $this->client->password = md5($_POST['password']);
                            $this->client->save();
                            header('location:'.$this->lurl.'/success_pw');
                            die;
                        }else{
                            $this->error = 'Password confirmation is wrong!';
                        }
                    }
                }

                $this->setView('../templates/login',true);
            }
            else
            {
                // Chargemement du template
                if($this->template->slug == '')
                {
                    header("HTTP/1.0 404 Not Found");
                    header('location:'.$this->lurl.'/404');
                    die;
                }
                elseif($this->tree->status == 0 && !isset($_SESSION['user']))
                {
                    header("HTTP/1.0 404 Not Found");
                    header('location:'.$this->lurl.'/404', true, 301);
                    die;
                }
                else
                {
                    $this->setView('../templates/'.$this->template->slug, true);
                }
            }
        } else {
            header("HTTP/1.0 404 Not Found");
            header('location:'.$this->lurl, true,301);
            die;
        }


        if($this->tree->id_template==3 && $this->tree->id_tree>1)
        {
            $sql = 'SELECT count(*) as nb FROM documents WHERE id_tree='.$this->tree->id_tree.' and id_document in (SELECT id_document FROM documents_profiles WHERE id_profile in ('.$_SESSION['user_profiles'].')) AND id_document in (SELECT id_document FROM documents_geozones WHERE id_geozone in ('.$_SESSION['user_geozones'].'))';
            $res = $this->bdd->run($sql);
            if((int)$res[0]['nb']==0)
            {
                //echo $sql;
                header('location:'.$this->lurl, true,301);
                die;
            }
        }
        if($_SESSION['auth']!='ok' && ($this->tree->id_template==6))
        {
            $_SESSION['postlogin'] = $_SERVER['REQUEST_URI'];
            header('location:'.$this->lurl, true,301);
            die;
        }
        if($_SERVER['REMOTE_ADDR']=='109.7.252.175')
        {
            echo 'timing:'.mktime();
        }

    }

    function _warmup()
    {
        $clts = new o\data('clients');
        foreach($clts as $c)
        {
                echo 'processing client'.$c->id_client;

                $this->cgc = (new o\data('clients_geozones'))->addWhere('id_client='.$c->id_client);
                $this->cpc = (new o\data('clients_profiles'))->addWhere('id_client='.$c->id_client);

                foreach($this->cgc as $g)
                    $z[] = $g->id_geozone;

                foreach($this->cpc as $p)
                    $pp[] = $p->id_profile;

                $_SESSION['user_geozones'] = implode(',',$z);
                $_SESSION['user_profiles'] = implode(',',$pp);
            $_SESSION['client']['id_client'] = $c->id_client;

            $nav_select = (new o\tree())->getArboSiteWithStatus($this->language, "1");
        }
        die;
    }

    function _dlAlert()
    {
        $sql = 'SELECT id_client,count(*) as total FROM downloads where added like "'.date('Y-m-d',strtotime('yesterday')).'%" group by id_client';
        $res = $this->bdd->run($sql);
        $nb = $this->clSettings->getParam('limite-alerte-download', 'globals');
        foreach($res as $line)
        {
            if($line['total']>$nb)
            {
                $client = new o\clients($line['id_client']);

                $msg = $client->prenom.' '.$client->nom. ' ('.$client->email.') a téléchargé '.$line['total']." documents hier\r\n\r\n";
                mail('b.jaugey@equinoa.com','Alerte telechargements NUXE Partners',utf8_decode($msg));
            }
        }

        $sql = 'SELECT id_client,count(*) as total FROM downloads where added >= date_add(now(),interval -1 month) group by id_client';
        $res = $this->bdd->run($sql);
        $nb = $this->clSettings->getParam('limite-alerte-download-mois', 'globals');
        foreach($res as $line)
        {
            if($line['total']>$nb)
            {
                $client = new o\clients($line['id_client']);

                $msg = $client->prenom.' '.$client->nom. ' ('.$client->email.') a téléchargé '.$line['total']." documents sur le dernier mois glissant\r\n\r\n";
                mail('b.jaugey@equinoa.com','Alerte telechargements NUXE Partners',utf8_decode($msg));
            }
        }

        die;
    }

    function _share()
    {
        if($this->params[1]!=md5('npartners'.$this->params[0].$this->params[2]))
            die;
        if($this->params[2]<mktime())
        {
            echo 'This download has expired';
            die;
        }
        $doc = new o\documents($this->params[0]);
        header('location:https://dlpartners.equinoa.net/'.$doc->file);die;
    }

    function _download()
    {
        //error_reporting(999);
        //ini_set('display_errors', TRUE);

        ini_set('max_execution_time',30000);
        ini_set('memory_limit','4096M');

        $this->clDocuments = new clDocuments();

        if(!is_numeric($this->params[0])){
            //$this->documents = new o\documents( array('file'=> str_replace('%20', ' ',$this->params[0])));
            //header('location:'.$this->lurl.'/'.$this->documents->file);die;
        } else {
            if(!(new o\clients())->checkAccess())
                die('not logged in');

            $this->documentZones = array();
            $this->documentProfiles = array();
            $this->clientZones = array();
            $this->clientProfiles = array();

            $this->documents = new o\documents( array('id_document'=>$this->params[0]));

            $this->cg = (new o\data('documents_geozones'))->addWhere('id_document='.$this->params[0]);
            $this->cp = (new o\data('documents_profiles'))->addWhere('id_document='.$this->params[0]);

            foreach($this->cg as $g)
                $this->documentZones[] = $g->id_geozone;

            foreach($this->cp as $p)
                $this->documentProfiles[] = $p->id_profile;

            $this->cgc = (new o\data('clients_geozones'))->addWhere('id_client='.$_SESSION['client']['id_client']);
            $this->cpc = (new o\data('clients_profiles'))->addWhere('id_client='.$_SESSION['client']['id_client']);

            $this->clientZones = [];
            $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
            foreach($zones as $z){
                $this->clientZones[] = $z->id_geozone;
            }

            $this->clientProfiles = [];
            $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
            foreach($profiles as $p){
                $this->clientProfiles[] = $p->id_profile;
            }

            //if(count(array_intersect($this->clientZones,$this->documentZones))>0 && count(array_intersect($this->clientProfiles,$this->documentProfiles))>0)
            if($this->clDocuments->authorized($this->documents->id_document))
            {
                $path = $this->path.'protected/documents/';

                // On va regarder si il y a plusieurs langues
                $lLangues = [];
                if(!empty($this->documents->file_en) && file_exists($path.$this->documents->file_en))
                    $lLangues[] = "en";
                if(!empty($this->documents->file_fr) && file_exists($path.$this->documents->file_fr))
                    $lLangues[] = "fr";
                if(!empty($this->documents->file_es) && file_exists($path.$this->documents->file_es))
                    $lLangues[] = "es";

                if(count($lLangues)==1) $this->params[1] = $lLangues[0];

                if($this->params[1] == "all"){
                    require $this->path.'vendor/autoload.php';
                    $filename = $path.'archives/'.$this->documents->id_document.'.zip';

                    if(!file_exists($filename)){
                        $zip = new ZipArchiveFile();
                        $zip->setZipFile($filename);
                        $zip->setComment("\nCreated on " . date('l jS \of F Y h:i:s A'));
                        if(!empty($this->documents->file_en) && file_exists($path.$this->documents->file_en))
                            $zip->addFile(file_get_contents($path.$this->documents->file_en), $this->documents->file_en);
                        if(!empty($this->documents->file_fr) && file_exists($path.$this->documents->file_fr))
                            $zip->addFile(file_get_contents($path.$this->documents->file_fr), $this->documents->file_fr);
                        if(!empty($this->documents->file_es) && file_exists($path.$this->documents->file_es))
                            $zip->addFile(file_get_contents($path.$this->documents->file_es), $this->documents->file_es);

                        $zip->finalize(); // as we are not using getZipData or getZipFile, we need to call finalize ourselves.
                    }

                    $size = filesize($filename);

                    $downloads= new o\downloads();
                    $downloads->id_document = $this->documents->id_document;
                    $downloads->id_client = $_SESSION['client']['id_client'];
                    $downloads->file = generateSlug($this->documents->label).'.zip';
                    $downloads->insert();

                    header('Content-type: application/zip');
                    header("Content-length: $size");
                    header('Content-Disposition: attachment; filename="uShare-'.generateSlug($this->documents->label).'.zip');
                    header('Expires: 0');
                    readfile($filename);
                    die;

                }elseif(in_array($this->params[1],['en','fr','es'])){
                    $champFile = 'file_'.$this->params[1];
                    $champName = 'name_'.$this->params[1];

                    if(!empty($this->documents->$champFile)){
                        $file=$this->path."protected/documents/".$this->documents->$champFile;

                        if (file_exists($file)) {
                            $downloads= new o\downloads();
                            $downloads->id_document = $this->documents->id_document;
                            $downloads->id_client = $_SESSION['client']['id_client'];
                            $downloads->file = $this->documents->$champFile;
                            $downloads->insert();

                            header('Content-disposition: attachment; filename="'.$this->documents->$champName.'"');
                            header("Content-Type: application/force-download");
                            header("Content-Transfer-Encoding: binary");
                            header("Content-Length: ".filesize($file));
                            header("Pragma: no-cache");
                            header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
                            header("Expires: 0");
                            $fp = fopen($file, 'rb');
                            fpassthru($fp);
                            //readfile($file);
                            die;
                        }
                    }else
                        die('No file');

                }else
                    die('No access');
                die;
            }
            else
                die('No access');
        }
    }

    function _download_cart()
    {
        ini_set('max_execution_time',30000);
        ini_set('memory_limit','4096M');

        $this->clDocuments = new clDocuments();

        if(strlen($this->params[0])!=13){ // l'ID du cart
            //$this->documents = new o\documents( array('file'=> str_replace('%20', ' ',$this->params[0])));
            //header('location:'.$this->lurl.'/'.$this->documents->file);die;
        } else {
            if(!(new o\clients())->checkAccess())
                die('not logged in');

            $this->cart = new o\carts( array('hash'=>$this->params[0]));

            // On vérifie si c'est le propriétaire qui veut le télécharger

            // Sinon on vérifie qu'il a les droits pour télécharger ce panier

            $this->documentZones = array();
            $this->documentProfiles = array();
            $this->clientZones = array();
            $this->clientProfiles = array();

            $this->cg = (new o\data('carts_geozones'))->addWhere('id_cart='.$this->cart->id_cart);
            $this->cp = (new o\data('carts_profiles'))->addWhere('id_cart='.$this->cart->id_cart);

            foreach($this->cg as $g)
                $this->documentZones[] = $g->id_geozone;

            foreach($this->cp as $p)
                $this->documentProfiles[] = $p->id_profile;

            $this->cgc = (new o\data('clients_geozones'))->addWhere('id_client='.$_SESSION['client']['id_client']);
            $this->cpc = (new o\data('clients_profiles'))->addWhere('id_client='.$_SESSION['client']['id_client']);

            $this->clientZones = [];
            $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
            foreach($zones as $z){
                $this->clientZones[] = $z->id_geozone;
            }

            $this->clientProfiles = [];
            $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
            foreach($profiles as $p){
                $this->clientProfiles[] = $p->id_profile;
            }

            //if(count(array_intersect($this->clientZones,$this->documentZones))>0 && count(array_intersect($this->clientProfiles,$this->documentProfiles))>0)
            if($this->cart->authorized())
            {
                require $this->path.'vendor/autoload.php';
                $path = $this->path.'protected/documents/';
                $filename = $path.'archives/'.$this->cart->hash.'.zip';

                if(!file_exists($filename)){
                    $this->documents = new data('carts_documents',['id_cart'=>(int)$this->cart->id_cart]);

                    if(count($this->documents)>0){
                        $zip = new ZipArchiveFile();
                        $zip->setZipFile($filename);
                        $zip->setComment("\nCreated on " . date('l jS \of F Y h:i:s A'));
                        foreach($this->documents as $d){
                            $this->doc = new o\documents( array('id_document'=>$d->id_document));
                            if(!empty($this->doc->file_en) && file_exists($path.$this->doc->file_en))
                                $zip->addFile(file_get_contents($path.$this->doc->file_en), $this->doc->label.'/'.$this->doc->file_en);
                            if(!empty($this->doc->file_fr) && file_exists($path.$this->doc->file_fr))
                                $zip->addFile(file_get_contents($path.$this->doc->file_fr), $this->doc->label.'/'.$this->doc->file_fr);
                            if(!empty($this->doc->file_es) && file_exists($path.$this->doc->file_es))
                                $zip->addFile(file_get_contents($path.$this->doc->file_es), $this->doc->label.'/'.$this->doc->file_es);

                            $downloads= new o\downloads();
                            $downloads->id_document = $this->doc->id_document;
                            $downloads->id_client = $_SESSION['client']['id_client'];
                            $downloads->file = generateSlug($this->doc->label).'.zip';
                            $downloads->insert();
                        }
                        $zip->finalize(); // as we are not using getZipData or getZipFile, we need to call finalize ourselves.
                    }
                }else{
                    $this->documents = new data('carts_documents',['id_cart'=>(int)$this->cart->id_cart]);
                    foreach($this->documents as $d){
                        $downloads= new o\downloads();
                        $downloads->id_document = $this->doc->id_document;
                        $downloads->id_client = $_SESSION['client']['id_client'];
                        $downloads->file = generateSlug($this->doc->label).'.zip';
                        $downloads->insert();
                    }
                }

                $size = filesize($filename);

                header('Content-type: application/zip');
                header("Content-length: $size");
                header('Content-Disposition: attachment; filename="uShare-cart-'.$this->cart->hash.'.zip');
                header('Expires: 0');
                readfile($filename);
                die;
            }
            else
                die('No access');
        }
    }

    public function _addDownload()
    {
        $downloads= new o\downloads();
        $downloads->id_document = $_POST["id_document"];
        $downloads->id_client = $_SESSION['client']['id_client'];
        $downloads->file = $_POST["file"];
        $downloads->insert();

        die;
    }

    function _view()
    {
        ini_set('max_execution_time',3000);
        ini_set('memory_limit','2048M');
        if($this->params[1]!=sha1($this->params[0].'kldjskdf'.$this->params[2]))
            die;
        $this->documentZones = array();
        $this->documentProfiles = array();
        $this->clientZones = array();
        $this->clientProfiles = array();

        $this->documents = new o\documents( array('id_document'=>$this->params[0]));
        $this->cg = (new o\data('documents_geozones'))->addWhere('id_document='.$this->params[0]);
        $this->cp = (new o\data('documents_profiles'))->addWhere('id_document='.$this->params[0]);

        foreach($this->cg as $g)
            $this->documentZones[] = $g->id_geozone;

        foreach($this->cp as $p)
            $this->documentProfiles[] = $p->id_profile;


        $this->cgc = (new o\data('clients_geozones'))->addWhere('id_client='.$this->params[2]);
        $this->cpc = (new o\data('clients_profiles'))->addWhere('id_client='.$this->params[2]);

        foreach($this->cgc as $g)
            $this->clientZones[] = $g->id_geozone;

        foreach($this->cpc as $p)
            $this->clientProfiles[] = $p->id_profile;

        //if(count(array_intersect($this->clientZones,$this->documentZones))>0 && count(array_intersect($this->clientProfiles,$this->documentProfiles))>0)
        $this->clDocuments = new clDocuments();
        if($this->clDocuments->authorized($this->documents->id_document))
        {
            foreach(['en','fr','es'] as $ln){
                $champ = 'file_'.$ln;
                if(!empty($this->documents->$champ)){
                    $champFile = 'file_'.$ln;
                    $preview = 'preview_'.$ln;
                }
            }
            if(!empty($this->documents->$champFile)) {
                if ($this->params[3] == 'doc.pptx' || $this->params[3] == 'doc.ppt' || $this->params[3] == 'doc.eps' || $this->params[3] == 'doc.doc' || $this->params[3] == 'doc.docx')
                    $file = $this->path . "protected/documents/previews/" . $this->documents->$preview;
                else
                    $file = $this->path . "protected/documents/" . $this->documents->$champFile;
                if ($this->params[3] == 'doc.zip' || $this->params[3] == 'doc.rar')
                    $file = $this->path . "protected/documents/previews/" . $this->documents->$preview;

                if (is_dir($file)) {
                    $file .= '/PDF/' . $this->documents->$champFile . '.pdf';

                    header("Content-disposition: inline;");
                    header("Content-Transfer-Encoding: binary");
                    header("Content-Length: " . filesize($file));
                    header("Pragma: no-cache");
                    header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
                    header("Expires: 0");
                    readfile($file);
                    die;
                }
                if (file_exists($file)) {
                    if ($this->params[3] == 'video.mov') {
                        $file = $this->path . "protected/documents/previews/" . $this->documents->$champFile . '.mp4';
                        header('Content-type: video/mp4');
                    } elseif ($this->params[3] == 'video.mp4') {
                        if ($this->documents->extension == 'mov') {
                            $file = $this->path . "protected/documents/previews/" . $this->documents->$preview;
                            //echo $file;die;
                            header("Accept-Ranges: 0-" . filesize($file));
                        }
                        header('Content-type: video/mp4');
                    } elseif ($this->params[3] == 'doc.doc')
                        header('Content-type: application/msword');


                    header("Content-disposition: inline;");
                    header("Content-Transfer-Encoding: binary");
                    header("Content-Length: " . filesize($file));
                    header("Pragma: no-cache");
                    header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
                    header("Expires: 0");
                    readfile($file);
                    die;
                }
            }
        }
        else
            die('No access');
    }

    function _thumb()
    {
        if(!(new o\clients())->checkAccess())
            die('not logged in');

        //if(!isset($_SESSION['client']['id_client']))
        //    die('not logged in');

        $search = ['+'];
        $transforme = ['||plus||'];
        $this->params[0] = str_replace($search,$transforme,$this->params[0]);
        $img = str_replace($transforme,$search,urldecode($this->params[0]));

        $this->documents = NULL;
        foreach(['en','fr','es'] as $ln){
            if(empty($this->documents)){
                $thumbnail = 'thumbnail_'.$ln;
                $this->documents = new o\documents( array($thumbnail => $img));
                if(empty($this->documents)) {
                    $this->documents = new o\documents(array($thumbnail => str_replace('-bigthumb','-thumb',$img)));
                    $this->params[1] = 'big';
                }
            }
        }

        if(!empty($this->documents))
        {
            if($this->params[1]=='big')
                $file=$this->path."protected/documents/thumbs/".str_replace('-thumb','-bigthumb',$this->documents->$thumbnail);
            else
                $file=$this->path."protected/documents/thumbs/".$this->documents->$thumbnail;

            if (file_exists($file)) {
                if($this->params[1]=='tb.png')
                    header( "Content-Type: image/png" );
                else
                    header( "Content-Type: image/jpg" );
                header("Content-Length: ".filesize($file));
                readfile($file);
                die;
            }
            else
            {
                $file = $this->path."protected/documents/thumbs/default.png";
                header( "Content-Type: image/png" );

                header("Content-Length: ".filesize($file));
                readfile($file);
                die;
            }
        }
        else
            die('No access');
    }

    function _digest()
    {
        ini_set('display_errors','on');
        error_reporting(9999);
        $clts = new o\data('clients');
        $clts->addWhere('email not like "DISABLED%"');

        foreach($clts as $c)
        {
            $docs = "";
            unset($this->documents);
            $z = array();
            $zR = array();
            $pp = array();
            $this->cgc = (new o\data('clients_geozones'))->addWhere('id_client='.$c->id_client);
            $this->cpc = (new o\data('clients_profiles'))->addWhere('id_client='.$c->id_client);

            foreach ($this->cgc as $g) {
                if ($g->id_geozone == 18 || $g->id_geozone == 26) {
                    $zR[] = $g->id_geozone;
                } else {
                    $z[] = $g->id_geozone;
                }
            }
            foreach ($this->cpc as $p)
                $pp[] = $p->id_profile;

            $leszonesResultime = implode(',', $zR);
            $leszones = implode(',', $z);
            $lesprofils = implode(',', $pp);

            $liste_ids_docs_resultime = array();

            if ((count($z) > 0 || count($zR) > 0) && count($pp) > 0) {
                $i = 0;
                if( count($zR) > 0){
                    // récupération des documents RESULTIME ajoutés dans les 7 derniers jours pour les géozones et profile du client
                    $whereRsultime = 'status=1 AND added>date_add(now(),interval -7 day) AND id_document in (SELECT id_document FROM documents_geozones where id_geozone in (' . $leszonesResultime . ')) AND id_document IN (SELECT id_document FROM documents_profiles where id_profile in (' . $lesprofils . '))';

                    $this->documentsResultime = new o\data('documents');
                    $this->documentsResultime->addWhere($whereRsultime);
                    $this->documentsResultime->order('new', 'DESC', '', 'label', 'ASC', '');

                    // si on a des documents Resultime :
                    if (count($this->documentsResultime) > 0) {
                        $docs .= '<br><h3><strong>Resultime Documents</strong></h3><br><table width="100%"><tr><td><strong>Document</strong></td><td width="100"><strong>Type</strong></td></tr>';

                        foreach ($this->documentsResultime as $docRes) {
                            $docs .= '<tr><td><a href="'.$this->lurl.'/download/' . $docRes->id_document . '" style="color:#034638">' . $docRes->label . '</a></td><td>' . $docRes->doctype->name . '</td></tr>';

                            $liste_ids_docs_resultime[] = $docRes->id_document;
                            $i++;
                        }
                        $docs .= '</table><br>';
                    }
                    $docs_resultime = implode(',', $liste_ids_docs_resultime);
                }


                if( count($z) > 0){
                    // récupération des DOCUMENTS ajoutés dansles 7 derniers jours pour les géozones et profile du client

                    $and = "";
                    if(count($zR) > 0 && !empty($docs_resultime)){
                        $and=' AND id_document NOT IN ('.$docs_resultime.')';
                    }

                    $whereNuxe = 'status=1 AND added>date_add(now(),interval -7 day) AND id_document in (SELECT id_document FROM documents_geozones where id_geozone in (' . $leszones . ')) AND id_document IN (SELECT id_document FROM documents_profiles where id_profile in (' . $lesprofils . ')) '.$and;

                    $this->documentsNuxe = new o\data('documents');
                    $this->documentsNuxe->addWhere($whereNuxe);
                    $this->documentsNuxe->order('new', 'DESC', '', 'label', 'ASC', '');

                    if (count($this->documentsNuxe) > 0) {
                        $docs .= '<br><h3><strong>Nuxe Documents</strong></h3><br><table width="100%"><tr><td><strong>Document</strong></td><td width="100"><strong>Type</strong></td></tr>';

                        foreach ($this->documentsNuxe as $docNuxe) {
                            $docs .= '<tr><td><a href="'.$this->lurl.'/download/' . $docNuxe->id_document . '" style="color:#034638">' . $docNuxe->label . '</a></td><td>' . $docNuxe->doctype->name . '</td></tr>';
                            $i++;
                        }

                        $docs .= '</table>';
                    }

                }

                if ($i > 0) {
                    $data['docs'] = $docs;
                    $this->envoyerMail('mail-hebdo', 'en', $c->email, $data);
                    $this->envoyerMail('mail-hebdo', 'en', 'b.jaugey@equinoa.com', $data);
                    if ($i == 1) {
                        //$this->envoyerMail('mail-hebdo', 'en', 'b.jaugey@equinoa.com', $data);
                    }
                }
            }
        }
        die;
    }

    function _docs()
    {
        $this->autoFireHeader = false;
        $this->autoFireHead = false;
        $this->autoFireFooter = false;
        $this->clDocuments = new clDocuments();
        $enfants = unserialize($_SESSION['nav']);

        if($_POST['tree']==0)
            $where = 'status=1 AND id_document in (select id_document FROM documents_tree WHERE id_tree in ('.($_POST['new']==1?'(SELECT id_tree FROM tree where id_parent=(SELECT id_parent FROM tree where id_parent='.$_POST['tree'].' and id_langue="'.$this->language.'"))':'SELECT id_tree FROM tree').')) AND id_document in (SELECT id_document FROM documents_geozones where id_geozone in ('.$_SESSION['user_geozones'].')) AND id_document IN (SELECT id_document FROM documents_profiles where id_profile in ('.$_SESSION['user_profiles'].'))';
        elseif($_POST['tree']==158)
            $where = 'status=1 AND id_client='.$_SESSION['client']['id_client'];
        elseif($_POST['tree']==159)
            $where = 'status=1 AND id_client>0';
        else
        {
            if($_POST['new']==1)
                $this->tree = new o\tree(array('id_tree' => $_POST['tree'], 'id_langue' => $this->language));
            $where = ($_POST['new']==1?'new=1 AND ':'').'status=1 AND id_document in (select id_document FROM documents_tree WHERE id_tree in ('.implode(',',$enfants[($_POST['new']==1?$this->tree->id_parent:$_POST['tree'])]).')) AND id_document in (SELECT id_document FROM documents_geozones where id_geozone in ('.$_SESSION['user_geozones'].')) AND id_document IN (SELECT id_document FROM documents_profiles where id_profile in ('.$_SESSION['user_profiles'].'))';
        }
        //echo $where;
        //print_r($enfants);
        if(isset($_POST['search']) && $_POST['search']!='')
        {
            $where .= ' AND (label like "%'.$_POST['search'].'%" OR description like "%'.$_POST['search'].'%" OR tags like "%'.$_POST['search'].'%")';
        }
        if(isset($_POST['filters']) && $_POST['filters']!='')
        {
            $where .= ' AND id_doctype in ('.$_POST['filters'].') ';
        }
        if(isset($_POST['new']) && $_POST['new']==1)
        {
            $where .= ' AND new=1';
        }
        //echo $where;
        if($_POST['tree']==159)
            $this->documents = (new o\data('documents'))->addWhere($where)->order('id_client','ASC','','new','DESC','','label','ASC','');
        else
            $this->documents = (new o\data('documents'))->addWhere($where)->order('new','DESC','','added','DESC','');



    }

    function _details()
    {
        $this->autoFireHeader = false;
        $this->autoFireHead = false;
        $this->autoFireFooter = false;
        $this->isAdmin = false;
        $this->clDocuments = new clDocuments();
        $this->documents = new o\documents( array('id_document'=>$this->params[0]));
        $this->likes = new o\likes(array('id_document'=>$this->params[0],'id_client'=>$_SESSION['client']['id_client']));
        $this->users = new o\users( array('email'=>$_SESSION['client']['email'],'status'=>1));
        if($this->users->exist())
        {
            $this->isAdmin = true;
        }
    }

    function _like()
    {
        $this->autoFireHeader = false;
        $this->autoFireHead = false;
        $this->autoFireFooter = false;
        $this->likes = new o\likes(array('id_document'=>$this->params[0],'id_client'=>$_SESSION['client']['id_client']));
        if($this->likes->exist())
        {
            $this->likes->delete();
        }
        else
        {
            $this->likes->id_document = $this->params[0];
            $this->likes->id_client = $_SESSION['client']['id_client'];
            $this->likes->save();
        }

    }

    function _logout()
    {
        session_destroy();
        header('location:/');die;
    }

    /**
     * Permet de placer la session de l'administrateur
     * connecté au back-office sur le front-office.
     *
     * @function _logAdminUser
     */
    public function _logAdminUser()
    {
        $this->autoFireNothing = true;
        $users = new o\users(array('email' => $_POST['email']));

        if ($users->exist()) {
            if ($_POST['hash'] == $users->hash) {
                $_SESSION['user'] = $users->getArray();
            } else {
                unset($_SESSION['user']);
            }
        } else {
            unset($_SESSION['user']);
        }
    }

    function _upload()
    {
        $this->clDocuments = new clDocuments();
        $this->clDocuments->handleUpload($this->path);
        die;
    }

    function _cron()
    {
        error_reporting(99999);
        ini_set('display_errors','on');
        $sql = 'UPDATE documents SET new=0 where new=1 and newed<=date_add(now(), interval -2 month)';
        $this->bdd->run($sql);
        $sql = 'UPDATE documents SET status=0 where expires <= now() and expires<>"0000-00-00 00:00:00"';
        $this->bdd->run($sql);
        $sql = 'UPDATE documents SET status=1 where status=0 AND planned<=now() and planned<>"0000-00-00 00:00:00" and (expires="0000-00-00 00:00:00" or expires>=now())';
        $this->bdd->run($sql);
        die;
    }

    function _index()
    {
        require $this->path.'vendor/algolia/algoliasearch-client-php/autoload.php';
        $appId = "CBXZ2YNNVG";
        $apiKey = "40ba07b382d37d829b1fa37f787dff0d";
        $client = \Algolia\AlgoliaSearch\SearchClient::create($appId, $apiKey);

        $index = $client->initIndex('Documents');

        $docs = new o\data('documents');

        // 1ere remonté ou pour tout cleaner
        /*foreach($docs as $doc) {
            echo $doc->id_document." - ";
            $index->deleteObjects([$doc->id_document]);
        }
        die;*/
        $docs->addWhere('status=0');
        foreach($docs as $doc) {
            $index->deleteObjects([$doc->id_document]);
        }

        $i = 0;
        $params = array();
        $docs->addWhere('status=1 and indexed=0');
        $ext = array('doc','docx','pdf','ppt','pptx');

        foreach($docs as $doc)
        {
            $i++;
            $activeLang = [];
            foreach(['en','fr','es'] as $ln){ $aTester = 'file_'.$ln;
                if(empty($activeLang) && !empty($doc->$aTester)){
                    $activeLang = $ln;
                    $file = 'file_'.$ln;
                    $name = 'name_'.$ln;
                    $extension = 'extension_'.$ln;
                    $thumbnail = 'thumbnail_'.$ln;
                    $size = 'size_'.$ln;
                }
            }

            // Récupération des infos
            $dt = new o\data('documents_tree');
            $dt->addWhere('id_document='.$doc->id_document);
            $tree = new o\tree(array('id_langue'=>'en','id_tree'=>$dt[0]->id_tree));

            $dt = new o\doctypes($doc->id_doctype);

            $prod = [
                'objectID' => $doc->id_document,
                'name' => $doc->$name,
                'label'  => $doc->label,
                'file'  => $doc->$file,
                'description'  => $doc->description,
                'tags'  => $doc->tags,
                'url'  => '/'.$tree->slug.'/'.$doc->id_document,
                'type'  => $dt->name,
                'textcontent'  =>  $dt->textcontent,
                'new'  => (int)$doc->new,
                'added'  => ($doc->added!="0000-00-00 00:00:00"?(int)strtotime($doc->added):0),
                'expires'  => ($doc->expires!="0000-00-00 00:00:00"?(int)strtotime($doc->expires):0),
                'status'  => (int)$doc->status,
                'thumbnail'  => ($doc->$thumbnail==''?'default.png':$doc->$thumbnail)
            ];

            $index->saveObject($prod);

            $doc->indexed = 1;
            $doc->save();

            if($i>1000)
                break;
        }
        $message = $i.' fichiers indexés';
        //mail('b.jaugey@equinoa.com','CRON Uriage MZ Root>Index',$message);
        echo $message;
        die;
    }



    function _index_tree()
    {
        require $this->path.'vendor/algolia/algoliasearch-client-php/autoload.php';
        $appId = "CBXZ2YNNVG";
        $apiKey = "40ba07b382d37d829b1fa37f787dff0d";
        $client = \Algolia\AlgoliaSearch\SearchClient::create($appId, $apiKey);

        $index = $client->initIndex('Tree');

        $tree = new o\data('tree');

        // 1ere remonté ou pour tout cleaner
        /*foreach($tree as $t) {
            echo $t->id_tree." - ";
            $index->deleteObjects([$t->id_tree]);
        }
        die;*/
        $tree->addWhere('indexation=0');
        foreach($tree as $t) {
            $index->deleteObjects([$t->id_tree]);
        }

        $i = 0;
        //$tree->addWhere('status=1 and indexation=1 and updated > "'.date('Y-m-d 00:00:00',strtotime('-1 days')).'"');
        $tree->addWhere('status=1 and indexation=1');

        foreach($tree as $t)
        {
            $treeParent = new o\tree(array('id_langue'=>'en','id_tree'=>$t->id_parent));
            $treeGrdParent = new o\tree(array('id_langue'=>'en','id_tree'=>$treeParent->id_parent));
            $parents = "";
            if(!empty($treeGrdParent->id_tree))
                $parents .= (!empty($treeGrdParent->menu_title)?$treeGrdParent->menu_title:$treeGrdParent->title)." > ";
            if(!empty($treeParent->id_tree))
                $parents .= (!empty($treeParent->menu_title)?$treeParent->menu_title:$treeParent->title);

            $i++;
            // Récupération des infos
            $record = [
                'objectID' => $t->id_tree,
                'title' => $t->title,
                'menu_title'  => $t->menu_title,
                'parents'  =>  $parents,
                'meta_title'  => $t->meta_title,
                'meta_description'  => $t->meta_description,
                'url'  => '/'.$t->slug,
                'status'  => (int)$t->status
            ];

            $index->saveObject($record);

            if($i>1000)
                break;
        }
        $message = $i.' rubriques indexées';
        //mail('b.jaugey@equinoa.com','CRON Uriage MZ Root>Index',$message);
        echo $message;
        die;
    }


    function _lancement(){
        //$this->clients = new data('clients');

        $c = new o\clients(array('id_client'=>'2'));
        $vars = [
            'client_prenom_nom' => $c->prenom." ".$c->nom,
            'client_email' => $c->email,
            'surl' => $this->surl,
            'url_video' => $this->surl.'/how_to_use',
            'lien' => $this->url,
            'meta_tile' => "New plateform",
            'x-splio-ref' => $this->Config['env'] . '_new-plateform_' . date('Y-m-d')
        ];

        echo $c->email;
        $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);

        /*$return = $this->clEmail->sendMail("new-plateform", 'en', $c->email,  $vars);
        echo " new-plateform ";
        var_dump($return);*/
        die;

        foreach($this->clients as $c){
            echo "<br/>";
            if(!empty($c->id_client) && 1==2){
            //if(in_array($c->id_client,[148,154,159,171,192,237,278])){
                echo $c->id_client.' ok ';

                /*$vars = [
                    'client_prenom_nom' => $c->prenom." ".$c->nom,
                    'client_email' => $c->email,
                    'surl' => $this->surl,
                    'url_video' => $this->surl.'/how_to_use',
                    'lien' => $this->url,
                    'meta_tile' => "Forgotten password",
                    'x-splio-ref' => $this->Config['env'] . '_mdp-oublie_' . date('Y-m-d')
                ];

                $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
                $return = $this->clEmail->sendMail("mdp-oublie", 'en', $c->email,  $vars);
                echo " mdp-oublie ";
                var_dump($return);

                $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
                $return = $this->clEmail->sendMail("mail-ouverture-de-compte", 'en', $c->email,  $vars);
                echo " mail-ouverture-de-compte ";
                var_dump($return);

                $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
                $return = $this->clEmail->sendMail("new-plateform", 'en', $c->email,  $vars);
                echo " new-plateform ";
                var_dump($return);

                $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
                $return = $this->clEmail->sendMail("shared-cart", 'en', $c->email,  $vars);
                echo " shared-cart ";
                var_dump($return); */
                $return = false;
                if(!empty($c->password_old)){
                    $vars = [
                        'client_prenom_nom' => $c->prenom." ".$c->nom,
                        'client_email' => $c->email,
                        'surl' => $this->surl,
                        'url_video' => $this->surl.'/how_to_use',
                        'lien' => $this->url,
                        'meta_tile' => "New plateform",
                        'x-splio-ref' => $this->Config['env'] . '_new-plateform_' . date('Y-m-d')
                    ];

                    $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
                    //$return = $this->clEmail->sendMail("new-plateform", 'en', $c->email,  $vars);
                    echo " new-plateform ";
                    var_dump($return);
                }else{
                    $vars = [
                        'client_prenom_nom' => $c->prenom." ".$c->nom,
                        'client_email' => $c->email,
                        'surl' => $this->surl,
                        'url_video' => $this->surl.'/how_to_use',
                        'lien' => $this->url.'/newpassword/'.$c->hash,
                        'meta_tile' => "New platform",
                        'x-splio-ref' => $this->Config['env'] . '_mail-ouverture-de-compte_' . date('Y-m-d')
                    ];

                    $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
                    //$return = $this->clEmail->sendMail("mail-ouverture-de-compte", 'en', $c->email,  $vars);
                    echo " mail-ouverture-de-compte ";
                    var_dump($return);

                }
            }

            echo " ".$c->email;
        }

        die;
    }

    function _import_old_users(){

        error_reporting(999);
        ini_set('display_errors', TRUE);
        //die;
        /*
        $csv = file_get_contents($this->path.'protected/imports/clients/import_client.csv');
        $lines = explode("\n",$csv);
        $i = 0;
        $tUsers = [];
        foreach($lines as $line) {$i++;
            if($i>1) {
                $line = utf8_encode($line);
                $client = explode(';', $line);


                if (!empty($client[0])) {
                    $email = strtolower($client[5]);

                    // Vérification des doublons
                    //                if(!isset($tUsers[$email]))
                    //                    $tUsers[$email] = 0;
                    //                $tUsers[$email]++;
                    //                $client = new o\clients(['email'=>$email]);


                    $clients_old = new o\clients_old(['email' => strtolower($client[5])]);
                    $this->clients = new o\clients();
                    $this->clients->nom = $client[4];
                    $this->clients->prenom = $client[3];
                    $this->clients->email = $client[5];
                    if (!empty($clients_old->encrypted_password)) {
                        $this->clients->password_old = $clients_old->encrypted_password;
                    }
                    $this->clients->status = ($client[2] == 'Interne' ? 2 : 1);
                    $this->clients->slug = $this->clFonctions->controlSlug('clients', generateSlug($this->clients->nom . ' ' . $this->clients->prenom));
                    $this->clients->save();

                    $lProfils = ['DISTRIBUTEUR' => 8, 'Filiale-Like' => 10, 'FILIALE' => 10];

                    $geozones = new o\geozones(['name' => $client[0]]);
                    if (empty($geozones->id_geozone) && !empty($client[0])) {
                        $geozones = new o\geozones();
                        $geozones->name = $client[0];
                        $geozones->save();
                    }
                    var_dump($client[5]);
                    var_dump($geozones->name);
                    echo "<br/>";
                    if (!empty($this->clients->id_client)) {
                        $id_client = $this->clients->id_client;
                        if (isset($lProfils[$client[2]])) {
                            if (!empty($id_client)) {
                                $clients_profiles = new o\clients_profiles();
                                $clients_profiles->id_client = $id_client;
                                $clients_profiles->id_profile = $lProfils[$client[2]];
                                $clients_profiles->insert();
                            }
                        } else { // c'est un admin on lui donne tous les droits
                            if (!empty($id_client)) {
                                $this->profiles = new o\data('profiles');
                                foreach ($this->profiles as $profile) {
                                    $clients_profiles = new o\clients_profiles();
                                    $clients_profiles->id_client = $id_client;
                                    $clients_profiles->id_profile = $profile->id_profile;
                                    $clients_profiles->insert();
                                }
                            }
                        }

                        $clients_geozones = new o\clients_geozones();
                        $clients_geozones->id_client = $id_client;
                        $clients_geozones->id_geozone = $geozones->id_geozone;
                        $clients_geozones->insert();
                    }
                }
            }
        }
        die; /* */

        /*$this->clients_old = new data('clients_old');

        if(count($this->clients_old)>0 && 1==1){
            $i = $i2 = $i3 = $j = 0;
            foreach($this->clients_old as $u){
                //if(in_array($u->email, $lUsers)) $i3++;
                //$type_profile = ['User::Distributeur'=>8,'User::PointDeVente'=>9,'User::ResponsableDeZone'=>10,'User::Agency'=>11];
                $type_profile = ['User::PointDeVente'=>9];
                if(isset($type_profile[$u->type])){
                    echo "<br/>";
                    echo $u->current_sign_in_at." PDV - ";
                    //if($u->current_sign_in_at < "2019-01-01 00:00:00"){$i++;
                    if($u->current_sign_in_at > "2018-07-01 00:00:00" || $u->confirmation_sent_at > "2019-01-01 00:00:00" ){$i++;
                        echo $i." - OK - ";

                        // Création des zones en fonction des country
                        $geozones = new o\geozones(['name'=>$u->country]);
                        echo $u->country;
                        if(empty($geozones->id_geozone) && !empty($u->country)){
                            $geozones = new o\geozones();
                            $geozones->name = $u->country;
                            $geozones->save();
                            echo " créer zone ";
                        }else{
                            echo " zone Ok ";
                        }

                        $this->clients = new o\clients();
                        $this->clients->nom = $u->lastname;
                        $this->clients->prenom = $u->firstname;
                        $this->clients->email = $u->email;
                        $this->clients->password_old = (!empty($u->encrypted_password)?$u->encrypted_password:'');
                        $this->clients->status = 1;
                        $this->clients->slug = $this->clFonctions->controlSlug('clients', generateSlug($this->clients->nom.' '.$this->clients->prenom));
                        $this->clients->save();


                        if(!empty($this->clients->id_client)){
                            $clients_profiles = new o\clients_profiles();
                            $clients_profiles->id_client = $this->clients->id_client;
                            $clients_profiles->id_profile = $type_profile[$u->type];
                            $clients_profiles->insert();

                            if(!empty($geozones->id_geozone)){
                                $clients_geozones = new o\clients_geozones();
                                $clients_geozones->id_client = $this->clients->id_client;
                                $clients_geozones->id_geozone = $geozones->id_geozone;
                                $clients_geozones->insert();
                            }
                        }

                    }else{ $j++;
                        echo $j." - Trop vieux - ";
                    }
                    echo $u->email;

                }else{ $i2++;
                    //echo $i2." pas ok - ".$u->type." ";
                }

                //echo $u->email;
            }
        }

        var_dump($i3); /* */


        // MAJ pour avoir le nombre de téléchargement par utilisateur et la date de dernière connexion


        die;
    }


    function rcopy($src, $dst) {
        if (file_exists($dst)) $this->rrmdir($dst);
        if (is_dir($src)) {
            mkdir($dst);
            $files = scandir($src);
            foreach ($files as $file)
                if ($file != "." && $file != "..") $this->rcopy("$src/$file", "$dst/$file");
        }
        else if (file_exists($src)) copy($src, $dst);
    }
    function rrmdir($dir) {
        if (is_dir($dir)) {
            $files = scandir($dir);
            foreach ($files as $file)
                if ($file != "." && $file != "..") $this->rrmdir("$dir/$file");
            rmdir($dir);
        }
        else if (file_exists($dir)) unlink($dir);
    }

}
