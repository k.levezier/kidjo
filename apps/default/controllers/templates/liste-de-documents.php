<?php
/******* Liste de documents *******/

$this->clDocuments = new clDocuments();

$this->lDocuments = [];
$this->lDocs = $this->clDocuments->listeDocumentZoneProfil($this->tree->id_tree);

foreach($this->lDocs as $ordre=>$g){
    $doc = new o\documents(array('id_document' => $g->id_document));
    $this->lDocuments[] = $doc;
}

$this->enfants = (new o\tree())->getArboSite($this->language, $this->tree->id_tree);
$this->lEnfantsDocuments = [];
foreach($this->enfants as $e){
    $this->lDocs = $this->clDocuments->listeDocumentZoneProfil($e['id_tree']);
    foreach($this->lDocs as $g){
        $doc = new o\documents(array('id_document' => $g->id_document));
        $this->lEnfantsDocuments[$e['id_tree']][] = $doc;
    }
}

if(!empty($this->params[1])){
    $this->docShow = new o\documents(array('id_document' => $this->params[1]));
}