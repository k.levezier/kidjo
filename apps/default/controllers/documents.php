<?php

use o\data;

class documentsController extends bootstrap
{
    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);

        if(!(new o\clients())->checkAccess())
        {
            header('location:'.$this->lurl);
        }

        $this->clDocuments = new clDocuments();
    }

    /**
     *
     * @function _default
     */
    public function _default()
    {
    }

    /**
     * @function _popup
     */
    public function _popup()
    {
        $this->autoFireHead = false;
        $this->autoFireHeader = false;
        $this->autoFireFooter = false;

        $this->doc = new o\documents( ['id_document'=>$this->params[0]]);
    }
}
