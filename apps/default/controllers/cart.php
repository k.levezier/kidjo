<?php

use o\data;

class cartController extends bootstrap
{
    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);

        if(!(new o\clients())->checkAccess())
        {
            header('location:'.$this->lurl);
        }

        $this->menu_actif = "cart";
        $this->clDocuments = new clDocuments();
    }

    /**
     *
     * @function _default
     */
    public function _default()
    {
        $this->menu_actif = "";
        $this->profiles = new o\data('profiles');
        $this->geozones = new o\data('geozones');
        $this->cart = new o\data('cart');

        if(!empty($_POST) && $_SESSION['client']['status'] == 2){
            $this->majSharePanier();
        }

        // Récupération des données pour la vue
        $cart = new o\carts(array('hash' => $_SESSION['cart']['hash']));

        $this->lDocuments = [];
        if(!empty($_SESSION['cart']['documents'])){
            foreach($_SESSION['cart']['documents'] as $d){
                $doc = new o\documents(array('id_document' => $d));
                if(!empty($doc->id_document))
                    $this->lDocuments[] = $doc;
            }
        }

        $carts_profils = new o\data('carts_profiles');
        $carts_geozones = new o\data('carts_geozones');
        $this->lProfiles = []; $this->lGeozones = [];
        $this->lProfilesLabel = []; $this->lGeozonesLabel = [];
        if($cart->shared==1){
            foreach($carts_profils->where("id_cart",$cart->id_cart) as $p){
                $profilesLabel = new o\profiles(['id_profile'=>$p['id_profile']]);
                $this->lProfiles[] = $p['id_profile'];
                $this->lProfilesLabel[$p['id_profile']] = $profilesLabel->name;
            }
            foreach($carts_geozones->where("id_cart",$cart->id_cart) as $z){
                $geozonesLabel = new o\geozones(['id_geozone'=>$z['id_geozone']]);
                $this->lGeozones[] = $z['id_geozone'];
                $this->lGeozonesLabel[$z['id_geozone']] = $geozonesLabel->name;
            }
        }

        $this->authorizationRemove = true;

        $this->cart = $cart;
    }



    /**
     *
     * @function _shared
     */
    public function _shared()
    {
        $carts = new o\carts();
        $this->lCarts = $carts->getSharedToMe();

        $this->carts = $carts;
    }

    /**
     *
     * @function _shared_detail
     */
    public function _shared_detail()
    {
        $cart = new o\carts(['hash'=>$this->params[0]]);
        $this->profiles = new o\data('profiles');
        $this->geozones = new o\data('geozones');

        if(empty($cart->id_cart) || $cart->shared!=1){
            header('location:'.$this->lurl.'/cart/shared');
            die;
        }

        if(!empty($_POST) && $_SESSION['client']['status'] == 2){
            $this->majSharePanier();
        }

        // Récupération des données pour la vue
        $docs = new o\data('carts_documents');
        $docs->where('id_cart',$cart->id_cart);
        $this->lDocuments = [];
        if(!empty($docs)){
            foreach($docs as $d){
                $doc = new o\documents(array('id_document' => $d['id_document']));
                if(!empty($doc->id_document))
                    $this->lDocuments[] = $doc;
            }
        }

        $carts_profils = new o\data('carts_profiles');
        $carts_geozones = new o\data('carts_geozones');
        $this->lProfiles = []; $this->lGeozones = [];
        $this->lProfilesLabel = []; $this->lGeozonesLabel = [];
        if($cart->shared==1){
            foreach($carts_profils->where("id_cart",$cart->id_cart) as $p){
                $profilesLabel = new o\profiles(['id_profile'=>$p['id_profile']]);
                $this->lProfiles[] = $p['id_profile'];
                $this->lProfilesLabel[$p['id_profile']] = $profilesLabel->name;
            }
            foreach($carts_geozones->where("id_cart",$cart->id_cart) as $z){
                $geozonesLabel = new o\geozones(['id_geozone'=>$z['id_geozone']]);
                $this->lGeozones[] = $z['id_geozone'];
                $this->lGeozonesLabel[$z['id_geozone']] = $geozonesLabel->name;
            }
        }
        $this->authorizationRemove = false;

        $this->cart = $cart;
    }

    public function _add_document()
    {
        // On regarde si il y a déjà un panier pour cette session sinon on le créé
        if(!isset($_SESSION['cart']) || !is_array($_SESSION['cart'])){
            unset($_SESSION['cart']);
            $_SESSION['cart'] = [];
        }

        if(!empty($_SESSION['cart']['hash'])) {
            $this->cart = new o\carts(['hash' => $_SESSION['cart']['hash']]);
        }

        if(empty($_SESSION['cart']['hash']) || empty($this->cart->id_cart)){
            $this->cart = new o\carts();
            $this->cart->id_client = $_SESSION['client']['id_client'];
            $this->cart->hash = uniqid();
            $this->cart->shared = 0;
            $this->cart->id_cart = $this->cart->save();

            $_SESSION['cart']['hash'] = $this->cart->hash;
        }

        if(!empty($this->cart->id_cart)){
            $this->cart_doc = new o\carts_documents();
            $this->cart_doc->id_cart =  $this->cart->id_cart;
            $this->cart_doc->id_document = $_POST['id_document'];
            $this->cart_doc->save();

            $data['id_cart'] = $this->cart_doc->id_cart;
            $data['id_document'] = $this->cart_doc->id_document;
            $data['return'] = 'success';
        }

        if(isset($_SESSION['cart']['documents']))
            unset($_SESSION['cart']['documents']);
        $lDocs = (new o\data('carts_documents'))->addWhere('id_cart='.$this->cart->id_cart);
        foreach($lDocs as $d){
            $_SESSION['cart']['documents'][] = $d->id_document;
        }
        $data['nbr_doc'] = count($_SESSION['cart']['documents']);

        $this->clDocuments = new clDocuments();
        $this->clDocuments->deleteCartArchive($this->cart->id_cart);

        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }

    public function _remove_document()
    {
        if(!empty($_SESSION['cart']['hash'])){
            $this->cart = new o\carts(['hash'=>$_SESSION['cart']['hash']]);
            $this->cart_doc = new o\carts_documents(['id_cart'=>$this->cart->id_cart,'id_document'=>$_POST['id_document']]);

            $data['id_cart'] = $this->cart_doc->id_cart;
            $data['id_document'] = $this->cart_doc->id_document;

            $this->cart_doc->delete();
            $data['return'] = 'success';
        }else{
            $data['return'] = 'error';
        }

        if(isset($_SESSION['cart']['documents']))
            unset($_SESSION['cart']['documents']);
        $lDocs = (new o\data('carts_documents'))->addWhere('id_cart='.$this->cart->id_cart);
        foreach($lDocs as $d){
            $_SESSION['cart']['documents'][] = $d->id_document;
        }
        $data['nbr_doc'] = count($_SESSION['cart']['documents']);

        $this->clDocuments = new clDocuments();
        $this->clDocuments->deleteCartArchive($this->cart->id_cart);

        header('Content-Type: application/json');
        echo json_encode($data);
        die;
    }

    private function majSharePanier(){

        $client = new o\clients(array('hash' => $_POST['client']));
        $cart = new o\carts(array('hash' => $_POST['cart']));
        if($client->id_client == $cart->id_client || $_SESSION['client']['status'] == 2){

            // On supprime les elements existant
            $carts_profiles = new o\data('carts_profiles', array('id_cart'=>$cart->id_cart));
            $carts_profiles->delete();
            $carts_geozones = new o\data('carts_geozones', array('id_cart'=>$cart->id_cart));
            $carts_geozones->delete();

            // Puis on ajoute les nouveaux
            $lProfiles = json_decode($_POST['profiles']);
            if(!empty($lProfiles)){
                foreach($lProfiles as $id_profile){
                    $carts_profils = new o\carts_profiles();
                    $carts_profils->id_cart = $cart->id_cart;
                    $carts_profils->id_profile = $id_profile;
                    $carts_profils->save();
                }
            }
            $lGeozones = json_decode($_POST['geozones']);
            if(!empty($lGeozones)){
                foreach($lGeozones as $id_geozone) {
                    $carts_geozones = new o\carts_geozones();
                    $carts_geozones->id_cart = $cart->id_cart;
                    $carts_geozones->id_geozone = $id_geozone;
                    $carts_geozones->save();
                }
            }

            if(!empty($lProfiles) || !empty($lGeozones)) {
                $cart->shared = 1;
            }else{
                $cart->shared = 0;
            }
            $cart->save();


            if( $cart->shared==1 && $_POST['send']=='true'){

                $i = 0;
                foreach($cart->getClientsAuthorized() as $c){
                    echo 'send mail for : '.$c->email;

                    if(strpos($c->email,"@equinoa.com")!==false){
                        $vars = [
                            'client_prenom_nom' => $c->prenom." ".$c->nom,
                            'client_email' => $c->email,
                            'surl' => $this->surl,
                            'lien' => $this->url."/cart/shared_detail/".$cart->hash,
                            'meta_tile' => "Shared cart",
                            'x-splio-ref' => $this->Config['env'] . '_mz-shared-cart_' . date('Y-m-d')
                        ];

                        $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);

                        $return = $this->clEmail->sendMail("shared-cart", 'en', $c->email,  $vars);
                        if($return){
                            $i++;
                            echo " - OK";
                        }
                    }else
                        $i++;
                    echo "<br/>";
                }

                $cart->send = date('Y-m-d H:m:d');
                $cart->nbr_email = $i;
                $cart->save();
                die;
            }
        }
    }

}
