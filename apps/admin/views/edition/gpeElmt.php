<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-7">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-'.$this->currentType, $this->language, 'Gestion des '.$this->currentType.'s') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/<?=$this->currentType.'s'?>"><?= $this->ln->txt('admin-edition', $this->currentType.'s', $this->language, $this->currentType.'s') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/elements<?=$this->typeShort3?>/<?=$this->typeShort2?>___<?= $this->currentID ?>"><?= $this->ln->txt('admin-edition', 'elements', $this->language, 'Eléments') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'groupes-elements', $this->language, 'Groupes d\'éléments') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-5">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formGpeElmt/<?=$this->typeShort2?>___<?= $this->currentID ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'add-gpeelmt', $this->language, 'Ajouter un groupe d\'éléments') ?>
            </a>
            <a href="<?= $this->lurl ?>/edition/<?=$this->typeShort1?>/<?=$this->typeShort2?>___<?= $this->currentID ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-elmt', $this->language, 'Retour aux éléments') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <?php if (count($this->elements_groupes) > 0) { ?>
            <div class="col-lg-6">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>
                            <?= $this->ln->txt('admin-edition', 'liste-groupes', $this->language, 'Liste des groupes d\'éléments :') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tbody>
                                    <?php foreach ($this->elements_groupes->order('ordre', 'ASC') as $gpe) { ?>
                                        <tr>
                                            <td>
                                                <span class="label label-primary"><i class="fa fa-th-list"></i></span>&nbsp;&nbsp;
                                                <strong><?= $this->ln->txt('admin-edition', 'nomgpe-' . $gpe->slug, $this->language, $gpe->nom) ?></strong>
                                            </td>
                                            <td class="col-sm-1">
                                                <a href="<?= $this->lurl ?>/edition/formGpeElmt/<?=$this->typeShort2?>___<?= $this->currentID ?>/gpe___<?= $gpe->id_groupe ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-edition', 'ordre-gpes', $this->language, 'Ordonnancement des groupes') ?></h5>                    
                    </div>
                    <div class="ibox-content">
                        <p><?= $this->ln->txt('admin-edition', 'expli-ordre-gpes', $this->language, 'Pour trier les groupes vous pouvez les déplacer avec votre souris.') ?></p>
                        <div class="dd" id="nestable">
                            <ol class="dd-list">
                                <?php foreach ($this->elements_groupes->order('ordre', 'ASC') as $gpe) { ?>
                                    <li class="dd-item" data-id="<?= $gpe->id_groupe ?>">
                                        <div class="dd-handle">
                                            <span class="label label-primary"><i class="fa fa-arrows"></i></span>&nbsp;&nbsp;
                                            <strong><?= $this->ln->txt('admin-edition', 'nomgpe-' . $gpe->slug, $this->language, $gpe->nom) ?></strong>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>
                            <?= $this->ln->txt('admin-edition', 'liste-groupes', $this->language, 'Liste des groupes d\'éléments :') ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target);

            if (window.JSON) {
                $.post('<?= $this->lurl ?>/edition/orderGpe', {orderGpe: window.JSON.stringify(list.nestable('serialize'))});
                return false;
            } else {
                alert('<?= $this->ln->txt('admin-alerte', 'alert-fonctionnalite', $this->language, 'Votre navigateur ne supporte pas cette fonctionnalité !') ?>');
            }
        };

        $('#nestable').nestable({maxDepth: 1}).on('change', updateOutput);
    });
</script>