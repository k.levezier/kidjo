<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-blocs', $this->language, 'Gestion des blocs') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'gestion-blocs', $this->language, 'Gestion des blocs') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formBloc" class="btn btn-primary"><?= $this->ln->txt('admin-edition', 'add-bloc', $this->language, 'Ajouter un bloc') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-edition', 'liste-blocs', $this->language, 'Liste des blocs') ?> 
                    </h5>
                </div>
                <div class="ibox-content">
                    <?php if (count($this->blocs) > 0) { ?>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="col-sm-5"><?= $this->ln->txt('admin-edition', 'tab-nom-bloc', $this->language, 'Nom') ?></th>
                                        <th class="col-sm-4"><?= $this->ln->txt('admin-edition', 'tab-slug-bloc', $this->language, 'Slug') ?></th>
                                        <th class="col-sm-1" style="text-align: center;"><?= $this->ln->txt('admin-edition', 'tab-status-bloc', $this->language, 'Status') ?></th>
                                        <th class="col-sm-2"><?= $this->ln->txt('admin-edition', 'tab-action-bloc', $this->language, 'Action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->blocs->order('name', 'ASC') as $bloc) { ?>
                                        <tr>
                                            <td>
                                                <strong><?= $bloc->name ?></strong>
                                            </td>
                                            <td><?= $bloc->slug ?></td>
                                            <td style="text-align: center;font-size: larger;">
                                                <?php
                                                switch($bloc->status){
                                                    case 0:
                                                        echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-edition', 'bloc-offline', $this->language, 'Offline').'"></i>';
                                                        break;
                                                    case 1:
                                                        echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-edition', 'bloc-online', $this->language, 'Online').'"></i>';
                                                        break;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/edition/formBloc/bloc___<?= $bloc->id_bloc ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'btn-edit', $this->language, 'Modifier') ?>
                                                </a>
                                                <a href="<?= $this->lurl ?>/edition/elementsBloc/bloc___<?= $bloc->id_bloc ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-th-list"></i> <?= $this->ln->txt('admin-generic', 'btn-elements', $this->language, 'Eléments') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>                                   
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>