<?php $this->clTemplates->setLng($this->params['lng']); ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-'.$this->currentType, $this->language, 'Gestion des '.$this->currentType.'s') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/<?=$this->currentType.'s'?>"><?= $this->ln->txt('admin-edition', $this->currentType.'s', $this->language, $this->currentType.'s') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/elements<?=$this->typeShort3?>/<?=$this->typeShort2?>___<?= $this->currentID ?>"><?= $this->ln->txt('admin-edition', 'elements', $this->language, 'Eléments') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'edit-elemt', $this->language, 'Modifier un élément') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/elements<?=$this->typeShort3?>/<?=$this->typeShort2?>___<?= $this->currentID ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-elemt', $this->language, 'Retour aux éléments') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-edition', 'form-edit-blocelmtcontent', $this->language, 'Formulaire de modification de contenu d\'un élément') ?></h5>
                </div>
                <div class="ibox-content">
                    <form method="POST" class="form-horizontal" name="formElemt" id="formElemt">
                        <input type="hidden" name="sendForm" />
                        <input type="hidden" name="restePage" id="restePage" value="0" />
                        <input type="hidden" name="id_element" value="<?= $_POST['id_element'] ?>" />
                        <input type="hidden" name="id_bloc" value="<?= $this->blocs->id_bloc ?>" />
                        <input type="hidden" value="<?= in_array($this->params['lng'], array_keys($this->ln->tabLangues))?$this->params['lng']:$this->language ?>" id="selectedLang" name="selectedLang" />
                        <div class="form-group">
                            <?php echo $this->clTemplates->affichageFormBO($this->element, clone $this->blocs_elements); ?>
                        </div> 
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/edition/elementsBloc/bloc___<?=$this->params['bloc']?>" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('change','#type_element', function(){
            if($(this).val() === 'Radio Button' || $(this).val() === 'Select'){
                $('#listComplements').fadeIn();
            } else {
                $('#listComplements').fadeOut();
            }
        });
        
        // Changement de la langues du formulaire
        $(document).on('click', '.change-lang-form', function () {
            var currentLang = $('#selectedLang').val();
            var newLang = $(this).data('lang');
            if (newLang != currentLang) {
                $('.field_lang_' + newLang).removeClass('hidden_field_lang');
                $('.field_lang_' + currentLang).addClass('hidden_field_lang');
                $('#selectedLang').val(newLang);
                $('.button-lang-form').find('img').attr('src', addSURL + '/images/admin/flags/' + newLang + '.png');
            }
        });
    });
</script>