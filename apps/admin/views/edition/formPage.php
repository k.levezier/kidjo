<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-edition', 'edition-site', $this->language, 'Edition du site') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/site"><?= $this->ln->txt('admin-edition', 'site-principal', $this->language, 'Site principal') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', ($this->new ? 'add-page' : 'edit-page'), $this->language, ($this->new ? 'Ajouter une page' : 'Modifier une page')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/site" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-site', $this->language, 'Retour au site') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm_delete">
                    <?= $this->ln->txt('admin-edition', 'delete-page', $this->language, 'Supprimer la page') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>

<?php $this->clTemplates->setLng($this->params['lng']); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">          
            <form method="POST" name="formPage" id="formPage" enctype="multipart/form-data" action="<?=$this->lurl.'/edition/formPage/page___'.$this->tree->id_tree.'/'?>">
                <input type="hidden" name="sendForm" />
                <input type="hidden" name="restePage" id="restePage" value="0" />
                <input type="hidden" name="id_tree" value="<?= $this->tree->id_tree ?>" />
                <input type="hidden" name="id_parent" id="id_parent" value="<?= (is_null($_POST['id_parent'])?'0':$_POST['id_parent']) ?>" />
                <input type="hidden" name="id_canonical" id="id_canonical" value="<?= $_POST['id_canonical'] ?>" />
                <input type="hidden" name="id_template" id="id_template" value="<?= $_POST['id_template'] ?>" />
                <input type="hidden" value="<?= in_array($this->params['lng'], array_keys($this->ln->tabLangues))?$this->params['lng']:$this->language ?>" id="selectedLang" name="selectedLang" />
                <input type="hidden" name="part" id="part" value="1" />
                <?php
                if (!$this->new) { ?><input type="hidden" value="1" name="isNew" /><?php }
                ?>

                <div class="tabs-container tabs-container-products">
                    <div class="tabs-left">
                        <ul class="nav nav-tabs">
                            <li <?=$this->params['part'] == 1 || !isset($this->params['part'])? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-1">
                                    <i class="fa fa-info-circle"></i> 
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-edition', 'onglet-informations', $this->language, 'Informations') ?></span>
                                </a>
                            </li>
                            <?php if($_POST['id_template']==9){ ?>
                            <li <?=$this->params['part'] == 6 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-6" >
                                    <i class="fa fa-book"></i>
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-edition', 'onglet-documents', $this->language, 'Documents') ?></span>
                                </a>
                            </li>
                            <?php } ?>
                            <li <?=$this->params['part'] == 3 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-3" >
                                    <i class="fa fa-file-text"></i> 
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-edition', 'onglet-tpl', $this->language, 'Template') ?></span>
                                </a>
                            </li>
                            <li <?=$this->params['part'] == 4 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-4" >
                                    <i class="fa fa-cogs"></i> 
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-edition', 'onglet-droits', $this->language, 'Droits') ?></span>
                                </a>
                            </li>
                            <li <?=$this->params['part'] == 5 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-5" >
                                    <i class="fa fa-child"></i>
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-edition', 'onglet-enfants', $this->language, 'Enfants') ?></span>
                                </a>
                            </li>
                            <li <?=$this->params['part'] == 2 ? 'class="active"':''?> style="display:none">
                                <a data-toggle="tab" href="#tab-2" >
                                    <i class="fa fa-tachometer"></i>
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-edition', 'onglet-seo', $this->language, 'SEO') ?></span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content ">
                            <div id="tab-1" class="tab-pane <?=$this->params['part'] == 1 || !isset($this->params['part']) ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-edition', 'infos-sur-la-page', $this->language, 'Informations sur la page') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="id_parent"><?= $this->ln->txt('admin-edition', 'form-page-lab-parent', $this->language, 'Page parente') ?></label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="id_parent" id="id_parent" >
                                                        <option value="0"><?= $this->ln->txt('admin-edition', 'form-page-choix-parent', $this->language, 'Choisir une page') ?></option>
                                                        <?= $this->clTemplates->getArboSelect($this->lPages, $_POST['id_parent']) ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="id_canonical"><?= $this->ln->txt('admin-edition', 'form-page-lab-canonique', $this->language, 'Page canonique') ?></label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="id_canonical" id="id_canonical" >
                                                        <option value="0"><?= $this->ln->txt('admin-edition', 'form-page-choix-canonique', $this->language, 'Choisir une page') ?></option>
                                                        <?= $this->clTemplates->getArboSelect($this->lPages, $_POST['id_canonical']) ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="id_template"><?= $this->ln->txt('admin-edition', 'form-page-lab-template', $this->language, 'Template') ?></label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="id_template" id="id_template" onchange="changeTemplate(this.value, 'id_template');">
                                                        <option value="" ><?= $this->ln->txt('admin-edition', 'form-page-choix-tpl', $this->language, 'Choisir un template') ?></option>
                                                        <?= $this->clTemplates->getTemplateSelect($this->lTemplates, $_POST['id_template']) ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <?php $label = $this->ln->txt('admin-edition', 'form-page-lab-titre', $this->language, 'Titre de la page'); ?>
                                                <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'title', $_POST, $label) ?>
                                            </div>
                                            
                                            <div class="form-group">                                                
                                                <?php $label = $this->ln->txt('admin-edition', 'form-page-lab-menu-title', $this->language, 'Titre des menus'); ?>
                                                <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'menu_title', $_POST, $label) ?>
                                            </div>        
                                            
                                            <div class="form-group">
                                                <?php $label = $this->ln->txt('admin-edition', 'form-page-lab-slug', $this->language, 'Lien permanent'); ?>
                                                <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'slug', $_POST, $label) ?>
                                            </div>
                                            
                                            <div class="hr-line-dashed"></div>
                                            <h4><?= $this->ln->txt('admin-edition', 'visibilite-sur-site', $this->language, 'Visibilité sur le site') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <?php 
                                                $label = $this->ln->txt('admin-edition', 'form-page-lab-statut', $this->language, 'Statut de la page'); 
                                                $complements = array(1=>$this->ln->txt('admin-edition', 'form-page-lab-statut-on', $this->language, 'En ligne'),
                                                                     0=>$this->ln->txt('admin-edition', 'form-page-lab-statut-off', $this->language, 'Hors ligne')); 
                                                
                                                echo $this->clTemplates->affichageFormBO(null, null, true, 'Radio Button', 'status', $_POST, $label, $complements);
                                                ?>
                                            </div>
                                            <div class="form-group">
                                                <?php 
                                                $label = $this->ln->txt('admin-edition', 'form-page-lab-nav', $this->language, 'Statut navigation'); 
                                                $complements = array(1=>$this->ln->txt('admin-edition', 'form-page-lab-nav-on', $this->language, 'Visible'),
                                                                     0=>$this->ln->txt('admin-edition', 'form-page-lab-nav-off', $this->language, 'Invisible')); 
                                                
                                                echo $this->clTemplates->affichageFormBO(null, null, true, 'Radio Button', 'status_menu', $_POST, $label, $complements);
                                                ?>
                                            </div>
                                            <div class="form-group">
                                                <?php 
                                                $label = $this->ln->txt('admin-edition', 'form-page-lab-visib', $this->language, 'Visibilité de la page'); 
                                                $complements = array(1=>$this->ln->txt('admin-edition', 'form-page-lab-visib-on', $this->language, 'Privée'),
                                                                     0=>$this->ln->txt('admin-edition', 'form-page-lab-visib-off', $this->language, 'Publique')); 
                                                
                                                echo $this->clTemplates->affichageFormBO(null, null, true, 'Radio Button', 'prive', $_POST, $label, $complements);
                                                ?>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="progress progress-striped active center loadingBar">
                                        <div style="width: 100%" class="progress-bar progress-bar-primary">
                                            <span class="sr-only"><?= $this->ln->txt('admin-generic', 'loading', $this->language, 'Chargement') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group action_btns" style="display:none;">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/edition/site" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(1);" class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane <?=$this->params['part'] == 2 ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-edition', 'elements-de-ref', $this->language, 'Eléments de référencement') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            
                                            <div class="form-group">
                                                <?php $label = $this->ln->txt('admin-edition', 'form-page-lab-meta-title', $this->language, 'Meta title'); ?>
                                                <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'meta_title', $_POST, $label) ?>
                                            </div>
                                            
                                            <div class="form-group">
                                                <?php $label = $this->ln->txt('admin-edition', 'form-page-lab-meta-desc', $this->language, 'Meta description'); ?>
                                                <?= $this->clTemplates->affichageFormBO(null, null, true, 'Textearea', 'meta_description', $_POST, $label) ?>
                                            </div>
                                            
                                            <div class="form-group">
                                                <?php $label = $this->ln->txt('admin-edition', 'form-page-lab-meta-keyw', $this->language, 'Mots clés'); ?>
                                                <?= $this->clTemplates->affichageFormBO(null, null, true, 'Textearea', 'meta_keywords', $_POST, $label) ?>
                                            </div>
                                            
                                            <div class="form-group">
                                                <?php 
                                                $label = $this->ln->txt('admin-edition', 'form-page-lab-index', $this->language, 'Indexation'); 
                                                $complements = array(1=>$this->ln->txt('admin-edition', 'form-page-lab-index-on', $this->language, 'Oui'),
                                                                     0=>$this->ln->txt('admin-edition', 'form-page-lab-index-off', $this->language, 'Non')); 
                                                
                                                echo $this->clTemplates->affichageFormBO(null, null, true, 'Radio Button', 'indexation', $_POST, $label, $complements);
                                                ?>
                                            </div>
                                            
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="progress progress-striped active center loadingBar">
                                        <div style="width: 100%" class="progress-bar progress-bar-primary">
                                            <span class="sr-only"><?= $this->ln->txt('admin-generic', 'loading', $this->language, 'Chargement') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group action_btns" style="display:none;">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/edition/site" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(2);" class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-3" class="tab-pane <?=$this->params['part'] == 3 ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-edition', 'elements-de-tpl', $this->language, 'Eléments de template') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <div id="block_template_elements">
                                            <?php
                                                $hasGroupElt = false;
                                                $hasOrphanElt = false;
                                            if($this->tree->template->exist()){
                                                if($this->tree->template->elements_groupes != null && $this->tree->template->elements_groupes->addWhere('status=1')->count() > 0) {
                                                    
                                                    if($this->tree->template->elements_groupes->order('ordre')->elements->addWhere('status=1')->count() > 0) {
                                                        $hasGroupElt = true;
                                                        
                                                        foreach($this->tree->template->elements_groupes as $gp){
                                                            ?>
                                                            <div class="panel panel-default" >
                                                                <div class="panel-heading">
                                                                    <?= $this->ln->txt('admin-edition', 'liste-element-gpe', $this->language, 'Liste des éléments du groupe :') ?> 
                                                                    <?= $this->ln->txt('admin-edition', 'nomgpe-' . $gp->slug, $this->language, $gp->nom) ?>
                                                                    <div class="ibox-tools">
                                                                        <a class="collapse-link collapse-link-gp-element" id="<?=$gp->id_groupe?>"> 
                                                                            <i class="fa fa-chevron-up"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-body panel-body-2 panel-collapse collapse in" id="g_<?=$gp->id_groupe?>">
                                                                    <?php
                                                                    if(!empty($gp->elements)){ foreach($gp->elements as $elt){
                                                                        $id_tree = $this->duplicate===true?$this->id_tree_origin:$this->tree->id_tree;
                                                                        $tree_elements = new o\data('tree_elements', array('id_element' => $elt->id_element, 'id_tree' => $id_tree));
                                                                        echo $this->clTemplates->affichageFormBO($elt, clone $tree_elements);
                                                                        echo '<div class="clearfix"></div>';
                                                                    } }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                    } 
                                                }
                                                $this->tree->template->elements->addWhere('status=1 AND id_groupe IS NULL');
                                                if($this->tree->template->elements->count() > 0) {
                                                    $hasOprhanElt = true;
                                            ?>
                                                    <div class="panel panel-default" >
                                                        <div class="panel-heading">
                                                            <?= $this->ln->txt('admin-edition', 'liste-element-no-gpe', $this->language, 'Liste des éléments sans groupe') ?> 
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link collapse-link-gp-element" id="0"> 
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="panel-body panel-body-2 panel-collapse collapse in" id="g_0">
                                                            <?php
                                                                    foreach($this->tree->template->elements->order('ordre','asc','') as $elt){
                                                                        $id_tree = $this->duplicate===true?$this->id_tree_origin:$this->tree->id_tree;
                                                                        $tree_elements = new o\data('tree_elements', array('id_element' => $elt->id_element, 'id_tree' => $id_tree));
                                                                        echo $this->clTemplates->affichageFormBO($elt, clone $tree_elements);
                                                                    }
                                                            ?>
                                                        </div>
                                                    </div>
                                            <?php
                                                }
                                                
                                                if(!$hasGroupElt && !$hasOrphanElt)
                                                {
                                                    echo $this->ln->txt('admin-generic', 'template-sans-element', $this->language, 'Ce template ne contient pas d\'élément actif');
                                                }
                                            }
                                            else {
                                                echo $this->ln->txt('admin-generic', 'aucun-template', $this->language, 'Il n\'y a aucun template associé à cette page');
                                            }
                                            ?>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="progress progress-striped active center loadingBar">
                                        <div style="width: 100%" class="progress-bar progress-bar-primary">
                                            <span class="sr-only"><?= $this->ln->txt('admin-generic', 'loading', $this->language, 'Chargement') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group action_btns" style="display:none;">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/edition/site" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(3);" class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-4" class="tab-pane <?=$this->params['part'] == 4 ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-edition', 'droits', $this->language, 'Droits') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-sm-6">
                                                <h4>Zones</h4>
                                                <div class="hr-line-dashed"></div>
                                                <p>
                                                    <i>Si aucune zone n'est sélectionnée alors tout le monde pourra voir cette page.</i>
                                                </p>
                                                <div class="text-center">
                                                    <a class="btn btn-info" onclick="$('#limiter-zones').slideToggle(1500)">Limiter l'accès à :</a>
                                                </div>
                                                <div id="limiter-zones" style="display:<?=(count($this->treeZones)==0?'none':'block')?>">
                                                <?php foreach($this->geozones as $zone) {  ?>
                                                    <div class="form-group" style="margin-bottom:0px;">
                                                        <div class="col-sm-2 m-t-sm">
                                                            <div class="checkbox i-checks">
                                                                <input type="checkbox" name="zone<?=$zone->id_geozone?>" id="zone<?=$zone->id_geozone?>" value="1"<?= (in_array($zone->id_geozone,$this->treeZones) || $this->new ? ' checked' : '') ?>>
                                                            </div>
                                                        </div>
                                                        <label class="text-left control-label" for="zone<?=$zone->id_geozone?>"><?=$zone->name?></label>
                                                    </div>
                                                <?php } ?>
                                                </div>
                                             </div>

                                            <div class="col-sm-6">
                                                <h4>Profils</h4>
                                                <div class="hr-line-dashed"></div>
                                            
                                                <?php foreach($this->profiles as $profile) { ?>
                                                    <div class="form-group">
                                                        <div class="col-sm-2 m-t-sm">
                                                            <div class="checkbox i-checks">
                                                                <input type="checkbox" name="profile<?=$profile->id_profile?>" id="profile<?=$profile->id_profile?>" value="1"<?= (in_array($profile->id_profile,$this->treeProfiles) || $this->new ? ' checked' : '') ?>>
                                                            </div>
                                                        </div>
                                                        <label class="text-left control-label" for="profile<?=$profile->id_profile?>"><?=$profile->name?></label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="progress progress-striped active center loadingBar">
                                        <div style="width: 100%" class="progress-bar progress-bar-primary">
                                            <span class="sr-only"><?= $this->ln->txt('admin-generic', 'loading', $this->language, 'Chargement') ?></span>
                                        </div>
                                    </div>
                                    <div class="form-group action_btns" style="display:none;">
                                        <div class="col-sm-8 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/edition/site" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(4);" class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary saveThenLeave" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-5" class="tab-pane <?=$this->params['part'] == 5 ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-edition', 'enfant-ordre', $this->language, "Gestion de l'ordre d'apparition des pages enfants") ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <?php
                                            $enfants = $this->tree->childs->order('ordre','asc');
                                            if(count($enfants)>0){ ?>
                                            <ul class="sortable" style="list-style: none" id="todo">
                                                <?php foreach($enfants as $child) { ?>
                                                <li style="width:100%; border:1px solid grey; margin:3px; padding:5px" id="child<?=$child->id_tree?>" data-order="<?=$child->ordre?>"><?=$child->menu_title?></li>
                                                <?php } ?>
                                            </ul>
                                            <?php }else{ ?>
                                                <?= $this->ln->txt('admin-edition', 'enfant-nop', $this->language, "Cette page n'a pas d'enfant") ?>
                                            <?php } ?>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-6" class="tab-pane <?=$this->params['part'] == 6 ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <h4><?= $this->ln->txt('admin-edition', 'documents-liste', $this->language, "Liste des documents") ?></h4>
                                        <div class="hr-line-dashed"></div>
                                    <?php if(count($this->treeDocuments)){ ?>
                                        <ul class="sortable" style="list-style: none" id="liste-documents">
                                            <?php foreach($this->treeDocuments as $doc){ ?>
                                                <li style="width:100%; line-height:30px; border:1px solid grey; margin:3px; padding:5px; cursor:pointer;" id="<?=$doc->id_document?>" data-order="<?=$doc->ordre?>">
                                                    <?=($doc->new==1?'<strong>(New)</strong>':'')?> <?=$doc->label?>
                                                    <a class="btn btn-sm btn-info pull-right" href="<?=$this->lurl?>/documents/add/<?=$this->tree->id_tree?>">Voir</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php }else{ ?>
                                        <?= $this->ln->txt('admin-edition', 'documents-nop', $this->language, "Cette page n'a pas de documents") ?>
                                    <?php }
                                        if(empty($this->new)){?>
                                        <div class="clearfix"></div>
                                            <div class="hr-line-dashed"></div>
                                        <div class="form-group action_btns" style="display:none;">
                                            <div class="col-sm-8 col-sm-offset-2">
                                                <a href="<?=$this->lurl.'/documents/add/page___'.$this->tree->id_tree.'/'?>" class="btn btn-primary">Ajouter un document</a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var deleteOnUnload = true;
    
    $(document).on('submit', '#formPage', function(){
        deleteOnUnload = false;
    });
    
    function changeTemplate(id, selectID) {
        var id_old = $("#id_template").val();

        if (id !== id_old) {                 
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'change-tpl-alert', $this->language, 'Confirmez vous le changement de template pour la page ? Ce changement entraînera la perte de toutes les données à l\'enregistrement !') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: true,
                closeOnCancel: true},
            function (isConfirm) {
                console.log('fct');
                if (isConfirm) {
                    $("#id_template").val(id);
                    $("#id_template_old").val(id);
                    
                    $.ajax({
                        url: addLURL + '/edition/getTemplateElements/<?= $this->tree->id_tree ?>/'+id,
                        success:function(data){
                            if(data !== ''){
                                $('#block_template_elements').html(data);
                            } else {
                                $('#block_template_elements').html("<?= $this->ln->txt('admin-generic', 'template-sans-element', $this->language, 'Ce template ne contient pas d\'élément actif') ?>");
                            }
                        },
                        error:function(){
                            $("#" + selectID + " option:selected").removeAttr('selected');
                            $("#" + selectID + " option[value='" + id_old + "']").attr('selected', 'selected');
                        }
                    });

                    return false;
                } else {
                    $("#id_template option:selected").removeAttr('selected');
                    $("#id_template option[value='" + id_old + "']").attr('selected', 'selected');
                    $("#id_template").val(id_old);
                    console.log('cancel'+id_old+$("#id_template").val());
                }
            });
        }
    }

    // Jolie gestion des boutons "parcourir" /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.valuefichier').on('click', function () {
        $(this).siblings('.fichierFile').trigger('click');
    });
    $('.fichierFile').on('change', function () {
        if ($(this).val() !== '') {
            $(this).parent().next('.fichierPath').attr('placeholder', $(this).val());
        } else {
            $(this).parent().next('.fichierPath').attr('placeholder', "<?= $this->ln->txt('admin-generic', 'no-file', $this->language, 'Aucun fichier séléctionné') ?>");
        }
    });

    function fillInput(nom, valeur) {
        $("#" + nom).val(valeur);
    }
   
    function initialiseJqueryPlugins() {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});

        $('.confirm_delete').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-page-alert', $this->language, 'Confirmez vous la suppression de la page et de tous les enfants éventuels ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deletePage/<?= $this->tree->id_tree ?>');
                    return false;
                }
            });
        });
        
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: '<?= $this->ln->txt('admin-generic', 'no-result', $this->language, 'Aucun résultat') ?>'},
            '.chosen-select-width': {width: "95%"}
        };
        for (var selector in config) {
            $(selector+':visible').chosen(config[selector]);
        }
        $("#todo").sortable({

            update: function( event, ui ) {
                var todo = $( "#todo" ).sortable( "toArray" );
                $.post( "/edition/move/<?=$this->params['page']?>", { ordre: window.JSON.stringify(todo), ln:"<?=$this->tree->id_langue?>" });
            }
        }).disableSelection();

        $("#liste-documents").sortable({
            update: function( event, ui ) {
                var liste = $( "#liste-documents" ).sortable( "toArray" );
                console.log(liste);
                $.post( "/edition/move_order_doc/<?=$this->tree->id_tree?>", { ordre: window.JSON.stringify(liste)});
            }
        }).disableSelection();
    }

    $(document).ready(function () {
        $(".loadingBar").hide();
        $(".action_btns").show();
        
        initialiseJqueryPlugins();
        
        // Changement de la langues du formulaire
        $(document).on('click', '.change-lang-form', function () {
            var currentLang = $('#selectedLang').val();
            var newLang = $(this).data('lang');
            if (newLang != currentLang) {
                $('.field_lang_' + newLang).removeClass('hidden_field_lang');
                $('.field_lang_' + currentLang).addClass('hidden_field_lang');
                $('#selectedLang').val(newLang);
                $('.button-lang-form').find('img').attr('src', addSURL + '/images/admin/flags/' + newLang + '.png');
            }
        });
        
        <?php if ($this->new) : ?>
            // Si c'est une nouvelle page, on doit supprimer la page lorsque l'on quitte la page. 
            $.ajaxSetup({async: false}); // Synchrone pour que le l'unload ne se fasse pas avant la requête ajax
            $(window).on('beforeunload', function () {
                if(deleteOnUnload === true){
                    $.ajax({
                        async: false,
                        type: "GET",
                        url: addLURL + "/edition/cancelAddPage/<?= $this->tree->id_tree ?>"
                    });
                    return '<?= $this->ln->txt('admin-edition', 'annulation-ajout-page', $this->language, 'Vous êtes sur le point de quitter la page sans enregistrer la page.') ?>';
                }
            });
        <?php endif; ?>
    });
    
    // Réinitialisation des plugins JQ /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Nécéssaire au changmeent de tabs car certain element initialisé alors que non visible seront mal initialisés
    $(document).on('click', '.tabs-left a', function(){
        initialiseJqueryPlugins();
    });
    
    
    // Mémorisation de dépliement des groupes
    $(document).on('click', '.collapse-link-gp-element', function(){
        var $pan = $(this).closest('.panel').find('.panel-collapse');
        $pan.toggleClass('in');
        localStorage.setItem('t_<?=$this->tree->id_tree?>g_'+$(this).attr('id'), ($pan.hasClass('in')?1:0));
    });
    
    $('.panel-collapse').each(function(){
        var mem = localStorage.getItem('t_<?=$this->tree->id_tree?>'+$(this).attr('id'));
        if(mem !== null && mem != 1){
            $(this).removeClass('in');
        }
    });
    
    $('.collapse-link-gp-element').each(function(){
        var mem = localStorage.getItem('t_<?=$this->tree->id_tree?>g_'+$(this).attr('id'));
        if(mem !== null && mem != 1){
            $(this).find('i').attr('class', 'fa fa-chevron-down');
        }
    });
    
</script>
