<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-blocs', $this->language, 'Gestion des blocs') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/blocs"><?= $this->ln->txt('admin-edition', 'blocs', $this->language, 'Blocs') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', ($this->new ? 'add-bloc' : 'edit-bloc'), $this->language, ($this->new ? 'Ajouter un bloc' : 'Modifier un bloc')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/blocs" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-blocs', $this->language, 'Retour aux blocs') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-edition', 'delete-bloc', $this->language, 'Supprimer le bloc') ?>
                </a>
            <?php } ?>            
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="POST" class="form-horizontal" name="formTpl" id="formTpl" enctype="multipart/form-data">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-edition', ($this->new ? 'form-add-bloc' : 'form-edit-bloc'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'un bloc' : 'Formulaire de modification d\'un bloc')) ?></h5>
                    </div>
                    <div class="ibox-content">                   
                        <input type="hidden" name="sendForm" />
                        <input type="hidden" name="restePage" id="restePage" value="0" />
                        <input type="hidden" name="id_bloc" value="<?= $_POST['id_bloc'] ?>" />
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormNom ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="name"><?= $this->ln->txt('admin-edition', 'form-bloc-lab-nom', $this->language, 'Nom') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $_POST['name'] ?>" required>
                                <?= (isset($_POST['sendForm']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>                                
                            </div>
                        </div>
                        <?php if($this->new == true): ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="slug"><?= $this->ln->txt('admin-edition', 'form-bloc-lab-slug', $this->language, 'Slug') ?></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="slug" id="slug" value="<?= $_POST['slug'] ?>">                            
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="status"><?= $this->ln->txt('admin-edition', 'form-bloc-lab-actif', $this->language, 'Activer le bloc') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="status" id="status" value="1"<?= ($_POST['status'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/edition/blocs" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-tpl-alert', $this->language, 'Confirmez vous la suppression du bloc ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deleteBloc/<?= $this->blocs->id_bloc ?>');
                    return false;
                }
            });
        });
    });
</script>