<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-edition', 'redirections', $this->language, 'Redirections') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'redirections', $this->language, 'Redirections') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formRedir" class="btn btn-primary"><?= $this->ln->txt('admin-edition', 'add-redir', $this->language, 'Ajouter une redirection') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-edition', 'liste-redir', $this->language, 'Liste des redirections') ?></h5>
                </div>
                <div class="ibox-content">
                    <?php if (count($this->redirections) > 0) { ?>
                        <div class="row">
                            <form name="filtresTab" id="filtresTab" method="POST">
                                <div class="col-sm-9">&nbsp;</div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="text" placeholder="<?= $this->ln->txt('admin-generic', 'recherche', $this->language, 'Recherche...') ?>" class="input-sm form-control" name="filtre" value="<?= $_POST['filtre'] ?>"> 
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-sm btn-primary" name="btnSendForm"> <?= $this->ln->txt('admin-generic', 'ok', $this->language, 'OK') ?></button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>                    
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="col-sm-4"><?= $this->ln->txt('admin-edition', 'tab-origine', $this->language, 'Origine') ?></th>
                                        <th class="col-sm-4"><?= $this->ln->txt('admin-edition', 'tab-destination', $this->language, 'Destination') ?></th>
                                        <th class="col-sm-2"><?= $this->ln->txt('admin-edition', 'tab-type', $this->language, 'Type') ?></th>
                                        <th class="col-sm-2">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->redirections->order('updated', 'DESC') as $redir) { ?>
                                        <tr>
                                            <td><a href="<?= $this->urlfront ?>/<?= (count($this->lLangues) > 1 ? $redir->id_langue . '/' : '') ?><?= $redir->from_slug ?>"><?= $redir->from_slug ?></a></td>
                                            <td><a href="<?= $this->urlfront ?>/<?= (count($this->lLangues) > 1 ? $redir->id_langue . '/' : '') ?><?= $redir->to_slug ?>"><?= $redir->to_slug ?></a></td>
                                            <td><?= $redir->type ?></td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/edition/formRedir/<?= $redir->id_redirection ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>                                   
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>