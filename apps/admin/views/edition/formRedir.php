<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $this->ln->txt('admin-edition', 'redirections', $this->language, 'Redirections') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/redirections"><?= $this->ln->txt('admin-edition', 'redirections', $this->language, 'Redirections') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', ($this->new ? 'add-redir' : 'edit-redir'), $this->language, ($this->new ? 'Ajouter une redirection' : 'Modifier une redirection')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/redirections" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-redir', $this->language, 'Retour aux redirections') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-edition', 'delete-redir', $this->language, 'Supprimer la redirection') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-edition', ($this->new ? 'form-add-redir' : 'form-edit-redir'), $this->language, ($this->new ? 'Formulaire d\'ajout de la redirection' : 'Formulaire de modification de la redirection')) ?></h5>
                </div>
                <div class="ibox-content">
                    <?php if ($this->new && count($this->lLangues) > 1) { ?>
                        <p><?= $this->ln->txt('admin-edition', 'intro-addredir', $this->language, 'L\'ajout des redirections par langue sont indépendantes. Sélectionnez la langue pour laquelle vous souhaitez créer votre redirection.') ?></p>
                    <?php } ?>
                    <div class="panel blank-panel">
                        <?php if (count($this->lLangues) > 1) { ?>
                            <div class="panel-heading">
                                <div class="panel-options">
                                    <ul class="nav nav-tabs">
                                        <?php if ($this->new && !isset($_POST['sendForm'])) { ?>
                                            <?php foreach ($this->lLangues as $key => $langue) { ?>                                                
                                                <li<?= ($key == $this->language ? ' class="active"' : '') ?>>
                                                    <a data-toggle="tab" href="#tab-<?= $key ?>"><img src="<?= $this->surl ?>/images/admin/flags/<?= $key ?>.png" title="<?= $langue ?>" /></a>
                                                </li>
                                            <?php } ?>
                                        <?php } else { ?>                                       
                                            <li class="active">
                                                <a data-toggle="tab" href="#tab-<?= $_POST['id_langue'] ?>"><img src="<?= $this->surl ?>/images/admin/flags/<?= $_POST['id_langue'] ?>.png" title="<?= $this->lLangues[$_POST['id_langue']] ?>" /></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?> 
                        <div class="panel-body">
                            <div class="tab-content">
                                <?php foreach ($this->lLangues as $key => $langue) { ?>
                                    <div id="tab-<?= $key ?>" class="tab-pane<?= ($this->new ? ($key == $this->language ? ' active' : '') : ($key == $_POST['id_langue'] ? ' active' : '')) ?>">
                                        <form method="POST" class="form-horizontal" name="formRedir<?= $key ?>" id="formRedir<?= $key ?>">
                                            <input type="hidden" name="sendForm" />
                                            <input type="hidden" name="restePage" id="restePage" value="0" /> 
                                            <input type="hidden" name="id_redirection" value="<?= $_POST['id_redirection'] ?>" />
                                            <input type="hidden" name="id_langue" value="<?= $key ?>" />
                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="type"><?= $this->ln->txt('admin-edition', 'form-redir-lab-type', $this->language, 'Type de redirection') ?></label>
                                                <div class="col-sm-10">
                                                    <select class="form-control" name="type" id="type">
                                                        <option value=""><?= $this->ln->txt('admin-edition', 'form-redir-choix-type', $this->language, 'Choisir un type de redirection') ?></option>
                                                        <?php foreach ($this->lTypesRedir as $num => $type) { ?>
                                                            <option value="<?= $num ?>"<?= ($_POST['type'] == $num ? ' selected' : '') ?>><?= $type ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormType ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormOrigin ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="from_slug"><?= $this->ln->txt('admin-edition', 'form-redir-lab-origine', $this->language, 'Origine') ?></label>
                                                <div class="col-sm-10 input-group">
                                                    <span class="input-group-addon"><?= $this->urlfront ?>/<?= (count($this->lLangues) > 1 ? $key . '/' : '') ?></span> 
                                                    <input type="text" class="form-control shorty" name="from_slug" id="from_slug" value="<?= $_POST['from_slug'] ?>">                                                    
                                                </div>
                                                <?= (isset($_POST['sendForm']) && $this->errorFormOrigin ? '<span class="col-sm-2">&nbsp;</span><span class="help-block m-b-none col-sm-10">' . $this->errorMsg . '</span>' : '') ?>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormDest ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="to_slug"><?= $this->ln->txt('admin-edition', 'form-redir-lab-destination', $this->language, 'Destination') ?></label>
                                                <div class="col-sm-10 input-group">
                                                    <span class="input-group-addon"><?= $this->urlfront ?>/<?= (count($this->lLangues) > 1 ? $key . '/' : '') ?></span> 
                                                    <input type="text" class="form-control shorty" name="to_slug" id="to_slug" value="<?= $_POST['to_slug'] ?>">                                                    
                                                </div>
                                                <?= (isset($_POST['sendForm']) && $this->errorFormDest ? '<span class="col-sm-2">&nbsp;</span><span class="help-block m-b-none col-sm-10">' . $this->errorMsg . '</span>' : '') ?>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="status"><?= $this->ln->txt('admin-edition', 'form-redir-lab-status', $this->language, 'Activer la redirection') ?></label>
                                                <div class="col-sm-10">
                                                    <div class="checkbox i-checks">
                                                        <input type="checkbox" name="status" id="status" value="1"<?= ($_POST['status'] == 1 ? ' checked' : '') ?>>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <div class="col-sm-6 col-sm-offset-2">
                                                    <a href="<?= $this->lurl ?>/edition/redirections" class="btn btn-white">
                                                        <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                                    </a>
                                                    <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                                    <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-redir-alert', $this->language, 'Confirmez vous la suppression de la redirection ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deleteRedir/<?= $this->redirections->id_redirection ?>');
                    return false;
                }
            });
        });
    });
</script>