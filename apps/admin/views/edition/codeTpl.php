<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-templates', $this->language, 'Gestion des templates') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/templates"><?= $this->ln->txt('admin-edition', 'templates', $this->language, 'Templates') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'codesource-templates', $this->language, 'Code source des templates') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/templates" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-tpl', $this->language, 'Retour aux templates') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?php if ($this->codeController != '') { ?>
                <div class="ibox float-e-margins">
                    <form method="POST" class="form-horizontal" name="formController" id="formController">
                        <input type="hidden" name="sendFormController" />
                        <input type="hidden" name="restePageController" id="restePageController" value="0" />
                        <div class="ibox-title">
                            <h5><?= $this->ln->txt('admin-edition', 'cs-cont-tpl', $this->language, 'Source du controller de template') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <textarea name="controller" id="controller"><?= $this->codeController ?></textarea>
                            <div class="ibox-title no-borders">
                                <a href="<?= $this->lurl ?>/edition/templates" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePageController').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            <?php } ?>
            <?php if ($this->codeView != '') { ?>
                <div class="ibox float-e-margins">
                    <form method="POST" class="form-horizontal" name="formView" id="formView">
                        <input type="hidden" name="sendFormView" />
                        <input type="hidden" name="restePageView" id="restePageView" value="0" />
                        <div class="ibox-title">
                            <h5><?= $this->ln->txt('admin-edition', 'cs-view-tpl', $this->language, 'Source de la vue du template') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <textarea name="view" id="view"><?= $this->codeView ?></textarea>
                            <div class="ibox-title no-borders">
                                <a href="<?= $this->lurl ?>/edition/templates" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePageView').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        <?php if ($this->codeController != '') { ?>
            CodeMirror.fromTextArea(document.getElementById("controller"), {
                lineNumbers: true,
                lineWrapping: true,
                matchBrackets: true,
                mode: "application/x-httpd-php",
                indentUnit: 4,
                indentWithTabs: true
            });
        <?php } ?>
        <?php if ($this->codeView != '') { ?>
            CodeMirror.fromTextArea(document.getElementById("view"), {
                lineNumbers: true,
                lineWrapping: true,
                matchBrackets: true,
                mode: "application/x-httpd-php",
                indentUnit: 4,
                indentWithTabs: true
            });
        <?php } ?>
    });
</script>