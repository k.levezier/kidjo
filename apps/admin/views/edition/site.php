<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-edition', 'edition-site', $this->language, 'Edition du site') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'site-principal', $this->language, 'Site principal') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formPage" class="btn btn-primary"><?= $this->ln->txt('admin-edition', 'add-page', $this->language, 'Ajouter une page') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-edition', 'edition-site-principal', $this->language, 'Edition du site principal') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="float-e-margins pull-right">
                        <a onclick="$('#arboSite').jstree('close_all');" class="btn btn-white btn-sm">
                            <i class="fa fa-compress"></i> <?= $this->ln->txt('admin-generic', 'btn-collapseall', $this->language, 'Replier les dossiers') ?>
                        </a>
                        <a onclick="$('#arboSite').jstree('open_all');" class="btn btn-white btn-sm">
                            <i class="fa fa-expand"></i> <?= $this->ln->txt('admin-generic', 'btn-expandall', $this->language, 'Déplier les dossiers') ?>
                        </a>
                    </div>                 
                    <i class="fa fa-home arboTree"></i>
                    <div id="arboSite"><?= $this->arbo ?></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6" id="siteInfos"><?php include('siteInfos.php'); ?></div>
    </div>
</div>
<script type="text/javascript">
    function movePage(id, direction) {
        $(location).attr('href', '<?= $this->lurl ?>/edition/movePage/page___' + id + '/direction___' + direction.toLowerCase());
        return false;
    }
    
    function formPage(id) {
        $(location).attr('href', '<?= $this->lurl ?>/edition/formPage/page___' + id);
        return false;
    }

    function loadAjaxFct(id) {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm_delete_page').on('click', function () {
            var _id = $(this).data('idtree');
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-page-alert', $this->language, 'Confirmez vous la suppression de la page et de tous les enfants éventuels ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                //alert('ok'+_id);
                if (isConfirm) {
                    //alert(_id);
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deletePage/' + _id);
                    return false;
                }
            });
        });
    }

    $(document).ready(function () {
        $('#arboSite').jstree({
            'plugins': ['types', 'dnd'],
            'types': {
                'default': {'icon': 'fa fa-folder'},
                'page': {'icon': 'fa fa-file-text-o'}
            }
        });

        $('#arboSite').jstree('open_all');
        $('#arboSite').bind('select_node.jstree', function (evt, data) {
            console.log(evt);
            $.post('<?= $this->lurl ?>/edition/siteInfos', {id_tree: data.node.data['id']}, function(reponse) {
                $('#siteInfos').html(reponse);
                loadAjaxFct(data.node.data['id']);
            });

            return false;
        });

        $('#arboSite').jstree('select_node', '.clicOnLoad');
    });
</script>