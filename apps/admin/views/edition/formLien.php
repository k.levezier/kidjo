<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-menus', $this->language, 'Gestion des menus') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/menus"><?= $this->ln->txt('admin-edition', 'menus', $this->language, 'Menus') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', ($this->new ? 'add-lien' : 'edit-lien'), $this->language, ($this->new ? 'Ajouter un lien' : 'Modifier un lien')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/elementsMenu/menu___<?= $this->menus->id_menu ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-menu', $this->language, 'Retour au menu') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm_delete">
                    <?= $this->ln->txt('admin-edition', 'delete-lien', $this->language, 'Supprimer le lien') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-edition', ($this->new ? 'form-add-lien' : 'form-edit-lien'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'un lien' : 'Formulaire de modification d\'un lien')) ?></h5>
                </div>
                <div class="ibox-content">
                    <?php if ($this->new && count($this->lLangues) > 1) { ?>
                        <p><?= $this->ln->txt('admin-edition', 'intro-addlien', $this->language, 'L\'ajout des liens par langue sont indissociables.') ?></p>
                    <?php } ?>
                    <div class="panel blank-panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="tab" class="tab-pane active">
                                    <form method="POST" class="form-horizontal" name="formLien" id="formLien">
                                        <input type="hidden" name="sendForm" />
                                        <input type="hidden" name="restePage" id="restePage" value="0" />
                                        <input type="hidden" name="id" value="<?= $_POST['id'] ?>" />
                                        <input type="hidden" name="id_menu" value="<?= $this->menus->id_menu ?>" />
                                        <input type="hidden" value="<?= $this->currentLn ?>" id="selectedLang" name="selectedLang" />
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <label class="control-label col-sm-8"><?= $this->ln->txt('admin-edition', 'label-nom-lien', $this->language, 'Nom du lien') ?></label>
                                                <div class="col-sm-4">
                                                    <div class="btn-group btn-group-lang">
                                                        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                                            <img src="<?=$this->surl?>/images/admin/flags/<?=$this->currentLn?>.png" alt="<?=$this->currentLn?>" /> <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            foreach ($this->ln->tabLangues as $key => $ln) {
                                                                ?>
                                                                <li>
                                                                    <a href="javascript:void(0);" class="change-lang-form" data-lang="<?=$key?>">
                                                                        <img src="<?=$this->surl?>/images/admin/flags/<?=$key?>.png" alt="<?=$ln?>" />
                                                                        <span class="nom_lang"><?=$ln?></span>
                                                                    </a>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <?php
                                                foreach ($this->ln->tabLangues as $key => $langues) {
                                                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                                                    ?>
                                                    <div class="col-sm-12">
                                                        <input type="text" value="<?=$_POST['nom_'.$key]?>" name="nom_<?=$key?>" class="form-control <?=$class?>" />
                                                    </div>
                                                    <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <label class="control-label col-sm-8"><?= $this->ln->txt('admin-edition', 'label-lien', $this->language, 'Lien') ?></label>
                                                <div class="col-sm-4">
                                                    <div class="btn-group btn-group-lang">
                                                        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                                            <img src="<?=$this->surl?>/images/admin/flags/<?=$this->currentLn?>.png" alt="<?=$this->currentLn?>" /> <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            foreach ($this->ln->tabLangues as $key => $ln) {
                                                                ?>
                                                                <li>
                                                                    <a href="javascript:void(0);" class="change-lang-form" data-lang="<?=$key?>">
                                                                        <img src="<?=$this->surl?>/images/admin/flags/<?=$key?>.png" alt="<?=$ln?>" />
                                                                        <span class="nom_lang"><?=$ln?></span>
                                                                    </a>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <?php
                                                $pages = new o\data('tree', array('id_langue'=>fr));
                                                $pdts = new o\data('produits');
                                                $cpt =1;
                                                $cpt += (int)$pages->countAll()>0;
                                                $cpt += (int)$pdts->countAll()>0;

                                                foreach($this->ln->tabLangues as $key=>$ln) {
                                                    $tmp_menus_content = new o\menus_content(array('id_langue'=>$key, 'id'=>$this->menus_content[0]->id));
                                                    $pdt_tree_element = $tmp_menus_content;
                                                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                                                    ?>
                                                    <div class="input-group full-width m-b <?=$class?>">
                                                        <?php

                                                        // Lien interne vers une page
                                                        if($pages->countAll()>0){ 
                                                            ?>
                                                            <div class="col-sm-<?=(12/$cpt)?>">
                                                                <select class="form-control" name="value_<?=$key?>[]">
                                                                    <option value=""><?=$this->ln->txt('admin-generic', 'select-page', $this->language, 'Séléctionner une page')?></option>
                                                                    <?php
                                                                    foreach ($pages as $p){
                                                                        $element = new o\elements(array('id_element'=>2, 'id_langue'=>fr));
                                                                        echo '<option value="'.$p->id_tree.'" '.($_POST['complement_'.$key]=="L" && $_POST['value_'.$key]==$p->id_tree?'selected':'').'>'.$p->title.'</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <?php
                                                        }

                                                        // Lien interne vers une fiche produit
                                                        if($pdts->countAll()>0){
                                                            ?>
                                                            <div class="col-sm-<?=(12/$cpt)?>">
                                                                <select class="form-control col-sm-<?=(12/$cpt)?>" name="value_<?=$key?>[]">
                                                                    <option value=""><?=$this->ln->txt('admin-generic', 'select-produit', $this->language, 'Séléctionner un produit')?></option>
                                                                    <?php
                                                                    foreach ($pdts as $p){
                                                                        $element = new o\produits_elements(array('id_element'=>5, 'id_langue'=>fr, 'id_produit'=>$p->id_produit));
                                                                        echo '<option value="'.$p->id_produit.'" '.($_POST['complement_'.$key]=="P" && $_POST['value_'.$key]==$p->id_produit?'selected':'').'>'.$element->value.'</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <?php
                                                        }
                                                        // Lien externe
                                                        echo '<div class="col-sm-'.(12/$cpt).'">'
                                                                . '<input type="text" name="value_'.$key.'[]" class="form-control col-sm-'.(12/$cpt).'" '.($_POST['complement_'.$key]=="LX"?$_POST['value_'.$key]:'').'>'
                                                            .'</div>';
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <label class="control-label col-sm-8"><?= $this->ln->txt('admin-edition', 'label-target', $this->language, 'Target') ?></label>
                                                <div class="col-sm-4">
                                                    <div class="btn-group btn-group-lang">
                                                        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                                            <img src="<?=$this->surl?>/images/admin/flags/<?=$this->currentLn?>.png" alt="<?=$this->currentLn?>" /> <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            foreach ($this->ln->tabLangues as $key => $ln) {
                                                                ?>
                                                                <li>
                                                                    <a href="javascript:void(0);" class="change-lang-form" data-lang="<?=$key?>">
                                                                        <img src="<?=$this->surl?>/images/admin/flags/<?=$key?>.png" alt="<?=$ln?>" />
                                                                        <span class="nom_lang"><?=$ln?></span>
                                                                    </a>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <?php
                                                foreach ($this->ln->tabLangues as $key => $langues) {
                                                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                                                    ?>
                                                    <div class="col-sm-12">
                                                        <select class="form-control <?=$class?>" name="target_<?=$key?>">
                                                            <?php
                                                            $tabTargets = array(
                                                                $this->ln->txt('admin-generic', 'select-target-self', $this->language, 'Onglet courant')=>'_self', 
                                                                $this->ln->txt('admin-generic', 'select-target-blank', $this->language, 'Nouvel onglet')=>'_blank'
                                                            );
                                                            foreach($tabTargets as $name => $value){
                                                                ?><option value="<?=$value?>"><?=$name?></option><?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <label class="control-label col-sm-8"><?= $this->ln->txt('admin-edition', 'label-status', $this->language, 'Statut') ?></label>
                                                <div class="col-sm-4">
                                                    <div class="btn-group btn-group-lang">
                                                        <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                                            <img src="<?=$this->surl?>/images/admin/flags/<?=$this->currentLn?>.png" alt="<?=$this->currentLn?>" /> <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <?php
                                                            foreach ($this->ln->tabLangues as $key => $ln) {
                                                                ?>
                                                                <li>
                                                                    <a href="javascript:void(0);" class="change-lang-form" data-lang="<?=$key?>">
                                                                        <img src="<?=$this->surl?>/images/admin/flags/<?=$key?>.png" alt="<?=$ln?>" />
                                                                        <span class="nom_lang"><?=$ln?></span>
                                                                    </a>
                                                                </li>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <?php
                                                foreach ($this->ln->tabLangues as $key => $langues) {
                                                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                                                    ?>
                                                    <div class="<?=$class?>">
                                                        <div class="floatingRB radio i-checks">
                                                            <label for="status_<?=$key?>_on"><?= $this->ln->txt('admin-edition', 'label-status-on', $this->language, 'En ligne') ?></label>
                                                            <input type="radio" value="1" id="status_<?=$key?>_on" name="status_<?=$key?>" <?=($_POST['status_'.$key]==1)?'checked':''?>>
                                                        </div>
                                                        <div class="floatingRB radio i-checks">
                                                            <label for="status_<?=$key?>_off"><?= $this->ln->txt('admin-edition', 'label-status-off', $this->language, 'Hors ligne') ?></label>
                                                            <input type="radio" value="0" id="status_<?=$key?>_off" name="status_<?=$key?>" <?=($_POST['status_'.$key]==0)?'checked':''?>>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-6 col-sm-offset-2">
                                                <a href="<?= $this->lurl ?>/edition/elementsMenu/menu___<?= $this->menus->id_menu ?>" class="btn btn-white">
                                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                                </a>
                                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        
        
        <?php if ( ! $this->new) : ?>
        $('.confirm_delete').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-page-lien', $this->language, 'Confirmez vous la suppression du lien ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deleteLien/<?= $this->menus_content[0]->getArray()['id'] ?>');
                    return false;
                }
            });
        });
        <?php endif; ?>
        
        // Changement de la langues du formulaire
        $(document).on('click', '.change-lang-form', function () {
            var currentLang = $('#selectedLang').val();
            var newLang = $(this).data('lang');
            if (newLang != currentLang) {
                $('.field_lang_' + newLang).removeClass('hidden_field_lang');
                $('.field_lang_' + currentLang).addClass('hidden_field_lang');
                $('#selectedLang').val(newLang);
                $('.button-lang-form').find('img').attr('src', addSURL + '/images/admin/flags/' + newLang + '.png');
            }
        }); 
    });
</script>