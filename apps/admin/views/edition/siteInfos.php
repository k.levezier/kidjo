<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?= $this->ln->txt('admin-edition', 'edition-infos-actions-page', $this->language, 'Informations et actions sur la page') ?></h5> 
        <div class="ibox-tools">
            <img src="<?= $this->surl ?>/images/admin/flags/<?= $this->language ?>.png" />
        </div>
    </div>
    <div class="ibox-content">
        <form method="POST" class="form-horizontal" name="formPageQuick" id="formPageQuick" enctype="multipart/form-data">
            <input type="hidden" name="sendFormQuick" />
            <input type="hidden" name="id_tree" value="<?= $_POST['id_tree'] ?>" />
            <input type="hidden" name="id_langue" value="<?= $this->language ?>" />
            <div class="form-group center">
                <a href="<?= $this->lurl ?>/edition/formPage/page___<?= $_POST['id_tree'] ?>" class="btn btn-white btn-sm">
                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'btn-edit', $this->language, 'Modifier') ?>
                </a>
                <a href="<?= $this->lurl ?>/edition/formPage/parent___<?= $_POST['id_tree'] ?>" class="btn btn-white btn-sm">
                    <i class="fa fa-plus"></i> <?= $this->ln->txt('admin-generic', 'btn-add-child', $this->language, 'Ajouter un enfant') ?>
                </a>
                <a href="<?= $this->lurl ?>/edition/formPage/duplicate___<?= $_POST['id_tree'] ?>" class="btn btn-white btn-sm">
                    <i class="fa fa-copy"></i> <?= $this->ln->txt('admin-generic', 'btn-duplicate', $this->language, 'Dupliquer') ?>
                </a>
                <a class="btn btn-danger btn-sm confirm_delete_page" data-idtree="<?= $_POST['id_tree'] ?>">
                    <i class="fa fa-times"></i> <?= $this->ln->txt('admin-generic', 'btn-delete', $this->language, 'Supprimer') ?>
                </a>            
            </div>
            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormTitle ? ' has-error' : '') ?>">                
                <div class="col-sm-12">
                    <label class="control-label-top" for="title"><?= $this->ln->txt('admin-edition', 'form-quickpage-lab-title', $this->language, 'Titre de la page') ?></label>
                    <input type="text" class="form-control" name="title" id="title" value="<?= $_POST['title'] ?>">
                    <?= (isset($_POST['sendForm']) && $this->errorFormTitle ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                </div>
            </div>
            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormMenuTitle ? ' has-error' : '') ?>">                
                <div class="col-sm-12">
                    <label class="control-label-top" for="menu_title"><?= $this->ln->txt('admin-edition', 'form-quickpage-lab-menutitle', $this->language, 'Titre dans le menu') ?></label>
                    <input type="text" class="form-control" name="menu_title" id="menu_title" value="<?= $_POST['menu_title'] ?>">
                    <?= (isset($_POST['sendForm']) && $this->errorFormMenuTitle ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                </div>
            </div>
            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormSlug ? ' has-error' : '') ?>">                
                <div class="col-sm-12">
                    <label class="control-label-top" for="slug"><?= $this->ln->txt('admin-edition', 'form-quickpage-lab-slug', $this->language, 'Lien permanent') ?></label>
                    <input type="text" class="form-control" name="slug" id="slug" value="<?= $_POST['slug'] ?>">
                    <?= (isset($_POST['sendForm']) && $this->errorFormSlug ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group">
                <label class="col-sm-4"><?= $this->ln->txt('admin-edition', 'form-quickpage-lab-status', $this->language, 'Statut de la page') ?></label>
                <div class="col-sm-8">
                    <div class="radio i-checks inline">
                        <label class="text-primary">
                            <input type="radio" value="1" name="status"<?= ($_POST['status'] == 1 ? ' checked' : '') ?>><i></i> <?= $this->ln->txt('admin-edition', 'form-quickpage-lab-online', $this->language, 'En ligne') ?>
                        </label>
                    </div>
                    <div class="radio i-checks inline">
                        <label class="text-danger">
                            <input type="radio" value="0" name="status"<?= ($_POST['status'] == 0 ? ' checked' : '') ?>><i></i> <?= $this->ln->txt('admin-edition', 'form-quickpage-lab-offline', $this->language, 'Hors ligne') ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed"></div>
            <div class="form-group no-margins center">
                <div class="col-sm-12">
                    <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-modifs', $this->language, 'Enregistrer les modifications') ?></button>
                </div>
            </div>
        </form>
    </div>
</div>