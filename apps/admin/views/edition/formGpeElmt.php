<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-'.$this->currentType, $this->language, 'Gestion des '.$this->currentType.'s') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/<?=$this->currentType.'s'?>"><?= $this->ln->txt('admin-edition', $this->currentType.'s', $this->language, $this->currentType.'s') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/elements<?=$this->typeShort3?>/<?=$this->typeShort2?>___<?= $this->currentID ?>"><?= $this->ln->txt('admin-edition', 'elements', $this->language, 'Eléments') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/gpeElmt/<?=$this->typeShort2?>___<?= $this->currentID ?>"><?= $this->ln->txt('admin-edition', 'groupes-elements', $this->language, 'Groupes d\'éléments') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', ($this->new ? 'add-gpe' : 'edit-gpe'), $this->language, ($this->new ? 'Ajouter un groupe' : 'Modifier un groupe')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/gpeElmt/<?=$this->typeShort2?>___<?= $this->currentID ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-gpe', $this->language, 'Retour aux groupes') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-edition', 'delete-gpe', $this->language, 'Supprimer le groupe') ?>
                </a>
            <?php } ?>            
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="POST" class="form-horizontal" name="formGpe" id="formGpe" enctype="multipart/form-data">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-edition', ($this->new ? 'form-add-gpe' : 'form-edit-gpe'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'un groupe' : 'Formulaire de modification d\'un groupe')) ?></h5>
                    </div>
                    <div class="ibox-content">                    
                        <input type="hidden" name="sendForm" />
                        <input type="hidden" name="restePage" id="restePage" value="0" />
                        <input type="hidden" name="id_groupe" value="<?= $_POST['id_groupe'] ?>" />
                        <input type="hidden" name="id_template" value="<?= $this->templates->id_template ?>" /> 
                        <input type="hidden" name="id_bloc" value="<?= $this->blocs->id_bloc ?>" /> 
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormNom ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="nom"><?= $this->ln->txt('admin-edition', 'form-gpe-lab-nom', $this->language, 'Nom') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nom" id="nom" value="<?= $_POST['nom'] ?>">
                                <?= (isset($_POST['sendForm']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>                                
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="status"><?= $this->ln->txt('admin-edition', 'form-gpe-lab-status', $this->language, 'Activer le groupe') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="status" id="status" value="1"<?= ($_POST['status'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/edition/gpeElmt/<?=$this->typeShort2?>___<?= $this->currentID ?>" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-gpe-alert', $this->language, 'Confirmez vous la suppression du groupe ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deleteGpe/<?=$this->typeShort2?>___<?= $this->currentID ?>/gpe___<?= $this->elements_groupes->id_groupe ?>');
                    return false;
                }
            });
        });
    });
</script>