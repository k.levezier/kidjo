<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-menus', $this->language, 'Gestion des menus') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'gestion-menus', $this->language, 'Gestion des menus') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formMenu" class="btn btn-primary"><?= $this->ln->txt('admin-edition', 'add-menu', $this->language, 'Ajouter un menu') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-edition', 'liste-menus', $this->language, 'Liste des menus') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <?php if ($this->menus->count() > 0) { ?>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="col-sm-4"><?= $this->ln->txt('admin-edition', 'tab-nom-menu', $this->language, 'Nom du menu') ?></th>
                                        <th class="col-sm-4"><?= $this->ln->txt('admin-edition', 'tab-slug-menu', $this->language, 'Slug du menu') ?></th>
                                        <th class="col-sm-2"><?= $this->ln->txt('admin-edition', 'tab-statut-menu', $this->language, 'Statut du menu') ?></th>
                                        <th class="col-sm-2">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->menus->order('nom') as $key => $menu) { ?>
                                        <tr>
                                            <td class="tooltip-demo">
                                                <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'code', $this->language, 'Code :') ?> $menu = $this->menus->getMenu('<?= $menu['slug'] ?>', $this->language, $this->lurl);"><?= $menu['nom'] ?></strong>
                                            </td>
                                            <td><?= $menu['slug'] ?></td>
                                            <td>
                                                <strong class="text-<?= ($menu['status'] == 1 ? 'info' : 'danger') ?>">
                                                    <?= ($menu['status'] == 1 ? $this->ln->txt('admin-generic', 'alert-actif', $this->language, 'Actif') : $this->ln->txt('admin-generic', 'alert-inactif', $this->language, 'Inactif')) ?>
                                                </strong>
                                            </td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/edition/formMenu/<?= $menu['id_menu'] ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'btn-edit', $this->language, 'Modifier') ?>
                                                </a>
                                                <a href="<?= $this->lurl ?>/edition/elementsMenu/menu___<?= $menu['id_menu'] ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-th-list"></i> <?= $this->ln->txt('admin-generic', 'btn-liens', $this->language, 'Liens') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>                                   
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>