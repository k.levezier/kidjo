<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-'.$this->currentType, $this->language, 'Gestion des '.$this->currentType.'s') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/<?=$this->currentType.'s'?>"><?= $this->ln->txt('admin-edition', $this->currentType.'s', $this->language, $this->currentType.'s') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/elements<?=$this->typeShort3?>/<?=$this->typeShort2?>___<?= $this->currentID ?>"><?= $this->ln->txt('admin-edition', 'elements', $this->language, 'Eléments') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', ($this->new ? 'add-elemt' : 'edit-elemt'), $this->language, ($this->new ? 'Ajouter un élément' : 'Modifier un élément')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/elements<?=$this->typeShort3?>/<?=$this->typeShort2?>___<?= $this->currentID ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-elemt', $this->language, 'Retour aux éléments') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-edition', 'delete-elemt', $this->language, 'Supprimer l\'élément') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-edition', ($this->new ? 'form-add-elemt' : 'form-edit-elemt'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'un élément' : 'Formulaire de modification d\'un élément')) ?></h5>
                </div>
                <div class="ibox-content">
                    <form method="POST" class="form-horizontal" name="formElemt" id="formElemt">
                        <input type="hidden" name="sendForm" />
                        <input type="hidden" name="restePage" id="restePage" value="0" />
                        <input type="hidden" name="id_element" value="<?= $_POST['id_element'] ?>" />
                        <input type="hidden" name="id_template" value="<?= $this->templates->id_template ?>" />
                        <input type="hidden" name="id_bloc" value="<?= $this->blocs->id_bloc ?>" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="id_groupe"><?= $this->ln->txt('admin-edition', 'form-elmt-lab-groupe', $this->language, 'Groupe d\'éléments') ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="id_groupe" id="id_groupe">
                                    <option value="0"><?= $this->ln->txt('admin-edition', 'form-elmt-choix-group', $this->language, 'Choisir un groupe d\'éléments') ?></option>
                                    <?php foreach ($this->elements_groupes->order('nom', 'ASC') as $gpe) { ?>
                                        <option value="<?= $gpe->id_groupe ?>"<?= ($_POST['id_groupe'] == $gpe->id_groupe ? ' selected' : '') ?>><?= $gpe->nom ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormName ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="name"><?= $this->ln->txt('admin-edition', 'form-elemt-lab-nom', $this->language, 'Nom de l\'élément') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $_POST['name'] ?>">
                                <?= (isset($_POST['sendForm']) && $this->errorFormName ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>                                
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="type_element"><?= $this->ln->txt('admin-edition', 'form-elmt-lab-type', $this->language, 'Type d\'élément') ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="type_element" id="type_element">
                                    <option value=""><?= $this->ln->txt('admin-edition', 'form-choix-type', $this->language, 'Choisir un type') ?></option>
                                    <?php foreach ($this->clTemplates->lElements as $type) { ?>
                                        <option value="<?= $type ?>"<?= ($_POST['type_element'] == $type ? ' selected' : '') ?>>
                                            <?= $this->ln->txt('admin-edition', 'type-' . generateSlug($type), $this->language, $type) ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <?= (isset($_POST['sendForm']) && $this->errorFormType ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?> 
                                
                                    <div id="listComplements" class="form-group list_cpl" <?=in_array($_POST['type_element'], array('Radio Button', 'Select'))?'':'style="display:none;"'?>>
                                        <div class="hr-line-dashed"></div>
                                            <div class="col-sm-6">
                                                <table class="table">
                                                    <thead>
                                                            <th class="col-sm-6">Nom</th>
                                                            <th class="col-sm-5">Valeur</th>
                                                            <th class="col-sm-1">&nbsp;</th>
                                                    </thead>
                                                    <tbody id="options_list">
                                                        <?php
                                                        $complements = unserialize($_POST['complement']);
                                                        if($complements !== false && count($complements)>0){
                                                            $start = true;
                                                            foreach($complements as $value => $name){
                                                                ?>
                                                                <tr>
                                                                    <td class="col-sm-6"><input type="text" name="option_name[]" value="<?=$name?>" class="form-control"/></td>
                                                                    <td class="col-sm-5"><input type="text" name="option_value[]" value="<?=$value?>" class="form-control"/></td>
                                                                    <td class="col-sm-1">
                                                                        <?php
                                                                        if($start!==true){
                                                                            ?>
                                                                            <a href="javascript:void(0);" class="del_option btn btn-danger"><i class="fa fa-times"></i></a>
                                                                            <?php
                                                                        } else {$start = false;}             
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <tr>
                                                                <td class="col-sm-6"><input type="text" name="option_name[]" value="" class="form-control"/></td>
                                                                <td class="col-sm-5"><input type="text" name="option_value[]" value="" class="form-control"/></td>
                                                                <td class="col-sm-1">
                                                                </td>
                                                            </tr>
                                                            <?php 
                                                        } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <a href="javascript:void(0);" id="add_option" class="btn btn-primary">
                                                    <?= $this->ln->txt('admin-edition', 'add-complement', $this->language, 'Ajouter une option') ?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="status"><?= $this->ln->txt('admin-edition', 'form-elemt-lab-status', $this->language, 'Activer l\'élément') ?></label>
                            <div class="col-sm-10">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="status" id="status" value="1"<?= ($_POST['status'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-2">
                                <?php $url = $this->templates->exist()?'elementsTpl/tpl___'.$this->templates->id_template:'elementsBloc/bloc___'.$this->blocs->id_bloc ?>
                                <a href="<?= $this->lurl ?>/edition/<?=$url?>" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-elemt-alert', $this->language, 'Confirmez vous la suppression de l\'élément ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    <?php $url_delete = $this->templates->exist()?'tpl___'.$this->templates->id_template:'bloc___'.$this->blocs->id_bloc ?>
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deleteElmt/<?= $url_delete ?>/elmt___<?= $this->elements->id_element ?>');
                    return false;
                }
            });
        });
        
        $(document).on('change','#type_element', function(){
            if($(this).val() === 'Radio Button' || $(this).val() === 'Select'){
                $('#listComplements').fadeIn();
            } else {
                $('#listComplements').fadeOut();
            }
        });
        
        $(document).on('click', '#add_option', function(){
            $('#options_list').append('<tr><td class="col-sm-6"><input type="text" name="option_name[]" value="" class="form-control"/></td><td class="col-sm-5"><input type="text" name="option_value[]" value="" class="form-control"/></td><td class="col-sm-1"><a href="javascript:void(0);" class="del_option btn btn-danger"><i class="fa fa-times"></i></a></td></tr>');
        });
        
        $(document).on('click', '.del_option', function(){
            $(this).parent().parent().remove();
        });
    });
</script>