<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-7">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-templates', $this->language, 'Gestion des templates') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/templates"><?= $this->ln->txt('admin-edition', 'templates', $this->language, 'Templates') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'elements-templates', $this->language, 'Eléments du template') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-5">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formElmt/tpl___<?= $this->templates->id_template ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'add-elmt', $this->language, 'Ajouter un élément') ?>
            </a>
            <a href="<?= $this->lurl ?>/edition/gpeElmt/tpl___<?= $this->templates->id_template ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'gpeelmt', $this->language, 'Groupe d\'éléments') ?>
            </a>
            <a href="<?= $this->lurl ?>/edition/templates" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-tpl', $this->language, 'Retour aux templates') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">    
    <?php if ($this->elements_groupes->count() > 0) {
        foreach ($this->elements_groupes->order('ordre', 'ASC') as $groupe) {
            if ($this->{'lElements' . $groupe->id_groupe}->count() > 0) { ?>
                <div class="row">
                    <div class="col-lg-<?= ($this->{'lElements' . $groupe->id_groupe}->count() > 1 ? 6 : 12) ?>">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>
                                    <?= $this->ln->txt('admin-edition', 'liste-element-gpe', $this->language, 'Liste des éléments du groupe :') ?> 
                                    <?= $this->ln->txt('admin-edition', 'nomgpe-' . $groupe->slug, $this->language, $groupe->nom) ?>
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th><?= $this->ln->txt('admin-edition', 'tab-nom-element', $this->language, 'Nom') ?></th>
                                                <th><?= $this->ln->txt('admin-edition', 'tab-type-element', $this->language, 'Type') ?></th>
                                                <th class="col-sm-1">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($this->{'lElements' . $groupe->id_groupe}->order('ordre', 'ASC') as $elmt) { ?>
                                                <tr>
                                                    <td class="tooltip-demo">
                                                        <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'utilisation', $this->language, 'Utilisation :') ?> $this->clTemplates->getElemt(<?= $this->templates->id_template ?>,'<?= $elmt->slug ?>','content');"><?= $this->ln->txt('admin-edition', 'nomelmt-' . $elmt->slug, $this->language, $elmt->name) ?></strong>
                                                    </td>
                                                    <td><?= $elmt->type_element ?></td>
                                                    <td>
                                                        <a href="<?= $this->lurl ?>/edition/formElmt/tpl___<?= $this->templates->id_template ?>/elmt___<?= $elmt->id_element ?>" class="btn btn-white btn-sm">
                                                            <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if ($this->{'lElements' . $groupe->id_groupe}->count() > 1) { ?>
                        <div class="col-lg-6">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5>
                                        <?= $this->ln->txt('admin-edition', 'ordre-element-gpe', $this->language, 'Ordonnancement des éléments du groupe :') ?> 
                                        <?= $this->ln->txt('admin-edition', 'nomgpe-' . $groupe->slug, $this->language, $groupe->nom) ?>
                                    </h5>                    
                                </div>
                                <div class="ibox-content">
                                    <p><?= $this->ln->txt('admin-edition', 'expli-ordre-elmt', $this->language, 'Pour trier les éléments vous pouvez les déplacer avec votre souris.') ?></p>
                                    <div class="dd" id="nestable<?= $groupe->id_groupe ?>">
                                        <ol class="dd-list">
                                            <?php foreach ($this->{'lElements' . $groupe->id_groupe}->order('ordre', 'ASC') as $elmt) { ?>
                                                <li class="dd-item" data-id="<?= $elmt->id_element ?>">
                                                    <div class="dd-handle">
                                                        <span class="label label-primary"><i class="fa fa-arrows"></i></span>&nbsp;&nbsp;
                                                        <strong><?= $this->ln->txt('admin-edition', 'nomelmt-' . $elmt->slug, $this->language, $elmt->name) ?></strong>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>
                                    <?= $this->ln->txt('admin-edition', 'liste-element-gpe', $this->language, 'Liste des éléments du groupe :') ?> 
                                    <?= $this->ln->txt('admin-edition', 'nomgpe-' . $groupe->slug, $this->language, $groupe->nom) ?>
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }
        }
    } else { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-edition', 'liste-gpe-elt', $this->language, 'Liste des groupes d\'éléments') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($this->orphan_elements->count() > 0) { ?>
        <div class="row">
            <div class="col-lg-<?= ($this->orphan_elements->count() > 1 ? 6 : 12) ?>">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-info"></i> <?= $this->ln->txt('admin-edition', 'liste-element-orphan', $this->language, 'Liste des éléments sans groupe') ?>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?= $this->ln->txt('admin-edition', 'tab-nom-element', $this->language, 'Nom') ?></th>
                                        <th><?= $this->ln->txt('admin-edition', 'tab-type-element', $this->language, 'Type') ?></th>
                                        <th class="col-sm-1">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->orphan_elements->order('ordre', 'ASC') as $elmt) { ?>
                                        <tr>
                                            <td class="tooltip-demo">
                                                <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'utilisation', $this->language, 'Utilisation :') ?> $this->clTemplates->getElemt(<?= $this->templates->id_template ?>,'<?= $elmt->slug ?>','content');"><?= $this->ln->txt('admin-edition', 'nomelmt-' . $elmt->slug, $this->language, $elmt->name) ?></strong>
                                            </td>
                                            <td><?= $elmt->type_element ?></td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/edition/formElmt/tpl___<?= $this->templates->id_template ?>/elmt___<?= $elmt->id_element ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>    
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($this->orphan_elements->count() > 1) { ?>
                <div class="col-lg-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-info"></i> <?= $this->ln->txt('admin-edition', 'ordre-element-ssgpe', $this->language, 'Ordonnancement des éléments sans groupe') ?>
                        </div>
                        <div class="panel-body">
                            <p><?= $this->ln->txt('admin-edition', 'expli-ordre-elmt', $this->language, 'Pour trier les éléments vous pouvez les déplacer avec votre souris.') ?></p>
                            <div class="dd" id="nestable0">
                                <ol class="dd-list">
                                    <?php foreach ($this->orphan_elements->order('ordre', 'ASC') as $elmt) { ?>
                                        <li class="dd-item" data-id="<?= $elmt->id_element ?>">
                                            <div class="dd-handle">
                                                <span class="label label-primary"><i class="fa fa-arrows"></i></span>&nbsp;&nbsp;
                                                <strong><?= $this->ln->txt('admin-edition', 'nomelmt-' . $elmt->slug, $this->language, $elmt->name) ?></strong>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
<?php if ($this->elements_groupes->count() > 0) { ?>
    <script type="text/javascript">
        $(document).ready(function () {
            <?php foreach ($this->elements_groupes->order('ordre', 'ASC') as $groupe) {
                if ($this->{'lElements' . $groupe->id_groupe}->count() > 1) { ?>
                    var updateOutput<?= $groupe->id_groupe ?> = function (e) {
                        var list<?= $groupe->id_groupe ?> = e.length ? e : $(e.target);

                        if (window.JSON) {
                            $.post('<?= $this->lurl ?>/edition/orderElmt', {orderElmt: window.JSON.stringify(list<?= $groupe->id_groupe ?>.nestable('serialize'))});
                            return false;
                        } else {
                            alert('<?= $this->ln->txt('admin-alerte', 'alert-fonctionnalite', $this->language, 'Votre navigateur ne supporte pas cette fonctionnalité !') ?>');
                        }
                    };

                    $('#nestable<?= $groupe->id_groupe ?>').nestable({maxDepth: 1}).on('change', updateOutput<?= $groupe->id_groupe ?>);
                <?php }
            } ?>
            <?php if ($this->orphan_elements->count() > 1) { ?>
                var updateOutput0 = function (e) {
                    var list0 = e.length ? e : $(e.target);

                    if (window.JSON) {
                        $.post('<?= $this->lurl ?>/edition/orderElmt', {orderElmt: window.JSON.stringify(list0.nestable('serialize'))});
                        return false;
                    } else {
                        alert('<?= $this->ln->txt('admin-alerte', 'alert-fonctionnalite', $this->language, 'Votre navigateur ne supporte pas cette fonctionnalité !') ?>');
                    }
                };

                $('#nestable0').nestable({maxDepth: 1}).on('change', updateOutput0);
            <?php } ?>
        });
    </script>
<?php } ?>