<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-templates', $this->language, 'Gestion des templates') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/templates"><?= $this->ln->txt('admin-edition', 'templates', $this->language, 'Templates') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', ($this->new ? 'add-tpl' : 'edit-tpl'), $this->language, ($this->new ? 'Ajouter un template' : 'Modifier un template')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/templates" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-tpl', $this->language, 'Retour aux templates') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-edition', 'delete-tpl', $this->language, 'Supprimer le template') ?>
                </a>
            <?php } ?>            
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="POST" class="form-horizontal" name="formTpl" id="formTpl" enctype="multipart/form-data">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-edition', ($this->new ? 'form-add-tpl' : 'form-edit-tpl'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'un template' : 'Formulaire de modification d\'un template')) ?></h5>
                    </div>
                    <div class="ibox-content">                   
                        <input type="hidden" name="sendForm" />
                        <input type="hidden" name="restePage" id="restePage" value="0" />
                        <input type="hidden" name="id_template" value="<?= $_POST['id_template'] ?>" />
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormNom ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="name"><?= $this->ln->txt('admin-edition', 'form-tpl-lab-nom', $this->language, 'Nom') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $_POST['name'] ?>">
                                <?= (isset($_POST['sendForm']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>                                
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="type"><?= $this->ln->txt('admin-edition', 'form-tpl-lab-type', $this->language, 'Type') ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="type" id="type">
                                    <option value=""><?= $this->ln->txt('admin-edition', 'form-choix-type', $this->language, 'Choisir un type') ?></option>
                                    <?php foreach ($this->clTemplates->lTypes as $type) { ?>
                                        <option value="<?= $type ?>"<?= ($_POST['type'] == $type ? ' selected' : '') ?>>
                                            <?= $this->ln->txt('admin-edition', 'type-' . generateSlug($type), $this->language, $type) ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <?= (isset($_POST['sendForm']) && $this->errorFormType ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?> 
                            </div>
                        </div> 
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="class_template"><?= $this->ln->txt('admin-edition', 'form-tpl-lab-classtpl', $this->language, 'Classe CSS') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="class_template" id="class_template" value="<?= $_POST['class_template'] ?>">                              
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="affichage"><?= $this->ln->txt('admin-edition', 'form-tpl-lab-affichage', $this->language, 'Template affichage') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="affichage" id="affichage" value="1"<?= ($_POST['affichage'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="is_gamme"><?= $this->ln->txt('admin-edition', 'form-tpl-lab-isgamme', $this->language, 'Template gamme') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="is_gamme" id="is_gamme" value="1"<?= ($_POST['is_gamme'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="status"><?= $this->ln->txt('admin-edition', 'form-tpl-lab-status', $this->language, 'Activer le template') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="status" id="status" value="1"<?= ($_POST['status'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/edition/templates" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-edition', 'delete-tpl-alert', $this->language, 'Confirmez vous la suppression du template ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/edition/deleteTpl/<?= $this->templates->id_template ?>');
                    return false;
                }
            });
        });
    });
</script>