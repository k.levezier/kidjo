<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-templates', $this->language, 'Gestion des templates') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'gestion-templates', $this->language, 'Gestion des templates') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formTpl" class="btn btn-primary"><?= $this->ln->txt('admin-edition', 'add-tpl', $this->language, 'Ajouter un template') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?php foreach ($this->clTemplates->lTypes as $type) { ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>
                            <?= $this->ln->txt('admin-edition', 'liste-tpl-type', $this->language, 'Liste des templates de type') ?> 
                            <?= $this->ln->txt('admin-edition', 'type-tpl-' . generateSlug($type), $this->language, $type) ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <?php if (count($this->{'lTemplates' . $type}) > 0) { ?>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th><?= $this->ln->txt('admin-edition', 'tab-nom-tpl', $this->language, 'Nom') ?></th>
                                            <th class="col-sm-2"><?= $this->ln->txt('admin-edition', 'tab-affichage-tpl', $this->language, 'Type') ?></th>
                                            <th class="col-sm-4">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($this->{'lTemplates' . $type}->order('name', 'ASC') as $tpl) { ?>
                                            <tr>
                                                <?php if ($tpl->affichage == 1) { ?>
                                                    <td class="tooltip-demo">
                                                        <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'fichier', $this->language, 'Fichier :') ?> <?= $tpl->slug ?>.php"><?= $tpl->name ?></strong>
                                                    </td>
                                                <?php } else { ?>
                                                    <td>
                                                        <strong><?= $tpl->name ?></strong>
                                                    </td>
                                                <?php } ?>
                                                <td><?= ($tpl->affichage == 1 ? $this->ln->txt('admin-edition', 'type-affichage-tpl', $this->language, 'Affichage') : $this->ln->txt('admin-edition', 'type-contenu-tpl', $this->language, 'Contenu')) ?></td>
                                                <td>
                                                    <a href="<?= $this->lurl ?>/edition/formTpl/<?= $tpl->id_template ?>" class="btn btn-white btn-sm">
                                                        <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'btn-edit', $this->language, 'Modifier') ?>
                                                    </a>
                                                    <?php if ($tpl->affichage == 1) { ?>
                                                        <a href="<?= $this->lurl ?>/edition/codeTpl/tpl___<?= $tpl->id_template ?>" class="btn btn-white btn-sm">
                                                            <i class="fa fa-code"></i> <?= $this->ln->txt('admin-generic', 'btn-code', $this->language, 'Code source') ?>
                                                        </a>
                                                    <?php } ?>
                                                    <a href="<?= $this->lurl ?>/edition/elementsTpl/tpl___<?= $tpl->id_template ?>" class="btn btn-white btn-sm">
                                                        <i class="fa fa-th-list"></i> <?= $this->ln->txt('admin-generic', 'btn-elements', $this->language, 'Eléments') ?>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>                                   
                                    </tbody>
                                </table>
                            </div>
                        <?php } else { ?>
                            <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>