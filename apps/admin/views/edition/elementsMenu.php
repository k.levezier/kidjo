<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-7">
        <h2><?= $this->ln->txt('admin-edition', 'gestion-menus', $this->language, 'Gestion des menus') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/edition"><?= $this->ln->txt('admin-edition', 'edition', $this->language, 'Edition') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/edition/menus"><?= $this->ln->txt('admin-edition', 'menus', $this->language, 'Menus') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-edition', 'liens-menus', $this->language, 'Liens du menu') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-5">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/edition/formLien/menu___<?= $this->menus->id_menu ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'add-lien', $this->language, 'Ajouter un lien') ?>
            </a>
            <a href="<?= $this->lurl ?>/edition/menus" class="btn btn-primary">
                <?= $this->ln->txt('admin-edition', 'back-menus', $this->language, 'Retour aux menus') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">    
    <?php if ($this->lLiens->count() > 0) { ?>
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>
                            <?= $this->ln->txt('admin-edition', 'liste-liens-menu', $this->language, 'Liste des liens du menu :') ?> 
                            <?= $this->menus->nom ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th><?= $this->ln->txt('admin-edition', 'tab-nom-lien', $this->language, 'Nom du lien') ?></th>
                                        <th><?= $this->ln->txt('admin-edition', 'tab-type-lien', $this->language, 'Type') ?></th>
                                        <th><?= $this->ln->txt('admin-edition', 'tab-target-lien', $this->language, 'Target') ?></th>
                                        <th class="col-sm-1">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->lLiens->order('ordre') as $key => $lien) { ?>
                                        <tr>
                                            <td>
                                                <strong class="text-<?= ($lien['status'] == 1 ? 'info' : 'danger') ?>">
                                                    <?= $lien['nom'] ?>
                                                </strong>
                                            </td>
                                            <td><?= ($lien['complement'] == 'LX' ? $this->ln->txt('admin-edition', 'type-lien-externe', $this->language, 'Externe') : $this->ln->txt('admin-edition', 'type-lien-interne', $this->language, 'Interne')) ?></td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/edition/formLien/menu___<?= $this->menus->id_menu ?>/lien___<?= $lien['id'] ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($this->lLiens->count() > 1) { ?>
                <div class="col-lg-6">
                    <div class="ibox ">
                        <div class="ibox-title">
                            <h5>
                                <?= $this->ln->txt('admin-edition', 'ordre-menu-lien', $this->language, 'Ordonnancement des liens du menu :') ?> 
                                <?= $this->menus->nom ?>
                            </h5>                    
                        </div>
                        <div class="ibox-content">
                            <p><?= $this->ln->txt('admin-edition', 'expli-ordre-lien', $this->language, 'Pour trier les liens vous pouvez les déplacer avec votre souris.') ?></p>
                            <div class="dd" id="nestable<?= $this->menus->id_menu ?>">
                                <ol class="dd-list">
                                    <?php foreach ($this->lLiens as $key => $lien) { ?>
                                        <li class="dd-item" data-id="<?= $lien['id'] ?>">
                                            <div class="dd-handle">
                                                <span class="label label-primary"><i class="fa fa-arrows"></i></span>&nbsp;&nbsp;
                                                <strong><?= $lien['nom'] ?></strong>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>
                            <?= $this->ln->txt('admin-edition', 'liste-liens-menu', $this->language, 'Liste des liens du menu :') ?> 
                            <?= $this->menus->nom ?>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<script type="text/javascript">        
    <?php if ($this->lLiens->count() > 1) { ?>
        $(document).ready(function () {
            var updateOutput<?= $this->menus->id_menu ?> = function (e) {
                var list<?= $this->menus->id_menu ?> = e.length ? e : $(e.target);

                if (window.JSON) {
                    $.post('<?= $this->lurl ?>/edition/orderLiens', {id_langue:'<?= $this->language ?>', orderLiens: window.JSON.stringify(list<?= $this->menus->id_menu ?>.nestable('serialize'))});
                    return false;
                } else {
                    alert('<?= $this->ln->txt('admin-alerte', 'alert-fonctionnalite', $this->language, 'Votre navigateur ne supporte pas cette fonctionnalité !') ?>');
                }
            };

            $('#nestable<?= $this->menus->id_menu ?>').nestable({maxDepth: 1}).on('change', updateOutput<?= $this->menus->id_menu ?>);
        });
    <?php } ?>    
</script>