<label class="col-sm-2 control-label" for="language"><?= $this->ln->txt('admin-folders', 'form-folder-lab-language', $this->language, 'Language') ?></label>
<div class="col-sm-10">
    <select id="folder-language" name="folder-language[]"
            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-language', $this->language, 'Language') ?>"
            class="chosen-select  form-control" multiple="multiple">
        <?php
        foreach($this->languages as $lang){
            ?>
            <option value="<?= $lang->id_language?>"
                    <?=($_POST['id_language'] == $lang->id_language?'selected':'')?>>
                <?= $lang->name ?>
            </option>
            <?php
        }?>
    </select>
</div>