<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <title><?= $this->ln->txt('admin-generic', 'admin-site', $this->language, 'Administration du site') ?> <?= $_SESSION['infosSite']['nom'] ?></title>
    <?php if (!empty($this->clSettings->getParam('favicon', 'sets'))) { ?>
        <link rel="shortcut icon" href="<?= $this->surl ?>/var/uploads/params/<?= str_replace(array(
            '.png',
            '.PNG',
        ), array('.ico', '.ico'), $this->clSettings->getParam('favicon', 'sets')) ?>" type="image/x-icon">
        <link rel="icon" href="<?= $this->surl ?>/var/uploads/params/<?= $this->clSettings->getParam('favicon', 'sets') ?>" type="image/png">
    <?php } else { ?>
        <link rel="shortcut icon" href="<?= $this->surl ?>/images/admin/favicon.ico">
    <?php } ?>
    <?= $this->callCss() ?>
    <script type="text/javascript">
        var addSURL = '<?= $this->surl ?>';
        var addLURL = '<?= $this->lurl ?>';
        var addURL = '<?= $this->url ?>';
        var addFRONT = '<?= $this->urlfront ?><?= (count($this->lLangues) > 1 ? '/' . $this->language : '') ?>';
        var lngSite = '<?= ($this->language == 'fr' ? 'fr' : 'en') ?>';
    </script>
    <?= $this->callJs() ?>
    <script type="text/javascript">
        $(function () {
            function logAdminUser() {
                $.post('<?= $this->urlfront ?><?= (count($this->lLangues) > 1 ? '/' . $this->language : '') ?>/logAdminUser', {
                    email: '<?= $_SESSION['user']['email'] ?>',
                    hash: '<?= $_SESSION['user']['hash'] ?>'
                });
            }
        });
    </script>
    <?php if (!empty($_SESSION['msgToast']['title']) && !empty($_SESSION['msgToast']['msg'])) { ?>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 3000
                    };
                    toastr.success("<?= $_SESSION['msgToast']['msg'] ?>", "<?= $_SESSION['msgToast']['title'] ?>");
                }, 1300);
            });
        </script>
    <?php } ?>
</head>
<body>
<div id="wrapper">
