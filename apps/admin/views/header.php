<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="<?= $this->ln->txt('admin-login', 'titre', $this->language, 'Administration du site') ?>" src="<?= $this->surl ?>/assets/images/logo.png" style="width: 169px;"/>
                    </span>
                    <a style="cursor: default;">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold"><?= $_SESSION['user']['firstname'] . ' ' . $_SESSION['user']['name'] ?></strong>
                            </span>
                        </span>
                    </a>
                </div>
                <div class="logo-element">
                    <img alt="<?= $this->ln->txt('admin-login', 'titre', $this->language, 'Administration du site') ?>" src="<?= $this->surl ?>/images/admin/logo_left_mini.png"/>
                </div>
            </li>
            <?php
            $navigation = '';
            $afficheSearch = false;

            foreach ($this->lZonesHeader as $key => $menu) {
                $zones = new o\zones($menu['id_zone']);
                $afficheSearch = ($zones->recherche == 1 ? true : $afficheSearch);

                $navigation .= '
                <li' . ($this->menuActive == $menu['checksecure'] ? ' class="active"' : '') . '>
                    <a href="' . $this->lurl . '/' . $menu['slug_destination'] . '" title="' . $this->ln->txt('admin-navigation', mb_strtolower($menu['name'], 'UTF-8'), $this->language, $menu['name']) . '">
                        <i class="fa ' . $menu['css'] . '"></i> 
                        <span class="nav-label">' . $this->ln->txt('admin-navigation', mb_strtolower($menu['name'], 'UTF-8'), $this->language, $menu['name']) . '</span> 
                        ' . (!empty($menu['childs']) && count($menu['childs']) > 0 ? '<span class="fa arrow"></span>' : '') . '
                    </a>';

                if (!empty($menu['childs']) && count($menu['childs']) > 0) {
                    $navigation .= '<ul class="nav nav-second-level">';

                    foreach ($menu['childs'] as $sskey => $ssmenu) {
                        $zones = new o\zones($ssmenu['id_zone']);
                        $afficheSearch = ($zones->recherche == 1 ? true : $afficheSearch);

                        $navigation .= '
                        <li' . ($this->ssMenuActive == $ssmenu['checksecure'] ? ' class="active"' : '') . '>
                            <a href="' . $this->lurl . '/' . $ssmenu['slug_destination'] . '" title="' . $this->ln->txt('admin-navigation', mb_strtolower($ssmenu['name'], 'UTF-8'), $this->language, $ssmenu['name']) . '">
                                ' . $this->ln->txt('admin-navigation', mb_strtolower($ssmenu['name'], 'UTF-8'), $this->language, $ssmenu['name']) . '
                            </a>
                        </li>';
                    }

                    $navigation .= '</ul>';
                }

                $navigation .= '</li>';
            }

            echo $navigation;
            ?>
        </ul>
    </div>
</nav>
<div id="page-wrapper" class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-minimalize minimalize-styl-2 btn btn-primary"><i class="fa fa-bars"></i></a>
                <?php if ($afficheSearch) { ?>
                    <form role="search" class="navbar-form-custom" method="post" name="formSearch" id="formSearch" action="<?= $this->lurl ?>/recherche">
                        <div class="form-group">
                            <input type="text" placeholder="<?= $this->ln->txt('admin-generic', 'recherche', $this->language, 'Recherche...') ?>" class="form-control" name="search" id="search">
                        </div>
                    </form>
                <?php } ?>
            </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message"><?= $this->ln->txt('admin-generic', 'admin-site', $this->language, 'Administration du site') ?> <?= $_SESSION['infosSite']['nom'] ?></span>
                </li>
                <?php if (isset($_SESSION['user']) && $_SESSION['user']['debug'] == 1 && $this->autoFireDebug && !$this->autoFireNothing) { ?>
                    <li>
                        <a class="count-info" onclick="barreDebug();" title="<?= $this->ln->txt('admin-generic', 'barredebug', $this->language, 'Afficher le débug') ?>">
                            <i class="fa fa-medkit"></i><?= ($this->nbErreurs > 0 ? ' <span class="badge badge-danger">' . $this->nbErreurs . '</span>' : '') ?>
                        </a>
                    </li>
                <?php } ?>
                <?php
                if (count($this->lLangues) > 1) {
                    foreach ($this->lLangues as $key => $langue) {
                        if ($key != $this->language) { ?>
                            <li>
                                <a href="<?= $this->changeLanguage($key, $this->language) ?>" title="<?= $langue ?>">
                                    <img src="<?= $this->surl ?>/images/admin/flags/<?= $key ?>.png" alt="<?= $langue ?>"/>
                                </a>
                            </li>
                        <?php }
                    }
                }
                ?>

                <?php if (count($this->lSitesUser) > 1) { ?>
                    <li>
                        <a href="<?= $this->lurl ?>/choiceSite" title="<?= $this->ln->txt('admin-generic', 'changesite', $this->language, 'Changer de site') ?>">
                            <i class="fa fa-exchange"></i> <?= $this->ln->txt('admin-generic', 'changesite', $this->language, 'Changer de site') ?>
                        </a>
                    </li>
                <?php } ?>
                <li>
                    <a href="<?= $this->lurl ?>/logout" title="<?= $this->ln->txt('admin-generic', 'logout', $this->language, 'Se déconnecter') ?>">
                        <i class="fa fa-sign-out"></i> <?= $this->ln->txt('admin-generic', 'logout', $this->language, 'Se déconnecter') ?>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
