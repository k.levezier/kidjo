<div class="wrapper wrapper-content animated fadeInRight">
    <div id="contenu" class="bdd_view_block">
        <div class="legend_wrapper">
            <div class="bdd_legend">
                <div class="table-cell">
                    <h2>
                        <i class="fa fa-database"></i>&nbsp;&nbsp;<?= $this->ln->txt('admin-eq', 'db-view', $this->language, 'DB View') ?>
                    </h2>
                    <span class="text-primary"><i class="fa fa-indent"></i>&nbsp;&nbsp;<?= $this->ln->txt('admin-eq', 'champs-de-la-base', $this->language, 'Champs de la base') ?></span><br/>
                    <span class="text-success"><i class="fa fa-key"></i>&nbsp;&nbsp;<?= $this->ln->txt('admin-eq', 'clefs-etrang', $this->language, 'Clés étrangères') ?></span><br/>
                    <span class="text-danger"><i class="fa fa-gears"></i>&nbsp;&nbsp;<?= $this->ln->txt('admin-eq', 'fonctions', $this->language, 'Fonctions') ?></span>
                </div>
            </div>
        </div>
        <br class="clearfix"/>
        <ul style="margin-left: -40px;">
            <?php
            foreach ($this->dbListTables as $table) {
                $data = new o\data($table);
                $functions = '';
                $i = 0;

                foreach (get_class_methods('o\\' . $table) as $function) {
                    if ($function == '__construct') {
                        break;
                    }
                    $reflexion = new ReflectionMethod('o\\' . $table, $function);
                    $doc = $reflexion->getDocComment();
                    $cleanDoc = '';
                    if ($doc !== false) {
                        $params = array();
                    }

                    foreach (explode('*', $doc) as $line) {
                        $cleanLine = trim($line);
                        if (trim($line) != '' && notFound(trim($line), array(
                                '/',
                                '@param',
                                '@return',
                                '@function',
                            ), 1)) {
                            $cleanDoc .= ' ' . $cleanLine;
                        }
                        if (strpos(trim($line), '@param') !== false || strpos(trim($line), '@return') !== false) {
                            $params[] = '<li>' . trim($line) . '</li>';
                        }
                        $paramsList = '';
                        if (count($params) > 0) {
                            $paramsList = '<ul class="listParams dnone">';
                            foreach ($params as $param) {
                                $paramsList .= $param;
                            }
                            $paramsList .= '</ul>';
                        }
                    }
                    $i++;

                    $functions .= '<li class="text-danger col-sm-12"' . ($i == 1 ? ' style="margin-top:15px;"' : '') . '><span class="link_params">' . $function . '()&nbsp;:&nbsp;' . $cleanDoc . '</span>' . ($paramsList) . '</li>';
                }
                ?>
                <li class="table_name_link col-sm-12">
                <span class="table_name_span">
                    <i class="fa fa-list-alt"></i>&nbsp;&nbsp;<?= $table ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span class="label-primary"><i class="fa fa-indent"></i>&nbsp;&nbsp;<?= count($data->desc()) ?></span>&nbsp;&nbsp;&nbsp;
                    <span class="label-success"><i class="fa fa-key"></i>&nbsp;&nbsp;<?= count($data->getCles()) ?></span>&nbsp;&nbsp;&nbsp;
                    <span class="label-danger"><i class="fa fa-gears"></i>&nbsp;<?= $i ?></span>&nbsp;&nbsp;&nbsp;
                    <span class="right-expand"><i class="fa fa-expand"></i>&nbsp;</span>
                </span>
                    <ul class="main-list col-sm-12">
                        <?php
                        foreach ($data->desc() as $k => $fields) {
                            echo '<li class="text-primary col-sm-12">' . $fields['Field'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="whitecolor">' . strtoupper($fields['Type']) . '</span></li>';
                        }
                        $nbclefs = 0;
                        foreach ($data->getCles() as $name => $keyDesc) {
                            if ($keyDesc['create_type'] == 'many') {
                                $keyDesc['keys'] = array_flip($keyDesc['keys']);
                            }
                            $keys = implode(', ', array_map(
                                function ($key, $value) {
                                    return $key . ' = ' . $value;
                                }, array_keys($keyDesc['keys']), array_values($keyDesc['keys'])));
                            echo '
                        <li class="text-success col-sm-12"' . ($nbclefs == 0 ? ' style="margin-top:15px;"' : '') . '>'
                                . $name . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="whitecolor">' . ($keyDesc['create_type'] == 'one' ? 'new o\\' . $keyDesc['table'] . '(' . $keys . ')' : "new o\\data('" . $keyDesc['table'] . "',array($keys))") . '</span>
                        </li>';
                            $nbclefs++;
                        }
                        echo $functions;
                        ?>
                    </ul>
                </li>
            <?php } ?>
        </ul>
        <br class="clearfix"/>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.table_name_span', function () {
        $(this).parent().find('.main-list').toggle('height');
        $(this).find('.right-expand').find('i').toggleClass('fa-expand').toggleClass('fa-compress');
    });
    $(document).on('click', '.link_params', function () {
        $(this).next('ul').toggle('height');
    });
</script>
