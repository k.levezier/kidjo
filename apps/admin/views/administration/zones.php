<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-administration', 'administration-zones', $this->language, 'Administration des zones') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', 'zones', $this->language, 'Zones') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/administration/formZone" class="btn btn-primary"><?= $this->ln->txt('admin-administration', 'add-zone', $this->language, 'Ajouter une zone') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', 'liste-zones', $this->language, 'Liste des zones du BO') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="project-list">
                        <?php if (count($this->lZonesHeader) > 0) { ?>
                            <table class="table table-hover vertical-center">
                                <tbody>
                                    <?php foreach ($this->lZonesHeader as $key => $menu) { ?>
                                        <tr>
                                            <td class="project-status">
                                                <span class="label label-primary"><i class="fa <?= $menu['css'] ?>"></i></span>
                                            </td>
                                            <td class="project-title tooltip-demo">
                                                <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'utilisation', $this->language, 'Utilisation :') ?> $this->users->checkAccess('<?= $menu['checksecure'] ?>');"><?= $this->ln->txt('admin-navigation', mb_strtolower($menu['name'], 'UTF-8'), $this->language, $menu['name']) ?></strong>
                                            </td>
                                            <td class="project-actions">
                                                <?php if (!empty($menu['childs']) && count($menu['childs']) > 0) { ?>
                                                    <a onClick="afficheZones('<?= $menu['checksecure'] ?>', '<?= count($menu['childs']) ?>');" class="btn btn-white btn-sm">
                                                        <i class="fa fa-folder"></i> <?= $this->ln->txt('admin-generic', 'details', $this->language, 'Détails') ?>
                                                    </a>
                                                <?php } ?>
                                                <a href="<?= $this->lurl . '/administration/formZone/' . $menu['id_zone'] ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php if (!empty($menu['childs']) && count($menu['childs']) > 0) { ?>
                                            <?php foreach ($menu['childs'] as $sskey => $ssmenu) { ?>
                                                <tr id="<?= $menu['checksecure'] . $sskey ?>" style="display:none;">
                                                    <td class="project-status">
                                                        <span class="label label-warning"><i class="fa <?= $menu['css'] ?>"></i></span>
                                                    </td>
                                                    <td class="project-title tooltip-demo">
                                                        <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'utilisation', $this->language, 'Utilisation :') ?> $this->users->checkAccess('<?= $ssmenu['checksecure'] ?>');"><?= $this->ln->txt('admin-navigation', mb_strtolower($ssmenu['name'], 'UTF-8'), $this->language, $ssmenu['name']) ?></strong>
                                                    </td>
                                                    <td class="project-actions">
                                                        <a href="<?= $this->lurl . '/administration/formZone/' . $ssmenu['id_zone'] ?>" class="btn btn-white btn-sm">
                                                            <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', 'ordre-zones', $this->language, 'Ordonnancement des zones') ?></h5>
                </div>
                <div class="ibox-content">
                    <p><?= $this->ln->txt('admin-administration', 'expli-ordre-zones', $this->language, 'Pour trier les zones vous pouvez les déplacer avec votre souris.') ?></p>
                    <?php if (count($this->lZonesHeader) > 0) { ?>
                        <div class="dd" id="nestable">
                            <ol class="dd-list">
                                <?php foreach ($this->lZonesHeader as $key => $menu) { ?>
                                    <li class="dd-item" data-id="<?= $menu['id'] ?>">
                                        <div class="dd-handle">
                                            <span class="label label-primary"><i class="fa <?= $menu['css'] ?>"></i></span>&nbsp;&nbsp;
                                            <?= $this->ln->txt('admin-navigation', mb_strtolower($menu['name'], 'UTF-8'), $this->language, $menu['name']) ?>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ol>
                        </div>
                    <?php } else { ?>
                        <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target);

            if (window.JSON) {
                $.post('<?= $this->lurl ?>/administration/orderZones', {orderZones: window.JSON.stringify(list.nestable('serialize'))});
                return false;
            } else {
                alert('<?= $this->ln->txt('admin-alerte', 'alert-fonctionnalite', $this->language, 'Votre navigateur ne supporte pas cette fonctionnalité !') ?>');
            }
        };

        $('#nestable').nestable({maxDepth: 1}).on('change', updateOutput);
    });

    $(document).on('click', '.popover-content', function () {
        var _this = $(this);
        var node = $(this).get(0);
        var text = $(this).text().toString();
        if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.setStart(node.firstChild, text.indexOf(':') + 2);
            range.setEnd(node.firstChild, text.length);
            selection.removeAllRanges();
            selection.addRange(range);
            document.execCommand("copy");
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 3000
            };
            toastr.success('<?= $this->ln->txt('admin-generic', 'fnc-copied', $this->language, 'Fonction copiée') ?>', '<?= $this->ln->txt('admin-generic', 'clipboard', $this->language, 'Presse-papier') ?>');
            setTimeout(function () {
                _this.parent().fadeOut();
            }, 1500);
        }
    });

    function afficheZones(name, nb) {
        for (i = 0; i < nb; i++) {
            if ($('#' + name + i).css('display') === 'none') {
                $('#' + name + i).show();
            } else {
                $('#' + name + i).hide();
            }
        }
    }
</script> 
