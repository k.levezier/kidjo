<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-administration', 'administration-users', $this->language, 'Gestion des administrateurs') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', 'users', $this->language, 'Administrateurs') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/administration/formUser" class="btn btn-primary"><?= $this->ln->txt('admin-administration', 'add-user', $this->language, 'Ajouter un administrateur') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', 'liste-users', $this->language, 'Liste des administrateurs') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form name="filtresTab" id="filtresTab" method="POST">
                            <div class="col-sm-3 m-b-xs">
                                <select class="input-sm form-control input-s-sm inline" name="id_site" onchange="$('#filtresTab').submit();">
                                    <option value=""><?= $this->ln->txt('admin-administration', 'filtre-par-site', $this->language, 'Filtre par site') ?></option>
                                    <?php foreach ($this->lSitesUser as $key => $site) { ?>
                                        <option value="<?= $site['id_site'] ?>"<?= (isset($_POST['id_site']) && $_POST['id_site'] == $site['id_site'] ? ' selected' : '') ?>><?= $site['nom'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-6">&nbsp;</div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" placeholder="<?= $this->ln->txt('admin-generic', 'recherche', $this->language, 'Recherche...') ?>" class="input-sm form-control" name="filtre" value="<?= $_POST['filtre'] ?>">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary" name="btnSendForm"> <?= $this->ln->txt('admin-generic', 'ok', $this->language, 'OK') ?></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <?php if (count($this->users_sites) > 0) { ?>
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th><?= $this->ln->txt('admin-administration', 'tab-nom', $this->language, 'Nom') ?></th>
                                        <th><?= $this->ln->txt('admin-administration', 'tab-prenom', $this->language, 'Prénom') ?></th>
                                        <th><?= $this->ln->txt('admin-administration', 'tab-email', $this->language, 'Adresse email') ?></th>
                                        <th><?= $this->ln->txt('admin-administration', 'tab-lastlogin', $this->language, 'Dernière connexion') ?></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->users_sites->order('name', 'ASC', 'users') as $key => $list) { ?>
                                        <tr>
                                            <td><?= $list->users->name ?></td>
                                            <td><?= $list->users->firstname ?></td>
                                            <td><?= $list->users->email ?></td>
                                            <td><?= ($list->users->lastlogin != '0000-00-00 00:00:00' ? date('d/m/Y', strtotime($list->users->lastlogin)) . ' ' . $this->ln->txt('admin-generic', 'agrave', $this->language, 'à') . ' ' . date('H:i:s', strtotime($list->users->lastlogin)) : $this->ln->txt('admin-generic', 'jamais', $this->language, 'Jamais')) ?></td>
                                            <td <?= ($list->users->status < 2 ? 'class="col-sm-2"' : 'class="col-sm-1"') ?>>
                                                <?php if ($list->users->status < 2) { ?>
                                                    <a href="<?= $this->lurl ?>/administration/users/changeStatus___<?= $list->users->hash ?>/newStatus___<?= ($list->users->status == 0 ? 1 : 0) ?>" class="btn btn-white btn-sm">
                                                        <i class="fa <?= ($list->users->status == 0 ? 'fa-times-circle-o text-danger' : 'fa-check-circle-o text-navy') ?>"></i>
                                                        <?= $this->ln->txt('admin-generic', ($list->users->status == 0 ? 'activer' : 'desactiver'), $this->language, ($list->users->status == 0 ? 'Activer' : 'Désactiver')) ?>
                                                    </a>
                                                <?php } ?>
                                                <a href="<?= $this->lurl ?>/administration/formUser/<?= $list->users->hash ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <p>&nbsp;</p>
                            <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
