<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-administration', 'administration-users', $this->language, 'Gestion des administrateurs') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/administration/users"><?= $this->ln->txt('admin-administration', 'users', $this->language, 'Administrateurs') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', ($this->newUser ? 'add-user' : 'edit-user'), $this->language, ($this->newUser ? 'Ajouter un administrateur' : 'Modifier un administrateur')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/administration/users" class="btn btn-primary">
                <?= $this->ln->txt('admin-administration', 'back-users', $this->language, 'Retour aux administrateurs') ?>
            </a>
            <?php if (!$this->newUser) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-administration', 'delete-users', $this->language, 'Supprimer l\'administrateur') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="POST" class="form-horizontal" name="formUser" id="formUser">
            <div class="col-lg-<?= (!$this->newUser ? 8 : 12) ?>">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-administration', ($this->newUser ? 'form-add-user' : 'form-edit-user'), $this->language, ($this->newUser ? 'Formulaire d\'ajout de l\'administrateur' : 'Formulaire de modification de l\'administrateur')) ?></h5>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" name="sendFormUser"/>
                        <input type="hidden" name="restePage" id="restePage" value="0"/>
                        <input type="hidden" name="hash" value="<?= $_POST['hash'] ?>"/>
                        <div class="form-group<?= (isset($_POST['sendFormUser']) && $this->errorFormNom ? ' has-error' : '') ?>">
                            <label class="col-sm-3 control-label" for="name"><?= $this->ln->txt('admin-administration', 'form-user-lab-nom', $this->language, 'Nom') ?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $_POST['name'] ?>">
                                <?= (isset($_POST['sendFormUser']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormUser']) && $this->errorFormPrenom ? ' has-error' : '') ?>">
                            <label class="col-sm-3 control-label" for="firstname"><?= $this->ln->txt('admin-administration', 'form-user-lab-prenom', $this->language, 'Prénom') ?></label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="firstname" id="firstname" value="<?= $_POST['firstname'] ?>">
                                <?= (isset($_POST['sendFormUser']) && $this->errorFormPrenom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormUser']) && $this->errorFormEmail ? ' has-error' : '') ?>">
                            <label class="col-sm-3 control-label" for="email"><?= $this->ln->txt('admin-administration', 'form-user-lab-email', $this->language, 'Adresse Email') ?></label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" name="email" id="email" value="<?= $_POST['email'] ?>">
                                <?= (isset($_POST['sendFormUser']) && $this->errorFormEmail ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormUser']) && $this->errorFormPassword ? ' has-error' : '') ?>">
                            <label class="col-sm-3 control-label" for="password"><?= $this->ln->txt('admin-administration', 'form-user-lab-password', $this->language, 'Mot de passe') ?></label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" name="password" id="password" value="<?= (isset($_POST['sendFormUser']) && $this->newUser ? $_POST['password'] : (isset($_POST['sendFormUser']) && strlen($_POST['password']) > 0 ? $_POST['password'] : '')) ?>" autocomplete="OFF">
                                <?= (isset($_POST['sendFormUser']) && $this->errorFormPassword ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="id_tree"><?= $this->ln->txt('admin-administration', 'form-user-lab-id_tree', $this->language, 'Blocage site') ?></label>
                            <div class="col-sm-9">
                                <select class="form-control" name="id_tree" id="id_tree">
                                    <option value="0"><?= $this->ln->txt('admin-administration', 'form-user-choix-blocage', $this->language, 'Choisir une rubrique') ?></option>
                                    <?= $this->clTemplates->getArboSelect($this->lPages, $_POST['id_tree']) ?>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="debug"><?= $this->ln->txt('admin-administration', 'form-user-lab-debug', $this->language, 'Activer le débug') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="debug" id="debug" value="1"<?= ($_POST['debug'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormUser']) && $this->errorFormSites ? ' has-error' : '') ?>">
                            <label class="col-sm-3 control-label" for="id_site"><?= $this->ln->txt('admin-administration', 'form-user-lab-site', $this->language, 'Attribuer des sites') ?></label>
                            <div class="col-sm-9">
                                <select data-placeholder="<?= $this->ln->txt('admin-administration', 'form-user-lab-select-site', $this->language, 'Sélectionner les sites') ?>" class="chosen-select col-sm-12" name="id_site[]" id="id_site" multiple>
                                    <?php foreach ($this->lSites->order('nom', 'ASC') as $site) { ?>
                                        <option value="<?= $site->id_site ?>"<?= (in_array($site->id_site, $_POST['id_site']) ? ' selected' : '') ?>><?= $site->nom ?></option>
                                    <?php } ?>
                                </select>
                                <?= (isset($_POST['sendFormUser']) && $this->errorFormSites ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-3">
                                <a href="<?= $this->lurl ?>/administration/users" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (!$this->newUser) { ?>
                <div class="col-lg-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?= $this->ln->txt('admin-administration', 'form-zone-user', $this->language, 'Liste des zones') ?></h5>
                        </div>
                        <div class="ibox-content">
                            <div class="table-responsive">
                                <p>
                                    <strong class="text-danger"><?= $this->ln->txt('admin-generic', 'attention', $this->language, 'Attention !') ?></strong>
                                    <?= $this->ln->txt('admin-administration', 'form-zone-alert-user', $this->language, 'Au choix de la zone cette dernière est automatiquement ajoutée ou enlevée à l\'administrateur.') ?>
                                </p>
                                <table class="table table-striped table-hover vertical-center">
                                    <tbody>
                                        <?php
                                        foreach ($this->lZones->order('name', 'ASC') as $zone) {
                                            $lssZones = new o\data('zones', array('id_parent' => $zone->id_zone));
                                            ?>
                                            <tr>
                                                <td>
                                                    <input type="checkbox" class="i-checks idZone" name="id_zone[]" value="<?= $zone->id_zone ?>"<?= (in_array($zone->id_zone, $_POST['id_zone']) ? ' checked' : '') ?>>
                                                </td>
                                                <td colspan="2"><strong><?= $zone->name ?></strong></td>
                                            </tr>
                                            <?php foreach ($lssZones->order('name', 'ASC') as $sszone) { ?>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <input type="checkbox" class="i-checks idZone" name="id_zone[]" value="<?= $sszone->id_zone ?>"<?= (in_array($sszone->id_zone, $_POST['id_zone']) ? ' checked' : '') ?>>
                                                    </td>
                                                    <td><?= $sszone->name ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-users-alert', $this->language, 'Confirmez vous la suppression de l\'administrateur ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/administration/deleteUser/<?= $this->uform->hash ?>');
                        return false;
                    }
                });
        });
        <?php if (!$this->newUser) { ?>
        $('.i-checks.idZone').on('ifToggled', function () {
            $.post('<?= $this->lurl ?>/administration/userZones', {
                id_zone: this.value,
                id_user: <?= $_POST['id_user'] ?>});
            return false;
        });
        <?php } ?>
    });

    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: '<?= $this->ln->txt('admin-generic', 'no-result', $this->language, 'Aucun résultat') ?>'},
        '.chosen-select-width': {width: "95%"}
    };

    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>  
