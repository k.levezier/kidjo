<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-administration', 'administration-sites', $this->language, 'Administration des sites') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', 'sites', $this->language, 'Sites') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/administration/formSite" class="btn btn-primary"><?= $this->ln->txt('admin-administration', 'add-site', $this->language, 'Ajouter un site') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', 'liste-sites', $this->language, 'Liste des sites disponibles') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="project-list">
                        <?php if (count($this->lSitesUser) > 0) { ?>
                            <table class="table table-hover">
                                <tbody>
                                    <?php foreach ($this->lSitesUser as $key => $site) { ?>
                                        <tr>
                                            <td class="project-status">
                                                <span class="label label-primary"><i class="fa fa-desktop"></i></span>
                                            </td>
                                            <td class="project-title">
                                                <a href="<?= $this->lurl . '/administration/formSite/' . $site['id_site'] ?>"><?= $site['nom'] ?></a>
                                            </td>
                                            <td class="project-actions">
                                                <a href="<?= $this->lurl . '/administration/formSite/' . $site['id_site'] ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', 'ordre-sites', $this->language, 'Ordonnancement des sites') ?></h5>
                </div>
                <div class="ibox-content">
                    <p><?= $this->ln->txt('admin-administration', 'expli-ordre-sites', $this->language, 'Pour trier les sites vous pouvez les déplacer avec votre souris.') ?></p>
                    <?php if (count($this->lSitesUser) > 0) { ?>
                        <div class="dd" id="nestable">
                            <ol class="dd-list">
                                <?php foreach ($this->lSitesUser as $key => $site) { ?>
                                    <li class="dd-item" data-id="<?= $site['id'] ?>">
                                        <div class="dd-handle">
                                            <span class="label label-primary"><i class="fa fa-desktop"></i></span>&nbsp;&nbsp;
                                            <?= $site['nom'] ?>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ol>
                        </div>
                    <?php } else { ?>
                        <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target);

            if (window.JSON) {
                $.post('<?= $this->lurl ?>/administration/orderSites', {orderSites: window.JSON.stringify(list.nestable('serialize'))});
                return false;
            } else {
                alert('<?= $this->ln->txt('admin-alerte', 'alert-fonctionnalite', $this->language, 'Votre navigateur ne supporte pas cette fonctionnalité !') ?>');
            }
        };

        $('#nestable').nestable({maxDepth: 1}).on('change', updateOutput);
    });
</script> 
