<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-administration', 'administration-logs', $this->language, 'Enregistrements du BO') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', 'administration-logs', $this->language, 'Enregistrements du BO') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <?php if (count($this->logs) > 0) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-administration', 'delete-logs', $this->language, 'Supprimer les enregistrements') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', 'liste-logs', $this->language, 'Liste des enregistrements') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form name="filtresTab" id="filtresTab" method="POST">
                            <div class="col-sm-9">&nbsp;</div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" placeholder="<?= $this->ln->txt('admin-generic', 'recherche', $this->language, 'Recherche...') ?>" class="input-sm form-control" name="filtre" value="<?= $_POST['filtre'] ?>">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary" name="btnSendForm"> <?= $this->ln->txt('admin-generic', 'ok', $this->language, 'OK') ?></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <?php if (count($this->logs) > 0) { ?>
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-sm-3"><?= $this->ln->txt('admin-administration', 'tab-date', $this->language, 'Date') ?></th>
                                        <th class="col-sm-2"><?= $this->ln->txt('admin-administration', 'tab-administrateur', $this->language, 'Administrateur') ?></th>
                                        <th class="col-sm-3"><?= $this->ln->txt('admin-administration', 'tab-action', $this->language, 'Actions') ?></th>
                                        <th class="col-sm-3"><?= $this->ln->txt('admin-administration', 'tab-details', $this->language, 'Détails') ?></th>
                                        <th class="col-sm-1">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->logs->order('added', 'DESC') as $key => $logs) { ?>
                                        <tr>
                                            <td><?= date('d/m/Y', strtotime($logs->added)) . ' ' . $this->ln->txt('admin-generic', 'agrave', $this->language, 'à') . ' ' . date('H:i:s', strtotime($logs->added)) ?></td>
                                            <td><?= $logs->users->firstname . ' ' . $logs->users->name ?></td>
                                            <td><?= $logs->action ?></td>
                                            <td><?= $this->ficelle->resumeBrut($logs->details, 50) ?></td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/administration/detailLog/<?= $logs->id_log ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-eye"></i> <?= $this->ln->txt('admin-generic', 'voir', $this->language, 'Voir') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <p>&nbsp;</p>
                            <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-logs-alert', $this->language, 'Confirmez vous la suppression de tous les enregistrements de ce site ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/administration/deleteLogs');
                        return false;
                    }
                });
        });
    });
</script> 
