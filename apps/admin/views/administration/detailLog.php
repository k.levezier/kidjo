<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-administration', 'administration-logs', $this->language, 'Enregistrements du BO') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/administration/logs"><?= $this->ln->txt('admin-administration', 'administration-logs', $this->language, 'Enregistrements du BO') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', 'details-logs', $this->language, 'Détails de l\'enregistrement') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/administration/logs" class="btn btn-primary">
                <?= $this->ln->txt('admin-administration', 'back-logs', $this->language, 'Retour aux enregistrements') ?>
            </a>
            <a class="btn btn-danger confirm">
                <?= $this->ln->txt('admin-administration', 'delete-log', $this->language, 'Supprimer l\'enregistrement') ?>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="m-b-md">
                                <h2><?= $this->logs->action ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <dl class="dl-horizontal">
                                <dt><?= $this->ln->txt('admin-administration', 'tab-administrateur', $this->language, 'Administrateur') ?>
                                    :
                                </dt>
                                <dd><?= $this->logs->users->firstname . ' ' . $this->logs->users->name ?></dd>
                                <dt><?= $this->ln->txt('admin-administration', 'tab-ip', $this->language, 'Adresse IP') ?>
                                    :
                                </dt>
                                <dd><?= $this->logs->ip ?></dd>
                            </dl>
                        </div>
                        <div class="col-lg-7" id="cluster_info">
                            <dl class="dl-horizontal">
                                <dt><?= $this->ln->txt('admin-administration', 'tab-date', $this->language, 'Date') ?>
                                    :
                                </dt>
                                <dd><?= date('d/m/Y', strtotime($this->logs->added)) . ' ' . $this->ln->txt('admin-generic', 'agrave', $this->language, 'à') . ' ' . date('H:i:s', strtotime($this->logs->added)) ?></dd>
                            </dl>
                        </div>
                    </div>
                    <?php if (!empty($this->logs->details) || !empty($this->logs->complement)) { ?>
                        <div class="row m-t-sm">
                            <div class="col-lg-12">
                                <div class="panel blank-panel">
                                    <div class="panel-heading">
                                        <div class="panel-options">
                                            <ul class="nav nav-tabs">
                                                <?php if (!empty($this->logs->details)) { ?>
                                                    <li class="active">
                                                        <a href="#tab-1" data-toggle="tab"><?= $this->ln->txt('admin-administration', 'tab-details', $this->language, 'Détails') ?></a>
                                                    </li>
                                                <?php } ?>
                                                <?php if (!empty($this->logs->complement)) { ?>
                                                    <li<?= ($this->logs->details == '' ? ' class="active"' : '') ?>>
                                                        <a href="#tab-2" data-toggle="tab"><?= $this->ln->txt('admin-administration', 'tab-complement', $this->language, 'Compléments') ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab-1">
                                                <div class="feed-activity-list">
                                                    <div class="feed-element">
                                                        <div class="media-body">
                                                            <div class="well wordwrap"><?= $this->logs->details ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="tab-2">
                                                <div class="feed-activity-list">
                                                    <div class="feed-element">
                                                        <div class="media-body">
                                                            <div class="well wordwrap"><?= $this->logs->complement ?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-log-alert', $this->language, 'Confirmez vous la suppression de l\'enregistrement ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/administration/deleteLog/<?= $this->logs->id_log ?>');
                        return false;
                    }
                });
        });
    });
</script>  
