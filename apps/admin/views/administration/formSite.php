<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $this->ln->txt('admin-administration', 'administration-sites', $this->language, 'Administration des sites') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/administration/sites"><?= $this->ln->txt('admin-administration', 'sites', $this->language, 'Sites') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', ($this->newSite ? 'add-site' : 'edit-site'), $this->language, ($this->newSite ? 'Ajouter un site' : 'Modifier un site')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/administration/sites" class="btn btn-primary">
                <?= $this->ln->txt('admin-administration', 'back-sites', $this->language, 'Retour aux sites') ?>
            </a>
            <?php if (!$this->newSite) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-administration', 'delete-sites', $this->language, 'Supprimer le site') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', ($this->newSite ? 'form-add-site' : 'form-edit-site'), $this->language, ($this->newSite ? 'Formulaire d\'ajout du site' : 'Formulaire de modification du site')) ?></h5>
                </div>
                <div class="ibox-content">
                    <form method="POST" class="form-horizontal" name="formSite" id="formSite">
                        <input type="hidden" name="sendFormSite"/>
                        <input type="hidden" name="restePage" id="restePage" value="0"/>
                        <input type="hidden" name="id_site" value="<?= $_POST['id_site'] ?>"/>
                        <div class="form-group<?= (isset($_POST['sendFormSite']) && $this->errorFormNom ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="nom"><?= $this->ln->txt('admin-administration', 'form-site-lab-nom', $this->language, 'Nom du site') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nom" id="nom" value="<?= $_POST['nom'] ?>">
                                <?= (isset($_POST['sendFormSite']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormSite']) && $this->errorFormApp ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="app"><?= $this->ln->txt('admin-administration', 'form-site-lab-app', $this->language, 'Code du site') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="app" id="app" value="<?= $_POST['app'] ?>">
                                <?= (isset($_POST['sendFormSite']) && $this->errorFormApp ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormSite']) && $this->errorFormBase ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="base"><?= $this->ln->txt('admin-administration', 'form-site-lab-base', $this->language, 'Nom de la BDD') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="base" id="base" value="<?= $_POST['base'] ?>">
                                <?= (isset($_POST['sendFormSite']) && $this->errorFormBase ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormSite']) && $this->errorFormUrl ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="url"><?= $this->ln->txt('admin-administration', 'form-site-lab-url', $this->language, 'URL du site (avec HTTP)') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="url" id="url" value="<?= $_POST['url'] ?>">
                                <?= (isset($_POST['sendFormSite']) && $this->errorFormUrl ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/administration/sites" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-sites-alert', $this->language, 'Confirmez vous la suppression du site ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/administration/deleteSite/<?= $this->sites->id_site ?>');
                        return false;
                    }
                });
        });
    });
</script>  
