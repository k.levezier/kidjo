<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $this->ln->txt('admin-administration', 'administration-zones', $this->language, 'Administration des zones') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/administration/zones"><?= $this->ln->txt('admin-administration', 'zones', $this->language, 'Zones') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-administration', ($this->newZone ? 'add-zone' : 'edit-zone'), $this->language, ($this->newZone ? 'Ajouter une zone' : 'Modifier une zone')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/administration/zones" class="btn btn-primary">
                <?= $this->ln->txt('admin-administration', 'back-zones', $this->language, 'Retour aux zones') ?>
            </a>
            <?php if (!$this->newZone) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-administration', 'delete-zones', $this->language, 'Supprimer la zone') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-administration', ($this->newZone ? 'form-add-zone' : 'form-edit-zone'), $this->language, ($this->newZone ? 'Formulaire d\'ajout de la zone' : 'Formulaire de modification de la zone')) ?></h5>
                </div>
                <div class="ibox-content">
                    <form method="POST" class="form-horizontal" name="formZone" id="formZone">
                        <input type="hidden" name="sendFormZone"/>
                        <input type="hidden" name="restePage" id="restePage" value="0"/>
                        <input type="hidden" name="id_zone" value="<?= $_POST['id_zone'] ?>"/>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="id_parent"><?= $this->ln->txt('admin-administration', 'form-zone-lab-parent', $this->language, 'Zone parente') ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="id_parent" id="id_parent">
                                    <option value="0"><?= $this->ln->txt('admin-administration', 'form-zone-choix-parent', $this->language, 'Choisir une zone parente') ?></option>
                                    <?php foreach ($this->lParents->order('name', 'ASC') as $zone) { ?>
                                        <option value="<?= $zone->id_zone ?>"<?= ($_POST['id_parent'] == $zone->id_zone ? ' selected' : '') ?>><?= $zone->name ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormZone']) && $this->errorFormName ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="name"><?= $this->ln->txt('admin-administration', 'form-zone-lab-nom', $this->language, 'Nom de la zone') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" id="name" value="<?= $_POST['name'] ?>">
                                <?= (isset($_POST['sendFormZone']) && $this->errorFormName ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendFormZone']) && $this->errorFormDestination ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="slug_destination"><?= $this->ln->txt('admin-administration', 'form-zone-lab-slug', $this->language, 'Destination') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="slug_destination" id="slug_destination" value="<?= $_POST['slug_destination'] ?>">
                                <?= (isset($_POST['sendFormZone']) && $this->errorFormDestination ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="css"><?= $this->ln->txt('admin-administration', 'form-zone-lab-css', $this->language, 'Classe CSS picto') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="css" id="css" value="<?= $_POST['css'] ?>">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="recherche"><?= $this->ln->txt('admin-administration', 'form-zone-lab-recherche', $this->language, 'Activer la recherche') ?></label>
                            <div class="col-sm-10">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="recherche" id="recherche" value="1"<?= ($_POST['recherche'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/administration/zones" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-zones-alert', $this->language, 'Confirmez vous la suppression de la zone et de toutes les zones dont elle est le parent ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/administration/deleteZone/<?= $this->zones->id_zone ?>');
                        return false;
                    }
                });
        });
    });
</script>  
