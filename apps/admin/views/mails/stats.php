<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-mails', 'stats', $this->language, 'Statistiques d\'un email transactionnel') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-mails', 'stats', $this->language, 'Statistiques d\'un email transactionnel') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/mails/templates' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-mails', 'back-mails', $this->language, 'Retour aux mails') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">

            <form method="post" enctype="multipart/form-data" class="form-inline pull-right">
                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="checkbox" value="1"
                               name="sent" <?= ($_SESSION['statsMailsSent'] == 1 ? ' checked="checked"' : '') ?>/> <?= $this->ln->txt('admin-mails', 'mails-sent', $this->language, 'Envoyés') ?>
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="checkbox" value="1"
                               name="opened" <?= ($_SESSION['statsMailsOpened'] == 1 ? ' checked="checked"' : '') ?>/> <?= $this->ln->txt('admin-mails', 'mails-opened', $this->language, 'Ouverts') ?>
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="checkbox" value="1"
                               name="clicked" <?= ($_SESSION['statsMailsClicked'] == 1 ? ' checked="checked"' : '') ?>/> <?= $this->ln->txt('admin-mails', 'mails-clicked', $this->language, 'Cliqués') ?>
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox-inline">
                        <input type="checkbox" value="1"
                               name="bounced" <?= ($_SESSION['statsMailsBounced'] == 1 ? ' checked="checked"' : '') ?>/> <?= $this->ln->txt('admin-mails', 'mails-bounced', $this->language, 'Rejetés') ?>
                    </label>
                </div>
                <?php if ($this->email->id_partenaire > 0) { ?>
                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" value="1"
                                   name="orders" <?= ($_SESSION['statsMailsOrders'] == 1 ? ' checked="checked"' : '') ?>/> <?= $this->ln->txt('admin-mails', 'mails-orders', $this->language, 'Commandes') ?>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="checkbox-inline">
                            <input type="checkbox" value="1"
                                   name="ca" <?= ($_SESSION['statsMailsCA'] == 1 ? ' checked="checked"' : '') ?>/> <?= $this->ln->txt('admin-mails', 'mails-ca', $this->language, 'Chiffre d\'affaires') ?>
                        </label>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <input type="text" name="from" placeholder="Date de début" class="form-control datePicker"
                           value="<?= $_SESSION['statsMailsFrom'] ?>"></div>
                <div class="form-group">
                    <input type="text" name="to" placeholder="Date de fin" class="form-control datePicker"
                           value="<?= $_SESSION['statsMailsTo'] ?>"></div>
                <div class="form-group">

                    &nbsp;<input type="submit" name="filter" value="Filtrer" class="btn btn-primary"
                                 style="margin-bottom:0"></div>
            </form>
        </div>
        <div class="col-lg-12">&nbsp;</div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <h5><?= $this->ln->txt('admin-mails', 'mails-sent', $this->language, 'Envoyés') ?></h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $this->stats['totals']['sent'] ?></h1>

                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <h5><?= $this->ln->txt('admin-mails', 'mails-opened', $this->language, 'Ouverts') ?></h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $this->stats['totals']['opened'] ?></h1>
                    <div class="stat-percent font-bold text-success"
                         style="font-size:14px; margin-bottom:5px"><?= (round(100 * $this->stats['totals']['opened'] / $this->stats['totals']['sent'])) ?>
                        %
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <h5><?= $this->ln->txt('admin-mails', 'mails-clicked', $this->language, 'Clickés') ?></h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $this->stats['totals']['clicked'] ?></h1>
                    <div class="stat-percent font-bold text-success"
                         style="font-size:14px; margin-bottom:5px"><?= (round(100 * $this->stats['totals']['clicked'] / $this->stats['totals']['sent'])) ?>
                        %
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <h5><?= $this->ln->txt('admin-mails', 'mails-bounced', $this->language, 'Refusés') ?></h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?= $this->stats['totals']['bounced'] ?></h1>
                    <div class="stat-percent font-bold text-success"
                         style="font-size:14px; margin-bottom:5px"><?= (round(100 * $this->stats['totals']['bounced'] / $this->stats['totals']['sent'])) ?>
                        %
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <h5><?= $this->ln->txt('admin-mails', 'mails-evol', $this->language, 'Evolution') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="flot-chart">
                        <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">

                    <h5><?= $this->ln->txt('admin-mails', 'mails-data', $this->language, 'Données') ?></h5>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th class="col-sm-2"><?= $this->ln->txt('admin-mails', 'mails-day', $this->language, 'Jour') ?></th>
                                <th class="col-sm-2"><?= $this->ln->txt('admin-mails', 'mails-sent', $this->language, 'Envoyés') ?></th>
                                <th class="col-sm-2"><?= $this->ln->txt('admin-mails', 'mails-opened', $this->language, 'Ouverts') ?></th>
                                <th class="col-sm-2"><?= $this->ln->txt('admin-mails', 'mails-clicked', $this->language, 'Cliqués') ?></th>
                                <th class="col-sm-1"><?= $this->ln->txt('admin-mails', 'mails-bounced', $this->language, 'Refusés') ?></th>
                                <?php if ($this->email->id_partenaire > 0) { ?>
                                    <th class="col-sm-1"><?= $this->ln->txt('admin-mails', 'mails-orders', $this->language, 'Commandes') ?></th>
                                    <th class="col-sm-2"><?= $this->ln->txt('admin-mails', 'mails-ca', $this->language, 'CA') ?></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($date = $this->from; $date <= $this->to; $date = strtotime('+1 day', $date)) { ?>
                                <tr>
                                    <td><?= date('d/m/Y', $date) ?></td>
                                    <td><?= (int)$this->stats['data'][date('Y-m-d', $date)]['sent'] ?></td>
                                    <td><?= (int)$this->stats['data'][date('Y-m-d', $date)]['opened'] ?></td>
                                    <td><?= (int)$this->stats['data'][date('Y-m-d', $date)]['clicked'] ?></td>
                                    <td><?= (int)$this->stats['data'][date('Y-m-d', $date)]['bounced'] ?></td>
                                    <?php if ($this->email->id_partenaire > 0) { ?>
                                        <td><?= (int)$this->campStats['data'][date('Y-m-d', $date)]['orders'] ?></td>
                                        <td><?= formatPrix((float)$this->campStats['data'][date('Y-m-d', $date)]['ca']) ?></td>
                                    <?php } ?>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {


        function gd(year, month, day) {
            return new Date(year, month - 1, day).getTime();
        }

        var previousPoint = null, previousLabel = null;
        var SENT = [
                <?php for ($date = $this->from; $date <= $this->to; $date = strtotime('+1 day', $date)) { ?>[gd(<?= date('Y', $date) ?>, <?= (int)date('m', $date) ?>, <?= (int)date('d', $date) ?>), <?= (int)$this->stats['data'][date('Y-m-d', $date)]['sent'] ?>],<?php } ?>
        ];
        var OPENED = [
                <?php for ($date = $this->from; $date <= $this->to; $date = strtotime('+1 day', $date)) { ?>[gd(<?= date('Y', $date) ?>, <?= (int)date('m', $date) ?>, <?= (int)date('d', $date) ?>), <?= (int)$this->stats['data'][date('Y-m-d', $date)]['opened'] ?>],<?php } ?>
        ]
        var CLICKED = [
                <?php for ($date = $this->from; $date <= $this->to; $date = strtotime('+1 day', $date)) { ?>[gd(<?= date('Y', $date) ?>, <?= (int)date('m', $date) ?>, <?= (int)date('d', $date) ?>), <?= (int)$this->stats['data'][date('Y-m-d', $date)]['clicked'] ?>],<?php } ?>
        ]
        var BOUNCED = [
                <?php for ($date = $this->from; $date <= $this->to; $date = strtotime('+1 day', $date)) { ?>[gd(<?= date('Y', $date) ?>, <?= (int)date('m', $date) ?>, <?= (int)date('d', $date) ?>), <?= (int)$this->stats['data'][date('Y-m-d', $date)]['bounced'] ?>],<?php } ?>
        ]
        var ORDERS = [
                <?php for ($date = $this->from; $date <= $this->to; $date = strtotime('+1 day', $date)) { ?>[gd(<?= date('Y', $date) ?>, <?= (int)date('m', $date) ?>, <?= (int)date('d', $date) ?>), <?= (int)$this->campStats['data'][date('Y-m-d', $date)]['orders'] ?>],<?php } ?>
        ]
        var TURNOVER = [
                <?php for ($date = $this->from; $date <= $this->to; $date = strtotime('+1 day', $date)) { ?>[gd(<?= date('Y', $date) ?>, <?= (int)date('m', $date) ?>, <?= (int)date('d', $date) ?>), <?= (float)$this->campStats['data'][date('Y-m-d', $date)]['ca'] ?>],<?php } ?>
        ]

        var dataset = [<?php if($_SESSION['statsMailsSent'] == 1) { ?>{
            label: "Envoyés",
            data: SENT,
            yaxis: 2,
            color: "rgba(255,148,57,1)",
            lines: {
                lineWidth: 1,
                show: true,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.2
                    }, {
                        opacity: 0.4
                    }]
                }
            },
            points: {
                show: true,
                radius: 2,
                symbol: "circle"
            },
            splines: {
                show: false,
                tension: 0.6,
                lineWidth: 1,
                fill: 0.1
            }
        },<?php } ?>
                <?php if($_SESSION['statsMailsOpened'] == 1) { ?>{
                label: "Ouverts",
                data: OPENED,
                yaxis: 2,
                color: "#1C84C6",
                lines: {
                    lineWidth: 1,
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.2
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                points: {
                    show: true,
                    radius: 2,
                    symbol: "circle"
                },
                splines: {
                    show: false,
                    tension: 0.6,
                    lineWidth: 1,
                    fill: 0.1
                }
            },<?php } ?>
                <?php if($_SESSION['statsMailsClicked'] == 1) { ?>{
                label: "Cliqués",
                data: CLICKED,
                yaxis: 2,
                color: "#F43779",
                lines: {
                    lineWidth: 1,
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.4
                        }, {
                            opacity: 0.2
                        }]
                    }
                },
                points: {
                    show: true,
                    radius: 2,
                    symbol: "circle"
                }
            },<?php } ?>
                <?php if($_SESSION['statsMailsBounced'] == 1) { ?>{
                label: "Refusés",
                data: BOUNCED,
                yaxis: 2,
                color: "#00978A",
                lines: {
                    lineWidth: 1,
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.2
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                points: {
                    show: true,
                    radius: 2,
                    symbol: "circle"
                },
                splines: {
                    show: false,
                    tension: 0.6,
                    lineWidth: 1,
                    fill: 0.1
                }
            },<?php } ?>
            <?php if ($this->email->id_partenaire > 0) { ?>
            <?php if($_SESSION['statsMailsOrders'] == 1) { ?>
            {
                label: "Commandes",
                data: ORDERS,
                yaxis: 2,
                color: "#FF0000",
                lines: {
                    lineWidth: 1,
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.2
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                points: {
                    show: true,
                    radius: 2,
                    symbol: "circle"
                },
                splines: {
                    show: false,
                    tension: 0.6,
                    lineWidth: 1,
                    fill: 0.1
                }
            },<?php } ?>
                <?php if($_SESSION['statsMailsCA'] == 1) { ?>{
                label: "Chiffre d'affaires",
                data: TURNOVER,
                yaxis: 1,
                color: "#FFF600",
                bars: {
                    show: true,
                    barWidth: 24 * 60 * 60 * 600,
                    fill: true,
                    lineWidth: 1,
                    fillColor: "rgba(255,246,0,0.5)"
                }
            }<?php } ?>
            <?php } ?>
        ];
        var options = {
            xaxis: {
                mode: "time",
                tickSize: [3, "day"],
                tickLength: 0,
                axisLabel: "Date",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Arial',
                axisLabelPadding: 10,
                color: "#d5d5d5"
            },
            yaxes: [{
                position: "left",
                //max: 1070,
                color: "#d5d5d5",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Arial',
                axisLabelPadding: 3
            }, {
                position: "right",
                clolor: "#d5d5d5",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: ' Arial',
                axisLabelPadding: 67
            }
            ],
            legend: {
                show: true,
                noColumns: 1,
                labelBoxBorderColor: "#000000",
                position: "nw"
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            }
        };
        $.plot($("#flot-dashboard-chart"), dataset, options);
        $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            border: "1px solid #fdd",
            padding: "2px",
            "background-color": "#fee",
            opacity: 0.80
        }).appendTo("body");
        $("#flot-dashboard-chart").bind("plothover", function (event, pos, item) {

            if (item) {
                var d = new Date(item.datapoint[0]);
                var x = d.toLocaleDateString(),
                    y = item.datapoint[1].toFixed(2);
                $("#tooltip").html('<strong>' + item.series.label + "</strong> - <strong>" + x + "</strong> = <strong>" + y + '</strong>')
                    .css({top: item.pageY + 5, left: item.pageX + 5})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });
        $('.datePicker').datepicker({
            todayBtn: false,
            language: "fr",
            format: "dd/mm/yyyy",
            todayHighlight: false,
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    });
</script>
