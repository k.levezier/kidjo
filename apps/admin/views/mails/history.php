<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-mails', 'history', $this->language, 'Historique des emails') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-mails', 'history', $this->language, 'Historique des emails') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/mails/templates' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-mails', 'back-mails', $this->language, 'Retour aux mails') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-mails', 'liste-mails-history', $this->language, 'Liste des email envoyés') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form name="filtresMails" method="GET" action="<?= $_SERVER['REQUEST_URI'] ?>/">
                            <div class="col-sm-1">&nbsp;</div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" class="form-control"
                                           placeholder="<?= $this->ln->txt('admin-mails', 'ph-search-cmde', $this->language, 'Mail destinataire, expéditeur, sujet') ?>"
                                           name="filtre" value="<?= $_GET['filtre'] ?>" required>
                                    <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary"
                                                    name="btnSendForm"> <?= $this->ln->txt('admin-generic', 'valider', $this->language, 'Valider') ?></button>                                        
                                        </span>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <a href="<?= $this->lurl ?>/mails/history"
                                   class="btn btn-primary"><?= $this->ln->txt('admin-generic', 'reset', $this->language, 'Effacer') ?></a>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <?php if ($this->count > 0) { ?>
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-history-date', $this->language, 'Date') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-history-from', $this->language, 'Expéditeur') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-history-dest-nmp', $this->language, 'Destinataire (NMP)') ?></th>
                                        <th class="col-lg-4"><?= $this->ln->txt('admin-mails', 'mails-history-subject', $this->language, 'Sujet') ?></th>
                                        <th class="col-lg-1">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->mails_filer as $key => $mail) { ?>
                                        <tr>
                                            <td>
                                                <?= date($this->clSettings->getParam('format-des-dates', 'sets') . ' H:i:s', strtotime($mail['added'])) ?>
                                            </td>
                                            <td>
                                                <?= $mail['from'] ?>
                                            </td>
                                            <td>
                                                <?= $mail['email_destinataire'] ?>
                                            </td>
                                            <td>
                                                <?= $mail['subject'] ?>
                                            </td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/mails/previewMail/<?= $mail['id_filermails'] ?>"
                                                   target="_blank" class="btn btn-white btn-xs btn-action">
                                                    <i class="fa fa-eye"></i> <?= $this->ln->txt('admin-generic', 'apercu', $this->language, 'Aperçu') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <p>&nbsp;</p>
                            <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
