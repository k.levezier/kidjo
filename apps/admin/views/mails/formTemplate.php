<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-mails', 'gestion-templates', $this->language, 'Gestion des templates d\'emails') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/mails/templates"><?= $this->ln->txt('admin-mails', 'tpl-mails', $this->language, 'Templates d\'emails') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-mails', 'mails-template-' . ($this->new ? 'add' : 'edit'), $this->language, ($this->new ? 'Ajout' : 'Modification') . ' d\'un template de mail') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/mails/templates' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-mails', 'back-mails', $this->language, 'Retour aux mails') ?>
            </a>
            <?php if (!$this->new) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-mails', 'delete-tplmail', $this->language, 'Supprimer le template de mail') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-mails', ($this->new ? 'form-add-tpl' : 'form-edit-tpl'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'un template de mail' : 'Formulaire de modification d\'un template de mail')) ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="panel blank-panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="tab" class="tab-pane active">
                                    <form method="POST" name="formTemplateEmail" id="formTemplateEmail"
                                          class="form-horizontal">
                                        <input type="hidden" name="sendForm"/>
                                        <input type="hidden" name="restePage" id="restePage" value="0"/>
                                        <input type="hidden" name="type" value="<?= $_POST['type'] ?>"/>
                                        <input type="hidden" name="selectedLang" id="selectedLang"
                                               value="<?= $this->selectedLang ?>"/>
                                        <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'name', $_POST, $this->ln->txt('admin-mails', 'form-nom-mail', $this->language, 'Nom')) ?>
                                        <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'exp_name', $_POST, $this->ln->txt('admin-mails', 'form-nom-exp-mail', $this->language, 'Nom d\'expéditeur')) ?>
                                        <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'exp_email', $_POST, $this->ln->txt('admin-mails', 'form-mail-exp-mail', $this->language, 'Adresse d\'expéditeur')) ?>
                                        <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'subject', $_POST, $this->ln->txt('admin-mails', 'form-subject-mail', $this->language, 'Sujet')) ?>
                                        <?= $this->clTemplates->affichageFormBO(null, null, true, 'Select', 'id_partenaire', $_POST, $this->ln->txt('admin-mails', 'form-part', $this->language, 'Partenaire'), $this->parts) ?>
                                        <?= $this->clTemplates->affichageFormBO(null, null, true, 'Textearea', 'content', $_POST, $this->ln->txt('admin-mails', 'form-content-mail', $this->language, 'Contenu')) ?>
                                        <div class="hr-line-dashed m-t-zero"></div>
                                        <div class="progress progress-striped active center loadingBar">
                                            <div class="progress-bar progress-bar-primary">
                                                <span class="sr-only"><?= $this->ln->txt('admin-generic', 'loading', $this->language, 'Chargement') ?></span>
                                            </div>
                                        </div>
                                        <div class="form-group action_btns hide">
                                            <div class="col-sm-6 col-sm-offset-2">
                                                <a href="<?= $this->lurl ?>/mails/templates" class="btn btn-white">
                                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                                </a>
                                                <button onClick="$('#restePage').val(1);" class="btn btn-primary"
                                                        type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                                <button class="btn btn-primary"
                                                        type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function initialiseJqueryPlugins() {
        <?php if (!$this->new) { ?>
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-mails', 'delete-tpl-alert', $this->language, 'Confirmez vous la suppression du template ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/mails/deleteTemplate/<?= $this->params[0] ?>');
                        return false;
                    }
                });
        });
        <?php } ?>

        $(document).on('click', '.change-lang-form', function () {
            var currentLang = $('#selectedLang').val();
            var newLang = $(this).data('lang');
            if (newLang !== currentLang) {
                $('textarea[name="value_content_' + currentLang + '"]').next('.CodeMirror').css('display', 'none');
                $('textarea[name="value_content_' + newLang + '"]').next('.CodeMirror').css('display', 'block');
                $('.field_lang_' + newLang).removeClass('hidden_field_lang');
                $('.field_lang_' + currentLang).addClass('hidden_field_lang');
                $('#selectedLang').val(newLang);
                $('.button-lang-form').find('img').attr('src', addSURL + '/images/admin/flags/' + newLang + '.png');
            }
        });
    }

    $(document).ready(function () {
        $(".loadingBar").hide();
        $(".action_btns").removeClass('hide');

        <?php foreach ($this->ln->tabLangues as $key => $ln) { ?>
        $('textarea[name="value_content_<?= $key ?>"]').data('display', $('textarea[name="value_content_<?= $key ?>"]').css('display'));
        CodeMirror.fromTextArea(document.getElementsByName("value_content_<?= $key ?>")[0], {
            lineNumbers: true,
            lineWrapping: true,
            matchBrackets: true,
            mode: "application/x-httpd-php",
            indentUnit: 4,
            indentWithTabs: true
        });
        $('textarea[name="value_content_<?= $key ?>"]').next('.CodeMirror').css('display', $('textarea[name="value_content_<?= $key ?>"]').data('display'));
        <?php } ?>

        initialiseJqueryPlugins();
    });
</script>
