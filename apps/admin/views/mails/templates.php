<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-mails', 'gestion-templates', $this->language, 'Gestion des templates d\'emails') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-mails', 'tpl-mails', $this->language, 'Templates d\'emails') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/mails/formTemplate"
               class="btn btn-primary"><?= $this->ln->txt('admin-mails', 'addmail', $this->language, 'Ajouter un template de mail') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-mails', 'liste-mails-templates', $this->language, 'Liste des templates de mail') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <?php if ($this->mails_text->count() > 0) { ?>

                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-lg-3"><?= $this->ln->txt('admin-mails', 'mails-nom', $this->language, 'Nom') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-expediteur', $this->language, 'Expéditeur') ?></th>
                                        <th class="col-lg-3"><?= $this->ln->txt('admin-mails', 'mails-sujet', $this->language, 'Sujet') ?></th>
                                        <th class="col-lg-1"><?= $this->ln->txt('admin-mails', 'mails-maj', $this->language, 'Mise à jour') ?></th>
                                        <th class="col-lg-3">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->mails_text->order('name', 'ASC') as $key => $template) { ?>
                                        <tr>
                                            <td class="tooltip-demo">
                                                <strong data-container="body" data-toggle="popover"
                                                        data-placement="right"
                                                        data-content="<?= $this->ln->txt('admin-generic', 'utilisation', $this->language, 'Utilisation :') ?> <?= $template->type ?>">
                                                    <?= $template->name ?>
                                                </strong>
                                            </td>
                                            <td>
                                                <?= $template->exp_name ?><br/> (<?= $template->exp_email ?>)
                                            </td>
                                            <td>
                                                <?= $template->subject ?>
                                            </td>
                                            <td>
                                                <?= date($this->clSettings->getParam('format-des-dates', 'sets'), strtotime($template->updated)) ?>
                                            </td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/mails/stats/<?= $template->id_textemail ?>"
                                                   class="btn btn-white btn-xs btn-action">
                                                    <i class="fa fa-line-chart"></i> <?= $this->ln->txt('admin-mails', 'mails-stats', $this->language, 'Stats') ?>
                                                </a>
                                                <a href="<?= $this->lurl ?>/mails/previewForm/<?= $template->type ?>"
                                                   class="btn btn-white btn-xs btn-action">
                                                    <i class="fa fa-eye"></i> <?= $this->ln->txt('admin-mails', 'mails-preview', $this->language, 'Preview') ?>
                                                </a>
                                                <a href="<?= $this->lurl ?>/mails/formTemplate/<?= $template->type ?>"
                                                   class="btn btn-white btn-xs btn-action">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-mails', 'mails-edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } else { ?>
                            <p>&nbsp;</p>
                            <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.popover-content', function () {
        var _this = $(this);
        var node = $(this).get(0);
        var text = $(this).text().toString();
        if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.setStart(node.firstChild, text.indexOf(':') + 2);
            range.setEnd(node.firstChild, text.length);
            selection.removeAllRanges();
            selection.addRange(range);
            document.execCommand("copy");
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 3000
            };
            toastr.success('<?= $this->ln->txt('admin-generic', 'fnc-copied', $this->language, 'Fonction copiée') ?>', '<?= $this->ln->txt('admin-generic', 'clipboard', $this->language, 'Presse-papier') ?>');
            setTimeout(function () {
                _this.parent().fadeOut();
            }, 1500);
        }
    });
</script>





