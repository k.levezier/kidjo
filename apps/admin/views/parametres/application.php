<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-parametres', 'parametres-appli', $this->language, 'Paramètres application') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/parametres"><?= $this->ln->txt('admin-parametres', 'parametres', $this->language, 'Paramètres') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-parametres', 'parametres-appli', $this->language, 'Paramètres application') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/parametres/formApp" class="btn btn-primary"><?= $this->ln->txt('admin-parametres', 'add-param', $this->language, 'Ajouter un paramètre') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?php
            if ($this->sets_categories->count() > 0) {
                foreach ($this->sets_categories as $categ) {
                    ?>
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>
                                <?= $this->ln->txt('admin-parametres', 'liste-param-categ', $this->language, 'Liste des paramètres de la catégorie') ?>
                                <?= $categ->nom ?>
                            </h5>
                            <div class="ibox-tools">
                                <a href="<?= $this->lurl ?>/parametres/formApp/cat___<?= $categ->id_categorie ?>">
                                    <i class="fa fa-plus"></i>
                                    <?= $this->ln->txt('admin-parametres', 'add-param', $this->language, 'Ajouter un paramètre') ?>
                                </a>
                                <a class="collapse-link-perso" id="app<?= $categ->id_categorie ?>">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content panbloc" id="app<?= $categ->id_categorie ?>">
                            <?php if ($categ->sets->count() > 0) { ?>
                                <div class="table-responsive">
                                    <table class="table table-striped table-hover vertical-center">
                                        <thead>
                                            <tr>
                                                <th class="col-sm-3"><?= $this->ln->txt('admin-parametres', 'tab-nom', $this->language, 'Nom') ?></th>
                                                <th class="col-sm-4"><?= $this->ln->txt('admin-parametres', 'tab-description', $this->language, 'Description') ?></th>
                                                <th class="col-sm-2"><?= $this->ln->txt('admin-parametres', 'tab-type', $this->language, 'Type') ?></th>
                                                <th class="col-sm-2"><?= $this->ln->txt('admin-parametres', 'tab-valeur', $this->language, 'Valeur') ?></th>
                                                <th class="col-sm-1">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($categ->sets as $key => $param) { ?>
                                                <tr>
                                                    <td class="tooltip-demo">
                                                        <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'utilisation', $this->language, 'Utilisation :') ?> $this->clSettings->getParam('<?= $param->slug ?>', 'sets');"><?= $param->nom ?></strong>
                                                    </td>
                                                    <td><?= $param->description ?></td>
                                                    <td><?= $param->type ?></td>
                                                    <td><?= $this->clSettings->affichageBO($param->type, $param->value) ?></td>
                                                    <td>
                                                        <a href="<?= $this->lurl ?>/parametres/formApp/<?= $param->id_set ?>" class="btn btn-white btn-sm">
                                                            <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } else { ?>
                                <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    </div>
                </div>
            <?php } ?>
            <?php if ($this->orphan_sets->count() > 0) { ?>
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <i class="fa fa-warning"></i> <?= $this->ln->txt('admin-parametres', 'liste-param-orphan', $this->language, 'Liste des paramètres sans catégorie') ?>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-sm-3"><?= $this->ln->txt('admin-parametres', 'tab-nom', $this->language, 'Nom') ?></th>
                                        <th class="col-sm-4"><?= $this->ln->txt('admin-parametres', 'tab-description', $this->language, 'Description') ?></th>
                                        <th class="col-sm-2"><?= $this->ln->txt('admin-parametres', 'tab-type', $this->language, 'Type') ?></th>
                                        <th class="col-sm-2"><?= $this->ln->txt('admin-parametres', 'tab-valeur', $this->language, 'Valeur') ?></th>
                                        <th class="col-sm-1">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->orphan_sets as $key => $param) { ?>
                                        <tr>
                                            <td class="tooltip-demo">
                                                <strong data-container="body" data-toggle="popover" data-placement="right" data-content="<?= $this->ln->txt('admin-generic', 'utilisation', $this->language, 'Utilisation :') ?> $this->clSettings->getParam('<?= $param->slug ?>', 'sets');"><?= $param->nom ?></strong>
                                            </td>
                                            <td><?= $param->description ?></td>
                                            <td><?= $param->type ?></td>
                                            <td><?= $this->clSettings->affichageBO($param->type, $param->value) ?></td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/parametres/formApp/<?= $param->id_set ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.popover-content', function () {
        var _this = $(this);
        var node = $(this).get(0);
        var text = $(this).text().toString();
        if (window.getSelection) {
            selection = window.getSelection();
            range = document.createRange();
            range.setStart(node.firstChild, text.indexOf(':') + 2);
            range.setEnd(node.firstChild, text.length);
            selection.removeAllRanges();
            selection.addRange(range);
            document.execCommand("copy");
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 3000
            };
            toastr.success('<?= $this->ln->txt('admin-generic', 'fnc-copied', $this->language, 'Fonction copiée') ?>', '<?= $this->ln->txt('admin-generic', 'clipboard', $this->language, 'Presse-papier') ?>');
            setTimeout(function () {
                _this.parent().fadeOut();
            }, 1500);
        }
    });

    $(document).on('click', '.collapse-link-perso', function () {
        var $panel = $(this).closest('.ibox').find('.panbloc');
        $panel.toggleClass('hide');
        if ($panel.hasClass('hide')) {
            $(this).find('i').attr('class', 'fa fa-chevron-down');
        } else {
            $(this).find('i').attr('class', 'fa fa-chevron-up');
        }
        localStorage.setItem('paramsapp' + $(this).attr('id'), ($panel.hasClass('hide') ? '1' : '0'));
    });

    $('.panbloc').each(function () {
        var mem = localStorage.getItem('paramsapp' + $(this).attr('id'));
        if (mem !== null && mem !== '0') {
            $(this).addClass('hide');
        }
    });

    $('.collapse-link-perso').each(function () {
        var mem = localStorage.getItem('paramsapp' + $(this).attr('id'));
        if (mem !== null && mem !== '0') {
            $(this).find('i').attr('class', 'fa fa-chevron-down');
        }
    });
</script>
