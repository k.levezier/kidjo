<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-parametres', 'parametres-categories', $this->language, 'Paramètres catégories') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/parametres"><?= $this->ln->txt('admin-parametres', 'parametres', $this->language, 'Paramètres') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-parametres', 'parametres-categories', $this->language, 'Paramètres catégories') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/parametres/formCategories" class="btn btn-primary"><?= $this->ln->txt('admin-parametres', 'add-categ', $this->language, 'Ajouter une catégorie') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-parametres', 'liste-parametres-categ', $this->language, 'Liste des catégories de paramètre') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <?php if ($this->sets_categories->count() > 0) { ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-sm-11"><?= $this->ln->txt('admin-parametres', 'tab-nom-categ', $this->language, 'Nom de la catégorie') ?></th>
                                        <th class="col-sm-1">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->sets_categories as $categ) { ?>
                                        <tr>
                                            <td><?= $categ->nom ?></td>
                                            <td>
                                                <a href="<?= $this->lurl ?>/parametres/formCategories/<?= $categ->id_categorie ?>" class="btn btn-white btn-sm">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-generic', 'edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>  
