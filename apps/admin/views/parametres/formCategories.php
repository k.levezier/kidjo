<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-parametres', 'parametres-categories', $this->language, 'Paramètres catégories') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/parametres"><?= $this->ln->txt('admin-parametres', 'parametres', $this->language, 'Paramètres') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/parametres/categories"><?= $this->ln->txt('admin-parametres', 'parametres-categories', $this->language, 'Paramètres catégories') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-parametres', ($this->new ? 'add-categ' : 'edit-categ'), $this->language, ($this->new ? 'Ajouter une catégorie' : 'Modifier une catégorie')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/parametres/categories" class="btn btn-primary">
                <?= $this->ln->txt('admin-parametres', 'back-categories', $this->language, 'Retour aux catégories') ?>
            </a>
            <?php if (!$this->new && $this->sets_categories->lock == 0) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-parametres', 'delete-categ', $this->language, 'Supprimer la catégorie') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="POST" class="form-horizontal" name="formCateg" id="formCateg" enctype="multipart/form-data">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-parametres', ($this->new ? 'form-add-categ' : 'form-edit-categ'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'une catégorie' : 'Formulaire de modification d\'une catégorie')) ?></h5>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" name="sendForm"/>
                        <input type="hidden" name="restePage" id="restePage" value="0"/>
                        <input type="hidden" name="id_categorie" value="<?= $_POST['id_categorie'] ?>"/>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormNom ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="nom"><?= $this->ln->txt('admin-parametres', 'form-categ-lab-nom', $this->language, 'Nom') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nom" id="nom" value="<?= $_POST['nom'] ?>">
                                <?= (isset($_POST['sendForm']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="lock"><?= $this->ln->txt('admin-parametres', 'form-categ-lab-lock', $this->language, 'Bloquer la catégorie') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="lock" id="lock" value="1"<?= ($_POST['lock'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/parametres/categories" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    function initialiseJqueryPlugins() {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-parametres', 'delete-categ-alert', $this->language, 'Confirmez vous la suppression de la catégorie ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/parametres/deleteCategorie/<?= $this->sets_categories->id_categorie ?>');
                        return false;
                    }
                });
        });
    }

    $(document).ready(function () {
        initialiseJqueryPlugins();
    });
</script>  
