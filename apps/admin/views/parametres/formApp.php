<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-parametres', 'parametres-appli', $this->language, 'Paramètres application') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/parametres"><?= $this->ln->txt('admin-parametres', 'parametres', $this->language, 'Paramètres') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/parametres/application"><?= $this->ln->txt('admin-parametres', 'parametres-appli', $this->language, 'Paramètres application') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-parametres', ($this->new ? 'add-param' : 'edit-param'), $this->language, ($this->new ? 'Ajouter un paramètre' : 'Modifier un paramètre')) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/parametres/application" class="btn btn-primary">
                <?= $this->ln->txt('admin-parametres', 'back-parametres', $this->language, 'Retour aux paramètres') ?>
            </a>
            <?php if (!$this->new && $this->sets->lock == 0) { ?>
                <a class="btn btn-danger confirm">
                    <?= $this->ln->txt('admin-parametres', 'delete-parametres', $this->language, 'Supprimer le paramètre') ?>
                </a>
            <?php } ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="POST" class="form-horizontal" name="formParam" id="formParam" enctype="multipart/form-data">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-parametres', ($this->new ? 'form-add-param' : 'form-edit-param'), $this->language, ($this->new ? 'Formulaire d\'ajout d\'un paramètre' : 'Formulaire de modification d\'un paramètre')) ?></h5>
                    </div>
                    <div class="ibox-content">
                        <input type="hidden" name="sendForm"/>
                        <input type="hidden" name="restePage" id="restePage" value="0"/>
                        <input type="hidden" name="id_set" value="<?= $_POST['id_set'] ?>"/>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormNom ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="nom"><?= $this->ln->txt('admin-parametres', 'form-param-lab-nom', $this->language, 'Nom') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nom" id="nom" value="<?= $_POST['nom'] ?>">
                                <?= (isset($_POST['sendForm']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="description"><?= $this->ln->txt('admin-parametres', 'form-param-lab-description', $this->language, 'Description') ?></label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="description" id="description" value="<?= $_POST['description'] ?>">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="type"><?= $this->ln->txt('admin-parametres', 'form-zone-lab-type', $this->language, 'Type') ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="type" id="type">
                                    <option value=""><?= $this->ln->txt('admin-parametres', 'form-choix-type', $this->language, 'Choisir un type') ?></option>
                                    <?php foreach ($this->clSettings->lTypes as $type) { ?>
                                        <option value="<?= $type ?>"<?= ($_POST['type'] == $type ? ' selected' : '') ?>>
                                            <?= $this->ln->txt('admin-parametres', 'type-' . generateSlug($type), $this->language, $type) ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <?= (isset($_POST['sendForm']) && $this->errorFormType ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div id="displayType">
                            <?php if (!empty($_POST['type'])) { ?>
                                <?= $this->clSettings->affichageFormBO($_POST['type'], $_POST['value'], $this->language) ?>
                            <?php } ?>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormCateg ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="id_categorie"><?= $this->ln->txt('admin-parametres', 'form-param-lab-categ', $this->language, 'Catégorie') ?></label>
                            <div class="col-sm-10">
                                <select class="form-control" name="id_categorie" id="id_categorie">
                                    <option value=""><?= $this->ln->txt('admin-administration', 'form-param-choix-categ', $this->language, 'Choisir une catégorie') ?></option>
                                    <?php foreach ($this->sets_categories as $categ) { ?>
                                        <option value="<?= $categ->id_categorie ?>"<?= ($_POST['id_categorie'] == $categ->id_categorie ? ' selected' : '') ?>><?= $categ->nom ?></option>
                                    <?php } ?>
                                </select>
                                <?= (isset($_POST['sendForm']) && $this->errorFormCateg ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="lock"><?= $this->ln->txt('admin-parametres', 'form-param-lab-lock', $this->language, 'Bloquer le paramètre') ?></label>
                            <div class="col-sm-9">
                                <div class="checkbox i-checks">
                                    <input type="checkbox" name="lock" id="lock" value="1"<?= ($_POST['lock'] == 1 ? ' checked' : '') ?>>
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/parametres/application" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    function initialiseJqueryPlugins() {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.confirm').on('click', function () {
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-parametres', 'delete-param-alert', $this->language, 'Confirmez vous la suppression du paramètre ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/parametres/deleteApp/<?= $this->sets->id_set ?>');
                        return false;
                    }
                });
        });

        $('#datePicker').datepicker({
            todayBtn: false,
            language: "<?= $this->language ?>",
            format: "<?= $this->clSettings->getParam('format-datepicker', 'sets') ?>",
            todayHighlight: true,
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

        $('#valuefichier').on('click', function () {
            $('#fichierFile').trigger('click');
        });

        $('#fichierFile').on('change', function () {
            if ($(this).val() !== '') {
                $('#fichierPath').attr('placeholder', $(this).val());
            } else {
                $('#fichierPath').attr('placeholder', "<?= $this->ln->txt('admin-generic', 'no-file', $this->language, 'Aucun fichier séléctionné') ?>");
            }
        });

        $('#type').on('change', function () {
            if ($(this).val() !== '') {
                var $type = '<?= $_POST['type'] ?>';
                var $value = (($(this).val() === $type) ? '<?= (!empty($_POST['type']) ? $_POST['value'] : '') ?>' : '');
                $.post('<?= $this->lurl ?>/parametres/displayType', {
                    type: $(this).val(),
                    value: $value,
                    id_langue: '<?= $this->language ?>'
                }, function (data) {
                    $('#displayType').html(data);
                    initialiseJqueryPlugins();
                });
            } else {
                $('#displayType').html('');
                initialiseJqueryPlugins();
            }
            return false;
        });
    }

    $(document).ready(function () {
        initialiseJqueryPlugins();
    });
</script> 
