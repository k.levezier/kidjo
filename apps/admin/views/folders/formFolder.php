<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-folders', 'folders', $this->language, 'Folders') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/folders/all_folders"><?= $this->ln->txt('admin-folders', 'folders', $this->language, 'Folders') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-folders', 'folders-'.($this->new?'add':'edit'), $this->language, ($this->new?'Add':'Edit').' folder') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/folders/all_folders' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-folders', 'back-all-folders', $this->language, 'Back all folders') ?>
            </a>
            <?php
            if (!$this->new) {
                ?>
                <a class="btn btn-danger confirmAlert">
                    <?= $this->ln->txt('admin-folders', 'delete-folders', $this->language, 'Delete folder') ?>
                </a>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">          
            <form method="POST" name="formFolders" id="formFolders" enctype="multipart/form-data" >
                
                <input type="hidden" name="sendForm" />
                <input type="hidden" name="restePage" id="restePage" value="0" />
                <input type="hidden" name="part" id="part" value="1" />
                <input type="hidden" name="id_folder" id="id_folder" value="<?=$_POST['id_folder']?>" />
                
                <div class="tabs-container tabs-container-products">
                    <div class="tabs-left">
                        <ul class="nav nav-tabs">
                            <li <?=$this->params['part'] == 1 || !isset($this->params['part']) ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-informations">
                                    <i class="fa fa-info-circle"></i> 
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-folders', 'folder-infos', $this->language, 'Informations') ?></span>
                                </a>
                            </li>
                            <?php
                            if (!$this->new) {
                                ?>
                                <li <?=$this->params['part'] == 2 ? 'class="active"':''?>>
                                    <a data-toggle="tab" href="#tab-images">
                                        <i class="fa fa-picture-o"></i>
                                        <span class="hideOnMobile"><?= $this->ln->txt('admin-folders', 'folder-images', $this->language, 'Images') ?></span>
                                    </a>
                                </li>

                                <li <?=$this->params['part'] == 3 ? 'class="active"':''?>>
                                    <a data-toggle="tab" href="#tab-videos">
                                        <i class="fa fa-file-video-o"></i>
                                        <span class="hideOnMobile"><?= $this->ln->txt('admin-folders', 'folder-video', $this->language, 'Videos') ?></span>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                        
                        <?php $i=1; ?>
                        <div class="tab-content ">
                            <div id="tab-informations" class="tab-pane <?=$this->params['part'] == $i || !isset($this->params['part']) ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal" autocomplete="off">
                                            <h4><?= $this->ln->txt('admin-folders', 'folder-infos-desc', $this->language, 'Informations folder') ?></h4>
                                            <div class="hr-line-dashed"></div>

                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormTitle ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="title"><?= $this->ln->txt('admin-folders', 'form-folder-lab-title', $this->language, 'Folder title') ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="title" id="title" value="<?=$_POST['title']?>" required/>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormTitle ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                </div>
                                            </div>

                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="language"><?= $this->ln->txt('admin-folders', 'form-folder-lab-language', $this->language, 'Language') ?></label>
                                                <div class="col-sm-10">
                                                    <select id="folder-language" name="folder-language[]"
                                                            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-language', $this->language, 'Language') ?>"
                                                            class="chosen-select  form-control" multiple="multiple">
                                                        <?php

                                                        foreach($this->languages as $lang){

                                                            ?>
                                                            <option value="<?= $lang->id_language?>"
                                                                    <?php
                                                                    if(count($_POST['languages_folder']) > 0) {
                                                                        echo(in_array($lang->id_language, $_POST['languages_folder']) ? 'selected' : '');
                                                                    }
                                                                    ?>
                                                                >
                                                                <?= $lang->name ?>
                                                            </option>
                                                            <?php
                                                        }?>
                                                    </select>
                                                </div>
                                            </div>



                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="country">
                                                    <?= $this->ln->txt('admin-folders', 'form-folder-lab-Countries', $this->language, 'Countries') ?>
                                                </label>
                                                <div class="col-sm-10">
                                                    <select id="folder-countries" name="folder-countries[]"
                                                            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-countries', $this->language, 'Countries') ?>"
                                                            class="chosen-select  form-control" multiple="multiple" style="height: 200px;">
                                                        <?php
                                                        foreach($this->countries as $country){
                                                            ?>
                                                            <option value="<?= $country->id_pays?>"
                                                                <?php
                                                                if(count($_POST['folder-countries']) > 0) {
                                                                    echo(in_array($country->id_pays, $_POST['folder-countries']) ? 'selected' : '');
                                                                }
                                                                ?>
                                                            >
                                                                <?= $country->en ?>
                                                            </option>
                                                            <?php
                                                        }?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="hr-line-dashed"></div>


                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="type"><?= $this->ln->txt('admin-folders', 'form-folder-lab-type', $this->language, 'Type') ?></label>
                                                <div class="col-sm-10">
                                                    <select id="folder-type" name="folder-type"
                                                            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-type', $this->language, 'Type') ?>"
                                                            class="chosen-select  form-control">
                                                        <?php
                                                        foreach($this->folders_type as $type){
                                                            ?>
                                                            <option value="<?= $type->id_folder_type?>"
                                                                <?=($_POST['id_type'] == $type->id_folder_type?'selected':'')?>>
                                                                <?= $type->name ?>
                                                            </option>
                                                            <?php
                                                        }?>
                                                    </select>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                </div>
                                            </div>


                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="mediatype"><?= $this->ln->txt('admin-folders', 'form-folder-lab-media-type', $this->language, 'Media Type') ?></label>
                                                <div class="col-sm-10">
                                                    <select id="folder-media-type" name="folder-media-type"
                                                            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-media-type', $this->language, 'Media type') ?>"
                                                            class="chosen-select  form-control">
                                                        <?php
                                                        foreach($this->folders_media_type as $media_type){
                                                            ?>
                                                            <option value="<?= $media_type->id_media_type?>"
                                                                <?=($_POST['id_media_type'] == $media_type->id_media_type ?'selected':'')?>>
                                                                <?= $media_type->name ?>
                                                            </option>
                                                            <?php
                                                        }?>
                                                    </select>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                </div>
                                            </div>

                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="priority"><?= $this->ln->txt('admin-folders', 'form-folder-lab-priority', $this->language, 'Priority') ?></label>
                                                <div class="col-sm-10">
                                                    <select id="folder-priority" name="folder-priority"
                                                            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-priority', $this->language, 'Priority') ?>"
                                                            class="chosen-select  form-control">
                                                        <?php
                                                        foreach($this->folders_priority as $priority){
                                                            ?>
                                                            <option value="<?= $priority->id_priority?>"
                                                                <?=($_POST['id_priority'] == $priority->id_priority ?'selected':'')?>>
                                                                <?= $priority->name ?>
                                                            </option>
                                                            <?php
                                                        }?>
                                                    </select>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                </div>
                                            </div>


                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="priority">
                                                    <?= $this->ln->txt('admin-folders', 'form-folder-age', $this->language, 'Age Min/max') ?>
                                                </label>
                                                <div class="col-sm-4">
                                                    <select id="folder-age_min" name="age_min"
                                                            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-age-min', $this->language, 'Age Min') ?>"
                                                            class="chosen-select  form-control">
                                                        <?php
                                                        for($age=0; $age <= 7; $age++){
                                                            ?>
                                                            <option value="<?= $age?>"
                                                                    <?=($_POST['age_min'] == $age ?'selected':'')?>>
                                                                <?= $age ?>
                                                            </option>
                                                            <?php
                                                        }?>
                                                    </select>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <select id="folder-age_max" name="age_max"
                                                            data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-ageg-max', $this->language, 'Age Max') ?>"
                                                            class="chosen-select  form-control">
                                                        <?php
                                                        for($age=0; $age <= 7; $age++){
                                                            ?>
                                                            <option value="<?= $age?>"
                                                                    <?=($_POST['age_max'] == $age ?'selected':'')?>>
                                                                <?= $age ?>
                                                            </option>
                                                            <?php
                                                        }?>
                                                    </select>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                </div>
                                            </div>


                                            <?php
                                            if ($this->new) {
                                                //CREATION
                                                ?>
                                                <input type="hidden" name="create">
                                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                    <label class="col-sm-2 control-label"
                                                           for="priority"><?= $this->ln->txt('admin-folders', 'form-folder-lab-image', $this->language, 'Image default') ?></label>
                                                    <div class="col-sm-10">
                                                        <input type="file" class="form-control" name="image" id="image"
                                                               value=""/>
                                                        <?= (isset($_POST['sendForm']) && $this->errorFormLanguage ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            else{
                                                // EDITION
                                                ?>
                                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                    <div class="i-checks">
                                                        <label for="active-folder"
                                                               class="col-sm-2 control-label"><?= $this->ln->txt('admin-folders', 'label-active-folder', $this->language, 'Active folder') ?></label>
                                                        <div class="col-lg-3" style="margin-top: 10px;">
                                                            <input name="active_folder" id="active_folder"
                                                                   class="form-control"
                                                                   type="checkbox"
                                                                   <?=($_POST['status'] == "1"?'checked':'')?>
                                                            >
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/folders/all_folders" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit">
                                                <?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?>
                                            </button>
                                            <button class="btn btn-primary" type="submit">
                                                <?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <?php $i++; ?>
                            <?php
                            if (!$this->new) {
                                ?>
                                <div id="tab-images" class="tab-pane <?=$this->params['part'] == $i ? 'active':''?>">
                                    <div class="panel-body">
                                        <div class="container-fluid">

                                            <?php
                                            $cpt = 0;
                                            foreach($this->folders_languages as $language){
                                                $this->languages = new o\languages($language->id_language);
                                                $cpt++;

                                                if($cpt > 1){
                                                    ?>
                                                    <div class="hr-line-dashed" style="margin-top: 75px;"></div>
                                                    <?php
                                                }
                                                ?>

                                                <fieldset class="form-horizontal">
                                                    <div class="row">
                                                        <h3><?=$this->languages->name?></h3>

                                                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormType ? ' has-error' : '') ?>">
                                                            <label class="col-sm-4 control-label" for="priority">
                                                                <?= $this->ln->txt('admin-folders', 'folder-import', $this->language, 'Import (620x620 with transparent background)') ?>
                                                            </label>
                                                            <div class="col-sm-12" style="margin-left:37px;">
                                                                <div class="input-group m-b field_lang_fr">
                                                                    <label class="input-group-btn">
                                                                        <button type="button" class="btn btn-primary valuefichier" id="valuefichier"><?=$this->ln->txt('admin-generic', 'select-file', $this->language, 'Séléctionner un fichier')?></button>
                                                                        <input type="file" class="hide fichierFile" name="image_<?=$language->id_language?>" id="fichierFile">
                                                                        <input type="text" class="hide" name="value_old_<?=$language->id_language?>" value="">
                                                                    </label>
                                                                    <input type="text" class="form-control fichierPath" id="fichierPath" placeholder="<?=$this->ln->txt('admin-generic', 'no-file', $this->language, 'Aucun fichier séléctionné') ?>" disabled="disabled">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                    $this->folders_languages = new o\data('folders_languages');
                                                    $this->folders_languages->addWhere('id_language = '.$language->id_language.' AND id_folder = '.$this->params['folder']);
                                                    $img_lang = $this->folders_languages->getArray();
                                                    $img_lang = $img_lang[0];
                                                    if($img_lang['image'] != ""){
                                                        ?>
                                                        <div class="hr-line-dashed"></div>

                                                        <label class="col-sm-4 control-label" for="priority"><?= $this->ln->txt('admin-folder', 'default_img', $this->language, 'Default image') ?></label>

                                                        <div class="big_preview">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>

                                                        <div class="medium_preview">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>

                                                        <div class="small_preview">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>


                                                        <div class="cl">&nbsp;</div>

                                                        <div class="hr-line-dashed"></div>

                                                        <h4><?= $this->ln->txt('admin-folder', 'preview', $this->language, 'Preview') ?></h4>

                                                        <div class="color_preview prev_red">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>

                                                        <div class="color_preview prev_yellow">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>

                                                        <div class="color_preview prev_blue">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>

                                                        <div class="color_preview prev_green">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>

                                                        <div class="color_preview prev_orange">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>

                                                        <div class="color_preview prev_purple">
                                                            <img src="<?=$this->surl.'/var/images/'.$img_lang['image']?>" class="img_preview">
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                </fieldset>
                                                <?php
                                            }
                                            ?>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-6 col-sm-offset-2">
                                                <a href="<?= $this->lurl ?>/folders/all_folders" class="btn btn-white">
                                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                                </a>
                                                <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit">
                                                    <?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?>
                                                </button>
                                                <button class="btn btn-primary" type="submit">
                                                    <?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            
                            <?php $i++; ?>
                            <div id="tab-videos" class="tab-pane <?=$this->params['part'] == $i ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->folders_videos->count().' '.$this->ln->txt('admin-folders', 'videos-in-folder', $this->language, 'Videos in folder') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            
                                            <table id="videos_table" class="table table-striped table-hover vertical-center">
                                                <thead>
                                                    <tr>
                                                        <th class="col-lg-4"><?= $this->ln->txt('admin-folders', 'title-subtitle', $this->language, 'Title & Subtitle') ?></th>
                                                        <th class="col-lg-2"><?= $this->ln->txt('admin-folders', 'language', $this->language, 'Language') ?></th>
                                                        <th class="col-lg-3"><?= $this->ln->txt('admin-folders', 'thumbnail', $this->language, 'Thumbnail') ?></th>
                                                        <th class="col-lg-1"><?= $this->ln->txt('admin-folders', 'active', $this->language, 'Active') ?></th>
                                                        <th class="col-lg-1"><?= $this->ln->txt('admin-folders', 'premium', $this->language, 'Premium') ?></th>
                                                        <th class="col-lg-3"><?= $this->ln->txt('admin-folders', 'folders', $this->language, 'Folders') ?></th>
                                                        <th class="col-lg-3"><?= $this->ln->txt('admin-folders', 'actions', $this->language, 'Actions') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="videos_list" class="sortable">
                                                    <?php
                                                    if($this->folders_videos->count() > 0) {
                                                        foreach ($this->folders_videos as $v){
                                                            $this->videos = new o\videos($v->id_video);
                                                            $this->languages = new o\languages($this->videos->id_language);

                                                            $this->folders_video_temp = new o\data('folders_videos');
                                                            $this->folders_video_temp->addWhere('id_video = '.$this->videos->id_video);

                                                            ?>
                                                            <tr>
                                                                <td><?=$this->videos->title?><br /><i><?=$this->videos->subtitle?></i></td>
                                                                <td><?=$this->languages->name?></td>
                                                                <td>Thumbnail</td>
                                                                <td>
                                                                    <?php
                                                                    switch($this->videos->active){
                                                                        case 0:
                                                                            echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-folders', 'form-folder-status-off', $this->language, 'Off').'"></i>';
                                                                            break;
                                                                        case 1:
                                                                            echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-folders', 'form-folder-status-on', $this->language, 'Active').'"></i>';
                                                                            break;
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    switch($this->videos->premium){
                                                                        case 0:
                                                                            echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-folders', 'form-folder-status-off', $this->language, 'Off').'"></i>';
                                                                            break;
                                                                        case 1:
                                                                            echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-folders', 'form-folder-status-on', $this->language, 'Active').'"></i>';
                                                                            break;
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                    $sep = "";
                                                                    foreach ($this->folders_video_temp as $vd){
                                                                        $this->folders_temp = new o\folders($vd->id_folder);
                                                                        echo $this->folders_temp->title;
                                                                        $sep = ", ";
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <a href="<?=$this->lurl?>/videos/formVideo/video___<?=$v->id_video?>" class="btn btn-primary btn-sm btn-action">
                                                                        <i class="fa fa-eye"></i> <?= $this->ln->txt('admin-folders', 'video-details', $this->language, 'Détails') ?>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    else{
                                                        ?>
                                                        <tr id="empty_commande_list" <?=(!empty($this->clients->transactions) && $this->clients->transactions->count() > 0)?'style="display:none;"':''?>>
                                                            <td colspan="4">
                                                                <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                                                                <span class="font-bold" style="margin-left: 15px;">
                                                                <?= $this->ln->txt('admin-folders', 'empty-video', $this->language, 'There are no video for the moment') ?>
                                                            </span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>

                                                </tbody>
                                            </table>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/clients/gestions" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">

    function initialiseJqueryPlugins() {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});

        // Suppression de la promotion /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.confirmAlert').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-catalogue', 'delete-client-alert', $this->language, 'Confirmez vous la suppression du client ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/clients/deleteClient/<?= $_POST['id_client'] ?>');
                    return false;
                }
            });
        });

        // Initialisation des datepickers /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.datePicker').datepicker({
            todayBtn: false,
            language: "<?= $this->language ?>",
            format: "<?= $this->clSettings->getParam('format-datepicker', 'sets') ?>",
            todayHighlight: false,
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    }

    // Réinitialisation des plugins JQ /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Nécéssaire au changmeent de tabs car certain element initialisé alors que non visible seront mal initialisés
    $(document).on('click', '.tabs-left a', function(){
        initialiseJqueryPlugins();
    });

    $(document).on('click', '#generate_password', function(){
        $(this).find('i').addClass('fa-spin');
        var _this = $(this);

        $.ajax('<?= $this->lurl ?>/clients/generatePassword/10',
        {
            success:function(data){
                $('#password_chromehack').val(data);
                setTimeout(function(){_this.find('i').removeClass('fa-spin');}, 620);
            }
        });
    });
    $(document).on('mousedown', '#see_password', function(){
        $('#password_chromehack').attr('type', 'text');
    }).on('mouseup blur drag', '#see_password', function(){
        $('#password_chromehack').attr('type', 'password');
    });

    $(document).on('click', '#copy_password', function(){
        var $temp = $("<input>");

        $("body").append($temp);
        $temp.val($('#password_chromehack').val()).select();

        document.execCommand("copy");

        $temp.remove();
        toastr["success"]('<?= $this->ln->txt('admin-generic', 'copied-to-clipboard', $this->language, 'Le mot de passe a bien été copié dans le presse-papier')?>');
    });
    $(document).ready(function () {
        $('.valuefichier').unbind().click(function () {
            $(this).siblings('.fichierFile').trigger('click');
        });
        $('.fichierFile').on('change', function () {
            if ($(this).val() !== '') {
                $(this).parent().next('.fichierPath').attr('placeholder', $(this).val());
            } else {
                $(this).parent().next('.fichierPath').attr('placeholder', "<?= $this->ln->txt('admin-generic', 'no-file', $this->language, 'Aucun fichier séléctionné') ?>");
            }
        });

        initialiseJqueryPlugins();
    });
</script>