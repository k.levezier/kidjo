<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-folders', 'folders_per_age', $this->language, 'Folders per age') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?= $this->ln->txt('admin-folders', 'folders', $this->language, 'Folders') ?>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content pt40">
                    <form action="<?= $this->lurl . '/folders/folders_per_age' ?>"
                          method="POST"
                          class="form-horizontal form-search-events">
                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label">
                                        <?= $this->ln->txt('admin-folders', 'label-language', $this->language, 'Language') ?>
                                    </label>

                                    <div class="col-lg-9">
                                        <select id="folder-language" name="folder-language[]"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-language', $this->language, 'Language') ?>"
                                                class="chosen-select  form-control" tabindex="3" multiple="multiple"
                                                style="height: 110px;">
                                            <option value="0" <?=(in_array('0',$_SESSION['filter_folder']['folder-language'])?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->languages as $lang){
                                                ?>
                                                <option value="<?= $lang->id_language?>"
                                                        <?=(in_array($lang->id_language,$_SESSION['filter_folder']['folder-language'])?'selected':'')?>>
                                                    <?= $lang->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label"><?= $this->ln->txt('admin-folders', 'label-type', $this->language, 'Type') ?></label>

                                    <div class="col-lg-9">
                                        <select id="folder-type" name="folder-type"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-type', $this->language, 'Type') ?>"
                                                class="chosen-select  form-control">
                                            <option value="0" <?=(in_array('0',$_SESSION['filter_folder']['folder-type'])?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->folders_type as $type){
                                                ?>
                                                <option value="<?= $type->id_folder_type?>"
                                                        <?=(in_array($type->id_folder_type,$_SESSION['filter_folder']['folder-type'])?'selected':'')?>>
                                                    <?= $type->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>


                            </div>


                            <div class="col-lg-6">
                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label"><?= $this->ln->txt('admin-folders', 'label-countries', $this->language, 'Countries') ?></label>

                                    <div class="col-lg-9">
                                        <select id="folder-language" name="folder-countries[]"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-countries', $this->language, 'Countries') ?>"
                                                class="chosen-select  form-control" tabindex="3" multiple="multiple"
                                                style="height: 110px;">
                                            <option value="0" <?=(in_array('0',$_SESSION['filter_folder']['folder-countries'])?'selected':'')?>>All</option>

                                            <?php
                                            foreach($this->countries as $country){
                                                ?>
                                                <option value="<?= $country->id_pays?>"
                                                        <?=(in_array($country->id_pays,$_SESSION['filter_folder']['folder-countries'])?'selected':'')?>>
                                                    <?= $country->en ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label" style="padding-top: 0px;"><?= $this->ln->txt('admin-folders', 'label-media-type', $this->language, 'Media type') ?></label>

                                    <div class="col-lg-9">
                                        <select id="folder-media-type" name="folder-media-type"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-media-type', $this->language, 'Media type') ?>"
                                                class="chosen-select  form-control">
                                            <option value="0" <?=($_SESSION['filter_folder']['folder-media-type'] == 0?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->folders_media_type as $media_type){
                                                ?>
                                                <option value="<?= $media_type->id_media_type?>"
                                                        <?=($_SESSION['filter_folder']['folder-media-type'] == $media_type->id_media_type ?'selected':'')?>>
                                                    <?= $media_type->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 align-center mt20" style="text-align: center;">
                                <input type="hidden" name="filter" value="ok">
                                <button class="btn btn-primary uppercase" type="submit">
                                    <?= $this->ln->txt('admin-folder', 'filter', $this->language, 'Filter') ?>
                                </button>
                                <br>
                                <a href="<?= $this->lurl ?>/folders/folder_per_age/reset">
                                    <?= $this->ln->txt('admin-folder', 'reset-filters', $this->language, 'Reset filters') ?>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <?php
                    foreach ($this->tab_onglets_age as $key => $onglet)
                    {
                        ?>
                        <li <?php if ($key == 0) {echo 'class="active"';} ?>><a data-toggle="tab" href="#tab-<?= $key ?>"> <?= $onglet ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
                <div class="tab-content">
                    <?php
                    if(count($this->tabAgeFolders) > 0){
                        foreach ($this->tabAgeFolders as $age_index => $tabFolders) {
                            $tabFolders = $tabFolders['folders'];
                            ?>
                            <div id="tab-<?= $age_index ?>" class="tab-pane <?php if ($age_index == 0) {
                                echo 'active';
                            } ?>">
                                <div class="panel-body" style="height: auto; padding-bottom: 5%;">
                                    <h2 class="dib"><?=count($tabFolders)?> <?= $this->ln->txt('admin-folders', 'folder-by-order', $this->language, 'folders by order') ?><br /></h2>

                                    <div class="content_listing_folder">
                                        <?php
                                        $tabColor = array('prev_yellow','prev_blue','prev_red', 'prev_green', 'prev_orange', 'prev_purple');
                                        $cpt = 0;
                                        foreach($tabFolders as $fd){
                                            unset($folder_lang);
                                            $this->folders_languages = new o\data('folders_languages');
                                            $this->folders_languages->addWhere('id_folder = ' . $fd['id_folder'].' AND image != ""');
                                            $folder_lang = $this->folders_languages->getArray();
                                            $folder_lang = $folder_lang[0];

                                            $this->folders_videos = new o\data('folders_videos');
                                            $this->folders_videos->addWhere('id_folder = ' . $fd['id_folder']);

                                            $this->folders_media_type = new o\folders_media_type(array('id_media_type' => $fd['id_media_type']));

                                            ?>
                                            <div class="bloc_folder <?=$tabColor[$cpt]?>">
                                                <img src="<?=$this->surl.'/var/images/'.$folder_lang['image']?>" class="img_preview_age">
                                                <div class="bloc_details_age">
                                                    <h4>
                                                        <a href="<?=$this->lurl?>/folders/formFolder/folder___<?=$fd['id_folder']?>">
                                                            <?=$folder_lang['title_folder']?>
                                                        </a>
                                                    </h4>
                                                    <span>
                                                    <?= $this->ln->txt('admin-folders', 'video-in-folder', $this->language, 'Videos in folder :').' '.$this->folders_videos->count() ?><br />
                                                    <?= $this->ln->txt('admin-folders', 'media-type-folder-age', $this->language, 'Media type :').' '.$this->folders_media_type->name ?>
                                                </span>
                                                </div>
                                            </div>
                                            <?php
                                            $cpt++;
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }else{
                        ?>
                        <div><?= $this->ln->txt('admin-folder', 'no-folder-age', $this->language, 'No folder') ?></div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});

        <?php
        if($_SESSION['filter_folder']['active_folder'] == "1"){
        ?>
        $('.i-checks').iCheck('check'); //To check the radio button
        <?php
        }
        ?>

        $('.confirm').on('click', function () {
            var _id = $(this).data('idfolder');
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-log-alert', $this->language, 'Confirmez vous la suppression de l\'enregistrement ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/folders/deleteFolder/folder___'+ _id);
                        return false;
                    }
                });
        });
    });
</script>