<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-folders', 'allfolders', $this->language, 'All folders') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?= $this->ln->txt('admin-folders', 'folders', $this->language, 'Folders') ?>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/folders/formFolder" class="btn btn-primary">
                <?= $this->ln->txt('admin-folder', 'addfolder', $this->language, 'Create a new folder') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content pt40">
                    <form action="<?= $this->lurl . '/folders/all_folders' ?>"
                          method="POST"
                          class="form-horizontal form-search-events">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <div class="col-lg-9" style="margin-left: 10%; width: 90%;">
                                        <input name="title" id="title" type="text" class="form-control"
                                               placeholder="<?= $this->ln->txt('admin-folders', 'label-search-folder', $this->language, 'Search a folder') ?>"
                                                value="<?=$_SESSION['filter_folder']['title']?>"
                                        >
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label">
                                        <?= $this->ln->txt('admin-folders', 'label-language', $this->language, 'Language') ?>
                                    </label>

                                    <div class="col-lg-9">
                                        <select id="folder-language" name="folder-language[]"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-language', $this->language, 'Language') ?>"
                                                class="chosen-select  form-control" tabindex="3" multiple="multiple"
                                                style="height: 110px;">
                                            <option value="0" <?=(in_array('0',$_SESSION['filter_folder']['folder-language'])?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->languages as $lang){
                                                ?>
                                                <option value="<?= $lang->id_language?>"
                                                    <?=(in_array($lang->id_language,$_SESSION['filter_folder']['folder-language'])?'selected':'')?>>
                                                    <?= $lang->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label"><?= $this->ln->txt('admin-folders', 'label-countries', $this->language, 'Countries') ?></label>

                                    <div class="col-lg-9">
                                        <select id="folder-language" name="folder-countries[]"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-countries', $this->language, 'Countries') ?>"
                                                class="chosen-select  form-control" tabindex="3" multiple="multiple"
                                                style="height: 110px;">
                                            <option value="0" <?=(in_array('0',$_SESSION['filter_folder']['folder-countries'])?'selected':'')?>>All</option>

                                            <?php
                                            foreach($this->countries as $country){
                                                ?>
                                                <option value="<?= $country->id_pays?>"
                                                    <?=(in_array($country->id_pays,$_SESSION['filter_folder']['folder-countries'])?'selected':'')?>>
                                                    <?= $country->en ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <div class="i-checks">
                                                <label for="active-folder"
                                                       class="col-lg-6 control-label"><?= $this->ln->txt('admin-folders', 'label-active-folder', $this->language, 'Active folder') ?></label>
                                                <div class="col-lg-3" style="margin-top: 10px;">
                                                    <input name="active_folder" id="active_folder"
                                                           class="form-control"
                                                           type="checkbox"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label"><?= $this->ln->txt('admin-folders', 'label-type', $this->language, 'Type') ?></label>

                                    <div class="col-lg-9">
                                        <select id="folder-type" name="folder-type[]"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-type', $this->language, 'Type') ?>"
                                                class="chosen-select  form-control" tabindex="3" multiple="multiple"
                                                style="height: 110px;">
                                            <option value="0" <?=(in_array('0',$_SESSION['filter_folder']['folder-type'])?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->folders_type as $type){
                                                ?>
                                                <option value="<?= $type->id_folder_type?>"
                                                    <?=(in_array($type->id_folder_type,$_SESSION['filter_folder']['folder-type'])?'selected':'')?>>
                                                    <?= $type->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="folder-language"
                                           class="col-lg-3 control-label" style="padding-top: 0px;"><?= $this->ln->txt('admin-folders', 'label-media-type', $this->language, 'Media type') ?></label>

                                    <div class="col-lg-9">
                                        <select id="folder-media-type" name="folder-media-type"
                                                data-placeholder="<?= $this->ln->txt('admin-folders', 'placeholder-media-type', $this->language, 'Media type') ?>"
                                                class="chosen-select  form-control" tabindex="3">
                                            <option value="0" <?=($_SESSION['filter_folder']['folder-media-type'] == 0?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->folders_media_type as $media_type){
                                                ?>
                                                <option value="<?= $media_type->id_media_type?>"
                                                    <?=($_SESSION['filter_folder']['folder-media-type'] == $media_type->id_media_type ?'selected':'')?>>
                                                    <?= $media_type->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 align-center mt20" style="text-align: center;">
                                <input type="hidden" name="filter" value="ok">
                                <button class="btn btn-primary uppercase" type="submit">
                                    <?= $this->ln->txt('admin-folder', 'filter', $this->language, 'Filter') ?>
                                </button>
                                <br>
                                <a href="<?= $this->lurl ?>/folders/all_folders/reset">
                                    <?= $this->ln->txt('admin-folder', 'reset-filters', $this->language, 'Reset filters') ?>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?=count($this->LFolders)?>&nbsp;<?= $this->ln->txt('admin-folders', 'existing-folders', $this->language, 'existing folders') ?><span class="badge badge-primary"></span></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <?php
                    if (count($this->LFolders) > 0) {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                <tr>
                                    <th class="col-lg-2"><?= $this->ln->txt('admin-folders', 'folder-title', $this->language, 'Title') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-folders', 'folder-language', $this->language, 'Language') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-folders', 'folder-countries', $this->language, 'Countries') ?></th>
                                    <th class="col-lg-2"><?= $this->ln->txt('admin-folders', 'folder-type', $this->language, 'Type') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-folders', 'folder-active', $this->language, 'Active') ?></th>
                                    <th class="col-lg-3" style="text-align: center;"><?= $this->ln->txt('admin-folders', 'folder-videos-compiles', $this->language, 'Vidéos actives / Compiles actives') ?></th>
                                    <th class="col-lg-2"><?= $this->ln->txt('admin-folders', 'folder-actions', $this->language, 'Actions') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($this->LFolders as $f) {
                                    $l_countries_folders = new o\data('folders_countries');
                                    $l_countries_folders->addWhere('id_folder = '.$f['id_folder']);

                                    $l_languages_folders = new o\data('folders_languages');
                                    $l_languages_folders->addWhere('id_folder = '.$f['id_folder']);

                                    ?>
                                    <tr>
                                        <td>
                                            <?=$f['title']?>
                                        </td>
                                        <td>
                                            <?php
                                            $sep = "";
                                            if($l_languages_folders->count() > 0) {
                                                foreach ($l_languages_folders as $language_folder) {
                                                    if (is_null($language_folder->id_language)) {
                                                        echo "None";
                                                    } else {
                                                        $this->languages = new o\languages($language_folder->id_language);
                                                        echo $sep . $this->languages->name;
                                                        $sep = ",";
                                                    }
                                                }
                                            }else{
                                                echo "None";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $sep = "";
                                            if($l_countries_folders->count() > 0) {
                                                foreach($l_countries_folders as $country_folder){
                                                    if(is_null($country_folder->id_country)){
                                                        echo "None";
                                                    }else{
                                                        $this->countries = new o\countries($country_folder->id_country);
                                                        echo $sep.$this->countries->en;
                                                        $sep=",";
                                                    }
                                                }
                                            }else{
                                                echo "None";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?=$f['type']?>
                                        </td>
                                        <td>
                                            <?php
                                            switch($f['status']){
                                                case 0:
                                                    echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-folders', 'form-folder-status-off', $this->language, 'Off').'"></i>';
                                                    break;
                                                case 1:
                                                    echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-folders', 'form-folder-status-on', $this->language, 'Active').'"></i>';
                                                    break;
                                            }
                                            ?>
                                        </td>
                                        <td class="center larger">
                                            <span class="badge badge-danger">0 </span> Vidéo / <span class="badge badge-primary">1</span> Compile
                                        </td>
                                        <td>
                                            <a href="<?=$this->lurl?>/folders/formFolder/folder___<?=$f['id_folder']?>" class="btn btn-white btn-action">
                                                <?= $this->ln->txt('admin-folders', 'folder-details', $this->language, 'Détails') ?>
                                            </a>
                                            <a class="btn btn-danger confirm" data-idfolder="<?= $f['id_folder'] ?>">
                                                <?= $this->ln->txt('admin-administration', 'delete-folder', $this->language, 'Delete') ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                        <span class="font-bold" style="margin-left: 15px;"><?= $this->ln->txt('admin-folders', 'empty-folder', $this->language, 'No folder') ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});

        <?php
        if($_SESSION['filter_folder']['active_folder'] == "1"){
            ?>
            $('.i-checks').iCheck('check'); //To check the radio button
            <?php
        }
        ?>

        $('.confirm').on('click', function () {
            var _id = $(this).data('idfolder');
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-log-alert', $this->language, 'Confirmez vous la suppression de l\'enregistrement ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/folders/deleteFolder/folder___'+ _id);
                        return false;
                    }
                });
        });
    });
</script>