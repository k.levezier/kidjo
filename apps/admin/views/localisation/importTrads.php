<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-localisation', 'traductions', $this->language, 'Traductions') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/localisation"><?= $this->ln->txt('admin-localisation', 'localisation', $this->language, 'Localisation') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/localisation/traductions"><?= $this->ln->txt('admin-localisation', 'traductions', $this->language, 'Traductions') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-localisation', 'import-traductions', $this->language, 'Import des traductions') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/localisation/traductions" class="btn btn-primary">
                <?= $this->ln->txt('admin-localisation', 'back-traductions', $this->language, 'Retour aux traductions') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <form method="POST" class="form-horizontal" name="formImport" id="formImport" enctype="multipart/form-data">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><?= $this->ln->txt('admin-localisation', 'form-import-traductions', $this->language, 'Formulaire d\'import des traductions') ?></h5>
                    </div>
                    <div class="ibox-content">
                        <p class="text-danger"><?= $this->ln->txt('admin-localisation', 'alert-import', $this->language, 'Attention ! Toutes les traductions du site et de l\'administration seront remplacées par celles contenues dans ce fichier.') ?></p>
                        <input type="hidden" name="sendForm"/>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group<?= (isset($_POST['sendForm']) && $this->error ? ' has-error' : '') ?>">
                            <label class="col-sm-2 control-label" for="valuefichier"><?= $this->ln->txt('admin-localisation', 'lab-fichier', $this->language, 'Fichier') ?></label>
                            <div class="col-sm-10">
                                <div class="input-group m-b">
                                    <label class="input-group-btn">
                                        <button type="button" class="btn btn-primary" id="valuefichier"><?= $this->ln->txt('admin-generic', 'select-file', $this->language, 'Séléctionner un fichier') ?></button>
                                        <input type="file" class="hide" name="csv" id="fichierFile">
                                    </label>
                                    <input type="text" class="form-control" id="fichierPath" placeholder="<?= $this->ln->txt('admin-generic', 'no-file', $this->language, 'Aucun fichier séléctionné') ?>" disabled="disabled">
                                </div>
                                <?= (isset($_POST['sendForm']) && $this->error ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-12 col-sm-offset-2">
                                <a href="<?= $this->lurl ?>/localisation/traductions" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#valuefichier').on('click', function () {
            $('#fichierFile').trigger('click');
        });

        $('#fichierFile').on('change', function () {
            if ($(this).val() !== '') {
                $('#fichierPath').attr('placeholder', $(this).val());
            } else {
                $('#fichierPath').attr('placeholder', "<?= $this->ln->txt('admin-generic', 'no-file', $this->language, 'Aucun fichier séléctionné') ?>");
            }
        });
    });
</script> 
