<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-localisation', 'traductions', $this->language, 'Traductions') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/localisation"><?= $this->ln->txt('admin-localisation', 'localisation', $this->language, 'Localisation') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-localisation', 'traductions', $this->language, 'Traductions') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a class="btn btn-primary confirm">
                <?= $this->ln->txt('admin-localisation', 'add-trad', $this->language, 'Ajouter une traduction') ?>
            </a>
            <a class="btn btn-success" href="<?= $this->lurl ?>/localisation/exportTrads">
                <?= $this->ln->txt('admin-localisation', 'export-trad', $this->language, 'Exporter les traductions') ?>
            </a>
            <a class="btn btn-warning" href="<?= $this->lurl ?>/localisation/importTrads">
                <?= $this->ln->txt('admin-localisation', 'import-trad', $this->language, 'Importer les traductions') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-localisation', 'liste-traductions', $this->language, 'Liste des traductions') ?></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form name="filtresTab" id="filtresTab" method="POST">
                            <input type="hidden" name="sendFiltresTab"/>
                            <div class="col-sm-3 m-b-xs">
                                <select class="input-sm form-control input-s-sm inline zeroPaddingTopBot" name="section" onchange="$('#filtresTab').submit();">
                                    <option value=""><?= $this->ln->txt('admin-localisation', 'choisir-section', $this->language, 'Choisir une section') ?></option>
                                    <?php foreach ($this->lSections as $key => $section) { ?>
                                        <option value="<?= $section ?>"<?= (isset($_POST['section']) && $_POST['section'] == $section ? ' selected' : '') ?>><?= $section ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-6">&nbsp;</div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" placeholder="<?= $this->ln->txt('admin-generic', 'recherche', $this->language, 'Recherche...') ?>" class="input-sm form-control" name="filtre" value="<?= urldecode($_POST['filtre']) ?>">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary" name="btnSendForm"> <?= $this->ln->txt('admin-generic', 'ok', $this->language, 'OK') ?></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (!empty($_POST['section']) && count($this->lTraductions) > 0) { ?>
        <form method="POST" class="form-horizontal" name="formTradSection" id="formTradSection" enctype="multipart/form-data">
            <div class="row">
                <input type="hidden" name="sendFormSection"/>
                <input type="hidden" name="restePage" id="restePage" value="0"/>
                <input type="hidden" name="section" value="<?= $_POST['section'] ?>"/>
                <?php foreach ($this->lLangues as $key => $lng) { ?>
                    <div class="col-lg-<?= (count($this->lLangues) > 1 ? (count($this->lLangues) == 2 ? '6' : '4') : '12') ?>">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h5><?= $this->ln->txt('admin-localisation', 'section', $this->language, 'Section :') ?> <?= $_POST['section'] ?></h5>
                                <div class="ibox-tools">
                                    <img src="<?= $this->surl ?>/images/admin/flags/<?= $key ?>.png"/>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <?php foreach ($this->lTraductions[$key] as $trad) { ?>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="id_traduction_<?= $trad['id_traduction'] ?>" value="<?= $trad['id_traduction'] ?>"/>
                                            <label class="control-label-top" for="texte_<?= $trad['id_traduction'] ?>"><?= $trad['nom'] ?></label>
                                            <textarea class="form-control" name="texte_<?= $trad['id_traduction'] ?>" id="texte_<?= $trad['id_traduction'] ?>"><?= $trad['texte'] ?></textarea>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="form-group no-margins center">
                                <a href="<?= $this->lurl ?>/localisation/traductions" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePage').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php } elseif (!empty($_POST['section']) && count($this->lTraductions) == 0) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if (!empty($_POST['filtre']) && empty($_POST['section']) && count($this->lTradSections) > 0) { ?>
        <form method="POST" class="form-horizontal" name="formTradGlobal" id="formTradGlobal" enctype="multipart/form-data">
            <div class="row">
                <input type="hidden" name="sendFormGlobal"/>
                <input type="hidden" name="restePageGlobal" id="restePageGlobal" value="0"/>
                <input type="hidden" name="filtre" value="<?= $_POST['filtre'] ?>"/>
                <?php foreach ($this->lTradSections as $sec => $list) { ?>
                    <?php foreach ($this->lLangues as $key => $lng) { ?>
                        <div class="col-lg-<?= (count($this->lLangues) > 1 ? (count($this->lLangues) == 2 ? '6' : '4') : '12') ?>">
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5><?= $this->ln->txt('admin-localisation', 'section', $this->language, 'Section :') ?> <?= $sec ?></h5>
                                    <div class="ibox-tools">
                                        <img src="<?= $this->surl ?>/images/admin/flags/<?= $key ?>.png"/>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <?php foreach ($list[$key] as $trad) { ?>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <input type="hidden" name="id_traduction_<?= $trad['id_traduction'] ?>" value="<?= $trad['id_traduction'] ?>"/>
                                                <label class="control-label-top" for="texte_<?= $trad['id_traduction'] ?>"><?= $trad['nom'] ?></label>
                                                <textarea class="form-control" name="texte_<?= $trad['id_traduction'] ?>" id="texte_<?= $trad['id_traduction'] ?>"><?= $trad['texte'] ?></textarea>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="form-group no-margins center">
                                <a href="<?= $this->lurl ?>/localisation/traductions" class="btn btn-white">
                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                </a>
                                <button onClick="$('#restePageGlobal').val(1);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    <?php } elseif (!empty($_POST['filtre']) && empty($_POST['section']) && count($this->lTradSections) == 0) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <p><?= $this->ln->txt('admin-generic', 'aucune-donnees', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.confirm').on('click', function () {
            swal({
                    html: true,
                    title: "<?= $this->ln->txt('admin-localisation', 'add-trad', $this->language, 'Ajouter une traduction') ?>",
                    text: "<?= $this->ln->txt('admin-localisation', 'add-trad-explication', $this->language, 'Pour ajouter une traduction il faut déclarer cette dernière directement dans la vue de cette façon :<br /><strong>$this->ln->txt(\'nom-section\', \'nom-traduction\', $this->language, \'Texte de la traduction\')</strong>') ?>",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'fermer', $this->language, 'Fermer') ?>",
                    cancelButtonText: "",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        return false;
                    }
                });
        });
    });
</script> 
