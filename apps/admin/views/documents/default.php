<script>
    function sort(content)
    {
        document.location = '/documents/sort/'+content;
    }
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-documents', 'liste-documents', $this->language, 'Tous les documents') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-4">

        <div class="title-action">
            <a href="<?= $this->lurl ?>/documents/xport" class="btn btn-info col-lg-2 btn-block" target="_blank"><?= $this->ln->txt('admin-documents', 'xport-doc', $this->language, 'Exporter les documents') ?></a>
            <a href="<?= $this->lurl ?>/documents/add" class="btn btn-primary col-lg-2 btn-block"><?= $this->ln->txt('admin-documents', 'add-doc', $this->language, 'Ajouter un document') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-documents', 'liste-doctuments', $this->language, 'Tous les documents') ?>&nbsp;<span class="badge badge-primary"><?=$this->documents->count()?></span></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="POST">
                        <div class="col-lg-12" style="clear:both">
                            <div class="col-lg-3">
                                <input type="text" name="keywords" class="form-control" placeholder="Keywords" value="<?=$_SESSION['keywords']?>"/>
                            </div>
                            <div class="col-lg-3">
                                <select class="form-control" name="type" placeholder="File type">
                                    <option value="0">File type</option>
                                    <?php foreach($this->doctypes as $type) { ?> <option value="<?=$type->id_doctype?>" <?=($_SESSION['type']==$type->id_doctype?'selected="selected"':'')?>><?=$type->name?></option> <? } ?>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <select class="form-control" name="user" placeholder="Author">
                                    <option value="0">Author</option>
                                    <?php foreach($this->lUsers as $type) { ?> <option value="<?=$type->id_user?>" <?=($_SESSION['id_user']==$type->id_user?'selected="selected"':'')?>><?=$type->firstname?> <?=$type->name?></option> <? } ?>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <label for="offline"><input type="checkbox" name="offline" id="offline" value="1" <?=($_SESSION['offline']==1?' checked="checked"':'')?>/> Inclure les documents hors ligne</label>
                            </div>
                            <div class="col-lg-1">
                                <input type="submit" class="btn btn-primary" value="Appliquer" name="filter"/>
                            </div>
                        </div>
                    </form>
                    <?php
                    if ($this->documents->count() > 0) {
                        ?>
                    
                        <div class="table-responsive" style="clear:both">
                            <table class="table table-striped table-hover vertical-center" id="tab_docs">
                                <thead>
                                    <tr>
                                        <th style="width:10px"><input type="checkbox" name="all" onclick="javascript:checkAll('tab_docs',this.checked);"></th>
                                        <th class="col-lg-3">
                                            <?= $this->ln->txt('admin-documents', 'documents-nom', $this->language, 'Nom') ?>
                                            <?php $this->sort('label') ?>
                                        </th>
                                        <th class="col-lg-1">
                                            <?= $this->ln->txt('admin-documents', 'documents-type', $this->language, 'Type') ?>
                                            <?php $this->sort('id_doctype') ?>
                                        </th>
                                        <th class="col-lg-1">
                                            <?= $this->ln->txt('admin-documents', 'documents-auteur', $this->language, 'Auteur') ?>
                                            <?php $this->sort('id_user') ?>
                                        </th>
                                        <th class="col-lg-3">
                                            <?= $this->ln->txt('admin-documents', 'documents-fichiers', $this->language, 'Fichiers') ?>
                                        </th>
                                        <th class="col-lg-1">
                                            <?= $this->ln->txt('admin-documents', 'documents-marque', $this->language, 'Marque') ?>
                                            <?php $this->sort('id_tree') ?>
                                        </th>
                                        <th class="col-lg-1">
                                            <?= $this->ln->txt('admin-documents', 'documents-expiration', $this->language, 'Expiration') ?>
                                            <?php $this->sort('updated') ?>
                                        </th>
                                        <th class="col-lg-1" style="text-align: center;">
                                            <?= $this->ln->txt('admin-documents', 'documents-statut', $this->language, 'Statut') ?>
                                            <?php $this->sort('status') ?>
                                        </th>
                                        <th class="col-lg-1">
                                            <?= $this->ln->txt('admin-documents', 'documents-actions', $this->language, 'Actions') ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($this->documents as $document) {
                                        if($document->id_tree==0)
                                        {
                                            $this->documents_tree = new o\data('documents_tree', array('id_document'=>$document['id_document']));
                                            foreach($this->documents_tree as $tree)
                                            {

                                                $bc = $this->tree->getBreadCrumb($tree['id_tree'],$this->language);
                                                $document->id_tree = $bc[1]->id_tree;
                                            }
                                            $document->save();
                                        }
                                    }
                                    foreach ($this->documents as $document) {
                                        $o++;
                                        if($o>500)
                                            break;
                                        $marques =array();
                                        $this->documents_tree = new o\data('documents_tree', array('id_document'=>$document['id_document']));
                                        $this->doctypes = new o\doctypes($document['id_doctype']);
                                        foreach($this->documents_tree as $tree)
                                        {

                                            $bc = $this->tree->getBreadCrumb($tree['id_tree'],$this->language);
                                            if(!in_array($bc[1]->menu_title,$marques))
                                            {
                                                $marques[]  = $bc[1]->menu_title;
                                                if($bc[1]->id_tree==4) // BB 22
                                                {
                                                $sql = 'DELETE FROM documents_geozones WHERE id_document='.$document['id_document'].' AND id_geozone=17';
                                                //$this->bdd->query($sql);
                                                //echo $sql;
                                                $sql = 'INSERT IGNORE INTO documents_geozones(id_document,id_geozone) VALUES('.$document['id_document'].',22)';
                                                //$this->bdd->query($sql);
                                                }
                                                if($bc[1]->id_tree==6) // RST 18
                                                {
                                                    $sql = 'DELETE FROM documents_geozones WHERE id_document='.$document['id_document'].' AND id_geozone=17';
                                                    //$this->bdd->query($sql);
                                                     $sql = 'INSERT IGNORE INTO documents_geozones(id_document,id_geozone) VALUES('.$document['id_document'].',18)';
                                                    //$this->bdd->query($sql);
                                                }
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" name="cb-<?=$document->id_document?>" value="<?=$document->id_document?>"></td>
                                            <td title="<?=$document->id_document?> - <?=$document->updated?> <?=$document->description?> ">
                                                <?=$document->label?><br/>
                                                <!-- <?=$document->id_document?><br/>
                                                Updated: <?=$document->updated?>
                                                <br/>
                                                Added: <?=$document->added?> -->
                                            </td>
                                            <td>
                                                <?=$this->doctypes->name?>
                                            </td>
                                            <td>
                                                <?=$document->user->firstname?> <?=$document->user->name?>
                                            </td>
                                            <td>
                                                <?php foreach(['en','fr','es'] as $ln){ $name = 'name_'.$ln; $size = 'size_'.$ln;
                                                    if(!empty($document->$name)){ ?>
                                                        <?=$document->$name?> <i>(<?=$this->clDocuments->showSize($document->$size)?>)</i><br/>
                                                    <?php }
                                                } ?>
                                            </td>
                                            <td>
                                                <?=implode(',',$marques)?>
                                            </td>
                                            <td>
                                                <?=($document->expires!='0000-00-00 00:00:00'?(new DateTime($document->expires))->format($this->clSettings->getParam('format-de-date', 'sets')):'')?>
                                            </td>
                                            <td style="text-align: center;">
                                              <?php
                                                switch($document->status){
                                                    case 0:
                                                        echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-documents', 'document-offline', $this->language, 'Offline').'"></i>';
                                                        break;
                                                    case 1:
                                                        echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-documents', 'document-online', $this->language, 'Online').'"></i>';
                                                        break;
                                                }
                                                ?>
                                            </td>

                                            <td>
                                                <a href="<?=$this->lurl?>/documents/add/<?=$document->id_document?>" class="btn btn-white btn-sm floatLeft btn-action">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-documents', 'list-promo-edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <a href="#" class="btn btn-danger" onclick="return multipleDelete()">Archiver les documents sélectionnés</a>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                        <span class="font-bold" style="margin-left: 15px;"><?= $this->ln->txt('admin-documents', 'empty-docs', $this->language, 'Il n\'y a aucun document pour le moment') ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function checkAll(container_id, state) {
        var checkboxes = document.getElementById(container_id).getElementsByTagName('input');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                checkboxes[i].checked = state;
            }
        }
        return true;
    }
    function whosChecked(container_id) {
        var output = "";
        var checkboxes = document.getElementById(container_id).getElementsByTagName('input');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox') {
                if (checkboxes[i].checked == true && checkboxes[i].name != 'all') {
                    output = output + checkboxes[i].value + ","
                }
            }
        }
        return output;
    }

    function multipleDelete() {
        if (!confirm('Etes vous sur ?')) {
            return false;
        }
        if (whosChecked('tab_docs') == '') {
            return false;
        }
        document.location = '/documents/archive/' + whosChecked('tab_docs').substring(0, whosChecked('tab_docs').length - 1);
    }
</script>