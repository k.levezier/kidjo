<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-documents', 'type', $this->language, 'Type de document') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-documents', 'geozones-'.($this->new?'add':'edit'), $this->language, ($this->new?'Ajout':'Modification').' d\'une zone') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/documents/geozones' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-documents', 'back-geozones', $this->language, 'Retour aux zones') ?>
            </a>
            <?php
            if (!$this->new) {
                ?>
                <a class="btn btn-danger confirmAlert">
                    <?= $this->ln->txt('admin-documents', 'del-geozone', $this->language, 'Supprimer la zone') ?>
                </a>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">          
            <form method="POST" name="formPromotion" id="formPromotion" enctype="multipart/form-data" action="<?=$this->lurl.'/documents/addGeozone/'.$this->geozones->id_geozone.'/'?>">
                <input type="hidden" name="sendForm" />
                <input type="hidden" name="restePage" id="restePage" value="0" />
                <input type="hidden" name="part" id="part" value="1" />
                <input type="hidden" name="id_geozone" id="id_geozone" value="<?=$this->geozones->id_geozone?>" />
                <input type="hidden" value="<?= $this->language ?>" id="selectedLang" />
                <?php
                if (!$this->new) { ?><input type="hidden" value="1" name="isNew" /><?php }
                ?>

<div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                       <?= $this->ln->txt('admin-documents', 'document-geozoneadd', $this->language, 'Zone') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                           
                                        <fieldset class="form-horizontal">
                                        
                                           
                                            
                                            
                                            <div class="form-group">
                                                <div class="col-sm-2">
                                                    <label class="control-label floatRight" for="name">
                                                        <?= $this->ln->txt('admin-documents', 'name', $this->language, 'Nom') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                       
                                                        <input type="text" class="form-control" name="name" id="name" value="<?= $_POST['name'] ?>">  
                                                    </div>
                                                </div>
                                            </div>
                                          
                                  
                                     
                         

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/promotions" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>   </fieldset>
                                </div>
                            </div>     </form>
        </div>
                                        
                    </div>
                </div>
       


<script type="text/javascript">    
    function initialiseJqueryPlugins() {
        
       
        

        
    }
    

    
    $(document).ready(function () {
        initialiseJqueryPlugins();
        // Suppression de la promotion /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.confirmAlert').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-documents', 'delete-doc-alert', $this->language, 'Confirmez vous la suppression du document') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/documents/deletegeozone/<?= $this->geozones->id_geozone ?>');
                    return false;
                }
            });
        });
    });
</script>