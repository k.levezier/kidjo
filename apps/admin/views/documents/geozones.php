<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-geozones', 'documents', $this->language, 'Documents') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-geozones', 'documents', $this->language, 'Documents') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-geozones', 'types', $this->language, 'Zones') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/documents/addGeozone" class="btn btn-primary"><?= $this->ln->txt('admin-geozones', 'add-doctype', $this->language, 'Ajouter une zone') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-geozones', 'list-geozones', $this->language, 'Liste des zones') ?>&nbsp;<span class="badge badge-primary"><?=$this->geozones->count()?></span></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php 
                    if ($this->geozones->count() > 0) {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-lg-1"><?= $this->ln->txt('admin-geozones', 'nom', $this->language, 'Nom') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-geozones', 'actions', $this->language, 'Actions') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($this->geozones as $geozone) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?=$geozone->name?>
                                            </td>
                                           
                                            <td>
                                                <a href="<?=$this->lurl?>/documents/addGeozone/<?=$geozone->id_geozone?>" class="btn btn-white btn-sm floatLeft btn-action">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-geozones', 'list-promo-edit', $this->language, 'Modifier') ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                        <span class="font-bold" style="margin-left: 15px;"><?= $this->ln->txt('admin-geozones', 'empty', $this->language, 'Il n\'y a aucune zone pour le moment') ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>