<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-documents', 'documents-stats', $this->language, 'Statistiques') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/documents' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-documents', 'back-documents', $this->language, 'Retour aux documents') ?>
            </a>
            
        </div>
    </div>
</div>
<div class="row">
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Téléchargements</h5>
                </div>
                <div class="ibox-content">
                    <div class="project-list">
                        <table class="table">
                            <tr>
                                <th>Date</th>
                                <th>Utilisateur</th>
                                <th>Document</th>
                                
                            </tr>
                        <?php foreach($this->downloads as $download)
                        {
                            $guy = new o\clients(array('id_client' => $download->id_client));
                            $doc = new o\documents(array('id_document' => $download->id_document));
                            ?>
                            <tr>
                                <td><?=(new DateTime($download->added))->format('d/m/y H:i:s')?></td>
                                <td><?=$guy->prenom?> <?=$guy->nom?></td>
                                <td><?=$doc->label?></td>
                            </tr>
                        <?php
                        }
?>                         </table> 
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Connexions</h5>
                </div>
                <div class="ibox-content">
             <table class="table">
                            <tr>
                                <th>Date</th>
                                <th>Utilisateur</th>

                                
                            </tr>
                        <?php foreach($this->logins as $download)
                        {
                            $guy = new o\clients(array('id_client' => $download->id_client));
  
                            ?>
                            <tr>
                                <td><?=(new DateTime($download->added))->format('d/m/y H:i:s')?></td>
                                <td><?=$guy->prenom?> <?=$guy->nom?></td>

                            </tr>
                        <?php
                        }
?>                         </table> 
                </div>
            </div>
        </div>
    </div>