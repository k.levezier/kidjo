<script>
            $(document).ready(function(){
            Dropzone.autoDiscover = false;
            <?php foreach(['en','fr','es'] as $ln){ ?>
                 var myDropzone<?=$ln?> = new Dropzone('#my-dropzone-<?=$ln?>', { // Make the whole body a dropzone
                    url: "<?=$this->lurl?>/documents/upload/<?=$ln?><?=($this->new?'':'/'.$this->params[0])?>", // Set the url
                    thumbnailWidth: 80,
                    maxFilesize: 999,
                    thumbnailHeight: 80,
                    parallelUploads: 1,
                    autoProcessQueue: true, // Make sure the files aren't queued until manually added
                    clickable: "#my-dropzone-<?=$ln?>", // Define the element that should be used as click trigger to select files.
                     init: function() {
                         this.on("addedfile", function(file) {
<?php                            foreach(['en','fr','es'] as $ln2){ if($ln!=$ln2){?>
                             $("#my-dropzone-<?=$ln2?>").slideUp();
<?php                            }  } ?>
                             }
                         );
                         this.on("success", function(file,response) {
                             if(response>0) {
                                 $('#id_document').val(response);
<?php                            foreach(['en','fr','es'] as $ln2){ ?>
                                 myDropzone<?=$ln2?>.options.url = "<?=$this->lurl?>/documents/upload/<?=$ln2?>/"+response;
<?php                            } ?>
                                 $('.lesboutons').removeClass('hide');
                             } else {
                                 alert("Erreur d'envoi du fichier");
                             }
<?php                        foreach(['en','fr','es'] as $ln2){ if($ln!=$ln2){?>
                                $("#my-dropzone-<?=$ln2?>").slideDown();
<?php                        }  } ?>
                         });
                     }
                });

                 $('#file_temp_<?=$ln?>').change(function(){
                     $.ajax({
                         url: $(this).data('url'), // Set the url
                         method: "POST",
                         data: { file_temp: $(this).val()},
                         success:function(data){
<?php                      foreach(['en','fr','es'] as $ln2){ ?>
                             $("#file_temp_<?=$ln2?>").data("url","<?=$this->lurl?>/documents/upload/<?=$ln2?>/"+data+"/ftp");
<?php                      } ?>
                             $('#id_document').val(data);
                             $('.lesboutons').removeClass('hide');
                         },
                         error:function(){ console.log("Error");}
                     });
                 })

             <?php } ?>
            });
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/documents"><?= $this->ln->txt('admin-documents', 'documents', $this->language, 'Documents') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-documents', 'documents-'.($this->new?'add':'edit'), $this->language, ($this->new?'Ajout':'Modification').' d\'un document') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/documents' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-documents', 'back-documents', $this->language, 'Retour aux documents') ?>
            </a>
            <?php
            if (!$this->new) {
                ?>
                <a class="btn btn-danger confirmAlert">
                    <?= $this->ln->txt('admin-documents', 'del-document', $this->language, 'Supprimer le document') ?>
                </a>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">          
            <form method="POST" name="formPromotion" id="formPromotion" enctype="multipart/form-data" action="<?=$this->lurl.'/documents/add/'.$this->documents->id_document.'/'?>">
                <input type="hidden" name="sendForm" />
                <input type="hidden" name="restePage" id="restePage" value="0" />
                <input type="hidden" name="part" id="part" value="1" />
                <input type="hidden" name="id_document" id="id_document" value="<?=$this->documents->id_document?>" />
                <input type="hidden" value="<?= $this->language ?>" id="selectedLang" />
                <?php
                if (!$this->new) { ?><input type="hidden" value="1" name="isNew" /><?php }
                ?>

                <div class="tabs-container tabs-container-products">
                    <div class="tabs-left">
                        <ul class="nav nav-tabs">
                            <li <?=$this->params['part'] == 1 || !isset($this->params['part']) ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-file">
                                    <i class="fa fa-info-circle"></i> 
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-documents', 'document-fichier', $this->language, 'Fichier') ?></span>
                                </a>
                            </li>
                            <li <?=$this->params['part'] == 2 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-droits">
                                    <i class="fa fa-cogs"></i>
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-documents', 'document-droits', $this->language, 'Droits d\'accès') ?></span>
                                </a>
                            </li>
                            <li <?=$this->params['part'] == 3 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-arbo" >
                                    <i class="fa fa-chain"></i>
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-documents', 'document-arborescence', $this->language, 'Arborescence') ?></span>
                                </a>
                            </li>
                            
                        </ul>

                       <?php $i=1; ?>
                        <div class="tab-content ">
                            <div id="tab-file" class="tab-pane <?=$this->params['part'] == $i || !isset($this->params['part']) ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-documents', 'document-fichier', $this->language, 'Document') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <div>
                                            <?php foreach(['en','fr','es'] as $ln){ ?>
                                                <div class="col-md-4 no-overflow text-center">
                                                    <img src="<?= $this->surl ?>/images/admin/flags/<?= $ln ?>.png" />
                                                    <div id="my-dropzone-<?= $ln ?>" class="dropzone dz-clickable" action="#">
                                                        <div class="dropzone-previews"></div>
                                                        <div class="dz-default dz-message">
                                                        <span>Drop files here to upload</span></div>
                                                    </div>
                                                    <div>
                                                        &nbsp;
                                                        <?php if(!empty($_POST['name_'.$ln])){ ?>
                                                            <?=$_POST['name_'.$ln]?> <i>(<?=round($_POST['size_'.$ln]/1000000,2)?>Mo)</i><a href="<?=$this->url?>/documents/add/<?=$this->documents->id_document?>/delete/<?=$ln?>"> <i class="fa fa-times-circle-o color-danger"></i> </a>
                                                        <?php } ?>
                                                    </div>

                                                    <div class="m-t-md">
                                                        <select class="form-control " name="file_temp_<?= $ln ?>" id="file_temp_<?= $ln ?>" data-url="<?=$this->lurl?>/documents/upload/<?=$ln?><?=($this->new?'':'/'.$this->params[0])?>/ftp">
                                                            <option>Fichiers FTP</option>
                                                            <?php foreach($this->filesTemp as $label=>$file){
                                                                if(is_array($file)){?>
                                                                    <optgroup label="<?=$label?>">
                                                                        <?php foreach($file as $child){ ?>
                                                                            <option value="<?=$label?>/<?=$child?>"><?=$child?></option>
                                                                        <?php } ?>
                                                                    </optgroup>
                                                                <?php }else{ ?>
                                                                    <option value="<?=$file?>"><?=$file?></option>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            </div>

                                            <div class="clearfix"></div>
                                            <hr/>

                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="condition"><?= $this->ln->txt('admin-documents', 'document-type', $this->language, 'Type de document') ?></label>
                                                <div class="col-sm-8">
                                                    <select name="id_doctype" class="form-control">
                                                    <?php foreach($this->doctypes as $doctype) { ?>
                                                        <option value="<?=$doctype->id_doctype?>" <?=($doctype->id_doctype==$_POST['id_doctype']?'selected':'')?>><?=$doctype->name?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php /* <div class="form-group">
                                                <label class="col-sm-4 control-label" for="condition"><?= $this->ln->txt('admin-documents', 'document-langue', $this->language, 'Langue') ?> <i class="fa fa-question-circle"data-toggle="tooltip" data-placement="right" title="<?= $this->ln->txt('admin-documents', 'document-aide-langue', $this->language, 'Langue') ?>"></i> </label>
                                                <div class="col-sm-8">
                                                    <select name="language" class="form-control">
                                                    <?php foreach($this->languages as $doctype) { ?>
                                                        <option value="<?=$doctype->code?>" <?=($doctype->code==$_POST['language']?'selected':'')?>><?=$doctype->name?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div> */ ?>
                                             <div class="form-group">
                                                <label class="col-sm-4 control-label" for="condition"><?= $this->ln->txt('admin-documents', 'document-name', $this->language, 'Intitulé') ?></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="label" id="label" value="<?=$_POST['label']?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="condition"><?= $this->ln->txt('admin-documents', 'document-description', $this->language, 'Description') ?></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control" name="description" id="description"><?=$_POST['description']?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="condition"><?= $this->ln->txt('admin-documents', 'document-tags', $this->language, 'Tags') ?></label>
                                                <div class="col-sm-8">
                                                    <textarea class="form-control" name="tags" id="tags"><?=$_POST['tags']?></textarea>
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-sm-4 control-label" for="condition"><?= $this->ln->txt('admin-documents', 'document-rights', $this->language, 'Droits') ?></label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" name="droits" id="droits" value="<?=$_POST['droits']?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label" for="condition"><?= $this->ln->txt('admin-documents', 'document-preview', $this->language, 'Prévisualisation (PDF)') ?></label>
                                                <div class="col-sm-8">
                                                    <input type="file" class="form-control" name="preview" id="preview" value="<?=$_POST['preview']?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label"><?= $this->ln->txt('admin-documents', 'doc-status', $this->language, 'Statut du document') ?></label>
                                                <div class="col-sm-6">
                                                    <div class="radio i-checks inline col-sm-5">
                                                        <label class="text-primary">
                                                            <input type="radio" value="1" name="status"<?= ($_POST['status'] == 1 ? ' checked' : '') ?>>
                                                            <i></i> <?= $this->ln->txt('admin-documents', 'form-promo-lab-statut-on', $this->language, 'En ligne') ?>
                                                        </label>
                                                    </div>
                                                    <div class="radio i-checks inline col-sm-5">
                                                        <label class="text-danger">
                                                            <input type="radio" value="0" name="status"<?= ($_POST['status'] == 0 ? ' checked' : '') ?>>
                                                            <i></i> <?= $this->ln->txt('admin-documents', 'form-promo-lab-statut-off', $this->language, 'Hors ligne') ?>
                                                        </label>
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label class="control-label floatRight" for="from">
                                                        <?= $this->ln->txt('admin-documents', 'document mel', $this->language, 'Date de mise en ligne') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control datePicker" name="planned" id="planned" value="<?= $_POST['planned'] ?>">  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label class="control-label floatRight" for="from">
                                                        <?= $this->ln->txt('admin-documents', 'document expiration', $this->language, 'Date d\'expiration') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control datePicker" name="expires" id="expires" value="<?= ($_POST['expires']=='0000-00-00 00:00:00'?'':$_POST['expires']) ?>">  
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label class="control-label floatRight" for="new">
                                                        <?= $this->ln->txt('admin-documents', 'document new', $this->language, 'Nouveauté') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-8">
                                                     <div class="checkbox i-checks">
                                                        <input type="checkbox" name="new" id="new" value="1"<?= ($_POST['new']==1 ? ' checked' : '') ?>>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    <label class="control-label floatRight" for="noshare">
                                                        <?= $this->ln->txt('admin-documents', 'document noshare', $this->language, 'Interdire le partage') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="checkbox i-checks">
                                                        <input type="checkbox" name="noshare" id="noshare" value="1"<?= ($_POST['noshare']==1 ? ' checked' : '') ?>>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group lesboutons<?=($this->params[0]>0?'':' hide')?>">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/promotions" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <?php $i++; ?>
                            <div id="tab-droits" class="tab-pane <?=$this->params['part'] == $i ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-documents', 'document-droits', $this->language, 'Droits') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-sm-6">
                                                <h4>Zones</h4>
                                            <div class="hr-line-dashed"></div>
                                                <p>
                                                    <i>Si aucune zone n'est sélectionnée alors tout le monde pourra voir cette page.</i>
                                                </p>
                                                <div class="text-center">
                                                    <a class="btn btn-info" onclick="$('#limiter-zones').slideToggle(1500)">Limiter l'accès à :</a>
                                                </div>
                                                <div id="limiter-zones" style="display:<?=(count($this->documentZones)==0?'none':'block')?>">
                                            <?php foreach($this->geozones as $zone) {  ?>
                                                <div class="form-group">
                                                    <div class="col-sm-2 m-t-sm">
                                                        <div class="checkbox i-checks">
                                                            <input type="checkbox" name="zone<?=$zone->id_geozone?>" id="zone<?=$zone->id_geozone?>" value="1"<?= (in_array($zone->id_geozone,$this->documentZones)? ' checked' : '') ?>>
                                                        </div>
                                                    </div>
                                                    <label class="text-left control-label" for="zone<?=$zone->id_geozone?>"><?=$zone->name?></label>
                                                </div>
                                            <?php } ?>
                                                </div>
                                       </div>
                                            <div class="col-sm-6">
                                                <h4>Profils</h4>
                                                <div class="hr-line-dashed"></div>
                                            <?php foreach($this->profiles as $profile) { ?>
                                                <div class="form-group">
                                                    <div class="col-sm-2 m-t-sm">
                                                        <div class="checkbox i-checks">
                                                            <input type="checkbox" name="profile<?=$profile->id_profile?>" id="profile<?=$profile->id_profile?>" value="1"<?= (in_array($profile->id_profile,$this->documentProfiles) || $this->new ? ' checked' : '') ?>>
                                                        </div>
                                                    </div>
                                                    <label class="text-left control-label" for="profile<?=$profile->id_profile?>"><?=$profile->name?></label>
                                                </div>
                                            <?php } ?>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group lesboutons<?=($this->params[0]>0?'':' hide')?>">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/promotions" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <?php $i++; ?>
                            <div id="tab-arbo" class="tab-pane <?=$this->params['part'] == $i ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-documents', 'document-arbo', $this->language, 'Arborescence') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            
                                           <div class="form-group">
                                                <label class="col-sm-4 control-label" for="liste_tree"><?= $this->ln->txt('admin-documents', 'document-rubrique', $this->language, 'Rubriques de présence') ?></label>
                                                <div class="col-sm-8">
                                                    <select class="form-control chosen-select" name="liste_tree[]" id="liste_tree" multiple data-placeholder="<?= $this->ln->txt('admin-documents', 'document-liste-pages', $this->language, 'Choisir les rubriques contenant le document') ?>">
                                                        <?= $this->arbo; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group lesboutons<?=($this->params[0]>0?'':' hide')?>">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/promotions" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>                       
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">    
    function initialiseJqueryPlugins() {
        
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'}).on('ifChecked', function() {
            // Exception pour le type de réduction
            if($(this).hasClass('type_reduc')){
                if($(this).val() == 1){
                    $('#value').siblings('.input-group-addon').html('%');
                } else {
                    $('#value').siblings('.input-group-addon').html('<?=$this->clSettings->getParam('devise', 'sets')?>');
                }
            }
        });
        
        

        // Initialisation des datepickers /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        jQuery.datetimepicker.setLocale('<?=$this->language?>');                
        $('.datePicker').datetimepicker({
            format:'<?= str_replace(array('yyyy', 'yy', 'dd', 'mm'),array('Y','y','d','m'),$this->clSettings->getParam('format-datepicker', 'sets')) ?> H:i',
            //defaultDate:'<?=date('Y-m-d')?>',
            defaultTime:'08:00',
            timepickerScrollbar:false     
        });
        
        // Gestion des select multiple /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: '<?= $this->ln->txt('admin-generic', 'no-result', $this->language, 'Aucun résultat') ?>'},
            '.chosen-select-width': {width: "95%"}
        };

        for (var selector in config) {
            $(selector+':visible').chosen(config[selector]);
        }
        
        $('[data-toggle="tooltip"]').tooltip();  
    }
    
    // Réinitialisation des plugins JQ /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Nécéssaire au changmeent de tabs car certain element initialisé alors que non visible seront mal initialisés
    $(document).on('click', '.tabs-left a', function(){
        initialiseJqueryPlugins();
    });
    
    $(document).ready(function () {
        initialiseJqueryPlugins();
        // Suppression de la promotion /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.confirmAlert').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-documents', 'delete-doc-alert', $this->language, 'Confirmez vous la suppression du document') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/documents/delete/<?= $this->documents->id_document ?>');
                    return false;
                }
            });
        });
    });
</script>