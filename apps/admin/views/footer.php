<div class="footer">
    <div class="pull-right">
        <strong><?= $this->version['nom'] ?></strong>
        <?= $this->version['numero'] ?> -
        <a href="<?= $this->urlfront ?>/licence.txt" target="_blank" title="<?= $this->ln->txt('admin-generic', 'licence', $this->language, 'Licence') ?>">
            <?= $this->ln->txt('admin-generic', 'licence', $this->language, 'Licence') ?>
        </a>
    </div>
    <div>
        <a href="<?= $this->version['copy_lien'] ?>" target="_blank" title="<?= $this->version['copy_nom'] ?>"><?= $this->version['copy_nom'] ?></a>
        &copy; <?= ($this->version['copy_annee'] < date('Y') ? $this->version['copy_annee'] . ' - ' : '') ?><?= date('Y') ?>
    </div>
</div>
</div>
</div>
</body>
</html>
<?php unset($_SESSION['msgToast']); ?>
