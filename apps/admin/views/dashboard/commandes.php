<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><?= $this->ln->txt('admin-dash', 'gestion-commandes', $this->language, 'Gestion des commandes') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/dashboard"><?= $this->ln->txt('admin-dash', 'dashboard', $this->language, 'Dashboard') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-dash', 'gestion-commandes', $this->language, 'Gestion des commandes') ?></strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-dash', 'liste-commandes', $this->language, 'Liste des commandes') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <?php if ($this->transactions->count() > 0) { ?>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:2%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-type', $this->language, 'Type') ?></th>
                                        <th style="width:5%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-date', $this->language, 'Date') ?></th>
                                        <th style="width:5%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-numero', $this->language, 'Numéro') ?></th>
                                        <th style="width:35%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-client', $this->language, 'Client') ?></th>
                                        <th style="width:10%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-montant', $this->language, 'Montant') ?></th>
                                        <th style="width:20%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-liv', $this->language, 'Livraison') ?></th>
                                        <th style="width:5%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-paiement', $this->language, 'Paiement') ?></th>
                                        <th style="width:8%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-etat', $this->language, 'Etat') ?></th>
                                        <th style="width:5%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-logistique', $this->language, 'Logistique') ?></th>
                                        <th style="width:5%;"><?= $this->ln->txt('admin-dash', 'tab-cmd-action', $this->language, 'Action') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($this->transactions->order('id_transaction','DESC') as $key => $cmd) { ?>
                                        <tr class="align-middle">
                                            <td class="icon-td text-success"><i class="fa fa-<?= $cmd->is_mobile==1?'mobile':'globe' ?>"></i></td>
                                            <td><?= date($this->clSettings->getParam('format-de-date', 'sets'), strtotime($cmd->date_transaction)) ?></td>
                                            <td><?= $cmd->id_transaction ?></td>
                                            <td><?= $cmd->client->email ?></td>
                                            <td><?= formatPrix($cmd->montant/100, 2, $this->clSettings->getParam('devise', 'sets'), false) ?></td>
                                            <td><?= $cmd->cp_liv.' '.$cmd->ville_liv ?></td>
                                            <td><?= $this->etatPaiement[$cmd->status] ?></td>
                                            <td><?=$this->etatCommande[$cmd->etat]?></td>
                                            <td><?=$this->etatLogistique[$cmd->logistic]?></td>
                                            
                                            <td>
                                                <a href="<?= $this->lurl ?>/dashboard/formCommande/cmd___<?= $cmd->id_transaction ?>" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-eye"></i> <?= $this->ln->txt('admin-generic', 'btn-details', $this->language, 'Détail') ?>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?>                                   
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <p><?= $this->ln->txt('admin-generic', 'no-results', $this->language, 'Il n\'y a pas de données pour le moment') ?></p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>