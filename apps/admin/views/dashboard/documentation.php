<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title text-center">
                    <h5><?= $this->ln->txt('admin-dash-doc', 'title-doc-stats', $this->language, 'Statistiques des documents') ?> </h5>
                </div>
                <div class="ibox-content container-fluid">
                    <form action="" method="post" id="dateStatsDocs">
                        <div class="row">
                            <div class="col-lg-3">
                                <h3><?= $this->ln->txt('admin-dash-doc', 'doc-periode', $this->language, 'Définir une période') ?></h3>
                            </div>
                            <div class="col-lg-3">
                                <div class="col-lg-1">
                                    <i class="fa fa-calendar text-success"
                                       style="display: inline-block; font-size:15px; padding: 8px;"></i>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="stats-doc-start" id="stats-doc-start"
                                           class="form-control datePicker" value="<?= $_POST['stats-doc-start'] ?>"
                                           placeholder="<?= $this->ln->txt('admin-dash-doc', 'doc-periode-deb', $this->language, 'début') ?>"/>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="col-lg-1">
                                    <i class="fa fa-calendar text-success"
                                       style="display: inline-block; font-size:15px; padding: 8px;"></i>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="stats-doc-end" id="stats-doc-end" class="form-control datePicker"
                                           value="<?= $_POST['stats-doc-end'] ?>"
                                           placeholder="<?= $this->ln->txt('admin-dash-doc', 'doc-periode-fin', $this->language, 'fin') ?>"/>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <a class="btn btn-primary full-width" onclick="$('#dateStatsDocs').submit();">
                                    <?= $this->ln->txt('admin-dash-doc', 'doc-voir', $this->language, 'Voir les statistiques') ?>
                                    <i class="fa fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="ibox-content container-fluid">
                    <div style="min-height: 390px;">
                        <div class="row">
                            <div class="col-lg-12" style="text-align:center;height:340px;">
                                <h3 style="position:absolute;top:0;left:0;width:100%;">
                                    <strong><?= $this->ln->txt('admin-dash-doc', 'title-donut-types', $this->language, 'Répartition des telechargements par type de documents') ?></strong>
                                </h3>
                                <span id="dyn-label-types" style="position:absolute;top:35px;left:0;display: inline-block;line-height: 300px; width: 100%;font-size: 13px; text-transform: uppercase;color:<?= $this->colorMaxCountType ?>;">
                                    <?= $this->typesMaxCountType . ' ' . $this->maxCountType . '%' ?>
                                </span>
                                <div id="donut_types" style="height:300px;top:35px;"></div>
                                <div style="position:relative;top:55px;">
                                    <a href="<?= $this->lurl ?>/dashboard/documentation/export___types/<?= $this->date_deb_types->format('Y-m-d') ?>/<?= $this->date_fin_types->format('Y-m-d') ?>" class="btn btn-primary">
                                        <?= $this->ln->txt('admin-dash-doc', 'export-types', $this->language, 'Exporter') ?>
                                        <i class="fa fa-download"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <br class="clearfix"/>
                        <div class="hr-line-dashed"></div>
                        <br class="clearfix"/>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3><strong><?= $this->ln->txt('admin-dash-doc', 'top-ten-dl', $this->language, 'Top 10 des documents téléchargés') ?></strong></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Document</th>
                                        <th>Type</th>
                                        <th>Nombre de Téléchargements</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ( $this->top_10_downloads as $topDoc) { ?>
                                        <tr>
                                            <td><?= $topDoc['file'] ?></td>
                                            <td><?= $topDoc['type'] ?></td>
                                            <td class="text-center"><?= $topDoc['nbrDl'] ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <div style="position:relative;top:10px;left:20px;">
                                <a href="<?= $this->lurl ?>/dashboard/documentation/export___top10dl/<?= $this->date_deb_types->format('Y-m-d') ?>/<?= $this->date_fin_types->format('Y-m-d') ?>" class="btn btn-primary">
                                    <?= $this->ln->txt('admin-dash-doc', 'export-types', $this->language, 'Exporter') ?>
                                    <i class="fa fa-download"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        jQuery.datetimepicker.setLocale('<?= $this->language ?>');
        $('.datePicker').datetimepicker({
            format: '<?= str_replace(array('yyyy', 'yy', 'dd', 'mm'), array(
                'Y',
                'y',
                'd',
                'm',
            ), $this->clSettings->getParam('format-datepicker', 'sets')) ?> H:i',
            //defaultDate:'<?= date('Y-m-d') ?>',
            defaultTime: '08:00',
            timepickerScrollbar: false
        });

        // Stats Donut
        function labelFormatter(label, series) {
            return "<div style='font-size:8pt; text-align:center; padding:2px;'>" + label + "</div>";
        }

        // DONUT ZONES
        var dataTypes = [ <?= $this->donutsTypesDatas ?> ];
        $.plot('#donut_types', dataTypes, {
            series: {
                pie: {
                    innerRadius: 0.6,
                    show: true,
                    label: {
                        show: false,
                        radius: 0.90,
                        formatter: labelFormatter
                    }
                }
            },
            grid: {
                hoverable: true
            },
            legend: {
                show: false
            }
        });
        $('#donut_types').bind("plothover", function (event, pos, obj) {
            if (!obj) {
                return;
            }
            var type = obj.series.label;
            var percent = parseFloat(obj.series.percent).toFixed(2);

            $('#dyn-label-types').html(type + ' ' + percent + '%').css('color', obj.series.color);
        });
    });
</script>
