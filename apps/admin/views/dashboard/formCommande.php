<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-dash', 'details-commande-num', $this->language, 'Détails de la commande N° ').str_pad($this->transactions->id_transaction, 8, '0', STR_PAD_LEFT) ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/dashboard"><?= $this->ln->txt('admin-dash', 'dashboard', $this->language, 'Dashboard') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/dashboard/commandes"><?= $this->ln->txt('admin-dash', 'gestion-commandes', $this->language, 'Gestion des commandes') ?></a>
            </li>
            <li >
                <strong><?= $this->ln->txt('admin-dash', 'details-commande-num', $this->language, 'Détails de la commande N° ').str_pad($this->transactions->id_transaction, 8, '0', STR_PAD_LEFT) ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/dashboard/commandes' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-dash', 'back-commandes', $this->language, 'Retour aux commandes') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <i class="fa fa-file-text"></i> <?= $this->ln->txt('admin-dash', 'commandes-etat', $this->language, 'État de la commande') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="widget style1 col-lg-12 <?=$this->etatCommandeBG[$this->transactions->etat]?>">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <span style="font-size:35px;"><?=explode('&nbsp;', $this->etatCommandeText[$this->transactions->etat])[0]?></span>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <span><?= $this->ln->txt('admin-dash', 'etat-commande', $this->language, 'État de la commande') ?></span>
                                        <h2 class="font-bold"><?=explode('&nbsp;', $this->etatCommandeText[$this->transactions->etat])[1]?></h2>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <?php $url_status = $this->lurl.'/dashboard/etatCommande/'.$this->transactions->id_transaction.'/'; ?>
                                <div class="col-xs-3"><a data-etat="0" class="confirmChangeEtat btn btn-<?=$this->transactions->etat==0?'white active':'primary'?>"><?=$this->ln->txt('admin-dash', 'etat-cmd-en-cours', $this->language, 'En cours')?></a></div>
                                <div class="col-xs-3"><a data-etat="1" class="confirmChangeEtat btn btn-<?=$this->transactions->etat==1?'white active':'primary'?>"><?=$this->ln->txt('admin-dash', 'etat-cmd-validee', $this->language, 'Validée')?></a></div>
                                <div class="col-xs-3"><a data-etat="2" class="confirmChangeEtat btn btn-<?=$this->transactions->etat==2?'white active':'primary'?>"><?=$this->ln->txt('admin-dash', 'etat-cmd-expediee', $this->language, 'Expédiée')?></a></div>
                                <div class="col-xs-3"><a data-etat="3" class="confirmChangeEtat btn btn-<?=$this->transactions->etat==3?'white active':'primary'?>"><?=$this->ln->txt('admin-dash', 'etat-cmd-annulee', $this->language, 'Annulée')?></a></div>
                            </div>
                            <br class="clearfix"/>
                        </div>
                        <div class="col-lg-4">
                            <div class="widget widget-150 style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <h3 class="font-bold"><?= $this->ln->txt('admin-dash', 'client-commande', $this->language, 'Client') ?></h3>
                                        <span>#<?= str_pad($this->transactions->client->id_client, 8, '0', STR_PAD_LEFT)?></span>
                                        <br/><br/>
                                        <span><?= $this->transactions->client->email?></span>
                                        <br/>
                                        <span><?= $this->transactions->client->nom?> <?= $this->transactions->client->prenom?></span>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <h3><?= $this->ln->txt('admin-dash', 'date-commande', $this->language, 'Date de la commande') ?></h3>
                                        <br/>
                                        <h2 class="font-bold"><?=date('d/m/Y', strtotime($this->transactions->added))?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="widget widget-150 style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'docs-commande', $this->language, 'Documents') ?>:</span>
                                        <br/>
                                        <br class="clearfix"/>
                                        <button class="btn btn-primary col-sm-12 disabled">
                                            <i class="fa fa-download"></i>
                                            <?= $this->ln->txt('admin-dash', 'commande-get-facture', $this->language, 'Télécharger la facture') ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <i class="fa fa-credit-card"></i> <?= $this->ln->txt('admin-dash', 'commandes-paiement-desc', $this->language, 'Informations sur le paiement') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="widget widget-120 style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'date-transac', $this->language, 'Date de la transaction') ?>:</span>
                                        <h2 class="font-bold" style="text-align: center;"><?=date('d/m/Y H:i:s', strtotime($this->transactions->date_transaction))?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget widget-120 style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'montant-transac', $this->language, 'Montant de la transaction') ?>:</span>
                                        <br/><br/>
                                        <h2 class="font-bold" style="font-size: 30px; text-align: center;"><?=formatPrix($this->transactions->montant/100, 2, $this->clSettings->getParam('devise', 'sets'), false)?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget widget-120 style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transac-type-paiement', $this->language, 'Type') ?> :</span> 
                                        <br/><br/>
                                        <h2 class="font-bold" style="font-size: 30px; text-align: center;"><?=$this->transactions->affichageTypePaiement()?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget widget-120 style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-status', $this->language, 'Statut') ?> :</span>
                                        <br/><br/>
                                        <h2 class="font-bold" style="font-size: 30px; text-align: center;">
                                            <?= $this->etatPaiement[$this->transactions->status] ?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget widget-120 style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-3ds', $this->language, '3D Secure') ?> :</span> 
                                        <h2 class="font-bold <?=$this->transactions->is3DS?'text-primary':'text-danger'?>" style="font-size: 50px; text-align: center;">
                                            <i class="fa fa-shield"></i> 
                                            <?= $this->ln->txt('admin-generic', $this->transactions->is3DS?'oui':'non', $this->language, $this->transactions->is3DS?'Oui':'Non') ?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget widget-120 style1 btn btn-primary full-width disabled">
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: center;">
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-refund', $this->language, 'Rembourser') ?></span>
                                        <h2 class="font-bold" style="font-size: 60px;">
                                            <i class="fa fa-rotate-left"></i> 
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <i class="fa fa-user"></i> <?= $this->ln->txt('admin-dash', 'commandes-client-desc', $this->language, 'Informations sur le client') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="widget style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-nom-client', $this->language, 'Nom') ?> :</span> 
                                        <h3 class="font-bold">
                                            <?=$this->transactions->client->civilite.' '.$this->transactions->client->prenom.' '.$this->transactions->client->nom?>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="widget style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-email-client', $this->language, 'Email') ?> :</span> 
                                        <h3 class="font-bold one-click-select">
                                            <?=$this->transactions->client->email?>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-date-client', $this->language, 'Date de création') ?> :</span> 
                                        <h3 class="font-bold">
                                            <?=date('d/m/Y', strtotime($this->transactions->client->added))?>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transac-total-client', $this->language, 'Total') ?> :</span> 
                                        <h3 class="font-bold">
                                            <?=formatPrix($this->transactions->client->getTotalCommandes(), 2, $this->clSettings->getParam('devise', 'sets'), false)?>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="widget style1 gray-bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-nb-cmd-client', $this->language, 'Nb commandes') ?> :</span> 
                                        <h3 class="font-bold">
                                            <?=$this->transactions->client->transactions->where('status', 1)->count()?>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">
                            <a href="<?=$this->lurl?>/clients/formClient/<?=$this->transactions->client->id_client?>" class="widget style1 btn btn-primary full-width">
                                <div class="row">
                                    <div class="col-xs-12" style="text-align: center;">
                                        <span>Voir</span>
                                        <h2 class="font-bold" style="font-size: 14px;">
                                            <i class="fa fa-eye"></i> 
                                        </h2>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <i class="fa fa-truck"></i> <?= $this->ln->txt('admin-dash', 'commandes-livraison-desc', $this->language, 'Informations de livraison') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row row-flex">
                        <div class="col-lg-4 col-flex-fill" >
                            <div class="widget style1 gray-bg widget-flex">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3 class="font-bold">
                                            <i class="fa fa-info-circle"></i> <?= $this->ln->txt('admin-dash', 'commandes-livraison-informations', $this->language, 'Informations') ?>
                                        </h3><br/>
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-fdp-montant', $this->language, 'Montant FDP') ?> :</span> 
                                        <strong class="font-bold"><?=formatPrix($this->transactions->fdp/100, 2, $this->clSettings->getParam('devise', 'sets'), false)?></strong>
                                        <br/><br/>
                                        
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-mode-livraison', $this->language, 'Mode de livraison') ?> :</span> 
                                        <strong class="font-bold">
                                            <?=$this->fdp_type->affichage?>
                                        </strong>
                                        <br/><br/>
                                        
                                        <span><?= $this->ln->txt('admin-dash', 'transaction-url-tracking', $this->language, 'URL suivi') ?> :</span> 
                                        <strong class="font-bold">
                                            <?= $this->url_tracking != ''?
                                                ('<a class="btn btn-primary btn-xs" target="_blank" href="'.$this->url_tracking.'">'.$this->ln->txt('admin-dash', 'transaction-suivre-colis', $this->language, 'suivre le colis').' <i class="fa fa-chevron-circle-right"></i></a>') :
                                                '<i class="fa fa-ban"></i>' ?>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-flex-fill">
                            <div class="widget style1 gray-bg widget-flex">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3 class="font-bold">
                                            <i class="fa fa-map-marker"></i> <?= $this->ln->txt('admin-dash', 'commandes-livraison-addr', $this->language, 'Adresse de livraison') ?>
                                        </h3><br/>
                                        <div class="google_map_bo_wrapper">
                                            <div class="google_map_bo">
                                                <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
                                                        src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?= urlencode($this->adresse_livraison->adresse1 . ($this->adresse_livraison->adresse2 != '' ? ' ' . $this->adresse_livraison->adresse2 . ' ' : ' ') . ($this->adresse_livraison->etat != '' ? $this->adresse_livraison->etat . ' ' : '') . $this->adresse_livraison->cp . ' ' . $this->adresse_livraison->ville . ' ' . $this->this->adresse_livraison->pays) ?>&amp;aq=1&amp;output=embed"
                                                        ></iframe>
                                                <br/>
                                            </div>
                                        </div>
                                        <br class="clearfix"/>
                                        <?php
                                        echo '<strong>'.$this->adresse_livraison->civilite . ' ' . $this->adresse_livraison->nom . ' ' . $this->adresse_livraison->prenom . '</strong>'.
                                             ' '.$this->ln->txt('admin-generic', 'chez', $this->language, 'chez').' <strong>'.($this->adresse_livraison->societe != '' ? ' ' . $this->adresse_livraison->societe . '<br>' : '<br>').'</strong>';
                                        echo $this->adresse_livraison->adresse1 . ($this->adresse_livraison->adresse2 != '' ? '<br>' . $this->adresse_livraison->adresse2 . '<br>' : '<br>');
                                        echo ($this->adresse_livraison->etat != '' ? $this->adresse_livraison->etat . ' ' : '') . $this->adresse_livraison->cp . ' ' . $this->adresse_livraison->ville . '<br>';
                                        echo $this->adresse_livraison->pays . '<br>';
                                        echo $this->adresse_livraison->telephone . '<br>';
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-flex-fill">
                            <div class="widget style1 gray-bg widget-flex">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h3 class="font-bold">
                                            <i class="fa fa-map-marker"></i> <?= $this->ln->txt('admin-dash', 'commandes-facturation-addr', $this->language, 'Adresse de facturation') ?>
                                        </h3><br/>
                                        <div class="google_map_bo_wrapper">
                                            <div class="google_map_bo">
                                                <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" 
                                                        src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?= urlencode($this->adresse_facturation->adresse1 . ($this->adresse_facturation->adresse2 != '' ? ' ' . $this->adresse_facturation->adresse2 . ' ' : ' ') . ($this->adresse_facturation->etat != '' ? $this->adresse_facturation->etat . ' ' : '') . $this->adresse_facturation->cp . ' ' . $this->adresse_facturation->ville . ' ' . $this->this->adresse_livraison->pays) ?>&amp;aq=1&amp;output=embed"
                                                        ></iframe>
                                                <br/>
                                            </div>
                                        </div>
                                        <br class="clearfix"/>
                                        <?php
                                        echo '<strong>'.$this->adresse_facturation->civilite . ' ' . $this->adresse_facturation->nom . ' ' . $this->adresse_facturation->prenom . '</strong>'.
                                             ' '.$this->ln->txt('admin-generic', 'chez', $this->language, 'chez').' <strong>'.($this->adresse_facturation->societe != '' ? ' ' . $this->adresse_facturation->societe . '<br>' : '<br>').'</strong>';
                                        echo $this->adresse_facturation->adresse1 . ($this->adresse_facturation->adresse2 != '' ? '<br>' . $this->adresse_facturation->adresse2 . '<br>' : '<br>');
                                        echo ($this->adresse_facturation->etat != '' ? $this->adresse_facturation->etat . ' ' : '') . $this->adresse_facturation->cp . ' ' . $this->adresse_facturation->ville . '<br>';
                                        echo $this->adresse_facturation->pays . '<br>';
                                        echo $this->adresse_facturation->telephone . '<br>';
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <i class="fa fa-shopping-cart"></i> <?= $this->ln->txt('admin-dash', 'commandes-contenu', $this->language, 'Contenu de la commande') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table id="addr_table" class="table table-striped table-hover vertical-center">
                            <thead>
                                <tr>
                                    <th class="col-lg-3"><?= $this->ln->txt('admin-dash', 'list-pdt-ref', $this->language, 'Référence') ?></th>
                                    <th class="col-lg-5"><?= $this->ln->txt('admin-dash', 'list-pdt-nom', $this->language, 'Produit') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-dash', 'list-pdt-prix', $this->language, 'Prix Unit.') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-dash', 'list-pdt-qte', $this->language, 'Quantité') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-dash', 'list-pdt-prix-total', $this->language, 'Prix total') ?></th>
                                    <th class="col-lg-1"></th>
                                </tr>
                            </thead>
                            <tbody id="addr_list" class="sortable">
                                <?php
                                if($this->transactions->produits->count() > 0) {
                                    foreach ($this->transactions->produits as $combi){
                                        ?>
                                        <tr>
                                            <td><?=$combi->reference?></td>
                                            <td><?=$combi->nom?></td>
                                            <td><?=formatPrix($combi->prix, 2, $this->clSettings->getParam('devise', 'sets'), false)?></td>
                                            <td><?=$combi->quantite?></td>
                                            <td><?=formatPrix($combi->prix*$combi->quantite, 2, $this->clSettings->getParam('devise', 'sets'), false)?></td>
                                            <td><a href="javascript:void(0);" class="text-danger confirmDeletePdt" data-id="<?=$combi->id_transaction_produit?>"><i class="fa fa-trash" style="font-size:20px;"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                <tr id="empty_addr_list" <?=($this->transactions->produits->count() > 0)?'style="display:none;"':''?>>
                                    <td colspan="3">
                                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                                        <span class="font-bold" style="margin-left: 15px;">
                                            <?= $this->ln->txt('admin-dash', 'empty-pdt-transaction', $this->language, 'Il n\'y a pas de produit') ?>
                                        </span>
                                    </td>          
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">    
    function initialiseJqueryPlugins() {
        
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        
        $('.confirmChangeEtat').on('click', function () {
            var _this = $(this);
            
            if( ! _this.hasClass('active')){
                swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-dashboard', 'transac-change-etat-alert', $this->language, 'Confirmez vous le changement d\'état de la commande ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true},
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl.'/dashboard/etatCommande/'.$this->transactions->id_transaction.'/'?>'+_this.data('etat'));
                        return false;
                    }
                });
            }
        });
        
        $('.confirmDeletePdt').on('click', function () {
            var _this = $(this);
            
            if( ! _this.hasClass('active')){
                swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-dashboard', 'transac-delete-pdt', $this->language, 'Confirmez vous la suppression du produit de cette commande ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true},
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl.'/dashboard/deletePdtCommande/'?>'+_this.data('id'));
                        return false;
                    }
                });
            }
        });
    }
    
    $(document).ready(function () {
        initialiseJqueryPlugins();
    });
    
    // Séléction le contenu et le place dans le presse papier
    $(document).on('click', '.one-click-select', function(){
        var _this = $(this);
        var node = $(this).get(0);
        var text = $(this).text().toString();
        
        var doc = document,
            range,
            selection;
        if (window.getSelection) {
            // On sélectionne le texte de la fonction
            selection = window.getSelection();        
            range = document.createRange();
            //range.selectNodeContents(text.split(':')[1]);
            range.setStart(node.firstChild, 0);
            range.setEnd(node.firstChild, text.length);
            selection.removeAllRanges();
            selection.addRange(range);
            
            // On copie dans le presse papier et on ferme le tooltip 1,5sec plus tard
            document.execCommand("copy");
            toastr["success"]('<?= $this->ln->txt('admin-generic', 'copied-to-clipboard-all', $this->language, 'Copiée dans le presse-papier')?>');
        }
    });
</script>