<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title text-center">
                    <h5><?= $this->ln->txt('admin-dashboard-trafic', 'title-trafic-stats', $this->language, 'Statistiques du trafic') ?> </h5>
                </div>
                <div class="ibox-content container-fluid">
                    <form action="" method="post" id="dateStatsTrafic">
                        <div class="row">
                            <div class="col-lg-3">
                                <h3><?= $this->ln->txt('admin-dashboard-trafic', 'trafic-periode', $this->language, 'Définir une période') ?></h3>
                            </div>
                            <div class="col-lg-3">
                                <div class="col-lg-1">
                                    <i class="fa fa-calendar text-success"
                                       style="display: inline-block; font-size:15px; padding: 8px;"></i>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="stats-trafic-start" id="stats-trafic-start"
                                           class="form-control datePicker" value="<?= $_POST['stats-trafic-start'] ?>"
                                           placeholder="<?= $this->ln->txt('admin-dashboard-trafic', 'trafic-periode-deb', $this->language, 'début') ?>"/>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="col-lg-1">
                                    <i class="fa fa-calendar text-success"
                                       style="display: inline-block; font-size:15px; padding: 8px;"></i>
                                </div>
                                <div class="col-lg-10">
                                    <input type="text" name="stats-trafic-end" id="stats-trafic-end" class="form-control datePicker"
                                           value="<?= $_POST['stats-trafic-end'] ?>"
                                           placeholder="<?= $this->ln->txt('admin-dashboard-trafic', 'trafic-periode-fin', $this->language, 'fin') ?>"/>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <a class="btn btn-primary full-width" onclick="$('#dateStatsTrafic').submit();">
                                    <?= $this->ln->txt('admin-dashboard-trafic', 'trafic-voir', $this->language, 'Voir les statistiques') ?>
                                    <i class="fa fa-chevron-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="ibox-content container-fluid">
                    <div style="min-height: 390px;">
                        <div class="row">
                            <div class="col-lg-6" style="text-align:center;height:340px;">
                                <h3 style="position:absolute;top:0;left:0;width:100%;">
                                    <strong><?= $this->ln->txt('admin-dashboard-trafic', 'title-donut-zones', $this->language, 'Répartition des connexions par zones') ?></strong>
                                </h3>
                                <span id="dyn-label-zone" style="position:absolute;top:35px;left:0;display: inline-block;line-height: 300px; width: 100%;font-size: 13px; text-transform: uppercase;color:<?= $this->colorMaxCountZone ?>;">
                                    <?= $this->zonesMaxCountZone . ' ' . $this->maxCountZone . '%' ?>
                                </span>
                                <div id="donut_zone" style="height:300px;top:35px;"></div>
                                <div style="position:relative;top:55px;">
                                    <a href="<?= $this->lurl ?>/dashboard/trafic/export___zones/<?= $this->date_deb->format('Y-m-d') ?>/<?= $this->date_fin->format('Y-m-d') ?>" class="btn btn-primary">
                                        <?= $this->ln->txt('admin-dashboard-trafic', 'export-zones', $this->language, 'Exporter') ?>
                                        <i class="fa fa-download"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-6" style="text-align:center;height:340px;">
                                <h3 style="position:absolute;top:0;left:0;width:100%;">
                                    <strong><?= $this->ln->txt('admin-dashboard-trafic', 'title-donut-profiles', $this->language, 'Répartition des connexions par profiles') ?></strong>
                                </h3>
                                <span id="dyn-label-profile" style="position:absolute;top:35px;left:0;display: inline-block;line-height: 300px; width: 100%;font-size: 13px; text-transform: uppercase;color:<?= $this->colorMaxCountProfile ?>;">
                                    <?= $this->profileMaxCountProfile . ' ' . $this->maxCountProfile . '%' ?>
                                </span>
                                <div id="donut_profile" style="height:300px;top:35px;"></div>
                                <div style="position:relative;top:55px;">
                                    <a href="<?= $this->lurl ?>/dashboard/trafic/export___profiles/<?= $this->date_deb->format('Y-m-d') ?>/<?= $this->date_fin->format('Y-m-d') ?>" class="btn btn-primary">
                                        <?= $this->ln->txt('admin-dashboard-trafic', 'export-profiles', $this->language, 'Exporter') ?>
                                        <i class="fa fa-download"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        jQuery.datetimepicker.setLocale('<?= $this->language ?>');
        $('.datePicker').datetimepicker({
            format: '<?= str_replace(array('yyyy', 'yy', 'dd', 'mm'), array(
                'Y',
                'y',
                'd',
                'm',
            ), $this->clSettings->getParam('format-datepicker', 'sets')) ?> H:i',
            //defaultDate:'<?= date('Y-m-d') ?>',
            defaultTime: '08:00',
            timepickerScrollbar: false
        });

        // Stats Donut
        function labelFormatter(label, series) {
            return "<div style='font-size:8pt; text-align:center; padding:2px;'>" + label + "</div>";
        }

        // DONUT ZONES
        var dataZones = [ <?= $this->donutsZonesDatas ?> ];
        $.plot('#donut_zone', dataZones, {
            series: {
                pie: {
                    innerRadius: 0.6,
                    show: true,
                    label: {
                        show: false,
                        radius: 0.90,
                        formatter: labelFormatter
                    }
                }
            },
            grid: {
                hoverable: true
            },
            legend: {
                show: false
            }
        });
        $('#donut_zone').bind("plothover", function (event, pos, obj) {
            if (!obj) {
                return;
            }
            var zone = obj.series.label;
            var percent = parseFloat(obj.series.percent).toFixed(2);

            $('#dyn-label-zone').html(zone + ' ' + percent + '%').css('color', obj.series.color);
        });

        // DONUT PROFILES
        var dataProfiles = [ <?= $this->donutsProfilesDatas ?> ];
        $.plot('#donut_profile', dataProfiles, {
            series: {
                pie: {
                    innerRadius: 0.6,
                    show: true,
                    label: {
                        show: false,
                        radius: 0.90,
                        formatter: labelFormatter
                    }
                }
            },
            grid: {
                hoverable: true
            },
            legend: {
                show: false
            }
        });
        $('#donut_profile').bind("plothover", function (event, pos, obj) {
            if (!obj) {
                return;
            }
            var profile = obj.series.label;
            var percent = parseFloat(obj.series.percent).toFixed(2);

            $('#dyn-label-profile').html(profile + ' ' + percent + '%').css('color', obj.series.color);
        });
    });
</script>
