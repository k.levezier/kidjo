<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-mails', 'gestion-preview', $this->language, 'Aperçu des templates d\'emails') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/administration"><?= $this->ln->txt('admin-administration', 'administration', $this->language, 'Administration') ?></a>
            </li>
            <li>
                <a href="<?= $this->lurl ?>/mails/templates"><?= $this->ln->txt('admin-mails', 'tpl-mails', $this->language, 'Templates d\'emails') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-mails', 'gestion-preview', $this->language, 'Aperçu des templates d\'emails') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/mails/templates' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-mails', 'back-mails', $this->language, 'Retour aux mails') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-mails', 'mail-template-vars', $this->language, 'Variables du template de mail') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div class="panel blank-panel">
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="tab" class="tab-pane active">
                                    <form method="POST" name="formPreview" id="formPreview" class="form-horizontal" action="<?= $this->lurl . '/mails/preview' ?>" target="_blank">
                                        <input type="hidden" name="sendForm"/>
                                        <input type="hidden" name="type" id="type" value="<?= $this->params[0] ?>"/>
                                        <input type="hidden" name="id_langue" id="id_langue" value="<?= $this->selectedLang ?>"/>
                                        <input type="hidden" name="sendmail" id="sendmail" value="0"/>
                                        <?php foreach ($this->vars as $var) { ?>
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" name="<?= $var ?>" id="<?= $var ?>" class="form-control" placeholder="<?= $var ?>"/>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="hr-line-dashed m-t-zero"></div>
                                        <div class="form-group action_btns col-sm-6">
                                            <a href="<?= $this->lurl ?>/mails/templates" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-eye"></i>
                                                <?= $this->ln->txt('admin-generic', 'preview', $this->language, 'Aperçu') ?>
                                            </button>
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <div class="col-sm-12">
                                                <div class="input-group">
                                                    <input type="email" class="form-control" id="email-preview" name="email-preview" placeholder="<?= $this->ln->txt('admin-generic', 'address-mail', $this->language, 'Adresse Email') ?>">
                                                    <span class="input-group-btn"> 
                                                        <button type="submit" id="btn-sendmail" class="btn btn-primary">
                                                            <i class="fa fa-envelope"></i>
                                                            <?= $this->ln->txt('admin-generic', 'send-by-mail', $this->language, 'Envoyer par Mail') ?>
                                                        </button> 
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '#btn-sendmail', function () {
        if ($('#email-preview').val() !== '' && validateEmail($('#email-preview').val())) {
            $('#sendmail').val(1);
            $('#formPreview').attr('target', '');
            $('#formPreview').submit();
        } else {
            alert('<?= $this->ln->txt('admin-alerte', 'alert-email', $this->language, 'Vous devez choisir une adresse email valide pour envoyer le mail !') ?>');
        }
    });

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>
