<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-12">
        <h2><?= $this->ln->txt('admin-mails', 'history', $this->language, 'Historique des emails') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?= $this->ln->txt('admin-mails', 'mails', $this->language, 'Mails') ?>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-mails', 'liste-mails-history', $this->language, 'Liste des email envoyés') ?>&nbsp;<span class="badge badge-primary"><?=$this->mails_filer->count()?></span></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    if ($this->mails_filer->count() > 0) {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-history-date', $this->language, 'Date') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-history-from', $this->language, 'From') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-history-dest-server', $this->language, 'Destinataire (Server)') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-history-dest-nmp', $this->language, 'Destinataire (NMP)') ?></th>
                                        <th class="col-lg-3"><?= $this->ln->txt('admin-mails', 'mails-history-subject', $this->language, 'Sujet') ?></th>
                                        <th class="col-lg-1"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($this->mails_filer as $mail) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?=date($this->clSettings->getParam('format-de-date', 'sets'), strtotime($mail->added))?>&nbsp;
                                                <?=date('H:i', strtotime($mail->added))?>
                                            </td>
                                            <td>
                                                <?=$mail->from?>
                                            </td>
                                            <td>
                                                <?=$mail->to?>
                                            </td>
                                            <td>
                                                <?=$mail->email_destinataire?>
                                            </td>
                                            <td>
                                                <?=$mail->subject?>
                                            </td>
                                            <td>
                                                <a href="<?=$this->lurl?>/mails/formTemplate/<?=$mail->type?>" class="btn btn-white btn-sm btn-action">
                                                    <i class="fa fa-eye"></i> <?= $this->ln->txt('admin-generic', 'apercu', $this->language, 'Aperçu') ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                        <span class="font-bold" style="margin-left: 15px;"><?= $this->ln->txt('admin-mails', 'empty-mails', $this->language, 'Il n\'y a aucun template de mail pour le moment') ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>