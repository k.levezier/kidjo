<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-mails', 'gestion-templates', $this->language, 'Gestion des templates d\'emails') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?= $this->ln->txt('admin-mails', 'mails', $this->language, 'Mails') ?>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/mails/formTemplate" class="btn btn-primary"><?= $this->ln->txt('admin-mails', 'addmail', $this->language, 'Ajouter un template de mail') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-mails', 'liste-mails-templates', $this->language, 'Liste des templates de mail') ?>&nbsp;<span class="badge badge-primary"><?=$this->mails_text->count()?></span></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?php
                    if ($this->mails_text->count() > 0) {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-lg-3"><?= $this->ln->txt('admin-mails', 'mails-nom', $this->language, 'Nom') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-type', $this->language, 'Type') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-expediteur', $this->language, 'Expéditeur') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-sujet', $this->language, 'Sujet') ?></th>
                                        <th class="col-lg-1"><?= $this->ln->txt('admin-mails', 'mails-maj', $this->language, 'Mise à jour') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-mails', 'mails-actions', $this->language, 'Actions') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($this->mails_text as $template) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?=$template->name?>
                                            </td>
                                            <td class="one-click-select">
                                                <?=$template->type?>
                                            </td>
                                            <td>
                                                <?=$template->exp_name?><br/>
                                                <<?=$template->exp_email?>>
                                            </td>
                                            <td>
                                                <?=$template->subject?>
                                            </td>
                                            <td>
                                                <?=date($this->clSettings->getParam('format-de-date', 'sets'), strtotime($template->updated))?>
                                            </td>
                                            <td>
                                                <a href="<?=$this->lurl?>/mails/formTemplate/<?=$template->type?>" class="btn btn-white btn-sm btn-action">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-mails', 'mails-edit', $this->language, 'Modifier') ?>
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn-danger btn-sm btn-action">
                                                    <i class="fa fa-times"></i> <?= $this->ln->txt('admin-mails', 'mails-delete', $this->language, 'Supprimer') ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                        <span class="font-bold" style="margin-left: 15px;"><?= $this->ln->txt('admin-mails', 'empty-mails', $this->language, 'Il n\'y a aucun template de mail pour le moment') ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">    
    // Séléction le contenu et le place dans le presse papier
    $(document).on('click', '.one-click-select', function(){
        var _this = $(this);
        var node = $(this).get(0);
        var text = $(this).text().toString();
        
        var doc = document,
            range,
            selection;
        if (window.getSelection) {
            // On sélectionne le texte de la fonction
            selection = window.getSelection();        
            range = document.createRange();
            //range.selectNodeContents(text.split(':')[1]);
            range.setStart(node.firstChild, 0);
            range.setEnd(node.firstChild, text.length);
            selection.removeAllRanges();
            selection.addRange(range);
            
            // On copie dans le presse papier et on ferme le tooltip 1,5sec plus tard
            document.execCommand("copy");
            toastr["success"]('<?= $this->ln->txt('admin-generic', 'copied-to-clipboard-all', $this->language, 'Copiée dans le presse-papier')?>');
        }
    });
</script>