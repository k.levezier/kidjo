<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-mails', 'gestion-templates', $this->language, 'Gestion des templates d\'emails') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/mails/templates"><?= $this->ln->txt('admin-mails', 'mails', $this->language, 'Mails') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-mails', 'mails-template-'.($this->new?'add':'edit'), $this->language, ($this->new?'Ajout':'Modification').' d\'un template de mail') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/mails/templates' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-mails', 'back-mails', $this->language, 'Retour aux mails') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-mails', 'mail-template', $this->language, 'Template de mail') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <form method="POST" name="formZone" id="formTransp" enctype="multipart/form-data" >
                        <input type="hidden" name="sendForm" />
                        <input type="hidden" name="restePage" id="restePage" value="0" />
                        <input type="hidden" name="id_textemail" id="id_textemail" value="<?=$_POST['id_textemail']?>" />
                        <input type="hidden" value="<?= in_array($this->params['lng'], array_keys($this->ln->tabLangues))?$this->params['lng']:$this->language ?>" id="selectedLang" name="selectedLang" />
                        <div class="container-fluid">
                            <fieldset class="form-horizontal">     
                                
                                <div class="form-group <?=isset($_POST['sendForm']) && $this->errorNom == true?'has-error':''?>">
                                    <?php $this->label = $this->ln->txt('admin-mails', 'form-nom-mail', $this->language, 'Nom'); ?>
                                    <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'name', $_POST, $this->label) ?>      
                                    <?=isset($_POST['sendForm']) && $this->errorNom == true?'<span class="help-block m-b-none">' . $this->errorMsg . '</span>':''?>
                                </div>      
                                
                                <div class="form-group <?=isset($_POST['sendForm']) && $this->errorNomExp == true?'has-error':''?>">
                                    <?php $this->label = $this->ln->txt('admin-mails', 'form-nom-exp-mail', $this->language, 'Nom d\'expéditeur'); ?>
                                    <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'exp_name', $_POST, $this->label) ?>      
                                    <?=isset($_POST['sendForm']) && $this->errorNomExp == true?'<span class="help-block m-b-none">' . $this->errorMsg . '</span>':''?>
                                </div> 
                                
                                <div class="form-group <?=isset($_POST['sendForm']) && $this->errorAddrExp == true?'has-error':''?>">
                                    <?php $this->label = $this->ln->txt('admin-mails', 'form-mail-exp-mail', $this->language, 'Adresse d\'expéditeur'); ?>
                                    <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'exp_email', $_POST, $this->label) ?>      
                                    <?=isset($_POST['sendForm']) && $this->errorAddrExp == true?'<span class="help-block m-b-none">' . $this->errorMsg . '</span>':''?>
                                </div>       
                                
                                <div class="form-group <?=isset($_POST['sendForm']) && $this->errorSubj == true?'has-error':''?>">
                                    <?php $this->label = $this->ln->txt('admin-mails', 'form-subject-mail', $this->language, 'Sujet'); ?>
                                    <?= $this->clTemplates->affichageFormBO(null, null, true, 'Texte', 'subject', $_POST, $this->label) ?>      
                                    <?=isset($_POST['sendForm']) && $this->errorSubj == true?'<span class="help-block m-b-none">' . $this->errorMsg . '</span>':''?>
                                </div>       
                                
                                <div class="form-group <?=isset($_POST['sendForm']) && $this->errorContent == true?'has-error':''?>">
                                    <?php $this->label = $this->ln->txt('admin-mails', 'form-content-mail', $this->language, 'Contenu'); ?>
                                    <?= $this->clTemplates->affichageFormBO(null, null, true, 'Textearea', 'content', $_POST, $this->label) ?>      
                                    <?=isset($_POST['sendForm']) && $this->errorContent == true?'<span class="help-block m-b-none">' . $this->errorMsg . '</span>':''?>
                                </div>        
                                
                            </fieldset>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-2">
                                    <a href="<?= $this->lurl ?>/statistiques/campagnes" class="btn btn-white">
                                        <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                    </a>
                                    <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                    <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">    
    // Changement de la langues du formulaire
    $(document).on('click', '.change-lang-form', function () {
        var currentLang = $('#selectedLang').val();
        var newLang = $(this).data('lang');
        if (newLang != currentLang) {
            $('.field_lang_' + newLang).removeClass('hidden_field_lang');
            $('.field_lang_' + currentLang).addClass('hidden_field_lang');
            $('#selectedLang').val(newLang);
            $('.button-lang-form').find('img').attr('src', addSURL + '/images/admin/flags/' + newLang + '.png');
        }
    });
</script>