<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-videos', 'allvideos', $this->language, 'All videos') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?= $this->ln->txt('admin-videos', 'videos', $this->language, 'Videos') ?>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/videos/formVideo" class="btn btn-primary">
                <?= $this->ln->txt('admin-videos', 'upload_videos', $this->language, 'Upload videos') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content pt40">
                    <form action="<?= $this->lurl . '/videos/all_videos' ?>"
                          method="POST"
                          class="form-horizontal form-search-events">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-lg-6" style="width: 67.333% !important;">
                                    <div class="form-group">
                                        <div class="col-lg-9" style="margin-left: 10%; width: 90%;">
                                            <input name="title" id="title" type="text" class="form-control"
                                                   placeholder="<?= $this->ln->txt('admin-videos', 'label-search-videos', $this->language, 'Search a videos') ?>"
                                                   value="<?=$_SESSION['filter_videos']['title']?>"
                                            >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">

                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="video-language"
                                           class="col-lg-3 control-label">
                                        <?= $this->ln->txt('admin-videos', 'label-language', $this->language, 'Language') ?>
                                    </label>

                                    <div class="col-lg-9">
                                        <select id="video-language" name="video-language[]"
                                                data-placeholder="<?= $this->ln->txt('admin-videos', 'placeholder-language', $this->language, 'Language') ?>"
                                                class="chosen-select  form-control" tabindex="3" multiple="multiple"
                                                style="height: 110px;">
                                            <option value="0" <?=(in_array('0',$_SESSION['filter_videos']['video-language'])?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->languages as $lang){
                                                ?>
                                                <option value="<?= $lang->id_language?>"
                                                        <?=(in_array($lang->id_language,$_SESSION['filter_videos']['video-language'])?'selected':'')?>>
                                                    <?= $lang->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="video-owner"
                                           class="col-lg-3 control-label"><?= $this->ln->txt('admin-videos', 'label-owner', $this->language, 'Owners') ?></label>

                                    <div class="col-lg-9">
                                        <select id="video-owner" name="video-owner"
                                                data-placeholder="<?= $this->ln->txt('admin-videos', 'placeholder-owner', $this->language, 'Owners') ?>"
                                                class="chosen-select  form-control">
                                            <option value="0" <?=($_SESSION['filter_videos']['videos-owner'] == 0?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->owners as $owner){
                                                ?>
                                                <option value="<?= $owner->id_owner?>"
                                                        <?=($_SESSION['filter_videos']['videos-owner'] == $owner->id_owner?'selected':'')?>>
                                                    <?= $owner->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4" style="margin-top: 22px;">

                                <div class="form-group">
                                    <label for="video-folders"
                                           class="col-lg-3 control-label"><?= $this->ln->txt('admin-videos', 'label-folder', $this->language, 'Folder') ?></label>

                                    <div class="col-lg-9">
                                        <select id="video-folders" name="video-folders"
                                                data-placeholder="<?= $this->ln->txt('admin-videos', 'placeholder-folder', $this->language, 'Folder') ?>"
                                                class="chosen-select  form-control">
                                            <option value="0" <?=($_SESSION['filter_video']['id_folder'] == 0?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->folders as $folder){
                                                ?>
                                                <option value="<?= $folder->id_folder?>"
                                                        <?=($_SESSION['filter_videos']['video-folders'] == $folder->id_folder?'selected':'')?>>
                                                    <?= $folder->title ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" style="margin-top: 23px;">
                                    <label for="video_licence"
                                           class="col-lg-3 control-label" style="padding-top: 0px;"><?= $this->ln->txt('admin-videos', 'label-licence', $this->language, 'Licence') ?></label>

                                    <div class="col-lg-9">
                                        <select id="video_licence" name="video_licence"
                                                data-placeholder="<?= $this->ln->txt('admin-videos', 'placeholder-licence', $this->language, 'Licence') ?>"
                                                class="chosen-select  form-control" tabindex="3">
                                            <option value="0" <?=($_SESSION['filter_videos']['video_licence'] == "0"?'selected':'')?>>All</option>
                                            <?php
                                            foreach($this->licences as $l){
                                                ?>
                                                <option value="<?= $l->id_licence?>"
                                                        <?=($_SESSION['filter_videos']['video_licence'] == $l->id_licence ?'selected':'')?>>
                                                    <?= $l->name ?>
                                                </option>
                                                <?php
                                            }?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4" style="margin-top: 22px;">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="i-checks">
                                                <label for="active_videos"
                                                       class="col-lg-6 control-label"><?= $this->ln->txt('admin-videos', 'label-active-video', $this->language, 'Active video') ?></label>
                                                <div class="col-lg-3" style="margin-top: 10px;">
                                                    <input name="active_videos" id="active_videos"
                                                           class="form-control"
                                                           type="checkbox"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <div class="i-checks-dialogue">
                                                <label for="no_dialogue"
                                                       class="col-lg-6 control-label"><?= $this->ln->txt('admin-videos', 'label-no-dialogue-video', $this->language, 'No dialogue') ?></label>
                                                <div class="col-lg-3" style="margin-top: 10px;">
                                                    <input name="no_dialogue" id="no_dialogue"
                                                           class="form-control"
                                                           type="checkbox"
                                                    >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 align-center mt20" style="text-align: center;">
                                <input type="hidden" name="filter" value="ok">
                                <button class="btn btn-primary uppercase" type="submit">
                                    <?= $this->ln->txt('admin-folder', 'filter', $this->language, 'Filter') ?>
                                </button>
                                <br>
                                <a href="<?= $this->lurl ?>/videos/all_videos/reset">
                                    <?= $this->ln->txt('admin-folder', 'reset-filters', $this->language, 'Reset filters') ?>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?=count($this->LVideos)?>&nbsp;<?= $this->ln->txt('admin-videos', 'existing-videos', $this->language, 'existing videos') ?><span class="badge badge-primary"></span></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <?php
                    if (count($this->LVideos) > 0) {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                <tr>
                                    <th class="col-lg-3"><?= $this->ln->txt('admin-videos', 'video-title-subtitle', $this->language, 'Title & Subtitle') ?></th>
                                    <th class="col-lg-2"><?= $this->ln->txt('admin-videos', 'video-language', $this->language, 'Language') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-videos', 'video-thumbnail', $this->language, 'Thumbnail') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-videos', 'video-active', $this->language, 'Active') ?></th>
                                    <th class="col-lg-1"><?= $this->ln->txt('admin-videos', 'video-premium', $this->language, 'Premium') ?></th>
                                    <th class="col-lg-2"><?= $this->ln->txt('admin-videos', 'video-folders', $this->language, 'Folders') ?></th>
                                    <th class="col-lg-2"><?= $this->ln->txt('admin-videos', 'video-actions', $this->language, 'Actions') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($this->LVideos as $v) {
                                    $Lfolders_videos = new o\data('folders_videos');
                                    $Lfolders_videos->addWhere('id_video = '.$v['id_video']);
                                    ?>
                                    <tr>
                                        <td>
                                            <?=$v['title']?><br />
                                            <i><?=$v['subtitle']?></i>
                                        </td>
                                        <td>
                                            <?php
                                            if($v['id_language'] > 0) {
                                                $this->languages = new o\languages($v['id_language']);
                                                echo $this->languages->name;
                                            }else{
                                                echo "None";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            Thumbnail
                                        </td>
                                        <td>
                                            <?php
                                            switch($v['active']){
                                                case 0:
                                                    echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-videos', 'form-folder-status-off', $this->language, 'Off').'"></i>';
                                                    break;
                                                case 1:
                                                    echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-videos', 'form-folder-status-on', $this->language, 'Active').'"></i>';
                                                    break;
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            switch($v['premium']){
                                                case 0:
                                                    echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-videos', 'form-folder-status-off', $this->language, 'Off').'"></i>';
                                                    break;
                                                case 1:
                                                    echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-videos', 'form-folder-status-on', $this->language, 'Active').'"></i>';
                                                    break;
                                            }
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            $sep = "";
                                            if($Lfolders_videos->count() > 0) {
                                                foreach($Lfolders_videos as $video_folder){
                                                    if(is_null($video_folder->id_folder)){
                                                        echo "None";
                                                    }else{
                                                        $this->folders = new o\folders($video_folder->id_folder);
                                                        echo $sep.$this->folders->title;
                                                        $sep=",";
                                                    }
                                                }
                                            }else{
                                                echo "None";
                                            }
                                            ?>
                                        </td>



                                        <td>
                                            <a href="<?=$this->lurl?>/videos/formVideo/video___<?=$v['id_video']?>" class="btn btn-white btn-action">
                                                <?= $this->ln->txt('admin-videos', 'folder-details', $this->language, 'Détails') ?>
                                            </a>
                                            <a class="btn btn-danger confirm" data-idvideo="<?= $v['id_video'] ?>">
                                                <?= $this->ln->txt('admin-administration', 'delete-folder', $this->language, 'Delete') ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                        <span class="font-bold" style="margin-left: 15px;"><?= $this->ln->txt('admin-videos', 'empty-video', $this->language, 'No video') ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        $('.i-checks-dialogue').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});


        <?php
        if($_SESSION['filter_videos']['active_videos'] == "1"){
            ?>
            $('.i-checks').iCheck('check'); //To check the radio button
            <?php
        }
        if($_SESSION['filter_videos']['no_dialogue'] == "1"){
            ?>
            $('.i-checks-dialogue').iCheck('check'); //To check the radio button
            <?php
        }
        ?>

        $('.confirm').on('click', function () {
            var _id = $(this).data('idvideo');
            swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-administration', 'delete-log-alert', $this->language, 'Confirmez vous la suppression de l\'enregistrement ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl ?>/videos/deleteVideo/video___'+ _id);
                        return false;
                    }
                });
        });
    });
</script>