<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <title><?= $this->ln->txt('admin-login', 'titre', $this->language, 'Administration du site') ?></title>
    <link rel="shortcut icon" href="<?= $this->surl ?>/images/admin/favicon.ico" type="image/x-icon">
    <?= $this->callCss() ?>
    <?= $this->callJs() ?>
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name"><?= $this->ln->txt('admin-login', 'logo', $this->language, 'Equinoa') ?></h1>
        </div>
        <?php if (isset($_POST['sendPassword']) && !$this->errorForm) { ?>
            <p><?= $this->ln->txt('admin-login', 'ok-lostpassword', $this->language, 'Un nouveau mot de passe a été généré.<br>Il vient de vous être envoyé par email') ?></p>
        <?php } else { ?>
            <p><?= $this->ln->txt('admin-login', 'intro-lostpassword', $this->language, 'Avant de pouvoir réinitialiser votre mot de passe, vous devez indiquer votre adresse email') ?></p>
        <?php } ?>
        <form class="m-t" role="form" method="post" name="formLostpassword" id="formLostpassword" action="<?= $this->lurl ?>/lostpassword">
            <?php if (!isset($_POST['sendPassword']) || (isset($_POST['sendPassword']) && $this->errorForm)) { ?>
                <div class="form-group<?= (isset($_POST['sendPassword']) && $this->errorForm ? ' has-error' : '') ?>">
                    <input type="email" class="form-control" placeholder="<?= $this->ln->txt('admin-login', 'email', $this->language, 'Adresse email') ?>" name="email" id="email" required>
                    <?= (isset($_POST['sendPassword']) && $this->errorForm ? '<p><small>' . $this->errorMsg . '</small></p>' : '') ?>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" name="sendPassword" id="sendPassword">
                    <?= $this->ln->txt('admin-login', 'btn-envoyer-mdp', $this->language, 'Envoyer le mot de passe') ?>
                </button>
            <?php } ?>
            <a href="<?= $this->lurl ?>/login">
                <small><?= $this->ln->txt('admin-login', 'retour-login', $this->language, 'Revenir à la page de connexion') ?></small>
            </a>
        </form>
        <p class="m-t">
            <small>
                <a href="<?= $this->version['copy_lien'] ?>" target="_blank" title="<?= $this->version['copy_nom'] ?>"><?= $this->version['copy_nom'] ?></a>
                &copy; <?= ($this->version['copy_annee'] < date('Y') ? $this->version['copy_annee'] . ' - ' : '') ?><?= date('Y') ?>
            </small>
        </p>
    </div>
</div>
</body>
</html>
