<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <title><?= $this->ln->txt('admin-login', 'titre', $this->language, 'Administration du site') ?></title>
    <link rel="shortcut icon" href="<?= $this->surl ?>/images/admin/favicon.ico" type="image/x-icon">
    <?= $this->callCss() ?>
    <?= $this->callJs() ?>
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name"><?= $this->ln->txt('admin-login', 'logo-uriage', $this->language, 'Uriage') ?></h1>
        </div>
        <?php if (isset($this->params['acces']) && $this->params['acces'] == 'interdit') { ?>
            <p class="warning">
                <?= $this->ln->txt('admin-login', 'acces-interdit', $this->language, 'Vous n\'avez pas accès à cette page.<br>Merci de vous connecter avec un compte ayant les droits pour y accéder.') ?>
            </p>
        <?php } ?>
        <form class="m-t" role="form" method="post" name="formLogin" id="formLogin" action="<?= $this->lurl ?>/login">
            <div class="form-group<?= (isset($_POST['sendLogin']) && ($this->errorFormEmail || $this->errorFormLogin) ? ' has-error' : (isset($_POST['sendLogin']) && $this->warningFormLogin ? ' has-warning' : '')) ?>">
                <input type="email" class="form-control" placeholder="<?= $this->ln->txt('admin-login', 'email', $this->language, 'Adresse email') ?>" name="email" id="email" value="<?= $_POST['email'] ?>" required>
                <?= (isset($_POST['sendLogin']) && $this->errorFormEmail ? '<p><small>' . $this->errorMsg . '</small></p>' : '') ?>
            </div>
            <div class="form-group<?= (isset($_POST['sendLogin']) && ($this->errorFormPassword || $this->errorFormLogin) ? ' has-error' : (isset($_POST['sendLogin']) && $this->warningFormLogin ? ' has-warning' : '')) ?>">
                <input type="password" class="form-control" placeholder="<?= $this->ln->txt('admin-login', 'password', $this->language, 'Mot de passe') ?>" name="password" id="password" value="<?= $_POST['password'] ?>" required>
                <?= (isset($_POST['sendLogin']) && $this->errorFormPassword ? '<p><small>' . $this->errorMsg . '</small></p>' : '') ?>
                <?= (isset($_POST['sendLogin']) && $this->errorFormLogin ? '<p><small>' . $this->errorMsg . '</small></p>' : '') ?>
                <?= (isset($_POST['sendLogin']) && $this->warningFormLogin ? '<p><small>' . $this->warningMsg . '</small></p>' : '') ?>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b" name="sendLogin" id="sendLogin"><?= $this->ln->txt('admin-login', 'btn-login', $this->language, 'Connexion') ?></button>
            <a href="<?= $this->lurl ?>/lostpassword">
                <small><?= $this->ln->txt('admin-login', 'lost-password', $this->language, 'Mot de passe oublié ?') ?></small>
            </a>
        </form>
        <p class="m-t">
            <small>
                <a href="<?= $this->version['copy_lien'] ?>" target="_blank" title="<?= $this->version['copy_nom'] ?>"><?= $this->version['copy_nom'] ?></a>
                &copy; <?= ($this->version['copy_annee'] < date('Y') ? $this->version['copy_annee'] . ' - ' : '') ?><?= date('Y') ?>
            </small>
        </p>
    </div>
</div>
</body>
</html>
