<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <title><?= $this->ln->txt('admin-login', 'titre', $this->language, 'Administration du site') ?></title>
    <link rel="shortcut icon" href="<?= $this->surl ?>/images/admin/favicon.ico" type="image/x-icon">
    <?= $this->callCss() ?>
    <?= $this->callJs() ?>
</head>
<body class="gray-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name"><?= $this->ln->txt('admin-login', 'logo', $this->language, 'Equinoa') ?></h1>
        </div>
        <p><?= $this->ln->txt('admin-login', 'intro-choicesite', $this->language, 'Vous êtes l\'administrateur de plusieurs sites.<br>Sélectionner le site à administrer.') ?></p>
        <form class="m-t" role="form" method="post" name="formChoiceSite" id="formChoiceSite" action="<?= $this->lurl ?>/choiceSite">
            <div class="form-group<?= (isset($_POST['sendChoiceSite']) && $this->errorForm ? ' has-error' : '') ?>">
                <select class="form-control" name="id_site" id="id_site">
                    <option value=""><?= $this->ln->txt('admin-choicesite', 'selectionner', $this->language, 'Sélectionner') ?></option>
                    <?php foreach ($this->lSitesUser as $site) { ?>
                        <option value="<?= $site['id_site'] ?>"><?= $site['nom'] ?></option>
                    <?php } ?>
                </select>
                <?= (isset($_POST['sendChoiceSite']) && $this->errorForm ? '<p><small>' . $this->errorMsg . '</small></p>' : '') ?>
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b" name="sendChoiceSite" id="sendChoiceSite"><?= $this->ln->txt('admin-choicesite', 'btn-envoyer-site', $this->language, 'Administrer le site') ?></button>
        </form>
        <p class="m-t">
            <small>
                <a href="<?= $this->version['copy_lien'] ?>" target="_blank" title="<?= $this->version['copy_nom'] ?>"><?= $this->version['copy_nom'] ?></a>
                &copy; <?= ($this->version['copy_annee'] < date('Y') ? $this->version['copy_annee'] . ' - ' : '') ?><?= date('Y') ?>
            </small>
        </p>
    </div>
</div>
</body>
</html>
