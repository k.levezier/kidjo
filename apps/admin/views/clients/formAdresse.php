<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-clients', 'gestion-addr', $this->language, 'Gestion des adresses') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/clients/formClient/<?=$this->params[0]?>/part___2"><?= $this->ln->txt('admin-clients', 'client', $this->language, 'Client') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-clients', 'client-addr-'.($this->new?'add':'edit'), $this->language, ($this->new?'Ajout':'Modification').' d\'une adresse client') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/clients/formClient/<?=$this->params[0]?>/part___2" class="btn btn-primary">
                <?= $this->ln->txt('admin-clients', 'back-client', $this->language, 'Retour au client') ?>
            </a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>
                        <?= $this->ln->txt('admin-clients', 'infos-addr', $this->language, 'Informations sur l\'adresse') ?>
                    </h5>
                </div>
                <div class="ibox-content">
                    <form method="POST" name="formClient" id="formClient" enctype="multipart/form-data" >
                        <input type="hidden" name="sendForm" />
                        <input type="hidden" name="restePage" id="restePage" value="0" />
                        <input type="hidden" name="part" id="part" value="1" />
                        <input type="hidden" name="id_client" id="id_client" value="<?= $_POST['id_client'] ?>" />
                        <input type="hidden" name="id_adresse" id="id_adresse" value="<?= $_POST['id_adresse'] ?>" />
                        <div class="container-fluid">
                            <fieldset class="form-horizontal" autocomplete="off">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><?= $this->ln->txt('admin-clients', 'form-client-lab-civilite', $this->language, 'Civilité') ?></label>
                                    <div class="col-sm-6">
                                        <div class="radio i-checks inline">
                                            <label>
                                                <input type="radio" value="M." name="civilite" <?= ($_POST['civilite'] == 'M.' ? ' checked' : '') ?>>
                                                <i></i> <?= $this->ln->txt('admin-clients', 'form-adresse-civilite-m', $this->language, 'M.') ?>
                                            </label>
                                        </div>
                                        <div class="radio i-checks inline">
                                            <label>
                                                <input type="radio" value="Mme" name="civilite" <?= ($_POST['civilite'] == 'Mme' ? ' checked' : '') ?>>
                                                <i></i> <?= $this->ln->txt('admin-clients', 'form-adresse-civilite-mme', $this->language, 'Mme') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormNom ? ' has-error' : '') ?>">
                                    <label class="col-sm-2 control-label" for="nom"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-nom', $this->language, 'Nom') ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nom" id="nom" value="<?= $_POST['nom'] ?>" required/>
                                        <?= (isset($_POST['sendForm']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                    </div>
                                </div>
                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormPrenom ? ' has-error' : '') ?>">
                                    <label class="col-sm-2 control-label" for="prenom"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-prenom', $this->language, 'Prenom') ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="prenom" id="prenom" value="<?= $_POST['prenom'] ?>" required/>
                                        <?= (isset($_POST['sendForm']) && $this->errorFormPrenom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="societe"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-societe', $this->language, 'Société') ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="societe" id="societe" value="<?= $_POST['societe'] ?>"/>  
                                    </div>
                                </div>
                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormAdresse1 ? ' has-error' : '') ?>">
                                    <label class="col-sm-2 control-label" for="adresse1"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-adresse1', $this->language, 'Adresse') ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="adresse1" id="adresse1" value="<?= $_POST['adresse1'] ?>" required/>  
                                        <?= (isset($_POST['sendForm']) && $this->errorFormAdresse1 ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="adresse2"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-adresse2', $this->language, 'Complément d\'adresse') ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="adresse2" id="adresse2" value="<?= $_POST['adresse2'] ?>"/>  
                                    </div>
                                </div>
                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormCp ? ' has-error' : '') ?>">
                                    <label class="col-sm-2 control-label" for="cp"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-cp', $this->language, 'Code postal') ?></label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="cp" id="cp" value="<?= $_POST['cp'] ?>" required/>  
                                        <?= (isset($_POST['sendForm']) && $this->errorFormCp ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                    </div>
                                </div>
                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormVille ? ' has-error' : '') ?>">
                                    <label class="col-sm-2 control-label" for="ville"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-ville', $this->language, 'Ville') ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="ville" id="ville" value="<?= $_POST['ville'] ?>" required/>  
                                        <?= (isset($_POST['sendForm']) && $this->errorFormVille ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                    </div>
                                </div>
                                <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormIdPays ? ' has-error' : '') ?>">
                                    <label class="col-sm-2 control-label" for="id_pays"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-pays', $this->language, 'Pays') ?></label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="id_pays" name="id_pays" required>
                                            <option value="" selected disabled><?= $this->ln->txt('admin-clients', 'form-adresse-lab-pays-choose', $this->language, 'Choisir un pays') ?></option>
                                            <?php
                                            foreach((new o\data('countries', array('status'=>1)))->order('ordre') as $pays) {
                                                ?><option value="<?=$pays->id_pays?>" <?= $_POST['id_pays']==$pays->id_pays?'selected':'' ?>><?=$pays->{$this->language}?></option><?php
                                            }
                                            ?>
                                        </select>
                                        <?= (isset($_POST['sendForm']) && $this->errorFormIdPays ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="telephone"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-telephone', $this->language, 'Téléphone') ?></label>
                                    <div class="col-sm-10">
                                        <input type="number" class="form-control" name="telephone" id="telephone" value="<?= $_POST['telephone'] ?>"/>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="nom_adresse"><?= $this->ln->txt('admin-clients', 'form-adresse-lab-nom_adresse', $this->language, 'Nom de l\'adresse') ?></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nom_adresse" id="nom_adresse" value="<?= $_POST['nom_adresse'] ?>"/>  
                                    </div>
                                </div>
                            </fieldset>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-2">
                                    <a href="<?= $this->lurl ?>/clients/formClient/<?=$this->params[0]?>/part___2" class="btn btn-white">
                                        <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                    </a>
                                    <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                    <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">    
    function initialiseJqueryPlugins() {
        
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        
        // Suppression de la promotion /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.confirmChangeEtat').on('click', function () {
            var _this = $(this);
            
            if( ! _this.hasClass('active')){
                swal({
                    title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                    text: "<?= $this->ln->txt('admin-dashboard', 'transac-change-etat-alert', $this->language, 'Confirmez vous le changement d\'état de la commande ?') ?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                    cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                    closeOnConfirm: false,
                    closeOnCancel: true},
                function (isConfirm) {
                    if (isConfirm) {
                        $(location).attr('href', '<?= $this->lurl.'/dashboard/etatCommande/'.$this->transactions->id_transaction.'/'?>'+_this.data('etat'));
                        return false;
                    }
                });
            }
        });
    }
    
    $(document).ready(function () {
        initialiseJqueryPlugins();
    });
    
    // Séléction le contenu et le place dans le presse papier
    $(document).on('click', '.one-click-select', function(){
        var _this = $(this);
        var node = $(this).get(0);
        var text = $(this).text().toString();
        
        var doc = document,
            range,
            selection;
        if (window.getSelection) {
            // On sélectionne le texte de la fonction
            selection = window.getSelection();        
            range = document.createRange();
            //range.selectNodeContents(text.split(':')[1]);
            range.setStart(node.firstChild, 0);
            range.setEnd(node.firstChild, text.length);
            selection.removeAllRanges();
            selection.addRange(range);
            
            // On copie dans le presse papier et on ferme le tooltip 1,5sec plus tard
            document.execCommand("copy");
            toastr["success"]('<?= $this->ln->txt('admin-generic', 'copied-to-clipboard-all', $this->language, 'Copiée dans le presse-papier')?>');
        }
    });
</script>