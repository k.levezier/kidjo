<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-6">
        <h2><?= $this->ln->txt('admin-clients', 'gestion-clients', $this->language, 'Gestion des clients') ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= $this->lurl ?>/clients/gestion"><?= $this->ln->txt('admin-clients', 'clients', $this->language, 'Clients') ?></a>
            </li>
            <li class="active">
                <strong><?= $this->ln->txt('admin-clients', 'client-'.($this->new?'add':'edit'), $this->language, ($this->new?'Ajout':'Modification').' d\'un client') ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-6">
        <div class="title-action">
            <a href="<?= $this->lurl . '/clients/gestion' ?>" class="btn btn-primary">
                <?= $this->ln->txt('admin-clients', 'back-clients', $this->language, 'Retour aux clients') ?>
            </a>
            <?php
            if (!$this->new) {
                ?>
                <a class="btn btn-danger confirmAlert">
                    <?= $this->ln->txt('admin-clients', 'del-client', $this->language, 'Supprimer le client') ?>
                </a>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">          
            <form method="POST" name="formClient" id="formClient" enctype="multipart/form-data" >
                
                <input type="hidden" name="sendForm" />
                <input type="hidden" name="restePage" id="restePage" value="0" />
                <input type="hidden" name="part" id="part" value="1" />
                <input type="hidden" name="id_client" id="id_client" value="<?=$_POST['id_client']?>" />
                
                <div class="tabs-container tabs-container-products">
                    <div class="tabs-left">
                        <ul class="nav nav-tabs">
                            <li <?=$this->params['part'] == 1 || !isset($this->params['part']) ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-informations">
                                    <i class="fa fa-info-circle"></i> 
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-clients', 'client-infos', $this->language, 'Informations') ?></span>
                                </a>
                            </li>
                            <?php
                            if (!$this->new) {
                                ?>
                                <li <?=$this->params['part'] == 2 ? 'class="active"':''?>>
                                    <a data-toggle="tab" href="#tab-adresses">
                                        <i class="fa fa-truck"></i>
                                        <span class="hideOnMobile"><?= $this->ln->txt('admin-clients', 'client-adresses', $this->language, 'Adresses') ?></span>
                                    </a>
                                </li>
                                <?php
                            }
                            ?>
                            <li <?=$this->params['part'] == 3 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-commandes">
                                    <i class="fa fa-copy"></i>
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-clients', 'client-commandes', $this->language, 'Commandes') ?></span>
                                </a>
                            </li>
                            <li <?=$this->params['part'] == 4 ? 'class="active"':''?>>
                                <a data-toggle="tab" href="#tab-droits">
                                    <i class="fa fa-cogs"></i>
                                    <span class="hideOnMobile"><?= $this->ln->txt('admin-clients', 'client-droits', $this->language, 'Droits') ?></span>
                                </a>
                            </li>
                        </ul>
                        
                        <?php $i=1; ?>
                        <div class="tab-content ">
                            <div id="tab-informations" class="tab-pane <?=$this->params['part'] == $i || !isset($this->params['part']) ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal" autocomplete="off">
                                            <input type="email" name="email" style="display:none;">
                                            <input type="password" name="password" style="display:none;">
                                            
                                            <h4><?= $this->ln->txt('admin-clients', 'client-infos-desc', $this->language, 'Informations client') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"><?= $this->ln->txt('admin-clients', 'form-client-lab-civilite', $this->language, 'Civilité') ?></label>
                                                <div class="col-sm-6">
                                                    <div class="radio i-checks inline">
                                                        <label>
                                                            <input type="radio" value="M." name="civilite" <?= ($_POST['civilite'] == 'M.' ? ' checked' : '') ?>>
                                                            <i></i> <?= $this->ln->txt('admin-clients', 'form-client-civilite-m', $this->language, 'M.') ?>
                                                        </label>
                                                    </div>
                                                    <div class="radio i-checks inline">
                                                        <label>
                                                            <input type="radio" value="Mme" name="civilite" <?= ($_POST['civilite'] == 'Mme' ? ' checked' : '') ?>>
                                                            <i></i> <?= $this->ln->txt('admin-clients', 'form-client-civilite-mme', $this->language, 'Mme') ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormNom ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="nom"><?= $this->ln->txt('admin-clients', 'form-client-lab-nom', $this->language, 'Nom') ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="nom" id="nom" value="<?=$_POST['nom']?>" required/>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormNom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                                </div>
                                            </div>
                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormPrenom ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="prenom"><?= $this->ln->txt('admin-clients', 'form-client-lab-prenom', $this->language, 'Prenom') ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control" name="prenom" id="prenom" value="<?=$_POST['prenom']?>" required/>
                                                    <?= (isset($_POST['sendForm']) && $this->errorFormPrenom ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                                </div>
                                            </div>
                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormEmail ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="email_chromehack"><?= $this->ln->txt('admin-clients', 'form-client-lab-email', $this->language, 'Email') ?></label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                        <input type="email" class="form-control" name="email_chromehack" id="email_chromehack" value="<?=$_POST['email']?>" required/>
                                                        <?= (isset($_POST['sendForm']) && $this->errorFormEmail ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group<?= (isset($_POST['sendForm']) && $this->errorFormPass ? ' has-error' : '') ?>">
                                                <label class="col-sm-2 control-label" for="password_chromehack"><?= $this->ln->txt('admin-clients', 'form-client-lab-pass', $this->language, 'Mot de passe') ?></label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-lock" style="font-size:18px;"></i></span>
                                                        <span class="input-group-addon addon-primary" id="generate_password"><i class="fa fa-refresh"></i></span>
                                                        <input type="password" class="form-control" name="password_chromehack" id="password_chromehack" value="" />
                                                        <span class="input-group-addon addon-btn-end" id="copy_password"><i class="fa fa-copy"></i></span>
                                                        <span class="input-group-addon addon-btn-end" id="see_password"><i class="fa fa-eye"></i></span>
                                                        <?= (isset($_POST['sendForm']) && $this->errorFormPass ? '<span class="help-block m-b-none">' . $this->errorMsg . '</span>' : '') ?>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="naissance"><?= $this->ln->txt('admin-clients', 'form-client-lab-naissance', $this->language, 'Date de naissance') ?></label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-birthday-cake"></i></span>
                                                        <input type="text" class="form-control datePicker" name="naissance" id="naissance" value="<?=$_POST['naissance']?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="telephone"><?= $this->ln->txt('admin-clients', 'form-client-lab-telephone', $this->language, 'Téléphone fixe') ?></label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                        <input type="text" class="form-control" name="telephone" id="telephone" value="<?=$_POST['telephone']?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="mobile"><?= $this->ln->txt('admin-clients', 'form-client-lab-mobile', $this->language, 'Téléphone mobile') ?></label>
                                                <div class="col-sm-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-mobile-phone" style="font-size:18px;"></i></span>
                                                        <input type="text" class="form-control" name="mobile" id="mobile" value="<?=$_POST['mobile']?>" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="hr-line-dashed"></div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"><?= $this->ln->txt('admin-clients', 'form-client-lab-status', $this->language, 'Statut') ?></label>
                                                <div class="col-sm-6">
                                                    <div class="radio i-checks inline">
                                                        <label>
                                                            <input type="radio" value="2" name="status" <?= ($_POST['status'] === '2' ? ' checked' : '') ?> required>
                                                            <i></i> <?= $this->ln->txt('admin-clients', 'form-client-status-admin', $this->language, 'Administrateur') ?>
                                                        </label>
                                                    </div>
                                                    <div class="radio i-checks inline">
                                                        <label>
                                                            <input type="radio" value="1" name="status" <?= ($_POST['status'] === '1' || $this->new ? ' checked' : '') ?> required>
                                                            <i></i> <?= $this->ln->txt('admin-clients', 'form-client-status-client', $this->language, 'Client') ?>
                                                        </label>
                                                    </div>
                                                    <div class="radio i-checks inline">
                                                        <label>
                                                            <input type="radio" value="0" name="status" <?= ($_POST['status'] === '0' ? ' checked' : '') ?> required>
                                                            <i></i> <?= $this->ln->txt('admin-clients', 'form-client-status-off', $this->language, 'Hors ligne') ?>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php if($this->new){ ?>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="prevenir"><?= $this->ln->txt('admin-clients', 'form-client-mail', $this->language, 'Prévenir par mail') ?></label>
                                                <div class="col-sm-6">
                                                    <div class="radio i-checks inline">
                                                        <label>
                                                            <input type="checkbox" value="1" name="prevenir" id="prevenir" <?=($this->new?'checked':'')?>>
                                                            <i> <?= $this->ln->txt('admin-clients', 'form-client-mail-explication', $this->language, 'Le client initialisera lui même son mot de passe') ?></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/clients/gestion" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                            <?php $i++; ?>
                            <?php
                            if (!$this->new) {
                                ?>
                                <div id="tab-adresses" class="tab-pane <?=$this->params['part'] == $i ? 'active':''?>">
                                    <div class="panel-body">
                                        <div class="container-fluid">
                                            <fieldset class="form-horizontal">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <h4><?= $this->ln->txt('admin-clients', 'client-adresses-desc', $this->language, 'Adresses du client') ?></h4>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <a href="<?= $this->lurl . '/clients/formAdresse/' .$this->clients->id_client ?>" class="btn btn-primary floatRight">
                                                            <i class="fa fa-plus-square"></i>&nbsp;
                                                            <?= $this->ln->txt('admin-clients', 'add-address', $this->language, 'Ajouter une adresse') ?>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                
                                                <table id="addr_table" class="table table-striped table-hover vertical-center">
                                                    <thead>
                                                        <tr>
                                                            <th class="col-lg-3"><?= $this->ln->txt('admin-clients', 'list-addr-name', $this->language, 'Nom') ?></th>
                                                            <th class="col-lg-7"><?= $this->ln->txt('admin-clients', 'list-address-val', $this->language, 'Adresse') ?></th>
                                                            <th class="col-lg-2"><?= $this->ln->txt('admin-clients', 'list-addr-action', $this->language, 'Action') ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="addr_list" class="sortable">
                                                        <?php
                                                        if(!empty($this->clients->adresses) && $this->clients->adresses->count() > 0) {
                                                            foreach ($this->clients->adresses as $addr){
                                                                ?>
                                                                <tr>
                                                                    <td><?=$addr->nom_adresse?></td>
                                                                    <td><?=$addr->adresse1?> <?=$addr->cp?> <?=$addr->ville?></td>
                                                                    <td>
                                                                        <a href="<?=$this->lurl?>/clients/formAdresse/<?=$this->clients->id_client?>/<?=$addr->id_adresse?>" class="btn btn-white btn-sm btn-action">
                                                                            <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-clients', 'client-edit', $this->language, 'Modifier') ?>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        <tr id="empty_addr_list" <?=(!empty($this->clients->adresses) && $this->clients->adresses->count() > 0)?'style="display:none;"':''?>>
                                                            <td colspan="3">
                                                                <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                                                                <span class="font-bold" style="margin-left: 15px;">
                                                                    <?= $this->ln->txt('admin-clients', 'empty-client-addr', $this->language, 'Il n\'y a aucune adresse pour le moment') ?>
                                                                </span>
                                                            </td>          
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                
                                            </fieldset>
                                        </div>

                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <div class="col-sm-6 col-sm-offset-2">
                                                <a href="<?= $this->lurl ?>/clients/gestions" class="btn btn-white">
                                                    <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                                </a>
                                                <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                                <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            
                            <?php $i++; ?>
                            <div id="tab-commandes" class="tab-pane <?=$this->params['part'] == $i ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-clients', 'client-commandes-desc', $this->language, 'Commandes client') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            
                                            <table id="commandes_table" class="table table-striped table-hover vertical-center">
                                                <thead>
                                                    <tr>
                                                        <th class="col-lg-1"><?= $this->ln->txt('admin-clients', 'list-commande-id', $this->language, 'ID') ?></th>
                                                        <th class="col-lg-4"><?= $this->ln->txt('admin-clients', 'list-commande-date', $this->language, 'Date') ?></th>
                                                        <th class="col-lg-3"><?= $this->ln->txt('admin-clients', 'list-commande-montant', $this->language, 'Montant') ?></th>
                                                        <th class="col-lg-3"><?= $this->ln->txt('admin-clients', 'list-commande-etat', $this->language, 'État') ?></th>
                                                        <th class="col-lg-1"><?= $this->ln->txt('admin-clients', 'list-commande-action', $this->language, 'Action') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="commandes_list" class="sortable">
                                                    <?php
                                                    if(!empty($this->clients->transactions) && $this->clients->transactions->count() > 0) {
                                                        foreach ($this->clients->transactions as $cmd){
                                                            ?>
                                                            <tr>
                                                                <td><?=$cmd->id_transaction?></td>
                                                                <td><?=$cmd->date_transaction?></td>
                                                                <td><?= formatPrix($cmd->montant/100, 2, $this->clSettings->getParam('devise', 'sets'), false) ?></td>
                                                                <td><?=$this->etatCommande[$cmd->etat]?></td>
                                                                <td>
                                                                    <a href="<?=$this->lurl?>/dashboard/formCommande/cmd___<?=$cmd->id_transaction?>" class="btn btn-primary btn-sm btn-action">
                                                                        <i class="fa fa-eye"></i> <?= $this->ln->txt('admin-clients', 'transaction-details', $this->language, 'Détails') ?>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                    <tr id="empty_commande_list" <?=(!empty($this->clients->transactions) && $this->clients->transactions->count() > 0)?'style="display:none;"':''?>>
                                                        <td colspan="4">
                                                            <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                                                            <span class="font-bold" style="margin-left: 15px;">
                                                                <?= $this->ln->txt('admin-clients', 'empty-client-commandes', $this->language, 'Il n\'y a aucune commandes pour le moment') ?>
                                                            </span>
                                                        </td>          
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/clients/gestions" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; ?>
                            <div id="tab-droits" class="tab-pane <?=$this->params['part'] == $i ? 'active':''?>">
                                <div class="panel-body">
                                    <div class="container-fluid">
                                        <fieldset class="form-horizontal">
                                            <h4><?= $this->ln->txt('admin-clients', 'client-droits', $this->language, 'Droits d\'accès') ?></h4>
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-sm-6">
                                                <h4>Zones</h4>
                                                <div class="hr-line-dashed"></div>
                                            <?php foreach($this->geozones as $zone) {  ?>
                                                <div class="form-group">
                                                    <div class="col-sm-2 m-t-sm">
                                                        <div class="checkbox i-checks">
                                                            <input type="checkbox" name="zone<?=$zone->id_geozone?>" id="zone<?=$zone->id_geozone?>" value="1"<?= (in_array($zone->id_geozone,$this->clientZones) ? ' checked' : '') ?>>
                                                        </div>
                                                    </div>
                                                    <label class="text-left control-label" for="zone<?=$zone->id_geozone?>"><?=$zone->name?></label>
                                                </div>
                                            <?php } ?></div>

                                            <div class="col-sm-6">
                                                <h4>Profils</h4>
                                                <div class="hr-line-dashed"></div>
                                            
                                            <?php foreach($this->profiles as $profile) { ?>
                                                <div class="form-group">
                                                    <div class="col-sm-2 m-t-sm">
                                                        <div class="checkbox i-checks">
                                                            <input type="checkbox" name="profile<?=$profile->id_profile?>" id="profile<?=$profile->id_profile?>" value="1"<?= (in_array($profile->id_profile,$this->clientProfiles) ? ' checked' : '') ?>>
                                                        </div>
                                                    </div>
                                                    <label class="text-left control-label" for="profile<?=$profile->id_profile?>"><?=$profile->name?></label>
                                                </div>

                                            <?php } ?></div>
                                            

                                        </fieldset>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-2">
                                            <a href="<?= $this->lurl ?>/clients/gestions" class="btn btn-white">
                                                <?= $this->ln->txt('admin-generic', 'cancel', $this->language, 'Annuler') ?>
                                            </a>
                                            <button onClick="$('#restePage').val(1);$('#part').val(<?=$i?>);" class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save-and-stay', $this->language, 'Enregistrer et rester') ?></button>
                                            <button class="btn btn-primary" type="submit"><?= $this->ln->txt('admin-generic', 'save', $this->language, 'Enregistrer') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">    
    function initialiseJqueryPlugins() {
        
        $('.i-checks').iCheck({checkboxClass: 'icheckbox_square-green', radioClass: 'iradio_square-green'});
        
        // Suppression de la promotion /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.confirmAlert').on('click', function () {
            swal({
                title: "<?= $this->ln->txt('admin-generic', 'alert-confirm-required', $this->language, 'Confirmation requise') ?>",
                text: "<?= $this->ln->txt('admin-catalogue', 'delete-client-alert', $this->language, 'Confirmez vous la suppression du client ?') ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?= $this->ln->txt('admin-generic', 'alert-oui', $this->language, 'Oui') ?>",
                cancelButtonText: "<?= $this->ln->txt('admin-generic', 'alert-non', $this->language, 'Non') ?>",
                closeOnConfirm: false,
                closeOnCancel: true},
            function (isConfirm) {
                if (isConfirm) {
                    $(location).attr('href', '<?= $this->lurl ?>/clients/deleteClient/<?= $_POST['id_client'] ?>');
                    return false;
                }
            });
        });
        
        // Initialisation des datepickers /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $('.datePicker').datepicker({
            todayBtn: false,
            language: "<?= $this->language ?>",
            format: "<?= $this->clSettings->getParam('format-datepicker', 'sets') ?>",
            todayHighlight: false,
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    }
    
    // Réinitialisation des plugins JQ /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Nécéssaire au changmeent de tabs car certain element initialisé alors que non visible seront mal initialisés
    $(document).on('click', '.tabs-left a', function(){
        initialiseJqueryPlugins();
    });
    
    $(document).on('click', '#generate_password', function(){
        $(this).find('i').addClass('fa-spin');
        var _this = $(this);
        
        $.ajax('<?= $this->lurl ?>/clients/generatePassword/10',
        {
            success:function(data){
                $('#password_chromehack').val(data);
                setTimeout(function(){_this.find('i').removeClass('fa-spin');}, 620);
            }
        });
    });
    $(document).on('mousedown', '#see_password', function(){
        $('#password_chromehack').attr('type', 'text');
    }).on('mouseup blur drag', '#see_password', function(){
        $('#password_chromehack').attr('type', 'password');
    });
    
    $(document).on('click', '#copy_password', function(){
        var $temp = $("<input>");
        
        $("body").append($temp);
        $temp.val($('#password_chromehack').val()).select();
        
        document.execCommand("copy");
        
        $temp.remove();
        toastr["success"]('<?= $this->ln->txt('admin-generic', 'copied-to-clipboard', $this->language, 'Le mot de passe a bien été copié dans le presse-papier')?>');
    });
    $(document).ready(function () {
        initialiseJqueryPlugins();
    });
</script>