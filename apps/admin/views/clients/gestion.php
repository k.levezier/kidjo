<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $this->ln->txt('admin-clients', 'gestion-clients', $this->language, 'Gestion des clients') ?></h2>
        <ol class="breadcrumb">
            <li>
                <?= $this->ln->txt('admin-clients', 'clients', $this->language, 'Clients') ?>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
        <div class="title-action">
            <a href="<?= $this->lurl ?>/clients/export" class="btn btn-warning" target="_blank"  style="margin-bottom:3px"><?= $this->ln->txt('admin-clients', 'xportclients', $this->language, 'Exporter') ?></a>
            <a href="<?= $this->lurl ?>/clients/formClient" class="btn btn-primary"><?= $this->ln->txt('admin-clients', 'addclient', $this->language, 'Ajouter un client') ?></a>
        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $this->ln->txt('admin-clients', 'liste-clients', $this->language, 'Liste des clients') ?>&nbsp;<span class="badge badge-primary"><?=$this->clients->count()?></span></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form method="POST" style="height:80px">
                        <div class="col-lg-12" style="clear:both">
                            <div class="col-lg-6">
                                <input type="text" name="name" class="form-control" placeholder="Name" value="<?=$_SESSION['cltName']?>"/>
                            </div>

                            <div class="col-lg-2">
                                <input type="submit" class="btn btn-primary" value="Appliquer" name="filter"/>
                            </div>
                            <div class="col-lg-2">
                                <input type="submit" class="btn btn-warning" value="Reset" name="reset"/>
                            </div>
                        </div>
                    </form>
                    <?php
                    if ($this->clients->count() > 0) {
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover vertical-center">
                                <thead>
                                    <tr>
                                        <th class="col-lg-1"><?= $this->ln->txt('admin-clients', 'clients-id', $this->language, '#') ?></th>
                                        <th class="col-lg-1"><?= $this->ln->txt('admin-clients', 'clients-last-login', $this->language, 'Dernière connexion') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-clients', 'clients-nom', $this->language, 'Nom') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-clients', 'clients-prenom', $this->language, 'Prénom') ?></th>
                                        <th class="col-lg-3"><?= $this->ln->txt('admin-clients', 'clients-email', $this->language, 'Email') ?></th>
                                        <th class="col-lg-1" style="text-align: center;"><?= $this->ln->txt('admin-clients', 'clients-status', $this->language, 'Statut') ?></th>
                                        <th class="col-lg-2"><?= $this->ln->txt('admin-clients', 'clients-actions', $this->language, 'Actions') ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($this->clients as $c) {
                                        ?>
                                        <tr>
                                            <td>
                                                <?=$c->id_client?>
                                            </td>
                                            <td>
                                                <?=($c->lastlogin!="0000-00-00 00:00:00"?nl2br(date("d/m/y \n H:i", strtotime($c->lastlogin))):'')?>
                                            </td>
                                            <td>
                                                <?=$c->nom?>
                                            </td>
                                            <td>
                                                <?=$c->prenom?>
                                            </td>
                                            <td>
                                                <?=$c->email?>
                                            </td>
                                            <td style="text-align: center;font-size: larger;">
                                                <?php
                                                switch($c->status){
                                                    case 0:
                                                        echo '<i class="fa fa-circle text-danger" title="'.$this->ln->txt('admin-clients', 'form-client-status-off', $this->language, 'Hors ligne').'"></i>';
                                                        break;
                                                    case 1:
                                                        echo '<i class="fa fa-circle text-primary" title="'.$this->ln->txt('admin-clients', 'form-client-status-client', $this->language, 'Client').'"></i>';
                                                        break;
                                                    case 2:
                                                        echo '<i class="fa fa-circle text-warning" title="'.$this->ln->txt('admin-clients', 'form-client-status-admin', $this->language, 'Administrateur').'"></i>';
                                                        break;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?=$this->lurl?>/clients/formClient/<?=$c->id_client?>" class="btn btn-white btn-sm btn-action">
                                                    <i class="fa fa-pencil"></i> <?= $this->ln->txt('admin-clients', 'client-edit', $this->language, 'Modifier') ?>
                                                </a>
                                                <!-- <form method="post" action="<?=$this->lurl?>" style="display: inline-block" target="_blank">
                                                    <input type="hidden" name="email" value="<?=$c->email?>"/>
                                                    <input type="hidden" name="password" value="<?=$c->password?>"/>
                                                    <input type="submit" name="loginbo" value="Voir le site" class="btn btn-info btn-sm"/>
                                                </form> -->
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-warning btn-circle" type="button"><i class="fa fa-warning"></i></button>
                        <span class="font-bold" style="margin-left: 15px;"><?= $this->ln->txt('admin-clients', 'empty-client', $this->language, 'Il n\'y a aucun client pour le moment') ?></span>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>