<?php

use o\data;

class bootstrapCore extends common
{

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);

        // Version plateforme
        $versions = new o\versions();
        $this->version = $versions->getVersionActive();

        // Si un utilisateur est connecté
        if (!empty($_SESSION['user']['id_user'])) {
            $users = new o\users();

            // Récuperation de la liste des zones autorisées pour le user
            $this->lZonesHeader = $users->selectZonesHeader($_SESSION['user']['id_user']);

            // Recupération de la liste des sites autorisés pour le user
            $this->lSitesUser = $users->selectSitesUser($_SESSION['user']['id_user']);
        }

        // Datas
        $this->users = new o\users();
        $this->users->url = $this->lurl;

        // Recuperation de l'url du front
        $this->urlfront = (isset($_SESSION['infosSite']) ? $_SESSION['infosSite']['url'] : '');

        // CSS
        if (isset($_SESSION['infosSite']) && $this->current_controller != 'root') {
            $this->loadCss('bootstrap');
            $this->loadCss('font-awesome');
            $this->loadCss('toastr.min');
            $this->loadCss('animate');
            $this->loadCss('style');
            $this->loadCss('style-equinoa');
        } else {
            $this->loadCss('bootstrap');
            $this->loadCss('font-awesome');
            $this->loadCss('animate');
            $this->loadCss('style');
            $this->loadCss('style-login');
        }

        // JS
        if (isset($_SESSION['infosSite']) && $this->current_controller != 'root') {
            $this->loadJs('jquery-2.1.1');
            $this->loadJs('bootstrap');
            $this->loadJs('jquery.metisMenu');
            $this->loadJs('jquery.slimscroll.min');
            $this->loadJs('inspinia');
            $this->loadJs('pace.min');
            $this->loadJs('toastr.min');
            $this->loadJs('jquery-ui-sortable.min');
            $this->loadJs('jquery-2.1.1');
            $this->loadJs('ajax');
        } else {
            $this->loadJs('jquery-2.1.1');
            $this->loadJs('bootstrap');
            $this->loadJs('pace.min');
        }
    }

    /**
     * Fonction qui envoi les mails NMP et Server
     *
     * @function envoyerMail
     *
     * @param string $type_mail slug de l'email en bdd
     * @param string $id_langue langue de l'email
     * @param string $emailDest adresse mail du destinataire du mail
     * @param array $varMailcomp tableau de variables de mail complémentaire
     *
     */
    public function envoyerMail($type_mail, $id_langue, $emailDest, $varMailcomp = array()) {
        $this->nmp = new o\nmp(array('id_nmp'=>0));
        $this->nmp_desabo = new o\data('nmp_desabo');
        $this->tnmp = new tnmp(array($this->nmp, $this->nmp_desabo));


        $emailDestNMP = $emailDest;


        $emailDestServer = $this->clSettings->getParam('mails-server-alias', 'globals');

        // Recuperation du modele de mail
        $this->mails_text = new o\mails_text(array('type'=>$type_mail, 'id_langue'=>$id_langue));
        if(!$this->mails_text->exist()){
            return false;
        }

        // Variables du mailing
        $varMail = array(
            'url' => $this->lurl,
            'surl' => $this->surl,
            'iurl' => $this->surl,
            'sender' => $this->mails_text->exp_email,
        );

        $varMail = array_merge($varMail, $varMailcomp);

        // Construction du tableau avec les balises EMV
        $tabVars = $this->tnmp->constructionVariablesServeur($varMail);


        // Attribution des données aux variables
        $sujetMail = strtr(utf8_decode($this->mails_text->subject), $tabVars);
        $texteMail = strtr(utf8_decode($this->mails_text->content), $tabVars);
        $exp_name = strtr(utf8_decode($this->mails_text->exp_name), $tabVars);

        // Envoi du mail
        $this->email = new email(array());
        $this->email->setFrom($this->mails_text->exp_email, $exp_name);
        $this->email->addRecipient($emailDestNMP);
        $this->email->setSubject(stripslashes(utf8_decode($sujetMail)));
        $this->email->setHTMLBody(stripslashes(utf8_decode($texteMail)));

        $this->mails_filer = new o\mails_filer(array('id_filermails'=>0));
        Mailer::sendNMP($this->email, $this->mails_filer, $this->mails_text->id_textemail, $emailDestNMP, $tabFiler);

        // Injection du mail NMP dans la queue
        if ($this->Config['env'] != 'dev'){
            $this->tnmp->sendMailNMP($tabFiler, $varMail, $this->mails_text->nmp_secure, $this->mails_text->id_nmp, $this->mails_text->nmp_unique);
        }

        return true;
    }

}
