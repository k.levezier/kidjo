<?php

class ajaxController extends bootstrap
{

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->autoFireHead = false;
        $this->autoFireHeader = false;
        $this->autoFireFooter = false;
        $this->autoFireDebug = false;
    }


    public function _loadLanguageFolder()
    {
        if(isset($this->params[0]) && $this->params[0] != '')
        {
            $this->languages = new o\data('languages');
            // Chargement de la vue
            $this->setView('loadLanguageFolder');
        }
    }
}
