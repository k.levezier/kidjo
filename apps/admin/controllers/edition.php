<?php

use o\data;

class editionController extends bootstrap {

    public function __construct($command, $config, $app) {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'edition';
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique 
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     * 
     * @function _default
     */
    public function _default() {
        $this->users->checkAccess('edition');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('edition'));
    }
    
    /**
     * Gestion de la liste des templates.
     * Utilise le moteur Octeract.
     * 
     * @function _templates
     */
    public function _templates() {
        $this->users->checkAccess('edittemplates');
        $this->ssMenuActive = 'edittemplates';
        $this->clTemplates = new clTemplates();

        foreach ($this->clTemplates->lTypes as $type) {
            $this->{'lTemplates' . $type} = new o\data ('templates', array('type' => $type));
        }
    }
    
    /**
     * Gestion de la suppression d'un template. 
     * Utilise le moteur Octeract.
     * 
     * @function _deleteTpl
     */
    public function _deleteTpl() {
        $this->users->checkAccess('edittemplates');
        $this->autoFireNothing = true;
        
        if (isset($this->params[0]) && $this->params[0] != '') {
            $templates = new o\templates(array($this->params[0]));
            
            if($templates->exist()) {
                $backLog = serialize($templates->getArray());
                $templates->delete();
                
                if (file_exists($this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/controllers/templates/' . $templates->slug . '.php')) {
                    @unlink($this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/controllers/templates/' . $templates->slug . '.php');
                }

                if (file_exists($this->path . 'apps/default/controllers/templates/' . $templates->slug . '.php')) {
                    @unlink($this->path . 'apps/default/controllers/templates/' . $templates->slug . '.php');
                }

                if (file_exists($this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/views/templates/' . $templates->slug . '.php')) {
                    @unlink($this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/views/templates/' . $templates->slug . '.php');
                }

                if (file_exists($this->path . 'apps/default/views/templates/' . $templates->slug . '.php')) {
                    @unlink($this->path . 'apps/default/views/templates/' . $templates->slug . '.php');
                }
                
                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-tpldel', $this->language, 'Supression d\'un template'), $templates->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'templates', $this->language, 'Templates'), $this->ln->txt('admin-banner', 'tpl-del', $this->language, 'Le template a bien été supprimé'));
                $this->redirect($this->lurl . '/edition/templates');
            } else {
                $this->redirect($this->lurl . '/edition/templates');
            }
        } else {
            $this->redirect($this->lurl . '/edition/templates');
        }
    }
    
    /**
     * Gestion du formulaire d'ajout / édition d'un template. 
     * Utilise le moteur Octeract.
     * 
     * @function _formTpl
     */
    public function _formTpl() {
        $this->users->checkAccess('edittemplates');
        $this->ssMenuActive = 'edittemplates';
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->clTemplates = new clTemplates();
        $this->templates = new o\templates($this->params[0]);
        $this->new = ($this->templates->exist() ? false : true);
        
        if (isset($_POST['sendForm']) && $this->params[0] == $_POST['id_template']) {
            $_POST['name'] = trim(htmlspecialchars($_POST['name']));
            $_POST['type'] = trim(htmlspecialchars($_POST['type']));
            $this->errorFormNom = false;
            $this->errorFormType = false;
            $this->error = false;
            $this->errorMsg = '';
            
            if (strlen($_POST['name']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['type']) == 0) {
                $this->errorFormType = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }
            
            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->templates->getArray()) : '');
                $this->templates->name = $_POST['name'];
                $this->templates->slug = (!$this->new ? $this->templates->slug : generateSlug($_POST['name']));
                $this->templates->type = $_POST['type'];
                $this->templates->class_template = $_POST['class_template'];
                $this->templates->status = (isset($_POST['status']) ? $_POST['status'] : 0);
                $this->templates->affichage = (isset($_POST['affichage']) ? $_POST['affichage'] : 0);
                $this->templates->is_gamme = (isset($_POST['is_gamme']) ? $_POST['is_gamme'] : 0);
                $this->templates->save();
                
                if ($this->templates->affichage == 1) {
                    if (!file_exists($this->path . 'apps/default/controllers/templates/' . $this->templates->slug . '.php')) {
                        $fp = fopen($this->path . 'apps/default/controllers/templates/' . $this->templates->slug . '.php', 'wb');
                        fputs($fp, "<?php \r\n// " . $this->templates->name . " Controller");
                        fclose($fp);
                        chmod($this->path . 'apps/default/controllers/templates/' . $this->templates->slug . '.php', 0777);
                    }
                    
                    if (!file_exists($this->path . 'apps/default/views/templates/' . $this->templates->slug . '.php')) {
                        $fp = fopen($this->path . 'apps/default/views/templates/' . $this->templates->slug . '.php', 'wb');
                        fputs($fp, "<!-- " . $this->templates->name . " View -->");
                        fclose($fp);
                        chmod($this->path . 'apps/default/views/templates/' . $this->templates->slug . '.php', 0777);
                    }
                }
                
                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-edittpl', $this->language, 'Modification d\'un template') : $this->ln->txt('admin-logs', 'action-addtpl', $this->language, 'Ajout d\'un template')), $this->templates->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'templates', $this->language, 'Templates'), (!$this->new ? $this->ln->txt('admin-banner', 'tpl-edit', $this->language, 'Le template a bien été modifié') : $this->ln->txt('admin-banner', 'tpl-add', $this->language, 'Le template a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/edition/formTpl/' . $this->templates->id_template);
                } else {
                    $this->redirect($this->lurl . '/edition/templates');
                }
            }
        } else {
            $_POST = (!$this->new ? $this->templates->getArray() : array());
        }
    }
    
    /**
     * Gestion du contenu des fichiers d'un template. 
     * Utilise le moteur Octeract.
     * 
     * @function _codeTpl
     */
    public function _codeTpl() {
        $this->users->checkAccess('edittemplates');
        $this->ssMenuActive = 'edittemplates';
        $this->loadJs('codemirror/codemirror');
        $this->loadJs('codemirror/mode/htmlmixed');
        $this->loadJs('codemirror/mode/xml');
        $this->loadJs('codemirror/mode/javascript');
        $this->loadJs('codemirror/mode/css');
        $this->loadJs('codemirror/mode/clike');
        $this->loadJs('codemirror/mode/php');
        $this->loadCss('codemirror/codemirror');
        $urlController = '';
        $this->codeController = '';
        $urlView = '';
        $this->codeView = '';
        $templates = new o\templates($this->params['tpl']);
        
        if ($templates->exist()) {
            if (file_exists($this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/controllers/templates/' . $templates->slug . '.php')) {
                $urlController = $this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/controllers/templates/' . $templates->slug . '.php';
            } elseif (file_exists($this->path . 'apps/default/controllers/templates/' . $templates->slug . '.php')) {
                $urlController = $this->path . 'apps/default/controllers/templates/' . $templates->slug . '.php';
            }
            
            if ($urlController != '') {
                $this->codeController = implode('', file($urlController));
                $this->codeController = htmlentities($this->codeController);
            }

            if (file_exists($this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/views/templates/' . $templates->slug . '.php')) {
                $urlView = $this->path . 'override/apps/' . $_SESSION['infosSite']['app'] . '/views/templates/' . $templates->slug . '.php';
            } elseif (file_exists($this->path . 'apps/default/views/templates/' . $templates->slug . '.php')) {
                $urlView = $this->path . 'apps/default/views/templates/' . $templates->slug . '.php';
            }
            
            if ($urlView != '') {
                $this->codeView = implode('', file($urlView));
                $this->codeView = htmlentities($this->codeView);
            }

            if ($urlController == '' && $urlView == '') {
                $this->redirect($this->lurl . '/edition/templates');
            }

            if (isset($_POST['sendFormController'])) {
                if (is_writeable($urlController)) {
                    $fp = fopen($urlController, 'wb');
                    fputs($fp, $_POST['controller']);
                    fclose($fp);
                    
                    $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-tpl-codecont', $this->language, 'Modification du code source du controller de template'), $templates->name, $this->codeController);
                    $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'templates', $this->language, 'Templates'), $this->ln->txt('admin-banner', 'tpl-code', $this->language, 'Le code source a bien été modifié'));

                    if ($_POST['restePageController'] == 1) {
                        $this->redirect($this->lurl . '/edition/codeTpl/tpl___' . $templates->id_template);
                    } else {
                        $this->redirect($this->lurl . '/edition/templates');
                    }
                }
            }

            if (isset($_POST['sendFormView'])) {
                if (is_writeable($urlView)) {
                    $fp = fopen($urlView, "wb");
                    fputs($fp, $_POST['view']);
                    fclose($fp);
                    
                    $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-tpl-codeview', $this->language, 'Modification du code source de la vue du template'), $templates->name, $this->codeView);
                    $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'templates', $this->language, 'Templates'), $this->ln->txt('admin-banner', 'tpl-code', $this->language, 'Le code source a bien été modifié'));

                    if ($_POST['restePageView'] == 1) {
                        $this->redirect($this->lurl . '/edition/codeTpl/tpl___' . $templates->id_template);
                    } else {
                        $this->redirect($this->lurl . '/edition/templates');
                    }
                }
            }
        } else {
            $this->redirect($this->lurl . '/edition/templates');
        }
    }
    
    /**
     * Gestion de la liste des éléments d'un template.
     * Utilise le moteur Octeract.
     * 
     * @function _elementsTpl
     */
    public function _elementsTpl() {
        $this->users->checkAccess('edittemplates');
        $this->ssMenuActive = 'edittemplates';
        $this->loadJs('jquery.nestable');
        $this->templates = new o\templates($this->params['tpl']);
        
        if($this->templates->exist()) {
            $this->elements_groupes = new o\data('elements_groupes', array('id_template' => $this->templates->id_template));
            foreach($this->elements_groupes as $eg) {
                $this->{'lElements' . $eg->id_groupe} = new o\data('elements', array('id_template' => $this->templates->id_template, 'id_groupe' => $eg->id_groupe));
            }
            $this->orphan_elements = new o\data('elements', array('id_template' => $this->templates->id_template, 'id_groupe' => NULL));
        } else {
            $this->redirect($this->lurl . '/edition/templates');
        }
    }
    
    /**
     * Gestion de l'ordre des éléments dans les groupes.
     * Utilise le moteur Octeract.
     * 
     * @function _orderElmt
     */
    public function _orderElmt() {
        $this->users->checkAccess('edittemplates');
        $this->autoFireNothing = true;
        
        if (isset($_POST['orderElmt']) && $_POST['orderElmt'] != '') {
            $lElements = json_decode($_POST['orderElmt'], true);
            
            foreach ($lElements as $key => $val) {
                $elements = new o\elements($val['id']);
                $elements->ordre = ($key + 1);
                $elements->update();
            }

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-orderelmt', $this->language, 'Ordonnancement des éléments'), serialize($lElements));
        }
    }
    
    /**
     * Gestion du formulaire d'ajout / édition d'un élément. 
     * Utilise le moteur Octeract.
     * 
     * @function _formElmt
     */
    public function _formElmt() {
        $this->templates = new o\templates($this->params['tpl']);
        $this->blocs = new o\blocs($this->params['bloc']);
        
        if($this->templates->exist())
        {
            $this->users->checkAccess('edittemplates');
            $this->ssMenuActive = 'edittemplates';
        }
        else
        {
            $this->users->checkAccess('editblocs');
            $this->ssMenuActive = 'editblocs';
        }
        
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->clTemplates = new clTemplates($this->surl, $this->ln, $this->language);
        
        if($this->templates->exist() || $this->blocs->exist()) {
            $type = $this->templates->exist()?'template':'bloc';  
            $id = $this->templates->exist()?$this->params['tpl']:$this->params['bloc'];  
            
            $this->currentID = $this->templates->exist() ? $this->templates->id_template : $this->blocs->id_bloc;
            $this->currentType = $this->templates->exist() ? 'template' : 'bloc';
            $this->typeShort1 = $this->templates->exist() ? 'elementsTpl' : 'elementsBloc';
            $this->typeShort2 = $this->templates->exist() ? 'tpl' : 'bloc';
            $this->typeShort3 = $this->templates->exist() ? 'Tpl' : 'Bloc';
            
            $this->elements = new o\elements($this->params['elmt']);
            $this->new = ($this->elements->exist() ? false : true);
            
            $params = $this->templates->exist() ? array('id_template' => $this->templates->id_template) : array('id_bloc' => $this->blocs->id_bloc);
            $this->elements_groupes = new o\data('elements_groupes', $params);
            
            if (isset($_POST['sendForm']) && $this->params['elmt'] == $_POST['id_element']) {
                $_POST['name'] = trim(htmlspecialchars($_POST['name']));
                $_POST['type_element'] = trim(htmlspecialchars($_POST['type_element']));
                $this->errorFormName = false;
                $this->errorFormType = false;
                $this->error = false;
                $this->errorMsg = '';

                if (strlen($_POST['type_element']) == 0) {
                    $this->errorFormType = true;
                    $this->error = true;
                    $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
                } elseif (strlen($_POST['name']) == 0) {
                    $this->errorFormName = true;
                    $this->error = true;
                    $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
                }
                
                if (!$this->error) {
                    $backLog = (!$this->new ? serialize($this->elements->getArray()) : '');
                    $this->elements->id_template = $_POST['id_template']!=''?$_POST['id_template']:null;
                    $this->elements->id_bloc = $_POST['id_bloc']!=''?$_POST['id_bloc']:null;
                    $this->elements->id_groupe = ($_POST['id_groupe'] == 0 ? NULL : $_POST['id_groupe']);
                    $this->elements->name = $_POST['name'];
                    $this->elements->slug = (!$this->new ? 
                                                $this->elements->slug : 
                                                $this->clFonctions->controlSlug('elements', generateSlug($_POST['name']), array('NOT id_'.$type => $id),false));     
                    
                    $this->elements->ordre = $this->elements->getMaxOrdre($_POST, $this->elements->id_groupe, $type);
                    $this->elements->type_element = $_POST['type_element'];
                    if(in_array($_POST['type_element'], array('Radio Button', 'Select'))){
                        $cpl = array();
                        foreach($_POST['option_name'] as $index => $name){
                            $cpl[$_POST['option_value'][$index]] = $name;
                        }
                        $this->elements->complement = serialize($cpl);
                    }
                    $this->elements->status = (isset($_POST['status']) ? $_POST['status'] : 0);
                    $this->elements->save();
                    
                    $this->clFonctions->updateSlug($this->elements, $this->elements->id_element);

                    $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editelmt', $this->language, 'Modification d\'un élément') : $this->ln->txt('admin-logs', 'action-addelmt', $this->language, 'Ajout d\'un élément')), $this->elements->name, $backLog);
                    $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'elements', $this->language, 'Eléments'), (!$this->new ? $this->ln->txt('admin-banner', 'elmt-edit', $this->language, 'L\'élément a bien été modifié') : $this->ln->txt('admin-banner', 'elmt-add', $this->language, 'L\'élément a bien été ajouté')));

                    if ($_POST['restePage'] == 1) {
                        $url = $this->templates->exist()?'tpl___'.$this->templates->id_template:'bloc___'.$this->blocs->id_bloc;
                        //var_dump($this->lurl . '/edition/formElmt/' . $url . '/elmt___' . $this->elements->id_element);die;
                        $this->redirect($this->lurl . '/edition/formElmt/' . $url . '/elmt___' . $this->elements->id_element);
                    } else {
                        $url = $this->templates->exist()?'elementsTpl/tpl___'.$this->templates->id_template:'elementsBloc/bloc___'.$this->blocs->id_bloc;
                        $this->redirect($this->lurl . '/edition/' . $url);
                    }
                }
            } else {
                $_POST = (!$this->new ? $this->elements->getArray() : array());                
            }
        } else {
            $this->redirect($this->lurl . '/edition/site');
        }
    }
    
    /**
     * Gestion de la suppression d'un élément. 
     * Utilise le moteur Octeract.
     * 
     * @function _deleteElmt
     */
    public function _deleteElmt() {
        $this->users->checkAccess('edittemplates');
        $this->autoFireNothing = true;
        $templates = new o\templates($this->params['tpl']);
        $blocs = new o\blocs($this->params['bloc']);
        
        if($templates->exist() || $blocs->exist()) {
            $elements = new o\elements($this->params['elmt']);
            $url = $templates->exist()?'elementsTpl/tpl___'.$templates->id_template:'elementsBloc/bloc___'.$blocs->id_bloc;
            /*<a href="<?= $this->lurl ?>/edition/<?=$url?>" class="btn btn-white">*/
            
            if($elements->exist()) {
                $backLog = serialize($elements->getArray());
                $elements->delete();
                
                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-elmtdel', $this->language, 'Supression d\'un élément'), $elements->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'elements', $this->language, 'Eléments'), $this->ln->txt('admin-banner', 'elmt-del', $this->language, 'L\'élément a bien été supprimé'));
                $this->redirect($this->lurl . '/edition/' . $url);
            } else {
                $this->redirect($this->lurl . '/edition/' . $url);
            }            
        } else {
            $this->redirect($this->lurl . '/edition/site');
        }
    }
	
    /**
     * Gestion de la liste des groupes d'éléments d'un template.
     * Utilise le moteur Octeract.
     * 
     * @function _gpeElmt
     */
    public function _gpeElmt() {
        $this->users->checkAccess('edittemplates');
        $this->ssMenuActive = 'edittemplates';
        $this->loadJs('jquery.nestable');
        $this->templates = new o\templates($this->params['tpl']);
        $this->blocs = new o\blocs($this->params['bloc']);

        if($this->templates->exist() || $this->blocs->exist()) {
            
            $this->currentID = $this->templates->exist() ? $this->templates->id_template : $this->blocs->id_bloc;
            $this->currentType = $this->templates->exist() ? 'template' : 'bloc';
            $this->typeShort1 = $this->templates->exist() ? 'elementsTpl' : 'elementsBloc';
            $this->typeShort2 = $this->templates->exist() ? 'tpl' : 'bloc';
            $this->typeShort3 = $this->templates->exist() ? 'Tpl' : 'Bloc';
            
            $params = $this->templates->exist() ? array('id_template' => $this->templates->id_template) : array('id_bloc' => $this->blocs->id_bloc);
            $this->elements_groupes = new o\data('elements_groupes', $params);
        } else {
            $this->redirect($this->lurl . '/edition/site');
        }
    }
    
    /**
     * Gestion de l'ordre des groupes dans le template.
     * Utilise le moteur Octeract.
     * 
     * @function _orderGpe
     */
    public function _orderGpe() {
        $this->users->checkAccess('edittemplates');
        $this->autoFireNothing = true;

        if (isset($_POST['orderGpe']) && $_POST['orderGpe'] != '') {
            $lGroupes = json_decode($_POST['orderGpe'], true);

            foreach ($lGroupes as $key => $val) {
                $elements_groupes = new o\elements_groupes($val['id']);
                $elements_groupes->ordre = ($key + 1);
                $elements_groupes->update();
            }

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-ordergpes', $this->language, 'Ordonnancement des groupes'), serialize($lGroupes));
        }
    }
    
    /**
     * Gestion de la suppression d'un groupe. 
     * Utilise le moteur Octeract.
     * 
     * @function _deleteGpe
     */
    public function _deleteGpe() {
        $this->users->checkAccess('edittemplates');
        $this->autoFireNothing = true;
        $templates = new o\templates($this->params['tpl']);
        $blocs = new o\blocs($this->params['bloc']);
        
        if($templates->exist() || $blocs->exist()) {
            $this->currentID = $this->templates->exist() ? $this->templates->id_template : $this->blocs->id_bloc;
            $this->currentType = $this->templates->exist() ? 'template' : 'bloc';
            $this->typeShort1 = $this->templates->exist() ? 'elementsTpl' : 'elementsBloc';
            $this->typeShort2 = $this->templates->exist() ? 'tpl' : 'bloc';
            $this->typeShort3 = $this->templates->exist() ? 'Tpl' : 'Bloc';
            
            $elements_groupes = new o\elements_groupes($this->params['gpe']);
            if($elements_groupes->exist()) {
                $backLog = serialize($elements_groupes->getArray());
                $elements_groupes->delete();
                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-gpedel', $this->language, 'Supression d\'un groupe'), $elements_groupes->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'elmt-gpe', $this->language, 'Groupe d\'éléments'), $this->ln->txt('admin-banner', 'gpe-del', $this->language, 'Le groupe a bien été supprimé'));
                $this->redirect($this->lurl . '/edition/gpeElmt/'.$this->typeShort2.'___' . $this->currentID);                
            } else {
                $this->redirect($this->lurl . '/edition/elements'.$this->typeShort3.'/'.$this->typeShort2.'___' . $this->currentID);
            }        
        } else {
            $this->redirect($this->lurl . '/edition/');
        }
    }
    
    /**
     * Gestion du formulaire d'ajout / édition d'un groupe. 
     * Utilise le moteur Octeract.
     * 
     * @function _formGpeElmt
     */
    public function _formGpeElmt() {
        $this->users->checkAccess('edittemplates');
        $this->ssMenuActive = 'edittemplates';
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->templates = new o\templates($this->params['tpl']);
        $this->blocs = new o\blocs($this->params['bloc']);
        
        if($this->templates->exist() || $this->blocs->exist()) {
            
            $this->currentID = $this->templates->exist() ? $this->templates->id_template : $this->blocs->id_bloc;
            $this->currentType = $this->templates->exist() ? 'template' : 'bloc';
            $this->typeShort1 = $this->templates->exist() ? 'elementsTpl' : 'elementsBloc';
            $this->typeShort2 = $this->templates->exist() ? 'tpl' : 'bloc';
            $this->typeShort3 = $this->templates->exist() ? 'Tpl' : 'Bloc';
            $params = $this->templates->exist() ? array('id_template' => $this->templates->id_template) : array('id_bloc' => $this->blocs->id_bloc);
            
            $this->elements_groupes = new o\elements_groupes($this->params['gpe']);
            $this->new = ($this->elements_groupes->exist() ? false : true);
            
            if (isset($_POST['sendForm']) && $this->params['gpe'] == $_POST['id_groupe']) {
                
                
                $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
                $this->errorFormNom = false;
                $this->error = false;
                $this->errorMsg = '';
                
                if (strlen($_POST['nom']) == 0) {
                    $this->errorFormNom = true;
                    $this->error = true;
                    $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
                }
                
                if (!$this->error) {
                    $backLog = (!$this->new ? serialize($this->elements_groupes->getArray()) : '');
                    $this->elements_groupes->id_template = $_POST['id_template']==''?null:$_POST['id_template'];
                    $this->elements_groupes->id_bloc = $_POST['id_bloc']==''?null:$_POST['id_bloc'];
                    $this->elements_groupes->nom = $_POST['nom'];
                    $this->elements_groupes->slug = (!$this->new ? $this->elements_groupes->slug : generateSlug($_POST['nom']));
                    $this->elements_groupes->ordre = $this->elements_groupes->getMaxOrdre($params);
                    $this->elements_groupes->status = (isset($_POST['status']) ? $_POST['status'] : 0);
                    $this->elements_groupes->save();
                    
                    $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editgpe', $this->language, 'Modification d\'un groupe') : $this->ln->txt('admin-logs', 'action-addgpe', $this->language, 'Ajout d\'un groupe')), $this->elements_groupes->nom, $backLog);
                    $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'elmt-gpe', $this->language, 'Groupe d\'éléments'), (!$this->new ? $this->ln->txt('admin-banner', 'gpe-edit', $this->language, 'Le groupe a bien été modifié') : $this->ln->txt('admin-banner', 'gpe-add', $this->language, 'Le groupe a bien été ajouté')));

                    if ($_POST['restePage'] == 1) {
                        $this->redirect($this->lurl . '/edition/formGpeElmt/'.$this->typeShort2.'___' . $this->currentID . '/gpe___' . $this->elements_groupes->id_groupe);
                    } else {
                        $this->redirect($this->lurl . '/edition/gpeElmt/'.$this->typeShort2.'___' . $this->currentID);
                    }
                }
            } else {
                $_POST = (!$this->new ? $this->elements_groupes->getArray() : array());                
            }
        } else {
            $this->redirect($this->lurl . '/edition/');
        }
    }
    
    /**
     * Gestion de la liste des redirections.
     * Utilise le moteur Octeract.
     * 
     * @function _redirections
     */
    public function _redirections() {
        $this->users->checkAccess('editredirections');
        $this->ssMenuActive = 'editredirections';
        $this->redirections = new o\data('redirections', array('id_langue' => $this->language));
        
        if(isset($_POST['filtre']) && $_POST['filtre'] != '') {
            $this->redirections->addWhere('`from_slug` LIKE "%' . $_POST['filtre'] . '%" OR `to_slug` LIKE "%' . $_POST['filtre'] . '%"');
        }
    }
    
    /**
     * Gestion du formulaire d'ajout / édition d'une redirection. 
     * Utilise le moteur Octeract.
     * 
     * @function _formRedir
     */
    public function _formRedir() {
        $this->users->checkAccess('editredirections');
        $this->ssMenuActive = 'editredirections';
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->redirections = new o\redirections($this->params[0]);
        $this->new = ($this->redirections->exist() ? false : true);
        
        if (isset($_POST['sendForm']) && $this->params[0] == $_POST['id_redirection']) {
            $_POST['type'] = trim(htmlspecialchars($_POST['type']));
            $_POST['from_slug'] = trim(htmlspecialchars($_POST['from_slug']));
            $_POST['to_slug'] = trim(htmlspecialchars($_POST['to_slug']));
            $this->errorFormType = false;
            $this->errorFormOrigin = false;
            $this->errorFormDest = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['type']) == 0) {
                $this->errorFormType = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['from_slug']) == 0) {
                $this->errorFormOrigin = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['to_slug']) == 0) {
                $this->errorFormDest = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->redirections->getArray()) : '');
                $this->redirections->id_langue = $_POST['id_langue'];
                $this->redirections->from_slug = $_POST['from_slug'];
                $this->redirections->to_slug = $_POST['to_slug'];
                $this->redirections->type = $_POST['type'];
                $this->redirections->status = (isset($_POST['status']) ? $_POST['status'] : 0);
                $this->redirections->save();
                
                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editredir', $this->language, 'Modification d\'une redirection') : $this->ln->txt('admin-logs', 'action-addredir', $this->language, 'Ajout d\'une redirection')), $this->redirections->from_slug. '/' . $this->redirections->to_slug, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'redirections', $this->language, 'Redirections'), (!$this->new ? $this->ln->txt('admin-banner', 'redir-edit', $this->language, 'La redirection a bien été modifiée') : $this->ln->txt('admin-banner', 'redir-add', $this->language, 'La redirection a bien été ajoutée')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/edition/formRedir/' . $this->redirections->id_redirection);
                } else {
                    $this->redirect($this->lurl . '/edition/redirections');
                }
            }
        } else {
            $_POST = (!$this->new ? $this->redirections->getArray() : array());
        }
    }
    
    /**
     * Gestion de la suppression d'une redirection. 
     * Utilise le moteur Octeract.
     * 
     * @function _deleteRedir
     */
    public function _deleteRedir() {
        $this->users->checkAccess('editredirections');
        $this->autoFireNothing = true;
        $redirections = new o\redirections($this->params[0]);
        
        if($redirections->exist()) {
            $backLog = serialize($redirections->getArray());
            $redirections->delete();
            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-redirdel', $this->language, 'Supression d\'une redirection'), $redirections->from_slug . '/' . $redirections->to_slug, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'redirections', $this->language, 'Redirections'), $this->ln->txt('admin-banner', 'redir-del', $this->language, 'La redirection a bien été supprimée'));
            $this->redirect($this->lurl . '/edition/redirections');
        } else {
            $this->redirect($this->lurl . '/edition/redirections');
        }
    }
    
    /**
     * Gestion de l'arborescence du site
     * et de l'enregistrement de l'édition des pages a la volée
     * Utilise le moteur Octeract.
     * 
     * @function _site
     */
    public function _site() {
        $this->users->checkAccess('editsite');
        $this->ssMenuActive = 'editsite';
        
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadJs('jsTree/jstree.min');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->loadCss('jsTree/style');
        
        if(isset($_POST['sendFormQuick'])){
            
            $tree = new o\tree(array('id_tree' => $_POST['id_tree'], 'id_langue' => $_POST['id_langue']));
            if ($tree->exist()) {
                $tree->title = $_POST['title'];
                $tree->menu_title = $_POST['menu_title'];
                //$tree->slug = $_POST['slug'];

                if($tree->id_tree == 1){
                    $tree->slug = '';
                }else{
                    $baseSlug = (!empty($_POST['slug']) ? $_POST['slug'] : $_POST['title']);
                    $tree->slug = $this->clFonctions->controlSlug('tree', generateSlug($baseSlug), array('id_tree' => $tree->id_tree, 'id_langue' => $_POST['id_langue']),false);
                    $this->clFonctions->updateSlug($tree, $this->params['page'].'-'.$_POST['id_langue']);
                }

                $tree->status = $_POST['status'];
                $tree->update();
            }
        }
        
        $this->clTemplates = new clTemplates();
        $tree = new o\tree();
        //$lPages = $tree->getArboSite($this->language, 0, 0);
        $lPages = $tree->getArboSite($this->language, null, 0);
        $this->arbo = $this->clTemplates->getArboSite($lPages);
    }
    
    /**
     * Gestion du déplacement d'une page dans l'arbo.
     * Utilise le moteur Octeract.
     * 
     * @function _movePage
     */
    public function _movePage() {
        $this->users->checkAccess('editsite');
        $this->autoFireNothing = true;  
        
        if(isset($this->params['page']) && $this->params['page'] != '' && isset($this->params['direction']) && $this->params['direction'] != '') {
            $tree = new o\tree(array('id_tree' => $this->params['page'], 'id_langue' => $this->language));
            $backLog = $tree->getArray();
            
            if($tree->movePage($this->params['direction'])) {
                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-movepage', $this->language, 'Déplacement d\'une page'), $this->params['direction'] . ' : ' . $tree->title, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-pages', $this->language, 'Gestion des pages'), $this->ln->txt('admin-banner', 'action-movepage', $this->language, 'La page a bien été déplacée'));
            } else {
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-pages', $this->language, 'Gestion des pages'), $this->ln->txt('admin-banner', 'erreur-movepage', $this->language, 'La page n\'a pas été déplacée'));
            }
            $this->redirect($this->lurl . '/edition/site');
        } else {
            $this->redirect($this->lurl . '/edition/site');
        }
    }
    
    /**
     * Gestion de l'édition d'une page à la volée
     * Utilise le moteur Octeract.
     * 
     * @function _siteInfos
     */
    public function _siteInfos() {
        $this->users->checkAccess('editsite');
        $this->autoFireHead = false;
        $this->autoFireHeader = false;
        $this->autoFireFooter = false;
        $this->autoFireDebug = false;
        
        $tree = new o\tree(array('id_tree' => $_POST['id_tree'], 'id_langue' => $this->language));
        $_POST = $tree->getArray();
    }
    
    /**
     * Gestion de l'édition/ajout d'une page
     * Utilise le moteur Octeract.
     * 
     * @function _formPage
     */
    public function _formPage() {
        $this->users->checkAccess('editsite');
        $this->ssMenuActive = 'editsite';
        
        $this->loadJs('icheck.min');
        $this->loadCss('icheck');
        
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        
        $this->loadJs('chosen.jquery');
        $this->loadCss('chosen');
        
        $this->loadJs('bootstrap-datepicker');
        $this->loadCss('datepicker');
        
        $this->loadJs('../../editeur/ckeditor/ckeditor');
        
        $this->clTemplates = new clTemplates($this->surl, $this->ln, $this->language);

        $this->new = false;
        $this->duplicate = false;
        $this->templates = new o\templates();
        
        if(isset($this->params['duplicate'])){
            $tree_origin = new o\data('tree', array('id_tree'=>$this->params['duplicate']));
            $this->duplicate = true;
            $this->id_tree_origin = $this->params['duplicate'];
            $this->id_template_origin = $tree_origin->current()->id_template;
        }
        
        if (!isset($this->params['page']) || ($tree_origin != NULL && count($tree_origin) > 0)) {
            $tree = new o\data('tree');
            $nextIdTree = max(array_merge(array(0), $tree->id_tree->getArray())) + 1;
            $nextOrdre = max(array_merge(array(0), $tree->ordre->getArray())) + 1;
            $this->tree = new o\tree();
            $this->tree->id_tree=$nextIdTree;
            $this->tree->id_langue=$this->language;
            $this->tree->ordre=$nextOrdre;
            $this->tree->id_template=$this->id_template_origin;
            $this->tree->prive = 1;
            $this->tree->status = 1;
            $this->tree->status_menu = 1;
            $this->tree->insert();
            
            $this->new = true;
            $this->params['page'] = $this->tree->id_tree;
        } else {
            $this->tree = new o\tree(array('id_tree' => $this->params['page'], 'id_langue' => $this->language));
            if ($this->tree->exist()) {
                
            } else {
                $this->redirect($this->lurl . '/edition/site/');
            }
        }
        $this->geozones = new o\data('geozones');
        $this->profiles = new o\data('profiles');
        //$this->geozones->order('name','ASC','');
        //$this->profiles->order('name','ASC','');
        //$this->lPages = $this->tree->getArboSite($this->language, 0, 0, 0);
        $this->lPages = $this->tree->getArboSite($this->language, null, 0, 0);
        $this->lTemplates = $this->templates->getListeTemplates();
        
        if (isset($_POST['sendForm']) && ($this->params['page']==$_POST['id_tree'] || isset($this->params['duplicate']))) {
            $this->error = false;
            $this->errorMsg = '';
            $this->tree_geozones = new o\data('tree_geozones', array('id_tree'=>$_POST['id_tree']));
            $this->tree_profiles = new o\data('tree_profiles', array('id_tree'=>$_POST['id_tree']));
            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->tree->getArray()) : '');
                
                foreach ($this->lLangues as $key => $langue) {
                    /////////////////////////////////////////////////////////////////////////////////////
                    /////////////////////////           Elt du tree           ///////////////////////////
                    /////////////////////////////////////////////////////////////////////////////////////
                    $tree = new o\tree(array('id_tree' => $this->params['page'], 'id_langue' => $key));
                    
                    $tree->id_tree = $this->params['page'];
                    $tree->id_langue = $key;
                    
                    $tree->id_parent = $_POST['id_parent'] == '0'?NULL:$_POST['id_parent'];
                    $tree->id_canonical = $_POST['id_canonical'];
                    $tree->id_template = $_POST['id_template']==""?NULL:$_POST['id_template'];
                    
                    $tree->title = $_POST['value_title_'.$key];
                    $tree->menu_title = $_POST['value_menu_title_'.$key];
                    
                    if($tree->id_tree == 1){
                        $tree->slug = '';
                    }else{
                        $baseSlug = (!empty($_POST['value_slug_'.$key]) ? $_POST['value_slug_'.$key] : $_POST['value_title_'.$key]);
                        $tree->slug = (!($this->new || $this->duplicate) ? 
                                          $this->clFonctions->controlSlug('tree', generateSlug($baseSlug), array('id_tree' => $this->params['page'], 'id_langue' => $key),false) :
                                          $this->clFonctions->controlSlug('tree', generateSlug($baseSlug)));
                        $this->clFonctions->updateSlug($tree, $this->params['page'].'-'.$key);
                    }
                    
                    $tree->status = $_POST['value_status_'.$key];
                    $tree->status_menu = $_POST['value_status_menu_'.$key];
                    $tree->prive = $_POST['value_prive_'.$key];
                    $tree->meta_title = $_POST['value_meta_title_'.$key];
                    $tree->meta_description = $_POST['value_meta_description_'.$key];
                    $tree->meta_keywords = $_POST['value_meta_keywords_'.$key];
                    $tree->indexation = $_POST['value_indexation_'.$key];
                    $tree->save();
                    
                    
                    /////////////////////////////////////////////////////////////////////////////////////
                    /////////////////////////     Elt Template de la page     ///////////////////////////
                    /////////////////////////////////////////////////////////////////////////////////////

                    // Suppression des anciens éléments
                    $tree_elements = new o\data('tree_elements', array('id_langue' => $key, 'id_tree' => $this->tree->id_tree));
                    foreach ($tree_elements as $elt) {
                        $elt->delete();
                    }

                    // Enregistrement des nouveaux
                    $elementsTPL = new o\data('elements', array('id_template'=>$_POST['id_template']));
                    
                    foreach ($elementsTPL as $elt) {
                        $tree_elements = new o\tree_elements(array('id_langue' => $key, 'id_element' => $elt->id_element, 'id_tree' => $this->tree->id_tree));
                        $tree_elements->id_tree = $this->tree->id_tree;
                        $tree_elements->id_element = $elt->id_element;
                        $this->clTemplates->handleFormBO($tree_elements, $elt, $key, $_POST, $_FILES, $elt->slug, $this->spath);
                    }
                }
               
                $this->tree_geozones->delete();
                $this->tree_profiles->delete();

                foreach($this->geozones as $zone)
                {
                    if($_POST['zone'.$zone->id_geozone]==1)
                    {

                        $tree_geozones = new o\tree_geozones();
                        $tree_geozones->id_tree = $_POST['id_tree'];
                        $tree_geozones->id_geozone = $zone->id_geozone;
                        $tree_geozones->insert();
                    }
                }

                foreach($this->profiles as $profile)
                {
                    if($_POST['profile'.$profile->id_profile]==1)
                    {
                        $tree_profiles = new o\tree_profiles();
                        $tree_profiles->id_tree = $_POST['id_tree'];
                        $tree_profiles->id_profile = $profile->id_profile;
                        $tree_profiles->insert();
                    }
                }

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-pageedit', $this->language, 'Modification d\'une page') : $this->ln->txt('admin-logs', 'action-pageadd', $this->language, 'Ajout d\'une page')), $this->tree->title, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'page', $this->language, 'Page'), (!$this->new ? $this->ln->txt('admin-banner', 'page-edit', $this->language, 'La page a bien été modifié') : $this->ln->txt('admin-banner', 'page-add', $this->language, 'La page a bien été ajouté')));
                
                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/edition/formPage/page___' . $this->tree->id_tree . '/part___' . $_POST['part'] . '/lng___' . $_POST['selectedLang']);
                } else {
                    $this->redirect($this->lurl . '/edition/site');
                }
            }
        } else {
            if($this->duplicate === true){
                $_POST = $tree_origin->current()->getArray();
                foreach($this->lLangues as $key => $langue){
                    $tree = new o\tree(array('id_tree' => $this->params['duplicate'], 'id_langue' => $key));
                    
                    $_POST['value_title_'.$key] = $tree->title;
                    $_POST['value_menu_title_'.$key] = $tree->menu_title;
                    $_POST['value_slug_'.$key] = $tree->slug;
                    $_POST['value_status_'.$key] = $tree->status;
                    $_POST['value_status_menu_'.$key] = $tree->status_menu;
                    $_POST['value_prive_'.$key] = $tree->prive;
                    $_POST['value_meta_title_'.$key] = $tree->meta_title;
                    $_POST['value_meta_description_'.$key] = $tree->meta_description;
                    $_POST['value_meta_keywords_'.$key] = $tree->meta_keywords;
                    $_POST['value_indexation_'.$key] = $tree->indexation;
                }
            } else {
                $this->ongletActif = (isset($this->params['part']) && $this->params['part'] != '' ? $this->params['part'] : 'tab-1');
                $this->langueActif = (isset($this->params['lng']) && $this->params['lng'] != '' ? $this->params['lng'] : $this->language);

                $_POST = $this->tree->getArray();
                foreach($this->lLangues as $key => $langue){
                    $tree = new o\tree(array('id_tree' => $this->params['page'], 'id_langue' => $key));
                    $_POST['value_title_'.$key] = $tree->title;
                    $_POST['value_menu_title_'.$key] = $tree->menu_title;
                    $_POST['value_slug_'.$key] = $tree->slug;
                    $_POST['value_status_'.$key] = $tree->status;
                    $_POST['value_status_menu_'.$key] = $tree->status_menu;
                    $_POST['value_prive_'.$key] = $tree->prive;
                    $_POST['value_meta_title_'.$key] = $tree->meta_title;
                    $_POST['value_meta_description_'.$key] = $tree->meta_description;
                    $_POST['value_meta_keywords_'.$key] = $tree->meta_keywords;
                    $_POST['value_indexation_'.$key] = $tree->indexation;
                }
            }
            
            if(isset($this->params['parent'])){
                $tree = new o\data('tree', array('id_tree' => $this->params['parent']));
                if($tree->count() > 0)
                {
                    $_POST['id_parent'] = $this->params['parent'];
                }
            }
            $this->treeZones = array();
            $this->treeProfiles = array();
            if(!$this->new) {
                $this->cg = (new o\data('tree_geozones'))->addWhere('id_tree='.$this->params['page']);
                $this->cp = (new o\data('tree_profiles'))->addWhere('id_tree='.$this->params['page']);
        
            foreach($this->cg as $g)
                $this->treeZones[] = $g->id_geozone;

            foreach($this->cp as $p)
                $this->treeProfiles[] = $p->id_profile;
            }

            /******* Liste de documents *******/
            $this->treeDocuments = array();
            $this->lDocs = (new o\data('documents_tree'))->addWhere('id_tree='.$this->params['page'])->order('ordre','ASC','');

            foreach($this->lDocs as $d){
                $doc = new o\documents(array('id_document' => $d->id_document));
                $doc->ordre = $d->ordre;
                $this->treeDocuments[$d->ordre.'-'.$doc->id_document] = $doc;
            }
            ksort($this->treeDocuments);
        }
    }
    
    /**
     * Gestion de la suppression d'une page (Suppression des tree_elements en cascade SQL)
     * Utilise le moteur Octeract.
     * 
     * @function _deletePage
     */
    public function _deletePage() {
        $this->users->checkAccess('editsite');
        $this->autoFireNothing = true;
        $tree = new o\data('tree', array('id_tree'=>$this->params[0]));
        
        if (isset($this->params[0]) && $this->params[0] != '' && $tree->count() > 0) {
            $backLog = serialize($tree);
            $tree->delete();
            //print_r($tree);die;
            //$this->clFonctions->logging($this->ln->txt('admin-logs', 'action-pagedel', $this->language, 'Supression d\'une page'), $tree->title, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'page', $this->language, 'Page'), $this->ln->txt('admin-banner', 'page-del', $this->language, 'La page a bien été supprimée'));
            $this->redirect($this->lurl . '/edition/site');
        } else {
            $this->redirect($this->lurl . '/edition/site');
        }
    }
    
    /**
     * Gestion de l'annulation de l'ajout d'une page 
     * Utilise le moteur Octeract.
     * 
     * @function _cancelAddPage
     */
    public function _cancelAddPage() {
        $this->users->checkAccess('editsite');
        $this->autoFireNothing = true;
        if (isset($this->params[0])) {
            $tree = new o\tree(array('id_tree' => $this->params[0]));
            $tree->delete();
        }
    }
        
    /**
     * Récupère les éléments de formulaire d'un template donné
     * 
     * @function _getTemplateElements
     */
    public function _getTemplateElements(){
        $this->users->checkAccess('editsite');
        $this->autoFireNothing = true;
        
        $this->clTemplates = new clTemplates($this->surl, $this->ln, $this->language);
        $templates = new o\templates(array('id_template'=>$this->params[1]));
        
        if($templates->exist()){
            if($templates->elements_groupes != null && $templates->elements_groupes->addWhere('status=1')->count() > 0) {
                
                if($templates->elements_groupes->order('ordre')->elements->addWhere('status=1')->count() > 0) {
                    foreach($templates->elements_groupes as $gp){
                        ?>
                        <div class="panel panel-default" >
                            <div class="panel-heading">
                                <?= $this->ln->txt('admin-edition', 'liste-element-gpe', $this->language, 'Liste des éléments du groupe :') ?> 
                                <?= $this->ln->txt('admin-edition', 'nomgpe-' . $gp->slug, $this->language, $gp->nom) ?>
                                <div class="ibox-tools">
                                    <a class="collapse-link collapse-link-gp-element" id="<?=$gp->id_groupe?>"> 
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="panel-body panel-body-2 panel-collapse collapse in" id="g_<?=$gp->id_groupe?>">
                            <?php
                            foreach($gp->elements as $elt){
                                $tree_elements = new o\data('tree_elements', array('id_element' => $elt->id_element, 'id_tree' => $this->params[0]));
                                echo $this->clTemplates->affichageFormBO($elt, clone $tree_elements);
                            }
                            ?>
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    echo $this->ln->txt('admin-generic', 'template-sans-element', $this->language, 'Ce template ne contient pas d\'élément actif');
                }
            }
            else{
                $templates->elements->addWhere('status=1');
                if($templates->elements->count() > 0) {
                    foreach($templates->elements as $elt){
                        $tree_elements = new o\data('tree_elements', array('id_element' => $elt->id_element, 'id_tree' => $this->tree->id_tree));
                        echo $this->clTemplates->affichageFormBO($elt, clone $tree_elements);
                    }
                } else {
                    echo $this->ln->txt('admin-generic', 'template-sans-element', $this->language, 'Ce template ne contient pas d\'élément actif');
                }
            }
        }
        die;
    }
    
    /**
     * Gestion de la liste des blocs.
     * Utilise le moteur Octeract.
     * 
     * @function _blocs
     */
    public function _blocs() {
        $this->users->checkAccess('editblocs');
        $this->ssMenuActive = 'editblocs';
        
        $this->blocs = new o\data('blocs');
    }
    
    /**
     * Gestion de l'édition/ajout d'un bloc.
     * Utilise le moteur Octeract.
     * 
     * @function _formBloc
     */
    public function _formBloc() {
        $this->users->checkAccess('editblocs');
        $this->ssMenuActive = 'editblocs';
        
        $this->loadJs('icheck.min');
        $this->loadCss('icheck');
        
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        
        if(isset($this->params['bloc'])){
            $this->blocs = new o\blocs(array('id_bloc'=>$this->params['bloc']));
            if($this->blocs->exist()){
                // Pas de dépendance pour l'instant
            } else {
                $this->redirect($this->lurl . '/edition/blocs');
            }
        } else {
            $this->new = true;
            $this->blocs = new o\blocs('');
        }
        
        
        if (isset($_POST['sendForm'])) {
            $_POST['name'] = trim(htmlspecialchars($_POST['name']));
            $this->errorFormNom = false;
            $this->error = false;
            $this->errorMsg = '';
            
            if (strlen($_POST['name']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }
            
            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->blocs->getArray()) : '');
                $this->blocs->name = $_POST['name'];
                $this->blocs->slug = generateSlug(! $this->new ? $this->blocs->slug : $this->clFonctions->controlSlug('blocs', $_POST['slug']!=""?$_POST['slug']:$_POST['name']));
                $this->blocs->status = (isset($_POST['status']) ? $_POST['status'] : 0);
                $this->blocs->save();
                
                $this->clFonctions->updateSlug($this->blocs, $this->blocs->id_bloc);
                
                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editbloc', $this->language, 'Modification d\'un bloc') : $this->ln->txt('admin-logs', 'action-addbloc', $this->language, 'Ajout d\'un bloc')), $this->blocs->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'bloc', $this->language, 'Bloc'), (!$this->new ? $this->ln->txt('admin-banner', 'bloc-edit', $this->language, 'Le bloc a bien été modifié') : $this->ln->txt('admin-banner', 'bloc-add', $this->language, 'Le bloc a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/edition/formBloc/bloc___' . $this->blocs->id_bloc);
                } else {
                    $this->redirect($this->lurl . '/edition/blocs');
                }
            }
        } else {
            $_POST = (!$this->new ? $this->blocs->getArray() : array());
        }
    }
    
    /**
     * Gestion de suppression d'un bloc
     * Utilise le moteur Octeract.
     * 
     * @function _deleteBloc
     */
    public function _deleteBloc() {
        $this->users->checkAccess('editblocs');
        $this->autoFireNothing = true;
        
        if (isset($this->params[0])) {
            $blocs = new o\blocs(array('id_bloc' => $this->params[0]));
            if($blocs->exist()) {
                $backLog = serialize($blocs->getArray());
                $blocs->delete();

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-blocdel', $this->language, 'Supression d\'un bloc'), $blocs->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'blocs', $this->language, 'Blocs'), $this->ln->txt('admin-banner', 'bloc-del', $this->language, 'Le bloc a bien été supprimé'));

                $this->redirect($this->lurl . '/edition/blocs');
            }
        }
        
        $this->redirect($this->lurl . '/edition/blocs');
    }
        
    /**
     * Gestion de la liste des éléments d'un bloc.
     * Utilise le moteur Octeract.
     * 
     * @function _elementsBloc
     */
    public function _elementsBloc() {
        $this->users->checkAccess('editblocs');
        $this->ssMenuActive = 'editblocs';
        
        $this->loadJs('jquery.nestable');
        
        $this->blocs = new o\blocs($this->params['bloc']);
        
        if($this->blocs->exist()) {
            $this->elements_groupes = new o\data('elements_groupes', array('id_bloc' => $this->blocs->id_bloc));
            foreach($this->elements_groupes as $eg) {
                $this->{'lElements' . $eg->id_groupe} = new o\data('elements', array('id_bloc' => $this->blocs->id_bloc, 'id_groupe' => $eg->id_groupe));
            }
            $this->orphan_elements = new o\data('elements', array('id_bloc' => $this->blocs->id_bloc, 'id_groupe' => NULL));
        } else {
            $this->redirect($this->lurl . '/edition/blocs');
        }
    }
    
    /**
     * Gestion de la liste des menus.
     * Utilise le moteur Octeract.
     * 
     * @function _menus
     */
    public function _menus() {
        $this->users->checkAccess('editmenus');
        $this->ssMenuActive = 'editmenus';
        
        $this->menus = new o\data('menus');
    }
    
    /**
     * Gestion de la suppression d'un menu.
     * Utilise le moteur Octeract.
     * 
     * @function _deleteMenu
     */
    public function _deleteMenu() {
        $this->users->checkAccess('editmenus');
        $this->autoFireNothing = true;
        
        $menus = new o\menus(array('id_menu'=>$this->params[0]));

        if ($menus->exist()) {
            $backLog = serialize($menus);
            $menus->delete();
            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-menudel', $this->language, 'Supression d\'un menu'), $menus->nom, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'menus', $this->language, 'Menus'), $this->ln->txt('admin-banner', 'menu-del', $this->language, 'Le menu a bien été supprimé'));
            $this->redirect($this->lurl . '/edition/menus');
        } else {
            $this->redirect($this->lurl . '/edition/menus');
        }
    }
    
    /**
     * Gestion de l'ajout/edition d'un menu.
     * Utilise le moteur Octeract.
     * 
     * @function _formMenu
     */
    public function _formMenu() {
        $this->users->checkAccess('editmenus');
        $this->ssMenuActive = 'editmenus';
        
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        
        $this->loadJs('icheck.min');
        $this->loadCss('icheck');
        
        $this->menus = new o\menus(array('id_menu'=>$this->params[0]));
        $this->new = ($this->menus->exist() ? false : true);
        
        if(isset($this->params[0]) && $this->params[0] != '' && $this->new){
            $this->redirect($this->lurl . '/edition/menus');
        }

        if (isset($_POST['sendForm'])) {
            $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
            $this->errorFormNom = false;
            $this->error = false;
            $this->errorMsg = '';
            
            if (strlen($_POST['nom']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = ($this->new==false ? serialize($this->menus) : '');
                $this->menus->nom = $_POST['nom'];
                $this->menus->slug = ($this->new==false ? $this->menus->slug : $this->bdd->generateSlug($_POST['nom']));
                $this->menus->status = (isset($_POST['status']) ? $_POST['status'] : 0);
                $this->menus->save();

                $this->clFonctions->logging(($this->new==false ? $this->ln->txt('admin-logs', 'action-editmenu', $this->language, 'Modification d\'un menu') : $this->ln->txt('admin-logs', 'action-addmenu', $this->language, 'Ajout d\'un menu')), $this->menus->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'menus', $this->language, 'Menus'), ($this->new==false ? $this->ln->txt('admin-banner', 'menu-edit', $this->language, 'Le menu a bien été modifié') : $this->ln->txt('admin-banner', 'menu-add', $this->language, 'Le menu a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/edition/formMenu/' . $this->menus->id_menu);
                } else {
                    $this->redirect($this->lurl . '/edition/menus');
                }
            }
        } else {
            $_POST = ((!$this->new)?$this->menus->getArray():array());
        }
    }
    
    /**
     * Gestion de la liste des éléments d'un menu.
     * Utilise le moteur Octeract.
     * 
     * @function _elementsMenu
     */
    public function _elementsMenu() {
        $this->users->checkAccess('editmenus');
        $this->ssMenuActive = 'editmenus';
        
        $this->loadJs('jquery.nestable');
        
        $this->menus = new o\menus(array('id_menu'=>$this->params['menu']));

        if ($this->menus->exist()) {
            $this->lLiens = new o\data('menus_content', array('id_langue'=>$this->language, 'id_menu'=>$this->menus->id_menu));
        } else {
            $this->redirect($this->lurl . '/edition/menus');
        }
    }
        
    /**
     * Gestion de l'ordonnancement des éléments d'un menu.
     * Utilise le moteur Octeract.
     * 
     * @function _orderLiens
     */
    public function _move()
    {
        $liste = json_decode($_POST['ordre']);
        $i = 0;
        $return = [];
        foreach($liste as $pg)
        {
            $id = str_replace('child','',$pg);
            $tree = new o\tree(array('id_langue'=>$_POST['ln'],'id_tree'=>$id));
            if($tree->exist()) {
                $return[$id] = $i;
                $tree->ordre = $i;
                $i++;
                $tree->save();
            }
        }
        //echo json_encode($return);
        die;
        //mail('t.raymond@equinoa.com','debug np',$_POST['ordre']);die;
    }

    public function _move_order_doc()
    {
        $liste = json_decode($_POST['ordre']);
        $i = 1;
        $return = [];
        foreach($liste as $id_doc)
        {
            $documents_tree = new o\documents_tree(array('id_document'=>$id_doc,'id_tree'=>$this->params[0]));
            if($documents_tree->exist()) {
                $return[$id_doc] = $i;
                $documents_tree->ordre = $i;
                $i++;
                $documents_tree->save();
            }
        }
        echo json_encode($return);
        die;
        //mail('t.raymond@equinoa.com','debug np',$_POST['ordre']);die;
    }

    public function _orderLiens() {
        $this->users->checkAccess('editmenus');
        $this->autoFireNothing = true;

        if (isset($_POST['orderLiens']) && $_POST['orderLiens'] != '' && isset($_POST['id_langue']) && $_POST['id_langue'] != '') {
            $lLiens = json_decode($_POST['orderLiens'], true);
            
            foreach ($lLiens as $key => $val) {
                $menus_content = new o\menus_content(array('id' => $val['id'], 'id_langue' => $_POST['id_langue']));
                if ($menus_content->exist()) {
                    $menus_content->ordre = ($key + 1);
                    $menus_content->update();
                }
            }

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-orderliens', $this->language, 'Ordonnancement des liens'), serialize($lLiens));
        }
    }
    
    /**
     * Gestion de l'ajout/edition d'un lien d'un menu.
     * Utilise le moteur Octeract.
     * 
     * @function _formLien
     */
    public function _formLien() {
        $this->users->checkAccess('editmenus');
        $this->ssMenuActive = 'editmenus';
        
        $this->loadJs('icheck.min');
        $this->loadCss('icheck');
        
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        
        $this->menus = new o\menus(array('id_menu'=>$this->params['menu']));
        $this->menus_content = new o\data('menus_content', array('id' => $this->params['lien']));

        if ($this->menus->exist()) {
            $this->new = $this->menus_content->count()>0 ? false : true;
            if($this->new){
                $menus_content_test =new o\data('menus_content');
                $nextID = max(array_merge(array(0), $menus_content_test->id->getArray())) + 1;
                $nextOrdre = max(array_merge(array(0), $menus_content_test->ordre->getArray())) + 1;
            }
            $this->currentLn = in_array($this->params['lng'], array_keys($this->ln->tabLangues))?$this->params['lng']:$this->language;

            if (isset($_POST['sendForm'])) {
                $_POST['nom_'.$this->language] = trim(htmlspecialchars($_POST['nom_'.$this->language]));
                $this->errorFormName = false;
                $this->error = false;
                $this->errorMsg = '';

                if (strlen($_POST['nom_'.$this->language]) == 0) {
                    $this->errorFormName = true;
                    $this->error = true;
                    $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
                }

                if (!$this->error) {
                    
                    foreach($this->ln->tabLangues as $key => $ln) {
                        $menus_content = new o\menus_content(array('id' => $this->params['lien'], 'id_langue'=>$this->language));                        
                        $backLog = (!$this->new ? serialize($menus_content) : '');
                        
                        $menus_content->id = $this->new?$nextID:$this->params['lien'];
                        $menus_content->id_langue = $key;
                        $menus_content->id_menu = $_POST['id_menu'];
                        $menus_content->status = $_POST['status_'.$key];
                        $menus_content->nom = $_POST['nom_'.$key];
                        $menus_content->target = $_POST['target_'.$key];
                        $menus_content->ordre = $nextOrdre;
                        
                        // Spécificité : Ne retenir que le dernier element non vide et remplir le champ complément.
                        $menus_content->value = $menus_content->complement = '';
                        $cpt = 0;
                        foreach ($_POST['value_'.$key] as $i => $val){
                            if($val != ''){
                                $menus_content->value = $val;
                                $cpt = $i;
                            }
                        }
                        switch($cpt){ case 0: $menus_content->complement='L'; break;  case 1: $menus_content->complement='P'; break;  case 2: $menus_content->complement='LX'; break;}
                        $menus_content->save();
                        $this->clFonctions->logging((!$this->new ? 
                                                      $this->ln->txt('admin-logs', 'action-editmenulien', $this->language, 'Modification d\'un lien de menu') : 
                                                      $this->ln->txt('admin-logs', 'action-addmenulien', $this->language, 'Ajout d\'un lien de menu')), $this->elements->name, $backLog);
                    }
                    $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'menus', $this->language, 'Menus'), (!$this->new ? $this->ln->txt('admin-banner', 'lien-edit', $this->language, 'Le lien a bien été modifié') : $this->ln->txt('admin-banner', 'lien-add', $this->language, 'Le lien a bien été ajouté')));

                    if ($_POST['restePage'] == 1) {
                        $this->redirect($this->lurl . '/edition/formLien/menu___' . $this->menus->id_menu . '/lien___' . $menus_content->id.'/lng___'.$_POST['selectedLang']);
                    } else {
                        $this->redirect($this->lurl . '/edition/elementsMenu/menu___' . $this->menus->id_menu);
                    }
                }                
            } else {
                $_POST = array();
                //$_POST = $this->menus_content->getArray();
                foreach ($this->menus_content->getArray() as $field => $value){
                    
                    foreach($this->ln->tabLangues as $key => $ln) {
                        $to_post = new o\menus_content(array('id_langue'=>$key, 'id' => $this->params['lien']));
                        $_POST['nom_'.$key] = $to_post->nom;
                        $_POST['value_'.$key] = $to_post->value;
                        $_POST['complement_'.$key] = $to_post->complement;
                        $_POST['target_'.$key] = $to_post->target;
                        $_POST['status_'.$key] = $to_post->status;
                    }
                }
            }            
        } else {
            $this->redirect($this->lurl . '/edition/menus');
        }
    }
    
    /**
     * Gestion de la suppression d'un lien d'un menu.
     * Utilise le moteur Octeract.
     * 
     * @function _deleteLien
     */
    public function _deleteLien() {
        $this->users->checkAccess('editmenus');
        $this->autoFireNothing = true;
        
        $menus_content = new o\data('menus_content', array('id' => $this->params[0]));
        
        if ($menus_content->count() > 0) {
            $backLog = serialize($menus_content);
            
            $id_menu = $menus_content->current()->id_menu;
            $nom = $menus_content->current()->nom;
            $menus_content->delete();
            
            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-liendel', $this->language, 'Supression d\'un lien'), $nom, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'menus', $this->language, 'Menus'), $this->ln->txt('admin-banner', 'lien-del', $this->language, 'Le lien a bien été supprimé'));
            
            $this->redirect($this->lurl . '/edition/elementsMenu/menu___'.$id_menu);
        } else {
            $this->redirect($this->lurl . '/edition/menus');
        }
    }
    
    /**
     * Gestion du formulaire d'ajout / édition du contenu d'un élément bloc 
     * Utilise le moteur Octeract.
     * 
     * @function _formBlocElmtContent
     */
    public function _formBlocElmtContent() {
        $this->users->checkAccess('editblocs');
        $this->ssMenuActive = 'editblocs';
        
        $this->clFonctions = new clFonctions($this->lurl, $this->surl, $this->ln, $this->language);
        $this->clTemplates = new clTemplates($this->surl, $this->ln, $this->language);
        $this->bloc = new o\blocs($this->params['bloc']);

        if($this->bloc->exist()) {
            $type = 'bloc';
            
            $this->currentID = $this->bloc->id_bloc;
            $this->currentType = 'bloc';
            $this->typeShort1 = 'elementsBloc';
            $this->typeShort2 = 'bloc';
            $this->typeShort3 = 'Bloc';
            
            $this->elements = $this->bloc->elements->where(array('id_element' => $this->params['elmt']));
            $this->element = $this->elements->getInstance();
            if($this->element->exist())
            {
                $this->blocs_elements = new o\data('blocs_elements', array('id_bloc' => $this->bloc->id_bloc, 'id_element' => $this->element->id_element));

                if(isset($_POST['sendForm']) && $this->params['elmt'] == $_POST['id_element']) {
                    $this->error = false;
                    $this->errorMsg = '';            

                    if(!$this->error) {
                        $backLog = serialize($this->elements->getArray());

                        foreach ($this->lLangues as $key => $langue) {
                            $bloc_element = new o\blocs_elements(array('id_langue' => $key, 'id_element' => $this->element->id_element, 'id_bloc' => $this->bloc->id_bloc));
                            $bloc_element->id_bloc = $this->bloc->id_bloc;
                            $bloc_element->id_element = $this->element->id_element;
                            $this->clTemplates->handleFormBO($bloc_element, $this->element, $key, $_POST, $_FILES, $this->element->slug, $this->spath);
                        }
                    
                        $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-bloceltcontentedit', $this->language, 'Modification de contenu d\'un élément de bloc'), $this->element->id_element, $backLog);
                        $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'blocelt', $this->language, 'Elément de bloc'), $this->ln->txt('admin-banner', 'blocelt-edit', $this->language, 'Le contenu de l\'élément de bloc a bien été modifié'));

                        if($_POST['restePage'] == 1) {
                            $this->redirect($this->lurl . '/edition/formBlocElmtContent/bloc___' . $this->bloc->id_bloc . '/elmt___' . $this->element->id_element . '/lng___' . $_POST['selectedLang']);
                        } else {
                            $this->redirect($this->lurl . '/edition/elementsBloc/bloc___' . $this->bloc->id_bloc);
                        }
                    }
                }
                else
                {
                    $this->langueActif = (isset($this->params['lng']) && $this->params['lng'] != '' ? $this->params['lng'] : $this->language);
                    $_POST['id_element'] = $this->params['elmt'];
                }
            }
            else
            {
                $this->redirect($this->lurl . '/edition/elementsBloc/bloc___' . $this->bloc->id_bloc);
            }
        }
        else
        {
            $this->redirect($this->lurl . '/edition/elementsBloc/bloc___' . $this->bloc->id_bloc);
        }    
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    

    

    

    
    
    
    
    

    

    

    

    

    

    

    

    

    

    

    

    

}
