<?php

class videosController extends bootstrap
{
    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'videos';
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default() {

        $this->users->checkAccess('videos');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('videos'));

    }

    /**
     * Gestion de la page de gestion des videos.
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _all_videos
     */
    public function _all_videos()
    {
        $this->users->checkAccess('videall-videos');
        $this->ssMenuActive = 'videall-videos';

        $this->loadJs('chosen.jquery');
        $this->loadCss('chosen');

        $this->loadCss('icheck');
        $this->loadJs('icheck.min');

        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');

        //récup des listes
        $this->languages = new o\data('languages');
        $this->owners = new o\data('owners');
        $this->folders = new o\data('folders');
        $this->licences = new o\data('licences');

        $reset = false;
        if(isset($_POST['filter']))
        {
            if(isset($_POST['title']) && !empty($_POST['title'])){
                $filters[] = 'v.title LIKE "%'.htmlspecialchars($_POST['title']).'%"';
            }

            if(isset($_POST['video-language']) && !empty($_POST['video-language'])){
                if(!in_array('0',$_POST['video-language']) && count($_POST['video-language']) > 0){
                    $LLanguages = implode(',',$_POST['video-language']);
                    $filters[] = 'v.id_language IN('.htmlspecialchars($LLanguages).')';
                }
            }
            else{
                $_POST['video-language'] = array();
            }

            if(isset($_POST['video-owner']) && !empty($_POST['video-owner'])){
                $filters[] = 'v.id_owner = '.htmlspecialchars($_POST['video-owner']);
            }

            if(isset($_POST['video-folders']) && !empty($_POST['video-folders']) && $_POST['video-folders'] !== "0"){
                $filters[] = 'v.id_video IN( SELECT fv.id_video FROM folders_videos fv WHERE fv.id_folder IN('.htmlspecialchars($_POST['video-folders']).'))';

            }

            if(isset($_POST['video_licence']) && !empty($_POST['video_licence']) && $_POST['video_licence'] !== 0){
                $filters[] = 'v.id_licence ='.htmlspecialchars($_POST['video_licence']);
            }

            if($_POST['active_videos'] === "on"){
                $status_searched = 1;
                $filters[] = 'v.active ='.$status_searched;
            }else{
                $status_searched = 0;
                $filters[] = 'v.active ='.$status_searched;
            }

            if($_POST['no_dialogue'] === "on"){
                $dialogue_searched = 1;
                $filters[] = 'v.no_dialogue ='.$dialogue_searched;
            }else{
                $dialogue_searched = 0;
                $filters[] = 'v.no_dialogue ='.$dialogue_searched;
            }



            $_SESSION['filter_videos']['title'] = $_POST['title'];
            $_SESSION['filter_videos']['active_videos'] = $status_searched;
            $_SESSION['filter_videos']['no_dialogue'] = $dialogue_searched;
            $_SESSION['filter_videos']['video-language'] = $_POST['video-language'];
            $_SESSION['filter_videos']['video-owner'] = $_POST['video-owner'];
            $_SESSION['filter_videos']['video-folders'] = $_POST['video-folders'];
            $_SESSION['filter_videos']['video_licence'] = $_POST['video_licence'];
            $_SESSION['filter_videos']['filters'] = $filters;


            $joins = array();
            $group_by = "";
            $orders[] = "v.ordre DESC";

        }
        elseif($this->params['0'] == "reset"){
            $reset = true;
            unset($_SESSION['filter_videos']);
            $this->redirect($this->lurl . '/videos/all_videos');
        }
        else{
            // on filtre en fonction des session
            /*if(isset($_SESSION['filter_folder'])){
                $this->folders->addWhere('title like "%'.$_SESSION['foldTitle'].'%" ');
            }*/
        }

        //reset ou situation initiale
        if($reset or !isset($_POST['filter'])){

            $_SESSION['filter_videos']['active_videos'] = 1;
            $_SESSION['filter_videos']['no_dialogue'] = 0;
            $_SESSION['filter_videos']['video-language'] = array();
            $_SESSION['filter_videos']['video-owner'] = 0;
            $_SESSION['filter_videos']['video-folders'] = array();
            $_SESSION['filter_videos']['video_licence'] = 0;


            $orders[] ="v.ordre DESC";
            $filters = array();
            $joins = array();
            $group_by = "";

        }

        $sql = 'SELECT v.*
                FROM `videos` as v'
            . implode(' ', $joins)
            .(!empty($filters) ? ' WHERE ' . implode(' AND ', $filters) : '')
            .$group_by
            . ' ORDER BY ' . implode(',', $orders);

        $_SESSION['filter_videos']['sql_videos'] = [
            'filters' => $filters,
            'orders' => $orders,
            'joins' => $joins,
            'group_by' => $group_by,
        ];

        $result = $this->bdd->query($sql);
        $list = [];
        while ($results = $this->bdd->fetch_assoc($result)) {
            $list[] = $results;
        }
        $this->LVideos = $list;

    }

    /**
     * Gestion de l'ajout/edition des videos.
     * Utilise le moteur Octeract.
     *
     * @function _formVideo
     */
    public function _formVideo()
    {
        $this->users->checkAccess('foldersallfolders');
        $this->ssMenuActive = 'foldersallfolders';


        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadJs('bootstrap-datepicker');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->loadCss('datepicker');

        //récup des listes
        $this->languages = new o\data('languages');
        $this->folders_type = new o\data('folders_type');
        $this->countries = new o\data('countries');
        $this->folders_media_type = new o\data('folders_media_type');
        $this->folders_priority = new o\data('folders_priority');


        $_POST['languages_video'] = array();
        if (!empty($this->params['video'])) {
            $this->videos = new o\videos(array('id_video' => $this->params['video']));
            if ($this->videos->exist()) {
                $this->new = false;
            } else {
                $this->redirect($this->lurl . '/videos/all_videos');
            }
        } else {
            $this->videos = new o\videos();
            $this->new = true;
        }

        if (isset($_POST['sendForm'])) {
            $_POST['title'] = trim(htmlspecialchars($_POST['title']));
            $_POST['folder-language'] = trim(htmlspecialchars($_POST['folder-language']));
            $_POST['folder-type'] = trim(htmlspecialchars($_POST['folder-type']));
            $_POST['folder-media-type'] = trim(htmlspecialchars($_POST['folder-media-type']));
            $_POST['folder-priority'] = trim(htmlspecialchars($_POST['folder-priority']));


            $this->errorFormNom = false;
            $this->errorFormPrenom = false;
            $this->errorFormEmail = false;
            $this->errorFormPass = false;
            $this->error = false;
            $this->errorMsg = '';
            if (strlen($_POST['title']) == 0) {
                $this->errorFormTitle = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->folders->getArray()) : '');

                //$this->folders->hash = "";
                $this->folders->id_type = $_POST['folder-type'];
                $this->folders->id_media_type = $_POST['folder-media-type'];
                $this->folders->id_priority = $_POST['folder-priority'];
                $this->folders->title = $_POST['title'];

                if (!empty($_FILES) && $_FILES['image']['name']!='') {
                    $tempFile = $_FILES['image']['tmp_name'];          //3

                    $targetPath = $this->path.'public/default/var/images/';  //4
                    $fn = mktime().'-'.$_FILES['image']['name'];
                    $targetFile =  $targetPath. $fn;  //5

                    move_uploaded_file($tempFile,$targetFile); //6
                    $this->folders->image  = $fn;

                    //thumbnail
                    $im = new imagick($this->path.'public/default/var/images/'.$fn.'[0]');
                    $im->setImageFormat('jpg');
                    $im->scaleImage(200,200,true);
                    $im = $im->flattenImages();
                    //header('Content-Type: image/jpeg');
                    $im->writeImage($this->path.'public/default/var/images/thumbs/'.$fn.'-thumb.jpg');;
                    $this->folders->thumbnail = $fn.'-thumb.jpg';
                }

                $this->folders->save();

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editfolder', $this->language, 'Modification d\'un dossier') : $this->ln->txt('admin-logs', 'action-addfolder', $this->language, 'Ajout d\'un dossier')), $this->folders->title, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'folders', $this->language, 'Folders'), (!$this->new ? $this->ln->txt('admin-banner', 'folder-edit', $this->language, 'Le dossier a bien été modifié') : $this->ln->txt('admin-banner', 'folder-add', $this->language, 'Le dossier a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/folders/formClient/folder___' . $this->folders->id_folder . '/part___' . $_POST['part']);
                } else {
                    $this->redirect($this->lurl . '/folders/all_folders');
                }
            }


        } else {
            $_POST = $this->videos->getArray();
            $_POST['languages_folder'] = array();
        }

        $this->image_import = false;

    }

    /**
     * Gestion de la suppression d'un video
     * Utilise le moteur Octeract.
     *
     * @function _deleteVideo
     */
    public function _deleteVideo()
    {
        $this->users->checkAccess('folders');
        $this->autoFireNothing = true;

        if (!empty($this->params['video'])) {
            $videos = new o\videos(array('id_video' => $this->params['video']));
            if ($videos->exist() > 0) {
                $backLog = serialize($videos->getArray());
                $nom = $videos->title;

                $videos->delete();

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-delete-video', $this->language, 'Video archiving'), $nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'video', $this->language, 'Video'), $this->ln->txt('admin-banner', 'video-del', $this->language, 'Video deleted'));
                $this->redirect($this->lurl . '/videos/all_videos');
            } else {
                $this->redirect($this->lurl . '/videos/all_videos');
            }
        } else {
            $this->redirect($this->lurl . '/videos/all_videos');
        }
    }

}
