<?php

class dashboardController extends bootstrap {

    public function __construct($command, $config, $app) {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'dashboard';

        $this->etatCommande = array(
            0 => '<span class=""><i class="fa fa-gear"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-en-cours', $this->language, 'En cours'),
            1 => '<span class="text-primary"><i class="fa fa-check"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-validee', $this->language, 'Validée').'</span>',
            2 => '<span class="text-primary"><i class="fa fa-truck"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-expediee', $this->language, 'Expédiée').'</span>',
            3 => '<span class="text-danger"><i class="fa fa-times"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-annulee', $this->language, 'Annulée').'</span>'
        );

        $this->etatCommandeBG = array(
            0 => 'gray-bg',
            1 => 'navy-bg',
            2 => 'navy-bg',
            3 => 'red-bg'
        );

        $this->etatCommandeText = array(
            0 => '<i class="fa fa-gear"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-en-cours', $this->language, 'En cours'),
            1 => '<i class="fa fa-check"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-validee', $this->language, 'Validée'),
            2 => '<i class="fa fa-truck"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-expediee', $this->language, 'Expédiée'),
            3 => '<i class="fa fa-times"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-annulee', $this->language, 'Annulée')
        );

        $this->etatLogistique = array(
            0 => '<span class="text-default">'.$this->ln->txt('admin-dash', 'etat-logistic-en-cours', $this->language, 'En cours').'</span>',
            1 => '<span class="text-primary">'.$this->ln->txt('admin-dash', 'etat-logistic-envoyee', $this->language, 'Envoyée').'</span>',
            2 => '<span class="text-danger">'.$this->ln->txt('admin-dash', 'etat-logistic-en-erreur', $this->language, 'En erreur').'</span>'
        );

        $this->etatPaiement = array(
            0 => '<span class="text-danger">'.$this->ln->txt('admin-dash', 'etat-paiement-non-valide', $this->language, 'Non valide').'</span>',
            1 => '<span class="text-primary">'.$this->ln->txt('admin-dash', 'etat-paiement-valide', $this->language, 'Valide').'</span>'
        );
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     *
     * @function _default
     */
    public function _default() {
        $this->users->checkAccess('dashboard');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('dashboard'));
    }

    /**
     * Gestion de la liste des commandes.
     * Utilise le moteur Octeract.
     *
     * @function _commandes
     */
    public function _commandes() {
        $this->users->checkAccess('dashcommandes');
        $this->transactions = new o\data('transactions');
        $this->ssMenuActive = 'dashcommandes';
    }

    /**
     * Détails d'une commande.
     * Utilise le moteur Octeract.
     *
     * @function _formCommande
     */
    public function _formCommande() {
        $this->users->checkAccess('dashcommandes');
        $this->transactions = new o\transactions(array('id_transaction'=>$this->params['cmd']));
        $this->ssMenuActive = 'dashcommandes';

        if($this->transactions->exist()){
            $this->loadJs('icheck.min');
            $this->loadCss('icheck');

            $this->loadJs('sweetalert.min');
            $this->loadCss('sweetalert');

            foreach(array('liv'=>'livraison', 'fac'=>'facturation') as $short => $long) {
                $this->{'adresse_'.$long} = new o\clients_adresses(array('id_adresse'=>$this->transactions->{'id_'.$long}));
                if( ! $this->{'adresse_'.$long}->exist()){
                    $this->{'adresse_'.$long}->civilite = $this->transactions->{'civilite_'.$short};
                    $this->{'adresse_'.$long}->nom = $this->transactions->{'nom_'.$short};
                    $this->{'adresse_'.$long}->prenom = $this->transactions->{'prenom_'.$short};
                    $this->{'adresse_'.$long}->societe = $this->transactions->{'societe_'.$short};
                    $this->{'adresse_'.$long}->adresse1 = $this->transactions->{'adresse1_'.$short};
                    $this->{'adresse_'.$long}->adresse2 = $this->transactions->{'adresse2_'.$short};
                    $this->{'adresse_'.$long}->cp = $this->transactions->{'cp_'.$short};
                    $this->{'adresse_'.$long}->ville = $this->transactions->{'ville_'.$short};
                    $this->{'adresse_'.$long}->id_pays = $this->transactions->{'pays_'.$short};
                    $this->{'adresse_'.$long}->telephone = $this->transactions->{'telephone_'.$short};
                }
            }

            $this->fdp_type = new o\fdp_type(array('id_type'=>$this->transactions->id_type, 'id_langue'=>$this->language));
            $this->url_tracking = $this->transactions->colis!=''?$this->fdp_type->url_suivi.$this->transactions->colis:'';

        } else {
            $this->redirect($this->clFonctions->goZoneUser('dashboard'));
        }
    }

    /**
     * Modification de l'état d'une commande.
     * Utilise le moteur Octeract.
     *
     * @function _etatCommande
     */
    public function _etatCommande() {
        $this->users->checkAccess('dashcommandes');
        $this->transactions = new o\transactions(array('id_transaction'=>$this->params[0]));

        if($this->transactions->exist()){
            $backLog = (!$this->new ? serialize($this->transactions->getArray()) : '');
            $this->transactions->etat = $this->params[1];
            $this->transactions->update();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-transaction-edit-state', $this->language, 'Modification d\'une commande'), $this->transactions->id_transaction, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'commande', $this->language, 'Commande'), $this->ln->txt('admin-banner', 'transactions-edit', $this->language, 'La commande a bien été modifiée'));

            $this->redirect($this->lurl.'/dashboard/formCommande/cmd___'.$this->params[0]);
        } else {
            $this->redirect($this->clFonctions->goZoneUser('dashboard'));
        }
    }

    /**
     * Suppression d'un produit d'une commande.
     * Utilise le moteur Octeract.
     *
     * @function _deletePdtCommande
     */
    public function _deletePdtCommande() {
        $this->users->checkAccess('dashcommandes');
        $this->transactions_produits = new o\transactions_produits(array('id_transaction_produit'=>$this->params[0]));

        if($this->transactions_produits->exist()){
            $backLog = (!$this->new ? serialize($this->transactions_produits->getArray()) : '');
            $this->transactions_produits->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-transaction-delete-pdt', $this->language, 'Suppression d\'un produit d\'une commande'), $this->transactions_produits->id_combinaison, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'commande', $this->language, 'Commande'), $this->ln->txt('admin-banner', 'transactions-edit', $this->language, 'La commande a bien été modifiée'));

            $this->redirect($this->lurl.'/dashboard/formCommande/cmd___'.$this->params[0]);
        } else {
            $this->redirect($this->clFonctions->goZoneUser('dashboard'));
        }
    }

    public function _ventes() {
        $this->users->checkAccess('dashventes');
    }

    public function _produits() {
        $this->users->checkAccess('dashproduits');
    }

    public function _trafic() {

        $this->users->checkAccess('trafic');
        $this->ssMenuActive = 'trafic';
        $this->loadJS('Chart.min');
        $this->loadJS('jquery.flot');
        $this->loadJS('jquery.flot.pie');
        $this->loadJS('jquery.flot.tooltip.min');
        $this->loadJs('jquery.datetimepicker.full.min');
        $this->loadCss('jquery.datetimepicker');

        $format = 'd/m/Y H:i';

        if(!empty($_POST['stats-trafic-start']) && !empty($_POST['stats-trafic-end'])){
            $this->date_deb = \DateTime::createFromFormat($format, $_POST['stats-trafic-start']);
            $this->date_fin = \DateTime::createFromFormat($format, $_POST['stats-trafic-end']);
        } else {
            $_POST['stats-trafic-start'] = date('d/m/Y 00:00', strtotime("-30 days"));
            $_POST['stats-trafic-end'] = date('d/m/Y 23:59', strtotime('today'));

            $this->date_deb = \DateTime::createFromFormat($format, $_POST['stats-trafic-start']);
            $this->date_fin = \DateTime::createFromFormat($format, $_POST['stats-trafic-end']);
        }

        $colors = array("#F43779", "#33A7E4", "#33E46D", "#FF9439", "#FF5039");
        shuffle($colors);

        $listIdZones = new o\data('geozones');
        $listIdProfiles = new o\data('profiles');

        if (!empty($this->params['export'])) {
            $this->autoFireNothing = true;
            if($this->params['export'] == "zones"){
                $calulatedStatsZones = $this->calculateStats($listIdZones,$this->params[1],$this->params[2], $colors, 'geozones');
                $this->statsZones=$calulatedStatsZones['stats'];
                $sumConnexionsZones = $calulatedStatsZones['sum'];
                $this->autoFireNothing = true;
                $lineBreak = "\n";
                $sepField = ";";
                $fieldsLabel = array(
                    'Zone',
                    'Nbre de Connexions',
                    '%',
                );
                $contentCSV = utf8_decode( implode($sepField, $fieldsLabel) . $lineBreak);

                foreach ($this->statsZones as $sZ){
                    $zone = $sZ['label'];
                    $nbrConnexionsZone = $sZ['data'];
                    $percent = $sumConnexionsZones > 0 ? number_format($nbrConnexionsZone / $sumConnexionsZones * 100, 2) : 0;

                    $contentCSV .= utf8_decode($zone . $sepField . $nbrConnexionsZone . $sepField . $percent . $sepField . $lineBreak);
                }

                $fileName='export-stats-zones-';

            } else if ($this->params['export'] == "profiles"){
                $calulatedStatsProfiles = $this->calculateStats($listIdProfiles,$this->params[1],$this->params[2], $colors, 'profiles');

                $this->statsProfiles=$calulatedStatsProfiles['stats'];
                $sumConnexionsZones=$calulatedStatsProfiles['sum'];
                $lineBreak = "\n";
                $sepField = ";";
                $fieldsLabel = array(
                    'Profile',
                    'Nbre de Connexions',
                    '%',
                );
                $contentCSV = utf8_decode(  implode($sepField, $fieldsLabel) . $lineBreak);

                foreach ($this->statsProfiles as $sP){
                    $profile = $sP['label'];
                    $nbrConnexionsProfile = $sP['data'];
                    $percent = $sumConnexionsZones > 0 ? number_format($nbrConnexionsProfile / $sumConnexionsZones * 100, 2) : 0;

                    $contentCSV .= utf8_decode($profile . $sepField . $nbrConnexionsProfile . $sepField . $percent . $sepField . $lineBreak);
                }

                $fileName='export-stats-profiles-';

            }
            header('Content-Type: application/csv-tab-delimited-table; charset=utf-8');
            header('Content-disposition: filename='. $fileName . date('YmdHis') . '.csv');
            header('Pragma: no-cache');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0, public');
            header('Expires: 0');

            echo $contentCSV;
            die;
        } else {
            $calulatedStatsZones = $this->calculateStats($listIdZones,$this->date_deb->format('Y-m-d'), $this->date_fin->format('Y-m-d'), $colors,'geozones');

            $this->statsZones=$calulatedStatsZones['stats'];
            $this->maxCountZone=$calulatedStatsZones['max_count'];
            $this->zonesMaxCountZone=$calulatedStatsZones['label_max_count'];
            $this->colorMaxCountZone=$calulatedStatsZones['colorMaxCountZones'];

            $this->donutsZonesDatas = array();
            foreach ($this->statsZones as $statZ) {
                $this->donutsZonesDatas[] = json_encode($statZ);
            }
            $this->donutsZonesDatas = implode(', ', $this->donutsZonesDatas);

            $calulatedStatsProfiles = $this->calculateStats($listIdProfiles,$this->date_deb->format('Y-m-d'), $this->date_fin->format('Y-m-d'), $colors,'profiles');

            $this->statsProfiles=$calulatedStatsProfiles['stats'];
            $this->maxCountProfile=$calulatedStatsProfiles['max_count'];
            $this->profileMaxCountProfile=$calulatedStatsProfiles['label_max_count'];
            $this->colorMaxCountProfile=$calulatedStatsProfiles['colorMaxCountZones'];

            $this->donutsProfilesDatas = array();
            foreach ($this->statsProfiles as $statP) {
                $this->donutsProfilesDatas[] = json_encode($statP);
            }
            $this->donutsProfilesDatas = implode(', ', $this->donutsProfilesDatas);
        }
    }

    public function _documentation() {
        $this->users->checkAccess('dashdocuments');
        $this->ssMenuActive = 'dashdocuments';
        $this->loadJS('Chart.min');
        $this->loadJS('jquery.flot');
        $this->loadJS('jquery.flot.pie');
        $this->loadJS('jquery.flot.tooltip.min');
        $this->loadJs('jquery.datetimepicker.full.min');
        $this->loadCss('jquery.datetimepicker');

        $format = 'd/m/Y H:i';
        if (!empty($_POST['stats-doc-start']) && !empty($_POST['stats-doc-end'])) {
            $this->date_deb_types = \DateTime::createFromFormat($format, $_POST['stats-doc-start']);
            $this->date_fin_types = \DateTime::createFromFormat($format, $_POST['stats-doc-end']);
        } else {
            $_POST['stats-doc-start'] = date('d/m/Y 00:00', strtotime("-30 days"));
            $_POST['stats-doc-end'] = date('d/m/Y 23:59', strtotime('today'));

            $this->date_deb_types = \DateTime::createFromFormat($format, $_POST['stats-doc-start']);
            $this->date_fin_types = \DateTime::createFromFormat($format, $_POST['stats-doc-end']);
        }

        $colors = array("#F43779", "#0092C7", "#0bbc43", "#FF9439", "#d3200c");
        shuffle($colors);

        $listIdTypes = new o\data('doctypes');

        if (!empty($this->params['export'])) {
            $this->autoFireNothing = true;

            if( $this->params['export'] == "types"){
                $calulatedStatsTypes = $this->calculateStats($listIdTypes,$this->params[1], $this->params[2], $colors, 'doctypes');
                $this->statsTypes = $calulatedStatsTypes['stats'];
                $sumDownlaodsTypes = $calulatedStatsTypes['sum'];

                $lineBreak = "\n";
                $sepField = ";";
                $fieldsLabel = array(
                    'Type de Document',
                    'Nbre de Téléchargements',
                    '%',
                );

                $contentCSV = utf8_decode(implode($sepField, $fieldsLabel) . $lineBreak);

                foreach ($this->statsTypes as $sT) {
                    $type = $sT['label'];
                    $nbrDownlaodsTypes = $sT['data'];
                    $percent = $sumDownlaodsTypes > 0 ? number_format($nbrDownlaodsTypes / $sumDownlaodsTypes * 100, 2) : 0;

                    $contentCSV .= utf8_decode($type . $sepField . $nbrDownlaodsTypes . $sepField . $percent . $sepField . $lineBreak);
                }
                $fileName = 'export-stats-telechargements-types-';

            } else if ( $this->params['export'] == "top10dl"){
                $this->top_10_downloads = $this->getTop10();

                $lineBreak = "\n";
                $sepField = ";";
                $fieldsLabel = array(
                    'Document',
                    'Type de Document',
                    'Nbre de Téléchargements'
                );

                $contentCSV = utf8_decode(implode($sepField, $fieldsLabel) . $lineBreak);

                foreach ($this->top_10_downloads as $topDlDoc) {
                    $file = $topDlDoc['file'];
                    $type = $topDlDoc['type'];
                    $nbDownloads = $topDlDoc['nbrDl'];

                    $contentCSV .= utf8_decode($file . $sepField . $type . $sepField . $nbDownloads . $sepField . $lineBreak);
                }
                $fileName = 'export-stats-telechargements-top10-';

            }

            header('Content-Type: application/csv-tab-delimited-table; charset=utf-8');
            header('Content-disposition: filename='. $fileName . date('YmdHis') . '.csv');
            header('Pragma: no-cache');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0, public');
            header('Expires: 0');

            echo $contentCSV;
            die;
        } else {

            $calulatedStatsTypes = $this->calculateStats($listIdTypes,$this->date_deb_types->format('Y-m-d'), $this->date_fin_types->format('Y-m-d'), $colors,'doctypes');

            $this->statsTypes = $calulatedStatsTypes['stats'];
            $this->maxCountType = $calulatedStatsTypes['max_count'];
            $this->typesMaxCountType = $calulatedStatsTypes['label_max_count'];
            $this->colorMaxCountType = $calulatedStatsTypes['colorMaxCountZones'];

            $this->donutsTypesDatas = array();
            foreach ($this->statsTypes as $statT) {
                $this->donutsTypesDatas[] = json_encode($statT);
            }
            $this->donutsTypesDatas = implode(', ', $this->donutsTypesDatas);

            $this->top_10_downloads = $this->getTop10();
        }
    }

    private function getTop10(){
        // Init
        $bdd = \bdd::get_instance();

        $sqlStats = 'SELECT COUNT(dl.id_download) as nbrDl, doc.label as file, dt.name as type
                    FROM downloads dl
                        JOIN documents doc 
                        ON dl.id_document = doc.id_document 
                        JOIN doctypes dt 
                        ON dt.id_doctype = doc.id_doctype 
                    WHERE dl.added > "'.$this->date_deb_types->format('Y-m-d').' 00:00:00"
                        AND dl.added < "'.  $this->date_fin_types->format('Y-m-d') .' 23:59:59"
                    GROUP BY dl.id_document 
                    ORDER BY nbrDl DESC 
                    LIMIT 10';
        //AND dl.id_client != "1"

        $res = $bdd->query($sqlStats);
        $this->top_10_downloads = array();
        while ($tmp = $bdd->fetch_array($res)) {
            $this->top_10_downloads[] = $tmp;
        }
        return $this->top_10_downloads;
    }

    private function calculateStats($listId,$date_deb,$date_fin,$colors,$tableName)
    {
        $countColors = count($colors);
//        $listId = new o\data($tableName);
        $this->stats = array();
        $j = $sum = $this->maxCount = $this->profileMaxCount = $this->colorMaxCount = 0;

        foreach ($listId as $item) {

            $label = $item->name;
            $exception = "";
            if ($tableName == 'doctypes'){
                //$exception = ' id_client != "1" AND ';
                $param1 = 'id_document';
                $tableCondition = 'documents';
                $param2 = 'id_doctype';
                $id = $item->id_doctype;
                $datasTable = 'downloads';

            } else if ($tableName == 'geozones') {
                //$exception = ' id_client != "1" AND ip != "91.151.70.8" AND ';
                $param1 = 'id_client';
                $tableCondition = 'clients_geozones';
                $param2 = 'id_geozone';
                $id = $item->id_geozone;
                $datasTable = 'logins';

            } else if ($tableName == 'profiles') {
                //$exception = ' id_client != "1" AND ip != "91.151.70.8" AND ';
                $param1 = 'id_client';
                $tableCondition = 'clients_profiles';
                $param2 = 'id_profile';
                $id = $item->id_profile;
                $datasTable = 'logins';

            }

            $where = $exception.' added > "' . $date_deb . ' 00:00:00" AND added < "' . $date_fin . ' 23:59:59" AND '. $param1 .' in (select '. $param1 .' FROM '. $tableCondition .' WHERE '. $param2 .' = ' . $id . ')';
            $datas = (new o\data($datasTable))->addWhere($where);
            $countDatas = count($datas);


            if($countDatas>0){
                $j++;
                $cOoleur = $colors[$j % $countColors];
            }else{
                $cOoleur = "#CCCCCC";
            }
            $this->stats[$id] = [
                'label' => $label,
                'data' => $countDatas,
                'color' => $cOoleur
            ];

            $sum += $countDatas;
            if ($countDatas >= $this->maxCount) {
                $this->maxCount = $countDatas;
                $this->profileMaxCount = $label;
                $this->colorMaxCount = $cOoleur;
            }
        }
        $this->maxCount = $sum > 0 ? number_format($this->maxCount / $sum * 100, 2) : 0;

        return $result = ['stats' => $this->stats, 'max_count' => $this->maxCount, 'label_max_count' => $this->profileMaxCount, 'colorMaxCountZones' => $this->colorMaxCount, 'sum' => $sum];
    }

}
