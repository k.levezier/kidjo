<?php

use o\data;
use o\mails_filer;
use o\partenaires;

class mailsController extends bootstrap
{

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'mails';

        $this->serveur_api = $this->clSettings->getParam('nmp-server-api', 'sets');
        $this->key_api = $this->clSettings->getParam('nmp-key', 'sets');
        $this->login_api = $this->clSettings->getParam('nmp-login', 'sets');
        $this->pwd_api = $this->clSettings->getParam('nmp-password', 'sets');
        $this->mail_api = $this->clSettings->getParam('nmp-mail', 'sets');
        $this->frommail_api = $this->clSettings->getParam('nmp-from-mail', 'sets');
        $this->id_clone_nmp = $this->clSettings->getParam('nmp-id-clonage', 'sets');
        $this->nmpEMV = false;

        //        if ($this->key_api != '' && $this->login_api != '' && $this->pwd_api != '' && $this->serveur_api != '') {
        //            $this->location_nmpsoap = 'https://' . $this->serveur_api . '/apitransactional/services/TransactionalService?wsdl';
        //            $this->nmpsoap = new SoapClient($this->location_nmpsoap);
        //            $result = $this->nmpsoap->openApiConnection(array('login' => $this->login_api, 'pwd' => $this->pwd_api, 'key' => $this->key_api));
        //            $this->token_soap = $result->return;
        //            $this->nmpEMV = true;
        //        }
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default()
    {
        $this->users->checkAccess('administration');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('administration'));
    }

    /**
     * Gestion des templates de mails
     * Utilise le moteur Octeract.
     *
     * @function _templates
     */
    public function _templates()
    {
        $this->users->checkAccess('mailstemplates');
        $this->ssMenuActive = 'mailstemplates';
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        $this->mails_text = new o\data('mails_text', array('id_langue' => $this->language));
    }

    /**
     * Gestion de la suppression d'un template de mail.
     * Utilise le moteur Octeract.
     *
     * @function _deleteTemplate
     */
    public function _deleteTemplate()
    {
        $this->users->checkAccess('mailstemplates');
        $this->autoFireNothing = true;
        $mails_text = new o\data('mails_text', array('type' => $this->params[0]));

        if (!empty($this->params[0]) && $mails_text->count() > 0) {
            $backLog = serialize($mails_text->getArray());
            $mails_text->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-tplmaildel', $this->language, 'Supression d\'un template de mail'), $mails_text->current()->name, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'mails', $this->language, 'Mails'), $this->ln->txt('admin-banner', 'tplmail-del', $this->language, 'Le template de mail a bien été supprimé'));

            $this->redirect($this->lurl . '/mails/templates');
        } else {
            $this->redirect($this->lurl . '/mails/templates');
        }
    }

    /**
     * Gestion de l'ajout / modification des templates de mails
     * Utilise le moteur Octeract.
     *
     * @function _formTemplate
     */
    public function _formTemplate()
    {
        $this->users->checkAccess('mailstemplates');
        $this->ssMenuActive = 'mailstemplates';
        $this->loadJs('codemirror/codemirror');
        $this->loadJs('codemirror/mode/htmlmixed');
        $this->loadJs('codemirror/mode/xml');
        $this->loadJs('codemirror/mode/javascript');
        $this->loadJs('codemirror/mode/css');
        $this->loadJs('codemirror/mode/clike');
        $this->loadJs('codemirror/mode/php');
        $this->loadJs('sweetalert.min');
        $this->loadCss('codemirror/codemirror');
        $this->loadCss('sweetalert');

        $this->selectedLang = (!empty($this->params['lng']) ? (in_array($this->params['lng'], array_keys($this->ln->tabLangues)) ? $this->params['lng'] : $this->language) : $this->language);
        $this->clTemplates = new clTemplates($this->surl, $this->ln, $this->selectedLang);

        $this->partenaires = new o\data('partenaires');
        $this->partenaires->order('nom', 'asc');
        $this->parts = array(0 => '');
        foreach ($this->partenaires as $part) {
            $this->parts[$part->id_partenaire] = $part->nom;
        }


        if (!empty($this->params[0])) {
            $this->mails_text = new o\data('mails_text', array('type' => $this->params[0]));

            if ($this->mails_text->count() > 0) {
                $this->new = false;
            } else {
                $this->redirect($this->lurl . '/mails/templates');
            }
        } else {
            $this->new = true;
        }

        if (isset($_POST['sendForm']) && $this->params[0] == $_POST['type']) {
            $_POST['value_name_' . $this->language] = trim($_POST['value_name_' . $this->language]);
            $_POST['value_exp_name_' . $this->language] = trim($_POST['value_exp_name_' . $this->language]);
            $_POST['value_exp_email_' . $this->language] = trim($_POST['value_exp_email_' . $this->language]);
            $_POST['value_subject_' . $this->language] = trim($_POST['value_subject_' . $this->language]);
            $_POST['value_content_' . $this->language] = trim($_POST['value_content_' . $this->language]);
            $this->errorNom = false;
            $this->errorNomExp = false;
            $this->errorAddrExp = false;
            $this->errorSubj = false;
            $this->errorContent = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['value_name_' . $this->language]) == 0) {
                $this->errorNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }
            if (strlen($_POST['value_exp_name_' . $this->language]) == 0) {
                $this->errorNomExp = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }
            if (strlen($_POST['value_exp_email_' . $this->language]) == 0) {
                $this->errorAddrExp = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }
            if (strlen($_POST['value_subject_' . $this->language]) == 0) {
                $this->errorSubj = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }
            if (strlen($_POST['value_content_' . $this->language]) == 0) {
                $this->errorContent = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = array();
                foreach ($this->ln->tabLangues as $key => $langue) {
                    $mails_text = new o\mails_text(array('type' => $_POST['type'], 'id_langue' => $key));
                    $isNew = ($mails_text->exist() ? false : true);
                    $backLog[$langue] = (!$isNew ? $mails_text->getArray() : '');

                    $mails_text->type = ($isNew ? generateSlug($_POST['value_name_' . $this->language]) : $mails_text->type);
                    $mails_text->id_langue = $key;
                    $mails_text->name = ($_POST['value_name_' . $key] != '' ? $_POST['value_name_' . $key] : $_POST['value_name_' . $this->language]);
                    $mails_text->exp_name = ($_POST['value_exp_name_' . $key] != '' ? $_POST['value_exp_name_' . $key] : $_POST['value_exp_name_' . $this->language]);
                    $mails_text->id_partenaire = ($_POST['value_id_partenaire_' . $key] != '' ? $_POST['value_id_partenaire_' . $key] : $_POST['value_id_partenaire_' . $this->language]);
                    $mails_text->exp_email = ($_POST['value_exp_email_' . $key] != '' ? $_POST['value_exp_email_' . $key] : $_POST['value_exp_email_' . $this->language]);
                    $mails_text->subject = cleanQuote(($_POST['value_subject_' . $key] != '' ? $_POST['value_subject_' . $key] : $_POST['value_subject_' . $this->language]));
                    $mails_text->content = cleanQuote(($_POST['value_content_' . $key] != '' ? $_POST['value_content_' . $key] : $_POST['value_content_' . $this->language]));
                    $mails_text->save();

                    if ($this->nmpEMV) {
                        if ($mails_text->id_nmp == '' || $mails_text->nmp_unique == '' || $mails_text->nmp_secure == '') {
                            $nmp = $this->nmpsoap->cloneTemplate(array(
                                'token'   => $this->token_soap,
                                'id'      => $this->id_clone_nmp,
                                'newName' => $mails_text->type . '-' . $key,
                            ));
                            $detNMP = $this->nmpsoap->getTemplate(array(
                                'token' => $this->token_soap,
                                'id'    => $nmp->return,
                            ));
                            $mails_text->id_nmp = $detNMP->return->id;
                            $mails_text->nmp_unique = $detNMP->return->random;
                            $mails_text->nmp_secure = $detNMP->return->encrypt;
                            $mails_text->save();
                        }
                        $template->id = $mails_text->id_nmp;
                        $template->description = $mails_text->name;
                        $template->name = $mails_text->type . '-' . $key;
                        $template->subject = $mails_text->subject;
                        $template->from = $mails_text->exp_name;
                        $template->fromEmail = $this->frommail_api;
                        $template->to = '';
                        $template->encoding = 'UTF-8';
                        $template->body = '[EMV HTMLPART]' . $mails_text->content;
                        $template->replyTo = $mails_text->exp_name;
                        $template->type = 'TRANSACTIONAL';
                        $template->sent = 1;
                        $template->replyToEmail = $mails_text->exp_email;
                        $this->nmpsoap->updateTemplateByObj(array(
                            'token'    => $this->token_soap,
                            'template' => $template,
                        ));
                    }

                    $this->clFonctions->logging((!$isNew ? $this->ln->txt('admin-logs', 'action-edit-tplmail', $this->language, 'Modification d\'un template de mail') : $this->ln->txt('admin-logs', 'action-add-tplmail', $this->language, 'Ajout d\'un template de mail')), $mails_text->name, serialize($backLog));
                }

                if ($this->nmpEMV) {
                    $this->nmpsoap->closeApiConnection(array('token' => $this->token_soap));
                }

                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'mails', $this->language, 'Mails'), (!$isNew ? $this->ln->txt('admin-banner', 'tplmail-edit', $this->language, 'Le template de mail a bien été modifié') : $this->ln->txt('admin-banner', 'tplmail-add', $this->language, 'Le template de mail a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/mails/formTemplate/' . $_POST['type'] . '/lng___' . $_POST['selectedLang']);
                } else {
                    $this->redirect($this->lurl . '/mails/templates');
                }
            }
        } else {
            if ($this->new) {
                $_POST = array();
            } else {
                foreach ($this->ln->tabLangues as $key => $langue) {
                    $mails_text = new o\mails_text(array('type' => $this->params[0], 'id_langue' => $key));

                    if ($mails_text->exist()) {
                        $_POST['id_textemail_' . $key] = $mails_text->id_textemail;
                        $_POST['type'] = $mails_text->type;
                        $_POST['value_name_' . $key] = $mails_text->name;
                        $_POST['value_exp_name_' . $key] = $mails_text->exp_name;
                        $_POST['value_exp_email_' . $key] = $mails_text->exp_email;
                        $_POST['value_subject_' . $key] = $mails_text->subject;
                        $_POST['value_content_' . $key] = htmlentities($mails_text->content);
                        $_POST['value_id_partenaire_' . $key] = $mails_text->id_partenaire;
                    } else {
                        $_POST['id_textemail_' . $key] = '';
                        $_POST['type'] = $this->params[0];
                        $_POST['value_name_' . $key] = '';
                        $_POST['value_exp_name_' . $key] = '';
                        $_POST['value_exp_email_' . $key] = '';
                        $_POST['value_subject_' . $key] = '';
                        $_POST['value_content_' . $key] = '';
                        $_POST['value_id_partenaire_' . $key] = '';
                    }
                }
            }
        }
    }

    /**
     * Formulaire pour renseigner les variables du mail avant envoi
     * Utilise le moteur Octeract.
     *
     * @function _previewForm
     */
    public function _previewForm()
    {
        $this->users->checkAccess('mailstemplates');
        $this->ssMenuActive = 'mailstemplates';
        $this->selectedLang = (!empty($this->params['lng']) ? (in_array($this->params['lng'], array_keys($this->ln->tabLangues)) ? $this->params['lng'] : $this->language) : $this->language);

        if (!empty($this->params[0])) {
            $this->mails_text = new o\mails_text(array('type' => $this->params[0], 'id_langue' => $this->selectedLang));
            if (!$this->mails_text->exist()) {
                $this->redirect($this->lurl . '/mails/templates');
            }
        } else {
            $this->redirect($this->lurl . '/mails/templates');
        }

        $matches = array();
        $this->vars = array();
        preg_match_all('/\[EMV DYN\].*?\[EMV \/DYN\]/is', $this->mails_text->content, $matches);
        foreach ($matches[0] as $m) {
            $var = str_replace('[EMV DYN]', '', str_replace('[EMV /DYN]', '', $m));
            if (!isset($this->clEmail->tabDefaultVars[$var]) && !in_array($var, $this->vars)) {
                $this->vars[] = $var;
            }
        }
    }

    /**
     * Affichage de la previsualisation (ou envoi) du mail
     * Utilise le moteur Octeract.
     *
     * @function _preview
     */
    public function _preview()
    {
        $this->users->checkAccess('mailstemplates');
        $this->ssMenuActive = 'mailstemplates';
        $this->autoFireNothing = true;

        if (!empty($_POST['type'])) {
            $this->mails_text = new o\mails_text(array('type' => $_POST['type'], 'id_langue' => $_POST['id_langue']));
            if (!$this->mails_text->exist()) {
                $this->redirect($this->lurl . '/mails/templates');
            }
        } else {
            $this->redirect($this->lurl . '/mails/templates');
        }

        $this->mailContent = $this->mails_text->content;
        $matches = array();
        preg_match_all('/\[EMV DYN\].*?\[EMV \/DYN\]/is', $this->mailContent, $matches);
        foreach ($matches[0] as $m) {
            $var = str_replace('[EMV DYN]', '', str_replace('[EMV /DYN]', '', $m));
            if (isset($this->clEmail->tabDefaultVars[$var])) {
                $this->mailContent = str_replace('[EMV DYN]' . $var . '[EMV /DYN]', $this->clEmail->tabDefaultVars[$var], $this->mailContent);
            } elseif (isset($_POST[$var])) {
                $this->mailContent = str_replace('[EMV DYN]' . $var . '[EMV /DYN]', $_POST[$var], $this->mailContent);
            }
        }

        if ($_POST['sendmail'] == 0) {
            echo $this->mailContent;
            die;
        } else {
            $this->clEmail->sendMailPreview($_POST['type'], $_POST['id_langue'], $_POST['email-preview'], $_POST);
            $this->redirect($this->lurl . '/mails/templates');
        }
    }

    /**
     * Affichage de la previsualisation du mail de l'historique
     * Utilise le moteur Octeract.
     *
     * @function _previewMail
     */
    public function _previewMail()
    {
        $this->users->checkAccess('mailshistory');
        $this->autoFireNothing = true;

        if (!empty($this->params[0])) {
            $this->mails_filer = new o\mails_filer(array('id_filermails' => $this->params[0]));
            if (!$this->mails_filer->exist()) {
                $this->redirect($this->lurl . '/mails/history');
            } else {
                echo stripslashes($this->mails_filer->content);
                die;
            }
        } else {
            $this->redirect($this->lurl . '/mails/history');
        }
    }

    /**
     * Gestion de l'historique des emails
     * Utilise le moteur Octeract.
     *
     * @function _history
     */
    public function _history()
    {
        ini_set('memory_limit', '4096M');
        $this->users->checkAccess('mailshistory');
        $this->menuActive = 'clients';
        $this->ssMenuActive = 'mailshistory';

        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');

        if (!empty($_GET['filtre'])) {
            $sql = 'SELECT * FROM mails_filer WHERE ("from" like "%' . $_GET['filtre'] . '%" OR email_destinataire like "%' . $_GET['filtre'] . '%" OR subject like "%' . $_GET['filtre'] . '%")  ORDER BY added DESC LIMIT 500';
        } else {
            $sql = "SELECT * FROM mails_filer WHERE 1 ORDER BY added DESC LIMIT 500";
        }

        $result = $this->bdd->query($sql);
        $list = [];

        while ($results = $this->bdd->fetch_assoc($result)) {
            $list[] = $results;
        }

        $this->mails_filer = $list;
        $this->count = count($list);

        // Pagination
        /*$this->pagination = new pagination($this->mails_filer, (empty($_GET['page']) ? 1 : $_GET['page']), 25);
        $this->list = $this->pagination->getList();*/
    }

    public function _stats()
    {
        $this->loadJS('Chart.min');
        $this->loadJS('jquery.flot');
        $this->loadJS('jquery.flot.time');
        $this->loadJS('jquery.flot.tooltip.min');
        $this->loadJs('bootstrap-datepicker');
        $this->loadCss('datepicker');
        if (isset($_POST['filter'])) {
            $_SESSION['statsMailsFrom'] = $_POST['from'];
            $_SESSION['statsMailsTo'] = $_POST['to'];
            $_SESSION['statsMailsSent'] = $_POST['sent'];
            $_SESSION['statsMailsOpened'] = $_POST['opened'];
            $_SESSION['statsMailsClicked'] = $_POST['clicked'];
            $_SESSION['statsMailsBounced'] = $_POST['bounced'];
            $_SESSION['statsMailsOrders'] = $_POST['orders'];
            $_SESSION['statsMailsCA'] = $_POST['ca'];
        } elseif (!isset($_SESSION['statsMailsFrom'])) {
            $_SESSION['statsMailsFrom'] = date('d/m/Y', strtotime('1 month ago'));
            $_SESSION['statsMailsTo'] = date('d/m/Y');
            $_SESSION['statsMailsSent'] = 1;
            $_SESSION['statsMailsOpened'] = 1;
            $_SESSION['statsMailsClicked'] = 1;
            $_SESSION['statsMailsBounced'] = 1;
            $_SESSION['statsMailsOrders'] = 1;
            $_SESSION['statsMailsCA'] = 1;
        }
        $this->email = new o\mails_text($this->params[0]);
        $this->stats = (new o\mails_filer())->getMailStats($this->params[0], $_SESSION['statsMailsFrom'], $_SESSION['statsMailsTo']);
        $this->from = strtotime(str_replace('/', '-', $_SESSION['statsMailsFrom']));
        $this->to = strtotime(str_replace('/', '-', $_SESSION['statsMailsTo']));
        if ($this->email->id_partenaire > 0) {
            $this->campStats = (new o\partenaires())->getStats($this->email->id_partenaire, $_SESSION['statsMailsFrom'], $_SESSION['statsMailsTo']);
        }
    }

}
