<?php

class rootController extends bootstrap
{

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
    }

    /**
     * Permet de maintenir la session active.
     * Utilise le moteur Octeract.
     *
     * @function _keepAlive
     */
    public function _keepAlive()
    {
        $this->autoFireNothing = true;
    }

    /**
     * Destruction de toutes les sessions puis redirection
     * vers la page de connection.
     * Utilise le moteur Octeract.
     *
     * @function _logout
     */
    public function _logout()
    {
        $this->autoFireNothing = true;
        $this->logout();
        $this->redirect($this->lurl . '/login');
    }

    /**
     * Dispatch vers la première zone de l'utilisateur ou vers les pages
     * de login / choix du site.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default()
    {
        $this->autoFireNothing = true;
        $users = new o\users($_SESSION['user']['id_user']);

        if ($users->exist()) {
            if (!empty($_SESSION['siteAdmin']) && isset($_SESSION['infosSite'])) {
                $goto = $users->getFirstZoneUser($_SESSION['user']['id_user']);
                $this->redirect($this->lurl . '/' . $goto['slug_destination']);
            } else {
                $this->redirect($this->lurl . '/choiceSite');
            }
        } else {
            $this->redirect($this->lurl . '/login');
        }
    }

    /**
     * Gestion du mot de passe oublié.
     * Utilise le moteur Octeract.
     *
     * @function _lostpassword
     */
    public function _lostpassword()
    {
        $this->autoFireHead = false;
        $this->autoFireDebug = false;
        $this->autoFireHeader = false;
        $this->autoFireFooter = false;
        $users = new o\users($_SESSION['user']['id_user']);

        if ($users->exist()) {
            $this->redirect($this->lurl);
        }

        if (isset($_POST['sendPassword'])) {
            $_POST['email'] = filter_var(trim(htmlspecialchars($_POST['email'])), FILTER_VALIDATE_EMAIL);
            $this->errorForm = false;

            if (strlen($_POST['email']) > 0 && $this->ficelle->isEmail($_POST['email'])) {
                $users = new o\users(array('email' => $_POST['email']));

                if ($users->exist()) {
                    $new_password = $this->ficelle->generatePassword();
                    $users->password = sha1(PREFIX_SALT . $new_password . SUFFIX_SALT);
                    $users->save();

                    $this->clEmail->envoyerMail('admin-identifiants-de-connexion', $this->language, $_POST['email'], array('site'     => $_SESSION['infosSite']['nom'],
                                                                                                                           'email'    => $_POST['email'],
                                                                                                                           'password' => $new_password,
                    ));
                } else {
                    $this->errorForm = true;
                    $this->errorMsg = $this->ln->txt('admin-login', 'error-user-inconnu', $this->language, 'Cette adresse email ne correspond à aucun utilisateur');
                }
            } else {
                $this->errorForm = true;
                $this->errorMsg = $this->ln->txt('admin-login', 'error-email-invalide', $this->language, 'Cette adresse email est invalide');
            }
        }
    }

    /**
     * Gestion de la page de login.
     * Utilise le moteur Octeract.
     *
     * @function _login
     */
    public function _login()
    {
        $this->autoFireHead = false;
        $this->autoFireDebug = false;
        $this->autoFireHeader = false;
        $this->autoFireFooter = false;
        $users = new o\users($_SESSION['user']['id_user']);

        if ($users->exist()) {
            $this->redirect($this->lurl);
        }

        if (isset($_POST['sendLogin'])) {
            $_POST['email'] = filter_var(trim(htmlspecialchars($_POST['email'])), FILTER_VALIDATE_EMAIL);
            $_POST['password'] = trim($_POST['password']);
            $this->errorFormEmail = false;
            $this->errorFormPassword = false;
            $this->errorFormLogin = false;
            $this->warningFormLogin = false;
            $this->errorMsg = '';
            $this->warningMsg = '';

            if (strlen($_POST['email']) > 0 && $this->ficelle->isEmail($_POST['email'])) {
                $users = new o\users(array('email' => $_POST['email']));

                if ($users->exist()) {
                    if (strlen($_POST['password']) > 0) {
                        if (password_verify($_POST['password'], $users->password)) {
                            $_SESSION['user'] = $users->getArray();
                            $users->lastlogin = date('Y-m-d H:i:s');
                            $users->save();

                            if (isset($_SESSION['user']) && $users->id_user === $_SESSION['user']['id_user']) {
                                if ($users->status > 0) {
                                    $lSitesUser = $users->selectSitesUser($_SESSION['user']['id_user']);

                                    if (count($lSitesUser) > 0) {
                                        if (count($lSitesUser) == 1) {
                                            $sites = new o\sites($lSitesUser[0]['id_site']);
                                            $infosSite = $sites->getArray();
                                            $_SESSION['siteAdmin'] = $infosSite['base'];
                                            $_SESSION['infosSite'] = $infosSite;

                                            if (!empty($_SESSION['requestUrl'])) {
                                                $this->redirect($this->url . $_SESSION['requestUrl']);
                                            } else {
                                                $this->redirect($this->lurl);
                                            }
                                        } else {
                                            $this->redirect($this->lurl . '/choiceSite');
                                        }
                                    } else {
                                        $this->warningFormLogin = true;
                                        $this->warningMsg = $this->ln->txt('admin-login', 'error-user-site', $this->language, 'Votre compte ne gère aucun site.<br>Merci de contacter l\'administrateur');
                                    }
                                } else {
                                    $this->logout();
                                    $this->warningFormLogin = true;
                                    $this->warningMsg = $this->ln->txt('admin-login', 'error-user-offline', $this->language, 'Votre compte est momentanément désactivé<br>Merci de contacter l\'administrateur du site');
                                }
                            } else {
                                $this->warningFormLogin = true;
                                $this->warningMsg = $this->ln->txt('admin-login', 'error-user-session', $this->language, 'Un problème est survenu à l\'ouverture de votre session<br>Merci de recommencer la connexion');
                            }
                        } else {
                            $this->errorFormLogin = true;
                            $_POST['email'] = '';
                            $_POST['password'] = '';
                            $this->errorMsg = $this->ln->txt('admin-login', 'error-user-badloginpass', $this->language, 'L\'adresse email et/ou le mot de passe sont invalide');
                        }
                    } else {
                        $this->errorFormPassword = true;
                        $_POST['password'] = '';
                        $this->errorMsg = $this->ln->txt('admin-login', 'error-password-invalide', $this->language, 'Vous devez indiquer un mot de passe');
                    }
                } else {
                    $this->errorFormLogin = true;
                    $_POST['email'] = '';
                    $_POST['password'] = '';
                    $this->errorMsg = $this->ln->txt('admin-login', 'error-user-badloginpass', $this->language, 'L\'adresse email et/ou le mot de passe sont invalide');
                }
            } else {
                $this->errorFormEmail = true;
                $_POST['email'] = '';
                $this->errorMsg = $this->ln->txt('admin-login', 'error-email-invalide', $this->language, 'Cette adresse email est invalide');
            }
        }
    }

    /**
     * Gestion de la page de choix du site à administrer.
     * Utilise le moteur Octeract.
     *
     * @function _choiceSite
     */
    public function _choiceSite()
    {
        $this->autoFireHead = false;
        $this->autoFireDebug = false;
        $this->autoFireHeader = false;
        $this->autoFireFooter = false;
        $users = new o\users($_SESSION['user']['id_user']);

        if (!isset($_SESSION['user']) || !$users->exist()) {
            $this->redirect($this->lurl . '/login');
        }

        if (count($this->lSitesUser) == 1) {
            $sites = new o\sites($this->lSitesUser[0]['id_site']);
            $infosSite = $sites->getArray();
            $_SESSION['siteAdmin'] = $infosSite['base'];
            $_SESSION['infosSite'] = $infosSite;

            if (!empty($_SESSION['requestUrl'])) {
                $this->redirect($this->url . $_SESSION['requestUrl']);
            } else {
                $this->redirect($this->lurl);
            }
        }

        if (isset($_POST['sendChoiceSite'])) {
            $this->errorForm = false;
            $this->errorMsg = '';

            if (strlen($_POST['id_site']) > 0) {
                $sites = new o\sites($_POST['id_site']);

                if ($sites->exist()) {
                    $infosSite = $sites->getArray();
                    $_SESSION['siteAdmin'] = $infosSite['base'];
                    $_SESSION['infosSite'] = $infosSite;

                    if (!empty($_SESSION['requestUrl'])) {
                        $this->redirect($this->url . $_SESSION['requestUrl']);
                    } else {
                        $this->redirect($this->lurl);
                    }
                } else {
                    $this->errorForm = true;
                    $this->errorMsg = $this->ln->txt('admin-choicesite', 'error-site-inexistant', $this->language, 'Le site sélectionné n\'existe pas');
                }
            } else {
                $this->errorForm = true;
                $this->errorMsg = $this->ln->txt('admin-choicesite', 'error-site-invalide', $this->language, 'Vous devez sélectionner un site');
            }
        }
    }

}
