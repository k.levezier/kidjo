<?php

use o\data;

class devboxController extends bootstrap
{

    var $Command;

    function devboxController($command, $config)
    {
        parent::__construct($command, $config, 'default');

        $this->autoFireHeader = false;
        $this->autoFireHead = false;
        $this->autoFireFooter = false;
        $this->autoFireView = false;
        $this->autoFireDebug = false;

        // Securisation des acces
        if (isset($_SERVER['REMOTE_ADDR']) && !in_array($_SERVER['REMOTE_ADDR'], $this->Config['ip_admin'][$this->Config['env']])) {
            //die;
        }

        $my_ip = "80.13.101.121";

        if ($_SERVER['REMOTE_ADDR'] != $my_ip && $_SERVER['REMOTE_ADDR'] != "::1") {
            //echo "non autorise";die;
        }

        ini_set('error_reporting', 999);

        ini_set('memory_limit', '4092M');
        ini_set('max_exec_time', '18400'); // 40mins
    }


    function _default(){
        echo "mon ctrl de dev";
        die;
    }

    function _sendmail(){
        ini_set('display_errors',1);


        $vars = [
            'meta_tile' => "Il s'agit d'un mail de TEST By Splio SMTP",
            'x-splio-ref' => $this->Config['env'] . '_id_mailing_' . generateSlug("mailtest") . '_' . date('Y-m-d')
        ];

        $vars = [
            'client_prenom_nom' => "Benoit Test 3",
            'lien' => $this->urlfront,
            'meta_tile' => "Il s'agit d'un mail de TEST By Splio SMTP",
            'x-splio-ref' => $this->Config['env'] . '_id_mailing_' . generateSlug("mailtest") . '_' . date('Y-m-d')
        ];

        $this->clEmail =
        $return = $this->clEmail->sendMail("mail-ouverture-de-compte", 'en', "b.jaugey@equinoa.com",  $vars);

        /*$vars = [
            'meta_tile' => $mails_text->subject,
            'x-splio-ref' => $this->Config['env'] . '_' . generateSlug($mails_text->type) . '_' . date('Y-m-d'),
            'surl' => $this->surl,
            'client_prenom_nom' => $_POST['client_prenom_nom'],
            'commande_id' => $_POST['commande_id'],
            'commande_date' => $_POST['commande_date'],
            'commande_lien_details' => $_POST['commande_lien_details'],
            'client_email' => $_POST['client_email'],
        ];

        $isMailSent = $this->clEmail->sendMail("confirmation-de-commande", $this->language, $_POST['email_client'], $vars);*/


        var_dump($return);
        die;
    }


    function _test_traitement_splio_SDS(){

        $this->clSplio->processFileSDS();
        echo "fin";die;
    }

    function _import_review(){

        //ALTER TABLE `produits_avis` ADD `old_id_client` INT NOT NULL AFTER `id_client`;
        //ALTER TABLE `produits_avis` CHANGE `old_id_client` `old_email_client` VARCHAR(255) NOT NULL;

        $product_reviews = (new o\data('product_reviews'))->addWhere( 'country = "FR"');
        foreach ($product_reviews as $rv) {

            //Récupération du produit
            $produit = (new o\product_translations(array('product_id' => $rv->product_id, 'locale' => "fr")));
            if($produit->exist()){

                //on check si on trouve un produit avec le même nom chez nous
                $produits_elements = (new o\data('produits_elements', array('id_element' => 7, 'id_langue' => 'fr')));
                $produits_elements->addWhere('value LIKE "%'.$produit->label.'%"');

                if($produits_elements->count() > 0){
                    $pdt = $produits_elements->current();

                    $produits_avis = (new o\produits_avis());
                    $produits_avis->id_langue = "fr";
                    $produits_avis->id_produit = $pdt->id_produit;
                    $produits_avis->old_email_client = $rv->email;
                    $produits_avis->nom = $rv->lastname;
                    $produits_avis->prenom = $rv->firstname;
                    $produits_avis->note = $rv->notation;
                    $produits_avis->avis = $rv->message;
                    $produits_avis->date = $rv->created_at;
                    $produits_avis->status = 1;
                    //print_r($produits_avis->insertQuery());die;
                    $produits_avis->insert();
                }


            }
        }
    }
}