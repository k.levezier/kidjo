<?php

use o\data;

class documentsController extends bootstrap {

    public function __construct($command, $config, $app) {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'documents';

        $this->clDocuments = new clDocuments();
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique 
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     * 
     * @function _default
     */

    function sort($field) {
        if($_SESSION['sort']==$field)
        {
            echo '<a href="#" onclick="sort(\''.$field.' '.($_SESSION['sortorder']=='asc'?'desc':'asc').'\'); return false;">'.($_SESSION['sortorder']=='desc'?'<i class="fa fa-caret-down"></i>':'<i class="fa fa-caret-up"></i>').'</a>';
        }
        else
        {
            echo '<a href="#" onclick="sort(\''.$field.' asc\'); return false;"><i class="fa fa-caret-up"></i><i class="fa fa-caret-down"></i></a>';
        }

    }


    function _archive()
    {
        $docs = explode(',',$this->params[0]);
        
        foreach($docs as $doc)
        {
            $document = new o\documents(array('id_document'=>$doc));
            $document->status = 0;
            $document->save();
        }

        $cacheFolder = $this->path . '/cache/';
        $dossier = opendir($cacheFolder);

        while ($fichier = readdir($dossier)) {
            if ($fichier != '.' && $fichier != '..' && $fichier != '.gitignore') {
                @unlink($cacheFolder . $fichier);
            }
        }

        closedir($dossier);

        header('location:/documents');
        die;
    }
    function _sort()
    {
        $or = explode(' ',urldecode($this->params[0]));
        $_SESSION['sort'] = $or[0];
        $_SESSION['sortorder'] = $or[1];
        header('location:/documents');
        die;
    }

    public function _default() {
        //error_reporting(999);
        //ini_set('display_errors','on');
        $this->users->checkAccess('documents');
        $this->ssMenuActive = 'doculiste-des-documents';
        $this->clDocuments = new clDocuments();
        $this->documents = new o\data('documents');
        if(isset($_POST['filter']))
        {
            $_SESSION['keywords'] = $_POST['keywords'];
            $_SESSION['type'] = $_POST['type'];
            $_SESSION['offline'] = $_POST['offline'];
            $_SESSION['id_user'] = $_POST['user'];
            header('location:/documents');
            die;

        }

        $this->documents->addWhere(($_SESSION['id_user']>0?'id_user='.$_SESSION['id_user'].' AND ':'').($_SESSION['offline']>0?'status in (0,1) AND ':' status=1 AND ').($_SESSION['type']>0?'id_doctype='.$_SESSION['type'].' AND':'').($_SESSION['keywords']!=''?'(label like "%'.$_SESSION['keywords'].'%" OR description like "%'.$_SESSION['keywords'].'%" OR tags like "%'.$_SESSION['keywords'].'%") AND ':'').' 1');
        //echo ($_POST['type']>0?'id_doctype='.$_POST['type'].' AND ':'').($_POST['keywords']!=''?'(label like "%'.$_POST['keywords'].'%" OR description like "%'.$_POST['keywords'].'%" OR tags like "%'.$_POST['keywords'].'%") AND ':'').' 1';

        if($_SESSION['sort']!='')
            $this->documents->order($_SESSION['sort'],strtoupper($_SESSION['sortorder']),'');
        else
            $this->documents->order('updated','DESC','');

        $this->doctypes = (new o\data('doctypes'))->order('name','ASC','');
        $this->tree = new o\tree(array('id_tree'=>0));
        $this->lUsers = new o\data('users');
        $this->lUsers->order('firstname','ASC','');
    }

    public function _deletes() {
        //error_reporting(999);
        //ini_set('display_errors','on');
        $this->users->checkAccess('documents');
        $this->ssMenuActive = 'doculiste-des-documents';
        $this->clDocuments = new clDocuments();
        $this->documents = new o\data('documents');
        if(isset($_POST['filter']))
        {
            $_SESSION['keywords'] = $_POST['keywords'];
            $_SESSION['type'] = $_POST['type'];
            $_SESSION['offline'] = $_POST['offline'];
            $_SESSION['id_user'] = $_POST['user'];
            header('location:/documents');
            die;

        }

        $this->documents->addWhere('id_document not in (select id_document from documents_tree where id_tree in (select id_tree FROM tree where menu_title="Visuals")) and (expires="0000-00-00 00:00:00")');

        //echo ($_POST['type']>0?'id_doctype='.$_POST['type'].' AND ':'').($_POST['keywords']!=''?'(label like "%'.$_POST['keywords'].'%" OR description like "%'.$_POST['keywords'].'%" OR tags like "%'.$_POST['keywords'].'%") AND ':'').' 1';

        if($_SESSION['sort']!='')
            $this->documents->order($_SESSION['sort'],strtoupper($_SESSION['sortorder']),'');
        $this->doctypes = (new o\data('doctypes'))->order('name','ASC','');
        $this->tree = new o\tree(array('id_tree'=>0));
        $this->lUsers = new o\data('users');
        $this->lUsers->order('firstname','ASC','');
    }
    
    function _where()
    {
        $this->documents = new o\data('documents');
        
        
        foreach($this->documents as $doc)
        {
            $good = true;
            $dc = new o\data('documents_tree');
            $dc->addWhere('id_tree not in (select id_parent from tree)');
            $dc->addWhere('id_document='.$doc->id_document);
            if(count($dc)==0)
            {
                $i++;
                echo $doc->id_document.' - '.$doc->name.'<br/>';
            }
            
        }
        echo '<hr/>Total : '.$i;
        die;
    }
    
    /**
     * Liste des codes promo.
     * Utilise le moteur Octeract.
     * 
     * @function _codes
     */
    public function _doctypes() {
        $this->users->checkAccess('documents');
        $this->ssMenuActive = 'docutypes-de-documents';
        $this->doctypes = new o\data('doctypes');
    }
    
     public function _geozones() {
        $this->users->checkAccess('documents');
        $this->ssMenuActive = 'docuzones';
        $this->geozones = new o\data('geozones');
    }
    
    public function _stats() {
       $this->users->checkAccess('documents');
        $this->ssMenuActive = 'docustatistiques';
        $this->logins = new o\data('logins'); 
        $this->downloads = new o\data('downloads'); 
        $this->logins->order('added','DESC','');
        $this->downloads->order('added','DESC','');
    }
    
     public function _profiles() {
        $this->users->checkAccess('documents');
        $this->ssMenuActive = 'docuprofils';
        $this->profiles = new o\data('profiles');
    }
    
    public function _addDoctype() {
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        $this->ssMenuActive = 'docutypes-de-documents';
        if (!isset($this->params[0]) || $this->params[0]=='') {
            $this->new = true;
        } else {
            $this->doctypes = new o\doctypes( array('id_doctype'=>$this->params[0]));
            if ($this->doctypes->exist()) {

            } else {
                $this->redirect($this->lurl . '/documents/doctypes');
            }
        }

        if (isset($_POST['sendForm'])) {
            $this->error = false;
            $this->errorMsg = '';

            $this->doctypes = new o\doctypes( array('id_doctype'=>$_POST['id_doctype']));


           
            
            $this->doctypes->name = $_POST['name'];


            
            $this->doctypes->save();


            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->doctypes->getArray()) : '');   
                
                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-doctypeedit', $this->language, 'Modification d\'un type de document') : $this->ln->txt('admin-logs', 'action-doctypeadd', $this->language, 'Ajout d\'un type de document')), $this->doctypes->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'documents', $this->language, 'Documents'), 
                                             ( !$this->new ? 
                                               $this->ln->txt('admin-banner', 'doctype-edit', $this->language, 'Le type de document a bien été modifié') : 
                                               $this->ln->txt('admin-banner', 'doctype-add', $this->language, 'Le type de document a bien été ajouté') ));
                
                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/documents/addDoctype/' . $this->doctypes->id_doctype.($_POST['part']!=0?'/part___'.$_POST['part']:''));
                } else {
                    $this->redirect($this->lurl . '/documents/doctypes');
                }
            }
        }
        if(!$this->new) { $_POST = $this->doctypes->getArray(); }  
    }

    function _xport()
    {
        header("Content-Type: text/plain");
        header("Content-disposition: attachment; filename=documents-".date('YmdHi').".csv");
        $docs = new o\data('documents');
        echo "id_document;id_doctype;doctype;name;label;file;description;tags;droits;id_user;user;id_tree;brand;extension;size;thumbnail;preview;language;status;newed;planned;expires;new;indexed;added;updated\r\n";
        foreach($docs as $doc)
        {
            $user = new o\users($doc->id_user);
            $doctype = new o\doctypes($doc->id_doctype);
            $tree = new o\tree(array('id_tree'=>$doc->id_tree,'id_langue'=>'en'));
            echo utf8_decode($doc->id_document.";".$doc->id_doctype.";".$doctype->name.";".$doc->name.";".$doc->label.";".$doc->file.";".$doc->description.";".$doc->tags.";".$doc->droits.";".$doc->id_user.";".$user->name.";".$doc->id_tree.";".$tree->menu_title.";".$doc->extension.";".$doc->size.";".$doc->thumbnail.";".$doc->preview.";".$doc->language.";".$doc->status.";".$doc->newed.";".$doc->planned.";".$doc->expires.";".$doc->new.";".$doc->indexed.";".$doc->added.";".$doc->updated."\r\n");
        }
        die;
    }

    public function _addGeozone() {
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        if (!isset($this->params[0]) || $this->params[0]=='') {
            $this->new = true;

        } else {
            $this->geozones = new o\geozones( array('id_geozone'=>$this->params[0]));
            if ($this->geozones->exist()) {

            } else {
                $this->redirect($this->lurl . '/documents/geozones');
            }
        }  
                if (isset($_POST['sendForm'])) {
            $this->error = false;
            $this->errorMsg = '';

            $this->geozones = new o\geozones( array('id_geozone'=>$_POST['id_geozone']));
            $this->geozones->name = $_POST['name'];
            $this->geozones->save();


            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->geozones->getArray()) : '');   
                
                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-geozoneedit', $this->language, 'Modification d\'une zone') : $this->ln->txt('admin-logs', 'action-geozoneadd', $this->language, 'Ajout d\'une zone')), $this->geozones->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'documents', $this->language, 'Documents'), 
                                             ( !$this->new ? 
                                               $this->ln->txt('admin-banner', 'geozone-edit', $this->language, 'La zone a bien été modifiée') : 
                                               $this->ln->txt('admin-banner', 'geozone-add', $this->language, 'La zone a bien été ajoutée') ));
                
                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/documents/addGeozone/' . $this->geozones->id_geozone.($_POST['part']!=0?'/part___'.$_POST['part']:''));
                } else {
                    $this->redirect($this->lurl . '/documents/geozones');
                }
            }
        }
        if(!$this->new) { $_POST = $this->geozones->getArray(); }  
    }
 
    public function _addProfile() {
    $this->loadJs('sweetalert.min');
    $this->loadCss('sweetalert');
    if (!isset($this->params[0]) || $this->params[0]=='') {
            $this->new = true;

        } else {
            $this->profiles = new o\profiles( array('id_profile'=>$this->params[0]));
            if ($this->profiles->exist()) {

            } else {
                $this->redirect($this->lurl . '/documents/profiles');
            }
        }  
                if (isset($_POST['sendForm'])) {
            $this->error = false;
            $this->errorMsg = '';

            $this->profiles = new o\profiles( array('id_profile'=>$_POST['id_profile']));
            $this->profiles->name = $_POST['name'];
            $this->profiles->save();


            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->profiles->getArray()) : '');   
                
                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-profileedit', $this->language, 'Modification d\'un profil') : $this->ln->txt('admin-logs', 'action-profileadd', $this->language, 'Ajout d\'un profil')), $this->profiles->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'documents', $this->language, 'Documents'), 
                                             ( !$this->new ? 
                                               $this->ln->txt('admin-banner', 'profile-edit', $this->language, 'Le profil a bien été modifié') : 
                                               $this->ln->txt('admin-banner', 'profile-add', $this->language, 'Le profil a bien été ajouté') ));
                
                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/documents/addProfile/' . $this->profiles->id_profile.($_POST['part']!=0?'/part___'.$_POST['part']:''));
                } else {
                    $this->redirect($this->lurl . '/documents/profiles');
                }
            }
        }
        if(!$this->new) { $_POST = $this->profiles->getArray(); }  
    } 
    
    function _import()
    {
        $csv = file_get_contents($this->path.'tmp/import.csv');
        $lines = explode("\r\n",$csv);
        foreach($lines as $line)
        {
            $line = utf8_encode($line);
            $doc = explode(';',$line);
            print_r($doc);
            $this->documents = new o\documents(array('id_document'=>0));
            $this->documents->name = $doc[0];
            $this->documents->file = $doc[0];
            $this->documents->label = $doc[1];
            $this->documents->description = $doc[3];
            $this->documents->id_doctype = $doc[2];
            $this->documents->tags = $doc[4];
            $format = 'd/m/Y';
            $expires = DateTime::createFromFormat($format, $doc[6]);
            $this->documents->expires = $expires!=false ? $expires->format('Y-m-d H:i:00'):'';
            
            $planned = DateTime::createFromFormat($format, $doc[5]);
            $this->documents->planned = $planned!=false ? $planned->format('Y-m-d H:i:00'):'';

            $this->documents->status = $doc[7];
            $this->documents->new = $doc[11];
            $this->documents->id_user = $_SESSION['user']['id_user'];
            $this->documents->indexed = 0;
            $this->documents->save();

            $this->clDocuments->deleteArchive($this->documents->id_document);

            $documents_geozones = new o\documents_geozones();
            $documents_geozones->id_document = $this->documents->id_document;
            $documents_geozones->id_geozone = $doc[8];
            $documents_geozones->insert();
            $documents_profiles = new o\documents_profiles();
            $documents_profiles->id_document = $this->documents->id_document;
            $documents_profiles->id_profile = $doc[9];
            $documents_profiles->insert();
            $documents_tree= new o\documents_tree();
            $documents_tree->id_document = $this->documents->id_document;
            $documents_tree->id_tree = $doc[10];
            $documents_tree->insert();
        }


        $cacheFolder = $this->path . '/cache/';
        $dossier = opendir($cacheFolder);

        while ($fichier = readdir($dossier)) {
            if ($fichier != '.' && $fichier != '..' && $fichier != '.gitignore') {
                @unlink($cacheFolder . $fichier);
            }
        }

        closedir($dossier);

        die;
    }
    

    
    /**
     * Gestion de l'ajout/édition d'un document.
     * Utilise le moteur Octeract.
     * 
     * @function _add
     */
    public function _add() {
        $this->users->checkAccess('docunouveau-document');
        $this->ssMenuActive = 'docunouveau-document';
        
        $this->loadJs('jquery.datetimepicker.full.min');
        $this->loadCss('jquery.datetimepicker');
        
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        
        $this->loadJs('icheck.min');
        $this->loadCss('icheck');
        
        $this->loadJs('chosen.jquery');
        $this->loadCss('chosen');
        
        $this->loadJs('dropzone/dropzone');
        $this->loadCss('dropzone/basic');
        $this->loadCss('dropzone/dropzone');
        
        $this->new = false;
        $this->doctypes = new o\data('doctypes');
        
        $this->languages = new o\data('languages');
        $this->languages->order('name','ASC','');

        if (!isset($this->params[0]) || $this->params[0]=='') {
            $this->new = true;
            $selected=array();
            if(isset($this->params['page']))
                $selected[] = $this->params['page'];
        } else {
            $this->documents = new o\documents( array('id_document'=>$this->params[0]));
            if ($this->documents->exist()) {
            $this->documents_tree = new o\data('documents_tree', array('id_document'=>$this->params[0]));
            foreach($this->documents_tree as $tree)
                $selected[] = $tree['id_tree'];
            } else {
                $this->redirect($this->lurl . '/documents');
            }
        }
        $this->geozones = new o\data('geozones');
        $this->profiles = new o\data('profiles');  
        //$this->geozones->order('name','ASC','');
        //$this->profiles->order('name','ASC','');

        if ($this->params[1] == "delete" && in_array($this->params[2],['en','fr','es'])) {
            $name = "name_".$this->params[2];
            $file = "file_".$this->params[2];
            $extension = "extension_".$this->params[2];
            $size = "size_".$this->params[2];
            $thumbnail = "thumbnail_".$this->params[2];
            $preview = "preview_".$this->params[2];


            if(file_exists(dirname(dirname(getcwd())) . '/protected/documents/'.$this->documents->$file))
                unlink(dirname(dirname(getcwd())) . '/protected/documents/'.$this->documents->$file);
            if(file_exists(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail))
                unlink(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
            if(file_exists(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$this->documents->$preview))
                unlink(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$this->documents->$preview);

            $this->documents->$name = "";
            $this->documents->$file = "";
            $this->documents->$extension = "";
            $this->documents->$size = "";
            $this->documents->$thumbnail = "";
            $this->documents->$preview = "";
            $this->documents->save();

            $this->clDocuments->deleteArchive($this->documents->id_document);

            $this->redirect($this->lurl . '/documents/add/'.$this->documents->id_document);
            die;

        }elseif (isset($_POST['sendForm'])) {
            $this->error = false;
            $this->errorMsg = '';

            $this->documents = new o\documents( array('id_document'=>$_POST['id_document']));
            $this->documents_geozones = new o\data('documents_geozones', array('id_document'=>$_POST['id_document']));
            $this->documents_profiles = new o\data('documents_profiles', array('id_document'=>$_POST['id_document']));
            $this->documents_tree = new o\data('documents_tree', array('id_document'=>$_POST['id_document']));
            // Dates
            $format = 'd/m/Y H:i';
            $expires = DateTime::createFromFormat($format, $_POST['expires']);

            $this->documents->expires = $expires!=false ? $expires->format('Y-m-d H:i:00'):'';

            $planned = DateTime::createFromFormat($format, $_POST['planned']);
            $this->documents->planned = $planned!=false ? $planned->format('Y-m-d H:i:00'):'';

            $this->documents->label = $_POST['label'];
            $this->documents->description = $_POST['description'];
            $this->documents->tags = $_POST['tags'];
            $this->documents->status = $_POST['status'];
            $this->documents->id_doctype = $_POST['id_doctype'];
            $this->documents->language = $_POST['language'];
            if($this->new && $_POST['new']==1)
                $this->documents->newed = date('Y-m-d H:i:s');
            if(!$this->new && $_POST['new']==1 && $this->documents->new==0)
                $this->documents->newed = date('Y-m-d H:i:s');
            if($_POST['new']==0)
                $this->documents->newed = '0000-00-00 00:00:00';
            $this->documents->new = $_POST['new'];
            $this->documents->noshare = (int)$_POST['noshare'];
            $this->documents->droits = $_POST['droits'];
            if (!empty($_FILES) && $_FILES['preview']['name']!='') {
                $tempFile = $_FILES['preview']['tmp_name'];          //3

                $targetPath = $this->path.'protected/documents/previews/';  //4
                $fn = mktime().'-'.$_FILES['preview']['name'];
                $targetFile =  $targetPath. $fn;  //5

                move_uploaded_file($tempFile,$targetFile); //6
                $this->documents->preview = $fn;
                $im = new imagick($this->path.'protected/documents/previews/'.$fn.'[0]');
                $im->setImageFormat('jpg');
                $im->scaleImage(200,200,true);
                $im = $im->flattenImages();
                //header('Content-Type: image/jpeg');
                $im->writeImage($this->path.'protected/documents/thumbs/'.$fn.'-thumb.jpg');;
                $this->documents->thumbnail = $fn.'-thumb.jpg';
            }

            $this->documents->indexed = 0;
            $this->documents->save();

            $this->clDocuments->deleteArchive($this->documents->id_document);

            $this->documents_geozones->delete();
            $this->documents_profiles->delete();
            $this->documents_tree->delete();
            if(!empty($this->geozones)) {
                foreach ($this->geozones as $zone) {
                    if ($_POST['zone' . $zone->id_geozone] == 1) {
                        $documents_geozones = new o\documents_geozones();
                        $documents_geozones->id_document = $_POST['id_document'];
                        $documents_geozones->id_geozone = $zone->id_geozone;
                        $documents_geozones->insert();
                    }
                }
            }

            if(!empty($this->profiles)) {
                foreach ($this->profiles as $profile) {
                    if ($_POST['profile' . $profile->id_profile] == 1) {
                        $documents_profiles = new o\documents_profiles();
                        $documents_profiles->id_document = $_POST['id_document'];
                        $documents_profiles->id_profile = $profile->id_profile;
                        $documents_profiles->insert();
                    }
                }
            }

            if(!empty($_POST['liste_tree'])){
                foreach($_POST['liste_tree'] as $tree)
                {
                        $documents_tree= new o\documents_tree();
                        $documents_tree->id_document = $_POST['id_document'];
                        $documents_tree->id_tree = $tree;
                        $documents_tree->insert();
                }
            }

            if (!$this->error) {

                $cacheFolder = $this->path . '/cache/';
                $dossier = opendir($cacheFolder);

                while ($fichier = readdir($dossier)) {
                    if ($fichier != '.' && $fichier != '..' && $fichier != '.gitignore') {
                        @unlink($cacheFolder . $fichier);
                    }
                }

                closedir($dossier);


                $backLog = (!$this->new ? serialize($this->documents->getArray()) : '');

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-docedit', $this->language, 'Modification d\'un document') : $this->ln->txt('admin-logs', 'action-docadd', $this->language, 'Ajout d\'un document')), $this->documents->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'documents', $this->language, 'Documents'),
                                             ( !$this->new ?
                                               $this->ln->txt('admin-banner', 'doc-edit', $this->language, 'Le document a bien été modifié') :
                                               $this->ln->txt('admin-banner', 'doc-add', $this->language, 'Le document a bien été ajouté') ));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/documents/add/' . $this->documents->id_document.($_POST['part']!=0?'/part___'.$_POST['part']:''));
                } else {
                    $this->redirect($this->lurl . '/documents');
                }
            }
        }
        
        if(!$this->new) { $_POST = $this->documents->getArray(); }

        if(isset($_POST['expires']) && $_POST['expires'] != '0000-00-00 00:00:00'){
            $_POST['expires'] = new DateTime($_POST['expires']);
            $_POST['expires'] = $_POST['expires']->format(str_replace(array('yyyy', 'yy', 'dd', 'mm'),array('Y','y','d','m'),$this->clSettings->getParam('format-datepicker', 'sets')).' H:i');
        }
        else{
            //$_POST['expires'] = new DateTime(date('Y-m-d H:i:s'));
            //$_POST['expires'] = $_POST['expires']->format(str_replace(array('yyyy', 'yy', 'dd', 'mm'),array('Y','y','d','m'),$this->clSettings->getParam('format-datepicker', 'sets')).' H:i');
        }
        
        if(isset($_POST['planned']) && $_POST['planned'] != '0000-00-00 00:00:00'){
            $_POST['planned'] = new DateTime($_POST['planned']);
            $_POST['planned'] = $_POST['planned']->format(str_replace(array('yyyy', 'yy', 'dd', 'mm'),array('Y','y','d','m'),$this->clSettings->getParam('format-datepicker', 'sets')).' H:i');
        }
        else{
            $_POST['planned'] = new DateTime(date('Y-m-d H:i:s'));
            $_POST['planned'] = $_POST['planned']->format(str_replace(array('yyyy', 'yy', 'dd', 'mm'),array('Y','y','d','m'),$this->clSettings->getParam('format-datepicker', 'sets')).' H:i');
        }
        $this->documentZones = array();
        $this->documentProfiles = array();
        if(!$this->new)
        {
        $this->cg = (new o\data('documents_geozones'))->addWhere('id_document='.$this->params[0]);
        $this->cp = (new o\data('documents_profiles'))->addWhere('id_document='.$this->params[0]);
        
        foreach($this->cg as $g)
            $this->documentZones[] = $g->id_geozone;
        
        foreach($this->cp as $p)
            $this->documentProfiles[] = $p->id_profile;
        }
        
        $this->clTemplates = new clTemplates();
        $tree = new o\tree();
        //$lPages = $tree->getArboSite($this->language, 0, 0);
        $lPages = $tree->getArboSite($this->language, null, 0);
        $this->arbo = $this->clTemplates->getArboSelectForDocs($lPages,$selected);

        // On va récupérer les fichiers qui sont dans le pipe des fichiers lourd

        $directory = $this->path.'/protected/documents/tmp';
        $scanned_directory = array_diff(scandir($directory), array('..', '.'));
        $this->filesTemp = [];
        foreach($scanned_directory as $file){
            if(is_dir($directory.'/'.$file)){
                $scanned_sous_directory = array_diff(scandir($directory.'/'.$file), array('..', '.'));
                foreach($scanned_sous_directory as $child){
                    if(!is_dir($file)){
                        $this->filesTemp[$file][$child] = $child;
                    }
                }
            }else{
                $this->filesTemp[$file] = $file;
            }
        }
    }

    public function _upload()
    {
        $this->clDocuments = new clDocuments();
        $id_doc = $this->clDocuments->handleUpload($this->path,$this->params);

        if(empty($this->params[1]) || (!empty($this->params[1]) && $this->params[1]=="ftp")){
            // On ajoute les zone et les profils
            $this->geozones = new o\data('geozones');
            $this->geozones->order('name','ASC','');
            foreach ($this->geozones as $zone) {
                $documents_geozones = new o\documents_geozones();
                $documents_geozones->id_document = $id_doc;
                $documents_geozones->id_geozone = $zone->id_geozone;
                $documents_geozones->insert();
            }

            $this->profiles = new o\data('profiles');
            $this->profiles->order('name','ASC','');
            foreach ($this->profiles as $profile) {
                $documents_profiles = new o\documents_profiles();
                $documents_profiles->id_document = $id_doc;
                $documents_profiles->id_profile = $profile->id_profile;
                $documents_profiles->insert();
            }
        }
        echo $id_doc;
        die;
    }
    
    public function _delete()
    {
        $this->autoFireNothing = true;
        $doc = new o\documents(array('id_document' => $this->params[0]));
        if ($doc->exist() > 0) {
            $backLog = serialize($doc->getArray());
            $nom = $doc->name;
            unlink($this->path.'protected/documents/'.$doc->file);
            $doc->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-document-delete', $this->language, 'Supression d\'un document'), $nom, $backLog);

            $cacheFolder = $this->path . '/cache/';
            $dossier = opendir($cacheFolder);

            while ($fichier = readdir($dossier)) {
                if ($fichier != '.' && $fichier != '..' && $fichier != '.gitignore') {
                    @unlink($cacheFolder . $fichier);
                }
            }

            closedir($dossier);
        }
        $this->redirect($this->lurl . '/documents');
    }
    
    public function _deletedoctype()
    {
        $this->autoFireNothing = true;
        $doc = new o\doctypes(array('id_doctype' => $this->params[0]));
        if ($doc->exist() > 0) {
            $backLog = serialize($doc->getArray());
            $nom = $doc->name;

            $doc->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-doctype-delete', $this->language, 'Supression d\'un type de document'), $nom, $backLog);
        }
        $this->redirect($this->lurl . '/documents/doctypes');
    }
    
    public function _deletegeozone()
    {
        $this->autoFireNothing = true;
        $doc = new o\geozones(array('id_geozone' => $this->params[0]));
        if ($doc->exist() > 0) {
            $backLog = serialize($doc->getArray());
            $nom = $doc->name;

            $doc->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-geozone-delete', $this->language, 'Supression d\'une zone'), $nom, $backLog);
        }
        $this->redirect($this->lurl . '/documents/geozones');
    }
    
    public function _deleteprofile()
    {
        $this->autoFireNothing = true;
        $doc = new o\profiles(array('id_profile' => $this->params[0]));
        if ($doc->exist() > 0) {
            $backLog = serialize($doc->getArray());
            $nom = $doc->name;

            $doc->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-profile-delete', $this->language, 'Supression d\'un profil'), $nom, $backLog);
        }
        $this->redirect($this->lurl . '/documents/profiles');
    }
}
