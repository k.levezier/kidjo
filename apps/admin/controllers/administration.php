<?php

class administrationController extends bootstrap
{
    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'administration';
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default()
    {
        $this->users->checkAccess('administration');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('administration'));
    }

    /**
     * Gestion de la liste des sites.
     * Utilise le moteur Octeract.
     *
     * @function _sites
     */
    public function _sites()
    {
        $this->users->checkAccess('adminsites');
        $this->ssMenuActive = 'adminsites';
        $this->loadJs('jquery.nestable');
    }

    /**
     * Gestion du formulaire ajout/edition sites.
     * Utilise le moteur Octeract.
     *
     * @function _formSite
     */
    public function _formSite()
    {
        $this->users->checkAccess('adminsites');
        $this->ssMenuActive = 'adminsites';
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        $this->sites = new o\sites($this->params[0]);
        $this->newSite = ($this->sites->exist() ? false : true);

        if (isset($_POST['sendFormSite']) && $this->params[0] == $_POST['id_site']) {
            $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
            $_POST['app'] = trim(htmlspecialchars($_POST['app']));
            $_POST['base'] = trim(htmlspecialchars($_POST['base']));
            $_POST['url'] = trim(htmlspecialchars($_POST['url']));
            $this->errorFormNom = false;
            $this->errorFormApp = false;
            $this->errorFormBase = false;
            $this->errorFormUrl = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['nom']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['app']) == 0) {
                $this->errorFormApp = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['base']) == 0) {
                $this->errorFormBase = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['url']) == 0) {
                $this->errorFormUrl = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->newSite ? serialize($this->sites->getArray()) : '');

                if (!$this->newSite && !$this->users->checkUserSite($_SESSION['user']['id_user'], $_POST['id_site'])) {
                    $this->redirect($this->lurl . '/administration/sites');
                }

                $this->sites->nom = $_POST['nom'];
                $this->sites->app = $_POST['app'];
                $this->sites->base = $_POST['base'];
                $this->sites->url = $_POST['url'];
                $this->sites->save();

                if ($this->newSite) {
                    $this->users->addSiteToUser($this->sites->id_site, $_SESSION['user']['id_user']);
                }

                $this->clFonctions->logging($this->ln->txt('admin-logs', (!$this->newSite ? 'action-editsite' : 'action-addsite'), $this->language, (!$this->newSite ? 'Modification d\'un site' : 'Ajout d\'un site')), $this->sites->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-site', $this->language, 'Gestion des sites'), $this->ln->txt('admin-banner', (!$this->newSite ? 'edit-site' : 'add-site'), $this->language, (!$this->newSite ? 'Le site a bien été modifié' : 'Le site a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/administration/formSite/' . $this->sites->id_site);
                } else {
                    $this->redirect($this->lurl . '/administration/sites');
                }
            }
        } else {
            if (!$this->newSite && !$this->users->checkUserSite($_SESSION['user']['id_user'], $this->params[0])) {
                $this->redirect($this->lurl . '/administration/sites');
            }
            $_POST = (!$this->newSite ? $this->sites->getArray() : array());
        }
    }

    /**
     * Gestion de la suppression d'un site.
     * Utilise le moteur Octeract.
     *
     * @function _deleteSite
     */
    public function _deleteSite()
    {
        $this->users->checkAccess('adminsites');
        $this->autoFireNothing = true;

        if (!empty($this->params[0])) {
            $sites = new o\sites($this->params[0]);

            if ($sites->exist() && $this->users->checkUserSite($_SESSION['user']['id_user'], $this->params[0])) {
                $backLog = $this->ln->txt('admin-logs', 'backlog-site', $this->language, 'Site') . ' : ' . serialize($sites->getArray());
                (new o\data('users_sites', array('id_site' => $this->params[0])))->delete();
                $sites->delete();

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-deletesites', $this->language, 'Supression d\'un  site'), $sites->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-site', $this->language, 'Gestion des sites'), $this->ln->txt('admin-banner', 'del-site', $this->language, 'Le site a bien été supprimé'));
                $this->redirect($this->lurl . '/administration/sites');
            } else {
                $this->redirect($this->lurl . '/administration/sites');
            }
        } else {
            $this->redirect($this->lurl . '/administration/sites');
        }
    }

    /**
     * Gestion de l'ordre des sites.
     * Utilise le moteur Octeract.
     *
     * @function _orderSites
     */
    public function _orderSites()
    {
        $this->users->checkAccess('adminsites');
        $this->autoFireNothing = true;

        if (!empty($_POST['orderSites'])) {
            $lSites = json_decode($_POST['orderSites'], true);

            foreach ($lSites as $key => $val) {
                $users_sites = new o\users_sites($val['id']);
                $users_sites->ordre = ($key + 1);
                $users_sites->update();
            }

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-ordersites', $this->language, 'Ordonnancement des sites'), serialize($lSites));
        }
    }

    /**
     * Gestion de la liste des zones du BO.
     * Utilise le moteur Octeract.
     *
     * @function _zones
     */
    public function _zones()
    {
        $this->users->checkAccess('adminzones');
        $this->ssMenuActive = 'adminzones';
        $this->loadJs('jquery.nestable');
    }

    /**
     * Gestion du formulaire ajout/edition zones.
     * Utilise le moteur Octeract.
     *
     * @function _formZone
     */
    public function _formZone()
    {
        $this->users->checkAccess('adminzones');
        $this->ssMenuActive = 'adminzones';
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->lParents = new o\data('zones', array('id_parent' => null));
        $this->zones = new o\zones($this->params[0]);
        $this->newZone = ($this->zones->exist() ? false : true);

        if (isset($_POST['sendFormZone']) && $this->params[0] == $_POST['id_zone']) {
            $_POST['name'] = trim(htmlspecialchars($_POST['name']));
            $_POST['slug_destination'] = trim(htmlspecialchars($_POST['slug_destination']));
            $this->errorFormName = false;
            $this->errorFormDestination = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['name']) == 0) {
                $this->errorFormName = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['slug_destination']) == 0) {
                $this->errorFormDestination = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->newZone ? serialize($this->zones->getArray()) : '');

                if (!$this->newZone && !$this->users->checkUserZone($_SESSION['user']['id_user'], $_POST['id_zone'])) {
                    $this->redirect($this->lurl . '/administration/zones');
                }

                $this->zones->id_parent = ($_POST['id_parent'] == 0 ? null : $_POST['id_parent']);
                $this->zones->name = $_POST['name'];
                $this->zones->slug_destination = $_POST['slug_destination'];
                $this->zones->css = $_POST['css'];
                $this->zones->recherche = (isset($_POST['recherche']) ? $_POST['recherche'] : 0);
                $this->zones->checksecure = (!$this->newZone ? $this->zones->checksecure : $this->zones->generateCheckSecure($this->zones->name, $this->zones->id_parent));
                $this->zones->save();

                if ($this->newZone) {
                    $this->users->addZoneToUser($this->zones->id_zone, $_SESSION['user']['id_user']);
                }

                $this->clFonctions->logging($this->ln->txt('admin-logs', (!$this->newZone ? 'action-editzone' : 'action-addzone'), $this->language, (!$this->newZone ? 'Modification d\'une zone' : 'Ajout d\'une zone')), $this->zones->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-zone', $this->language, 'Gestion des zones'), $this->ln->txt('admin-banner', (!$this->newZone ? 'edit-zone' : 'add-zone'), $this->language, (!$this->newZone ? 'La zone a bien été modifiée' : 'La zone a bien été ajoutée')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/administration/formZone/' . $this->zones->id_zone);
                } else {
                    $this->redirect($this->lurl . '/administration/zones');
                }
            }
        } else {
            if (!$this->newZone && !$this->users->checkUserZone($_SESSION['user']['id_user'], $this->zones->id_zone)) {
                $this->redirect($this->lurl . '/administration/zones');
            }
            $_POST = (!$this->newZone ? $this->zones->getArray() : array());
        }
    }

    /**
     * Gestion de la suppression d'une zone.
     * Utilise le moteur Octeract.
     *
     * @function _deleteZone
     */
    public function _deleteZone()
    {
        $this->users->checkAccess('adminzones');
        $this->autoFireNothing = true;
        $zones = new o\zones($this->params[0]);

        if (!empty($this->params[0])) {
            $zones = new o\zones($this->params[0]);

            if ($zones->exist() && $this->users->checkUserZone($_SESSION['user']['id_user'], $this->params[0])) {
                $backLog = $this->ln->txt('admin-logs', 'backlog-zone', $this->language, 'Zone') . ' : ' . serialize($zones->getArray()) . (count($zones->enfants->getArray()) > 0 ? ' / ' . $this->ln->txt('admin-logs', 'backlog-childs', $this->language, 'Enfants') . ' : ' . serialize($zones->enfants->getArray()) : '');
                (new o\data('users_zones', array('id_zone' => $this->params[0])))->delete();
                foreach ($zones->enfants as $childzone) {
                    (new o\data('users_zones', array('id_zone' => $childzone->id_zone)))->delete();
                }
                $zones->enfants->delete();
                $zones->delete();
                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-deletezones', $this->language, 'Supression d\'une  zone'), $zones->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-zone', $this->language, 'Gestion des zones'), $this->ln->txt('admin-banner', 'del-zone', $this->language, 'La zone a bien été supprimée'));
                $this->redirect($this->lurl . '/administration/zones');
            } else {
                $this->redirect($this->lurl . '/administration/zones');
            }
        } else {
            $this->redirect($this->lurl . '/administration/zones');
        }
    }

    /**
     * Gestion de l'ordre des zones.
     * Utilise le moteur Octeract.
     *
     * @function _orderZones
     */
    public function _orderZones()
    {
        $this->users->checkAccess('adminzones');
        $this->autoFireNothing = true;

        if (!empty($_POST['orderZones'])) {
            $lZones = json_decode($_POST['orderZones'], true);

            foreach ($lZones as $key => $val) {
                $users_zones = new o\users_zones($val['id']);
                $users_zones->ordre = ($key + 1);
                $users_zones->update();
            }

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-orderzones', $this->language, 'Ordonnancement des zones'), serialize($lZones));
        }
    }

    /**
     * Gestion de la liste des administrateur du BO.
     *
     * @function _users
     */
    public function _users()
    {
        $this->users->checkAccess('adminusers');
        $this->ssMenuActive = 'adminusers';

        if (!empty($_POST['id_site'])) {
            $lIDusers = $this->users->getListeUserForSite($_POST['id_site']);
        }

        $this->users_sites = new o\data('users_sites');
        $this->users_sites->join('users');
        $this->users_sites->addWhere('1 = 1' .
            ($_SESSION['user']['status'] < 2 ? ' AND `users`.`status` < 2' : '') .
            (!empty($_POST['id_site']) ? ' AND `users_sites`.`id_user` IN (' . implode(',', $lIDusers) . ')' : '') .
            (!empty($_POST['filtre']) ? ' AND (`users`.`name` LIKE "%' . $_POST['filtre'] . '%" OR `users`.`firstname` LIKE "%' . $_POST['filtre'] . '%" OR `users`.`email` LIKE "%' . $_POST['filtre'] . '%")' : ''));

        if (!empty($this->params['changeStatus']) && isset($this->params['newStatus'])) {
            $users = new o\users(array('hash' => $this->params['changeStatus']));

            if ($users->exist() && $users->status < 2 && $users->id_user != $_SESSION['user']['id_user']) {
                $backLog = serialize($users->getArray());
                $users->status = $this->params['newStatus'];
                $users->update();

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-newstatus', $this->language, 'User nouveau statut'), $this->params['newStatus'], $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-users', $this->language, 'Gestion des administrateurs'), $this->ln->txt('admin-banner', 'statut-user', $this->language, 'Le statut a bien été modifié'));
                $this->redirect($this->lurl . '/administration/users');
            }
        }
    }

    /**
     * Gestion du formulaire ajout/edition user.
     * Utilise le moteur Octeract.
     *
     * @function _formUser
     */
    public function _formUser()
    {
        $this->users->checkAccess('adminusers');
        $this->ssMenuActive = 'adminusers';
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadJs('chosen.jquery');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->loadCss('chosen');
        $this->clTemplates = new clTemplates($this->surl, $this->ln, $this->language);
        $tree = new o\tree();
        $this->lPages = $tree->getArboSite($this->language, null, 0);
        $this->lSites = (new o\data('sites'))->addWhere('`app` != "" AND `base` != ""');
        $this->lZones = new o\data('zones', array('id_parent' => null));
        $this->uform = new o\users(array('hash' => $this->params[0]));
        $this->newUser = ($this->uform->exist() ? false : true);

        if (isset($_POST['sendFormUser']) && $this->params[0] == $_POST['hash']) {
            $_POST['name'] = trim(htmlspecialchars($_POST['name']));
            $_POST['firstname'] = trim(htmlspecialchars($_POST['firstname']));
            $_POST['email'] = filter_var(trim(htmlspecialchars($_POST['email'])), FILTER_VALIDATE_EMAIL);
            $_POST['password'] = trim(strip_tags($_POST['password']));
            $this->errorFormNom = false;
            $this->errorFormPrenom = false;
            $this->errorFormEmail = false;
            $this->errorFormPassword = false;
            $this->errorFormSites = false;
            $this->error = false;
            $this->errorMsg = '';
            $newMail = false;

            if (strlen($_POST['name']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['firstname']) == 0) {
                $this->errorFormPrenom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['email']) == 0 || !$this->ficelle->isEmail($_POST['email'])) {
                $this->errorFormEmail = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif ($this->newUser && (new o\data('users', array('email' => $_POST['email'])))->count() > 0) {
                $this->errorFormEmail = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-email-deja', $this->language, 'Cette adresse existe déjà');
            } elseif ($this->newUser && strlen($_POST['password']) == 0) {
                $this->errorFormPassword = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            } elseif (!is_array($_POST['id_site']) || count($_POST['id_site']) == 0) {
                $_POST['id_site'] = array();
                $this->errorFormSites = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->newUser ? serialize($this->uform->getArray()) : '');
                $newMail = (!$this->newUser ? ($this->uform->email != $_POST['email'] ? true : $newMail) : $newMail);

                $this->uform->firstname = $_POST['firstname'];
                $this->uform->name = $_POST['name'];
                $this->uform->email = $_POST['email'];
                $this->uform->password = ($this->newUser ? password_hash($_POST['password'], PASSWORD_DEFAULT) : (strlen($_POST['password']) > 0 ? password_hash($_POST['password'], PASSWORD_DEFAULT) : $this->uform->password));
                $this->uform->id_tree = ($_POST['id_tree'] == '0' ? null : $_POST['id_tree']);
                $this->uform->debug = (isset($_POST['debug']) ? $_POST['debug'] : 0);
                $this->uform->status = $_POST['status'];
                $this->uform->status = ($this->newUser ? 0 : $this->uform->status);
                $this->uform->save();

                (new o\data('users_sites', array('id_user' => $this->uform->id_user)))->delete();
                foreach ($_POST['id_site'] as $id_site) {
                    $this->users->addSiteToUser($id_site, $this->uform->id_user);
                }

                if ($this->newUser || (!$this->newUser && (strlen($_POST['password']) > 0 || $newMail))) {
                    $this->clEmail->envoyerMail('admin-identifiants-de-connexion', $this->language, $this->uform->email, array('site'     => $_SESSION['infosSite']['nom'],
                                                                                                                               'email'    => $this->uform->email,
                                                                                                                               'password' => ($this->newUser ? $_POST['password'] : (strlen($_POST['password']) > 0 ? $_POST['password'] : $this->ln->txt('admin-generic', 'nochangepassword', $this->language, 'Mot de passe inchangé'))),
                    ));
                }

                $this->clFonctions->logging($this->ln->txt('admin-logs', (!$this->newUser ? 'action-edituser' : 'action-adduser'), $this->language, (!$this->newUser ? 'Modification d\'un user' : 'Ajout d\'un user')), $this->uform->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-user', $this->language, 'Gestion des administrateurs'), $this->ln->txt('admin-banner', (!$this->newUser ? 'edit-user' : 'add-user'), $this->language, (!$this->newUser ? 'L\'administrateur a bien été modifié' : 'L\'administrateur a bien été ajouté')));

                if ($newMail && $_SESSION['user']['id_user'] == $this->uform->id_user) {
                    $this->logout();
                    $this->redirect($this->lurl . '/login');
                }

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/administration/formUser/' . $this->uform->hash);
                } else {
                    $this->redirect($this->lurl . '/administration/users');
                }
            }
        } else {
            $_POST = (!$this->newUser ? $this->uform->getArray() : array());
            $_POST['id_site'] = (!$this->newUser ? $this->users->selectIDSitesUser($this->uform->id_user) : array());
            $_POST['id_zone'] = (!$this->newUser ? $this->users->selectIDZonesUser($this->uform->id_user) : array());
        }
    }

    /**
     * Gestion des zones de l'administrateur.
     * Utilise le moteur Octeract.
     *
     * @function _userZones
     */
    public function _userZones()
    {
        $this->users->checkAccess('adminusers');
        $this->autoFireNothing = true;

        if (!empty($_POST['id_zone']) && !empty($_POST['id_user'])) {
            $users_zones = new o\users_zones(array('id_zone' => $_POST['id_zone'], 'id_user' => $_POST['id_user']));

            if ($users_zones->exist()) {
                $users_zones->delete();

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-userzonesdel', $this->language, 'Suppression de la zone'), 'User : ' . $_POST['id_user'] . ' / Zone : ' . $_POST['id_zone']);
            } else {
                $this->users->addZoneToUser($_POST['id_zone'], $_POST['id_user']);
                $zones = new o\zones($_POST['id_zone']);

                if ($zones->exist() && $zones->id_parent !== null) {
                    $this->users->addZoneToUser($zones->id_parent, $_POST['id_user']);
                }

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-userzonesadd', $this->language, 'Ajout de la zone'), 'User : ' . $_POST['id_user'] . ' / Zone : ' . $_POST['id_zone']);
            }
        }
    }

    /**
     * Gestion de la suppression des utilisateurs.
     * Utilise le moteur Octeract.
     *
     * @function _deleteUser
     */
    public function _deleteUser()
    {
        $this->users->checkAccess('adminusers');
        $this->autoFireNothing = true;

        if (!empty($this->params[0])) {
            $users = new o\users(array('hash' => $this->params[0]));

            if ($users->exist() && $users->status < 2 && $users->id_user != $_SESSION['user']['id_user']) {
                $backLog = $this->ln->txt('admin-logs', 'backlog-user', $this->language, 'Administrateur') . ' : ' . serialize($users->getArray());
                (new o\data('users_zones', array('id_user' => $users->id_user)))->delete();
                (new o\data('users_sites', array('id_user' => $users->id_user)))->delete();
                $users->delete();

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-deleteusers', $this->language, 'Supression d\'un administrateur'), $users->name, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-users', $this->language, 'Gestion des administrateurs'), $this->ln->txt('admin-banner', 'del-user', $this->language, 'L\'administrateur a bien été supprimé'));
                $this->redirect($this->lurl . '/administration/users');
            } else {
                $this->redirect($this->lurl . '/administration/users');
            }
        } else {
            $this->redirect($this->lurl . '/administration/users');
        }
    }

    /**
     * Gestion de la liste des logs du site en cours.
     * Utilise le moteur Octeract.
     *
     * @function _logs
     */
    public function _logs()
    {
        $this->users->checkAccess('admilogs');
        $this->ssMenuActive = 'admilogs';
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        $this->logs = new o\data('logs', array('id_site' => $_SESSION['infosSite']['id_site']));

        if (!empty($_POST['filtre'])) {
            $this->logs->join('users', 'u');
            $this->logs->addWhere('u.`firstname` LIKE "%' . $_POST['filtre'] . '%" OR u.`name` LIKE "%' . $_POST['filtre'] . '%" OR `logs`.`action` LIKE "%' . $_POST['filtre'] . '%" OR `logs`.`details` LIKE "%' . $_POST['filtre'] . '%" OR `logs`.`complement` LIKE "%' . $_POST['filtre'] . '%"');
        }
    }

    /**
     * Gestion du détail d'un log du site en cours.
     * Utilise le moteur Octeract.
     *
     * @function _detailLog
     */
    public function _detailLog()
    {
        $this->users->checkAccess('admilogs');
        $this->ssMenuActive = 'admilogs';
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        $this->logs = new o\logs($this->params[0]);

        if (!$this->logs->exist()) {
            $this->redirect($this->lurl . '/administration/logs');
        }
    }

    /**
     * Gestion de la suppression des logs du site en cours.
     * Utilise le moteur Octeract.
     *
     * @function _deleteLogs
     */
    public function _deleteLogs()
    {
        $this->users->checkAccess('admilogs');
        $this->autoFireNothing = true;

        if (!empty($_SESSION['infosSite']['id_site'])) {
            (new o\data('logs', array('id_site' => $_SESSION['infosSite']['id_site'])))->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-deletelogs', $this->language, 'Supression des enregistrements'));
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-logs', $this->language, 'Enregistrements du BO'), $this->ln->txt('admin-banner', 'del-logs', $this->language, 'Les enregistrements ont bien été supprimés'));

            $this->redirect($this->lurl . '/administration/logs');
        } else {
            $this->redirect($this->lurl . '/administration/logs');
        }
    }

    /**
     * Gestion de la suppression d'un log du site en cours.
     * Utilise le moteur Octeract.
     *
     * @function _deleteLog
     */
    public function _deleteLog()
    {
        $this->users->checkAccess('admilogs');
        $this->autoFireNothing = true;
        $this->logs = new o\logs($this->params[0]);

        if ($this->logs->exist()) {
            $backLog = serialize($this->logs->getArray());
            $this->logs->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-deletelog', $this->language, 'Supression d\'un enregistrement'), $this->logs->action, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-logs', $this->language, 'Enregistrements du BO'), $this->ln->txt('admin-banner', 'del-log', $this->language, 'L\'enregistrement a bien été supprimé'));

            $this->redirect($this->lurl . '/administration/logs');
        } else {
            $this->redirect($this->lurl . '/administration/logs');
        }
    }

}
