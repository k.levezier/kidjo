<?php

class clientsController extends bootstrap {

    public function __construct($command, $config, $app) {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'utilisateurs';
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default() {

        $this->users->checkAccess('utilisateurs');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('clients'));
    }

    public function _export()
    {
        header("Content-Type: text/plain");
        header("Content-disposition: attachment; filename=clients-".date('YmdHi').".csv");
        $sql = 'SELECT `civilite`, `nom`, `prenom`, `mobile` as pays, `email`, (select group_concat(name) from profiles p where p.id_profile in (select id_profile from clients_profiles cp where cp.id_client=clients.id_client)) as profiles, (select group_concat(name) from geozones p where p.id_geozone in (select id_geozone from clients_geozones cp where cp.id_client=clients.id_client)) as zones,`status` FROM `clients` WHERE email not like "DISABLED%"';
        $res = $this->bdd->run($sql);
        echo "civilite;nom;prenom;pays;email;profils;zones;statut\r\n";
        foreach($res as $doc)
        {
            //print_r($doc);

            echo utf8_decode($doc['civilite'].";".$doc['nom'].";".$doc['prenom'].";".$doc['pays'].";".$doc['email'].";".$doc['profiles'].";".$doc['zones'].";".$doc['status']."\r\n");
        }
        die;
    }

    function _tmp()
    {
        if ($handle = opendir('/tmp')) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    echo "$entry\n";
                }
            }
            closedir($handle);
        }
    }

    function _importadmin()
    {
        $csv = file_get_contents($this->path.'protected/importadmin.csv');
        $lines = explode("\r\n",$csv);
        foreach($lines as $line)
        {
            $line = utf8_encode($line);
            $doc = explode(';',$line);
            print_r($doc);
            $this->users = new o\users(array('email'=>$doc[2]));
            if($this->users->exist())
            {

            }
            else
            {
                $pass=generatePassword(8);
                $sql = 'INSERT INTO users(hash, firstname,name,email,password,status,added) VALUES("'.md5($doc[2]).'","'.$doc[5].'","'.$doc[3].'","'.$doc[2].'",sha1(concat("'.SUFFIX_SALT.'",(select telephone FROM clients WHERE email="'.$doc[2].'"),"'.SUFFIX_SALT.'")),1,now())';
                echo $sql;
                $this->bdd->query($sql);
                $this->users = new o\users(array('email'=>$doc[2]));
                $idc = $this->users->id_user;

                // PROFILES
                if(1) // 2
                {
                    $sql = 'INSERT INTO users_sites(id_user,id_site,ordre) VALUES('.$idc.',1,1)';
                    $this->bdd->query($sql);
                }

                if(1) // 17
                {
                    $sql = 'INSERT INTO users_zones(id_user,id_zone,ordre) SELECT '.$idc.',id_zone,ordre FROM users_zones WHERE id_user=1';
                    $this->bdd->query($sql);
                }
            }
        }
        die;
    }


    function _import()
    {
        $csv = file_get_contents($this->path.'protected/import.csv');
        $lines = explode("\r\n",$csv);
        foreach($lines as $line)
        {
            $line = utf8_encode($line);
            $doc = explode(';',$line);
            print_r($doc);
            $this->clients = new o\clients(array('email'=>$doc[2]));
            if($this->clients->exist())
            {
                //$sql = 'UPDATE clients set mobile="'.$doc[3].'",nom="'.$doc[5].'" WHERE id_client='.$this->clients->id_client;
                //$this->bdd->query($sql);
                //echo $sql;
                if($doc[2]!='')
                {
                    $params['email'] = $this->clients->email;
                    $params['password'] = $this->clients->telephone;
                    //$this->envoyerMail('confirmation-inscription-newsletter', 'fr', 't.raymond@equinoa.com', $params);
                    $this->envoyerMail('confirmation-inscription-newsletter', 'fr', $doc[2], $params);
                }
                //die;
            }
            else
            {
                $pass=generatePassword(8);
                $sql = 'INSERT INTO clients(hash, slug, nom, email, mobile, password, telephone, added) VALUES("'.md5($doc[2]).'","'.$doc[0].'","'.$doc[5].'","'.$doc[2].'","'.$doc[3].'","'.md5($pass).'","'.$pass.'",now())';
                echo $sql;
                $this->bdd->query($sql);
                $this->clients = new o\clients(array('email'=>$doc[2]));
                $idc = $this->clients->id_client;

                // PROFILES
                if(1) // 2
                {
                    $sql = 'INSERT INTO clients_profiles(id_client,id_profile) VALUES('.$idc.',1)';
                    $this->bdd->query($sql);
                }

                if(1) // 17
                {
                    $sql = 'INSERT INTO clients_geozones(id_client,id_geozone) VALUES('.$idc.',17)';
                    $this->bdd->query($sql);
                }
            }
        }
        die;
    }

    /**
     * Gestion des clients.
     * Utilise le moteur Octeract.
     *
     * @function _gestion
     */
    public function _gestion() {
        $this->users->checkAccess('utilgestion-des-clients');
        $this->ssMenuActive = 'utilgestion-des-clients';
        $this->clients = new o\data('clients');
        if(isset($_POST['filter']))
        {
            $_SESSION['cltName'] = $_POST['name'];
            header('location:/clients/gestion/'.$this->params[0]);die;
        }elseif(isset($_POST['reset'])){
            unset($_SESSION['cltName']);
        }
        if($this->params[0]=='')
        {
            $this->clients->addWhere('email not like "DISABLED-%"');
        }
        if($_SESSION['cltName']!='')
        {
            $this->clients->addWhere('nom like "%'.$_SESSION['cltName'].'%" || prenom like "%'.$_SESSION['cltName'].'%" || email like "%'.$_SESSION['cltName'].'%"');
        }
        $this->clients->order('nom','asc','');
    }

    /**
     * Gestion de l'ajout/edition des clients.
     * Utilise le moteur Octeract.
     *
     * @function _formClients
     */
    public function _formClient() {
        //error_reporting(9999);
        //ini_set('display_errors','on');
        $this->users->checkAccess('utilisateurs');
        $this->ssMenuActive = 'cligestion';

        $this->loadJs('icheck.min');
        $this->loadCss('icheck');

        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');

        $this->loadJs('bootstrap-datepicker');
        $this->loadCss('datepicker');

        if ($this->params[0]==''){
            $this->new = true;
            $this->clients = new o\clients(array('id_client'=>0));
        } else {
            $this->clients = new o\clients($this->params[0]);
            if ($this->clients->exist()) {
                $this->new = false;
                $this->etatCommande = array(
                    0 => '<span class=""><i class="fa fa-gear"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-en-cours', $this->language, 'En cours'),
                    1 => '<span class="text-primary"><i class="fa fa-check"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-validee', $this->language, 'Validée').'</span>',
                    2 => '<span class="text-primary"><i class="fa fa-truck"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-expediee', $this->language, 'Expédiée').'</span>',
                    3 => '<span class="text-danger"><i class="fa fa-times"></i>&nbsp;'.$this->ln->txt('admin-dash', 'etat-cmd-annulee', $this->language, 'Annulée').'</span>'
                );
                $this->clients_geozones = new o\data('clients_geozones', array('id_client'=>$this->params[0]));
                $this->clients_profiles = new o\data('clients_profiles', array('id_client'=>$this->params[0]));
            } else {
                $this->redirect($this->lurl . '/clients/gestion/');
            }
        }
        $this->geozones = new o\data('geozones');
        $this->profiles = new o\data('profiles');
        if(isset($_POST['sendForm']) && $this->params[0] == $_POST['id_client']){
            $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
            $_POST['prenom'] = trim(htmlspecialchars($_POST['prenom']));
            $_POST['email'] = trim($_POST['email_chromehack']);
            $_POST['password'] = trim($_POST['password_chromehack']);

            $this->errorFormNom = false;
            $this->errorFormPrenom = false;
            $this->errorFormEmail = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['nom']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['prenom']) == 0) {
                $this->errorFormPrenom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['email']) == 0) {
                $this->errorFormEmail = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $this->errorFormEmail = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'invalid-email-format', $this->language, 'Le format de l\'adresse email est incorrect');
            } else {
                $tempClient = new o\clients(array('email'=>$_POST['email']));
                if (($this->new == true && $tempClient->exist()) || ($this->new == false && $tempClient->exist() && $tempClient->id_client != $this->clients->id_client) ) {
                    $this->errorFormEmail = true;
                    $this->error = true;
                    $this->errorMsg = $this->ln->txt('admin-generic', 'email-already-taken', $this->language, 'Cette adresse email est déjà utilisée');
                }
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->clients->getArray()) : '');
                $this->clients->civilite = $_POST['civilite'];
                $this->clients->nom = $_POST['nom'];
                $this->clients->prenom = $_POST['prenom'];
                $this->clients->email = $_POST['email'];
                if($this->new){
                    $pass = ($_POST['password']!=''?$_POST['password']:generatePassword(8));
                    $this->clients->password = md5($pass);
                } elseif($_POST['password'] != '')
                    $this->clients->password = md5($_POST['password']);
                //$time = DateTime::createFromFormat('!d/m/Y', $_POST['naissance'])->getTimestamp();
                $this->clients->naissance = date('Y-m-d', $time);
                $this->clients->telephone = $_POST['telephone'];
                $this->clients->mobile = $_POST['mobile'];
                $this->clients->status = $_POST['status'];
                $this->clients->slug = (!$this->new ? $this->clients->slug : $this->clFonctions->controlSlug('clients', generateSlug($_POST['nom'].' '.$_POST['prenom'])));
                $this->clients->save();
                if(!$this->new)
                {
                    $this->clients_geozones->delete();
                    $this->clients_profiles->delete();
                }else {
                    $data['login'] = $_POST['email'];
                    $data['password'] = $pass;
                    //$this->envoyerMail('recap-acces', 'en', $_POST['email'], $data);
                }

                if(isset($_POST['prevenir'])){
                    //ini_set('display_errors',1);
                    //error_reporting(9999);
                    $this->clients->password = "";
                    $this->clients->save();
                    $vars = [
                        'client_prenom_nom' => $this->clients->prenom." ".$this->clients->nom,
                        'client_email' => $this->clients->email,
                        'surl' => $this->surl,
                        'url_video' => $this->urlfront.'/how_to_use',
                        'lien' => $this->urlfront.'/newpassword/'.$this->clients->hash,
                        'meta_tile' => "New account",
                        'x-splio-ref' => $this->Config['env'] . '_mail-ouverture-de-compte_' . date('Y-m-d')
                    ];

                    $this->clEmail = new clEmail($this->Config['env'], $this->lurl, $this->surl);
                    $this->clEmail->sendMail("mail-ouverture-de-compte", 'en', $this->clients->email,  $vars);
                }


                foreach($this->geozones as $zone)
                {
                    if($_POST['zone'.$zone->id_geozone]==1)
                    {
                        $clients_geozones = new o\clients_geozones();
                        $clients_geozones->id_client = $this->clients->id_client;
                        $clients_geozones->id_geozone = $zone->id_geozone;
                        $clients_geozones->insert();
                    }
                }

                foreach($this->profiles as $profile)
                {
                    if($_POST['profile'.$profile->id_profile]==1)
                    {
                        $clients_profiles = new o\clients_profiles();
                        $clients_profiles->id_client = $this->clients->id_client;
                        $clients_profiles->id_profile = $profile->id_profile;
                        $clients_profiles->insert();
                    }
                }

                $cacheFolder = $this->path . '/cache/';
                $dossier = opendir($cacheFolder);

                while ($fichier = readdir($dossier)) {
                    if ($fichier != '.' && $fichier != '..' && $fichier != '.gitignore') {
                        @unlink($cacheFolder . $fichier);
                    }
                }

                closedir($dossier);


                $this->clFonctions->updateSlug($this->clients, $this->clients->id_client);

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editclient', $this->language, 'Modification d\'un client') : $this->ln->txt('admin-logs', 'action-addclient', $this->language, 'Ajout d\'un client')), $this->clients->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'client', $this->language, 'Client'), (!$this->new ? $this->ln->txt('admin-banner', 'client-edit', $this->language, 'Le client a bien été modifié') : $this->ln->txt('admin-banner', 'client-add', $this->language, 'Le client a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/clients/formClient/' . $this->clients->id_client.($_POST['part']!=0?'/part___'.$_POST['part']:''));
                } else {
                    $this->redirect($this->lurl . '/clients/gestion');
                }
            }
        }

        $_POST = $this->clients->getArray();
        $_POST['naissance'] = date('d/m/Y', strtotime($_POST['naissance']));

        $this->clientZones = array();
        $this->clientProfiles = array();

        if(!$this->new) {
            $this->cg = (new o\data('clients_geozones'))->addWhere('id_client='.$this->params[0]);
            $this->cp = (new o\data('clients_profiles'))->addWhere('id_client='.$this->params[0]);
            $this->clientZones = array();
            foreach($this->cg as $g)
                $this->clientZones[] = $g->id_geozone;
            $this->clientProfiles = array();
            foreach($this->cp as $p)
                $this->clientProfiles[] = $p->id_profile;
        }
    }

    /**
     * Gestion de l'ajout/edition des adresses clients
     * Utilise le moteur Octeract.
     *
     * @function _formAdresse
     */
    public function _formAdresse() {
        $this->users->checkAccess('utilisateurs');
        $this->ssMenuActive = 'cligestion';

        $this->loadJs('icheck.min');
        $this->loadCss('icheck');

        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');

        $this->loadJs('bootstrap-datepicker');
        $this->loadCss('datepicker');

        $clients = new o\clients($this->params[0]);
        if (!$clients->exist()){
            $this->redirect($this->lurl . '/clients/gestion');
        } else {
            if (!isset($this->params[1])){
                $this->new = true;
                $this->adresse = new o\clients_adresses(array('id_client'=>0));
                $adresses = new o\data('clients_adresses');
                $ID = max(array_merge(array(0), $adresses->id_adresse->getArray())) + 1;
            } else {
                $this->adresse = new o\clients_adresses($this->params[1]);
                if ($this->adresse->exist()) {
                    $this->new = false;
                    $ID = $this->adresse->id_adresse;
                } else {
                    $this->redirect($this->lurl . '/clients/formClient/'.$this->params[0]);
                }
            }
        }

        if(isset($_POST['sendForm']) && ($this->params[0] == $_POST['id_client'] && $this->params[1] == $_POST['id_adresse'] || $this->new)){
            $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
            $_POST['prenom'] = trim(htmlspecialchars($_POST['prenom']));
            $_POST['adresse1'] = trim($_POST['adresse1']);
            $_POST['cp'] = trim($_POST['cp']);
            $_POST['ville'] = trim($_POST['ville']);
            $_POST['id_pays'] = trim($_POST['id_pays']);

            $this->errorFormNom = false;
            $this->errorFormPrenom = false;
            $this->errorFormEmail = false;
            $this->error = false;
            $this->errorMsg = '';

            $requiredMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            if (strlen($_POST['nom']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $requiredMsg;
            } elseif (strlen($_POST['prenom']) == 0) {
                $this->errorFormPrenom = true;
                $this->error = true;
                $this->errorMsg = $requiredMsg;
            } elseif (strlen($_POST['adresse1']) == 0) {
                $this->errorFormAdresse1 = true;
                $this->error = true;
                $this->errorMsg = $requiredMsg;
            } elseif (strlen($_POST['cp']) == 0) {
                $this->errorFormCp = true;
                $this->error = true;
                $this->errorMsg = $requiredMsg;
            } elseif (ctype_digit($_POST['cp']) === false) {
                $this->errorFormCp = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-cp-digit', $this->language, 'Le code postal ne doit être composé que de chiffres');
            } elseif (strlen($_POST['ville']) == 0) {
                $this->errorFormVille = true;
                $this->error = true;
                $this->errorMsg = $requiredMsg;
            } elseif (strlen($_POST['id_pays']) == 0) {
                $this->errorFormIdPays = true;
                $this->error = true;
                $this->errorMsg = $requiredMsg;
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->adresse->getArray()) : '');

                if($this->new){
                    $this->adresse->id_client = $clients->id_client;
                }
                $this->adresse->civilite = $_POST['civilite'];
                $this->adresse->nom = $_POST['nom'];
                $this->adresse->prenom = $_POST['prenom'];
                $this->adresse->societe = $_POST['societe'];
                $this->adresse->adresse1 = $_POST['adresse1'];
                $this->adresse->adresse2 = $_POST['adresse2'];
                $this->adresse->cp = $_POST['cp'];
                $this->adresse->ville = $_POST['ville'];
                $this->adresse->id_pays = $_POST['id_pays'];
                $country = new o\countries(array('id_pays'=>$_POST['id_pays']));
                if($country->exist()) {
                    $ln = $clients->id_langue!=''?$clients->id_langue:$this->language;
                    $this->adresse->pays = $country->{$ln};
                }
                $this->adresse->telephone = $_POST['telephone'];
                $this->adresse->nom_adresse = $_POST['nom_adresse']!=''?$_POST['nom_adresse']:$this->ln->txt('admin-generic', 'adresse', $this->language, 'Adresse').'-'.$ID;
                $this->adresse->save();

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editadresse', $this->language, 'Modification d\'une adresse') : $this->ln->txt('admin-logs', 'action-addadresse', $this->language, 'Ajout d\'une adresse')), $this->adresse->nom_adresse, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'adresse', $this->language, 'Adresse'), (!$this->new ? $this->ln->txt('admin-banner', 'adresse-edit', $this->language, 'L\'adresse a bien été modifiée') : $this->ln->txt('admin-banner', 'adresse-add', $this->language, 'L\'adresse a bien été ajoutée')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/clients/formAdresse/' . $clients->id_client.'/'.$this->adresse->id_adresse);
                } else {
                    $this->redirect($this->lurl . '/clients/formClient/'.$this->params[0].'/part___2');
                }
            }
        }

        $_POST = $this->adresse->getArray();
    }

    /**
     * Gestion de la suppression d'un client
     * Utilise le moteur Octeract.
     *
     * @function _deleteClient
     */
    public function _deleteClient() {
        $this->users->checkAccess('utilisateurs');
        $this->autoFireNothing = true;
        $clients = new o\clients(array('id_client' => $this->params[0]));
        if ($clients->exist() > 0) {
            $backLog = serialize($clients->getArray());
            $nom = $clients->nom;
            $clients->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-client-delete', $this->language, 'Supression d\'un client'), $nom, $backLog);
        }
	header('location:/clients');die;
    }

    /**
     * Méthode de génération d'un mot de passe
     * Utilise le moteur Octeract.
     *
     * @function _generatePassword
     */
    public function _generatePassword() {
        $this->users->checkAccess('utilisateurs');
        $this->autoFireNothing = true;
        $min_length = 8;

        if(isset($this->params[0]) && ctype_digit($this->params[0])){
            echo generatePassword(max($min_length, $this->params[0]));
        } else {
            echo generatePassword();
        }

        die;
    }
}
