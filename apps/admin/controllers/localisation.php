<?php

class localisationController extends bootstrap
{

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'localisation';
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default()
    {
        $this->users->checkAccess('localisation');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('localisation'));
    }

    /**
     * Gestion de la liste des traductions.
     * Utilise le moteur Octeract.
     *
     * @function _traductions
     */
    public function _traductions()
    {
        $this->users->checkAccess('loctraductions');
        $this->ssMenuActive = 'loctraductions';
        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');
        $this->ln->checkNewLanguage();
        $this->lSections = $this->ln->listeSections($this->language);

        if (!empty($this->params['section']) && !isset($_POST['sendFiltresTab']) && !isset($_POST['sendFormSection']) && !isset($_POST['sendFormGlobal'])) {
            $_POST['section'] = $this->params['section'];
        }

        if (!empty($this->params['filtre']) && !isset($_POST['sendFiltresTab']) && !isset($_POST['sendFormSection']) && !isset($_POST['sendFormGlobal'])) {
            $_POST['filtre'] = $this->params['filtre'];
        }

        if (!empty($_POST['section'])) {
            $this->lTraductions = $this->ln->listeTraductionsSection($_POST['section'], $_POST['filtre']);

            if (isset($_POST['sendFormSection'])) {
                foreach ($this->lLangues as $key => $lng) {
                    $backLog = serialize($this->lTraductions[$key]);

                    foreach ($this->lTraductions[$key] as $trad) {
                        $traduc = new o\traductions($_POST['id_traduction_' . $trad['id_traduction']]);

                        if ($traduc->exist()) {
                            $traduc->texte = $_POST['texte_' . $trad['id_traduction']];
                            $traduc->update();
                        }
                    }

                    $this->clFonctions->logging($this->ln->txt('admin-logs', 'modif-trad-' . $key . '-section', $this->language, 'Modification des traductions ' . strtoupper($key) . ' pour une section'), $_POST['section'], $backLog);
                }

                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-trad', $this->language, 'Gestion des traductions'), $this->ln->txt('admin-banner', 'modif-trad', $this->language, 'Les traductions ont bien été modifiées'));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/localisation/traductions/section___' . $_POST['section'] . (!empty($_POST['filtre']) ? '/filtre___' . $_POST['filtre'] : ''));
                } else {
                    $this->redirect($this->lurl . '/localisation/traductions');
                }
            }
        } elseif (!empty($_POST['filtre']) && empty($_POST['section'])) {
            $this->lTradSections = $this->ln->listeTraductionsFiltre($_POST['filtre']);

            if (isset($_POST['sendFormGlobal'])) {
                foreach ($this->lTradSections as $sec => $list) {
                    foreach ($this->lLangues as $key => $lng) {
                        $backLog = serialize($list[$key]);

                        foreach ($list[$key] as $trad) {
                            $traduc = new o\traductions($_POST['id_traduction_' . $trad['id_traduction']]);

                            if ($traduc->exist()) {
                                $traduc->texte = $_POST['texte_' . $trad['id_traduction']];
                                $traduc->update();
                            }
                        }

                        $this->clFonctions->logging($this->ln->txt('admin-logs', 'modif-trad-' . $key . '-section', $this->language, 'Modification des traductions ' . strtoupper($key) . ' pour une section'), $sec, $backLog);
                    }
                }

                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-trad', $this->language, 'Gestion des traductions'), $this->ln->txt('admin-banner', 'modif-trad', $this->language, 'Les traductions ont bien été modifiées'));

                if ($_POST['restePageGlobal'] == 1) {
                    $this->redirect($this->lurl . '/localisation/traductions/filtre___' . $_POST['filtre']);
                } else {
                    $this->redirect($this->lurl . '/localisation/traductions');
                }
            }
        }
    }

    /**
     * Gestion de l'export des traductions.
     * Utilise le moteur Octeract.
     *
     * @function _exportTrads
     */
    public function _exportTrads()
    {
        $this->users->checkAccess('loctraductions');
        $this->ssMenuActive = 'loctraductions';
        $this->autoFireNothing = true;
        $contentCSV = "\"ID\";\"LANGUE\";\"SECTION\";\"NOM\";\"TRADUCTION\"\n";
        $lTrads = new o\data('traductions');

        foreach ($lTrads->order('section', 'ASC', '', 'nom', 'ASC', '', 'id_langue', 'ASC') as $traduction) {
            $contentCSV .= "\"" . $traduction->id_traduction . "\";\"" . $traduction->id_langue . "\";\"" . utf8_decode($traduction->section) . "\";\"" . utf8_decode($traduction->nom) . "\";\"" . utf8_decode($traduction->texte) . "\"\n";
        }

        header('Content-Type: application/csv-tab-delimited-table; charset=utf-8');
        header('Content-disposition: filename=traductions.csv');
        header('Pragma: no-cache');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0, public');
        header('Expires: 0');

        echo $contentCSV;
    }

    /**
     * Gestion de l'import des traductions.
     * Utilise le moteur Octeract.
     *
     * @function _importTrads
     */
    public function _importTrads()
    {
        $this->users->checkAccess('loctraductions');
        $this->ssMenuActive = 'loctraductions';

        if (isset($_POST['sendForm'])) {
            $this->error = false;
            $this->errorMsg = '';

            if (!empty($_FILES['csv']['name'])) {
                $namefile = 'import-traductions-' . date('YmdHis');
                $this->upload = new upload();
                $this->upload->setExtValide(array('csv'));
                $this->upload->setUploadDir($this->path, 'protected/imports/traductions/');

                if ($this->upload->doUpload('csv', $namefile)) {
                    $handle = fopen($this->path . 'protected/imports/traductions/' . $namefile . '.csv', 'r');

                    while ($data = fgetcsv($handle, 1000, ';')) {
                        $trad = new o\traductions($data[0]);
                        if ($trad->exist()) {
                            $trad->texte = trim(utf8_encode($data[4]));
                            $trad->update();
                        }
                    }

                    fclose($handle);

                    $this->clFonctions->logging($this->ln->txt('admin-logs', 'import-traduction', $this->language, 'Import des traductions'), $namefile, '');
                    $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'gestion-trad', $this->language, 'Gestion des traductions'), $this->ln->txt('admin-banner', 'import-trad', $this->language, 'L\'import des traductions a bien été effectué'));
                    $this->redirect($this->lurl . '/localisation/traductions');
                } else {
                    $this->error = true;
                    $this->errorMsg = $this->upload->getErrorType();
                }
            } else {
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'form-champ-obli', $this->language, 'Vous devez renseigner ce champ');
            }
        }
    }

} 
