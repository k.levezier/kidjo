<?php

class foldersController extends bootstrap
{
    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'folders';
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default() {

        $this->users->checkAccess('folders');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('folders'));

    }

    /**
     * Gestion de la page de gestion des dossiers.
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _all_folders
     */
    public function _all_folders()
    {
        $this->users->checkAccess('foldersallfolders');
        $this->ssMenuActive = 'foldersallfolders';

        $this->loadJs('chosen.jquery');
        $this->loadCss('chosen');

        $this->loadCss('icheck');
        $this->loadJs('icheck.min');

        $this->loadJs('sweetalert.min');
        $this->loadCss('sweetalert');

        //récup des listes
        $this->languages = new o\data('languages');
        $this->folders_type = new o\data('folders_type');
        $this->countries = new o\data('countries');
        $this->folders_media_type = new o\data('folders_media_type');

        $reset = false;
        if(isset($_POST['filter']))
        {
            if(isset($_POST['title']) && !empty($_POST['title'])){
                $filters[] = 'f.title LIKE "%'.htmlspecialchars($_POST['title']).'%"';
            }

            if($_POST['active_folder'] === "on"){
                $status_searched = 1;
                $filters[] = 'f.status ='.$status_searched;
            }else{
                $status_searched = 0;
                $filters[] = 'f.status ='.$status_searched;
            }


            if(isset($_POST['folder-language']) && !empty($_POST['folder-language'])){
                if(!in_array('0',$_POST['folder-language']) && count($_POST['folder-language']) > 0){
                    $LLanguages = implode(',',$_POST['folder-language']);
                    $filters[] = 'f.id_folder IN( SELECT fl.id_folder FROM folders_languages fl WHERE fl.id_language IN('.htmlspecialchars($LLanguages).'))';
                }
            }


            if(isset($_POST['folder-countries']) && !empty($_POST['folder-countries'])){
                if(!in_array('0',$_POST['folder-countries']) && count($_POST['folder-countries']) > 0){
                    $LCountries = implode(',',$_POST['folder-countries']);
                    $filters[] = 'f.id_folder IN( SELECT fc.id_folder FROM folders_countries fc WHERE fc.id_country IN('.htmlspecialchars($LCountries).'))';
                }
            }

            if(isset($_POST['folder-type']) && !empty($_POST['folder-type'])){
                if(!in_array('0',$_POST['folder-type']) && count($_POST['folder-type']) > 0){
                    $LType = implode(',',$_POST['folder-type']);
                    $filters[] = 'f.id_type IN('.htmlspecialchars($LType).')';
                }
            }


            if(isset($_POST['folder-media-type']) && !empty($_POST['folder-media-type']) && $_POST['folder-media-type'] != 0){
                $filters[] = 'f.id_media_type ='.htmlspecialchars($_POST['folder-media-type']);
            }

            $_SESSION['filter_folder']['title'] = $_POST['title'];
            $_SESSION['filter_folder']['active_folder'] = $status_searched;
            $_SESSION['filter_folder']['folder-language'] = $_POST['folder-language'];
            $_SESSION['filter_folder']['folder-countries'] = $_POST['folder-countries'];
            $_SESSION['filter_folder']['folder-type'] = $_POST['folder-type'];
            $_SESSION['filter_folder']['folder-media-type'] = $_POST['folder-media-type'];
            $_SESSION['filter_folder']['filters'] = $filters;


            $joins = array();
            $group_by = "";
            $orders[] = "f.ordre DESC";



        }
        elseif($this->params['0'] == "reset"){
            $reset = true;
            unset($_SESSION['filter_folder']);
            $this->redirect($this->lurl . '/folders/all_folders');
        }
        else{
            // on filtre en fonction des session
            /*if(isset($_SESSION['filter_folder'])){
                $this->folders->addWhere('title like "%'.$_SESSION['foldTitle'].'%" ');
            }*/
        }

        //reset ou situation initiale
        if($reset or !isset($_POST['filter'])){
            $_SESSION['filter_folder']['active_folder'] = 1;
            $_SESSION['filter_folder']['folder-language'] = array(0);
            $_SESSION['filter_folder']['folder-countries'] = array(0);
            $_SESSION['filter_folder']['folder-type'] = array(0);
            $_SESSION['filter_folder']['folder-media-type'] = 0;

            $orders[] ="f.added DESC";
            $filters = array();
            $joins = array();
            $group_by = "";

        }

        $sql = 'SELECT f.*, ft.name as type, fmt.name as media_type
                FROM `folders` as f
                LEFT JOIN folders_type ft ON (ft.id_folder_type = f.id_type)
                LEFT JOIN folders_media_type fmt ON (fmt.id_media_type = f.id_media_type)'
            . implode(' ', $joins)
            .(!empty($filters) ? ' WHERE ' . implode(' AND ', $filters) : '')
            .$group_by
            . ' ORDER BY ' . implode(',', $orders);


        $_SESSION['filter_folder']['sql_folders'] = [
            'filters' => $filters,
            'orders' => $orders,
            'joins' => $joins,
            'group_by' => $group_by,
        ];

        $result = $this->bdd->query($sql);
        $list = [];
        while ($results = $this->bdd->fetch_assoc($result)) {
            $list[] = $results;
        }
        $this->LFolders = $list;

    }

    /**
     * Gestion de l'ajout/edition des folders.
     * Utilise le moteur Octeract.
     *
     * @function _formFolder
     */
    public function _formFolder()
    {
        $this->users->checkAccess('foldersallfolders');
        $this->ssMenuActive = 'foldersallfolders';


        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadJs('bootstrap-datepicker');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->loadCss('datepicker');

        //récup des listes
        $this->languages = new o\data('languages');
        $this->folders_type = new o\data('folders_type');
        $this->countries = new o\data('countries');
        $this->countries->order('en', 'ASC');
        $this->folders_media_type = new o\data('folders_media_type');
        $this->folders_priority = new o\data('folders_priority');
        $this->folders_languages = new o\data('folders_languages');
        $this->folders_videos = new o\data('folders_videos');


        $_POST['languages_folder'] = array();
        if (!empty($this->params['folder'])) {
            $this->folders = new o\folders(array('id_folder' => $this->params['folder']));
            if ($this->folders->exist()) {
                $this->new = false;

                //récupération des langues du folder
                $l_languages_folders = new o\data('folders_languages');
                $l_languages_folders->addWhere('id_folder = '.$this->params['folder']);
                if($l_languages_folders->count() > 0){
                    foreach($l_languages_folders as $lg){
                        $_POST['languages_folder'][] = $lg->id_language;
                    }
                }

                //récupération des pays du folder
                $l_countries_folders = new o\data('folders_countries');
                $l_countries_folders->addWhere('id_folder = '.$this->params['folder']);
                if($l_countries_folders->count() > 0){
                    foreach($l_countries_folders as $ct){
                        $_POST['folder-countries'][] = $ct->id_pays;
                    }
                }

            } else {
                $this->redirect($this->lurl . '/clients/gestion');
            }
        } else {
            $this->folders = new o\folders();
            $this->new = true;
        }

        if (isset($_POST['sendForm'])) {
            $_POST['title'] = trim(htmlspecialchars($_POST['title']));
            $_POST['folder-type'] = trim(htmlspecialchars($_POST['folder-type']));
            $_POST['folder-media-type'] = trim(htmlspecialchars($_POST['folder-media-type']));
            $_POST['folder-priority'] = trim(htmlspecialchars($_POST['folder-priority']));

            $this->errorFormNom = false;
            $this->errorFormPrenom = false;
            $this->errorFormEmail = false;
            $this->errorFormPass = false;
            $this->error = false;
            $this->errorMsg = '';
            if (strlen($_POST['title']) == 0) {
                $this->errorFormTitle = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->folders->getArray()) : '');

                //$this->folders->hash = "";
                $this->folders->id_type = $_POST['folder-type'];
                $this->folders->id_media_type = $_POST['folder-media-type'];
                $this->folders->id_priority = $_POST['folder-priority'];
                $this->folders->title = $_POST['title'];


                if($_POST['active_folder'] === "on"){
                    $this->folders->status = 1;
                }else{
                    $this->folders->status = 0;
                }

                $this->folders->age_min = $_POST['age_min'];
                $this->folders->age_max = $_POST['age_max'];
                $this->folders->save();

                //enregistrement folder countries
                if(is_array($_POST['folder-countries'])){
                    $this->folders_countries = new o\data('folders_countries');
                    $this->folders_countries->addWhere('id_folder = '.$this->folders->id_folder);
                    $this->folders_countries->delete();

                    foreach ($_POST['folder-countries'] as $pays){
                        if($pays != "" && !is_null($pays)){
                            $this->folders_countries = new o\folders_countries();
                            $this->folders_countries->id_folder = $this->folders->id_folder;
                            $this->folders_countries->id_country = $pays;
                            $this->folders_countries->save();
                        }
                    }
                }

                if(isset($_POST['create'])){
                    foreach ($_POST['folder-language'] as $id_language){
                        $tempFile = $_FILES['image']['tmp_name'];

                        $targetPath = $this->path.'public/default/var/images/';
                        $fn = mktime().'-'.$_FILES['image']['name'];
                        $targetFile =  $targetPath. $fn;

                        move_uploaded_file($tempFile,$targetFile);

                        $this->folders_languages = new o\folders_languages();
                        $this->folders_languages->id_folder = $this->folders->id_folder;
                        $this->folders_languages->id_language = $id_language;
                        $this->folders_languages->title_folder = $_POST['title'];
                        $this->folders_languages->image = $fn;
                        $this->folders_languages->save();
                    }
                }else{
                    foreach($_POST['folder-language'] as $id_language){
                        if ((!empty($_FILES) && $_FILES['image_'.$id_language]['name']!='')) {
                            $tempFile = $_FILES['image_'.$id_language]['tmp_name'];

                            $targetPath = $this->path.'public/default/var/images/';
                            $fn = mktime().'-'.$_FILES['image_'.$id_language]['name'];
                            $targetFile =  $targetPath. $fn;

                            move_uploaded_file($tempFile,$targetFile);

                            $this->folders_languages = new o\folders_languages(array('id_folder'=>$this->folders->id_folder, 'id_language'=> $id_language));
                            $this->folders_languages->image = $fn;
                            $this->folders_languages->save();

                        }
                    }
                }





                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-editfolder', $this->language, 'Modification d\'un dossier') : $this->ln->txt('admin-logs', 'action-addfolder', $this->language, 'Ajout d\'un dossier')), $this->folders->title, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'folders', $this->language, 'Folders'), (!$this->new ? $this->ln->txt('admin-banner', 'folder-edit', $this->language, 'Le dossier a bien été modifié') : $this->ln->txt('admin-banner', 'folder-add', $this->language, 'Le dossier a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/folders/formClient/folder___' . $this->folders->id_folder . '/part___' . $_POST['part']);
                } else {
                    $this->redirect($this->lurl . '/folders/all_folders');
                }
            }


        } else {
            if(isset($this->params['folder'])){
                $_POST = $this->folders->getArray();

                $this->folders_languages = new o\data('folders_languages');
                $this->folders_languages->addWhere('id_folder = '.$this->params['folder']);
                foreach ($this->folders_languages as $lang){
                    $_POST['languages_folder'][] = $lang->id_language;
                }

                $this->folders_countries = new o\data('folders_countries');
                $this->folders_countries->addWhere('id_folder = '.$this->params['folder']);
                $_POST['folder-countries'] = array();
                foreach ($this->folders_countries as $pays){
                    $_POST['folder-countries'][] = $pays->id_country;
                }
            }
            else{
                $_POST['folder-countries'] = array();
            }
        }

        $this->image_import = false;

        $this->folders_videos->addWhere('id_folder = '.$this->params['folder']);


    }

    /** Gestion des dossiers par age */
    public function _folders_per_age(){
        $this->users->checkAccess('foldfolders-per-age');
        $this->ssMenuActive = 'foldfolders-per-age';

        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadJs('bootstrap-datepicker');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->loadCss('datepicker');

        //récup des listes
        $this->languages = new o\data('languages');
        $this->folders_type = new o\data('folders_type');
        $this->countries = new o\data('countries');
        $this->folders_media_type = new o\data('folders_media_type');

        $reset = false;
        if(isset($_POST['filter']))
        {
            if(isset($_POST['folder-language']) && !empty($_POST['folder-language'])){
                if(!in_array('0',$_POST['folder-language']) && count($_POST['folder-language']) > 0){
                    $LLanguages = implode(',',$_POST['folder-language']);
                    $filters[] = 'f.id_folder IN( SELECT fl.id_folder FROM folders_languages fl WHERE fl.id_language IN('.htmlspecialchars($LLanguages).'))';
                }
            }
            if(isset($_POST['folder-countries']) && !empty($_POST['folder-countries'])){
                if(!in_array('0',$_POST['folder-countries']) && count($_POST['folder-countries']) > 0){
                    $LCountries = implode(',',$_POST['folder-countries']);
                    $filters[] = 'f.id_folder IN( SELECT fc.id_folder FROM folders_countries fc WHERE fc.id_country IN('.htmlspecialchars($LCountries).'))';
                }
            }

            if(isset($_POST['folder-type']) && !empty($_POST['folder-type']) && $_POST['folder-type'] != 0){
                $filters[] = 'f.id_type ='.htmlspecialchars($_POST['folder-type']);
            }

            if(isset($_POST['folder-media-type']) && !empty($_POST['folder-media-type']) && $_POST['folder-media-type'] != 0){
                $filters[] = 'f.id_media_type ='.htmlspecialchars($_POST['folder-media-type']);
            }

            $_SESSION['filter_folder']['folder-language'] = $_POST['folder-language'];
            $_SESSION['filter_folder']['folder-countries'] = $_POST['folder-countries'];
            $_SESSION['filter_folder']['folder-type'] = $_POST['folder-type'];
            $_SESSION['filter_folder']['folder-media-type'] = $_POST['folder-media-type'];
            $_SESSION['filter_folder']['filters'] = $filters;

            $joins = array();
            $group_by = "";
            $orders[] = "f.ordre DESC";

        }
        elseif($this->params['0'] == "reset"){
            $reset = true;
            unset($_SESSION['filter_folder']);
        }
        else{
            // on filtre en fonction des session
            /*if(isset($_SESSION['filter_folder'])){
                $this->folders->addWhere('title like "%'.$_SESSION['foldTitle'].'%" ');
            }*/
        }

        //reset ou situation initiale
        if($reset or !isset($_POST['filter'])){
            $_SESSION['filter_folder']['active_folder'] = 1;
            $_SESSION['filter_folder']['folder-language'] = array(0);
            $_SESSION['filter_folder']['folder-countries'] = array(0);
            $_SESSION['filter_folder']['folder-type'] = array(0);
            $_SESSION['filter_folder']['folder-media-type'] = 0;

            $orders[] ="f.added DESC";
            $filters = array();
            $joins = array();
            $group_by = "";

        }

        $sql = 'SELECT f.*, ft.name as type, fmt.name as media_type
                FROM `folders` as f
                LEFT JOIN folders_type ft ON (ft.id_folder_type = f.id_type)
                LEFT JOIN folders_media_type fmt ON (fmt.id_media_type = f.id_media_type)'
            . implode(' ', $joins)
            .(!empty($filters) ? ' WHERE ' . implode(' AND ', $filters) : '')
            .$group_by
            . ' ORDER BY ' . implode(',', $orders);


        $_SESSION['filter_folder']['sql_folders'] = [
            'filters' => $filters,
            'orders' => $orders,
            'joins' => $joins,
            'group_by' => $group_by,
        ];

        $result = $this->bdd->query($sql);
        $list = [];
        while ($results = $this->bdd->fetch_assoc($result)) {
            $list[] = $results;
        }
        $this->LFolders = $list;

        $this->tab_onglets_age = array("No age","Age : 1", "Age : 2", "Age : 3", "Age : 4", "Age : 5", "Age : 6", "Age : 7");

        $tabOngletsAgeFolder = array();
        if(count($this->LFolders) > 0){
            foreach($this->LFolders as $fold){
                foreach($this->tab_onglets_age as $ageIndex => $ageName){
                    if($ageIndex >= $fold['age_min'] && $ageIndex <= $fold['age_max']){
                        $tabOngletsAgeFolder[$ageIndex]['folders'][] = $fold;
                    }
                }
            }
        }
        $this->tabAgeFolders = $tabOngletsAgeFolder;
    }

    /**
     * Gestion de la suppression d'un folder
     * Utilise le moteur Octeract.
     *
     * @function _deleteFolder
     */
    public function _deleteFolder()
    {
        $this->users->checkAccess('folders');
        $this->autoFireNothing = true;

        if (!empty($this->params['folder'])) {
            $folders = new o\folders(array('id_folder' => $this->params['folder']));
            if ($folders->exist() > 0) {
                $backLog = serialize($folders->getArray());
                $nom = $folders->title;

                $folders->delete();

                $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-delete-folder', $this->language, 'Folder archiving'), $nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'folders', $this->language, 'Folders'), $this->ln->txt('admin-banner', 'folder-del', $this->language, 'Folder deleted'));
                $this->redirect($this->lurl . '/folders/all_folders');
            } else {
                $this->redirect($this->lurl . '/folders/all_folders');
            }
        } else {
            $this->redirect($this->lurl . '/folders/all_folders');
        }
    }

}
