<?php

class parametresController extends bootstrap
{

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'parametres';
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default()
    {
        $this->users->checkAccess('parametres');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('parametres'));
    }

    /**
     * Gestion de la liste des paramètres de l'application.
     * Utilise le moteur Octeract.
     *
     * @function _application
     */
    public function _application()
    {
        $this->users->checkAccess('paramapplication');
        $this->ssMenuActive = 'paramapplication';
        $this->sets_categories = new o\data('sets_categories');
        $this->orphan_sets = new o\data('sets', array('id_categorie' => null));
    }

    /**
     * Gestion du formulaire d'ajout / édition d'un paramètre de l'application.
     * Utilise le moteur Octeract.
     *
     * @function _formApp
     */
    public function _formApp()
    {
        $this->users->checkAccess('paramapplication');
        $this->ssMenuActive = 'paramapplication';
        $this->loadJs('icheck.min');
        $this->loadJs('bootstrap-datepicker');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('datepicker');
        $this->loadCss('sweetalert');
        $this->sets_categories = new o\data('sets_categories');
        $this->sets = new o\sets($this->params[0]);
        $this->new = ($this->sets->exist() ? false : true);

        if (isset($_POST['sendForm']) && $this->params[0] == $_POST['id_set']) {
            $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
            $_POST['type'] = trim(htmlspecialchars($_POST['type']));
            $_POST['id_categorie'] = trim(htmlspecialchars($_POST['id_categorie']));
            $this->errorFormNom = false;
            $this->errorFormType = false;
            $this->errorFormCateg = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['nom']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['type']) == 0) {
                $this->errorFormType = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['id_categorie']) == 0) {
                $this->errorFormCateg = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->sets->getArray()) : '');
                $this->sets->nom = $_POST['nom'];
                $this->sets->description = $_POST['description'];
                $this->sets->type = $_POST['type'];
                $this->sets->id_categorie = $_POST['id_categorie'];
                $this->sets->lock = (isset($_POST['lock']) ? $_POST['lock'] : 0);
                $this->sets->slug = (!$this->new ? $this->sets->slug : $this->clFonctions->controlSlug('sets', generateSlug($_POST['nom'])));
                $this->sets->value = $this->clSettings->handleFormBO($_POST['type'], $_POST, $_FILES, $this->sets->slug, $this->spath);
                $this->sets->save();
                $this->clFonctions->updateSlug($this->sets, $this->sets->id_set);

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-parameditapp', $this->language, 'Modification d\'un paramètre application') : $this->ln->txt('admin-logs', 'action-paramaddapp', $this->language, 'Ajout d\'un paramètre application')), $this->sets->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'param-app', $this->language, 'Paramètres application'), (!$this->new ? $this->ln->txt('admin-banner', 'param-edit', $this->language, 'Le paramètre a bien été modifié') : $this->ln->txt('admin-banner', 'param-add', $this->language, 'Le paramètre a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/parametres/formApp/' . $this->sets->id_set);
                } else {
                    $this->redirect($this->lurl . '/parametres/application');
                }
            }
        } else {
            $_POST = $this->sets->getArray();
            $_POST['value'] = $this->clSettings->decodeParam($_POST['value']);
            $_POST['id_categorie'] = (!empty($this->params['cat']) ? $this->params['cat'] : $_POST['id_categorie']);
        }
    }

    /**
     * Gestion de la suppression d'un paramètre de l'application.
     * Utilise le moteur Octeract.
     *
     * @function _deleteApp
     */
    public function _deleteApp()
    {
        $this->users->checkAccess('paramapplication');
        $this->autoFireNothing = true;
        $this->sets = new o\sets($this->params[0]);

        if ($this->sets->exist() && $this->sets->lock == 0) {
            $backLog = serialize($this->sets->getArray());
            $this->sets->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-paramdel', $this->language, 'Supression d\'un paramètre'), $this->sets->nom, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'param-app', $this->language, 'Paramètres application'), $this->ln->txt('admin-banner', 'param-del', $this->language, 'Le paramètre a bien été supprimé'));

            $this->redirect($this->lurl . '/parametres/application');
        } else {
            $this->redirect($this->lurl . '/parametres/application');
        }
    }

    /**
     * Gestion de la liste des catégories des paramètres
     * de l'application.
     * Utilise le moteur Octeract.
     *
     * @function _categories
     */
    public function _categories()
    {
        $this->users->checkAccess('parametrescategories');
        $this->ssMenuActive = 'parametrescategories';
        $this->sets_categories = new o\data('sets_categories');
    }

    /**
     * Gestion du formulaire d'ajout / édition d'une catégorie
     * des paramètres de l'application.
     * Utilise le moteur Octeract.
     *
     * @function _formCategories
     */
    public function _formCategories()
    {
        $this->users->checkAccess('parametrescategories');
        $this->ssMenuActive = 'parametrescategories';
        $this->loadJs('icheck.min');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('sweetalert');
        $this->sets_categories = new o\sets_categories($this->params[0]);
        $this->new = ($this->sets_categories->exist() ? false : true);

        if (isset($_POST['sendForm']) && $this->params[0] == $_POST['id_categorie']) {
            $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
            $this->errorFormNom = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['nom']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->sets_categories->getArray()) : '');
                $this->sets_categories->nom = $_POST['nom'];
                $this->sets_categories->lock = (isset($_POST['lock']) ? $_POST['lock'] : 0);
                $this->sets_categories->save();

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-parameditcat', $this->language, 'Modification d\'une catégorie') : $this->ln->txt('admin-logs', 'action-paramaddcat', $this->language, 'Ajout d\'une catégorie')), $this->sets_categories->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'param-categ', $this->language, 'Paramètres catégories'), (!$this->new ? $this->ln->txt('admin-banner', 'param-editcat', $this->language, 'La catégorie a bien été modifiée') : $this->ln->txt('admin-banner', 'param-addcat', $this->language, 'La catégorie a bien été ajoutée')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/parametres/formCategories/' . $this->sets_categories->id_categorie);
                } else {
                    $this->redirect($this->lurl . '/parametres/categories');
                }
            }
        } else {
            $_POST = $this->sets_categories->getArray();
        }
    }

    /**
     * Gestion de la suppression d'une catégorie
     * des paramètres de l'application.
     * Utilise le moteur Octeract.
     *
     * @function _deleteCategorie
     */
    public function _deleteCategorie()
    {
        $this->users->checkAccess('parametrescategories');
        $this->autoFireNothing = true;
        $this->sets_categories = new o\sets_categories($this->params[0]);

        if ($this->sets_categories->exist() && $this->sets_categories->lock == 0) {
            $backLog = serialize($this->sets_categories->getArray());
            $this->sets_categories->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-paramdelcat', $this->language, 'Supression d\'une catégorie'), $this->sets_categories->nom, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'param-categ', $this->language, 'Paramètres catégories'), $this->ln->txt('admin-banner', 'param-delcat', $this->language, 'La catégorie a bien été supprimée'));

            $this->redirect($this->lurl . '/parametres/categories');
        } else {
            $this->redirect($this->lurl . '/parametres/categories');
        }
    }

    /**
     * Gestion de la liste des paramètres globaux.
     * Utilise le moteur Octeract.
     *
     * @function _globaux
     */
    public function _globaux()
    {
        $this->users->checkAccess('paramglobaux');
        $this->ssMenuActive = 'paramglobaux';

        foreach ($this->clSettings->lTypes as $type) {
            $this->{'lParams' . $type} = new o\data('globals', array('type' => $type));
        }
    }

    /**
     * Gestion du formulaire d'ajout / édition d'un paramètre global.
     * Utilise le moteur Octeract.
     *
     * @function _formGlobaux
     */
    public function _formGlobaux()
    {
        $this->users->checkAccess('paramglobaux');
        $this->ssMenuActive = 'paramglobaux';
        $this->loadJs('icheck.min');
        $this->loadJs('bootstrap-datepicker');
        $this->loadJs('sweetalert.min');
        $this->loadCss('icheck');
        $this->loadCss('datepicker');
        $this->loadCss('sweetalert');
        $this->globals = new o\globals($this->params[0]);
        $this->new = ($this->globals->exist() ? false : true);

        if (isset($_POST['sendForm']) && $this->params[0] == $_POST['id_global']) {
            $_POST['nom'] = trim(htmlspecialchars($_POST['nom']));
            $_POST['type'] = trim(htmlspecialchars($_POST['type']));
            $this->errorFormNom = false;
            $this->errorFormType = false;
            $this->error = false;
            $this->errorMsg = '';

            if (strlen($_POST['nom']) == 0) {
                $this->errorFormNom = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            } elseif (strlen($_POST['type']) == 0) {
                $this->errorFormType = true;
                $this->error = true;
                $this->errorMsg = $this->ln->txt('admin-generic', 'post-obligatoire', $this->language, 'Vous devez renseigner ce champ');
            }

            if (!$this->error) {
                $backLog = (!$this->new ? serialize($this->globals->getArray()) : '');
                $this->globals->nom = $_POST['nom'];
                $this->globals->description = $_POST['description'];
                $this->globals->type = $_POST['type'];
                $this->globals->lock = (isset($_POST['lock']) ? $_POST['lock'] : 0);
                $this->globals->slug = (!$this->new ? $this->globals->slug : $this->clFonctions->controlSlug('globals', generateSlug($_POST['nom'])));
                $this->globals->value = $this->clSettings->handleFormBO($_POST['type'], $_POST, $_FILES, $this->globals->slug, $this->spath);
                $this->globals->save();
                $this->clFonctions->updateSlug($this->globals, $this->globals->id_global);

                $this->clFonctions->logging((!$this->new ? $this->ln->txt('admin-logs', 'action-parameditglob', $this->language, 'Modification d\'un paramètre global') : $this->ln->txt('admin-logs', 'action-paramaddglob', $this->language, 'Ajout d\'un paramètre global')), $this->globals->nom, $backLog);
                $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'param-globals', $this->language, 'Paramètres globaux'), (!$this->new ? $this->ln->txt('admin-banner', 'param-edit', $this->language, 'Le paramètre a bien été modifié') : $this->ln->txt('admin-banner', 'param-add', $this->language, 'Le paramètre a bien été ajouté')));

                if ($_POST['restePage'] == 1) {
                    $this->redirect($this->lurl . '/parametres/formGlobaux/' . $this->globals->id_global);
                } else {
                    $this->redirect($this->lurl . '/parametres/globaux');
                }
            }
        } else {
            $_POST = $this->globals->getArray();
            $_POST['value'] = $this->clSettings->decodeParam($_POST['value']);
            $_POST['type'] = (!empty($this->params['type']) ? urldecode($this->params['type']) : $_POST['type']);
        }
    }

    /**
     * Gestion de la suppression d'un paramètre global.
     * Utilise le moteur Octeract.
     *
     * @function _deleteGlobaux
     */
    public function _deleteGlobaux()
    {
        $this->users->checkAccess('paramglobaux');
        $this->autoFireNothing = true;
        $this->globals = new o\globals($this->params[0]);

        if ($this->globals->exist() && $this->globals->lock == 0) {
            $backLog = serialize($this->globals->getArray());
            $this->globals->delete();

            $this->clFonctions->logging($this->ln->txt('admin-logs', 'action-paramdel', $this->language, 'Supression d\'un paramètre'), $this->globals->nom, $backLog);
            $this->clFonctions->msgToast($this->ln->txt('admin-banner', 'param-globals', $this->language, 'Paramètres globaux'), $this->ln->txt('admin-banner', 'param-del', $this->language, 'Le paramètre a bien été supprimé'));

            $this->redirect($this->lurl . '/parametres/globaux');
        } else {
            $this->redirect($this->lurl . '/parametres/globaux');
        }
    }

    /**
     * Gestion de l'affichage du formulaire de type de paramètre.
     * AJAX
     *
     * @function _displayType
     */
    public function _displayType()
    {
        $this->users->checkAccess('parametres');
        $this->autoFireNothing = true;
        echo $this->clSettings->affichageFormBO($_POST['type'], $_POST['value'], $_POST['id_langue']);
    }

} 
