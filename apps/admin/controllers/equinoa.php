<?php

class equinoaController extends bootstrap
{

    /**
     * @var array $storageEngineList liste des moteurs de BDD
     * @var array $charsetList liste des charset de BDD
     */
    public $storageEngineList;
    public $charsetList;

    public function __construct($command, $config, $app)
    {
        parent::__construct($command, $config, $app);
        $this->menuActive = 'equinoa';
        $this->charsetList = bdd::showCharset();
        $this->storageEngineList = bdd::showEngines();
        if ($config['bdd_config'][$config['env']]['BDD'] == $config['bdd_config'][$config['env']]['BDD_COMMUN']) {
            $this->dbList = array($this->Config['bdd_config'][$this->Config['env']]['BDD']);
            $this->dbListTables = $this->bdd->getInnoDBTables($this->Config['bdd_config'][$this->Config['env']]['BDD']);
            sort($this->dbListTables);
        } else {
            $this->dbList = array(
                $this->Config['bdd_config'][$this->Config['env']]['BDD'],
                $config['bdd_config'][$config['env']]['BDD_COMMUN'],
            );
            $this->dbListTables = array_merge($this->bdd->getInnoDBTables($this->Config['bdd_config'][$this->Config['env']]['BDD']), $this->bdd->getInnoDBTables($config['bdd_config'][$config['env']]['BDD_COMMUN']));
            sort($this->dbListTables);
        }
    }

    /**
     * Gestion de la page par défaut de la rubrique.
     * Si pas de page par défaut on renvoie vers la première sous-rubrique
     * disponible pour le user connecté.
     * Utilise le moteur Octeract.
     *
     * @function _default
     */
    public function _default()
    {
        $this->users->checkAccess('equinoa');
        $this->autoFireNothing = true;
        $this->redirect($this->clFonctions->goZoneUser('equinoa'));
    }

    /**
     * Gestion de la page DB View.
     *
     * @function _db_view
     */
    public function _db_view()
    {
        $this->users->checkAccess('admidb');
        $this->ssMenuActive = 'admidb';
        $this->loadCss('dbview');
    }

}
