<?php

use PHPMailer\PHPMailer\PHPMailer;

/**
 * La classe clEmail regroupe un ensemble de methodes utilisées pour
 * l'envoi des mails transactionnels
 *
 * @class       clEmail
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://aspartam.io/license.txt
 */
class clEmail
{

    /**
     * @var string $env Environnement
     * @var string $lurl URL du site
     * @var string $surl URL du static
     * @var array $tabDefaultVars Tableau des variables par defaut
     */
    protected $env;
    protected $lurl;
    protected $surl;
    public $vars;

    /**
     * Constructeur de la classe
     *
     * @function __construct
     * @param string $env Environnement
     * @param string $lurl URL du site
     * @param string $surl URL du static
     */
    public function __construct($env, $lurl, $surl)
    {
        $this->env = $env;
        $this->lurl = $lurl;
        $this->surl = $surl;
        $this->vars = array(
            'url' => $this->lurl,
            'surl' => $this->surl,
        );
    }

    /**
     * Méthode qui envoi les mails NMP et Server
     *
     * @function sendMail
     * @param string $type_mail slug de l'email en bdd
     * @param string $id_langue langue de l'email
     * @param string $emailDest adresse mail du destinataire du mail
     * @param array $varMailComp tableau de variables de mail complémentaire
     * @return bool
     */
    public function sendMail($type_mail, $id_langue, $emailDest, $varMailComp = array())
    {
        // Recuperation du modele de mail
        $mails_text = new o\mails_text(array('type' => $type_mail, 'id_langue' => $id_langue));

        if (!$mails_text->exist()) {
            return false;
        }

        return $this->sendContentMail($mails_text, $emailDest, $varMailComp);
    }

    /**
     * @param $mails_text
     * @param $emailDest
     * @param array $varMailComp
     * @return bool
     */
    public function sendContentMail($mails_text, $emailDest, $varMailComp = array())
    {
        $emailDest = is_array($emailDest) ? $emailDest : explode(',', $emailDest);
        $this->clSettings = new clSettings((new o\sets()), (new o\globals()), (!empty($this->ln)?$this->ln:""), $this->surl);
        // Dev/Demo remplacement des destinataires.
        if ($this->env === 'dev' || $this->env === 'demo') {
            $devEmail =  $this->clSettings->getParam('mails-serveur-alias', 'sets');
            if (!empty($devEmail)) {
                $emailDest = [$devEmail];
            }
        }

        // Variables du mailing
        $this->vars = array_merge($this->vars, $varMailComp);

        // Construction du tableau avec les balises EMV
        $tabVar = $this->parseVars($this->vars);

        // Attribution des données aux variables
        $mails_text = $this->assignVars($mails_text, $tabVar);

        try {
            $mail = new PHPMailer(true);
            $this->send($mail, $emailDest, $mails_text);
            return true;
        } catch (\PHPMailer\PHPMailer\Exception $e) {
            error_log($e->errorMessage());
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
        return false;
    }

    /**
     * Méthode qui ajoute un évènement
     *
     * @function envoyerEvent
     * @param string $type_mail slug de l'email en bdd
     * @param string $id_langue langue de l'email
     * @param string|array $emailDest adresse mail du destinataire du mail
     * @param array $varMailComp tableau de variables de mail complémentaire
     * @return bool
     */
    public function sendEvent($type_mail, $id_langue, $emailDest, $varMailComp = array()): ?bool
    {
        $emailDest = is_array($emailDest) ? $emailDest : explode(',', $emailDest);

        // Dev/Demo remplacement des destinataires.
        if ($this->env === 'dev' || $this->env === 'demo') {
            $devEmail = $GLOBALS['clSettings']->getParam('mails-serveur-alias', 'sets');
            if (!empty($devEmail)) {
                $emailDest = [$devEmail];
            }
        }

        // Recuperation du modele de mail
        $mails_text = new o\mails_text(array('type' => $type_mail, 'id_langue' => $id_langue));
        if (!$mails_text->exist()) {
            return false;
        }

        // Variables du mailing
        $this->vars = array_merge($this->vars, [
            'summary' => 'Meeting',
            'startTime' => '',
            'endTime' => '',
            'location' => '',
            'description' => '',
            'request' => 'REQUEST'
        ], $varMailComp);


        try {
            // Envoi du mail
            $this->sendIcalEvent($mails_text, $emailDest);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @description mail pour ajouter un événement au calendrier
     * @help https://tools.ietf.org/html/rfc2446
     * @param $mails_text
     * @param $emailDest
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function sendIcalEvent($mails_text, $emailDest): void
    {
        // Construction du tableau avec les balises EMV
        $tabVar = $this->parseVars($this->vars);

        $status = ['PUBLISH' => 'CONFIRMED', 'REQUEST' => 'CONFIRMED', 'CANCEL' => 'CANCELLED'];

        // Attribution des données aux variables
        $mails_text = $this->assignVars($mails_text, $tabVar);

        $mail = new PHPMailer(true);

        $mail->addCustomHeader('MIME-version', '1.0');
        $mail->addCustomHeader('Content-Type', 'text/calendar;name="meeting.ics";method=REQUEST;charset=utf-8');
        $mail->addCustomHeader('Content-Transfer-Encoding', '8bit');
        $mail->addCustomHeader('X-Mailer', 'Microsoft Office Outlook 12.0');
        $mail->addCustomHeader('Content-class: urn:content-classes:calendarmessage');


        //Create Email Body (HTML)
        $mail->Ical = 'BEGIN:VCALENDAR' . "\r\n" .
            'PRODID:-//Microsoft Corporation//Outlook 15.0 MIMEDIR//EN' . "\r\n" .
            'CALSCALE:GREGORIAN' . "\r\n" .
            'VERSION:2.0' . "\r\n" .
            'METHOD:' . $this->vars['request'] . "\r\n" .
            'BEGIN:VTIMEZONE' . "\r\n" .
            'TZID:Europe/paris' . "\r\n" .
            'X-WR-TIMEZONE:Europe/paris' . "\r\n" .
            'BEGIN:STANDARD' . "\r\n" .
            'DTSTART:19810329T020000' . "\r\n" .
            'RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10' . "\r\n" .
            'END:STANDARD' . "\r\n" .
            'BEGIN:DAYLIGHT' . "\r\n" .
            'DTSTART:19961027T030000' . "\r\n" .
            'RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3' . "\r\n" .
            'TZOFFSETFROM:+0000' . "\r\n" .
            'TZOFFSETTO:+0200' . "\r\n" .
            'END:DAYLIGHT' . "\r\n" .
            'END:VTIMEZONE' . "\r\n" .
            'BEGIN:VEVENT' . "\r\n" .
            'ORGANIZER;CN="' . $mails_text->exp_name . '":MAILTO:' . $mails_text->exp_email . "\r\n" .
            'ATTENDEE;CN="' . implode(',', $emailDest) . '";ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:' . implode(',', $emailDest) . "\r\n" .
            'LAST-MODIFIED:' . date("Ymd\TGis") . "\r\n" .
            'UID:' . (isset($varMail['uid']) ? $this->vars['uid'] : date("Ymd\TGis", strtotime($this->vars['startTime'])) . rand() . '@exchangecore.com') . "\r\n" .
            'DTSTAMP:' . date("Ymd\TGis") . "\r\n" .
            'DTSTART:' . date("Ymd\THis", strtotime($this->vars['startTime'])) . "\r\n" .
            'DTEND:' . date("Ymd\THis", strtotime($this->vars['endTime'])) . "\r\n" .
            'TRANSP:OPAQUE' . "\r\n" .
            'SEQUENCE:1' . "\r\n" .
            'STATUS:' . $status[$this->vars['request']] . "\r\n" .
            'SUMMARY:' . $this->vars['summary'] . "\r\n" .
            'LOCATION:' . $this->vars['location'] . "\r\n" .
            'CLASS:PUBLIC' . "\r\n" .
            'PRIORITY:5' . "\r\n" .
            'X-MICROSOFT-CDO-BUSYSTATUS:BUSY' . "\r\n" .
            'X-MICROSOFT-CDO-IMPORTANCE:1' . "\r\n" .
            'X-MICROSOFT-DISALLOW-COUNTER:FALSE' . "\r\n" .
            'X-MS-OLK-AUTOFILLLOCATION:FALSE' . "\r\n" .
            'X-MS-OLK-CONFTYPE:0' . "\r\n" .
            'BEGIN:VALARM' . "\r\n" .
            'TRIGGER:-PT15M' . "\r\n" .
            'ACTION:DISPLAY' . "\r\n" .
            'DESCRIPTION:' . $this->vars['description'] . "\r\n" .
            'END:VALARM' . "\r\n" .
            'END:VEVENT' . "\r\n" .
            'END:VCALENDAR' . "\r\n";

        $this->send($mail, $emailDest, $mails_text);
    }

    /**
     * Méthode qui envoi les mails NMP et Server en Preview
     *
     * @function sendMailPreview
     * @param string $type_mail slug de l'email en bdd
     * @param string $id_langue langue de l'email
     * @param string|array $emailDest adresse mail du destinataire du mail
     * @param array $varMailcomp tableau de variables de mail complémentaire
     * @return bool
     */
    public function sendMailPreview($type_mail, $id_langue, $emailDest, $varMailcomp = array()): ?bool
    {
        $emailDest = is_array($emailDest) ? $emailDest : explode(',', $emailDest);

        // Recuperation du modele de mail
        $mails_text = new o\mails_text(array('type' => $type_mail, 'id_langue' => $id_langue));
        if (!$mails_text->exist()) {
            return false;
        }

        // Variables du mailing
        $this->vars = array_merge($this->vars, $varMailcomp);

        // Construction du tableau avec les balises EMV
        $tabVar = $this->parseVars($this->vars);

        // Attribution des données aux variables
        $mails_text->subject = strtr('[PREVIEW]' . $mails_text->subject, $tabVar);
        $mails_text->content = strtr($mails_text->content, $tabVar);

        try {
            $mail = new PHPMailer(true);
            $this->send($mail, $emailDest, $mails_text);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param PHPMailer $mail
     * @param array $emailDest
     * @param $mails_text
     * @throws \PHPMailer\PHPMailer\Exception
     */
    private function send(PHPMailer $mail, array $emailDest = [], $mails_text): void
    {

        if (!empty(MAIL_SERVER)) {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = MAIL_SERVER;
            $mail->SMTPAuth = true;
            $mail->Username = MAIL_LOGIN;
            $mail->Password = MAIL_PASSWORD;
            $mail->SMTPSecure = 'tls';
            $mail->Port = MAIL_PORT;
        }


        $mail->setFrom($mails_text->exp_email, $mails_text->exp_name);
        foreach ($emailDest as $email) {
            $mail->addAddress($email);
        }

        if (!empty($mails_text->reply_email)) {
            $mail->addReplyTo($mails_text->reply_email, $mails_text->reply_name);
        }

        $mail->CharSet = 'UTF-8';

        $mail->isHTML();
        $mail->Subject = $mails_text->subject;
        $mail->Body = $mails_text->content;
        $mail->AltBody = $mails_text->content;

        $mail->addCustomHeader('x-splio-ref', $this->vars['x-splio-ref'] ?? $this->env . '_' . $mails_text->type . '_' . date('Y-m-d'));
        $mail->addCustomHeader('x-splio-canal', $this->vars['x-splio-canal'] ?? $mails_text->type);
        $mail->addCustomHeader('x-splio-extid', $emailDest[0]);

        if ($mail->send() !== false) {
            foreach ($emailDest as $email) {
                $mails_filer = new o\mails_filer();
                $mails_filer->id_textemail = $mails_text->id_textemail;
                $mails_filer->email_destinataire = $email;
                $mails_filer->from = '"' . $mails_text->exp_name . '" <' . $mails_text->exp_email . '>';
                $mails_filer->to = $mails_text->exp_name;
                $mails_filer->subject = $mail->Subject;
                $mails_filer->content = $mail->Body;
                $mails_filer->headers = $mail->getSentMIMEMessage();
                $mails_filer->insert();
            }
        } else {
            if (!empty(MAIL_SERVER)) {
                error_log('MAIL_SERVER: ' . MAIL_SERVER);
            }

        }
    }

    /**
     * Méthode qui permet la construction des variables pour les mails serveur
     *
     * @function parseVars
     * @param array $tab tableau des variables
     * @return array
     */
    private function parseVars($tab): array
    {
        $result = array();

        foreach ($tab as $key => $value) {
            $result['[EMV DYN]' . $key . '[EMV /DYN]'] = $value;
        }

        return $result;
    }

    /**
     * @param $mails_text
     * @param $tabVar
     * @return mixed
     */
    private function assignVars($mails_text, $tabVar)
    {
        $tabVar['[EMV DYN]meta_title[EMV /DYN]'] = $tabVar['[EMV DYN]meta_title[EMV /DYN]'] ?? $mails_text->name;
        // Attribution des données aux variables
        $mails_text->subject = stripslashes(strtr($mails_text->subject, $tabVar));
        $mails_text->content = stripslashes(strtr($mails_text->content, $tabVar));
        $mails_text->exp_name = stripslashes(strtr($mails_text->exp_name, $tabVar));
        $mails_text->exp_email = stripslashes(strtr($mails_text->exp_email, $tabVar));
        return $mails_text;
    }

}
