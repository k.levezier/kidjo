<?php

/**
 * La classe clSettings regroupe un ensemble de methodes utilisées pour
 * l'affichage, l'utilisation et le traitement des paramètres
 * 
 * @class       clSettings
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2008-2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://leeloo.equinoa.net/licence.txt
 */
class clSettings {
    
    /**
     * @var object $sets Objet SETS passé à l'instanciation
     * @var object $globals Objet GLOBALS passé à l'instanciation
     * @var object $ln Objet LN passé à l'instanciation
     * @var string $surl Url static du site passée à l'instanciation
     * @var object $daVinciCode Objet DAVINCICODE instancié dans le constructeur
     * @var object $upload Objet UPLOAD instancié dans le constructeur
     * @var array $lTypes Tableau des différents types de paramètres
     */
    protected $sets;
    protected $globals;
    protected $ln;
    protected $surl;
    protected $daVinciCode;
    protected $upload;
    public $lTypes;

    /**
     * Constructeur de la classe
     * 
     * @function __construct
     * @param object $sets Instance de l'objet SETS
     * @param object $globals Instance de l'objet GLOBALS
     * @param object $ln Instance de l'objet LN
     * @param string $surl Url static du site
     */
    public function __construct($sets, $globals, $ln, $surl) {
        $this->sets = $sets;
        $this->globals = $globals;
        $this->ln = $ln;
        $this->surl = $surl;
        
        $this->daVinciCode = new daVinciCode(CRYPT_KEY);
        $this->upload = new upload();
        $this->lTypes = array('Texte', 'Fichier', 'Date', 'Booléen');
    }

    /**
     * Méthode qui permet d'encoder le paramètre
     * 
     * @function encodeParam
     * @param string $value Valeur du paramètre à encoder
     * @return string Valeur du paramètre encodé
     */
    public function encodeParam($value) {
        return $this->daVinciCode->encode($value);
    }

    /**
     * Méthode qui permet de décoder le paramètre
     * 
     * @function decodeParam
     * @param string $value Valeur du paramètre à décoder
     * @return string Valeur du paramètre décodé
     */
    public function decodeParam($value) {
        return $this->daVinciCode->decode($value);
    }

    /**
     * Méthode qui permet de récupérer le paramètre
     * 
     * @function getParam
     * @param string $slug Slug du paramètre à récupérer
     * @param string $base Nom de la table du paramètre avec sets ou globals
     * @return string Valeur du paramètre décodé
     */
    public function getParam($slug, $base) {
        $value = $this->{$base}->getParamValue($slug);

        return $this->decodeParam($value);
    }

    /**
     * Méthode qui permet l'affichage du paramètre dans le BO
     * 
     * @function affichageBO
     * @param string $type Type du paramètre
     * @param string $value Valeur du paramètre encodée
     * @return string Code html pour l'affichage du paramètre
     */
    public function affichageBO($type, $value) {
        $content = '';
        
        switch ($type) {
            case 'Fichier':
                $content = '
                <a href="' . $this->surl . '/var/uploads/params/' . $this->decodeParam($value) . '" target="_blank">' . $this->decodeParam($value) . '</a>';
                break;
            default:
                $content = $this->decodeParam($value);
                break;
        }

        return $content;
    }

    /**
     * Méthode qui permet l'affichage du formulaire dans le BO en fonction
     * du type de paramètre
     * 
     * @function affichageFormBO
     * @param string $type Type du paramètre
     * @param string $value Valeur du paramètre décodée
     * @param string $id_langue Code de la langue en cours
     * @return string Code html pour l'affichage du formulaire
     */
    public function affichageFormBO($type, $value, $id_langue) {
        $content = '';

        switch ($type) {
            case 'Texte':
                $content = '
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="valueTexte">' . $this->ln->txt('admin-parametres', 'param-lab-value', $id_langue, 'Valeur') . '</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="value" id="valueTexte">' . $value . '</textarea>
                    </div>
                </div>';
                break;

            case 'Fichier':
                $content = '
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="valuefichier">' . $this->ln->txt('admin-parametres', 'param-lab-value', $id_langue, 'Valeur') . '</label>
                    <div class="col-sm-10">
                        <div class="input-group m-b">
                            <label class="input-group-btn">
                                <button type="button" class="btn btn-primary" id="valuefichier">' . $this->ln->txt('admin-generic', 'select-file', $id_langue, 'Séléctionner un fichier') . '</button>
                                <input type="file" class="hide" name="value" id="fichierFile">
                                <input type="text" class="hide" name="value_old" value="' . $value . '">
                            </label>
                            <input type="text" class="form-control" id="fichierPath" placeholder="' . $this->ln->txt('admin-generic', 'no-file', $id_langue, 'Aucun fichier séléctionné') . '" disabled="disabled">
                        </div>
                        ' . ($value != "" ? '<div>' . $this->ln->txt('admin-generic', 'fichier-actuel', $id_langue, 'Fichier actuel') . ' : ' . $this->affichageBO($type, $this->encodeParam($value)) . '</div>' : '') . '
                    </div>
                </div>';
                break;

            case 'Date':
                $content = '
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="valueDatepicker">' . $this->ln->txt('admin-parametres', 'param-lab-value', $id_langue, 'Valeur') . '</label>
                    <div class="col-sm-3 input-group date" id="datePicker">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" class="form-control" name="value" id="valueDatepicker" value="' . $value . '">
                    </div>
                </div>';
                break;
            
            case 'Booléen':
                $content = '
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="valueBoleen">' . $this->ln->txt('admin-parametres', 'param-lab-value', $id_langue, 'Valeur') . '</label>
                    <div class="col-sm-10">
                        <div class="checkbox i-checks">
                            <input type="checkbox" name="value" id="valueBoleen" value="1"' . ($value == 1 ? ' checked' : '') . '>
                        </div>
                    </div>
                </div>';
                break;
        }
        
        return $content;
    }

    /**
     * Méthode qui permet le traitement de la valeur du paramètre reçu par
     * le formulaire du BO
     * 
     * @function handleFormBO
     * @param string $type Type du paramètre
     * @param array $post Tableau POST du formulaire
     * @param array $files Tableau FILES du formulaire
     * @param string $slug Slug du nom du paramètre
     * @param string $path Chemin jusqu'au dossier var du public/static ou public/default
     * @return string Valeur du paramètre encodé
     */
    public function handleFormBO($type, $post, $files, $slug, $path) {
        $return = '';
        
        switch ($type) {
            case 'Fichier':
                if (isset($files['value']) && $files['value']['name'] != '') {
                    $this->upload->setUploadDir($path, 'uploads/params/');

                    if ($this->upload->doUpload('value', $slug)) {
                        $return = $this->daVinciCode->encode($this->upload->getName());
                    } else {
                        $return = $this->daVinciCode->encode($post['value_old']);
                    }
                } else {
                    $return = $this->daVinciCode->encode($post['value_old']);
                }
                break;

            case 'Booléen':
                $return = $this->daVinciCode->encode((isset($post['value']) ? $post['value'] : '0'));
                break;

            default:
                $return = $this->daVinciCode->encode($post['value']);
                break;
        }

        return $return;
    }

}
