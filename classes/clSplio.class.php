<?php

use phpseclib\Net\SFTP;

class clSplio
{
    /**
     * Constructeur de la classe
     *
     * @function __construct
     */
    public function __construct(){ }

    /**
     * Service CURL vers SPLIO
     *
     * @function curl
     * @param string $method methode de CURL ("GET" / "POST" / "PUT" / "DELETE")
     * @param string $resource table de Splio ("fields" / "lists" / "contact" / "blacklist")
     * @param string $email_to_check email
     * @param array $parametre_splio parametre CURL pour SPLIO
     * @return array
     */
    public function curl($method="", $resource="", $email_to_check="", $parametre_splio=[])
    {
        $universe = "uriage_smtp";
        $pass = "1776b21bd6d0ce1ea98a7a93a1f5c29c16c400c4";

        $service_url = "https://".$universe.":".$pass."@s3s.fr/api/data/1.9/".$resource."/".$email_to_check;
        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        if (!empty($parametre_splio)) {
            $qstring = json_encode($parametre_splio);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $qstring);
        }
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Expect:"));
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $return = json_decode($curl_response, true);
        return $return;
    }

    /**
     * Vérifie si l'email est blacklisté
     *
     * @function isBlacklisted
     * @param string $email_to_check email à vérifier
     * @return bool
     */
    public function isBlacklisted($email_to_check)
    {
        $retour = $this->curl("GET", "blacklist", $email_to_check);
        return ($retour["code"] == "200" ? 1 : 0);
    }

    /**
     * Vérifie l'email et l'ajoute ou le met à jour dans SPLIO
     *
     * @function subscribeSplio
     * @param string $email_to_check email à vérifier et mettre à jour
     * @return bool
     */
    public function subscribeSplio($email_to_check)
    {
        $email_to_check = trim($email_to_check);

        if(!empty($email_to_check)) {
            $id_campaign_to_subscribe = 1;

            // On va chercher toutes les infos de commandes pour ce user qui a dit oui à la NL
            $contact_newsletter = new o\newsletters(array('email' => $email_to_check));

            if($contact_newsletter->exist()){
                // On check s'il est blacklisté ou non
                $isBlacklisted = $this->isBlacklisted($contact_newsletter->email);

                // si l'email est blacklisté, on met le statut newsletter à 0. Terminé
                if ($isBlacklisted) {
                    $contact_newsletter->status = 0; // ou -1 ?
                    $contact_newsletter->update();
                    return false;
                } else {
                    //Sinon on continue
                    // Si le client.. n'est pas client PE
                    if($contact_newsletter->id_client == 0){
                        $moy_montant = 0;
                        $max_montant = 0;
                        $nb_commande = 0;
                        $is_buyer = 0;
                        $isClient = 0;
                    } else {
                        // Sinon on récupère les informations de transactions du client
                        $transactions = new o\data("transactions");
                        $transac = $transactions->addWhere("id_client = " . $contact_newsletter->id_client);
                        $moy_montant = intval($transac->avg("montant")->montant->getArray()[0]) / 100;
                        $max_montant = intval($transac->max("montant")->montant->getArray()[0]) / 100;
                        $nb_commande = $transac->count();
                        $is_buyer = ($nb_commande > 0 ? 1 : 0);
                        $isClient = 1;
                    }

                    // Retourne les informations enregistrées dans SPLIO pour cet email
                    $result = $this->curl("GET", "contact", $contact_newsletter->email);

                    // Si l'email n'a pas été créé dans SPLIO
                    if (isset($result["code"]) && $result["code"] == "404") {
                        // requete créant le "compte" email dans SPLIO du user
                        $query = array('email' => $contact_newsletter->email, 'firstname' => $contact_newsletter->prenom, 'lastname' => $contact_newsletter->nom, 'lang' => $contact_newsletter->id_langue, 'fields' => array( array('id' => '0', 'value' => $contact_newsletter->added), array('id' => '2', 'value' => 'oui'), array('id' => '3', 'value' => (preg_match("#collecte#",$contact_newsletter->origine) ? 'collecte' : 'site')), array('id' => '11', 'value' => $is_buyer), array('id' => '12', 'value' => $nb_commande), array('id' => '13', 'value' => $isClient), array('id' => '14', 'value' => $moy_montant), array('id' => '15', 'value' => $max_montant)), 'lists' => array( array('id' => $id_campaign_to_subscribe)));

                        // Création dans SPLIO
                        $this->curl("POST", "contact", "", $query);
                    } else {
                        // Sinon (Si le mail existe déjà dans SPLIO)
                        // On recherche dans les informations renvoyées, s'il est abonné à la NL (faisant parti de la liste)
                        $inList = false;
                        foreach ($result["lists"] as $list) {
                            if ($list['id'] == $id_campaign_to_subscribe) {
                                $inList = true;
                            }
                        }

                        //Si les informations ont changé, on met à jour les infos sur SPLIO
                        if ($result["fields"][2]["value"] != 'oui' || $result["fields"][3]["value"] != 'site' || $result["fields"][11]["value"] != $is_buyer || $result["fields"][12]["value"] != $nb_commande || $result["fields"][13]["value"] != ($contact_newsletter["id_client"] != 0 ? 1 : 0) || $result["fields"][14]["value"] != $moy_montant || $result["fields"][15]["value"] != $max_montant || !$inList) {

                            // On met à jour le "compte" email du user dans SPLIO
                            $query = array('firstname' => $contact_newsletter->prenom, 'lastname' => $contact_newsletter->nom, 'fields' => array(array('id' => '2', 'value' => 'oui'), array( 'id' => '3', 'value' => (preg_match("#collecte#",$contact_newsletter->origine) ? 'collecte' : 'site')), array('id' => '11', 'value' => $is_buyer), array('id' => '12', 'value' => $nb_commande), array('id' => '13', 'value' => $isClient), array( 'id' => '14', 'value' => $moy_montant), array('id' => '15', 'value' => $max_montant), ), 'lists' => array(array('id' => $id_campaign_to_subscribe)));

                            // Mise à jour dans SPLIO
                            $this->curl("PUT", "contact", $contact_newsletter->email, $query);
                        }
                    }
                    $contact_newsletter->status = 1; // ou -1 ?
                    $contact_newsletter->update();

                    return true;
                }
            } else {

                // si l'email est blacklisté, on met le statut newsletter à 0. Terminé
                $client_infos = new o\clients(array('email' => $email_to_check));

                // Sinon on récupère les informations de transactions du client
                $transactions = new o\data("transactions");
                $transac = $transactions->addWhere("id_client = " . $client_infos->id_client);
                $moy_montant = intval($transac->avg("montant")->montant->getArray()[0]) / 100;
                $max_montant = intval($transac->max("montant")->montant->getArray()[0]) / 100;
                $nb_commande = $transac->count();
                $is_buyer = ($nb_commande > 0 ? 1 : 0);
                $isClient = 1;

                // Retourne les informations enregistrées dans SPLIO pour cet email
                $result = $this->curl("GET", "contact", $client_infos->email);

                // Si l'email n'a pas été créé dans SPLIO
                if (isset($result["code"]) && $result["code"] == "404") {
                    // requete créant le "compte" email dans SPLIO du user
                    $query = array('email' => $client_infos->email, 'firstname' => $client_infos->prenom, 'lastname' => $client_infos->nom,
                        'lang' => $client_infos->id_langue,'fields' => array(array('id' => '0', 'value' => $client_infos->added), array('id' => '2', 'value' => 'oui'), array('id' => '3', 'value' => 'site'), array('id' => '11', 'value' => $is_buyer), array('id' => '12', 'value' => $nb_commande), array('id' => '13', 'value' => $isClient), array('id' => '14', 'value' => $moy_montant), array('id' => '15', 'value' => $max_montant)), 'lists' => array(array('id' => $id_campaign_to_subscribe)));

                    // Création dans SPLIO
                    $this->curl("POST", "contact", "", $query);
                } else {
                    // Sinon (Si le mail existe déjà dans SPLIO)
                    // On recherche dans les informations renvoyées, s'il est abonné à la NL (faisant parti de la liste)
                    $inList = false;
                    foreach ($result["lists"] as $list) {
                        if ($list['id'] == $id_campaign_to_subscribe) {
                            $inList = true;
                        }
                    }

                    //Si les informations ont changé, on met à jour les infos sur SPLIO
                    if ($result["fields"][2]["value"] != 'oui' || $result["fields"][3]["value"] != 'site' || $result["fields"][11]["value"] != $is_buyer || $result["fields"][12]["value"] != $nb_commande || $result["fields"][13]["value"] != ($contact_newsletter["id_client"] != 0 ? 1 : 0) || $result["fields"][14]["value"] != $moy_montant || $result["fields"][15]["value"] != $max_montant || !$inList) {

                        // On met à jour le "compte" email du user dans SPLIO
                        $query = array('firstname' => $client_infos->prenom, 'lastname' => $client_infos->nom, 'fields' => array(array('id' => '2', 'value' => 'oui'), array( 'id' => '3', 'value' => 'site'), array('id' => '11', 'value' => $is_buyer), array('id' => '12', 'value' => $nb_commande), array('id' => '13', 'value' => $isClient), array( 'id' => '14', 'value' => $moy_montant), array('id' => '15', 'value' => $max_montant), ), 'lists' => array(array('id' => $id_campaign_to_subscribe)));

                        // Mise à jour dans SPLIO
                        $this->curl("PUT", "contact", $client_infos->email, $query);
                    }
                }
                $contact_newsletter->status = 1;
                $contact_newsletter->id_langue = $client_infos->id_langue;
                $contact_newsletter->id_client = $client_infos->id_client;
                $contact_newsletter->email = $client_infos->email;
                $contact_newsletter->nom = $client_infos->nom;
                $contact_newsletter->prenom = $client_infos->prenom;
                $contact_newsletter->origine = $_SESSION['url_origine'];
                $contact_newsletter->save();

                return true;
            }
        } else {
            return false;
        }
    }

    public function unsubscribeSplio($email_to_check)
    {
        $email_to_check = trim($email_to_check);
        if(!empty($email_to_check)) {

            $id_campaign_to_unsubscribe = 1;

            // On va désinscrire de la base
            $contact_newsletter = new o\newsletters(array('email' => $email_to_check));

            //Si les informations ont changé, on met à jour les infos sur SPLIO
            $contact_newsletter->status = 0; // ou -1 ?
            $contact_newsletter->update();

            // On met à jour le "compte" email du user dans SPLIO
            $query = array(
                'lists' => array(
                    array(
                        'id' => $id_campaign_to_unsubscribe,
                        "action"=> "unsubscribe"
                    )
                ));

            // Mise à jour dans SPLIO
            $this->curl("PUT", "contact", $contact_newsletter->email, $query);
            return true;

        } else {
            return false;
        }
    }


    /**
     * Connexion avec le SFTP distant
     * @return SFTP
     *
     */
    private function connectSftp(){
        $sftp = new SFTP(Splio_SFTP_host);
        if (!$sftp->login(Splio_SFTP_login, Splio_SFTP_pass)) {
            exit('Login Failed');
        }
        return $sftp;
    }

    public function processFileSDS(){

        $sftp = $this->connectSftp();

        //Récupération de la liste des fichiers dispos sur le serveurs
        $lFilesDirectory = $sftp->nlist(Splio_SFTP_path_SDS);

        //exclude item
        $tabExcludeItem = array('.','..','archivage');

        if(count($lFilesDirectory) > 3){
            foreach ($lFilesDirectory as $file){
                if(!in_array($file, $tabExcludeItem)){
                    //check si le fichier a déjà été traité chez nous
                    $splio_sds_files = new o\data('splio_sds_files',array('file' => $file));
                    $nbTraitementsFichier = $splio_sds_files->addWhere('processed_at != "0000-00-00 00:00:00"')->countAll();

                    if ($nbTraitementsFichier === 0) {
                        // traiter le fichier
                        $this->processFile($sftp, Splio_SFTP_path_SDS.$file);
                    }
                }
            }
        }
    }

    private function processFile($sftp, $filename){

        //Liste status desabo (On traite les statuts suivant comme un desabo car les contacts seront blacklistés :
        //Soft : softbounce
        //Hard : hardbounce
        //Fbl : declaration spam
        $LStatusDesabo = array('unsub','soft', 'hard', 'fbl');

        //Récupération du contenu
        $data = $sftp->get($filename);

        if( $data != ""){

            $lines = explode("\n",$data);

            if(count($lines) > 1){

                $nbLine = $nbLignesTraitees = 0;
                foreach($lines as $line){

                    if($nbLine > 0) {

                        $line = utf8_decode($line);
                        $content = explode('|', $line);
                        $content = str_replace('"','',$content);
                        $hash = md5($line);
                        $CampaignName = $content[0];
                        $ContactID = $content[2];
                        $Statusattends = $content[4];

                        if(in_array($Statusattends,$LStatusDesabo ) && $ContactID != ""){

                            //Récupération du client
                            $clients = new o\clients(array('email' => $ContactID));
                            if($clients->exist()){

                                $nmp_desabo                 = new o\nmp_desabo();
                                $nmp_desabo->id_client      = $clients->id_client;
                                $nmp_desabo->email          = $ContactID;
                                $nmp_desabo->raison         = $CampaignName;
                                $nmp_desabo->commentaire    = $line;
                                $nmp_desabo->save();


                                //on retire le client de la liste des nl
                                $nl = new o\newsletters(array('email' => $clients->email));
                                $nl->delete();

                                $nbLignesTraitees++;
                            }
                        }
                    }
                    $nbLine++;
                }
            }
        }

        //on enregistre le fait qu'on ait traité le fichier
        $splio_sds_files = new o\splio_sds_files();
        $splio_sds_files->file = $filename;
        $splio_sds_files->nb_ligne_traitees = $nbLignesTraitees;
        $splio_sds_files->processed_at = date('Y-m-d H:i:s');
        $splio_sds_files->save();

    }
}
