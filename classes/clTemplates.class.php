<?php

use o\data;

/**
 * La classe clTemplates regroupe un ensemble de methodes utilisées pour
 * l'affichage, l'utilisation et le traitement des templates
 *
 * @class       clTemplates
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2008-2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://leeloo.equinoa.net/licence.txt
 */
class clTemplates {

    /**
     * @var array $lTypes Tableau des différents types de templates
     * @var array $lTypesProduit Tableau des différents types de produits
     * @var array $lElements Tableau des différents types d'éléments
     * @var string $surl Url statique du site
     * @var object $ln Objet LN passé à l'instanciation
     * @var object $upload Objet UPLOAD instancié dans le constructeur
     * @var object $currentLn langue en cours
     */
    public $lTypes;
    public $lTypesProduit;
    public $lElements;
    protected $surl;
    protected $ln;
    protected $currentLn;
    protected $upload;

    /**
     * Constructeur de la classe
     *
     * @param string $surl Url statique du site
     * @param object $ln Objet LN passé à l'instanciation
     * @param object $currentLn langue en cours
     *
     * @function __construct
     */
    public function __construct($surl="", $ln=null, $currentLn='fr') {
        $this->lTypes = array('Page', 'Produit', 'Formulaire');
        $this->lTypesProduit = array(0 => 'Produit', 1 => 'Cadeau', 2 => 'Echantillon');
        $this->lElements = array('Texte', 'Textearea', 'Texte Editor', 'Lien', 'Image', 'Fichier', 'Checkbox', 'Radio Button', 'Select', 'Datepicker');

        $this->surl = $surl;
        $this->ln = $ln;
        $this->currentLn = $currentLn;

        $this->upload = new upload();
    }

    /**
     * Méthode qui permet de changer la langue par default
     *
     * @function setLng
     * @param string $lng nouvelle langue
     */
    public function setLng($lng='') {
        if($lng != '')
            $this->currentLn = $lng;
    }

    /**
     * Méthode qui permet de créer le code html pour afficher la liste des templates
     * dans un select Back Office
     *
     * @function getTemplateSelect
     * @param array $tabListe Tableau de la liste des templates
     * @param string $selected Contenu de la valeur actuelle
     * @return string Code HTML de la liste des templates
     */
    public function getTemplateSelect($tabListe, $selected) {
        $htmlListe = '';

        foreach($tabListe as $type => $liste) {
            $htmlListe .= '<optgroup label="' . $type .'">';

            foreach ($liste as $tpl) {
                $htmlListe .= '<option value="' . $tpl['id_template'] . '"' . ($selected == $tpl['id_template'] ? ' selected' : '') . '>' . $tpl['name'] . '</option>';
            }

            $htmlListe .= '</optgroup>';
        }

        return (string) $htmlListe;
    }

    /**
     * Méthode qui permet de créer le code html pour afficher l'arborescence
     * dans un select Back Office
     *
     * @function getArboSelect
     * @param array $tabArbo Tableau des pages de l'arborescence du site
     * @param string $selected Contenu de la valeur actuelle
     * @param string $indentation Texte pour l'indentation dans le select
     * @param string $htmlArbo Code HTML de l'arborescence pour la recursivité
     * @return string Code HTML de l'arborescence
     */
    public function getArboSelect($tabArbo, $selected, $indentation = '', $htmlArbo = '') {
        $indentation .= ($htmlArbo != '' ? '---' : '');
        $htmlArbo = '';

        foreach ($tabArbo as $page) {
            $htmlArbo .= '<option value="' . $page['id_tree'] . '"' . ($selected == $page['id_tree'] ? ' selected' : '') . '>' . ($indentation != '' ? $indentation . '&nbsp;' : $indentation) . $page['menu_title'] . '</option>';

            if (count($page['children']) > 0) {
                $htmlArbo .= $this->getArboSelect($page['children'], $selected, $indentation, $htmlArbo);
            }
        }

        return (string) $htmlArbo;
    }

    /**
     * Méthode qui permet de créer le code html pour afficher l'arborescence
     * dans un select Back Office
     *
     * @function getArboSelect
     * @param array $tabArbo Tableau des pages de l'arborescence du site
     * @param string $selected Contenu de la valeur actuelle
     * @param string $indentation Texte pour l'indentation dans le select
     * @param string $htmlArbo Code HTML de l'arborescence pour la recursivité
     * @return string Code HTML de l'arborescence
     */
    public function getArboSelectForDocs($tabArbo, $selected, $indentation = '', $htmlArbo = '') {
        $indentation .= ($htmlArbo != '' ? '---' : '');
        $htmlArbo = '';

        foreach ($tabArbo as $page) {
            $htmlArbo .= '<option value="' . $page['id_tree'] . '"' . (is_array($selected) && in_array($page['id_tree'],$selected) ? ' selected' : '') . '>' . ($indentation != '' ? $indentation . '&nbsp;' : $indentation) . $page['menu_title'] . '</option>';

            if (count($page['children']) > 0) {
                $htmlArbo .= $this->getArboSelectForDocs($page['children'], $selected, $indentation, $htmlArbo);
            }
        }


        return (string) $htmlArbo;
    }

    /**
     * Méthode qui permet de créer le code html pour afficher la liste des produits
     * dans un select Back Office
     *
     * @function getProduitsSelect
     * @param string $selected Contenu de la valeur actuelle
     * @return string Code HTML de l'arborescence
     */
    public function getProduitsSelect($selected) {
        $htmlPdt = '';
        $produits = new o\data('produits', array('status' => 1));

        foreach ($produits->type->distinct()->order('type', 'ASC') as $type) {
            $htmlPdt .= '<optgroup label="' . $this->lTypesProduit[$type] . '">';

            foreach ($produits as $pdt) {
                $htmlPdt .= '<option value="' . $pdt->id_produit . '"' . ($pdt->id_produit == $selected ? ' selected' : '') . '>' . $pdt->getName($this->currentLn) . '</option>';
            }
            $htmlPdt .= '</optgroup>';
        }

        return (string) $htmlPdt;
    }

    /**
     * Méthode qui permet de créer le code html pour afficher l'arborescence
     * dans le Back Office
     *
     * @function getArboSite
     * @param array $tabArbo Tableau des pages de l'arborescence du site
     * @param string $htmlArbo Code HTML de l'arborescence pour la recursivité
     * @return string Code HTML de l'arborescence
     */
    public function getArboSite($tabArbo, $htmlArbo = '') {
        $htmlArbo = '<ul>';

        foreach ($tabArbo as $page) {
            $htmlArbo .= '
            <li data-id="' . $page['id_tree'] . '"' . (count($page['children']) == 0 ? ' data-jstree=\'{"type":"page"}\'' : '') . ($page['id_tree'] == 1 ? ' class="clicOnLoad"' : '') . '>
                <span title="' . $page['title'] . '">' . $page['menu_title'] . ($page['prive']==1?' <i class="fa fa-lock"></i>':' <i class="fa fa-unlock text-danger"></i>') . ' | </span>
                <span>
                    ' . ($page['up'] ? '<a onclick="movePage(' . $page['id_tree'] . ',\'up\');"><i class="fa fa-arrow-up arboTree-Action"></i></a>' : '') . '
                    ' . ($page['down'] ? '<a onclick="movePage(' . $page['id_tree'] . ',\'down\');"><i class="fa fa-arrow-down arboTree-Action"></i></a>' : '') . '
                    <a onclick="formPage(' . $page['id_tree'] . ');"><i class="fa fa-edit arboTree-Action"></i></a>
                </span>';

            if (count($page['children']) > 0) {
                $htmlArbo .= $this->getArboSite($page['children'], $htmlArbo);
            }

            $htmlArbo .= '
            </li>';
        }

        $htmlArbo .= '</ul>';

        return (string) $htmlArbo;
    }

    /**
     * Méthode qui permet l'affichage du formulaire dans le BO en fonction
     * du type d'élément
     *
     * @function affichageFormBO
     * @param string $element Element à afficher
     * @param object $pdt_tree_elements tree_element ou produit_element
     * @param bool $is_tree $tree_element ou produit_element
     * @param string $forcedType override du type d'élément
     * @param string $forcedName override du name de l'élément
     * @param string $forcedValue override de la value de l'élément
     * @param string $forcedLabel override du label de l'élément
     * @param array $forcedComplements override des complements utilisé pour la liste des RB et des Select
     * @return string Code html pour l'affichage du formulaire
     */
    public function affichageFormBO($element, $pdt_tree_elements, $is_tree=false, $forcedType='', $forcedName='', $forcedValue='', $forcedLabel='', $forcedComplements=array(), $currentLn='') {
        $content = '';
        if($is_tree !== true){
            $id=$element->id_element;
            $type = $element->type_element;
            $label = $element->name;
        } else {
            $id=$forcedName;
            $type = $forcedType;
            $label = $forcedLabel;
            $complements = $forcedComplements;
        }

        switch ($type) {
            case 'Texte':
                $content .= '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.='<label class="control-label col-sm-10">' . $label . '</label>'.
                        '<div class="btn-group btn-group-lang col-sm-4">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                        <a class="change-lang-form" data-lang="'.$key.'">
                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                            <span class="nom_lang"> '.$ln.'</span>
                                        </a>
                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div>';
                }else{
                    $content.='<label class="control-label col-sm-10">' . $label . '</label>';
                }
                $content .= '</div>';
                $content .='<div class="col-sm-8">';
                foreach ($this->ln->tabLangues as $key => $langues) {
                    if($is_tree !== true){
                        $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                        $value =$pdt_tree_element->value;
                    } else {
                        $value = $forcedValue['value_'.$forcedName.'_'.$key];
                    }
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                    $content.='<input type="text" value="'. $value .'" name="value_'.$id.'_'.$key.'" class="form-control '.$class.'" />';
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Textearea':
                $content .= '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.='<label class="control-label col-sm-10">' . $label . '</label>'.
                        '<div class="btn-group btn-group-lang col-sm-4">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div>';
                }
                $content .='</div>';
                $content .='<div class="col-sm-8">';
                foreach ($this->ln->tabLangues as $key => $langues) {
                    if($is_tree !== true){
                        $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                        $value =$pdt_tree_element->value;
                    } else {
                        $value = $forcedValue['value_'.$forcedName.'_'.$key];
                    }
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                    $content.='<textarea name="value_'.$id.'_'.$key.'" class="form-control textarea '.$class.'">'. $value .'</textarea>';
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Texte Editor':
                $content .= '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.='<label class="control-label col-sm-10">' . $label . '</label>'.
                        '<div class="btn-group btn-group-lang col-sm-4">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div>';
                }
                $content .='</div>';
                $content .='<div class="col-sm-8">';
                foreach ($this->ln->tabLangues as $key => $langues) {
                    if($is_tree !== true){
                        $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                        $value =$pdt_tree_element->value;
                    } else {
                        $value = $forcedValue['value_'.$forcedName.'_'.$key];
                    }
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                    $content.='<div class="editor-wrapper '.$class.'"><textarea name="value_'.$id.'_'.$key.'" class="form-control">'. $value .'</textarea></div>';
                    $content.='<script>CKEDITOR.replace("value_'.$id.'_'.$key.'");</script>';
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Lien':
                $content .= '
                <div class="form-group">';
                $content .= '<div class="col-sm-4"><label class="control-label col-sm-10">' . $label . '</label></div>';
                $content .= '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.=
                        '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                        <a class="change-lang-form" data-lang="'.$key.'">
                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                            <span class="nom_lang"> '.$ln.'</span>
                                        </a>
                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div></div>';
                }
                $content .='</div>';
                $content .='<div class="col-sm-8">';
                $pages = new o\data('tree', array('id_langue'=>'en','id_parent'=>NULL));
                $pdts = new o\data('produits');
                $cpt =1;
                $cpt += (int)$pages->countAll()>0;
                $cpt += (int)$pdts->countAll()>0;

                foreach($this->ln->tabLangues as $key=>$ln) {
                    $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                    $content .= '<div class="input-group m-b '.$class.'">';
                    // Lien interne vers une page
                    if($pages->countAll()>0){
                        $content .='<div class="col-sm-'.(12/$cpt).'"><select class="form-control" name="value_'.$id.'_'.$key.'[]">'
                            . '<option value="">'.$this->ln->txt('admin-generic', 'select-page', $this->currentLn, 'Séléctionner une page').'</option>';
                        foreach ($pages as $p){
                            $content .='<option value="'.$p->id_tree.'" '.($pdt_tree_element->complement=="L" && $pdt_tree_element->value==$p->id_tree?'selected':'').'>'.$p->menu_title.'</option>';
                            $content = $this->nextPage($p->id_tree,$pdt_tree_element,1,$content);
                        }
                        $content .='</select></div>';
                    }

                    // Lien externe
                    $content .='<div class="col-sm-'.(12/$cpt).'"><input type="text" name="value_'.$id.'_'.$key.'[]" class="form-control col-sm-'.(12/$cpt).'" '.($pdt_tree_elements->complement=="LX"?$pdt_tree_elements->value:'').'></div>';
                    $content .= '</div>';
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Image':
                $content = '
                <div class="form-group">';
                $content .= '<div class="col-sm-4"><label class="control-label col-sm-10">' . $label . '</label></div>';
                $content .= '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.=
                        '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                        <a class="change-lang-form" data-lang="'.$key.'">
                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                            <span class="nom_lang"> '.$ln.'</span>
                                        </a>
                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div></div>';
                }
                $content .='</div>';
                $content .='<div class="col-sm-8">';
                foreach($this->ln->tabLangues as $key=>$ln) {
                    if($is_tree !== true){
                        $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                        $value =$pdt_tree_element->value;
                    } else {
                        $value = $forcedValue['value_'.$forcedName.'_'.$key];
                    }
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';

                    $content .= '
                                <div class="input-group m-b '.$class.'">
                                    <label class="input-group-btn">
                                        <button type="button" class="btn btn-primary valuefichier">' . $this->ln->txt('admin-generic', 'select-file', $this->currentLn, 'Séléctionner un fichier') . '</button>
                                        <input type="file" class="hide fichierFile" name="value_'.$id.'_'.$key.'" accept=".jpg,.jpeg,.png">
                                        <input type="text" class="hide" name="value_old_'.$id.'_'.$key.'" id="value_old_'.$id.'_'.$key.'" value="' . $value . '">
                                    </label>
                                    <input type="text" class="form-control fichierPath" placeholder="' . $this->ln->txt('admin-generic', 'no-file', $this->currentLn, 'Aucun fichier séléctionné') . '" disabled="disabled">
                                </div>
                                ' . ($value != "" ? '<div class="'.$class.'">' . $this->ln->txt('admin-generic', 'fichier-actuel', $this->currentLn, 'Fichier actuel') . ' : ' . ' <img src="'.$this->surl.'/var/images/'. $value .'" style="max-height:100px;" id="preview_old_'.$id.'_'.$key.'"/>' . '<a href="#" onclick="$(\'#value_old_'.$id.'_'.$key.'\').val(\'\');$(\'#preview_old_'.$id.'_'.$key.'\').attr(\'src\',\'\');return false;">Supprimer</a></div>' : '');

                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Fichier':
                $content = '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">';
                $content .= '<label class="control-label col-sm-10">' . $label . '</label>'.
                    '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.=
                        '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div></div>';
                }
                $content .='</div>';
                $content .='<div class="col-sm-8">';
                foreach($this->ln->tabLangues as $key=>$ln) {
                    $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';

                    $content .= '
                                <div class="input-group m-b '.$class.'">
                                    <label class="input-group-btn">
                                        <button type="button" class="btn btn-primary valuefichier">' . $this->ln->txt('admin-generic', 'select-file', $this->currentLn, 'Séléctionner un fichier') . '</button>
                                        <input type="file" class="hide fichierFile" name="value_'.$id.'_'.$key.'" >
                                        <input type="text" class="hide" name="value_old_'.$id.'_'.$key.'" value="' . $pdt_tree_element->value . '">
                                    </label>
                                    <input type="text" class="form-control fichierPath" placeholder="' . $this->ln->txt('admin-generic', 'no-file', $this->currentLn, 'Aucun fichier séléctionné') . '" disabled="disabled">
                                </div>
                                ' . ($pdt_tree_element->value != "" ? '<div class="'.$class.'">' . $this->ln->txt('admin-generic', 'fichier-actuel', $this->currentLn, 'Fichier actuel') . ' : ' . ' <a href="'.$this->surl.'/var/fichiers/'. $pdt_tree_element->value .'" target="_blank">'. $pdt_tree_element->value .'</a>' . '</div>' : '');
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Datepicker':
                $content = '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">';
                $content .= '<label class="control-label col-sm-10">' . $label . '</label>'.
                    '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.=
                        '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div></div>';
                }
                $content .='</div>';
                $content .='<div class="col-sm-8">';
                foreach($this->ln->tabLangues as $key=>$ln) {
                    $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';

                    $content .= '
                                <div class="input-group m-b '.$class.'">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control datePicker" name="value_'.$id.'_'.$key.'" id="value_'.$id.'_'.$key.'" value="' . $pdt_tree_element->value . '">'
                        .'</div>';
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Checkbox':
                $content = '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">';
                $content .= '<label class="control-label col-sm-10">' . $label . '</label>'.
                    '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.=
                        '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div></div>';
                }
                $content .='</div>';
                $content .='<div class="col-sm-8">';
                foreach($this->ln->tabLangues as $key=>$ln) {
                    $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';

                    $content .= '
                                <div class="checkbox i-checks '.$class.'">
                                    <input type="checkbox" name="value_'.$id.'_'.$key.'" id="value_'.$id.'_'.$key.'" value="1"' . ($pdt_tree_element->value == 1 ? ' checked' : '') . '>
                                </div>';
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Radio Button':
                $content .= '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">
                    <label class="control-label col-sm-10">' . $label . '</label>';

                if (count($this->ln->tabLangues) > 1) {
                    $content.='<div class="btn-group btn-group-lang col-sm-4">
                            <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                        <a class="change-lang-form" data-lang="'.$key.'">
                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                            <span class="nom_lang"> '.$ln.'</span>
                                        </a>
                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div>';
                }
                $content .= '</div>';
                $content .='<div class="col-sm-8">';
                foreach ($this->ln->tabLangues as $key => $langues) {
                    if($is_tree !== true){
                        $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                        $valueOld =$pdt_tree_element->value;
                    } else {
                        $valueOld = $forcedValue['value_'.$forcedName.'_'.$key];
                    }
                    $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                    $content.='<fieldset class="'.$class.'">';

                    if($is_tree !== true){
                        $complements = unserialize($element->complement);
                    } else {
                        // Déjà setted
                    }
                    if($complements !== false && count($complements)>0){
                        foreach($complements as $value => $name){
                            $content.='<div class="col-sm-5"><div class="floatingRB radio i-checks">'
                                .       '<input type="radio" value="'.$value.'" id="value_'.$id.'_'.$key.'_'.$value.'" name="value_'.$id.'_'.$key.'" '.($valueOld==$value?' checked ':'').'>'
                                .       '<label for="value_'.$id.'_'.$key.'_'.$value.'">'.$name.'</label>'
                                . '</div></div>';
                        }
                    }
                    $content.='</fieldset>';
                }
                $content.='</div>'
                    .'</div>';
                break;

            case 'Select':
                $content .= '
                <div class="form-group">';
                $content .= '<div class="col-sm-4">';
                if (count($this->ln->tabLangues) > 1) {
                    $content.='<label class="control-label col-sm-10">' . $label . '</label>'.
                        '<div class="btn-group btn-group-lang col-sm-4">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$this->currentLn.'.png" alt="'.$this->currentLn.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                    foreach ($this->ln->tabLangues as $key => $ln) {
                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                    }
                    $content.=
                        '</ul>
                            </div>';
                }
                $content .= '</div>';
                $content .='<div class="col-sm-8">';
                foreach ($this->ln->tabLangues as $key => $langues) {
                    if($is_tree !== true){
                        $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];
                        $pdt_tree_element = $pdt_tree_elements->where('id_langue',$key)[0];

                        $class = ($key == $this->currentLn?'':' hidden_field_lang').' field_lang_'.$key.' ';
                        $content.='<select name="value_'.$id.'_'.$key.'" class="form-control '.$class.'">';
                        $complements = unserialize($element->complement);
                        if($complements !== false && count($complements)>0){
                            foreach($complements as $value => $name){
                                $content.='<option value="'.$value.'" '.($pdt_tree_element->value==$value?' selected ':'').'>'.$name.'</option>';
                            }
                        }
                        $content.='</select>';
                    } else {
                        // Ca marche pas
                    }

                }
                $content.='</div>'
                    .'</div>';
                break;
        }

        return $content;
    }
    function nextPage($id_tree,$pdt_tree_element,$level,$content)
    {
        $level++;
        $pages = new o\data('tree', array('id_langue'=>'en','id_parent'=>$id_tree));
        $pages->order('ordre','ASC');
        $indent = "";
        for($i=1;$i<=$level;$i++)
            $indent .= '--- ';
        if($pages->countAll()>0){

            foreach ($pages as $p){
                $content .='<option value="'.$p->id_tree.'" '.($pdt_tree_element->complement=="L" && $pdt_tree_element->value==$p->id_tree?'selected':'').'>'.($indent) . $p->menu_title.'</option>';
                $content = $this->nextPage($p->id_tree,$pdt_tree_element,$level,$content);
            }

        }
        return $content;
    }
    /**
     * Méthode qui permet le traitement de la valeur d'un element de template reçu par
     * le formulaire du BO
     *
     * @function handleFormBO
     * @param object $table_element table d'elements (tree_elements ou produits_elements ou blocs_elements)
     * @param object $element element courant
     * @param string $lang langue courante
     * @param array  $post Tableau POST du formulaire
     * @param array  $files Tableau FILES du formulaire
     * @param string $slug Slug de l'élément
     * @param string $path Chemin jusqu'au dossier var du public/static ou public/default
     *
     */
    public function handleFormBO($table_element, $element, $lang, $post, $files, $slug, $path) {
        $table_element->id_langue = $lang;
        switch ($element->type_element) {
            case 'Lien':
                // Spécificité : Ne retenir que le dernier element non vide et remplir le champ complément.
                $table_element->value = $table_element->complement = '';
                $cpt = 0;
                foreach ($post['value_'.$element->id_element.'_'.$lang] as $i => $val){
                    if($val != ''){
                        $table_element->value = $val;
                        $cpt = $i;
                    }
                }
                switch($cpt){ case 0: $table_element->complement='L'; break;  case 1: $table_element->complement='P'; break;  case 2: $table_element->complement='LX'; break;}
                break;

            case 'Fichier':case 'Image':
            if (isset($files['value_'.$element->id_element.'_'.$lang]) && $files['value_'.$element->id_element.'_'.$lang]['name'] != '') {
                $this->upload->setUploadDir($path, strtolower($element->type_element).'s/');

                if ($this->upload->doUpload('value_'.$element->id_element.'_'.$lang, $slug)) {
                    $val = $this->upload->getName();
                } else {
                    $val = $post['value_old_'.$element->id_element.'_'.$lang];
                }
            } else {
                $val = $post['value_old_'.$element->id_element.'_'.$lang];
            }
            $table_element->value = $val;
            break;

            case 'Checkbox':
                $table_element->value = (isset($post['value_'.$element->id_element.'_'.$lang]) ? $post['value_'.$element->id_element.'_'.$lang] : '0');
                break;

            default: case 'Select': case 'Radio Button':case 'Datepicker':case 'Texte Editor':case 'Textearea':case 'Texte':
            $table_element->value = $post['value_'.$element->id_element.'_'.$lang];
            break;
        }
        $table_element->save();
    }
}
