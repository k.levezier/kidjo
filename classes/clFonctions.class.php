<?php

/**
 * La classe clFonctions regroupe un ensemble de methodes utilisées un peu 
 * partout sur la plateforme
 * 
 * @class       clFonctions
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2008-2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://leeloo.equinoa.net/licence.txt
 */
class clFonctions {

    /**
     * @var string $lurl Url du site avec la langue
     * @var string $surl Url statique du site
     * @var object $ln Objet LN passé à l'instanciation
     * @var object $currentLn langue en cours
     */
    protected $lurl;
    protected $surl;
    protected $ln;
    protected $currentLn;
    
    /**
     * @const suffixe_slug Suffixe temporaire pour le slug
     */
    const suffixe_slug = "-tmp";

    /**
     * Constructeur de la classe
     * 
     * @function __construct
     * @param string $lurl Url du site avec la langue
     * @param string $surl Url statique du site
     * @param object $ln Objet LN passé à l'instanciation
     * @param object $currentLn langue en cours
     */
    public function __construct($lurl, $surl='', $ln=null, $currentLn='fr') {
        $this->lurl = $lurl;
        $this->surl = $surl;
        $this->ln = $ln;
        $this->currentLn = $currentLn;
    }

    /**
     * Méthode de logging des actions effectuées sur la plateforme
     * L'enregistrement ne se fait que si la constante ACTIVATE_LOGSBO est à TRUE
     * Utilise le moteur Octeract.
     * 
     * @function logging
     * @param string $action Nom de l'action effectuée
     * @param string $details Détails de l'action effectuée
     * @param string $complement Complément d'information sur l'action effectuée
     */
    public function logging($action, $details = '', $complement = '') {
        if (ACTIVATE_LOGSBO) {
            $logs = new o\logs();
            $logs->id_site = $_SESSION['infosSite']['id_site'];
            $logs->id_user = $_SESSION['user']['id_user'];
            $logs->action = $action;
            $logs->details = $details;
            $logs->complement = $complement;
            $logs->ip = (filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') != '' ? filter_input(INPUT_SERVER, 'HTTP_X_FORWARDED_FOR') : filter_input(INPUT_SERVER, 'REMOTE_ADDR'));
            $logs->insert();
        }
    }

    /**
     * Méthode de mise en session des messages pour les alertes Toast
     * 
     * @function msgToast
     * @param string $title Titre du message
     * @param string $msg Texte du message
     */
    public function msgToast($title, $msg) {
        $_SESSION['msgToast']['title'] = $title;
        $_SESSION['msgToast']['msg'] = $msg;
    }

    /**
     * Méthode qui permet de déterminer la page d'atterissage d'un utilisateur
     * quand il arrive dans une zone.
     * Utilise le moteur Octeract.
     * 
     * @function goZoneUser
     * @param string $checksecure Zone appelée
     * @return string L'url qui servira pour la redirection
     */
    public function goZoneUser($checksecure) {
        $users = new o\users($_SESSION['user']['id_user']); 
        $call_zone = new o\data('zones', array('checksecure' => $checksecure));
        $call_zone->limit(1);
        $slug = '';    
        
        if (isset($_SESSION['user']) && $users->exist()) {
            if(count($call_zone) > 0) {                
                foreach ($call_zone as $zone) {
                    $childs_zone = new o\data('zones', array('id_parent' => $zone->id_zone, 'users_zones->id_user' => $users->id_user));
                    $childs_zone->order('ordre','ASC','users_zones')->limit(1);
                    foreach ($childs_zone as $uz) {
                        $slug = $uz->slug_destination;
                    }
                }
                return $this->lurl . '/'.$slug;
            } else {
                return $this->lurl . '/login';
            }
        } else {
            return $this->lurl . '/login';
        }
    }
    
    /**
     * Méthode qui permet de controler l'unicité du slug dans une table.
     * Utilise le moteur Octeract.
     * 
     * @function controlSlug
     * @param string $table Nom de la table
     * @param string $slug Slug à controller
     * @param array $conditions Tableau des conditions de l'unicité en plus du slug
     * @param bool $new = false si c'est un edit, et dans ce cas, $condition sert à ne pas tester le slug lui-même
     * @return string Le slug
     */
    public function controlSlug($table, $slug, $conditions = array(), $new = true) {
        $tabslug = array('slug' => $slug);
        if($new===false){
            $where = $tabslug;
            $addWhere = array();
            foreach ($conditions as $field => $value){                
                $addWhere[] = $field.'="'.$value.'"';
            }
            $addWhere = 'NOT ('.implode(' AND ', $addWhere).')';
        } else {
            $where = array_merge($tabslug, $conditions);   
            $addWhere = ' 1 ';
        }     
        $tocheck = new o\data($table, $where);
        
        if(count($tocheck->addWhere($addWhere)) > 0) {
            return $slug. self::suffixe_slug;
        } else {
            return $slug;
        }   
    }
    
    /**
     * Méthode qui permet de mettre à jour le slug en cas de doublons.
     * Utilise le moteur Octeract.
     * 
     * @function updateSlug
     * @param object $obj Instance de la ligne a checker
     * @param string $suffixe Suffixe à rajouter au slug
     */
    public function updateSlug($obj, $suffixe) {        
        
        if(strpos($obj->slug, self::suffixe_slug) !== false) {
            $obj->slug = str_replace(self::suffixe_slug, '-' . $suffixe, $obj->slug);
            $obj->save();
        } elseif($obj->slug == '') {
            $obj->slug = $obj->getTable().'-'.$suffixe;
        }
    }
    
    /**
     * Méthode qui permet l'affichage du formulaire dans le BO en fonction
     * du type d'attribut (gère le multilangue)
     * 
     * @function affichageFormBO
     * @param string $type Type du paramètre
     * @param string $value Valeur du paramètre décodée
     * @param string $id_langue Code de la langue en cours
     * @return string Code html pour l'affichage du formulaire
     */
    public function affichageFormBOAttributs($type, $value, $id_langue) {
        $content = '';
        
        switch ($type) {
            case 'Texte':
                $content = '
                <div class="hr-line-dashed"></div>
                <div class="form-group">';
                    $content .= '<div class="col-sm-2">';
                        if (count($this->ln->tabLangues) > 1) {
                            $content.=
                            '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$id_langue.'.png" alt="'.$id_langue.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                                    foreach ($this->ln->tabLangues as $key => $ln) {
                                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                                    }
                                $content.=
                                '</ul>
                            </div>';
                        }
                        $content .='<label class="control-label floatRight">' . $this->ln->txt('admin-parametres', 'param-lab-value', $this->currentLn, 'Valeur') . '</label>'
                    .'</div>';
                        $content .='<div class="col-sm-10">';
                        foreach($this->ln->tabLangues as $key=>$ln) {
                        $class = ($key == $id_langue?'':' hidden_field_lang').' field_lang_'.$key.' ';
                        $content.='<input type="text" value="'. $value[$key] .'" name="value_'.$key.'" class="form-control '.$class.'" />';
                        }
                    $content.='</div>'
                .'</div>';
                break;
            
            case 'Couleur':
                $content = '
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <div class="col-sm-2">';
                        if (count($this->ln->tabLangues) > 1) {
                            $content.=
                            '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$id_langue.'.png" alt="'.$id_langue.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                                    foreach ($this->ln->tabLangues as $key => $ln) {
                                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                                    }
                                $content.=
                                '</ul>
                            </div>';
                        }
                        $content .='<label class="control-label floatRight">' . $this->ln->txt('admin-parametres', 'param-lab-value', $this->currentLn, 'Valeur') . '</label>'
                    .'</div>';
                    $content .='<div class="col-sm-10">';
                        foreach($this->ln->tabLangues as $key=>$ln) {
                        $class = ($key == $id_langue?'':' hidden_field_lang').' field_lang_'.$key.' ';
                        $content.='<div class="input-group color '.$class.'">
                                      <span class="input-group-addon"><i></i></span>
                                      <input type="text" value="'. $value[$key] .'" name="value_'.$key.'" class="form-control" />
                                   </div>';
                        }
                    $content.='</div>'
                .'</div>';
                break;

            case 'Fichier':                
                $content = '
                <div class="hr-line-dashed"></div>
                <div class="form-group">';
                    $content .= '<div class="col-sm-2">';
                        if (count($this->ln->tabLangues) > 1) {
                            $content.=
                            '<div class="btn-group btn-group-lang">
                                <button data-toggle="dropdown" class="btn btn-white dropdown-toggle button-lang-form" aria-expanded="false">
                                    <img src="'.$this->surl.'/images/admin/flags/'.$id_langue.'.png" alt="'.$id_langue.'" /> <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">';
                                    foreach ($this->ln->tabLangues as $key => $ln) {
                                        $content.='<li>
                                                        <a class="change-lang-form" data-lang="'.$key.'">
                                                            <img src="'.$this->surl.'/images/admin/flags/'.$key.'.png" alt="'.$ln.'" />
                                                            <span class="nom_lang"> '.$ln.'</span>
                                                        </a>
                                                   </li>';
                                    }
                                $content.=
                                '</ul>
                            </div>';
                        }
                        $content .='<label class="control-label floatRight">' . $this->ln->txt('admin-parametres', 'param-lab-value', $this->currentLn, 'Valeur') . '</label>'
                    .'</div>';
                        $content .='<div class="col-sm-10">';
                        foreach($this->ln->tabLangues as $key=>$ln) {
                            $class = ($key == $id_langue?'':' hidden_field_lang').' field_lang_'.$key.' ';

                            $content .= '
                                <div class="input-group m-b '.$class.'">
                                    <label class="input-group-btn">
                                        <button type="button" class="btn btn-primary valuefichier">' . $this->ln->txt('admin-generic', 'select-file', $this->currentLn, 'Séléctionner un fichier') . '</button>
                                        <input type="file" class="hide fichierFile" name="value_'.$key.'" accept=".jpg,.jpeg,.png">
                                        <input type="text" class="hide" name="value_old_'.$key.'" value="' . (isset($value[$key])?$value[$key]:'') . '">
                                    </label>
                                    <input type="text" class="form-control fichierPath" placeholder="' . $this->ln->txt('admin-generic', 'no-file', $this->currentLn, 'Aucun fichier séléctionné') . '" disabled="disabled">
                                </div>
                                ' . ($value != "" ? '<div class="'.$class.'">' . $this->ln->txt('admin-generic', 'fichier-actuel', $this->currentLn, 'Fichier actuel') . ' : ' . (isset($value[$key])?' <img src="'.$this->surl.'/var/uploads/attributs/'. $value[$key] .'"/>':'') . '</div>' : '');

                        }
                    $content.='</div>'
                .'</div>';
                break;
        }
        
        return $content;
    }

    /**
     * Méthode qui retourne le prix HT en fonction du prix ttc et de la TVA
     * 
     * @function getHT
     * @param float $prix_ttc Prix TTC
     * @param float $tva TVA
     * @return float prix HT
     */
    public function getHT($prix_ttc, $tva){
        return $prix_ttc - ($prix_ttc * $tva / 100);
    }
    
    /**
     * Méthode qui retourne un slug nettoyé des paramètres après "?"
     * 
     * @function cleanSlug
     * @param string $slug
     * @return string slug clean
     */
    public function cleanSlug($slug){
        return (substr($slug,0,1) == '?'?'':$slug);
    }
    
    /**
     * Méthode qui retourne le lien selon son complément (L, LX, P)
     * 
     * @function getLink
     * @param string $element - blocs_elements ou menus_content ou tree_elements
     * @return string lien
     */
    public function getLink($element){
        $link = "";
        if($element->complement != '')
        {
            switch($element->complement)
            {
                case 'L':
                    if($element->value != '')
                    {
                        $tree = new o\tree(array('id_tree' => $element->value, 'id_langue' => $this->currentLn, 'status' => 1));
                        if($tree->exist())
                            $link = $this->lurl.'/'.$tree->slug;
                    }                    
                    break;
                case 'LX':
                    $link = $element->value . '" target="_blank';
                    break;
                case 'P':
                    $produit = new o\produits(array('id_produit' => $element->value, 'id_langue' => $this->currentLn, 'status' => 1));
                    if($produit->exist())
                    {
                        // Récupération de la combinaison par défaut
                        $defaultCombi = $produit->getDefaultCombi();
                        
                        // Matière
                        $slugMatiereDefault = $defaultCombi->getMatiere($this->currentLn);
                        
                        // Couleur 
                        $slugCouleurDefault = $defaultCombi->getCouleur($this->currentLn);
                        
                        $link = $this->lurl.'/'.$produit->getCategorieSlug($this->currentLn).'/'.$produit->getSlug($this->currentLn).'/'.$slugMatiereDefault.'/'.$slugCouleurDefault;
                    }
                    break;
            }
        }
        return $link;
    }
    

}
