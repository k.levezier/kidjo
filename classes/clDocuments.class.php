<?php

use o\data;

/**
 * La classe clDocuments regroupe un ensemble de methodes utilisées pour
 * l'affichage, l'utilisation et le traitement des documents
 * 
 * @class       clDocuments
 * @author      Equinoa Digital Agency - http://www.equinoa.com
 * @copyright   2008-2016 Equinoa Digital Agency
 * @version     1.0
 * @license     http://leeloo.equinoa.net/licence.txt
 */
class clDocuments {
    
    /**
     * @var object $sets Objet SETS passé à l'instanciation
     * @var object $globals Objet GLOBALS passé à l'instanciation
     * @var object $ln Objet LN passé à l'instanciation
     * @var string $surl Url static du site passée à l'instanciation
     * @var object $daVinciCode Objet DAVINCICODE instancié dans le constructeur
     * @var object $upload Objet UPLOAD instancié dans le constructeur
     * @var array $lTypes Tableau des différents types de paramètres
     */
    protected $sets;
    protected $globals;
    protected $ln;
    protected $surl;

    /**
     * Constructeur de la classe
     * 
     * @function __construct
     * @param object $sets Instance de l'objet SETS
     * @param object $globals Instance de l'objet GLOBALS
     * @param object $ln Instance de l'objet LN
     * @param string $surl Url static du site
     */
    public function __construct() {


    }

   
    public function isInZone($id_client,$id_zone)
    {
        $line = (new o\clients_geozones(array('id_client'=>$id_client)))->addWhere('id_zone='.$id_zone);

        if($line->exist())
            return true;
        else 
            return false;
    }
    
    public function isInProfile($id_client,$id_profile)
    {
        
    }
    
    public function isDocInZone($id_document,$id_zone)
    {
        
    }
    
    public function isDocInProfile($id_document,$id_profile)
    {
        
    }
    
    function showSize($size)
    {
        if($size>1024*1024*1024)
        {
            return round($size/(1024*1024*1024),2).' Go';
        }
        elseif($size>1024*1024)
        {
            return round($size/(1024*1024),2).' Mo';
        }
        elseif($size>1024)
        {
            return round($size/(1024),2).' Ko';
        }
    }
    
    function handleUpload($path,$params=NULL)
    {
        $id = (empty($params[1]) || $params[1]=="ftp"?0:$params[1]);
        $name = 'name_'.$params[0];
        $file = 'file_'.$params[0];
        $size = 'size_'.$params[0];
        $thumbnail = 'thumbnail_'.$params[0];
        $preview = 'preview_'.$params[0];
        $extension = 'extension_'.$params[0];
        $this->path = $path;

        if (!empty($_FILES) || !empty($_POST['file_temp'])) {
            ini_set("upload_tmp_dir",$path.'tmp/' );
            error_log(ini_get('upload_tmp_dir'));
            error_log(disk_free_space ('/tmp'));
            if(!empty($_FILES)){
                $file_name = str_replace(['é'],['é'],$_FILES['file']['name']);

                error_log(serialize($_FILES));
                $tempFile = $_FILES['file']['tmp_name'];          //3
                $ext = explode('.',$file_name);
                $fileExt = strtolower($ext[count($ext)-1]);
                $fileName = substr($file_name,0,strlen($file_name)-strlen($fileExt)-1);
                $targetPath = dirname(dirname(getcwd())) . '/protected/documents/';  //4
                $nomPasTropLong = substr($fileName,0,25);
                $fn = date('ymd-His').'-'.str_replace(' ','_',$nomPasTropLong).'.'.$fileExt;
                $fnDecli = date('ymd-His').'-'.str_replace(' ','_',$nomPasTropLong).'-'.$fileExt;
                $targetFile =  $targetPath. $fn;  //5

                move_uploaded_file($tempFile,$targetFile); //6


            }elseif(!empty($_POST['file_temp'])){
                $tempFile = $path.'protected/documents/tmp/'.$_POST['file_temp'];          //3

                $fichier = end(explode('/',$_POST['file_temp']));
                $ext = explode('.',$fichier);
                $fileExt = strtolower($ext[count($ext)-1]);
                $fileName = substr($fichier,0,strlen($fichier)-strlen($fileExt)-1);
                $targetPath = $path . 'protected/documents/';  //4
                $nomPasTropLong = substr($fileName,0,25);
                $fn = date('ymd-His').'-'.str_replace(' ','_',$nomPasTropLong).'.'.$fileExt;
                $fnDecli = date('ymd-His').'-'.str_replace(' ','_',$nomPasTropLong).'-'.$fileExt;
                $targetFile =  $targetPath. $fn;  //5

                rename($tempFile,$targetFile); //6
                $file_name = $fn;
            }

            if($id!=0)
                $this->documents = new o\documents(array('id_document'=>(int)$id));
            else
                $this->documents = new o\documents();

            if($_SESSION['client']['id_client']>0)
                $this->documents->status=1;
            $this->documents->$name = $file_name;
            if(empty($this->documents->label))
                $this->documents->label = $file_name;
            $this->documents->$file = $fn;
            $this->documents->id_user = ($_SESSION['user']['id_user']>0?$_SESSION['user']['id_user']:1);
            $this->documents->id_client = ($_SESSION['client']['id_client']>0?$_SESSION['client']['id_client']:0);

            $this->documents->$size = filesize($targetFile);
            $this->documents->$extension = $fileExt;

                if($fileExt=='pdf')
                {
                    $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'[0]');
                    $im->setImageFormat('jpg');
                    $im->scaleImage(200,200,true);
                    $im = $im->flattenImages();
                    //header('Content-Type: image/jpeg');
                    $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);

                    //print_r($im);die;

                }
                if($fileExt=='pptx' || $fileExt=='ppt')
                {
                    if(!file_exists(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.pdf'))
                    {
                        exec('curl -H "Accept: application/octet-stream" -F "File=@'.dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'" https://v2.convertapi.com/convert/'.$fileExt.'/to/pdf?Secret=Mcs422pUUgET2J17 > '.dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.pdf');
                      //  exec('/usr/sbin/container-run oxeva/centos7 curl -L \'https://api.cloudconvert.com/convert\' -F inputformat=\''.$fileExt.'\' -F outputformat=\'pdf\' -F file=@\''.dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'\' -F \'apikey=5nRFFinyCYM87MJdum5nx_QvMmA_MW5G-VAng2ECuDd5MHb3r6ydyuEUdWSnoZyi0DhtefDyVCoDK09xGYA3GQ\' -F \'input=upload\' -F \'download=inline\' > \''.dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fn.'.pdf'.'\'');
                        $this->documents->$preview = $fnDecli.'.pdf';
                        //mail('t.raymond@equinoa.com','debug docs','/usr/sbin/container-run oxeva/centos7 curl -L \'https://api.cloudconvert.com/convert\' -F inputformat=\''.$fileExt.'\' -F outputformat=\'pdf\' -F file=@\''.dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'\' -F \'apikey=5nRFFinyCYM87MJdum5nx_QvMmA_MW5G-VAng2ECuDd5MHb3r6ydyuEUdWSnoZyi0DhtefDyVCoDK09xGYA3GQ\' -F \'input=upload\' -F \'download=inline\' > \''.dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fn.'.pdf'.'\'');
                        $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$this->documents->$preview.'[0]');
                        $im->setImageFormat('jpg');
                        $im->scaleImage(200,200,true);
                        $im = $im->flattenImages();
                        //header('Content-Type: image/jpeg');
                        $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                        $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                    }
                    
                }
                if($fileExt=='docx' || $fileExt=='doc')
                {
                    if(!file_exists(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.pdf'))
                    {
                        exec('curl -H "Accept: application/octet-stream" -F "File=@'.dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'" https://v2.convertapi.com/convert/'.$fileExt.'/to/pdf?Secret=Mcs422pUUgET2J17 > '.dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.pdf');
                        $this->documents->$preview = $fnDecli.'.pdf';
                        $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$this->documents->$preview.'[0]');
                        $im->setImageFormat('jpg');
                        $im->scaleImage(200,200,true);
                        $im = $im->flattenImages();
                        //header('Content-Type: image/jpeg');
                        $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                        $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                    }
                }
                if($fileExt=='mov')
                {
                    exec(dirname(dirname(getcwd())) . '/bin/ffmpeg-4.1.3-amd64-static/ffmpeg -i "' . dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'" -vcodec h264 -acodec aac -strict -2 -y -movflags +faststart "' . dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.mp4"');
                    if(file_exists(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.mp4'))
                        $this->documents->$preview = $fnDecli.'.mp4';

                    exec(dirname(dirname(getcwd())) . '/bin/ffmpeg-4.1.3-amd64-static/ffmpeg -i "' . dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'" -ss 00:00:15.000 -vframes 1 "' . dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-thumb.jpg"');
                    if(file_exists(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-thumb.jpg'))
                        $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                }
                if($fileExt=='mp4' || $fileExt=='avi')
                {
                    exec(dirname(dirname(getcwd())) . '/bin/ffmpeg-4.1.3-amd64-static/ffmpeg -i "' . dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'" -ss 00:00:15.000 -vframes 1 "' . dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-thumb.jpg"');
                    if(file_exists(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-thumb.jpg'))
                        $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                }

                if($fileExt=='jpg' || $fileExt=='jpeg')
                {
                    $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'[0]');
                    $im->thumbnailImage(600,600,true);
                    //header('Content-Type: image/jpeg');
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-bigthumb.jpg');;
                    $im->thumbnailImage(200,200,true);
                    //header('Content-Type: image/jpeg');
                    $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                    //print_r($im);die;

                }
                if($fileExt=='png')
                {
                    $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'[0]');
                    $im->thumbnailImage(600,600,true);
                    //header('Content-Type: image/jpeg');
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-bigthumb.png');;
                    $im->thumbnailImage(200,200,true);
                    //header('Content-Type: image/jpeg');
                    $this->documents->$thumbnail = $fnDecli.'-thumb.png';
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                    //print_r($im);die;

                }
                if($fileExt=='psd')
                {
                    $i = new Imagick();
                    $i->setRegistry('temporary-path', dirname(dirname(getcwd())) . '/tmp/');
                    $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'[0]');
                    $im->setRegistry('temporary-path', dirname(dirname(getcwd())) . '/tmp/');
                    $im->setImageFormat('jpg');
                    $im->scaleImage(500,500,true);
                    //header('Content-Type: image/jpeg');
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-bigthumb.jpg');;
                    $im->scaleImage(200,200,true);
                    //header('Content-Type: image/jpeg');
                    $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                    

                }
                if($fileExt=='tiff' || $fileExt=='tif')
                {
                    $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'');
                    $im->setImageFormat('jpg');
                    $im->scaleImage(500,500,true);
                    //header('Content-Type: image/jpeg');
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$fnDecli.'-bigthumb.jpg');;
                    $im->scaleImage(200,200,true);
                    //header('Content-Type: image/jpeg');
                    $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                    //print_r($im);die;

                }
                if($fileExt=='eps')
                {
                    if(!file_exists(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.pdf'))
                    {
                        exec('/usr/sbin/container-run oxeva/centos7 curl -L \'https://api.cloudconvert.com/convert\' -F inputformat=\''.$fileExt.'\' -F outputformat=\'pdf\' -F file=@\''.dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'\' -F \'apikey=5nRFFinyCYM87MJdum5nx_QvMmA_MW5G-VAng2ECuDd5MHb3r6ydyuEUdWSnoZyi0DhtefDyVCoDK09xGYA3GQ\' -F \'input=upload\' -F \'download=inline\' > \''.dirname(dirname(getcwd())) . '/protected/documents/previews/'.$fnDecli.'.pdf'.'\'');
                        $this->documents->$preview = $fnDecli.'.pdf';

                        $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/previews/'.$this->documents->$preview.'[0]');
                        $im->setImageFormat('jpg');
                        $im->scaleImage(200,200,true);
                        $im = $im->flattenImages();
                        //header('Content-Type: image/jpeg');
                        $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                        $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                    }
                }
                if($fileExt==''&& is_dir(dirname(dirname(getcwd())) . '/protected/documents/'.$fn) && file_exists(dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'/PDF/'.$fn.'.pdf'))
                {
                    $im = new imagick(dirname(dirname(getcwd())) . '/protected/documents/'.$fn.'/PDF/'.$fn.'.pdf[0]');
                    $im->setImageFormat('jpg');
                    $im->scaleImage(200,200,true);
                    //header('Content-Type: image/jpeg');
                    $this->documents->$thumbnail = $fnDecli.'-thumb.jpg';
                    $im->writeImage(dirname(dirname(getcwd())) . '/protected/documents/thumbs/'.$this->documents->$thumbnail);
                }
            $this->documents->indexed = 0;
            $this->documents->save();

            $this->deleteArchive($this->documents->id_document);

            return $this->documents->id_document;
        }
    }

    function listeDocumentZoneProfil($id_tree){
        $zonesOk = [];
        $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($zones as $z){
            $zonesOk[] = $z->id_geozone;
        }

        $profilesOk = [];
        $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($profiles as $p){
            $profilesOk[] = $p->id_profile;
        }

        $this->documents = new data('documents_tree',['id_tree'=>(int)$id_tree]);
        //$this->documents->order('ordre','ASC','');
        $lDocuments = [];
        foreach($this->documents as $doc) {
            $zoneDone = false;
            $zones = new data('documents_geozones',['id_document'=>$doc->id_document]);

            if(count($zones)==0){
                $zoneDone = true;
            }else{
                foreach($zones as $key=>$z){
                    if(in_array($z->id_geozone,$zonesOk)){$zoneDone = true; break;}
                }
            }

            $profilDone = false;
            $profiles = new data('documents_profiles',['id_document'=>$doc->id_document]);
            foreach($profiles as $p){
                if(in_array($p->id_profile,$profilesOk)){$profilDone = true; break;}
            }

            if($zoneDone && $profilDone){
                $docDetail = new o\documents(array('id_document'=>(int)$doc->id_document));
                if($docDetail->status==1){
                    $ordre = ($docDetail->new==1?1:2).str_pad($doc->ordre, 3, "0", STR_PAD_LEFT);
                    $lDocuments[$ordre.$docDetail->updated.$docDetail->id_document] = $docDetail;
                }
            }
        }
        ksort($lDocuments);
        return (array) $lDocuments;
    }

    function authorized($id_document=NULL){

        $zonesOk = [];
        $zones = new data('clients_geozones',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($zones as $z){
            $zonesOk[] = $z->id_geozone;
        }

        $profilesOk = [];
        $profiles = new data('clients_profiles',['id_client'=>$_SESSION['client']['id_client']]);
        foreach($profiles as $p){
            $profilesOk[] = $p->id_profile;
        }

        $zoneDone = false;
        $zones = new data('documents_geozones',['id_document'=>$id_document]);

        if(count($zones)==0){
            $zoneDone = true;
        }else{
            foreach($zones as $key=>$z){
                if(in_array($z->id_geozone,$zonesOk)){$zoneDone = true; break;}
            }
        }

        $profilDone = false;
        $profiles = new data('documents_profiles',['id_document'=>$id_document]);
        foreach($profiles as $p){
            if(in_array($p->id_profile,$profilesOk)){$profilDone = true; break;}
        }

        if($zoneDone && $profilDone){
            return true;
        }else{
            return false;
        }
    }

    function deleteArchive($id_document){
        $documents = new o\documents(array('id_document'=>(int)$id_document));
        $filename = dirname(dirname(getcwd())).'/protected/documents/archives/'.$documents->id_document.'.zip';
        if(file_exists($filename))
            unlink($filename);

        $lCart = new data('carts_documents',['id_document'=>$documents->id_document]);
        foreach($lCart as $c){
            $cart = new o\carts(array('id_cart'=>(int)$c->id_cart));
            $filename = dirname(dirname(getcwd())).'/protected/documents/archives/'.$cart->hash.'.zip';
            if(file_exists($filename))
                unlink($filename);
        }
    }

    function deleteCartArchive($id_cart)
    {
        $cart = new o\carts(array('id_cart' => (int)$id_cart));
        $filename = dirname(dirname(getcwd())) . '/protected/documents/archives/' . $cart->hash . '.zip';
        if (file_exists($filename))
            unlink($filename);
    }

}
